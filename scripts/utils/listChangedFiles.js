'use strict'

const exec = require('./exec')

const execGitCmd = args =>
  exec('git', args)
    .trim()
    .toString()
    .split('\n')

module.exports = mergeBaseBranch => {
  const mergeBase = execGitCmd([
    'merge-base',
    'HEAD',
    'origin/' + mergeBaseBranch,
  ])

  return new Set([
    ...execGitCmd(['diff', '--name-only', '--diff-filter=ACMRTUB', mergeBase]),
    ...execGitCmd(['ls-files', '--others', '--exclude-standard']),
  ])
}
