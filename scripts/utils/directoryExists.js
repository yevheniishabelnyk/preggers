/**
 *
 * directoryExists
 *
 */

const fs = require('fs')

function directoryExists(filePath) {
  try {
    return fs.statSync(filePath).isDirectory()
  } catch (err) {
    return false
  }
}

module.exports = directoryExists
