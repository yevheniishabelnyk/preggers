const execFileSync = require('child_process').execFileSync

module.exports = (command, args) => {
  console.log('> ' + [command].concat(args).join(' '))
  const options = {
    cwd: process.cwd(),
    env: process.env,
    stdio: 'pipe',
    encoding: 'utf-8',
  }
  return execFileSync(command, args, options)
}
