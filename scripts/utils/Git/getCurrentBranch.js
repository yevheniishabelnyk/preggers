/**
 *
 * getCurrentBranch
 *
 */

const git = require('simple-git')
const trim = require('trim')

module.exports = () =>
  new Promise((resolve, reject) => {
    const GitClient = git()

    GitClient.branchLocal((err, branchSummary) => {
      if (err) {
        return reject(err)
      }

      if (!branchSummary.current) {
        return reject({
          message: 'getCurrentBranch error: missing branchSummary.current data',
        })
      }

      return resolve(trim(branchSummary.current))
    })
  })
