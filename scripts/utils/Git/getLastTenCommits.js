/**
 *
 * getLastTenCommits
 *
 */

const exec = require('../execPromise')

module.exports = (targetBranch, parentBranch) =>
  exec(
    `git log --pretty=format:"- %s %Cgreen(%cd)%Creset" --graph --abbrev-commit --date=relative ${parentBranch}..${targetBranch}`
  )
    .then(function(commits = '') {
      const lastTenCommits = commits
        .replace(/\*/g, '')
        .trim()
        .split('\n')
        .slice(0, 9)
        .join('\n')

      return lastTenCommits
    })
    .catch(err => console.info('err: ', err))
