/**
 *
 * getUserEmail
 *
 */

const git = require('simple-git')
const trim = require('trim')

module.exports = () =>
  new Promise((resolve, reject) => {
    const GitClient = git()

    GitClient._run(['config', '--global', 'user.email'], function(err, email) {
      if (err) {
        return reject(err)
      }

      return resolve(trim(email))
    })
  })
