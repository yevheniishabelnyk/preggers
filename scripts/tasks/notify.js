/**
 *
 * Expo post publish notification
 *
 */

const Git = require('../utils/Git')
const SlackBot = require('../bots/SlackBot')
const config = require('../config')
const path = require('path')
const argv = require('minimist')(process.argv.slice(2))

const projectDir = path.resolve(__dirname, '../../')

const slackBot = new SlackBot({
  token: config.slackToken,
})

const nofify = () =>
  new Promise((resolve, reject) => {
    process.chdir(projectDir)

    const gitData = {
      originUrl: '',
      currentBranch: '',
      username: '',
    }

    return Git.getOriginUrl()
      .then(gitOriginUrl => {
        gitData.originUrl = gitOriginUrl

        return Git.getUserEmail().then(gitUserEmail => {
          gitData.userEmail = gitUserEmail

          return Git.getCurrentBranch()
            .then(gitCurrentBranch => {
              gitData.currentBranch = gitCurrentBranch

              const publisher = config.users.find(
                user => user.gitUserEmail === gitData.userEmail
              )

              switch (argv.subject) {
                case 'rules':
                case 'functions':
                case 'admin':
                case 'appengine':
                  return slackBot.sendDeploymentReport({
                    environment: argv.env,
                    subject: argv.subject,
                    publisher,
                    gitData: gitData,
                  })

                default:
                  return Promise.resolve()
              }
            })
            .catch(err => {
              console.info('err: ', err)

              const publisher = config.users.find(
                user => user.gitUsername === gitData.username
              )

              switch (argv.subject) {
                case 'rules':
                case 'functions':
                case 'admin':
                case 'appengine':
                  return slackBot.sendDeploymentReport({
                    environment: argv.env,
                    subject: argv.subject,
                    publisher,
                    gitData: gitData,
                  })

                default:
                  return Promise.resolve()
              }
            })
        })
      })
      .then(data => resolve(data))
      .catch((err, data) => {
        console.info('data: ', data)
        reject('Error:\n' + JSON.stringify(err, null, 4))
      })
  })

nofify().catch(err => console.info(err))
