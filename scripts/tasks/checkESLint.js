'use strict'

const chalk = require('chalk')

const glob = require('glob')
const CLIEngine = require('eslint').CLIEngine
const listChangedFiles = require('../utils/listChangedFiles')

const argv = require('minimist')(process.argv.slice(2))

const baseBranch = argv.base || 'master'

const changedFiles = listChangedFiles(baseBranch)

// console.info('changedFiles: ', changedFiles)

const files = glob
  .sync('**/*.js', { ignore: '**/node_modules/**' })
  .filter(f => changedFiles.has(f))

// console.info('files: ', files)

if (!files.length) {
  return
}

const cli = new CLIEngine({ useEslintrc: true })

const report = cli.executeOnFiles(files.filter(file => file !== 'package.json'))

const messages = report.results.filter(item => {
  const ignoreMessage =
    'File ignored because of a matching ignore pattern. Use "--no-ignore" to override.'

  return !(item.messages[0] && item.messages[0].message === ignoreMessage)
})

const formatter = cli.getFormatter()

const output = formatter(messages)

console.info('\n', output)

if (!report.warningCount && !report.errorCount) {
  console.log(chalk.cyan('    LINT PASSED'))
} else {
  console.log(chalk.red('    LINT FAILED\n'))

  process.exit(1)
}
