'use strict'

const chalk = require('chalk')
const glob = require('glob')
const prettier = require('prettier')
const fs = require('fs')
const listChangedFiles = require('../utils/listChangedFiles')
const prettierConfigPath = require.resolve('../../.prettierrc')

const argv = require('minimist')(process.argv.slice(2))

const baseBranch = argv.base || 'master'

const changedFiles = listChangedFiles(baseBranch)

let didWarn = false
let didError = false

const files = glob
  .sync('**/*.{js,json,md}', { ignore: '**/node_modules/**' })
  .filter(f => changedFiles.has(f))

if (!files.length) {
  return
}

files.forEach(file => {
  const options = prettier.resolveConfig.sync(file, {
    config: prettierConfigPath,
  })

  try {
    const input = fs.readFileSync(file, 'utf8')

    if (!prettier.check(input, options)) {
      if (!didWarn) {
        console.log(
          '\n' +
            chalk.red(
              `  This project uses Prettier to format all JavaScript code.\n\n`
            ) +
            chalk.dim('    Please run ') +
            chalk.reset('npm run prettify') +
            chalk.dim(
              ` and add changes to files listed below to your commit:`
            ) +
            `\n`
        )

        didWarn = true
      }

      console.log('    ' + file)
    }
  } catch (error) {
    didError = true
    console.log('\n\n' + error.message)
    console.log('    ' + file)
  }
})

if (didWarn || didError) {
  console.log('\n\n')
  process.exit(1)
} else {
  console.log(chalk.cyan('\n    All changed files are prettified!'))
}
