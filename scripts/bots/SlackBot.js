/**
 *
 * SlackBot
 *
 */

'use strict'

const request = require('request')
const webhookUrls = require('../config').webhookUrls

class SlackBot {
  constructor(props) {
    this.token = props.token

    this.sendMessage = this.sendMessage.bind(this)
    this.sendDeploymentReport = this.sendDeploymentReport.bind(this)
  }

  sendMessage(data, channel) {
    return new Promise((resolve, reject) => {
      request.post(
        webhookUrls[channel],
        {
          form: {
            payload: JSON.stringify(data),
          },
        },
        function(err, response) {
          if (err) {
            return reject(err)
          }

          if (response.body !== 'ok') {
            return reject(new Error(response.body))
          }

          resolve(response.body)
        }
      )
    })
  }

  sendDeploymentReport({ environment, subject, publisher, gitData }) {
    let appName, channel, projectId

    switch (environment) {
      case 'development':
        appName = 'Preggers Development'
        channel = 'deploy_dev'
        projectId = 'preggers-development'
        break

      case 'staging':
        appName = 'Preggers Staging'
        channel = 'deploy_stage'
        projectId = 'preggers-taging'
        break

      case 'production':
        appName = 'Preggers'
        channel = 'deploy'
        projectId = 'preggers-production'
        break

      default:
        appName = 'Preggers'
        channel = 'deploy'
        projectId = 'preggers-production'
    }

    let text

    switch (subject) {
      case 'rules':
        text = `${appName} published *Database Rules*!`
        break

      case 'functions':
        text = `${appName} published *Cloud Functions*!`
        break

      case 'admin': {
        const host =
          environment === 'production'
            ? 'https://admin.preggersapp.com'
            : environment === 'staging'
              ? 'https://staging.preggersapp.com'
              : ''

        text = `${appName} published *Admin* to ${host}!`
        break
      }

      case 'appengine': {
        const appangineJobsUrl = `https://console.cloud.google.com/appengine/taskqueues/cron?project=${projectId}`

        text = `${appName} published *App Engine*! You can view and run *App Engine* jobs here ${appangineJobsUrl}.`
        break
      }

      default:
        text = `${appName} published new release!`
    }

    const message = {
      text,
      mrkdwn_in: ['text'],
      attachments: [],
      unfurl_links: 0,
    }

    const publisherAttachment = {
      fallback: '',
      pretext: '',
      ts: Date.now() / 1000,
      fields: [
        {
          title: 'Publisher',
          value: `<@${publisher.slackUsername}>`,
          short: true,
        },
        {
          title: 'Git branch',
          value: `<${gitData.originUrl}/branch/${gitData.currentBranch}|${
            gitData.currentBranch
          }>`,
          short: true,
        },
      ],
    }

    message.attachments.push(publisherAttachment)

    return this.sendMessage(message, channel)
  }
}

module.exports = SlackBot
