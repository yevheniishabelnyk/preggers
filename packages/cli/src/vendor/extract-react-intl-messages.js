'use strict'
const path = require('path')
const fs = require('fs')
const mkdirp = require('mkdirp')
const merge = require('lodash/merge')
const difference = require('lodash/difference')
const keys = require('lodash/keys')
const yaml = require('js-yaml')
const pify = require('pify')
const { flatten, unflatten } = require('flat')
const loadJsonFile = require('load-json-file')
const extractReactIntl = require('./extract-react-intl')
const sortKeys = require('sort-keys')
const prettier = require('prettier')

const repoRoot = path.resolve(process.cwd(), '../../')
const prettierConfigPath = path.resolve(repoRoot, './.prettierrc')
const prettierConfig = JSON.parse(fs.readFileSync(prettierConfigPath, 'utf8'))

const writeJson = (outputPath, obj) => {
  const config = Object.assign({}, prettierConfig, { parser: 'json' })
  delete config.overrides

  const json = prettier.format(JSON.stringify(obj), config)

  return new Promise((resolve, reject) =>
    fs.writeFile(`${outputPath}.json`, json, 'utf8', err => {
      if (!err) {
        resolve()
      } else {
        console.info('err: ', err)

        reject()
      }
    })
  )
}

const writeYaml = (outputPath, obj) => {
  return pify(fs.writeFile)(`${outputPath}.yml`, yaml.safeDump(obj), 'utf8')
}

const isJson = ext => ext === 'json'

function loadLocaleFiles(locales, buildDir, ext, delimiter) {
  const oldLocaleMaps = {}

  try {
    mkdirp.sync(buildDir)
  } catch (err) {
    console.info('err: ', err)
  }

  for (const locale of locales) {
    const file = path.resolve(buildDir, `${locale}.${ext}`)

    // Initialize json file
    try {
      const output = isJson(ext) ? JSON.stringify({}) : yaml.safeDump({})
      fs.writeFileSync(file, output, { flag: 'wx' })
    } catch (err) {
      if (err.code !== 'EEXIST') {
        throw err
      }
    }

    let messages = isJson(ext)
      ? loadJsonFile.sync(file)
      : yaml.safeLoad(fs.readFileSync(file, 'utf8'), { json: true })

    messages = flatten(messages, { delimiter })

    oldLocaleMaps[locale] = {}
    for (const messageKey of Object.keys(messages)) {
      const message = messages[messageKey]
      if (message && typeof message === 'string' && message !== '') {
        oldLocaleMaps[locale][messageKey] = messages[messageKey]
      }
    }
  }
  return oldLocaleMaps
}

module.exports = (locales, pattern, buildDir, opts) => {
  if (!Array.isArray(locales)) {
    return Promise.reject(
      new TypeError(`Expected a Array, got ${typeof locales}`)
    )
  }

  if (typeof pattern !== 'string') {
    return Promise.reject(
      new TypeError(`Expected a string, got ${typeof pattern}`)
    )
  }

  if (typeof buildDir !== 'string') {
    return Promise.reject(
      new TypeError(`Expected a string, got ${typeof buildDir}`)
    )
  }

  const jsonOpts = { format: 'json', flat: true }
  const yamlOpts = { format: 'yaml', flat: false }
  const defautlOpts =
    opts && opts.format && !isJson(opts.format) ? yamlOpts : jsonOpts

  opts = Object.assign({ defaultLocale: 'en' }, defautlOpts, opts)

  const ext = isJson(opts.format) ? 'json' : 'yml'

  const { defaultLocale, cwd } = opts

  const delimiter = opts.delimiter ? opts.delimiter : '.'

  const oldLocaleMaps = loadLocaleFiles(locales, buildDir, ext, delimiter)

  return extractReactIntl(locales, pattern, {
    defaultLocale,
    cwd,
    globOptions: {
      cwd,
    },
  }).then(newLocaleMaps => {
    return Promise.all(
      locales.map(locale => {
        // If the default locale, overwrite the origin file
        const unusedMessagesIds = difference(
          keys(oldLocaleMaps[locale]),
          keys(newLocaleMaps[locale])
        )

        unusedMessagesIds.forEach(messageId => {
          if (!/^\s\s__\[UNUSED\]__\s\s/.test(messageId)) {
            oldLocaleMaps[locale][`  __[UNUSED]__  ${messageId}`] =
              oldLocaleMaps[locale][messageId]
            delete oldLocaleMaps[locale][messageId]
          }
        })

        const localeMap =
          locale === defaultLocale
            ? merge(oldLocaleMaps[locale], newLocaleMaps[locale])
            : merge(newLocaleMaps[locale], oldLocaleMaps[locale])

        const fomattedLocaleMap = opts.flat
          ? sortKeys(localeMap, { deep: true })
          : unflatten(sortKeys(localeMap), { delimiter })

        const fn = isJson(opts.format) ? writeJson : writeYaml

        return fn(path.resolve(buildDir, locale), fomattedLocaleMap)
      })
    )
  })
}
