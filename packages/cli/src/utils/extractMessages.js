/**
 *
 * Extract react-intl messages util
 *
 */

'use strict'

const path = require('path')

const extractReactIntlMessages = require('../vendor/extract-react-intl-messages')

const repoRoot = path.resolve(process.cwd(), '../../')

const defaultConfigPath = path.resolve(
  repoRoot,
  './packages/app/config/default.json'
)

const defaultConfig = require(defaultConfigPath)

const pattern = 'src/**/!(*.test).js'

const translationsDir = path.resolve(repoRoot, './packages/app/translations')
const appDir = path.resolve(repoRoot, './packages/app')

const defaultLocale = defaultConfig.DEFAULT_LOCALE

module.exports = (locales, buildDir = translationsDir) =>
  extractReactIntlMessages(locales, pattern, buildDir, {
    defaultLocale,
    cwd: appDir,
  })
