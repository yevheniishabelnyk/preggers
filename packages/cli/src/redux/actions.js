/**
 *
 * App actions
 *
 */

import * as Constant from './constants'

export const setApp = (app, environment) => ({
  type: Constant.SET_APP,
  app,
  environment,
})

export const setToken = (token, environment) => ({
  type: Constant.SET_TOKEN,
  token,
  environment,
})

export const setProjectId = (projectId, environment) => ({
  type: Constant.SET_PROJECT_ID,
  projectId,
  environment,
})
