/**
 *
 * App constants
 *
 */

export const SET_APP = 'SET_APP'
export const SET_TOKEN = 'SET_TOKEN'
export const SET_PROJECT_ID = 'SET_PROJECT_ID'
