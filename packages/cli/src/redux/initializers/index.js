/**
 *
 * Initializers
 *
 */

import initLogger from './redux-logger'
import initReducers from './reducers'
import initMiddleware from './middleware'

export default [initMiddleware, initReducers, initLogger]
