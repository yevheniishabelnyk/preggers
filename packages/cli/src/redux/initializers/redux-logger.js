/**
 *
 * Redux logger
 *
 */

import { createLogger } from 'redux-logger'
import { applyMiddleware } from 'redux'

const logger = createLogger({
  collapsed: true,
  // only log in development mode
  predicate: () => process.env.CLI_APP_ENV === 'development',
  colors: false,
})

export default {
  enhancers:
    process.env.CLI_APP_ENV === 'development' ? [applyMiddleware(logger)] : [],
}
