/**
 *
 * Reducers
 *
 */

import * as Reducer from '../reducers'

const reducers = {
  main: Reducer.mainReducer,
}

export default {
  name: 'reducers',
  reducers: reducers,
}
