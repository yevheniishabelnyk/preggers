/**
 *
 * Main reducer
 *
 */

import * as Constant from '../constants'

const initialState = {
  environments: {},
}

export default function mainReducer(state = initialState, action) {
  switch (action.type) {
    case Constant.SET_APP:
      return {
        ...state,
        environments: {
          ...state.environments,
          [action.environment]: {
            ...(state.environments[action.environment] || {}),
            app: action.app,
          },
        },
      }

    case Constant.SET_TOKEN:
      return {
        ...state,
        environments: {
          ...state.environments,
          [action.environment]: {
            ...(state.environments[action.environment] || {}),
            token: action.token,
          },
        },
      }

    case Constant.SET_PROJECT_ID:
      return {
        ...state,
        environments: {
          ...state.environments,
          [action.environment]: {
            ...(state.environments[action.environment] || {}),
            projectId: action.projectId,
          },
        },
      }

    default:
      return state
  }
}
