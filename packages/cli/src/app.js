/**
 *
 * App
 *
 */

import chalk from 'chalk'
import clear from 'clear'
import figlet from 'figlet'
import inquirer from 'inquirer'
import Preferences from 'preferences'
import { PathPrompt } from 'inquirer-path'
import * as admin from 'firebase-admin'

const shell = require('shelljs')
import { google } from 'googleapis'

import store from './redux/store'
import * as Action from './redux/actions'

import { values } from 'lodash'

import prettyjson from 'prettyjson'

import { Spinner } from 'cli-spinner'
const spinner = new Spinner('%s')

inquirer.registerPrompt('path', PathPrompt)

const repoRoot = path.resolve(process.cwd(), '../../')
const appDefaultConfigPath = path.resolve(
  repoRoot,
  './packages/app/config/default.json'
)
const migrationsDirPath = path.resolve(repoRoot, './packages/migrations')
const prettierConfigPath = path.resolve(repoRoot, './.prettierrc')
const prettierConfigRoot = JSON.parse(
  fs.readFileSync(prettierConfigPath, 'utf8')
)

const prettierConfig = Object.assign({}, prettierConfigRoot, { parser: 'json' })
delete prettierConfig.overrides

import prettier from 'prettier'
import { table } from 'table'

import languages from 'languages'
import translate from 'translate'
import fs from 'fs-extra'
import path from 'path'

import { keys, omit, without, pull } from 'lodash'

import { chalkColors } from './constants'

import extractMessagesUtil from './utils/extractMessages'

const appConfigDirPath = path.resolve(repoRoot, './packages/app/config')

const config = {
  default: require(appConfigDirPath + '/default.json'),
  development: require(appConfigDirPath + '/development.json'),
  staging: require(appConfigDirPath + '/staging.json'),
  production: require(appConfigDirPath + '/production.json'),
}

let appLocales = [...config.default.APP_LOCALES]
let defaultLocale = config.default.DEFAULT_LOCALE

const migrationsTableConfig = {
  columns: {
    0: {
      alignment: 'left',
      minWidth: 10,
    },
    1: {
      alignment: 'left',
      minWidth: 10,
    },
    2: {
      alignment: 'left',
      minWidth: 10,
    },
    3: {
      alignment: 'left',
      width: 70,
    },
  },
}

clear()

console.log(
  chalk
    .hex('#F63669')
    .bold(figlet.textSync('Preggers CLI', { horizontalLayout: 'full' })),
  '\n'
)

const start = () => browseMain()

/**
 * MAIN
 */
const browseMain = () =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What do you want to work with?',
        choices: [
          'i18n',
          'Admins',
          'Users',
          'Migrations',
          new inquirer.Separator(),
          'Quit',
        ],
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'i18n':
          return browsei18n()

        case 'Admins':
          return selectEnvironment(browseAdmins)

        case 'Users':
          return selectEnvironment(browseUsers)

        case 'Migrations':
          return selectEnvironment(browseMigrations)

        case 'Quit':
          return process.exit()
      }
    })

/**
 * i18n
 */
const browsei18n = () =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What do you want to work with in i18n?',
        choices: [
          'Translations',
          'Languages',
          new inquirer.Separator(),
          'Go Back',
        ],
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'Translations':
          return browseTranslations()

        case 'Languages':
          return browseLanguages()

        case 'Go Back':
          return browseMain()
      }
    })

/**
 * TRANSLATIONS
 */
const browseTranslations = (defaultValue = 'extract_messages') =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What would you like to do?',
        choices: [
          { value: 'extract_messages', name: 'Extract default messages' },
          {
            value: 'get_missing_translations',
            name: 'Get missing translations',
          },
          {
            value: 'delete_unused_translations',
            name: 'Delete unused translations',
          },
          new inquirer.Separator(),
          'Go Back',
        ],
        default: defaultValue,
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'extract_messages':
          return extractMessages()

        case 'get_missing_translations':
          return browseGetMissingTranslations()

        case 'delete_unused_translations':
          return deleteUnusedTranslations()

        case 'Go Back':
          return browsei18n()
      }
    })

/**
 * LANGUAGES
 */
const browseLanguages = () =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What would you like to do?',
        choices: [
          { value: 'show_app_languages', name: 'Show app languages' },
          { value: 'add', name: 'Add new language' },
        ].concat(
          appLocales.length > 1
            ? [
                { value: 'remove', name: 'Remove language' },
                new inquirer.Separator(),
                'Go Back',
              ]
            : [new inquirer.Separator(), 'Go Back']
        ),
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'show_app_languages':
          return showAppLanguages()

        case 'add':
          return chooseWayToAddLanguage()

        case 'remove':
          return chooseLanguageToRemove()

        case 'Go Back':
          return browsei18n()
      }
    })

/**
 * CHOOSE WAY TO ADD LANGUAGE
 */
const chooseWayToAddLanguage = () =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'How do you want to add languages?',
        choices: [
          { value: 'languages_all_list', name: 'List all languages' },
          { value: 'enter_language_name', name: 'Enter language name' },
          new inquirer.Separator(),
          'Go Back',
        ],
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'languages_all_list':
          return addLanugageFromList()

        case 'enter_language_name':
          return addLanguageByName()

        case 'Go Back':
          return browseLanguages()
      }
    })

/**
 * ADD LANUGAGE FROM LIST
 */
const addLanugageFromList = () =>
  inquirer
    .prompt([
      {
        type: 'checkbox',
        name: 'action',
        message: 'What languages do you want to add?',
        choices: languages
          .getAllLanguageCode()
          .filter(locale => appLocales.indexOf(locale) === -1)
          .map(locale => ({
            value: locale,
            name: languages.getLanguageInfo(locale).name,
          })),
        pageSize: 20,
      },
    ])
    .then(({ action }) => {
      if (action !== 'Go Back') {
        return addLanguage(action)
      }

      return browseLanguages()
    })

/**
 * ADD LANGUAGE BY NAME
 */
const addLanguageByName = () => {
  const languagesCodes = languages.getAllLanguageCode()
  const languagesNames = languagesCodes.map(
    locale => languages.getLanguageInfo(locale).name
  )
  const appLanguagesNames = appLocales.map(
    locale => languages.getLanguageInfo(locale).name
  )

  inquirer
    .prompt([
      {
        type: 'input',
        name: 'action',
        message: 'What lanugage do you want to add?',
        validate: function(value) {
          if (appLanguagesNames.indexOf(value) !== -1) {
            return 'This language has already been added'
          }

          if (languagesNames.indexOf(value) !== -1) {
            return true
          }

          return 'Please enter a valid language name'
        },
      },
    ])
    .then(({ action }) => {
      const languageIndex = languagesNames.indexOf(action)
      const languageLocale = languagesCodes[languageIndex]

      return addLanguage([languageLocale])
    })
}

/**
 * ADD LANGUAGE
 */
const addLanguage = (locales = []) => {
  const addedLanguages = []

  locales.forEach(locale => {
    const languageName = languages.getLanguageInfo(locale).name
    const translationFilePath = path.resolve(
      repoRoot,
      `./packages/app/translations/${locale}.json`
    )

    appLocales.push(locale)

    fs.writeFile(translationFilePath, '{}', 'utf8', err => {
      if (!err) {
        const defaultLocaleTranslationsPath = path.resolve(
          repoRoot,
          `./packages/app/translations/${defaultLocale}.json`
        )

        delete require.cache[defaultLocaleTranslationsPath]

        const defaultLocaleTranslations = require(defaultLocaleTranslationsPath)

        const localeTranslationsWithEmpytIds = Object.assign(
          {},
          defaultLocaleTranslations
        )

        keys(localeTranslationsWithEmpytIds).forEach(messageId => {
          localeTranslationsWithEmpytIds[messageId] = ''
        })

        const json = JSON.stringify(localeTranslationsWithEmpytIds, null, 2)

        fs.writeFile(translationFilePath, json, 'utf8', () => {
          // console.info('err: ', err)
        })
      }
    })

    addedLanguages.push(languageName)
  })

  const defaultConfig = require(appDefaultConfigPath)

  defaultConfig.APP_LOCALES = appLocales

  const json = JSON.stringify(defaultConfig, null, 2)

  fs.writeFile(appDefaultConfigPath, json, 'utf8', () => {
    // console.info('err: ', err)
  })

  updateDynamicImports(appLocales)

  if (locales.length) {
    console.log(
      chalk.hex('#308ae3')('\nYou have successfully added ') +
        chalk.red(
          addedLanguages.length > 1
            ? addedLanguages.slice(0, -1).join(', ') +
              ' and ' +
              addedLanguages[addedLanguages.length - 1]
            : addedLanguages[0]
        ) +
        chalk.hex('#308ae3')(' to the app!\n')
    )
  }

  return browseLanguages()
}

/**
 * UPDATE DYNAMIC IMPORTS
 *
 * Dynamically add imports react-intl/locale-data/<locale>
 * https://github.com/yahoo/react-intl/issues/777
 */
const updateDynamicImports = (locales = []) =>
  new Promise((resolve, reject) => {
    const dynamicImportsPath = path.resolve(
      repoRoot,
      './packages/app/translations/index.js'
    )

    let dynamicImports = ''

    dynamicImports +=
      '// This file created with CLI app, never change it manually!!!\n'

    dynamicImports += locales.reduce((out, locale, index, array) => {
      if (index === 0) {
        out += 'module.exports.translations = {\n'
      }

      out += `  ${locale}: require('./${locale}.json'),\n`

      if (index === array.length - 1) {
        out += '}\n'
      }

      return out
    }, '')

    dynamicImports += locales.reduce((out, locale, index, array) => {
      if (index === 0) {
        out += 'module.exports.localesData = {\n'
      }

      out += `  ${locale}: require('react-intl/locale-data/${locale}.js'),\n`

      if (index === array.length - 1) {
        out += '}\n'
      }

      return out
    }, '')

    fs.writeFile(dynamicImportsPath, dynamicImports, 'utf8', err => {
      // console.info('err: ', err)
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })

/**
 * GET ALL LANGUAGES
 */
const showAppLanguages = () => {
  const languagesNames = appLocales.map(locale => {
    const languageInfo = languages.getLanguageInfo(locale)
    const name = languageInfo.name || locale
    return name
  })

  const logText = languagesNames.reduce((out, language, index) => {
    const indexColor =
      language === languages.getLanguageInfo(defaultLocale).name
        ? 'red'
        : 'blue'

    out = out + `${chalk[indexColor](index + 1 + '. ')} ${language}\n`

    return out
  }, '\n')

  console.log(logText)

  return browseLanguages()
}

/**
 * CHOOSE LANGUAGE TO REMOVE
 */
const chooseLanguageToRemove = () =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What language do you want to remove?',
        choices: without(appLocales, defaultLocale)
          .map(locale => {
            const languageInfo = languages.getLanguageInfo(locale)
            const name = languageInfo.name || locale

            return { value: locale, name }
          })
          .concat([new inquirer.Separator(), 'Go Back']),
      },
    ])
    .then(({ action }) => {
      if (appLocales.indexOf(action) !== -1) {
        return removeLanguage(action)
      }

      return browseLanguages()
    })

/**
 * REMOVE LANGUAGE
 */
const removeLanguage = locale => {
  const languageName = languages.getLanguageInfo(locale).name
  const localeTranslationsPath = path.resolve(
    repoRoot,
    `./packages/app/translations/${locale}.json`
  )

  const defaultConfig = require(appDefaultConfigPath)

  pull(defaultConfig.APP_LOCALES, locale)
  pull(appLocales, locale)

  const json = JSON.stringify(defaultConfig, null, 2)

  fs.writeFile(appDefaultConfigPath, json, 'utf8', () => {
    // console.info('err: ', err)
  })

  if (fs.existsSync(localeTranslationsPath)) {
    fs.unlinkSync(localeTranslationsPath)
  }

  updateDynamicImports(appLocales)

  console.log(
    chalk.hex('#308ae3')('\nYou have successfully removed ') +
      chalk.red(languageName) +
      chalk.hex('#308ae3')(' from the app!\n')
  )

  return browseLanguages()
}

/**
 * EXTRACT MESSAGES
 */
const extractMessages = () => {
  console.log(
    chalk.hex('#829496')('\nExtracting...\nIt may take some time. Please wait.')
  )

  // return exec('cd ../../../ && npx prettier --write')
  //   .then(function() {
  //     console.log('good')
  //     return browseTranslations('get_missing_translations')
  //   })
  //   .catch(err => {
  //     console.info('err: ', err)

  //     return browseTranslations('get_missing_translations')
  //   })

  return extractMessagesUtil(appLocales)
    .then(() => {
      console.log(
        chalk.hex('#308ae3')('\nYou have successfully extracted messages!\n')
      )

      return browseTranslations('get_missing_translations')
    })
    .catch(() => {
      console.log(
        chalk.red('\nError: something went wrog! Please try again.\n')
      )

      return browseTranslations()
    })
}

/**
 * DELETE UNUSED TRANSLATIONS
 */
const deleteUnusedTranslations = () => {
  const defaultMessagesTempFilePath = path.resolve(
    __dirname,
    `./utils/${defaultLocale}.json`
  )

  delete require.cache[defaultMessagesTempFilePath]

  if (fs.existsSync(defaultMessagesTempFilePath)) {
    fs.unlinkSync(defaultMessagesTempFilePath)
  }

  return extractMessagesUtil([defaultLocale], 'src/utils')
    .then(() => {
      console.log(
        chalk.hex('#308ae3')(
          '\nYou have successfully deleted unused translations!\n'
        )
      )

      const defaultMessages = require(defaultMessagesTempFilePath)

      appLocales.forEach(locale => {
        const localeTranslationsPath = path.resolve(
          repoRoot,
          `./packages/app/translations/${locale}.json`
        )

        delete require.cache[localeTranslationsPath]

        const localeTranslations = require(localeTranslationsPath)

        const deletedLocaleTranslations = keys(localeTranslations).reduce(
          (out, messageId) => {
            if (
              defaultMessages[messageId] &&
              typeof localeTranslations[messageId] === 'string'
            ) {
              out[messageId] = localeTranslations[messageId]
            }

            return out
          },
          {}
        )

        const json = prettier.format(
          JSON.stringify(deletedLocaleTranslations, null, 2),
          prettierConfig
        )

        fs.writeFile(localeTranslationsPath, json, 'utf8', () => {
          // console.info('err: ', err)
        })
      })

      fs.unlinkSync(defaultMessagesTempFilePath)

      return browseTranslations()
    })
    .catch(err => {
      console.info('err: ', err)
      console.log(
        chalk.red('\nError: something went wrog! Please try again.\n')
      )

      return browseTranslations()
    })
}

/**
 * BROWSE GET MISSING TRANSLATIONS
 */
const browseGetMissingTranslations = () =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'For what language?',
        choices: without(appLocales, defaultLocale)
          .map(locale => {
            const languageInfo = languages.getLanguageInfo(locale)
            const name = languageInfo.name || locale

            return { value: locale, name }
          })
          .concat([
            new inquirer.Separator(),
            { value: 'all', name: 'All' },
            new inquirer.Separator(),
            'Go Back',
          ]),
        pageSize: 10,
      },
    ])
    .then(({ action }) => {
      if (appLocales.indexOf(action) !== -1) {
        return getMissingTranslations([action])
      }

      if (action === 'all') {
        return getMissingTranslations(appLocales)
      }

      return browseTranslations()
    })

/**
 * GET MISSING TRANSLATIONS
 */
const getMissingTranslations = (locales = []) => {
  const colors =
    locales.length > chalkColors.length
      ? chalkColors.concat(chalkColors)
      : chalkColors
  const ui = new inquirer.ui.BottomBar()
  let translationIndex = 1

  const defaultMessagesPath = path.resolve(
    repoRoot,
    `./packages/app/translations/${defaultLocale}.json`
  )
  let defaultMessages = {}

  if (fs.existsSync(defaultMessagesPath)) {
    delete require.cache[defaultMessagesPath]
    defaultMessages = require(defaultMessagesPath)
  }

  const defaultMessagesIds = keys(defaultMessages)
  const localeMessages = {}
  const missingMessages = {}
  const missingMessagesIds = {}
  const requiredTranslations = {}
  const localeMessagesPaths = {}

  locales.forEach(locale => {
    let messages = {}
    const localeMessagesPath = path.resolve(
      repoRoot,
      `./packages/app/translations/${locale}.json`
    )

    localeMessagesPaths[locale] = localeMessagesPath

    if (fs.existsSync(localeMessagesPath)) {
      delete require.cache[localeMessagesPath]
      messages = require(localeMessagesPath)
    }

    const localeMissingMessagesIds = keys(messages).filter(
      messageId => messages[messageId] === ''
    )

    const localeExistingMessagesIds = without(
      keys(messages),
      localeMissingMessagesIds
    )

    localeMissingMessagesIds.forEach(messageId => {
      if (defaultMessagesIds.indexOf(messageId) !== -1) {
        if (requiredTranslations[messageId]) {
          requiredTranslations[messageId] = requiredTranslations[
            messageId
          ].concat(locale)
        } else {
          requiredTranslations[messageId] = [locale]
        }
      }
    })

    missingMessagesIds[locale] = localeMissingMessagesIds
    localeMessages[locale] = messages
    missingMessages[locale] = omit(messages, localeExistingMessagesIds)
  })

  // console.info('requiredTranslations: ', requiredTranslations)

  const promises = keys(requiredTranslations).map(messageId => {
    const defaultMessage = defaultMessages[messageId]
    let queryText = defaultMessage
    const localesToTranslate = requiredTranslations[messageId]

    if (/\{.+\}/.test(queryText)) {
      queryText = queryText
        .replace(/\{/g, '<span class="notranslate">')
        .replace(/\}/g, '</span>')
    }

    return () =>
      new Promise((resolve, reject) => {
        const translatePromises = localesToTranslate.map(locale => {
          return () => {
            return translate(queryText, {
              from: defaultLocale,
              to: locale,
              engine: 'google',
              key: config.default.GOOGLE__API_KEY,
            })
          }
        })

        Promise.all(translatePromises.map(promise => promise()))
          .then(messageTranslations => {
            let logText = `\n${chalk.blue(
              translationIndex + '. '
            )} ${defaultMessage}\n`

            localesToTranslate.forEach((locale, localeIndex) => {
              let queryTranslation = messageTranslations[localeIndex]

              if (
                /<span class="notranslate">.+<\/span>/.test(queryTranslation)
              ) {
                queryTranslation = queryTranslation
                  .replace(/<span class="notranslate">/g, '{')
                  .replace(/<\/span>/g, '}')
              }

              missingMessages[locale][messageId] = queryTranslation

              logText =
                logText +
                `${chalk[colors[localeIndex]](
                  locale + ':'
                )} ${queryTranslation}\n`
            })

            ui.log.write(logText)

            translationIndex++

            resolve()
          })
          .catch(err => {
            reject(err)
          })
      })
  })

  spinner.start()
  const errors = []

  return Promise.all(promises.map(promise => promise()))
    .catch(err => {
      // console.info('err: ', err)
      errors.push(err)
      spinner.stop(true)

      console.log(
        chalk.red('\nError: something went wrog! Please try again.\n')
      )
    })
    .then(() => {
      if (translationIndex !== 1) {
        const writeResultPromises = keys(missingMessages).map(locale => {
          Object.assign(localeMessages[locale], missingMessages[locale])

          const json = prettier.format(
            JSON.stringify(localeMessages[locale]),
            prettierConfig
          )

          return () =>
            new Promise((resolve, reject) =>
              fs.writeFile(localeMessagesPaths[locale], json, 'utf8', err => {
                if (!err) {
                  resolve()
                } else {
                  console.info('err: ', err)

                  reject()
                }
              })
            )
        })

        return Promise.all(writeResultPromises.map(promise => promise())).then(
          () => {
            spinner.stop(true)

            console.log(
              chalk.hex('#308ae3')(
                '\nYou have successfully translated missing messages!\n'
              )
            )
          }
        )
      } else {
        spinner.stop(true)

        console.log(chalk.green('\nThere are no missing translations!\n'))
      }
    })
    .then(() => browseTranslations())
}

const selectEnvironment = cb =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'environment',
        message: 'In what environment do you want to work?',
        choices: [
          { value: 'production', name: 'Production' },
          { value: 'staging', name: 'Staging' },
          { value: 'development', name: 'Development' },
          new inquirer.Separator(),
          'Go Back',
        ],
      },
    ])
    .then(({ environment }) => {
      switch (environment) {
        case 'production':
        case 'staging':
        case 'development':
          return cb({ environment })

        case 'Go Back':
          return browseMain()

        default:
          return browseMain()
      }
    })

const browseUsers = ({ environment }) =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What would you like to do?',
        choices: [
          { value: 'get_by_id', name: 'Get user profile by ID' },
          { value: 'get_by_email', name: 'Get user profile by email' },
          new inquirer.Separator(),
          'Go Back',
        ],
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'get_by_id':
          return authenticate(environment, getUserById)
        case 'get_by_email':
          return authenticate(environment, getUserByEmail)
        case 'Go Back':
          return browseMain()
        default:
          return process.exit()
      }
    })

const getUserById = (app, environment) =>
  inquirer
    .prompt([
      {
        type: 'input',
        name: 'userId',
        message: 'Please enter user ID',
      },
    ])
    .then(({ userId }) =>
      app
        .auth()
        .getUser(userId)
        .then(function(userRecord) {
          // See the UserRecord reference doc for the contents of userRecord.
          console.log('\n')
          console.log(JSON.stringify(userRecord, null, 4))
          console.log('\n')
        })
        .catch(function(error) {
          console.log('Error fetching user data:', error)
        })
        .then(() => browseUsers({ environment }))
    )

const getUserByEmail = (app, environment) =>
  inquirer
    .prompt([
      {
        type: 'input',
        name: 'email',
        message: 'Please enter user email',
      },
    ])
    .then(({ email }) =>
      app
        .auth()
        .getUserByEmail(email)
        .then(function(userRecord) {
          // See the UserRecord reference doc for the contents of userRecord.
          console.log('\n')
          console.log(JSON.stringify(userRecord, null, 4))
          console.log('\n')
        })
        .catch(function(error) {
          console.log('\nError fetching user data:', error)
        })
        .then(() => browseUsers({ environment }))
    )

const browseAdmins = ({ environment }) =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What would you like to do?',
        choices: [
          { value: 'add', name: 'Add admin' },
          { value: 'remove', name: 'Remove admin' },
          { value: 'all', name: 'View all' },
          new inquirer.Separator(),
          'Go Back',
        ],
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'add':
          return authenticate(environment, addAdmin)
        case 'remove':
          return authenticate(environment, removeAdmin)
        case 'all':
          return authenticate(environment, viewAllAdmins)
        case 'Go Back':
          return browseMain()
        default:
          return process.exit()
      }
    })

const browseMigrations = ({ environment }) => {
  var migrations = []

  if (fs.existsSync(migrationsDirPath)) {
    const isDirectory = source => fs.lstatSync(source).isDirectory()
    const getDirectories = source =>
      fs
        .readdirSync(source)
        .map(name => path.join(source, name))
        .filter(isDirectory)

    const migrationDirectories = getDirectories(migrationsDirPath)

    migrations = migrationDirectories
      .map(dirPath => {
        const migrationMetadataPath = path.join(dirPath, 'meta.json')

        if (fs.existsSync(migrationMetadataPath)) {
          return require(migrationMetadataPath)
        }

        return null
      })
      .filter(item => item !== null)
  }

  let migrationsTable = ''

  if (!migrations.length) {
    console.log('\nThe directory with migrations is empty.\n')

    return browseMain()
  } else {
    const data = []
    data.push(['ID', 'Status', 'Rollback', 'Description'])

    migrations.forEach(meta => {
      data.push([
        meta.id,
        meta.environments[environment].status,
        meta.rollback || false,
        meta.description,
      ])
    })

    migrationsTable = table(data, migrationsTableConfig)
  }

  return inquirer
    .prompt([
      {
        type: 'list',
        name: 'migration',
        message: `What migration do you want to run? \n\n${migrationsTable}`,
        choices: migrations
          .map(meta => ({
            value: meta,
            name: meta.id,
          }))
          .concat([new inquirer.Separator(), 'Go Back']),
      },
    ])
    .then(({ migration }) => {
      switch (migration) {
        case 'Go Back':
          return browseMain()

        default:
          return authenticate(environment, (...rest) =>
            browseMigrationItem({ migration, environment, ...rest })
          )
      }
    })
}

const browseMigrationItem = ({ migration, environment }) =>
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'action',
        message: 'What do you want to do?',
        choices: [
          {
            value: 'Migrate',
            name: `Migrate (${migration.environments[environment].status})`,
          },
        ]
          .concat(
            migration.rollback &&
            migration.environments[environment].status === 'done'
              ? [{ value: 'Rollback', name: 'Rollback' }]
              : []
          )
          .concat([new inquirer.Separator(), 'Go Back']),
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case 'Migrate':
          return confirm()
            .then(() => runMigration({ migration, environment }))
            .then(() =>
              updateMigrationStatus({ migration, environment, status: 'done' })
            )
            .then(() => browseMigrations({ environment }))
            .catch(() => browseMigrations({ environment }))

        case 'Rollback': {
          return confirm()
            .then(() =>
              runMigration({ migration, environment, rollback: true })
            )
            .then(() =>
              updateMigrationStatus({
                migration,
                environment,
                status: 'pending',
              })
            )
            .then(() => browseMigrations({ environment }))
            .catch(() => browseMigrations({ environment }))
        }

        case 'Go Back':
          return browseMigrations({ environment })

        default:
          return process.exit()
      }
    })

const confirm = () =>
  inquirer
    .prompt([
      {
        type: 'confirm',
        name: 'isConfirmed',
        message: 'Are you sure?',
      },
    ])
    .then(({ isConfirmed }) => {
      if (isConfirmed) {
        return Promise.resolve()
      } else {
        return Promise.reject()
      }
    })

const updateMigrationStatus = ({ migration, environment, status }) => {
  const migrationItemMetaPath = path.join(
    migrationsDirPath,
    migration.id,
    'meta.json'
  )

  migration.environments[environment].status = status

  const json = prettier.format(
    JSON.stringify(migration, null, 2),
    prettierConfig
  )

  fs.writeFileSync(migrationItemMetaPath, json, 'utf8')

  return Promise.resolve()
}

const runMigration = ({ migration, environment, rollback }) => {
  const env = store.getState().main.environments[environment]

  if (!env.token) {
    return Promise.reject('Token is required to run migrations!')
  }

  const migrationItemPath = path.join(migrationsDirPath, migration.id)

  const queryFileName = rollback ? 'rollback.jq' : 'query.jq'

  let response = shell.exec(
    `
docker run \
--rm -v ${migrationItemPath}:/queries fcavalieri/zorba \
-q ${queryFileName} -f -i \
-e projectId:="${env.projectId}" \
-e token:="${env.token}"
`,
    {
      silent: true,
    }
  ).stdout

  console.info('response 1: ', response)

  try {
    response = JSON.parse(response)
  } catch (err) {
    console.info('err: ', err)
    return Promise.reject()
  }

  console.info('response: ', response)

  if (response.status !== 200) {
    return Promise.reject()
  }

  return Promise.resolve()
}

const addAdmin = (app, environment) =>
  inquirer
    .prompt([
      {
        type: 'input',
        name: 'email',
        message: 'Please enter user email',
      },
    ])
    .then(({ email }) =>
      app
        .auth()
        .getUserByEmail(email)
        .then(userRecord =>
          app
            .database()
            .ref('/admins')
            .child(userRecord.uid)
            .set(userRecord.email)
            .then(() => {
              console.log(
                chalk.hex('#0A64C8')('\nYou have successfully added'),
                chalk.yellow(userRecord.email),
                chalk.hex('#0A64C8')('to the admins!\n')
              )
            })
        )
        .catch(() => {
          console.log(
            chalk.red('\nThere is no user with email'),
            chalk.yellow(email),
            '\n'
          )
        })
        .then(() => browseAdmins({ environment }))
    )

const removeAdmin = (app, environment) => {
  spinner.start()

  return app
    .database()
    .ref('/admins')
    .once('value', adminsSnap => {
      spinner.stop(true)

      if (adminsSnap.exists()) {
        const admins = adminsSnap.val()

        const choices = []

        adminsSnap.forEach(snap => {
          const email = snap.val()
          const userId = snap.key

          choices.push({
            value: userId,
            name: email,
          })
        })

        choices.push(new inquirer.Separator())

        choices.push({ value: 'back', name: 'Go Back' })

        return inquirer
          .prompt([
            {
              type: 'list',
              name: 'userId',
              message: 'What user do you want to remove from the admins?',
              choices,
            },
          ])
          .then(({ userId }) => {
            if (userId === 'back') {
              return browseAdmins({ environment })
            } else {
              return app
                .database()
                .ref('/admins')
                .child(userId)
                .set(null)
                .then(() => {
                  console.log(
                    chalk.hex('#0A64C8')('\nYou have successfully removed'),
                    chalk.yellow(admins[userId]),
                    chalk.hex('#0A64C8')('from the admins!\n')
                  )

                  return browseAdmins({ environment })
                })
            }
          })
      } else {
        console.log(chalk.red('\nThere is no admins\n'))
      }

      return browseAdmins({ environment })
    })
}

const viewAllAdmins = (app, environment) => {
  spinner.start()

  return app
    .database()
    .ref('/admins')
    .once('value', adminsSnap => {
      spinner.stop(true)

      if (adminsSnap.exists()) {
        const admins = adminsSnap.val()

        console.log('\n', prettyjson.render({ admins: values(admins) }), '\n')
      } else {
        console.log(chalk.red('\nThere is no admins\n'))
      }

      return browseAdmins({ environment })
    })
}

start()

/*
 * Auth
 */
const authenticate = (environment, cb) => {
  const env = store.getState().main.environments[environment]
  const app = env && env.app

  if (app) {
    return cb(app, environment)
  } else {
    return getFirebaseAdminCredentials(environment, browseMain, credentials => {
      const app = admin.initializeApp(
        {
          credential: admin.credential.cert(credentials),
          databaseURL: config[environment].FIREBASE__DATABASE_URL,
        },
        environment
      )

      store.dispatch(Action.setApp(app, environment))
      store.dispatch(Action.setProjectId(credentials.project_id, environment))

      return getAccessToken(credentials)
        .then(accessToken => {
          store.dispatch(Action.setToken(accessToken, environment))

          return cb(app, environment)
        })
        .catch(() => {
          return cb(app, environment)
        })
    })
  }
}

const getAccessToken = credentials =>
  new Promise((resolve, reject) => {
    const scopes = [
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/firebase.database',
    ]

    const jwtClient = new google.auth.JWT(
      credentials.client_email,
      null,
      credentials.private_key,
      scopes
    )

    jwtClient.authorize(function(error, tokens) {
      if (error) {
        console.log('Error making request to generate access token:', error)

        reject(error)
      } else if (tokens.access_token === null) {
        console.log(
          'Provided service account does not have permission to generate access tokens'
        )

        reject(
          new Error(
            'Provided service account does not have permission to generate access tokens'
          )
        )
      } else {
        resolve(tokens.access_token)
      }
    })
  })

function askFirebaseAdminCredentials(environment, callback) {
  console.log(
    '\nThis operation requires authentication. Please enter a path to',
    chalk.hex('#F63669')(config[environment].FIREBASE__PROJECT_ID),
    "Private Key.\nIf you don't have it, you can generate it here",
    chalk.blue(
      `https://console.firebase.google.com/u/0/project/${
        config[environment].FIREBASE__PROJECT_ID
      }/settings/serviceaccounts/adminsdk`
    ),
    '\n'
  )

  inquirer
    .prompt([
      {
        type: 'path',
        name: 'path',
        message: 'Enter a path',
        default: repoRoot,
      },
    ])
    .then(callback)
}

function getFirebaseAdminCredentials(environment, goBack, callback) {
  const prefs = new Preferences('preggersFirebaseCLI')

  if (
    prefs.preggersFirebaseCLI &&
    prefs.preggersFirebaseCLI[environment] &&
    prefs.preggersFirebaseCLI[environment].private_key &&
    prefs.preggersFirebaseCLI[environment].client_email
  ) {
    return callback(prefs.preggersFirebaseCLI[environment])
  }

  return askFirebaseAdminCredentials(environment, answers => {
    if (answers.path) {
      const credentials = require(answers.path)

      if (credentials && credentials.private_key && credentials.client_email) {
        prefs.preggersFirebaseCLI = {
          [environment]: credentials,
        }

        return callback(credentials)
      } else {
        console.log(chalk.red('\nError: credentials has wrong format!\n'))

        return goBack()
      }
    } else {
      console.log(chalk.red('Error: path is required!'))

      return goBack()
    }
  })
}
