![Preggers logo](./assets/images/logo.png 'Preggers')

# Preggers CLI

Command Line application for managing project translations and updates.

- [Preggers CLI](#preggers-cli)
  - [Start](#start)
    - [First run](#first-run)
    - [Regular start](#regular-start)
  - [Internationalization](#internationalization)
    - [Translations](#translations)
    - [Languages](#languages)

## Start

### First run

To run `Preggers CLI` on your local machine at the first time execute follows

```shell
git clone https://bitbucket.org/strollerapp/preggers
cd preggers
npm install -g yarn
yarn

npm run cli
```

This will install all dependencies and start the application.

### Regular start

```shell
npm run cli
```

This will start the application.

## Internationalization

If you want to work with internationalization you should start the app and open `i18n` section.

![First slide](./assets/images/slide-1.gif)

### Translations

To extract Preggers default messages

```shell
i18n > Translations > Extract default messages
```

![Second slide](./assets/images/slide-2.gif)

After that you can get missing translations from Google Translate

```shell
i18n > Translations > Get missing translations
```

![Third slide](./assets/images/slide-3.gif)

### Languages

You can show, add or remove Preggers languages

```shell
i18n > Lanugages > ...
```

![Fourth slide](./assets/images/slide-4.gif)
