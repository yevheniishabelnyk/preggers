const functions = require('firebase-functions')
const functionsConfig = functions.config()

/**
 * Set <Mandrill Api Key>, <Twitter Consumer Secret>, <Twitter Consumer Key>, <Algolia App ID>, <Algolia Admin Key> with command
 *
 * firebase use dev && firebase functions:config:set mandrill.api_key=<Api Key>
 */
module.exports = {
  mandrill:
    functionsConfig.mandrill ||
    {
      // api_key: <Mandrill Api Key>
    },

  database: functionsConfig.database || {
    environment: 'development', // development|staging|production
  },

  twitter:
    functionsConfig.twitter ||
    {
      // consumer_secret: <Twitter Consumer Secret>
      // consumer_key: <Twitter Consumer Key>
    },

  algolia:
    functionsConfig.algolia ||
    {
      // app_id: <Algolia App ID>
      // admin_key: <Algolia Admin Key>
    },
}
