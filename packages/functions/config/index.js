/*
 *
 * Configs
 *
 */

const merge = require('lodash/merge')
const remoteConfig = require('./remote')

var localConfig

switch (remoteConfig.database.environment) {
  case 'development':
    localConfig = require('./development')
    break

  case 'staging':
    localConfig = require('./staging')
    break

  case 'production':
    localConfig = require('./production')
    break

  default:
    localConfig = require('./development')
}

const localDefaults = require('./default')

module.exports = merge(remoteConfig, localDefaults, localConfig)
