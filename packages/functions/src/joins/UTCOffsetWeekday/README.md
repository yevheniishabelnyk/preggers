# UTC Offset Weekday Join Table

## Cases

1.  update week day when user pregnancy `due date` change
2.  update week day when user pregnancy `length type` change
3.  update UTC offset when `UTCOffset` field change in user profile
    - 3.1 user change UTC zone
    - 3.2 user removes UTC zone
4.  update week day when `pregnancy` field change in user profile

    - 4.1 pregnancy was removed
    - 4.2 pregnancy was created
    - 4.3 pregnancy was cancelled
    - 4.4 pregnancy was ended with status child is borned
    - 4.5 user connected to partner and got mother pregnancy
    - 4.6 user is connected and mother removed her pregnancy
    - 4.7 user is connected and mother cancelled her pregnancy
    - 4.8 user is connected and mother ended pregnancy with status child is borned
    - 4.9 user pregnancy has change due to disconnection from mother
    - 4.10 user was removed
