/**
 *
 * remove user from UTCOffsetWeekday join table
 *
 */

module.exports = function(userId) {
  const DB = require('src/services/DB')

  return DB.get(`/joins/UTCOffsetWeekdayByUser/${userId}`).then(
    userUTCOffsetWeekday =>
      userUTCOffsetWeekday &&
      DB.update({
        joins: {
          usersByUTCOffsetWeekday: {
            [userUTCOffsetWeekday]: {
              [userId]: null,
            },
          },

          UTCOffsetWeekdayByUser: {
            [userId]: null,
          },
        },
      })
  )
}
