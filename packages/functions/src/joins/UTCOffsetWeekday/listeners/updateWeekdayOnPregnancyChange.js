/**
 *
 * updateWeekdayOnPregnancyChange
 *
 */

const functions = require('firebase-functions')

let lastListenerId

module.exports = functions.database
  .ref('/pregnancies/{pregnancyId}')
  .onWrite((change, context) => {
    if (
      // JOINS [UTCOffsetWeekday > 4.2] covered in profile "pregnancy" field listener
      !change.before.exists() ||
      // JOINS [UTCOffsetWeekday > 4.1] covered in profile "pregnancy" field listener
      !change.after.exists()
    ) {
      return null
    }

    const oldPregnancy = change.before.val()
    const newPregnancy = change.after.val()

    if (
      // JOINS [UTCOffsetWeekday > 1] update week day when user pregnancy due date change
      oldPregnancy.dueDate !== newPregnancy.dueDate ||
      // JOINS [UTCOffsetWeekday > 2] update week day when user pregnancy length type change
      oldPregnancy.lengthType !== newPregnancy.lengthType
    ) {
      const currentListenerId = Date.now()
      lastListenerId = currentListenerId

      const getUTCOffsetWeekDay = require('src/utils/getUTCOffsetWeekDay')
      const getUserById = require('src/helpers/getUserById')
      const getUserUTCOffsetWeekday = require('src/helpers/getUserUTCOffsetWeekday')
      const merge = require('lodash/merge')
      const isEmpty = require('lodash/isEmpty')

      const FirebaseAdmin = require('src/app').FirebaseAdmin

      const pregnancyId = context.params.pregnancyId

      const initialState = {
        app: FirebaseAdmin,
        dueDate: newPregnancy.dueDate,
        lengthType: newPregnancy.lengthType,
        userId: newPregnancy.createdBy,
      }

      return getUserById({
        state: initialState,
        userId: initialState.userId,
      })
        .then(state => {
          if (state.user.pregnancy === pregnancyId) {
            return getUserUTCOffsetWeekday(state, state.userId)
          }

          return Promise.reject('User updated none active pregnancy')
        })
        .then(state => {
          if (state.user.partner) {
            const saveAs = 'partner'

            return getUserById({
              state,
              userId: state.user.partner,
              saveAs,
            })
          }

          return state
        })
        .then(state => {
          if (state.partner) {
            const saveAs = 'partnerCurrentUTCOffsetWeekday'

            return getUserUTCOffsetWeekday(state, state.partner.id, saveAs)
          }

          return state
        })
        .then(state => {
          const {
            userId,
            currentUTCOffsetWeekday,
            partnerCurrentUTCOffsetWeekday,
            partner,
          } = state

          const newUTCOffsetWeekday = getUTCOffsetWeekDay(
            state.dueDate,
            state.lengthType,
            state.user.UTCOffset
          )

          const databaseDiffObj = {}

          if (
            !currentUTCOffsetWeekday ||
            newUTCOffsetWeekday !== currentUTCOffsetWeekday
          ) {
            merge(databaseDiffObj, {
              joins: {
                usersByUTCOffsetWeekday: {
                  [newUTCOffsetWeekday]: {
                    [userId]: true,
                  },
                },

                UTCOffsetWeekdayByUser: {
                  [userId]: newUTCOffsetWeekday,
                },
              },
            })

            if (currentUTCOffsetWeekday) {
              merge(databaseDiffObj, {
                joins: {
                  usersByUTCOffsetWeekday: {
                    [currentUTCOffsetWeekday]: {
                      [userId]: null,
                    },
                  },
                },
              })
            }
          }

          if (partner) {
            const partnerNewUTCOffsetWeekday = getUTCOffsetWeekDay(
              state.dueDate,
              state.lengthType,
              state.partner.UTCOffset
            )

            if (
              !partnerCurrentUTCOffsetWeekday ||
              partnerNewUTCOffsetWeekday !== partnerCurrentUTCOffsetWeekday
            ) {
              merge(databaseDiffObj, {
                joins: {
                  usersByUTCOffsetWeekday: {
                    [partnerNewUTCOffsetWeekday]: {
                      [partner.id]: true,
                    },
                  },

                  UTCOffsetWeekdayByUser: {
                    [partner.id]: partnerNewUTCOffsetWeekday,
                  },
                },
              })

              if (partnerCurrentUTCOffsetWeekday) {
                merge(databaseDiffObj, {
                  joins: {
                    usersByUTCOffsetWeekday: {
                      [partnerCurrentUTCOffsetWeekday]: {
                        [partner.id]: null,
                      },
                    },
                  },
                })
              }
            }
          }

          if (
            !isEmpty(databaseDiffObj) &&
            lastListenerId === currentListenerId
          ) {
            return require('src/services/DB').update(databaseDiffObj)
          }
        })
    }

    return null
  })
