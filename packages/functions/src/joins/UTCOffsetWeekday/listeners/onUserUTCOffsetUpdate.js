/**
 *
 * onUserUTCOffsetUpdate
 *
 */

const functions = require('firebase-functions')

module.exports = functions.database
  .ref('/users/{userId}/UTCOffset')
  .onWrite((change, context) => {
    const userId = context.params.userId

    if (
      // JOINS [UTCOffsetWeekday > 3.2] user removes UTC zone
      !change.after.exists()
    ) {
      const removeUserFromUTCOffsetWeekdayJoin = require('../helpers/removeUserFromUTCOffsetWeekdayJoin')

      return removeUserFromUTCOffsetWeekdayJoin(userId)
    }

    const oldUTCOffset = change.before.val()
    const newUTCOffset = change.after.val()

    if (
      // JOINS [UTCOffsetWeekday > 3.1] user change UTC zone
      oldUTCOffset !== newUTCOffset
    ) {
      const FirebaseAdmin = require('src/app').FirebaseAdmin
      const getUserUTCOffsetWeekday = require('src/helpers/getUserUTCOffsetWeekday')
      const getUserById = require('src/helpers/getUserById')
      const getPregnancyById = require('src/helpers/getPregnancyById')
      const getUTCOffsetWeekDay = require('src/utils/getUTCOffsetWeekDay')
      const merge = require('lodash/merge')

      const initialState = {
        app: FirebaseAdmin,
        userId,
        newUTCOffset,
        oldUTCOffset,
      }

      return getUserUTCOffsetWeekday(initialState, userId)
        .then(state => {
          const {
            newUTCOffset,
            oldUTCOffset,
            currentUTCOffsetWeekday,
            userId,
          } = state

          if (currentUTCOffsetWeekday) {
            state.newUTCOffsetWeekday = currentUTCOffsetWeekday.replace(
              oldUTCOffset,
              newUTCOffset
            )

            return state
          } else {
            return getUserById({ state, userId })
              .then(
                state =>
                  state.user.pregnancy
                    ? getPregnancyById({
                        state,
                        pregnancyId: state.user.pregnancy,
                      })
                    : Promise.reject('User has no active pregnancy')
              )
              .then(state => {
                state.newUTCOffsetWeekday = getUTCOffsetWeekDay(
                  state.pregnancy.dueDate,
                  state.pregnancy.lengthType,
                  newUTCOffset
                )

                return state
              })
          }
        })
        .then(state => {
          const { newUTCOffsetWeekday, currentUTCOffsetWeekday, userId } = state

          if (
            currentUTCOffsetWeekday &&
            newUTCOffsetWeekday === currentUTCOffsetWeekday
          ) {
            return null
          }

          const databaseDiffObj = {
            joins: {
              usersByUTCOffsetWeekday: {
                [newUTCOffsetWeekday]: {
                  [userId]: true,
                },
              },

              UTCOffsetWeekdayByUser: {
                [userId]: newUTCOffsetWeekday,
              },
            },
          }

          if (currentUTCOffsetWeekday) {
            merge(databaseDiffObj, {
              joins: {
                usersByUTCOffsetWeekday: {
                  [currentUTCOffsetWeekday]: {
                    [userId]: null,
                  },
                },
              },
            })
          }

          return require('src/services/DB').update(databaseDiffObj)
        })
    }

    return null
  })
