/**
 *
 * onUserPregnancyUpdate
 *
 */

const functions = require('firebase-functions')

module.exports = functions.database
  .ref('/users/{userId}/pregnancy')
  .onWrite((change, context) => {
    const userId = context.params.userId

    if (
      // JOINS [UTCOffsetWeekday > 4.1, 4.3, 4.4, 4.6, 4.7, 4.8, 4.10]
      // pregnancy was removed
      // pregnancy was cancelled
      // pregnancy was ended with status child is borned
      // user is connected and mother removed her pregnancy
      // user is connected and mother cancelled her pregnancy
      // user is connected and mother ended pregnancy with status child is borned
      // user was removed
      change.before.exists() &&
      !change.after.exists()
    ) {
      const removeUserFromUTCOffsetWeekdayJoin = require('../helpers/removeUserFromUTCOffsetWeekdayJoin')

      return removeUserFromUTCOffsetWeekdayJoin(userId)
    }

    const oldPregnancyId = change.before.val()
    const newPregnancyId = change.after.val()

    if (
      // JOINS [UTCOffsetWeekday > 4.2, 4.5, 4.9]
      // pregnancy was created
      // user connected to partner and got mother pregnancy
      // user pregnancy has change due to disconnection from mother
      oldPregnancyId !== newPregnancyId
    ) {
      const FirebaseAdmin = require('src/app').FirebaseAdmin
      const getUTCOffsetWeekDay = require('src/utils/getUTCOffsetWeekDay')
      const getUserUTCOffsetWeekday = require('src/helpers/getUserUTCOffsetWeekday')
      const getUserById = require('src/helpers/getUserById')
      const getPregnancyById = require('src/helpers/getPregnancyById')
      const merge = require('lodash/merge')

      const initialState = {
        app: FirebaseAdmin,
        pregnancyId: newPregnancyId,
        userId,
      }

      return getUserById({ state: initialState, userId })
        .then(state =>
          getPregnancyById({
            state,
            pregnancyId: state.pregnancyId,
          })
        )
        .then(state => getUserUTCOffsetWeekday(state, state.userId))
        .then(state => {
          const {
            pregnancy: { lengthType, dueDate },
            user: { UTCOffset },
            userId,
            currentUTCOffsetWeekday,
          } = state

          const newUTCOffsetWeekday = getUTCOffsetWeekDay(
            dueDate,
            lengthType,
            UTCOffset
          )

          if (
            currentUTCOffsetWeekday &&
            newUTCOffsetWeekday === currentUTCOffsetWeekday
          ) {
            return null
          }

          const databaseDiffObj = {
            joins: {
              usersByUTCOffsetWeekday: {
                [newUTCOffsetWeekday]: {
                  [userId]: true,
                },
              },

              UTCOffsetWeekdayByUser: {
                [userId]: newUTCOffsetWeekday,
              },
            },
          }

          if (currentUTCOffsetWeekday) {
            merge(databaseDiffObj, {
              joins: {
                usersByUTCOffsetWeekday: {
                  [currentUTCOffsetWeekday]: {
                    [userId]: null,
                  },
                },
              },
            })
          }

          return require('src/services/DB').update(databaseDiffObj)
        })
    }

    return null
  })
