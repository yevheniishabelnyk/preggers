/**
 *
 * Delete object from Algolia Index
 *
 */

const app = require('src/app')

const Algolia = app.Algolia

module.exports = function(indexId, objectId) {
  const index = Algolia.initIndex(indexId)

  return new Promise((resolve, reject) =>
    index.deleteObject(objectId, err => {
      if (err) {
        console.info(`Error removing object ${objectId} from ${indexId} index`)

        reject(err)
      } else {
        console.info(`Object ${objectId} removed from ${indexId} index`)

        resolve()
      }
    })
  )
}
