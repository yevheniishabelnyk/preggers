/**
 *
 * Algolia indices list
 *
 */

const config = require('config')

const envSplitter = '__'

const envSuffix =
  config.database.environment === 'production'
    ? ''
    : envSplitter + config.database.environment

module.exports = {
  PREGGERS_KNOWLEDGE_ITEMS: config.APP__LANGUAGES.reduce((out, locale) => {
    out[locale] = `preggers-knowledge-items--${locale}${envSuffix}`

    return out
  }, {}),
}
