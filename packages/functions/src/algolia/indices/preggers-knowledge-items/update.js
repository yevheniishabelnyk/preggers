/**
 *
 * Update "preggers-knowledge-items" Algolia Index
 *
 */

const app = require('src/app')
const getArticles = require('src/helpers/getArticles')
const getTags = require('src/helpers/getTags')

const FirebaseAdmin = app.FirebaseAdmin
const Algolia = app.Algolia
const AlgoliIndices = require('src/algolia/indices/list')
const KnowledgeItemTypes = require('src/constants/KnowledgeItemTypes')
const config = require('config')

const keys = require('lodash/keys')
const get = require('lodash/get')
const difference = require('lodash/difference')
const compact = require('lodash/compact')

module.exports = function(localeToUpdate) {
  console.info('[ALGOLIA][UPDATE-PREGGERS-KNOWLEDGE-ITEMS-INDEX]')

  const initialState = {
    app: FirebaseAdmin,
  }

  return getArticles(initialState)
    .then(getTags)
    .then(buildIndex)
    .then(data => updateIndices(data, localeToUpdate))
    .catch(err => {
      console.info(
        '[ALGOLIA][UPDATE-PREGGERS-KNOWLEDGE-ITEMS-INDEX] Error: ',
        err
      )
      return Promise.reject(err)
    })
}

function updateIndices(data, localeToUpdate) {
  console.info('[ALGOLIA][UPDATE-PREGGERS-KNOWLEDGE-ITEMS-INDEX] updateIndices')

  let locales = localeToUpdate ? [localeToUpdate] : config.APP__LANGUAGES

  return Promise.all([
    locales.map(locale => {
      const index = Algolia.initIndex(
        AlgoliIndices.PREGGERS_KNOWLEDGE_ITEMS[locale]
      )

      return index
        .search({
          query: '',
        })
        .then(result => {
          const newData = data.filter(item => item.language === locale)

          const dataToUpdate = findRecordsToUpdate(result.hits, newData)

          return dataToUpdate
        })
        .catch(err => {
          console.info('err: ', err)

          return Promise.resolve([])
        })
        .then((data = []) => {
          if (data.length) {
            return index.saveObjects(data)
          }

          console.info(
            '[ALGOLIA][UPDATE-PREGGERS-KNOWLEDGE-ITEMS-INDEX] no data to update: ',
            AlgoliIndices.PREGGERS_KNOWLEDGE_ITEMS[locale]
          )

          return Promise.resolve()
        })
    }),
  ])
}

function findRecordsToUpdate(oldData = [], newData) {
  return newData.filter(newRecord => {
    const oldRecord = oldData.find(item => item.id === newRecord.id)

    if (
      !oldRecord ||
      oldRecord.updatedAt !== newRecord.updatedAt ||
      oldRecord.stars !== newRecord.stars
    ) {
      return true
    }

    const oldTags = oldRecord._tags || []
    const newTags = newRecord._tags || []

    if (difference(oldTags, newTags).length) {
      return true
    }

    return false
  })
}

function buildIndex({ articles, tags }) {
  if (!articles || !tags) {
    return Promise.reject({ message: 'Missing required params!' })
  }

  return keys(articles).map(articleId => {
    const article = articles[articleId]

    return {
      objectID: articleId,
      id: articleId,
      title: article.title,
      text: article.text,
      language: article.language,
      type: KnowledgeItemTypes.ARTICLE,
      writtenBy: article.writtenBy,
      image: article.image,
      sponsoredBy: article.sponsoredBy,
      stars: article.stars && article.stars > 0 ? article.stars : 0,
      _tags: compact(
        keys(article.tags).map(tagId =>
          get(tags, `${tagId}.name.${article.language}`)
        )
      ),
      createdAt: article.createdAt,
      updatedAt: article.updatedAt,
    }
  })
}
