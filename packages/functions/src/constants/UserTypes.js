/**
 *
 *  User Types
 *
 */

module.exports = {
  MOTHER: 'mother',
  SINGLE_MOTHER: 'single_mother',
  FATHER: 'father',
  PARTNER: 'partner',
  GRANDPARENT: 'grandparent',
  UNCLE: 'uncle',
  ANT: 'ant',
  FRIEND: 'friend',
  FAMILY: 'family',
}
