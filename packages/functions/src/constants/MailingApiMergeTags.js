module.exports = [
  'IS_NEW_USER',
  'IS_NEW_WEEK',
  'IS_MOTHER',
  'USER_NAME',
  'WEEK_NUMBER',
  'HAS_PARTNER',
  'PARTNER_NAME',
  'HAS_CHILD_NAME',
  'CHILD_NAME',
]
