// Knowledge Item types

module.exports = {
  ARTICLE: 'article',
  VIDEO: 'video',
  PODCAST: 'podcast',
}
