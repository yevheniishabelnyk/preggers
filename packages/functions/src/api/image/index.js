const functions = require('firebase-functions')
const cors = require('cors')
const express = require('express')
const formDataParser = require('src/middlewares/formDataParser')
const validateFirebaseIdToken = require('src/middlewares/validateFirebaseIdToken')
const upload = require('./upload')

const app = express()

app.use(cors({ origin: true }))
app.use(validateFirebaseIdToken)

formDataParser('/upload', app)

app.post('/upload', function(req, response, next) {
  upload(req.files.file[0])
    .then(metadata => {
      response.status(200).json(metadata[0])
      next()
    })
    .catch(error => {
      console.log(error)
      response.status(500).json({ error })
      next()
    })
})

module.exports = functions.https.onRequest(app)
