const FirebaseAdmin = require('src/app').FirebaseAdmin

module.exports = file => {
  const storage = FirebaseAdmin.storage()

  return new Promise((resolve, reject) => {
    const fileUpload = storage
      .bucket()
      .file(`images/users/${file.originalname}`)

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: 'image/jpg',
      },
    })

    blobStream.on('error', error => reject(error))

    blobStream.on('finish', () => {
      fileUpload
        .getMetadata()
        .then(metadata => resolve(metadata))
        .catch(error => reject(error))
    })

    blobStream.end(file.buffer)
  })
}
