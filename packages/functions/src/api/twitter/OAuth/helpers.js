var Crypto = require('crypto-js')

/**
 * Parse a form encoded string into an object
 * @param  {string} formEncoded Form encoded string
 * @return {Object}             Decoded data object
 */
exports.parseFormEncoding = formEncoded => {
  const pairs = formEncoded.split('&')
  const result = {}
  for (const pair of pairs) {
    const [key, value] = pair.split('=')
    result[key] = value
  }
  return result
}

/**
 * Creates the Token Request OAuth header
 * @param  {Object} params OAuth params
 * @return {string}        OAuth header string
 */
exports.createHeaderString = params => {
  return (
    'OAuth ' +
    Object.keys(params)
      .map(key => {
        const encodedKey = encodeURIComponent(key)
        const encodedValue = encodeURIComponent(params[key])
        return `${encodedKey}="${encodedValue}"`
      })
      .join(', ')
  )
}

/**
 * Creates the Signature for the OAuth header
 * @param  {Object}  params         OAuth + Request Parameters
 * @param  {string}  HTTPMethod     Type of Method (POST,GET...)
 * @param  {string}  requestURL     Full Request URL
 * @param  {string}  consumerSecret Twitter Consumer Secret
 * @param  {?string} tokenSecret    Secret token (Optional)
 * @return {string}                 Returns the encoded/hashed signature
 */
exports.createSignature = (
  params,
  httpMethod,
  requestURL,
  consumerSecret,
  tokenSecret
) => {
  const encodedParameters = percentEncodeParameters(params)
  const upperCaseHTTPMethod = httpMethod.toUpperCase()
  const encodedRequestURL = encodeURIComponent(requestURL)
  const encodedConsumerSecret = encodeURIComponent(consumerSecret)

  const signatureBaseString =
    upperCaseHTTPMethod +
    '&' +
    encodedRequestURL +
    '&' +
    encodeURIComponent(encodedParameters)

  let signingKey
  if (tokenSecret !== undefined) {
    signingKey = encodedRequestURL + '&' + encodeURIComponent(tokenSecret)
  } else {
    signingKey = encodedConsumerSecret + '&'
  }
  const signature = Crypto.HmacSHA1(signatureBaseString, signingKey)
  const encodedSignature = Crypto.enc.Base64.stringify(signature)
  return encodedSignature
}

/**
 * Percent encode the OAUTH Header + Request parameters for signature
 * @param  {Object} params Dictionary of params
 * @return {string}        Percent encoded parameters string
 */
function percentEncodeParameters(params) {
  return Object.keys(params)
    .map(key => {
      const encodedKey = encodeURIComponent(key)
      const encodedValue = encodeURIComponent(params[key])
      return `${encodedKey}=${encodedValue}`
    })
    .join('&')
}

/**
 * Creates the header with the base parameters (Date, nonce etc...)
 * @return {Object} returns a header dictionary with base fields filled.
 */
exports.createHeaderBase = () => {
  return {
    oauth_consumer_key: '',
    oauth_nonce: createNonce(),
    oauth_signature_method: 'HMAC-SHA1',
    oauth_timestamp: new Date().getTime() / 1000,
    oauth_version: '1.0',
  }
}

/**
 * Creates a nonce for OAuth header
 * @return {string} nonce
 */
function createNonce() {
  let text = ''
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  for (let i = 0; i < 32; i += 1) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return text
}
