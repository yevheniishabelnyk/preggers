var request = require('request')
var helpers = require('./helpers')

// URL + Routes
const requestTokenURL = '/oauth/request_token'
const authorizationURL = '/oauth/authorize'
const accessURL = '/oauth/access_token'
const baseURL = 'https://api.twitter.com'

// Callback URL of your application, change if standalone. Otherwise this is the one for in Exponent apps.
exports.getRedirectUrl = (
  callbackURL,
  twitterConsumerKey,
  twitterConsumerSecret
) =>
  new Promise((resolve, reject) => {
    // get callback url the query
    // Request Token
    // Creates base header + Request URL
    const tokenRequestHeaderParams = helpers.createHeaderBase()
    const tokenRequestURL = baseURL + requestTokenURL
    // Add additional parameters for signature + Consumer Key
    tokenRequestHeaderParams.oauth_consumer_key = twitterConsumerKey

    // Creates copy to add additional request params, to then create the signature
    const callBackParam = { oauth_callback: callbackURL }
    const parametersForSignature = Object.assign(
      {},
      callBackParam,
      tokenRequestHeaderParams
    )

    const signature = helpers.createSignature(
      parametersForSignature,
      'POST',
      tokenRequestURL,
      twitterConsumerSecret
    )

    tokenRequestHeaderParams.oauth_signature = signature

    // Creates the Header String, adds the callback parameter
    const headerString = helpers.createHeaderString(tokenRequestHeaderParams)
    const callbackKeyValue =
      ', oauth_callback="' + encodeURIComponent(callbackURL) + '"'
    const tokenRequestHeader = headerString + callbackKeyValue

    // Request
    request(
      {
        method: 'POST',
        uri: tokenRequestURL,
        headers: { Authorization: tokenRequestHeader },
      },
      function(error, response, body) {
        if (!error) {
          const tokenResponse = helpers.parseFormEncoding(body)
          const authToken = tokenResponse.oauth_token
          const authTokenSecret = tokenResponse.oauth_token_secret
          // Token Authorization, send the URL to the native app to then display in 'Webview'
          const authURL =
            baseURL + authorizationURL + '?oauth_token=' + authToken

          resolve({
            redirectURL: authURL,
            token: authToken,
            secretToken: authTokenSecret,
          })
        } else {
          reject(error)
        }
      }
    )
  })

exports.getAccessToken = (
  oauthVerifier,
  oauthToken,
  oauthSecretToken,
  twitterConsumerKey,
  twitterConsumerSecret
) =>
  new Promise((resolve, reject) => {
    // Creates base header + Access Token URL
    const accessTokenHeaderParams = helpers.createHeaderBase()
    const accessTokenURL = baseURL + accessURL

    // Add additional parameters for signature + Consumer Key
    accessTokenHeaderParams.oauth_consumer_key = twitterConsumerKey
    accessTokenHeaderParams.oauth_token = oauthToken
    accessTokenHeaderParams.oauth_token_secret = oauthSecretToken

    const accessTokenSignature = helpers.createSignature(
      accessTokenHeaderParams,
      'POST',
      accessTokenURL,
      twitterConsumerSecret
    )
    accessTokenHeaderParams.oauth_signature = accessTokenSignature

    // Creates the Header String, adds the oauth verfier
    const accessTokenHeaderString = helpers.createHeaderString(
      accessTokenHeaderParams
    )
    const verifierKeyValue =
      ', oauth_verifier="' + encodeURIComponent(oauthVerifier) + '"'
    const accessTokenRequestHeader = accessTokenHeaderString + verifierKeyValue

    // Convert token to Access Token
    request(
      {
        method: 'POST',
        uri: accessTokenURL,
        headers: { Authorization: accessTokenRequestHeader },
      },
      function(error, response, body) {
        if (!error) {
          const accessTokenResponse = helpers.parseFormEncoding(body)

          resolve(accessTokenResponse)
        } else {
          reject(error)
        }
      }
    )
  })
