/**
 *
 * postTweet
 *
 */

'use strict'

const Twitter = require('twitter')
const config = require('config')

module.exports = function(req) {
  const { accessTokenKey, accessTokenSecret, status } = req.data

  const file = req.file

  const consumerKey = config.twitter.consumer_key
  const consumerSecret = config.twitter.consumer_secret

  if (accessTokenKey && accessTokenSecret && status && file) {
    var client = new Twitter({
      consumer_key: consumerKey,
      consumer_secret: consumerSecret,
      access_token_key: accessTokenKey,
      access_token_secret: accessTokenSecret,
    })

    return client
      .post('media/upload', { media: file.buffer })
      .then(media => {
        const data = {
          status,
          media_ids: media.media_id_string,
        }

        return client.post('statuses/update', data)
      })
      .then(tweet => ({ tweet }))
  } else {
    return Promise.reject({ code: 401, message: 'Missing required params!' })
  }
}
