/**
 *
 * getAccessToken
 *
 */

'use strict'

const OAuth = require('./OAuth')
const config = require('config')

module.exports = function(req) {
  const { oauthVerifier, oauthToken, oauthSecretToken } = req.body

  const consumerKey = config.twitter.consumer_key
  const consumerSecret = config.twitter.consumer_secret

  if (oauthVerifier && oauthToken && oauthSecretToken) {
    return OAuth.getAccessToken(
      oauthVerifier,
      oauthToken,
      oauthSecretToken,
      consumerKey,
      consumerSecret
    )
  } else {
    return Promise.reject({ code: 401, message: 'Missing callback url!' })
  }
}
