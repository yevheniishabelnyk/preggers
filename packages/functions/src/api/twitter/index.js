const functions = require('firebase-functions')
const express = require('express')

const getAccessToken = require('./get-access-token')
const getAuthUrl = require('./get-auth-url')
const postTweet = require('./post-tweet')

// Middlewares
const validateFirebaseIdToken = require('src/middlewares/validateFirebaseIdToken')
const cors = require('cors')
const bodyParser = require('body-parser')
const Busboy = require('busboy')
const getRawBody = require('raw-body')
const contentType = require('content-type')

const app = express()

app.post('/get-access-token', function(req, res, next) {
  getAccessToken(req)
    .then(responseBody => {
      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.log(error)
      const code = error.code || 500

      res.status(code).json({ error })
      next()
    })
})

app.post('/get-auth-url', function(req, res, next) {
  getAuthUrl(req)
    .then(responseBody => {
      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.log(error)
      const code = error.code || 500

      res.status(code).json({ error })
      next()
    })
})

app.post(
  '/post-tweet',
  [
    validateFirebaseIdToken,

    cors({ origin: true }),

    bodyParser.json(),

    bodyParser.urlencoded({
      extended: true,
    }),

    (req, res, next) => {
      if (
        req.rawBody === undefined &&
        req.method === 'POST' &&
        req.headers['content-type'].startsWith('multipart/form-data')
      ) {
        getRawBody(
          req,
          {
            length: req.headers['content-length'],
            limit: '10mb',
            encoding: contentType.parse(req).parameters.charset,
          },
          function(err, string) {
            if (err) return next(err)
            req.rawBody = string
            next()
          }
        )
      } else {
        next()
      }
    },

    (req, res, next) => {
      if (
        req.method === 'POST' &&
        req.headers['content-type'].startsWith('multipart/form-data')
      ) {
        const busboy = new Busboy({
          headers: req.headers,
        })

        var fileBuffer = new Buffer('')

        busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
          file.on('data', data => {
            fileBuffer = Buffer.concat([fileBuffer, data])
          })

          file.on('end', () => {
            const file_object = {
              fieldname,
              originalname: filename,
              encoding,
              mimetype,
              buffer: fileBuffer,
            }

            req.file = file_object
          })
        })

        req.data = {}

        busboy.on('field', function(
          fieldname,
          val
          // fieldnameTruncated
          // valTruncated
          // encoding
          // mimetype
        ) {
          req.data[fieldname] = val
        })

        busboy.on('finish', function() {
          console.log('Done parsing form!')

          next()
        })

        busboy.end(req.rawBody)
      } else {
        next()
      }
    },
  ],
  function(req, res, next) {
    postTweet(req)
      .then(responseBody => {
        res.status(200).json(responseBody)
        next()
      })
      .catch(error => {
        console.log(error)
        const code = error.code || 500

        res.status(code).json({ error })
        next()
      })
  }
)

module.exports = functions.https.onRequest(app)
