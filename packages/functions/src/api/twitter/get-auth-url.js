/**
 *
 * getAuthUrl
 *
 */

'use strict'

const OAuth = require('./OAuth')
const config = require('config')

module.exports = function(req) {
  const { callbackUrl } = req.body

  const consumerKey = config.twitter.consumer_key
  const consumerSecret = config.twitter.consumer_secret

  if (callbackUrl) {
    return OAuth.getRedirectUrl(callbackUrl, consumerKey, consumerSecret)
  } else {
    return Promise.reject({ code: 401, message: 'Missing callback url!' })
  }
}
