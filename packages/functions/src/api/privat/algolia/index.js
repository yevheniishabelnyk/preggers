/**
 *
 * Algolia
 *
 */

const functions = require('firebase-functions')
const cors = require('cors')
const express = require('express')
const validateFirebaseIdToken = require('src/middlewares/validateFirebaseIdToken')
const validateAdminRights = require('src/middlewares/validateAdminRights')

const updateIndex = require('./update-index')

const app = express()

app.use(cors())
app.use(validateFirebaseIdToken)
app.use(validateAdminRights)

app.post('/update-index', function(req, res, next) {
  updateIndex(req)
    .then(responseBody => {
      console.info('[ALGOLIA][UPDATE-INDEX] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[ALGOLIA][UPDATE-INDEX] Error: ', error)

      const status = error.status || 500

      res.status(status).json({ code: error.code, message: error.message })

      next()
    })
})

module.exports = functions.https.onRequest((req, res) => {
  if (!req.path) {
    // https://github.com/firebase/firebase-functions/issues/27

    req.url = `/${req.url}`
  }

  return app(req, res)
})
