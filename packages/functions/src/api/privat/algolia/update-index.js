/**
 *
 * Update Algolia Index
 *
 */

'use strict'

const updatePreggersKnowledgeItems = require('src/algolia/indices/preggers-knowledge-items/update')

const envSplitter = '__'

module.exports = function(req) {
  const { indexId, locale } = req.body

  console.info('[ALGOLIA API][UPDATE-INDEX] Index: ', indexId)

  const id = indexId.split(envSplitter)[0]

  if (id.startsWith('preggers-knowledge-items')) {
    return updatePreggersKnowledgeItems(locale)
  }

  return Promise.reject({
    status: 400,
    code: 'algolia/wrong-index-id',
    message: `Index with ID "${indexId}" is missing in Algolia indices!`,
  })
}
