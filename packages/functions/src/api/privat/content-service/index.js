/**
 *
 * Algolia
 *
 */

const functions = require('firebase-functions')
const cors = require('cors')
const express = require('express')

const fetchChecklistItems = require('./fetch-checklist-items')
const fetchDealCategories = require('./fetch-deal-categories')
const fetchTags = require('./fetch-tags')

const app = express()
const validateContentServiceToken = require('src/middlewares/validateContentServiceToken')

app.use(cors({ origin: true }))
app.use(validateContentServiceToken)

app.post('/fetch-checklist-items', function(req, res, next) {
  fetchChecklistItems(req)
    .then(responseBody => {
      console.info('[SCS][FETCH-CHECKLIST-ITEMS] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[SCS][FETCH-CHECKLIST-ITEMS] Error: ', error)

      const status = error.status || 500

      res.status(status).json({ code: error.code, message: error.message })

      next()
    })
})

app.post('/fetch-deal-categories', function(req, res, next) {
  fetchDealCategories(req)
    .then(responseBody => {
      console.info('[SCS][GET DEAL CATECORIES] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[SCS][GET DEAL CATECORIES] Error: ', error)

      const status = error.status || 500

      res.status(status).json({ code: error.code, message: error.message })

      next()
    })
})

app.post('/fetch-tags', function(req, res, next) {
  fetchTags(req)
    .then(responseBody => {
      console.info('[SCS][GET TAGS] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[SCS][GET TAGS] Error: ', error)

      const status = error.status || 500

      res.status(status).json({ code: error.code, message: error.message })

      next()
    })
})

module.exports = functions.https.onRequest(app)
