/**
 *
 * Fetch deal categories for stroller content service
 *
 */

'use strict'

const FirebaseAdmin = require('src/app').FirebaseAdmin

const getDealCategories = require('src/helpers/getDealCategories')

module.exports = function() {
  console.info('[SCS][GET DEAL CATECORIES]')

  const initialState = { app: FirebaseAdmin }

  return getDealCategories(initialState).then(state => {
    console.log('[SCS][GET DEAL CATECORIES]', state.dealCatagories)

    return state.dealCatagories
  })
}
