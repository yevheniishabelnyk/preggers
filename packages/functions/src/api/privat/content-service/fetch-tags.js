/**
 *
 * Fetch deal categories for stroller content service
 *
 */

'use strict'

const FirebaseAdmin = require('src/app').FirebaseAdmin

const getTags = require('src/helpers/getTags')

module.exports = function() {
  console.info('[SCS][GET TAGS]')

  const initialState = { app: FirebaseAdmin }

  return getTags(initialState).then(state => {
    console.log('[SCS][GET TAGS]', state.tags)

    return state.tags
  })
}
