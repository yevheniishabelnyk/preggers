/**
 *
 * Fetch checklist items for stroller content service
 *
 */

'use strict'

const FirebaseAdmin = require('src/app').FirebaseAdmin

const getChecklistItems = require('src/helpers/getChecklistItems')

module.exports = function() {
  console.info('[SCS][FETCH-CHECKLIST-ITEMS]')

  const initialState = { app: FirebaseAdmin }

  return getChecklistItems(initialState).then(state => {
    console.log('[SCS][FETCH-CHECKLIST-ITEMS]', state.checklistItems)

    return state.checklistItems
  })
}
