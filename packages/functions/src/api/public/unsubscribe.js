/**
 *
 * send new user welcome email
 *
 */

'use strict'

const FirebaseAdmin = require('src/app').FirebaseAdmin

module.exports = function(req) {
  console.info('[PUBLIC][UNSUBSCRIBE] email', req.query.email)

  return FirebaseAdmin.database()
    .ref('/users')
    .orderByChild('email')
    .equalTo(req.query.email)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const userId = Object.keys(snap.val())[0]

        return FirebaseAdmin.database()
          .ref(`/userOwned/settings/${userId}/isWeeklyEmailsEnabled`)
          .set(false)
      }

      return Promise.reject()
    })
}
