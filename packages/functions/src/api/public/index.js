const functions = require('firebase-functions')
const express = require('express')

const unsubscribe = require('./unsubscribe')

const app = express()

const router = express()

router.get('/unsubscribe', function(req, res, next) {
  const template = content => `
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style>
    .container {
      background-color: #fff;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      -webkit-box-sizing: border-box;
      color: rgba(0,0,0,.87);
      direction: ltr;
      font: 16px Roboto,arial,sans-serif;
      margin: 0 auto;
      max-width: 360px;
      overflow: visible;
      position: relative;
      text-align: left;
      width: 100%;
      box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
      display: flex;
      flex-direction: column;
      min-height: 200px;
      z-index: 1;
      background: #fff;
      border-radius: 2px;
    }

    .header {
      padding: 24px 24px 0 24px;
      display: block;
      color: rgba(0,0,0,.87);
      direction: ltr;
      font: 16px Roboto,arial,sans-serif;
      text-align: left;
    }

    .title {
      color: rgba(0,0,0,.87);
      direction: ltr;
      font-size: 20px;
      font-weight: 500;
      line-height: 24px;
      margin: 0;
      padding: 0;
      text-align: left;
      padding-bottom: 20px;
    }

    .content {
      padding: 0 24px;
      color: rgba(0,0,0,.87);
      direction: ltr;
      font: 16px Roboto,arial,sans-serif;
    }

    .message {
      color: rgba(0,0,0,.87);
      direction: ltr;
      font-size: 16px;
      line-height: 24px;
      text-align: left;
      font: 16px Roboto,arial,sans-serif;
    }
  </style>
</head>
<body>
  <div class="container">
    <div class="header">
      <h1 class="title">${content.title}</h1>
    </div>

    <div class="content">
      <p class="message">${content.message}</p>
    </div>
  </div>
</body>
</html>
`
  unsubscribe(req)
    .then(responseBody => {
      console.info('[PUBLIC][UNSUBSCRIBE] Success!', responseBody)

      const title =
        'You will no longer receive the weekly e-mails from Preggers'
      const message =
        'If you wish to receive the weekly e-mails again, you can always turn them back on from the settings in your app.'

      res.status(200).send(template({ title, message }))

      next()
    })
    .catch(error => {
      console.error('[PUBLIC][UNSUBSCRIBE] Error: ', error)

      const title = 'Oops, something went wrong!'
      const message =
        'You can always turn off the weekly e-mails from your settings in your app. Your profile > App settings > Weekly e-mails \n\nStill doesn’t work? Please contact us at hello@preggersapp.com'

      res.status(500).send(template({ title, message }))

      next()
    })
})

app.use('/api', router)

module.exports = functions.https.onRequest(app)
