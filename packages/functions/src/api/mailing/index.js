const functions = require('firebase-functions')
const cors = require('cors')
const express = require('express')
const validateFirebaseIdToken = require('src/middlewares/validateFirebaseIdToken')
const validateAdminRights = require('src/middlewares/validateAdminRights')

const newUser = require('./new-user')
const newWeek = require('./new-week')

const app = express()

app.use(cors({ origin: true }))
app.use(validateFirebaseIdToken)

app.post('/new-user', function(req, res, next) {
  newUser(req)
    .then(responseBody => {
      console.info('[MAILING][NEW-USER] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[MAILING][NEW-USER] Error: ', error)

      const status = error.status || 500

      res.status(status).json({ code: error.code, message: error.message })

      next()
    })
})

app.post('/new-week', validateAdminRights, function(req, res, next) {
  newWeek(req)
    .then(responseBody => {
      console.info('[MAILING][NEW-WEEK] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[MAILING][NEW-WEEK] Error: ', error)

      const status = error.status || 500

      res.status(status).json({ code: error.code, message: error.message })

      next()
    })
})

module.exports = functions.https.onRequest(app)
