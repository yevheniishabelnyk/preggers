/**
 *
 * send new user welcome email
 *
 */

'use strict'

const sendWelcomeEmail = require('src/mailing/sendWelcomeEmail')
const FirebaseAdmin = require('src/app').FirebaseAdmin
const getUserByEmail = require('src/helpers/getUserByEmail')
const getUserById = require('src/helpers/getUserById')

const config = require('config')

module.exports = function(req) {
  if (!config.MAILING__IS_ENABLED) {
    return Promise.reject('Mailing is desabled with config.')
  }

  const userId = req.user.uid
  const { email, week } = req.body

  const initialState = {
    app: FirebaseAdmin,
    pregnancyWeek: week,
    cache: {},
  }

  if (email) {
    console.info('[MAILING][NEW-USER] user email', email)

    return getUserByEmail({ state: initialState, email }).then(getUserCallback)
  }

  console.info('[MAILING][NEW-USER] user ID', userId)

  return getUserById({ state: initialState, userId }).then(getUserCallback)
}

function getUserCallback(state) {
  return sendWelcomeEmail({
    userId: state.user.id,
    isNewUser: true,
    pregnancyWeek: state.pregnancyWeek,
    cache: state.cache,
  })
}
