/**
 *
 * declineInvitation
 *
 */

'use strict'

const flatten = require('src/utils/flatten')
const FirebaseAdmin = require('src/app').FirebaseAdmin
const ServerTimestamp = require('src/models/shared/ServerTimestamp')
const getInvitationById = require('src/helpers/getInvitationById')

module.exports = function(req) {
  const { invitationId } = req.body

  if (invitationId) {
    console.info('[CONNECT][DECLINE-INVITATION] invitationId: ', invitationId)

    const initialState = { app: FirebaseAdmin, requestUser: req.user }

    return getInvitationById(initialState, invitationId)
      .then(validateRequest)
      .then(updateDatabase)
      .then(() => {
        return {}
      })
  } else {
    return Promise.reject({ status: 400, message: 'Invitation ID required' })
  }
}

function validateRequest(state) {
  console.info('[CONNECT][DECLINE-INVITATION] validateRequest')

  if (state.invitation.toId === state.requestUser.uid) {
    return Promise.resolve(state)
  } else {
    return Promise.reject({ status: 400, message: 'Permission denied' })
  }
}

function updateDatabase(state) {
  console.info('[CONNECT][DECLINE-INVITATION] updateDatabase')

  const databaseDiffObj = {
    invitations: {
      [state.invitation.id]: {
        status: 'declined',
        updatedAt: ServerTimestamp,
      },
    },

    invitationCodes: {
      [state.invitation.id]: null,
    },

    users: {
      [state.invitation.fromId]: {
        invitation: null,
      },
    },

    userOwned: {
      invitations: {
        [state.invitation.toId]: {
          [state.invitation.id]: 'declined',
        },
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  console.info(
    '[CONNECT][DECLINE-INVITATION] diff: ',
    JSON.stringify(databaseDiff, null, 4)
  )

  return state.app
    .database()
    .ref()
    .update(databaseDiff)
    .then(() => state)
}
