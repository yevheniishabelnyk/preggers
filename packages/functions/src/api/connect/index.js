const functions = require('firebase-functions')
const cors = require('cors')
const express = require('express')
const validateFirebaseIdToken = require('src/middlewares/validateFirebaseIdToken')

const acceptInvitation = require('./accept-invitation')
const cancelConnection = require('./cancel-connection')
const declineInvitation = require('./decline-invitation')
const sendInvitation = require('./send-invitation')
const cancelInvitation = require('./cancel-invitation')

const app = express()

app.use(cors({ origin: true }))
app.use(validateFirebaseIdToken)

app.post('/accept-invitation', function(req, res, next) {
  acceptInvitation(req)
    .then(responseBody => {
      console.info('[CONNECT][ACCEPT-INVITATION] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[CONNECT][ACCEPT-INVITATION] Error: ', error)

      const status = error.status || 500

      res.status(status).json({
        code: error.code,
        message: error.message,
        payload: error.payload || null,
      })

      next()
    })
})

app.post('/cancel-connection', function(req, res, next) {
  cancelConnection(req)
    .then(responseBody => {
      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.log(error)
      const code = error.status || 500

      res.status(code).json({ code: error.code, message: error.message })
      next()
    })
})

app.post('/decline-invitation', function(req, res, next) {
  declineInvitation(req)
    .then(responseBody => {
      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.log(error)
      const code = error.status || 500

      res.status(code).json({ code: error.code, message: error.message })
      next()
    })
})

app.post('/send-invitation', function(req, res, next) {
  sendInvitation(req)
    .then(responseBody => {
      console.info('[CONNECT][SEND-INVITATION] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[CONNECT][SEND-INVITATION] Error: ', error)

      const code = error.status || 500

      res.status(code).json({ code: error.code, message: error.message })
      next()
    })
})

app.post('/cancel-invitation', function(req, res, next) {
  cancelInvitation(req)
    .then(responseBody => {
      console.info('[CONNECT][CANCEL-INVITATION] Success!', responseBody)

      res.status(200).json(responseBody)
      next()
    })
    .catch(error => {
      console.error('[CONNECT][CANCEL-INVITATION] Error: ', error)

      const code = error.status || 500

      res.status(code).json({ code: error.code, message: error.message })
      next()
    })
})

module.exports = functions.https.onRequest(app)
