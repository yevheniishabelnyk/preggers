/**
 *
 * cancelConnection
 *
 */

'use strict'

const flatten = require('src/utils/flatten')

const sendPushNotifications = require('src/utils/Expo/sendPushNotifications')

const FirebaseAdmin = require('src/app').FirebaseAdmin
const ServerTimestamp = require('src/models/shared/ServerTimestamp')

const getConnectionById = require('src/helpers/getConnectionById')
const getUserById = require('src/helpers/getUserById')
const getUserExpoPushTokens = require('src/helpers/getUserExpoPushTokens')

module.exports = function(req) {
  const { connectionId } = req.body

  console.info('[CONNECT][CANCEL-CONNECTION] connectionId: ', connectionId)

  if (connectionId) {
    const initialState = { app: FirebaseAdmin, requestUser: req.user }

    return getConnectionById(initialState, connectionId)
      .then(getToUser)
      .then(getFromUser)
      .then(validateRequest)
      .then(getNotificationObject)
      .then(updateDatabase)
      .then(sendPushNotification)
      .then(() => {
        return {}
      })
  } else {
    return Promise.reject({ status: 400, message: 'Connection ID required' })
  }
}

function getNotificationObject(state) {
  console.info('[CONNECT][CANCEL-CONNECTION] getNotificationObject')

  const { toUser, fromUser } = state

  if (toUser && fromUser) {
    const notificationReceiver =
      state.connection.fromId === state.requestUser.uid ? toUser : fromUser

    const connectionCanceller =
      notificationReceiver === toUser ? fromUser : toUser

    const notificationId = state.app
      .database()
      .ref(`userOwned/notifications/${notificationReceiver.id}`)
      .push().key

    const notificationObject = {
      id: notificationId,
      notificationType: 'connection-cancelled',
      status: 'unread',
      payload: {
        cancellerName: connectionCanceller.name,
      },
      updatedAt: ServerTimestamp,
      createdAt: ServerTimestamp,
    }

    console.info('[CONNECT][CANCEL-CONNECTION]', notificationObject)

    return Object.assign(state, {
      connectionCanceller,
      notificationReceiver,
      notificationObject,
    })
  }

  return state
}

function validateRequest(state) {
  console.info('[CONNECT][CANCEL-CONNECTION] validateRequest')

  if (
    state.connection.fromId === state.requestUser.uid ||
    state.connection.toId === state.requestUser.uid
  ) {
    return Promise.resolve(state)
  } else {
    return Promise.reject({ status: 400, message: 'Permission denied' })
  }
}

function updateDatabase(state) {
  console.info('[CONNECT][CANCEL-CONNECTION] updateDatabase')

  const databaseDiffObj = {
    connections: {
      [state.connection.id]: {
        status: 'canceled',
        updatedAt: ServerTimestamp,
      },
    },

    users: {
      [state.connection.fromId]: {
        connection: null,
        partner: null,
        updatedAt: ServerTimestamp,
      },

      [state.connection.toId]: {
        connection: null,
        partner: null,
        pregnancy: state.toUser.inactivePregnancy || null,
        updatedAt: ServerTimestamp,
      },
    },
  }

  if (state.notificationReceiver && state.notificationObject) {
    Object.assign(databaseDiffObj, {
      userOwned: {
        notifications: {
          [state.notificationReceiver.id]: {
            [state.notificationObject.id]: state.notificationObject,
          },
        },
      },
    })
  }

  if (state.fromUser.pregnancy) {
    Object.assign(databaseDiffObj, {
      usersByPregnancy: {
        [state.fromUser.pregnancy]: {
          [state.toUser.id]: null,
        },
      },

      pregnanciesByUser: {
        [state.toUser.id]: {
          [state.fromUser.pregnancy]: null,
        },
      },
    })
  }

  if (state.toUser.inactivePregnancy) {
    Object.assign(databaseDiffObj, {
      pregnancies: {
        [state.toUser.inactivePregnancy]: {
          status: 'active',
        },
      },
    })
  }

  const databaseDiff = flatten(databaseDiffObj)

  console.info(
    '[CONNECT][CANCEL-CONNECTION] diff: ',
    JSON.stringify(databaseDiff, null, 4)
  )

  return state.app
    .database()
    .ref()
    .update(databaseDiff)
    .then(() => state)
}

function getFromUser(state) {
  console.info('[CONNECT][CANCEL-CONNECTION] getFromUser')

  return getUserById({
    state,
    userId: state.connection.fromId,
    saveAs: 'fromUser',
  })
}

function getToUser(state) {
  console.info('[CONNECT][CANCEL-CONNECTION] getToUser')

  if (state.connection.toId) {
    return getUserById({
      state,
      userId: state.connection.toId,
      saveAs: 'toUser',
    })
  } else {
    return Object.assign(state, { toUser: null })
  }
}

function sendPushNotification(state) {
  console.info('[CONNECT][CANCEL-CONNECTION] sendPushNotification')

  if (!state.notificationReceiver || !state.notificationObject) {
    return state
  }

  return getUserExpoPushTokens(state, state.notificationReceiver.id).then(
    state => {
      if (!state.expoPushTokens) {
        console.error("toUser doesn't have Expo push tokens")

        return Promise.resolve(state)
      }

      console.info('state.expoPushTokens: ', state.expoPushTokens)

      const message = getPushNotificationMessage(
        state.connectionCanceller,
        state.notificationReceiver
      )

      const pushNotifications = []

      Object.keys(state.expoPushTokens).forEach(function(key) {
        const token = state.expoPushTokens[key]

        pushNotifications.push({
          to: token,
          sound: 'default',
          body: message,
          data: Object.assign({}, state.notificationObject, {
            notificationType: 'connection-cancelled',
          }),
        })
      })

      return sendPushNotifications(pushNotifications)
        .then(() => Promise.resolve(state))
        .catch(error => {
          console.info('[CONNECT][CANCEL-CONNECTION] Error:', error)

          Promise.resolve(state)
        })
    }
  )
}

function getPushNotificationMessage(fromUser, toUser) {
  switch (toUser.appLocale) {
    case 'en':
      return `${fromUser.name} has disconnected from you`

    case 'sv':
      return `${fromUser.name} har avbrutit kopplingen mellan era konton`

    default:
      return `${fromUser.name} has disconnected from you`
  }
}
