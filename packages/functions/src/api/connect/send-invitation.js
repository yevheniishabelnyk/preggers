/**
 *
 * sendInvitation
 *
 */

'use strict'

const random = require('lodash/random')
const get = require('lodash/get')
const request = require('request')

const Handlebars = require('handlebars')

const flatten = require('src/utils/flatten')

const FirebaseAdmin = require('src/app').FirebaseAdmin
const sendTemplate = require('src/utils/Mandrill/sendTemplate')
const ServerTimestamp = require('src/models/shared/ServerTimestamp')
const UserTypes = require('src/constants/UserTypes')

const getUserById = require('src/helpers/getUserById')
const getUserByEmail = require('src/helpers/getUserByEmail')
const getMailingInvite = require('src/helpers/getMailingInvite')
const getUserExpoPushTokens = require('src/helpers/getUserExpoPushTokens')

const config = require('config')

module.exports = function(req) {
  const {
    email: toEmail,
    relationshipType,
    shareContractionTimer,
    shareTimeline,
    shareChecklist,
    shareBabyNames,
  } = req.body

  console.info('[CONNECT][SEND-INVITATION] toEmail: ', toEmail)
  console.info(
    '[CONNECT][SEND-INVITATION] relationshipType: ',
    relationshipType
  )

  if (req.user.email === toEmail) {
    return Promise.reject({
      status: 400,
      message: "You can't connect to yourself",
      code: 'connect/cant-connect-to-yourself',
    })
  }

  if (toEmail) {
    const initialState = {
      app: FirebaseAdmin,
      shareContractionTimer,
      shareTimeline,
      shareChecklist,
      shareBabyNames,
      relationshipType,
      toEmail,
      requestUser: req.user,
    }

    return getFromUser(initialState)
      .then(getToUser)
      .then(validateRequest)
      .then(updateDatabase)
      .then(notifyToUser)
      .then(() => ({}))
  } else {
    return Promise.reject({
      status: 400,
      message: 'Email required',
    })
  }
}

function getFromUser(state) {
  const fromUserId = state.requestUser.uid

  console.info('[CONNECT][SEND-INVITATION] getFromUser: ', fromUserId)

  return getUserById({ state, userId: fromUserId, saveAs: 'fromUser' })
}

function getToUser(state) {
  console.info('[CONNECT][SEND-INVITATION] getToUser: ', state.toEmail)

  return new Promise(resolve => {
    getUserByEmail({ state, email: state.toEmail, saveAs: 'toUser' })
      .then(state => {
        if (!state.toUser) {
          state.toUser = { email: state.toEmail }
        }

        resolve(state)
      })
      .catch(() => {
        state.toUser = { email: state.toEmail }
        resolve(state)
      })
  })
}

function generateCode() {
  return random(100000, 999999)
}

function validateRequest(state) {
  console.info('[CONNECT][SEND-INVITATION] validateRequest')

  if (
    [UserTypes.MOTHER, UserTypes.SINGLE_MOTHER].indexOf(state.toUser.type) !==
    -1
  ) {
    return Promise.reject({
      status: 400,
      code: 'connect/to-user-is-one-of-mother-types',
      message:
        'You have registered as a mother. You can not invite another mother to follow your pregnancy. You can only invite a user which has register as partner, father, friend or family.',
    })
  }

  return state
}

function notifyToUser(state) {
  console.info('[CONNECT][SEND-INVITATION] notifyToUser: ', state.toEmail)

  if (state.toUser.id) {
    return sendNotification(state)
  } else if (state.toUser.email) {
    return getMailingInvite({ state })
      .then(sendInvitationEmail)
      .catch(() => sendInvitationEmail(state))
  }
}

function sendNotification(state) {
  console.info('[CONNECT][SEND-INVITATION] sendNotification')

  return getUserExpoPushTokens(state, state.toUser.id).then(state => {
    if (!state.expoPushTokens) {
      console.error("toUser doesn't have Expo push tokens")

      return Promise.resolve(state)
    }

    console.info('state.expoPushTokens: ', state.expoPushTokens)

    const message = getPushNotificationMessage(state.fromUser, state.toUser)

    const pushNotifications = []

    Object.keys(state.expoPushTokens).forEach(function(key) {
      const token = state.expoPushTokens[key]

      pushNotifications.push({
        to: token,
        sound: 'default',
        body: message,
        data: Object.assign({}, state.invitation, {
          notificationType: 'invited-to-connect',
        }),
      })
    })

    return new Promise((resolve, reject) => {
      request(
        {
          method: 'POST',
          uri: `${config.EXPO__API_URL}/push/send`,
          json: pushNotifications,
          headers: {
            Accept: 'application/json',
            'Accept-Encoding': 'gzip, deflate',
            'User-Agent': 'expo-server-sdk-node',
          },
        },
        function(error, response, body) {
          if (error || response.statusCode !== 200) {
            console.log('Response error', error, body, response)

            reject(error)
          }

          console.log('Sent push notification to user ', state.toUser.id)

          resolve(state)
        }
      )
    })
  })
}

function sendInvitationEmail(state) {
  console.info('[CONNECT][SEND-INVITATION] sendInvitationEmail')

  return maybeSendInvitationEmail(state)
    .then(state =>
      state.app
        .database()
        .ref(`/invitationCodes/${state.invitation.id}`)
        .set(state.code)
    )
    .then(() => state)
}

function maybeSendInvitationEmail(state) {
  const code = generateCode()
  state.code = code

  if (!config.MAILING__IS_ENABLED) {
    return Promise.resolve(state)
  }

  const fromName = state.fromUser.name || ''
  const toUserRelationshipType = state.relationshipType || ''
  const fromLocale = state.fromUser.appLocale || config.APP__DEFAULT_LANGUAGE
  const fromUserLocation = get(state.fromUser, `location.countryCode`, '')

  const title = get(state, `mailingInvite.title.${fromLocale}`, '')
  let subject = get(state, `mailingInvite.subject.${fromLocale}`, '')
  const header = get(state, `mailingInvite.header.${fromLocale}`, '')
  const text = get(state, `mailingInvite.text.${fromLocale}`, '')
  const codeLabel = get(state, `mailingInvite.codeLabel.${fromLocale}`, '')
  const buttonTitle = get(state, `mailingInvite.buttonTitle.${fromLocale}`, '')
  const buttonLink = get(state, `mailingInvite.buttonLink`, '')
  const image = get(state, `mailingInvite.image.uri`, '')

  const apiMergeTags = ['FROM_USER_NAME', 'TO_USER_RELATIONSHIP_TYPE']

  const templateMergeTags = [
    {
      name: 'FROM_USER_NAME',
      content: fromName,
    },
    {
      name: 'HAS_FROM_USER_NAME',
      content: Boolean(fromName),
    },
    {
      name: 'FROM_USER_LOCALE',
      content: fromLocale,
    },
    {
      name: 'HAS_FROM_USER_LOCALE',
      content: Boolean(fromLocale),
    },
    {
      name: 'FROM_USER_COUNTRY_CODE',
      content: fromUserLocation,
    },
    {
      name: 'HAS_FROM_USER_COUNTRY_CODE',
      content: Boolean(fromUserLocation),
    },
    {
      name: 'TO_USER_RELATIONSHIP_TYPE',
      content: toUserRelationshipType,
    },
    {
      name: 'HAS_TO_USER_RELATIONSHIP_TYPE',
      content: Boolean(toUserRelationshipType),
    },
    {
      name: 'CODE',
      content: code,
    },
    {
      name: 'INVITE_TITLE',
      content: title,
    },
    {
      name: 'HAS_INVITE_TITLE',
      content: Boolean(title),
    },
    {
      name: 'INVITE_BUTTON_TITLE',
      content: buttonTitle,
    },
    {
      name: 'HAS_INVITE_BUTTON_TITLE',
      content: Boolean(buttonTitle),
    },
    {
      name: 'INVITE_BUTTON_LINK',
      content: buttonLink,
    },
    {
      name: 'HAS_INVITE_BUTTON_LINK',
      content: Boolean(buttonLink),
    },
    {
      name: 'INVITE_IMAGE',
      content: image,
    },
    {
      name: 'HAS_INVITE_IMAGE',
      content: Boolean(image),
    },
    {
      name: 'INVITE_HEADER',
      content: header,
    },
    {
      name: 'HAS_INVITE_HEADER',
      content: Boolean(header),
    },
    {
      name: 'INVITE_TEXT',
      content: text,
    },
    {
      name: 'HAS_INVITE_TEXT',
      content: Boolean(text),
    },
    {
      name: 'INVITE_CODE_LABEL',
      content: codeLabel,
    },
    {
      name: 'HAS_INVITE_CODE_LABEL',
      content: Boolean(codeLabel),
    },
  ]

  const apiData = templateMergeTags
    .filter(tag => apiMergeTags.indexOf(tag.name) !== -1)
    .reduce((out, tag) => {
      out[tag.name] = tag.content

      return out
    }, {})

  templateMergeTags
    .filter(tag => apiMergeTags.indexOf(tag.name) === -1)
    .forEach(tag => {
      if (typeof tag.content === 'string') {
        const template = Handlebars.compile(tag.content)
        tag.content = template(apiData)
      }
    })

  const emailSubjectTemplate = Handlebars.compile(subject)
  subject = emailSubjectTemplate(apiData)

  return sendTemplate({
    templateName: config.MAILING__INVITE_TEMPLATE,
    to: [
      {
        email: state.toUser.email,
        name: '',
      },
    ],
    fromEmail: 'invite@preggersapp.com',
    fromName: 'Preggers',
    subject,
    globalMergeVars: templateMergeTags,
  })
    .then(() => state)
    .catch(err => {
      console.info('Sending invitation template error: ', err)

      return state
    })
}

function updateDatabase(state) {
  console.info('[CONNECT][SEND-INVITATION] updateDatabase')

  const newInvitationId = state.app
    .database()
    .ref('/invitations')
    .push().key

  state.invitation = {
    id: newInvitationId,

    status: 'unread',

    fromId: state.fromUser.id,
    fromEmail: state.fromUser.email,
    fromName: state.fromUser.name || null,
    fromPhoto: state.fromUser.image ? state.fromUser.image.uri : null,

    toId: state.toUser.id || null,
    toEmail: state.toUser.email,
    toName: state.toUser.name || null,
    toPhoto: state.toUser.image ? state.toUser.image.uri : null,

    shareContractionTimer: state.shareContractionTimer || false,
    shareTimeline: state.shareTimeline || false,
    shareChecklist: state.shareChecklist || false,
    shareBabyNames: state.shareBabyNames || false,

    relationshipType: state.relationshipType || null,

    updatedAt: ServerTimestamp,
    createdAt: ServerTimestamp,
  }

  const databaseDiffObj = {
    invitations: {
      [state.invitation.id]: state.invitation,
    },

    users: {
      [state.fromUser.id]: {
        invitation: state.invitation.id,
      },
    },
  }

  if (state.toUser.id) {
    databaseDiffObj.userOwned = {
      invitations: {
        [state.toUser.id]: {
          [state.invitation.id]: 'unread',
        },
      },
    }
  }

  const databaseDiff = flatten(databaseDiffObj)

  return state.app
    .database()
    .ref()
    .update(databaseDiff)
    .then(() => state)
}

function getPushNotificationMessage(fromUser, toUser) {
  switch (toUser.appLocale) {
    case 'en':
      return `${fromUser.name} wants to connect`

    case 'sv':
      return `${fromUser.name} vill ansluta`

    default:
      return `${fromUser.name} wants to connect`
  }
}
