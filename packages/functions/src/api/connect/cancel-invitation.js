/**
 *
 * cancelConnection
 *
 */

'use strict'

const flatten = require('src/utils/flatten')

const FirebaseAdmin = require('src/app').FirebaseAdmin
const ServerTimestamp = require('src/models/shared/ServerTimestamp')
const getInvitationById = require('src/helpers/getInvitationById')

module.exports = function(req) {
  const { invitationId } = req.body

  console.info('[CONNECT][CANCEL-INVITATION] invitationId: ', invitationId)

  if (invitationId) {
    const initialState = { app: FirebaseAdmin, requestUser: req.user }

    return getInvitationById(initialState, invitationId)
      .then(validateRequest)
      .then(updateDatabase)
      .then(() => {
        return {}
      })
  } else {
    return Promise.reject({ status: 400, message: 'Connection ID required' })
  }
}

function validateRequest(state) {
  console.info('[CONNECT][CANCEL-INVITATION] validateRequest')

  if (state.invitation.fromId === state.requestUser.uid) {
    return Promise.resolve(state)
  } else {
    return Promise.reject({ status: 400, message: 'Permission denied' })
  }
}

function updateDatabase(state) {
  console.info('[CONNECT][CANCEL-INVITATION] updateDatabase')

  const databaseDiffObj = {
    invitations: {
      [state.invitation.id]: {
        status: 'canceled',
        updatedAt: ServerTimestamp,
      },
    },

    invitationCodes: {
      [state.invitation.id]: null,
    },

    users: {
      [state.invitation.fromId]: {
        invitation: null,
      },
    },
  }

  if (state.invitation.toId) {
    databaseDiffObj.userOwned = {
      invitations: {
        [state.invitation.toId]: {
          [state.invitation.id]: 'canceled',
        },
      },
    }
  }

  const databaseDiff = flatten(databaseDiffObj)

  console.info(
    '[CONNECT][CANCEL-INVITATION] diff: ',
    JSON.stringify(databaseDiff, null, 4)
  )

  return state.app
    .database()
    .ref()
    .update(databaseDiff)
    .then(() => state)
}
