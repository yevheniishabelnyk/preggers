/**
 *
 * acceptInvitation
 *
 */

'use strict'

const flatten = require('src/utils/flatten')

const FirebaseAdmin = require('src/app').FirebaseAdmin
const ServerTimestamp = require('src/models/shared/ServerTimestamp')
const getUserById = require('src/helpers/getUserById')
const getPregnancyById = require('src/helpers/getPregnancyById')
const getInvitationById = require('src/helpers/getInvitationById')
const UserTypes = require('src/constants/UserTypes')

module.exports = function(req) {
  const { invitationId, code } = req.body

  console.info('[CONNECT][ACCEPT-INVITATION] invitation ID', invitationId)
  console.info('[CONNECT][ACCEPT-INVITATION] invitation code', code)

  const initialState = {
    app: FirebaseAdmin,
    requestUser: req.user,
    code,
    invitationId,
  }

  return getInvitation(initialState, invitationId, code)
    .then(getFromUser)
    .then(getFromUserPregnancy)
    .then(getToUser)
    .then(validateRequest)
    .then(updateDatabase)
    .then(state => ({
      connectionId: state.newConnectionId,
      pregnancyId: state.fromUser.pregnancy,
    }))
}

function getInvitation(state, invitationId, code) {
  if (invitationId) {
    return getInvitationById(state, invitationId).catch(() =>
      Promise.reject({
        status: 400,
        code: 'connect/invitation-is-missing',
        message: `There is no invitation with ID ${invitationId} in the database.`,
      })
    )
  } else if (code) {
    return new Promise((resolve, reject) => {
      getInvitationIdByCode(state, code)
        .then(id =>
          getInvitationById(state, id)
            .then(resolve)
            .catch(() =>
              reject({
                status: 400,
                code: 'connect/wrong-code',
                message: `There is no invitation for code ${code} in the database.`,
              })
            )
        )
        .catch(() =>
          reject({
            status: 400,
            code: 'connect/wrong-code',
            message: `There is no invitation for code ${code} in the database.`,
          })
        )
    })
  }
}

function getInvitationIdByCode(state, code) {
  console.info('[CONNECT][ACCEPT-INVITATION] getInvitationIdByCode', code)

  const codesRef = state.app.database().ref('/invitationCodes')

  return new Promise((resolve, reject) => {
    codesRef
      .once('value')
      .then(snap => {
        if (snap.exists()) {
          const codes = snap.val()

          let invitationId
          Object.keys(codes).forEach(key => {
            if (codes[key] == code) {
              invitationId = key
            }
          })

          if (invitationId) {
            console.info(
              '[CONNECT][ACCEPT-INVITATION] getInvitationIdByCode result',
              invitationId
            )

            return resolve(invitationId)
          } else {
            reject('There is no invitation for this code')
          }
        } else {
          reject('There is no invitation for this code')
        }
      })
      .catch(reject)
  })
}

function getFromUser(state) {
  console.info(
    '[CONNECT][ACCEPT-INVITATION] getFromUser',
    state.invitation.fromId
  )

  return getUserById({
    state,
    userId: state.invitation.fromId,
    saveAs: 'fromUser',
  })
}

function getToUser(state) {
  const toUserId = state.invitation.toId || state.requestUser.uid

  console.info('[CONNECT][ACCEPT-INVITATION] getToUser', toUserId)

  return getUserById({ state, userId: toUserId, saveAs: 'toUser' })
}

function getFromUserPregnancy(state) {
  console.info(
    '[CONNECT][ACCEPT-INVITATION] getFromUserPregnancy',
    state.fromUser.pregnancy
  )

  if (state.fromUser.pregnancy) {
    return getPregnancyById({
      state,
      pregnancyId: state.fromUser.pregnancy,
      saveAs: 'fromUserPregnancy',
    })
  }

  return state
}

function validateRequest(state) {
  console.info('[CONNECT][ACCEPT-INVITATION] validateRequest')

  if (state.invitation.toEmail !== state.requestUser.email) {
    return Promise.reject({
      status: 400,
      code: 'connect/permission-denied',
      message: 'Only to user of invitation can accept it.',
    })
  }

  if (
    [UserTypes.MOTHER, UserTypes.SINGLE_MOTHER].indexOf(state.fromUser.type) ===
    -1
  ) {
    return Promise.reject({
      status: 400,
      code: 'connect/from-user-type-is-not-one-of-mother-types',
      message:
        'Mother has changed her user type. You can only connect to user with type "mother" or "single_mother".',
    })
  }

  if (
    state.toUser.type === UserTypes.MOTHER ||
    state.toUser.type === UserTypes.SINGLE_MOTHER
  ) {
    return Promise.reject({
      status: 400,
      code: 'connect/to-user-type-is-not-one-of-partner-types',
      message: `Partner has not valid user type "${state.toUser.type}"`,
    })
  }

  if (
    state.invitation.status === 'canceled' ||
    state.invitation.status === 'declined' ||
    state.invitation.status === 'accepted'
  ) {
    return Promise.reject({
      status: 400,
      code: 'connect/invitation-was-resolved',
      message: 'Invitation was resolved',
    })
  }

  if (state.fromUser.connection) {
    return Promise.reject({
      status: 400,
      code: 'connect/from-user-already-connected-with-another-user',
      message: 'Mother already connected with another user',
      payload: {
        fromUserName: state.fromUser.name,
      },
    })
  }

  if (state.toUser.connection) {
    return Promise.reject({
      status: 400,
      code: 'connect/to-user-already-connected-with-another-user',
      message: 'Partner already connected with another user',
    })
  }

  return state
}

function updateDatabase(state) {
  const newConnectionId = state.app
    .database()
    .ref('/connections')
    .push().key

  const databaseDiffObj = {
    users: {
      [state.toUser.id]: {
        connection: newConnectionId,
        partner: state.fromUser.id,
        invitation: null,
        updatedAt: ServerTimestamp,
        pregnancy: state.fromUser.pregnancy || null,
        inactivePregnancy: state.toUser.pregnancy || null,
      },

      [state.fromUser.id]: {
        connection: newConnectionId,
        partner: state.toUser.id,
        invitation: null,
        updatedAt: ServerTimestamp,
      },
    },

    invitations: {
      [state.invitation.id]: {
        status: 'accepted',
        toName: state.toUser.name || null,
        fromName: state.fromUser.name || null,
        fromPhoto: state.fromUser.image ? state.fromUser.image.uri : null,
        toId: state.toUser.id,
        toEmail: state.toUser.email,
        toPhoto: state.toUser.image ? state.toUser.image.uri : null,
        connectionId: newConnectionId,
        updatedAt: ServerTimestamp,
      },
    },

    userOwned: {
      invitations: {
        [state.toUser.id]: {
          [state.invitation.id]: 'accepted',
        },
      },
    },

    connections: {
      [newConnectionId]: {
        id: newConnectionId,

        status: 'active',

        invitationId: state.invitation.id,

        fromName: state.fromUser.name || null,
        fromEmail: state.fromUser.email,
        fromId: state.fromUser.id || null,
        fromPhoto: state.fromUser.image ? state.fromUser.image.uri : null,
        toName: state.toUser.name || null,
        toId: state.toUser.id,
        toEmail: state.toUser.email,
        toPhoto: state.toUser.image ? state.toUser.image.uri : null,

        shareContractionTimer: state.invitation.shareContractionTimer || false,
        shareTimeline: state.invitation.shareTimeline || false,
        shareChecklist: state.invitation.shareChecklist || false,
        shareBabyNames: state.invitation.shareBabyNames || false,

        relationshipType: state.invitation.relationshipType || null,

        updatedAt: ServerTimestamp,
        createdAt: ServerTimestamp,
      },
    },

    invitationCodes: {
      [state.invitation.id]: null,
    },
  }

  if (state.fromUser.pregnancy) {
    Object.assign(databaseDiffObj, {
      usersByPregnancy: {
        [state.fromUser.pregnancy]: {
          [state.toUser.id]: state.toUser.name || 'name',
        },
      },

      pregnanciesByUser: {
        [state.toUser.id]: {
          [state.fromUser.pregnancy]: state.fromUserPregnancy.childName,
        },
      },

      permissions: {
        pregnancies: {
          [state.fromUser.pregnancy]: {
            [state.toUser.id]: {
              level: 10,
              createdBy: state.fromUser.id,
              updatedAt: ServerTimestamp,
              createdAt: ServerTimestamp,
            },
          },
        },
      },
    })
  }

  if (state.toUser.pregnancy) {
    Object.assign(databaseDiffObj, {
      pregnancies: {
        [state.toUser.pregnancy]: {
          status: 'inactive',
        },
      },
    })
  }

  const databaseDiff = flatten(databaseDiffObj)

  console.info(
    '[CONNECT][ACCEPT-INVITATION] diff',
    JSON.stringify(databaseDiff, null, 4)
  )

  return state.app
    .database()
    .ref()
    .update(databaseDiff)
    .then(() => {
      state.newConnectionId = newConnectionId

      return state
    })
}
