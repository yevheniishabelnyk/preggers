/**
 *
 * Database Service
 *
 */

module.exports = {
  update: function(diffObj) {
    const flatten = require('src/utils/flatten')
    const FirebaseAdmin = require('src/app').FirebaseAdmin

    return FirebaseAdmin.database()
      .ref()
      .update(flatten(diffObj))
  },

  get: function(path) {
    const FirebaseAdmin = require('src/app').FirebaseAdmin

    return FirebaseAdmin.database()
      .ref(path)
      .once('value')
      .then(snap => snap.val())
  },

  queryEqualTo: function({ path, key, value }) {
    const FirebaseAdmin = require('src/app').FirebaseAdmin

    return FirebaseAdmin.database()
      .ref(path)
      .orderByChild(key)
      .equalTo(value)
      .once('value')
      .then(snap => snap.val())
  },
}
