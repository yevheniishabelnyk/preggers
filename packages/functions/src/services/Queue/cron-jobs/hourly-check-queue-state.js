/**
 *
 * Hourly check Queue state
 *
 */

const functions = require('firebase-functions')

const Queue = require('../index')

module.exports = functions.pubsub
  .topic('hourly-check-queue-state')
  .onPublish(() => Queue.ping())
