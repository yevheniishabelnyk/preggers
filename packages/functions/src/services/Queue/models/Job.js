/**
 *
 * Queue job model
 *
 */

const Model = require('objectmodel')

const ServerTimestamp = require('src/models/shared/ServerTimestamp')
const Timestamped = require('src/models/generic/Timestamped')
const firebaseKey = require('firebase-key')

const Job = new Model.ObjectModel({
  id: String,
  type: String,
  payload: [Object],
  meta: [Object],
})
  .defaults({
    payload: {},
    meta: {},
  })
  .extend(Timestamped)

Job.create = function(props) {
  const defaults = {
    id: firebaseKey.key(),
    status: 'pending',
    createdAt: ServerTimestamp,
    updatedAt: ServerTimestamp,
  }

  return new Job(Object.assign(defaults, props))
}

module.exports = Job
