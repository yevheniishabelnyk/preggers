/**
 *
 * preExecute JobQueue middleware
 *
 */

const FirebaseAdmin = require('src/app').FirebaseAdmin

const Queue = require('../index')
const QueueError = require('../constants/errors')
const QueueActionType = require('../constants/actionTypes')
const QueueJobType = require('../constants/jobTypes')

const JobModel = require('../models/Job')

module.exports = function(req) {
  if (!req.body.jobId) {
    return Promise.reject(QueueError.BAD_REQUEST)
  }

  req.timestamp = Date.now()

  return FirebaseAdmin.database()
    .ref('/queue/jobs')
    .limitToFirst(1)
    .once('value')
    .then(snap => {
      const result = snap.val()

      if (!result) {
        return Promise.reject(QueueError.IS_EMPTY)
      }

      if (!result[req.body.jobId]) {
        return Promise.reject(QueueError.IS_NOT_THE_NEXT_JOB_IN_QUEUE)
      }

      req.job = result[req.body.jobId]

      if (req.job.type !== req.jobType) {
        if (!QueueJobType[req.job.type]) {
          return Promise.reject(
            QueueError.JOB_TYPE_DOESNT_MATCH_ANY_API_HANDLER
          )
        }

        return Promise.reject(QueueError.JOB_TYPE_DOESNT_MATCH_API_HANDLER)
      }

      const payload = req.job.payload

      if (payload) {
        for (var prop in payload) {
          if (
            typeof payload[prop] === 'string' &&
            payload[prop].startsWith('[')
          ) {
            payload[prop] = JSON.parse(payload[prop])
          }
        }
      }

      try {
        req.job = new JobModel(req.job)
      } catch (err) {
        return Promise.reject(QueueError.INVALID_SCHEMA)
      }

      return Queue.transitState({
        type: QueueActionType.RUN,
        job: req.job,
        startedAt: req.timestamp,
      })
    })
    .catch(error => {
      console.log('catch', error)

      if (error === QueueError.IS_EMPTY) {
        return Promise.reject(QueueError.IS_EMPTY)
      }

      if (typeof error === 'object') {
        return Queue.catch({
          type: error.error,
          message: error.error,
          jobType: req.jobType,
          job: req.job || { id: req.body.jobId },
          queueState: error.state,
        }).then(() => Promise.reject(error.error))
      }

      return Queue.catch({
        type: error,
        message: error,
        jobType: req.jobType,
        job: req.job || { id: req.body.jobId },
      }).then(() => Promise.reject(error))
    })
}
