/**
 *
 * On mailing queues jobs write listener
 *
 */

const functions = require('firebase-functions')
const Queue = require('../index')

module.exports = functions.database.ref('/queue/jobs').onWrite(change => {
  if (change.after.exists()) {
    return Queue.ping()
  }

  return null
})
