/**
 *
 * Job Logger
 *
 */

const sendMessage = require('src/utils/Slack/sendMessage')
const config = require('config')

exports.error = function({ message, jobType, jobId, isExecutionError }) {
  const slackMessage = {
    text: `
Preggers *Queue Service* has catched an \`error\` on job type *${jobType}* with ID *${jobId}*!\n
\`${isExecutionError ? 'Execution - ' : 'Pre execution - '}${message}\``,
    mrkdwn_in: ['text'],
    attachments: [],
    unfurl_links: 0,
  }

  return sendMessage(slackMessage, config.JOB_QUEUE__SLACK_WEBHOOK)
}

exports.info = function(text, attachments) {
  const message = {
    text,
    mrkdwn_in: ['text'],
    attachments: [],
    unfurl_links: 0,
  }

  if (attachments) {
    const messageAttachment = {
      fields: attachments.map(item => Object.assign(item, { short: true })),
    }

    message.attachments.push(messageAttachment)
  }

  return sendMessage(message, config.JOB_QUEUE__SLACK_WEBHOOK)
}
