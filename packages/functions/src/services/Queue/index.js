/**
 *
 * Job Queue service
 *
 */

const flatten = require('src/utils/flatten')

const FirebaseAdmin = require('src/app').FirebaseAdmin

const config = require('config')
const request = require('request')

const QueueLogger = require('./utils/logger')
const QueueActionType = require('./constants/actionTypes')
const QueueStatus = require('./constants/statuses')
const QueueError = require('./constants/errors')
const JobRuntimeOptions = require('./constants/jobRuntimeOptions')
const JobModel = require('./models/Job')
const JobType = require('./constants/jobTypes')

exports.ping = function() {
  const jobsRef = FirebaseAdmin.database().ref('/queue/jobs')

  return jobsRef
    .limitToFirst(1)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const result = snap.val()

        const jobToProcess = result[Object.keys(result)[0]]

        if (jobToProcess) {
          if (JobType[jobToProcess.type]) {
            request({
              method: 'POST',
              uri: `${config.API__URL}/jobs-${jobToProcess.type}`,
              json: true,
              body: {
                jobId: jobToProcess.id,
              },
            })
          } else {
            return exports.catch({
              type: QueueError.JOB_TYPE_DOESNT_MATCH_ANY_API_HANDLER,
              message: QueueError.JOB_TYPE_DOESNT_MATCH_ANY_API_HANDLER,
              jobType: jobToProcess.type,
              job: jobToProcess,
            })
          }
        }
      }
    })
}

exports.transitState = function transitState(event) {
  var error = null

  return FirebaseAdmin.database()
    .ref('/queue/state')
    .transaction(function(state) {
      switch (event.type) {
        case QueueActionType.CLEAN: {
          if (state && state.processingJobId === event.jobId) {
            return {
              status: QueueStatus.FREE,
              processingJobId: null,
              startedAt: null,
              timeout: null,
            }
          }

          return state
        }

        case QueueActionType.RUN: {
          if (state && state.status !== QueueStatus.FREE) {
            const processingJobTimeout = state.timeout || 60

            if (
              !state.startedAt ||
              processingJobTimeout > JobRuntimeOptions.MAX_TIMEOUT ||
              (state.startedAt &&
                Date.now() - state.startedAt > processingJobTimeout * 1000)
            ) {
              error = QueueError.JOB_HAS_REACHED_TIMEOUT
            } else {
              error = QueueError.ANOTHER_JOB_IS_PROCESSING
            }

            return state
          }

          const jobTimeout =
            (JobRuntimeOptions[event.job.type] &&
              JobRuntimeOptions[event.job.type].timeoutSeconds) ||
            60

          return {
            status: QueueStatus.BUSY,
            processingJobId: event.job.id,
            startedAt: event.startedAt || Date.now(),
            timeout: jobTimeout,
          }
        }

        default:
          error = QueueError.UNKNOWN_STATE_TRANSITION

          return state
      }
    })
    .then(response => {
      const result = {
        state: response.snapshot.val(),
        error,
      }

      if (error) {
        return Promise.reject(result)
      }

      return result
    })
}

exports.addMany = function(jobs = []) {
  const databaseDiffObj = {
    queue: {
      jobs: jobs.map(item => JobModel.create(item)).reduce((out, job) => {
        out[job.id] = job

        return out
      }, {}),
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  return FirebaseAdmin.database()
    .ref()
    .update(databaseDiff)
    .then(() =>
      QueueLogger.info(
        `Preggers *Queue Service* has successfully queued *${
          jobs.length
        }* jobs!`
      )
    )
}

exports.then = function({ job, loggerAttachments }) {
  const databaseDiffObj = {
    queue: {
      state: {
        status: QueueStatus.FREE,
        processingJobId: null,
        startedAt: null,
        timeout: null,
      },
      jobs: {
        [job.id]: null,
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  return FirebaseAdmin.database()
    .ref()
    .update(databaseDiff)
    .then(() =>
      QueueLogger.info(
        `Preggers *Queue Service* has successfully executed *${
          job.type
        }* job with ID *${job.id}*!`,
        loggerAttachments
      )
    )
}

exports.catch = function({
  type,
  message,
  jobType,
  job = {},
  isExecutionError,
  queueState,
}) {
  switch (type) {
    case QueueError.JOB_TYPE_DOESNT_MATCH_ANY_API_HANDLER: {
      return exports
        .transitState({
          type: QueueActionType.CLEAN,
          jobId: job.id,
        })
        .then(result => {
          const databaseDiffObj = {
            queue: {
              jobs: {
                [job.id]: null,
              },
              errors: {
                [job.id]: {
                  id: job.id,
                  job,
                  error: message,
                },
              },
            },
          }

          if (result.state.processingJobId === job.id) {
            databaseDiffObj.queue.state = {
              status: QueueStatus.FREE,
              processingJobId: null,
              startedAt: null,
              timeout: null,
            }
          }

          return applyDatabaseDiff(databaseDiffObj).then(() =>
            QueueLogger.error({
              message: message,
              jobType: job.type,
              jobId: job.id,
              isExecutionError,
            })
          )
        })
    }

    case QueueError.JOB_HAS_REACHED_TIMEOUT:
    case QueueError.EXECUTION_ERROR: {
      const databaseDiffObj = {
        queue: {
          state: {
            status: QueueStatus.FREE,
            processingJobId: null,
            startedAt: null,
            timeout: null,
          },
          jobs: {
            [queueState.processingJobId]: null,
          },
        },
      }

      if (queueState.processingJobId !== job.id) {
        return FirebaseAdmin.database()
          .ref(`/queue/jobs/${queueState.processingJobId}`)
          .once('value')
          .then(snap => {
            const isProcessingJobExists = snap.exists()

            if (isProcessingJobExists) {
              databaseDiffObj.queue.errors = {
                [job.id]: {
                  id: job.id,
                  job: snap.val(),
                  error: JSON.stringify(message),
                },
              }
            }

            return applyDatabaseDiff(databaseDiffObj)
              .then(() => {
                if (!isProcessingJobExists) {
                  return exports.ping()
                }
              })
              .then(() =>
                QueueLogger.error({
                  message: JSON.stringify(message),
                  jobType: job.type,
                  jobId: job.id,
                  isExecutionError,
                })
              )
          })
      } else {
        databaseDiffObj.queue.errors = {
          [job.id]: {
            id: job.id,
            job,
            error: JSON.stringify(message),
          },
        }

        return applyDatabaseDiff(databaseDiffObj).then(() =>
          QueueLogger.error({
            message: JSON.stringify(message),
            jobType: job.type,
            jobId: job.id,
            isExecutionError,
          })
        )
      }
    }

    case QueueError.JOB_TYPE_DOESNT_MATCH_API_HANDLER:
      return QueueLogger.error({
        message,
        jobType: job.type,
        jobId: job.id,
        isExecutionError,
      })

    default:
      return QueueLogger.error({
        message,
        jobType,
        jobId: job.id,
        isExecutionError,
      })
  }

  function applyDatabaseDiff(diffObj) {
    const databaseDiff = flatten(diffObj)

    return FirebaseAdmin.database()
      .ref()
      .update(databaseDiff)
  }
}
