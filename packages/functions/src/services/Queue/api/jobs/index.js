/**
 *
 * /jobs Api
 *
 */

const functions = require('firebase-functions')

const sendWelcomeNewWeekEmails = require('./send-welcome-new-week-emails')

const Queue = require('../../index')
const QueueError = require('../../constants/errors')
const jobRuntimeOptions = require('../../constants/jobRuntimeOptions')
const preExecute = require('../../middlewares/preExecute')

exports.sendWelcomeNewWeekEmails = functions
  .runWith(jobRuntimeOptions.sendWelcomeNewWeekEmails)
  .https.onRequest((req, res) => {
    switch (req.method) {
      case 'POST':
        if (!req.body.jobId) {
          res.status(400).send('Please provide job ID')
          return
        }

        req.jobType = 'sendWelcomeNewWeekEmails'

        return preExecute(req)
          .then(() => {
            const { userIds, UTCOffset } = req.job.payload

            return sendWelcomeNewWeekEmails({ userIds, UTCOffset })
              .then(recipientUserIds =>
                Queue.then({
                  job: req.job,
                  loggerAttachments: [
                    { title: 'Sent emails', value: recipientUserIds.length },
                    { title: 'UTC offset', value: UTCOffset },
                    {
                      title: 'Execution time',
                      value: Math.floor((Date.now() - req.timestamp) / 1000),
                    },
                  ],
                })
              )
              .then(() => res.send('The job is successful!'))
              .catch(error =>
                Queue.catch({
                  type: QueueError.EXECUTION_ERROR,
                  message: error,
                  jobType: req.job.type,
                  job: req.job,
                  queueState: { processingJobId: req.job.id },
                  isExecutionError: true,
                }).then(() => Promise.reject(error))
              )
          })
          .catch(err => {
            if (
              err === QueueError.JOB_TYPE_DOESNT_MATCH_API_HANDLER &&
              req.job
            ) {
              res.redirect(307, `jobs-${req.job.type}`)
            } else {
              res.status(500).send(JSON.stringify(err))
            }
          })

      default:
        res.status(400).send('Please send a POST request')
    }
  })
