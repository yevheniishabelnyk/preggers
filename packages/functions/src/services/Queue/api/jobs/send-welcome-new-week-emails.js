const FirebaseAdmin = require('src/app').FirebaseAdmin

const compact = require('lodash/compact')
const get = require('lodash/get')

const userTypeMap = require('src/utils/ContentService/userTypeMap')

const getUserById = require('src/helpers/getUserById')
const getPregnancyById = require('src/helpers/getPregnancyById')
const getUserMailingSettings = require('src/helpers/getUserMailingSettings')

const sendWelcomeEmail = require('src/mailing/sendWelcomeEmail')

const request = require('request-promise')

const config = require('config')

const PromisePool = require('es6-promise-pool')
const MAX_CONCURRENT = 200

module.exports = function({ userIds, UTCOffset }) {
  const initialState = {
    app: FirebaseAdmin,
    UTCOffset,
    userIds,
    eligibleRecipientIds: [],
    contentServiceUserRequests: [],
    cache: {},
  }

  return fetchEmailRecipients(initialState)
    .then(fetchAdsIntoCache)
    .then(sendEmails)
}

function fetchEmailRecipients(jobState) {
  const eligibleRecipientIds = []

  const ids = jobState.userIds.slice()

  const pool = new PromisePool(
    () =>
      getUserIfTheyShouldReceiveEmailPromiseProducer(
        ids,
        jobState.app,
        jobState.cache,
        jobState.UTCOffset,
        eligibleRecipientIds
      ),
    MAX_CONCURRENT
  )

  return pool.start().then(() => {
    jobState.eligibleRecipientIds = eligibleRecipientIds

    return jobState
  })
}

function getUserIfTheyShouldReceiveEmailPromiseProducer(
  ids,
  app,
  cache,
  UTCOffset,
  eligibleRecipientIds
) {
  if (!ids.length) {
    return null
  }

  const userId = ids.pop()

  const state = {
    app,
    UTCOffset,
    userId,
    cache,
  }

  return getUserMailingSettings({ state, userId })
    .then(state => {
      if (!state.isWeeklyEmailsEnabled) {
        return Promise.reject('user-disabled-mailing')
      }

      return state
    })
    .then(state => getUserById({ state, userId }))
    .then(state => {
      if (!state.user.pregnancy) {
        return
      }

      return getPregnancyById({
        state,
        pregnancyId: state.user.pregnancy,
        withModel: true,
      }).then(state => {
        try {
          if (state.pregnancy.isFirstWeekDay(state.UTCOffset)) {
            eligibleRecipientIds.push(state.user.id)
          }
        } catch (err) {
          console.error('isFirstWeekDay method failed!', err)
        }
      })
    })
    .catch(error => {
      // Promise gets rejected if user disabled email, but it's not an actual error that we want to track.
      if (error === 'user-disabled-mailing') {
        return null
      }

      const errorObject = {
        location: 'fetchEmailRecipients',
        message: `Failed determining if user ${userId} should receive email`,
        thrownError: error || '',
      }

      console.error(errorObject)
    })
}

function fetchAdsIntoCache(jobState) {
  if (!jobState.eligibleRecipientIds.length) {
    return jobState
  }

  jobState.contentServiceUserRequests = compact(
    jobState.eligibleRecipientIds.map(userId => {
      const user = get(jobState.cache, `users.${userId}`)

      if (!user) {
        return null
      }

      const userPregnancy = get(jobState.cache, `pregnancies.${user.pregnancy}`)

      if (!userPregnancy) {
        return null
      }

      const pregnancyWeek = userPregnancy.getCurrentWeek(jobState.UTCOffset)

      const userLanguage = user.appLocale

      const locale =
        config.APP__LANGUAGES.indexOf(userLanguage) !== -1
          ? userLanguage
          : config.APP__DEFAULT_LANGUAGE

      return {
        userId,
        client: 'PREGGERS',
        types: ['EMAIL_TEMPLATE_AD'],
        email: user.email,
        userType: userTypeMap[user.type],
        gender: user.gender === 'm' ? 'MALE' : 'FEMALE',
        pregnancies: [userPregnancy.dueDate],
        locale: locale === 'en' ? 'en_GB' : 'sv_SE',
        country: get(user, 'location.countryCode', false),
        // There is no partner in the state at this moment!!!
        excludeIfEmailConverted: false,
        weekNumber: pregnancyWeek,
        emailTemplateType: 'NEW_WEEK_EMAIL',
      }
    })
  )

  if (!jobState.contentServiceUserRequests.length) {
    return jobState
  }

  return request({
    method: 'POST',
    uri: `${config.CONTENT_SERVICE__BASE_URL}/content/batch_fetch/`,
    json: true,
    body: jobState.contentServiceUserRequests,
  })
    .then(body => {
      if (Array.isArray(body)) {
        jobState.cache.ads = body
      }

      return jobState
    })
    .catch(err => {
      if (err) {
        const errorObject = {
          location: 'fetchAds',
          message: 'Failed fetching ads',
          thrownError: err || '',
        }

        console.error(errorObject)

        return jobState
      }
    })
}

function sendEmails(state) {
  const successIds = []

  const recipientIds = state.eligibleRecipientIds.slice()

  const pool = new PromisePool(
    () =>
      sendEmailPromiseProducer(
        recipientIds,
        state.UTCOffset,
        state.cache,
        successIds
      ),
    MAX_CONCURRENT
  )

  return pool.start().then(() => successIds)
}

function sendEmailPromiseProducer(ids, UTCOffset, cache, successIds) {
  if (!ids.length) {
    return null
  }

  const userId = ids.pop()

  return sendWelcomeEmail({
    userId,
    isNewWeek: true,
    UTCOffset,
    cache,
  })
    .then(() => {
      successIds.push(userId)
    })
    .catch(error => {
      if (
        typeof error === 'object' &&
        error.code === 'missing-mailing-week-data'
      ) {
        return null
      }

      const errorObject = {
        location: 'sendEmails',
        message: `Failed sending email to ${userId}`,
        thrownError: error || '',
      }

      console.error(errorObject)
    })
}
