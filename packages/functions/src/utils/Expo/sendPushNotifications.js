/**
 *
 * Send push notifications
 *
 */

const request = require('request')

const config = require('config')

module.exports = pushNotifications => {
  return new Promise((resolve, reject) => {
    request(
      {
        method: 'POST',
        uri: `${config.EXPO__API_URL}/push/send`,
        json: pushNotifications,
        headers: {
          Accept: 'application/json',
          'Accept-Encoding': 'gzip, deflate',
          'User-Agent': 'expo-server-sdk-node',
        },
      },
      (error, response) => {
        if (error || response.statusCode !== 200) {
          reject(error)
        }

        resolve()
      }
    )
  })
}
