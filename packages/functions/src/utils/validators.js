/**
 *
 * Validators
 *
 */

function isValidTimeFormat(value) {
  return isValidDateFormat(value) || isValidDateWithTimeFormat(value)
}

function isValidDateFormat(value) {
  return /^((\d{4})-(\d{2})-(\d{2}))$/.test(value)
}

function isValidDateWithTimeFormat(value) {
  return /^((\d{4})-(\d{2})-(\d{2})\s(\d{1,2}):(\d{2}))$/.test(value)
}

module.exports = {
  isValidTimeFormat,
  isValidDateFormat,
  isValidDateWithTimeFormat,
}
