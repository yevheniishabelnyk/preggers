/**
 *
 * Mandrill utils
 *
 */

const config = require('config')
const mandrill = require('node-mandrill')(config.mandrill.api_key)

module.exports = ({ to, headers, fromEmail, fromName, subject, text }) =>
  new Promise((resolve, reject) =>
    mandrill(
      '/messages/send',

      {
        message: {
          to,
          headers,
          from_email: fromEmail,
          from_name: fromName,
          subject,
          text,
        },
      },

      function(error, response) {
        if (error) {
          reject(error)
        } else {
          resolve(response)
        }
      }
    )
  )
