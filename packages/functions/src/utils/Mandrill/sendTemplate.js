/**
 *
 * Mandrill utils
 *
 */

const config = require('config')
const mandrill = require('node-mandrill')(config.mandrill.api_key)

module.exports = ({
  templateName,
  templateContent = [],
  to,
  fromEmail,
  fromName,
  subject,
  globalMergeVars,
}) =>
  new Promise((resolve, reject) =>
    mandrill(
      '/messages/send-template',

      {
        template_name: templateName,
        template_content: templateContent,
        message: {
          to,
          from_email: fromEmail,
          from_name: fromName,
          subject,
          global_merge_vars: globalMergeVars,
        },
      },

      function(error, response) {
        if (error) {
          reject(error)
        } else {
          resolve(response)
        }
      }
    )
  )
