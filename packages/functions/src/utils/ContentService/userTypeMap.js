const UserTypes = require('src/constants/UserTypes')

module.exports = {
  [UserTypes.MOTHER]: 'MOTHER',
  [UserTypes.SINGLE_MOTHER]: 'SINGLE_MOTHER',
  [UserTypes.FATHER]: 'FATHER',
  [UserTypes.PARTNER]: 'PARTNER',
  [UserTypes.GRANDPARENT]: 'GRANDPARENT',
  [UserTypes.UNCLE]: 'UNCLE',
  [UserTypes.ANT]: 'AUNT',
  [UserTypes.FRIEND]: 'FRIEND',
  [UserTypes.FAMILY]: 'FAMILY',
}
