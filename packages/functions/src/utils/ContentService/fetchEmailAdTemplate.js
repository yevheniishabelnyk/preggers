/**
 *
 * Content Service Utils
 *
 */

const get = require('lodash/get')
const config = require('config')
const request = require('request')
const userTypeMap = require('./userTypeMap')

module.exports = state =>
  new Promise(resolve => {
    const userLanguage = state.user.appLocale

    if (state.cache && state.cache.ads) {
      state.adHtmlTemplate = state.cache.ads
        .map(ad => {
          if (
            ad &&
            ad.eligibleUserIds &&
            ad.eligibleUserIds.includes(state.user.id)
          ) {
            return ad.htmlTemplate
          }

          return ''
        })
        .join('')

      return resolve(state)
    }

    const locale =
      config.APP__LANGUAGES.indexOf(userLanguage) !== -1
        ? userLanguage
        : config.APP__DEFAULT_LANGUAGE

    // console.log(
    //   '[ADS] Fetching email ads with request body:',
    //   JSON.stringify(requestParams)
    // )

    request(
      {
        method: 'POST',
        uri: `${config.CONTENT_SERVICE__BASE_URL}/content/fetch/`,
        json: true,
        body: {
          client: 'PREGGERS',
          types: ['EMAIL_TEMPLATE_AD'],
          email: state.user.email,
          userType: userTypeMap[state.user.type],
          gender: state.user.gender === 'm' ? 'MALE' : 'FEMALE',
          pregnancies: [get(state, 'pregnancy.dueDate')],
          locale: locale === 'en' ? 'en_GB' : 'sv_SE',
          country: get(state, 'user.location.countryCode'),
          excludeIfEmailConverted: get(state, 'partner.email'),
          weekNumber: state.pregnancyWeek,
          emailTemplateType: state.isNewUser
            ? 'WELCOME_EMAIL'
            : 'NEW_WEEK_EMAIL',
        },
      },
      (error, response, body) => {
        if (error) {
          console.log('[ADS] Server error when fetching ads', error)

          return resolve(state)
        }

        if (Array.isArray(body)) {
          state.adHtmlTemplate = body.map(ad => ad.htmlTemplate).join('')
        }

        resolve(state)
      }
    )
  })
