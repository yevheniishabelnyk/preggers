module.exports = function(preggersLocale) {
  const contentServiceDefault = 'en_GB'

  const map = {
    en: 'en_GB',
    sv: 'sv_SE',
  }

  return map[preggersLocale] || contentServiceDefault
}
