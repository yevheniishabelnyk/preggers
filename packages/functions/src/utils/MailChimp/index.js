/**
 *
 * MailChimp utils
 *
 */

const request = require('request')

exports.addListMember = (listId, email) =>
  new Promise((resolve, reject) => {
    const options = {
      method: 'POST',
      url: `https://us15.api.mailchimp.com/3.0/lists/${listId}/members`,
      headers: {
        authorization: 'Basic <mailchimp_token>',
        'content-type': 'application/json',
      },
      body: {
        email_address: email,
        status: 'subscribed',
      },
      json: true,
    }

    request(options, function(error, response, body) {
      if (error && body.title !== 'Member Exists') {
        console.info('addMailChimpMember error: ', body)
        reject()
      } else {
        console.info('addMailChimpMember success: ', body)
        resolve()
      }
    })
  })

exports.triggerEmailEvent = email =>
  new Promise((resolve, reject) => {
    const options = {
      method: 'POST',
      url:
        'https://us15.api.mailchimp.com/3.0/automations/fd49cb360f/emails/bdf60ec7eb/queue',
      headers: {
        authorization: 'Basic <mailchimp_token>',
        'content-type': 'application/json',
      },
      body: { email_address: email },
      json: true,
    }

    request(options, function(error, response, body) {
      if (error) {
        console.info('sendEmailMailChimp error: ', body)
        reject(error)
      } else {
        console.info('sendEmailMailChimp success: ', body)
        resolve()
      }
    })
  })
