/**
 *
 * getUTCOffsetWeekDay
 *
 */

module.exports = function(dueDate, lengthType, userUTCOffset) {
  const moment = require('moment')
  const Weekdays = require('../constants/Weekdays')

  let newWeekStartsOnDayIndex = moment(dueDate).isoWeekday()

  if (lengthType === '39+6') {
    newWeekStartsOnDayIndex =
      newWeekStartsOnDayIndex !== 7 ? newWeekStartsOnDayIndex + 1 : 1
  }

  const weekdays = {
    1: Weekdays.MONDAY,
    2: Weekdays.TUESDAY,
    3: Weekdays.WEDNESDAY,
    4: Weekdays.THURSDAY,
    5: Weekdays.FRIDAY,
    6: Weekdays.SATURDAY,
    7: Weekdays.SUNDAY,
  }

  return `${userUTCOffset}_${weekdays[newWeekStartsOnDayIndex]}`
}
