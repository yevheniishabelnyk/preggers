/**
 *
 * Send message to Slack
 *
 */

const request = require('request')

module.exports = function sendMessage(data, webhookUrl) {
  return new Promise((resolve, reject) => {
    request.post(
      webhookUrl,
      {
        form: {
          payload: JSON.stringify(data),
        },
      },
      function(err, response) {
        if (err) {
          return reject(err)
        }

        if (response.body !== 'ok') {
          return reject(new Error(response.body))
        }

        resolve(response.body)
      }
    )
  })
}
