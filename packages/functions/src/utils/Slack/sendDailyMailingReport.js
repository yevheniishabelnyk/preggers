/**
 *
 * Send mailing report to Slack
 *
 */

'use strict'

const config = require('config')
const sendMessage = require('./sendMessage')

module.exports = function sendDailyMailingReport(UTCOffset, emails) {
  var adminUrl

  switch (config.database.environment) {
    case 'development':
      adminUrl = 'http://localhost:3000'
      break

    case 'staging':
      adminUrl = 'http://staging.preggersapp.com'
      break

    case 'production':
      adminUrl = 'http://admin.preggersapp.com'
      break

    default:
      adminUrl = 'http://admin.preggersapp.com'
  }

  const text = `\`Daily ${
    config.MAILING__SENDING_HOURS
  }:00\`\n Preggers has successfully sent *Welcome New Week* emails to *${
    emails.length
  }* user${emails.length > 1 ? 's' : ''} in *${UTCOffset}:00*
     UTC offset. :envelope_with_arrow:`

  const message = {
    text,
    mrkdwn_in: ['text'],
    attachments: [],
    unfurl_links: 0,
  }

  const publisherAttachment = {
    fallback: '',
    pretext: '',
    ts: Date.now() / 1000,
    fields: [
      {
        title: 'Edit Merge Tags',
        value: `<${adminUrl}/mailing/welcomeNewUser|Welcome New User>\n<${adminUrl}/mailing/weeks|Welcome New Week>`,
        short: true,
      },
      {
        title: 'Sandbox',
        value: `<${adminUrl}/mailing/testing|Link>`,
        short: true,
      },
    ],
  }

  message.attachments.push(publisherAttachment)

  return sendMessage(message, config.MAILING__SLACK_WEBHOOK)
}
