/**
 *
 * Send daily new users report to Slack
 *
 */

'use strict'

const config = require('config')
const sendMessage = require('./sendMessage')
const countBy = require('lodash/countBy')
const values = require('lodash/values')

module.exports = function sendDailyMailingReport({ users, day, error }) {
  let usersCollection = []
  if (users) {
    usersCollection = values(users)
  }

  const usersCountByDevice = countBy(usersCollection, user => user.device)

  const usersCountByPlatform = Object.keys(usersCountByDevice).reduce(
    (out, device) => {
      if (/^(iPhone|iPad)(.+)?$/i.test(device)) {
        out.iOS = out.iOS + usersCountByDevice[device]
      } else if (/^(android)(.+)?$/i.test(device)) {
        out.Android = out.Android + usersCountByDevice[device]
      } else {
        out.Unknown = out.Unknown + usersCountByDevice[device]
      }

      return out
    },
    {
      iOS: 0,
      Android: 0,
      Unknown: 0,
    }
  )

  const deviceInfo = Object.keys(usersCountByPlatform)
    .map(platform => `\n${platform}: ${usersCountByPlatform[platform]}`)
    .join('')

  const textBody =
    error ||
    `*${usersCollection.length}* new users on *${day}*! :tada: ${deviceInfo}`

  const text = `\`Daily 7:00\`\n ${textBody}`

  console.info('text: ', text)

  const message = {
    text,
    mrkdwn_in: ['text'],
    attachments: [],
    unfurl_links: 0,
  }

  return sendMessage(message, config.PREGGERS__SLACK_WEBHOOK)
}
