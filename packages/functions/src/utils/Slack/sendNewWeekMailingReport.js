/**
 *
 * Send mailing report to Slack
 *
 */

'use strict'

const config = require('config')
const sendMessage = require('./sendMessage')

module.exports = function sendNewWeekMailingReport(
  userId,
  UTCOffset,
  wasSentonDueDateChange
) {
  var adminUrl

  switch (config.database.environment) {
    case 'development':
      adminUrl = 'http://localhost:3000'
      break

    case 'staging':
      adminUrl = 'http://staging.preggersapp.com'
      break

    case 'production':
      adminUrl = 'http://admin.preggersapp.com'
      break

    default:
      adminUrl = 'http://admin.preggersapp.com'
  }

  var header

  if (wasSentonDueDateChange) {
    header = '`Due date has changed - Welcome New Week`'
  } else {
    header = '`Welcome New Week`'
  }

  const text = `${header}\n Preggers has successfully sent *Welcome New Week* email to user with ID *${userId}* in *${UTCOffset}:00* UTC offset. :envelope_with_arrow:`

  const message = {
    text,
    mrkdwn_in: ['text'],
    attachments: [],
    unfurl_links: 0,
  }

  const publisherAttachment = {
    fallback: '',
    pretext: '',
    ts: Date.now() / 1000,
    fields: [
      {
        title: 'Edit Merge Tags',
        value: `<${adminUrl}/mailing/welcomeNewUser|Welcome New User>\n<${adminUrl}/mailing/weeks|Welcome New Week>`,
        short: true,
      },
      {
        title: 'Sandbox',
        value: `<${adminUrl}/mailing/testing|Link>`,
        short: true,
      },
    ],
  }

  message.attachments.push(publisherAttachment)

  return sendMessage(message, config.MAILING__SLACK_WEBHOOK)
}
