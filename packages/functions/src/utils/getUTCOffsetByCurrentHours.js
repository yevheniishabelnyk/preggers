/**
 *
 * Get UTC offset by current hours
 */

const moment = require('moment')

module.exports = function(hours) {
  const UTCHours = moment.utc().hours()

  let offset

  if (UTCHours > hours || UTCHours === 0) {
    const numberOfUTCHours = UTCHours === 0 && hours !== 0 ? 24 : UTCHours

    offset = hours - numberOfUTCHours
  } else {
    offset = hours - UTCHours
  }

  return offset > 0 ? `+${offset}` : offset + ''
}
