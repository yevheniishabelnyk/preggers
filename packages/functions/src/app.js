/**
 *
 * App
 *
 */

const admin = require('firebase-admin')
const algoliasearch = require('algoliasearch')
const config = require('config')

const appOptions = JSON.parse(process.env.FIREBASE_CONFIG)

const app = admin.initializeApp(appOptions, 'default')

module.exports.FirebaseAdmin = app

var Algolia

if (config.algolia.app_id) {
  Algolia = algoliasearch(config.algolia.app_id, config.algolia.admin_key)
}

module.exports.Algolia = Algolia
