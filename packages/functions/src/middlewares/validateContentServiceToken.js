const FirebaseAdmin = require('src/app').FirebaseAdmin

module.exports = function validateContentServiceToken(req, res, next) {
  console.info('[MIDDLEWARES][validateContentServiceToken]')

  const authorization = req.get('Authorization')

  if (!authorization || !authorization.startsWith('Bearer ')) {
    console.error(
      '[MIDDLEWARES][validateContentServiceToken] Error: No content service token was supplied'
    )

    return res.status(401).send('Unauthorized')
  }

  if (authorization && authorization.startsWith('Bearer ')) {
    console.info(
      '[MIDDLEWARES][validateContentServiceToken] Found content service token in request header!'
    )

    const token = authorization.split('Bearer ')[1]

    FirebaseAdmin.database()
      .ref('/apiTokens/contentService')
      .once('value')
      .then(snap => {
        if (!snap.exists()) {
          return res
            .status(404)
            .send('There is no content service token available')
        }

        const value = snap.val()

        if (value !== token) {
          return res.status(401).send('Unauthorized: not an active token')
        }

        next()
      })
      .catch(error => {
        console.error(
          '[MIDDLEWARES][validateContentServiceToken] Token verification failed',
          error
        )

        res.status(403).send('Token verification failed')
      })
  }
}
