const FirebaseAdmin = require('src/app').FirebaseAdmin

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
module.exports = function validateFirebaseIdToken(req, res, next) {
  console.info('[MIDDLEWARES][validateFirebaseIdToken]')

  const authorization = req.get('Authorization')

  if (!authorization || !authorization.startsWith('Bearer ')) {
    console.error(
      '[MIDDLEWARES][validateFirebaseIdToken] Error: No Firebase ID token was passed'
    )

    return res.status(403).send('Unauthorized')
  }

  let idToken

  if (authorization && authorization.startsWith('Bearer ')) {
    console.info(
      '[MIDDLEWARES][validateFirebaseIdToken] Found ID token in request header!'
    )

    idToken = authorization.split('Bearer ')[1]
  }

  FirebaseAdmin.auth()
    .verifyIdToken(idToken)
    .then(decodedIdToken => {
      console.info(
        '[MIDDLEWARES][validateFirebaseIdToken] ID Token correctly decoded!'
      )

      req.user = decodedIdToken

      if (req.user && req.user.uid) {
        console.log(
          '[MIDDLEWARES][validateFirebaseIdToken] Request user uid: ',
          req.user.uid
        )
      }

      next()
    })
    .catch(error => {
      console.error(
        '[MIDDLEWARES][validateFirebaseIdToken] Error: Firebase ID token verification faild',
        error
      )

      res.status(403).send('Unauthorized')
    })
}
