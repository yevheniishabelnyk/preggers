/**
 *
 * Express middleware that validates request user if one of the admins.
 *
 */

const FirebaseAdmin = require('src/app').FirebaseAdmin

module.exports = function validateAdminRights(req, res, next) {
  console.info('[MIDDLEWARES][validateAdminRights]')

  const user = req.user

  if (!user) {
    console.error(
      '[MIDDLEWARES][validateFirebaseIdToken] Error: No user in request!'
    )

    return res.status(403).send("You don't have admin rights")
  }

  FirebaseAdmin.database()
    .ref(`/admins/${user.uid}`)
    .once('value')
    .then(snap => {
      if (!snap.exists()) {
        return res.status(403).send("You don't have admin rights")
      }

      const value = snap.val()

      if (value !== user.email) {
        return res.status(403).send("You don't have admin rights")
      }

      next()
    })
    .catch(error => {
      console.error(
        '[MIDDLEWARES][validateAdminRights] Error: Admin rights verification faild',
        error
      )

      res.status(403).send("You don't have admin rights")
    })
}
