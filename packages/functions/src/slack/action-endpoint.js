/**
 *
 * Slack action endpoint
 *
 */

'use strict'

const find = require('lodash/find')
const findIndex = require('lodash/findIndex')
const pull = require('lodash/pull')

module.exports = function(req) {
  const event = JSON.parse(req.body.payload)

  const message = event.original_message
  message.replace_original = true

  // console.info('event: ', JSON.stringify(event, null, 4))

  if (event.callback_id === 'app_release_review_actions') {
    const publisherUserId = find(
      find(message.attachments, {
        id: 2,
      }).fields,
      { title: 'Publisher' }
    ).value.match(/^<@(.+)>/)[1]

    if (publisherUserId === event.user.id) {
      return Promise.resolve(message)
    } else {
      const reviewAction = find(event.actions, {
        name: 'review_action',
      })

      const reviewActionsAttachmentIndex = findIndex(message.attachments, {
        callback_id: 'app_release_review_actions',
      })

      let reviewAttachment =
        message.attachments[reviewActionsAttachmentIndex + 1]

      if (!reviewAttachment) {
        reviewAttachment = {
          fallback: 'reviews',
          mrkdwn_in: ['pretext'],
        }

        message.attachments.push(reviewAttachment)
      }

      reviewAttachment.pretext = createReviews(
        reviewAttachment.pretext,
        reviewAction.value,
        event.user.id
      )

      return Promise.resolve(message)
    }
  } else {
    return Promise.resolve(message)
  }
}

function createReviews(reviews = '', reviewValue, userId) {
  const approvedReviews = reviews.split('\n')[0] || ''
  const rejectedReviews = reviews.split('\n')[1] || ''

  let approvedUsers = (approvedReviews.match(/<@[^>]+>/g) || []).map(str =>
    str.substring(2, str.length - 1)
  )

  let rejectedUsers = (rejectedReviews.match(/<@[^>]+>/g) || []).map(str =>
    str.substring(2, str.length - 1)
  )

  switch (reviewValue) {
    case 'approved':
      if (rejectedUsers.indexOf(userId) !== -1) {
        rejectedUsers = pull(rejectedUsers, userId)
      }

      if (approvedUsers.indexOf(userId) === -1) {
        approvedUsers.push(userId)
      }

      break

    case 'rejected':
      if (approvedUsers.indexOf(userId) !== -1) {
        approvedUsers = pull(approvedUsers, userId)
      }

      if (rejectedUsers.indexOf(userId) === -1) {
        rejectedUsers.push(userId)
      }

      break
  }

  const approved = approvedUsers.length
    ? `:white_check_mark: ${approvedUsers
        .map(name => `<@${name}>`)
        .join(', ')} ${
        approvedUsers.length > 1 ? 'have' : 'has'
      } approved this release.`
    : ''

  const rejected = rejectedUsers.length
    ? `:warning: ${rejectedUsers.map(name => `<@${name}>`).join(', ')} ${
        rejectedUsers.length > 1 ? 'have' : 'has'
      } rejected this release.`
    : ''

  return `${approved}${approved ? '\n' : ''}${rejected}`
}
