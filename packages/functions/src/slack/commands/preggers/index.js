/**
 *
 * database slash commands
 *
 */

const getUsers = require('./getUsers')
const minimist = require('minimist')
const stringArgv = require('string-argv')

module.exports = function(req) {
  const text = (req.body.text || '').replace(/‘|’|"|“|”/g, "'")

  const processArgv = stringArgv(text)

  if (processArgv.length) {
    const command = processArgv[0]
    const commandArgv = processArgv.slice(1)

    const argv = minimist(commandArgv)

    Object.keys(argv).forEach(key => {
      const value = argv[key]

      if (typeof value === 'string') {
        argv[key] = value.replace(/'/g, '')
      }
    })

    switch (command) {
      case 'users':
        return getUsers(argv)

      default:
        return Promise.resolve('Unknown command')
    }
  }

  return Promise.resolve('Please provide some arguments')
}
