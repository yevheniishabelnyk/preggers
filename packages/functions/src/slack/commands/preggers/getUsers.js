/**
 *
 * "/preg users" Slack command
 *
 */

const FirebaseAdmin = require('src/app').FirebaseAdmin
const getNewUsersInPeriod = require('src/helpers/getNewUsersInPeriod')

module.exports = function(argv) {
  const initialState = {
    timezone: 'Europe/Stockholm',
    startTime: argv.start,
    endTime: argv.end,
    day: argv.day,
    app: FirebaseAdmin,
  }

  console.info('Get new users statistics arguments: ', argv)

  return getNewUsersInPeriod({ state: initialState }).then(state => {
    if (state.error) {
      console.info('state.error: ', state.error)
      return state.error
    }

    const { usersNumber, startDate, endDate } = state.data

    return `Preggers has *${usersNumber}* new users between *${startDate}* and *${endDate}*! :eyes:`
  })
}
