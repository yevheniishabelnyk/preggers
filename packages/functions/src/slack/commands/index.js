/**
 *
 * Slack slash commands
 *
 */

'use strict'

const PreggersCommands = require('./preggers')

module.exports = function(req) {
  const { command } = req.body

  switch (command) {
    case '/preg':
      return PreggersCommands(req)

    default:
      return Promise.reject({ code: 401, message: 'Bad request' })
  }
}
