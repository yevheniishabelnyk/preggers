const functions = require('firebase-functions')

exports.commands = functions.https.onRequest((req, res) => {
  const handleSlashCommand = require('./commands')

  switch (req.method) {
    case 'POST': {
      return handleSlashCommand(req)
        .then(responseText => {
          res.status(200).json({
            response_type: 'ephemeral',
            text: responseText,
          })
        })
        .catch(error => {
          console.log(error)
          const code = error.code || 500

          res.status(code).json({ error })
        })
    }
  }
})
