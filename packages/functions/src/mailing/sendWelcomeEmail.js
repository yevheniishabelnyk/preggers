/**
 *
 * Send welcome new user email
 *
 */

const get = require('lodash/get')
const Handlebars = require('handlebars')

const FirebaseAdmin = require('src/app').FirebaseAdmin
const sendTemplate = require('src/utils/Mandrill/sendTemplate')
const fetchEmailAdTemplate = require('src/utils/ContentService/fetchEmailAdTemplate')

const getWeek = require('src/helpers/getWeek')
const getMailingWeek = require('src/helpers/getMailingWeek')
const getMailingWelcomeNewUser = require('src/helpers/getMailingWelcomeNewUser')
const getUserById = require('src/helpers/getUserById')
const getPregnancyById = require('src/helpers/getPregnancyById')

const UserTypes = require('src/constants/UserTypes')
const MailingApiMergeTags = require('src/constants/MailingApiMergeTags')
const DatabaseConstants = require('src/constants/database')

const config = require('config')

module.exports = function({
  userId,
  isNewUser = false,
  isNewWeek = false,
  pregnancyWeek,
  UTCOffset,
  cache,
}) {
  const initialState = {
    app: FirebaseAdmin,
    userId,
    isNewUser,
    isNewWeek,
    pregnancyWeek,
    UTCOffset,
    cache,
  }

  return getUserById({ state: initialState, userId })
    .then(state => {
      if (!state.user.pregnancy) {
        return Promise.reject({
          status: 500,
          message: "User doesn't have pregnancy",
        })
      }

      return getPregnancyById({
        state,
        pregnancyId: state.user.pregnancy,
        withModel: true,
      })
    })
    .then(state => {
      if (!state.pregnancyWeek) {
        state.pregnancyWeek = state.pregnancy.getCurrentWeek(state.UTCOffset)
      }

      return getWeek({ state, weekNumber: state.pregnancyWeek })
    })
    .then(state => getMailingWeek({ state, weekNumber: state.pregnancyWeek }))
    .then(state => {
      if (!state.mailingWeek) {
        return Promise.reject({
          code: 'missing-mailing-week-data',
          message: `No mailing week for week ${
            state.pregnancyWeek
          } in firebase`,
        })
      }

      return state
    })
    .then(state => getMailingWelcomeNewUser({ state }))
    .then(state => {
      if (state.user.partner) {
        return getUserById({
          state,
          userId: state.user.partner,
          saveAs: 'partner',
          isRequired: false,
        })
      }

      return state
    })
    .then(fetchEmailAdTemplate)
    .then(selectTemplateMergeTags)
    .then(sendNewWeekTemplate)
}

function selectTemplateMergeTags(state) {
  // console.log('selectTemplateMergeTags')

  const userLanguage = state.user.appLocale
  const locale =
    userLanguage && config.APP__LANGUAGES.indexOf(userLanguage) !== -1
      ? userLanguage
      : config.APP__DEFAULT_LANGUAGE

  // welcome new user vars
  const welcomeSubject = get(
    state,
    `mailingWelcomeNewUser.subject.${locale}`,
    'Welcome to Preggers!'
  )
  const welcomeTitle = get(state, `mailingWelcomeNewUser.title.${locale}`, '')
  const welcomeImage = get(state, 'mailingWelcomeNewUser.image.url', '')
  const welcomeIngress = get(
    state,
    `mailingWelcomeNewUser.ingress.${locale}`,
    ''
  )
  const welcomeText = get(state, `mailingWelcomeNewUser.text.${locale}`, '')
  const welcomeButtonTitle = get(
    state,
    `mailingWelcomeNewUser.buttonTitle.${locale}`,
    ''
  )
  const welcomeButtonLink = get(state, 'mailingWelcomeNewUser.buttonLink', '')

  // welcome new week vars
  const weekImage = get(state, 'week.image.uri', '')
  const mailingWeekImage = get(state, 'mailingWeek.image.uri', '')
  const weekSubject = get(state, `mailingWeek.subject.${locale}`, '')
  const weekTitle = get(state, `mailingWeek.title.${locale}`, '')
  const weekIngress = get(state, `mailingWeek.ingress.${locale}`, '')
  const weekText = get(state, `mailingWeek.text.${locale}`, '')
  const weekButtonTitle = get(state, `mailingWeek.buttonTitle.${locale}`, '')
  const weekButtonLink = get(state, 'mailingWeek.buttonLink', '')

  // other vars
  const partnerName = get(state, 'partner.name', '')
  const childName = get(state, 'pregnancy.childName', '')
  const hasChildName =
    Boolean(childName) &&
    childName !== DatabaseConstants.PREGNANCY__DEFAULT_BABY_NAME

  // ads
  const adHtmlTemplate = get(state, 'adHtmlTemplate', '')

  state.emailSubject = state.isNewUser ? welcomeSubject : weekSubject

  state.templateMergeTags = [
    {
      name: 'IS_NEW_USER',
      content: state.isNewUser,
    },
    {
      name: 'IS_NEW_WEEK',
      content: state.isNewWeek,
    },
    {
      name: 'IS_MOTHER',
      content:
        state.user.type === UserTypes.MOTHER ||
        state.user.type === UserTypes.SINGLE_MOTHER,
    },
    {
      name: 'LOCALE',
      content: locale,
    },
    {
      name: 'USER_NAME',
      content: state.user.name,
    },
    {
      name: 'WEEK_NUMBER',
      content: state.pregnancyWeek,
    },
    {
      name: 'WELCOME_TITLE',
      content: welcomeTitle,
    },
    {
      name: 'HAS_WELCOME_TITLE',
      content: Boolean(welcomeTitle),
    },
    {
      name: 'WELCOME_IMAGE',
      content: welcomeImage,
    },
    {
      name: 'HAS_WELCOME_IMAGE',
      content: Boolean(welcomeImage),
    },
    {
      name: 'WELCOME_INGRESS',
      content: welcomeIngress,
    },
    {
      name: 'HAS_WELCOME_INGRESS',
      content: Boolean(welcomeIngress),
    },
    {
      name: 'WELCOME_TEXT',
      content: welcomeText,
    },
    {
      name: 'HAS_WELCOME_TEXT',
      content: Boolean(welcomeText),
    },
    {
      name: 'WEEK_IMAGE',
      content: weekImage,
    },
    {
      name: 'HAS_WEEK_IMAGE',
      content: Boolean(weekImage),
    },
    {
      name: 'MAILING_WEEK_IMAGE',
      content: mailingWeekImage,
    },
    {
      name: 'HAS_MAILING_WEEK_IMAGE',
      content: Boolean(mailingWeekImage),
    },
    {
      name: 'WEEK_TITLE',
      content: weekTitle,
    },
    {
      name: 'HAS_WEEK_TITLE',
      content: Boolean(weekTitle),
    },
    {
      name: 'WEEK_INGRESS',
      content: weekIngress,
    },
    {
      name: 'HAS_WEEK_INGRESS',
      content: Boolean(weekIngress),
    },
    {
      name: 'WEEK_TEXT',
      content: weekText,
    },
    {
      name: 'HAS_WEEK_TEXT',
      content: Boolean(weekText),
    },
    {
      name: 'PARTNER_NAME',
      content: partnerName,
    },
    {
      name: 'HAS_PARTNER',
      content: Boolean(partnerName),
    },
    {
      name: 'CHILD_NAME',
      content: hasChildName ? childName : '',
    },
    {
      name: 'HAS_CHILD_NAME',
      content: hasChildName,
    },
    {
      name: 'WELCOME_BUTTON_TITLE',
      content: welcomeButtonTitle,
    },
    {
      name: 'HAS_WELCOME_BUTTON_TITLE',
      content: Boolean(welcomeButtonTitle),
    },
    {
      name: 'WELCOME_BUTTON_LINK',
      content: welcomeButtonLink,
    },
    {
      name: 'HAS_WELCOME_BUTTON_LINK',
      content: Boolean(welcomeButtonLink),
    },
    {
      name: 'WEEK_BUTTON_TITLE',
      content: weekButtonTitle,
    },
    {
      name: 'HAS_WEEK_BUTTON_TITLE',
      content: Boolean(weekButtonTitle),
    },
    {
      name: 'WEEK_BUTTON_LINK',
      content: weekButtonLink,
    },
    {
      name: 'HAS_WEEK_BUTTON_LINK',
      content: Boolean(weekButtonLink),
    },
    {
      name: 'HAS_AD_HTML_TEMPLATE',
      content: Boolean(adHtmlTemplate),
    },
    {
      name: 'AD_HTML_TEMPLATE',
      content: adHtmlTemplate,
    },
  ]

  return state
}

function sendNewWeekTemplate(state) {
  // console.log('sendNewWeekTemplate')

  const apiData = state.templateMergeTags
    .filter(tag => MailingApiMergeTags.indexOf(tag.name) !== -1)
    .reduce((out, tag) => {
      out[tag.name] = tag.content

      return out
    }, {})

  state.templateMergeTags
    .filter(tag => MailingApiMergeTags.indexOf(tag.name) === -1)
    .forEach(tag => {
      if (typeof tag.content === 'string') {
        const template = Handlebars.compile(tag.content)
        tag.content = template(apiData)
      }
    })

  const emailSubjectTemplate = Handlebars.compile(state.emailSubject)
  state.emailSubject = emailSubjectTemplate(apiData)

  // console.info(
  //   'state.templateMergeTags: ',
  //   JSON.stringify(state.templateMergeTags, null, 4)
  // )

  var fromEmail = 'welcome@preggersapp.com'

  if (state.isNewUser) {
    fromEmail = 'welcome@preggersapp.com'
  }

  if (state.isNewWeek) {
    fromEmail = 'week@preggersapp.com'
  }

  if (!fromEmail) {
    fromEmail = 'welcome@preggersapp.com'
  }

  return sendTemplate({
    templateName: config.MAILING__WELCOME_TEMPLATE,
    to: [
      {
        email: state.user.email,
        name: state.user.name,
      },
    ],
    fromEmail,
    fromName: 'Preggers',
    subject: state.emailSubject,
    globalMergeVars: state.templateMergeTags,
  })
}
