/**
 *
 * getMailingWeek
 *
 */

const get = require('lodash/get')
const set = require('lodash/set')

module.exports = function({ state, weekNumber, saveAs = 'mailingWeek' }) {
  // console.log('getMailingWeek', weekNumber)

  if (state.cache) {
    const week = get(state.cache, `mailing.weeks.${weekNumber}`)

    if (week) {
      state[saveAs] = week

      return Promise.resolve(state)
    }
  }

  return state.app
    .database()
    .ref(`/mailing/weeks/${weekNumber}`)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const week = snap.val()

        if (state.cache) {
          set(state.cache, `mailing.weeks.${weekNumber}`, week)
        }

        state[saveAs] = week

        return state
      } else {
        return state
      }
    })
}
