/**
 *
 * getWeeks
 *
 */

module.exports = function(state, saveAs = 'weeks') {
  console.log('getWeeks')

  return state.app
    .database()
    .ref('/content/weeks')
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = snap.val()

        console.info('getWeeks result: ', state[saveAs])

        return state
      } else {
        return state
      }
    })
}
