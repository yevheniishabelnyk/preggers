/**
 *
 * getUserBabynameLikes
 *
 */

module.exports = function(state, userId, saveAs = 'userBabynameLikes') {
  console.log('[HELPERS] getUserBabynameLikes', userId)

  const userBabynameLikesRef = state.app
    .database()
    .ref('/babynameLikesByUser')
    .child(userId)

  return userBabynameLikesRef.once('value').then(snap => {
    if (snap.exists()) {
      state[saveAs] = snap.val()

      console.info('pregnancySnap.val(): ', snap.val())

      return state
    } else {
      return Promise.reject({
        code: 401,
        message: 'Error: no user babyname likes data in database',
      })
    }
  })
}
