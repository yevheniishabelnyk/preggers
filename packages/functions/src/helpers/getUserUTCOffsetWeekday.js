/**
 *
 * getUserUTCOffsetWeekday
 *
 */

module.exports = function(state, userId, saveAs = 'currentUTCOffsetWeekday') {
  return state.app
    .database()
    .ref(`/joins/UTCOffsetWeekdayByUser/${userId}`)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const currentUTCOffsetWeekday = snap.val()

        state[saveAs] = currentUTCOffsetWeekday
      }

      return state
    })
}
