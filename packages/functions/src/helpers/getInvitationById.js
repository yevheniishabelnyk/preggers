/**
 *
 * getInvitationById
 *
 */

module.exports = function(state, invitationId, saveAs = 'invitation') {
  console.log('getInvitationById', invitationId)

  if (invitationId) {
    const invitationRef = state.app
      .database()
      .ref('/invitations')
      .child(invitationId)

    return invitationRef.once('value').then(invitationSnap => {
      if (invitationSnap.exists()) {
        state[saveAs] = invitationSnap.val()

        console.info('invitationSnap.val(): ', invitationSnap.val())

        return state
      } else {
        return Promise.reject({
          code: 401,
          message: 'Error: no invitation data in database',
        })
      }
    })
  }

  return Promise.reject({
    code: 401,
    message: 'Error: no invitation ID',
  })
}
