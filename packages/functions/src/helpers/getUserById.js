/**
 *
 * getUserById
 *
 */

const get = require('lodash/get')
const set = require('lodash/set')

module.exports = function({
  state,
  userId,
  saveAs = 'user',
  isRequired = true,
}) {
  // console.log('[HELPERS] getUserById', userId)

  if (state.cache) {
    const user = get(state.cache, `users.${userId}`)

    if (user) {
      state[saveAs] = user

      return Promise.resolve(state)
    }
  }

  const userRef = state.app
    .database()
    .ref('/users')
    .child(userId)

  return userRef.once('value').then(userSnap => {
    if (userSnap.exists()) {
      const user = userSnap.val()
      state[saveAs] = user

      if (state.cache) {
        set(state.cache, `users.${userId}`, user)
      }

      return state
    } else if (isRequired) {
      return Promise.reject({
        code: 401,
        message: 'Error: no user data in database',
      })
    } else {
      return state
    }
  })
}
