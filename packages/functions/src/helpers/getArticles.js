/**
 *
 * getArticles
 *
 */

module.exports = function(state, saveAs = 'articles') {
  console.log('getArticles')

  return state.app
    .database()
    .ref('/content/articles')
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = snap.val()

        console.info('getArticles result: ', state[saveAs])

        return state
      } else {
        return state
      }
    })
}
