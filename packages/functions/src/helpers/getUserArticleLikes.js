/**
 *
 * getUserArticleLikes
 *
 */

module.exports = function(state, userId, saveAs = 'userArticleLikes') {
  console.log('[HELPERS] getUserArticleLikes', userId)

  const userBabynameLikesRef = state.app
    .database()
    .ref('/articleLikesByUser')
    .child(userId)

  return userBabynameLikesRef.once('value').then(snap => {
    if (snap.exists()) {
      state[saveAs] = snap.val()

      console.info('[HELPERS]  getUserArticleLikes result: ', snap.val())

      return state
    } else {
      return Promise.reject({
        code: 401,
        message: 'Error: no user article likes data in database',
      })
    }
  })
}
