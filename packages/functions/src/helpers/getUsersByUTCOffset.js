/**
 *
 * getUsersByUTCOffset
 *
 */

module.exports = function(app, UTCOffset) {
  return app
    .database()
    .ref(`/usersByUTCOffset/${UTCOffset}`)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        return Object.keys(snap.val())
      }
    })
}
