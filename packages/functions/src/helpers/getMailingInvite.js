/**
 *
 * getMailingWInvite
 *
 */

const get = require('lodash/get')
const set = require('lodash/set')

module.exports = function({ state, saveAs = 'mailingInvite' }) {
  console.log('getMailingInvite')

  if (state.cache) {
    const mailingInvite = get(state.cache, 'mailing.invite')

    if (mailingInvite) {
      state[saveAs] = mailingInvite

      return Promise.resolve(state)
    }
  }

  return state.app
    .database()
    .ref('/mailing/invite')
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const mailingInvite = snap.val()

        if (state.cache) {
          set(state.cache, 'mailing.invite', mailingInvite)
        }

        state[saveAs] = mailingInvite

        return state
      } else {
        return state
      }
    })
}
