/**
 *
 * getWeek
 *
 */

const get = require('lodash/get')
const set = require('lodash/set')

module.exports = function({ state, weekNumber, saveAs = 'week' }) {
  // console.log('getWeek', weekNumber)

  if (state.cache) {
    const week = get(state.cache, `content.weeks.${weekNumber}`)

    if (week) {
      state[saveAs] = week

      return state
    }
  }

  return state.app
    .database()
    .ref(`/content/weeks/${weekNumber}`)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const week = snap.val()

        if (state.cache) {
          set(state.cache, `content.weeks.${weekNumber}`, week)
        }

        state[saveAs] = week

        return state
      } else {
        return state
      }
    })
}
