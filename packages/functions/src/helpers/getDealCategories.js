/**
 *
 * getDealCatagories
 *
 */

module.exports = function(state, saveAs = 'dealCatagories') {
  console.log('getDealCatagories')

  return state.app
    .database()
    .ref('/content/dealCategories')
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = snap.val()

        console.info('getDealCatagories result: ', state[saveAs])

        return state
      } else {
        return state
      }
    })
}
