/**
 *
 * getArticleUserLikes
 *
 */

module.exports = function(state, articleId, saveAs = 'articleUserLikes') {
  console.log('getArticleUserLikes', articleId)

  return state.app
    .database()
    .ref(`/userLikesByArticle/${articleId}`)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = snap.val()

        console.info('getArticleUserLikes result: ', state[saveAs])

        return state
      } else {
        return state
      }
    })
}
