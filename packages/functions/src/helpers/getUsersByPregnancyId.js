/**
 *
 * getUsersByPregnancyId
 *
 */

const keys = require('lodash/keys')

module.exports = function(state, pregnancyId, saveAs = 'userIds') {
  console.log('getUsersByPregnancyId', pregnancyId)

  return state.app
    .database()
    .ref('/usersByPregnancy')
    .child(pregnancyId)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = keys(snap.val())

        return state
      } else {
        state[saveAs] = []

        return state
      }
    })
}
