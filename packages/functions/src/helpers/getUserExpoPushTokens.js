/**
 *
 * getUserExpoPushTokens
 *
 */

module.exports = function(state, userId, saveAs = 'expoPushTokens') {
  console.log('getUserExpoPushTokens', userId)

  return state.app
    .database()
    .ref(`/userOwned/tokens/${userId}/expoPushTokens`)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = snap.val()

        return state
      } else {
        return state
      }
    })
}
