/**
 *
 * getUserPregnancies
 *
 */

const keys = require('lodash/keys')
const compact = require('lodash/compact')

module.exports = function(state, userId, saveAs = 'userPregnancies') {
  console.log('getUserPregnancies', userId)

  return state.app
    .database()
    .ref('/pregnanciesByUser')
    .child(userId)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const pregnanciesIds = keys(snap.val())

        if (pregnanciesIds.length) {
          const promises = pregnanciesIds.map(pregnancyId => () =>
            state.app
              .database()
              .ref('/pregnancies')
              .child(pregnancyId)
              .once('value')
              .then(pregnancySnap => {
                if (pregnancySnap.exists()) {
                  return pregnancySnap.val()
                }

                return null
              })
          )

          return Promise.all(promises.map(promise => promise())).then(
            pregnancies => {
              console.info('user pregnancies: ', pregnancies)

              state[saveAs] = compact(pregnancies)
              return state
            }
          )
        } else {
          state[saveAs] = []
          return state
        }
      } else {
        state[saveAs] = []
        return state
      }
    })
}
