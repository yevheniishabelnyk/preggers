/**
 *
 * getPregnancyById
 *
 */

const Pregnancy = require('src/models/Pregnancy')
const get = require('lodash/get')
const set = require('lodash/set')

module.exports = function({
  state,
  pregnancyId,
  saveAs = 'pregnancy',
  withModel,
}) {
  // console.log('[HELPERS] getPregnancyById', pregnancyId)

  if (state.cache) {
    const pregnancy = get(state.cache, `pregnancies.${pregnancyId}`)

    if (pregnancy) {
      state[saveAs] = pregnancy

      return Promise.resolve(state)
    }
  }

  const pregnancyRef = state.app
    .database()
    .ref('/pregnancies')
    .child(pregnancyId)

  return pregnancyRef.once('value').then(pregnancySnap => {
    if (pregnancySnap.exists()) {
      var pregnancy

      if (withModel) {
        pregnancy = new Pregnancy(pregnancySnap.val())
      } else {
        pregnancy = pregnancySnap.val()
      }

      if (state.cache) {
        set(state.cache, `pregnancies.${pregnancy.id}`, pregnancy)
      }

      state[saveAs] = pregnancy

      return state
    } else {
      return Promise.reject({
        code: 401,
        message: 'Error: no pregnancy data in database',
      })
    }
  })
}
