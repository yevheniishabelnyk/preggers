/**
 *
 * getMailingWelcomeNewUser
 *
 */

const get = require('lodash/get')
const set = require('lodash/set')

module.exports = function({ state, saveAs = 'mailingWelcomeNewUser' }) {
  // console.log('getMailingWelcomeNewUser')

  if (state.cache) {
    const mailingWelcomeNewUser = get(state.cache, 'mailing.welcomeNewUser')

    if (mailingWelcomeNewUser) {
      state[saveAs] = mailingWelcomeNewUser

      return Promise.resolve(state)
    }
  }

  return state.app
    .database()
    .ref('/mailing/welcomeNewUser')
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const mailingWelcomeNewUser = snap.val()

        if (state.cache) {
          set(state.cache, 'mailing.welcomeNewUser', mailingWelcomeNewUser)
        }

        state[saveAs] = mailingWelcomeNewUser

        return state
      } else {
        return state
      }
    })
}
