/**
 *
 * getTags
 *
 */

module.exports = function(state, saveAs = 'tags') {
  console.log('getTags')

  return state.app
    .database()
    .ref('/content/tags')
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = snap.val()

        console.info('getTags result: ', state[saveAs])

        return state
      } else {
        return state
      }
    })
}
