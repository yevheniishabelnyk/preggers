/**
 *
 * getConnectionId
 *
 */

module.exports = function(state, connectionId, saveAs = 'connection') {
  console.log('getConnectionId', connectionId)

  if (connectionId) {
    const connectionRef = state.app
      .database()
      .ref('/connections')
      .child(connectionId)

    return connectionRef.once('value').then(connectionSnap => {
      if (connectionSnap.exists()) {
        state[saveAs] = connectionSnap.val()

        console.info('connectionSnap.val(): ', connectionSnap.val())

        return state
      } else {
        return Promise.reject({
          code: 401,
          message: 'Error: no connection data in database',
        })
      }
    })
  }

  return Promise.reject({
    code: 401,
    message: 'Error: no connection ID',
  })
}
