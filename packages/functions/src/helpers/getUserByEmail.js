/**
 *
 * getUserByEmail
 *
 */

const values = require('lodash/values')
const set = require('lodash/set')

module.exports = function({ state, email, saveAs = 'user' }) {
  console.log('[HELPERS] getUserByEmail', email)

  if (state.cache && state.cache.users) {
    const user = values(state.cache.users).find(user => user.email === email)

    if (user) {
      state[saveAs] = user

      return Promise.resolve(state)
    }
  }

  return state.app
    .database()
    .ref('/users')
    .orderByChild('email')
    .equalTo(email)
    .once('value')
    .then(querySnap => {
      if (querySnap.exists()) {
        const queryResult = values(querySnap.val())

        const user = queryResult[0]

        if (user) {
          state[saveAs] = user

          if (state.cache) {
            set(state.cache, `users.${user.id}`, user)
          }
        }

        return state
      } else {
        return Promise.reject({
          code: 401,
          message: 'Error: no user data in database',
        })
      }
    })
}
