/**
 *
 * getUserFiles
 *
 */

const keys = require('lodash/keys')

module.exports = function(state, userId, saveAs = 'userFiles') {
  console.log('getUserFiles', userId)

  return state.app
    .database()
    .ref('/userOwned/files')
    .child(userId)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = keys(snap.val()).map(filePath =>
          decodeURIComponent(filePath)
        )

        console.info('getUserFiles result: ', state[saveAs])
      }

      return state
    })
}
