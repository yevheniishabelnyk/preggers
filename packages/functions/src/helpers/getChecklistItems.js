/**
 *
 * getChecklistItems
 *
 */

module.exports = function(state, saveAs = 'checklistItems') {
  console.log('getChecklistItems')

  return state.app
    .database()
    .ref('/content/checklistItems')
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = snap.val()

        console.info('getChecklistItems result: ', state[saveAs])

        return state
      } else {
        return state
      }
    })
}
