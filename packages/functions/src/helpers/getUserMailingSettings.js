/**
 *
 * getUserMailingSettings
 *
 */

module.exports = function({ state, userId, saveAs = 'isWeeklyEmailsEnabled' }) {
  return state.app
    .database()
    .ref(`/userOwned/settings/${userId}/isWeeklyEmailsEnabled`)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const mailingSettings = snap.val()

        state[saveAs] = mailingSettings

        return state
      } else {
        state[saveAs] = true

        return state
      }
    })
}
