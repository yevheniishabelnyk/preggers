/**
 *
 * getUserChildrenIds
 *
 */

const keys = require('lodash/keys')

module.exports = function(state, userId, saveAs = 'childrenIds') {
  console.log('getUserChildrenIds', userId)

  return state.app
    .database()
    .ref('/childrenByUser')
    .child(userId)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        state[saveAs] = keys(snap.val())

        console.info('getUserChildrenIds result: ', state[saveAs])

        return state
      } else {
        state[saveAs] = []

        return state
      }
    })
}
