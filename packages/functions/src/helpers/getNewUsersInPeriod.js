/**
 *
 * getNewUsersInPeriod
 *
 */

const validators = require('src/utils/validators')

module.exports = function({ state, saveAs = 'data' }) {
  console.log('getNewUsersInPeriod')

  state[saveAs] = {}

  const moment = require('moment-timezone')

  if (state.day) {
    if (!validators.isValidDateFormat(state.day)) {
      state.error = 'Valid day format is "YYYY-MM-DD"'

      return Promise.resolve(state)
    }

    state.startTime = `${state.day} 00:00`
    state.endTime = `${state.day} 23:59`
  }

  if (state.startTime) {
    if (!validators.isValidTimeFormat(state.startTime)) {
      state.error =
        'Start time has invalid format. Valid formats are "YYYY-MM-DD" and "YYYY-MM-DD HH:mm"'

      return Promise.resolve(state)
    }

    if (validators.isValidDateFormat(state.startTime)) {
      state.startTime = moment
        .tz(state.startTime + ' 00:00', state.timezone)
        .valueOf()
    } else if (validators.isValidDateWithTimeFormat(state.startTime)) {
      state.startTime = moment.tz(state.startTime, state.timezone).valueOf()
    }
  }

  if (state.endTime) {
    if (!validators.isValidTimeFormat(state.endTime)) {
      state.error =
        'End time has invalid format. Valid formats are "YYYY-MM-DD" and "YYYY-MM-DD HH:mm"'

      return Promise.resolve(state)
    }

    if (validators.isValidDateFormat(state.endTime)) {
      state.endTime = moment
        .tz(state.endTime + ' 23:59', state.timezone)
        .valueOf()
    } else if (validators.isValidDateWithTimeFormat(state.endTime)) {
      state.endTime = moment.tz(state.endTime, state.timezone).valueOf()
    }
  }

  if (!state.startTime || !state.endTime) {
    if (!state.startTime) {
      state.startTime = moment()
        .tz(state.timezone)
        .startOf('day')
        .valueOf()
    }

    if (!state.endTime) {
      state.endTime = moment()
        .tz(state.timezone)
        .valueOf()
    }
  }

  if (!state.startTime || !state.endTime) {
    state.error = 'Start time and end time are required'

    return Promise.resolve(state)
  }

  if (state.startTime > state.endTime) {
    state.error = 'Start time should be less than the end time'

    return Promise.resolve(state)
  }

  return state.app
    .database()
    .ref('/users')
    .orderByChild('createdAt')
    .startAt(state.startTime)
    .endAt(state.endTime)
    .once('value')
    .then(snap => {
      const data = {}

      data.startDate = moment(state.startTime)
        .tz(state.timezone)
        .format('YYYY-MM-DD HH:mm')

      data.endDate = moment(state.endTime)
        .tz(state.timezone)
        .format('YYYY-MM-DD HH:mm')

      if (snap.exists()) {
        data.users = snap.val()
        data.usersNumber = Object.keys(data.users).length

        console.info('usersNumber: ', data.usersNumber)

        if (data.usersNumber) {
          state[saveAs] = data

          return state
        }
      }

      state.error = `No new users between *${data.startDate}* and *${
        data.endDate
      }*.`

      return state
    })
    .catch(error => {
      state.error = error.message || 'Sorry, something went wrong'

      return state
    })
}
