/**
 *
 * getUserProfileByEmail
 *
 */

module.exports = function(state, email, saveAs = 'userProfile') {
  return state.app
    .auth()
    .getUserByEmail(email)
    .then(function(userProfile) {
      console.log('Successfully fetched user data:', userProfile.toJSON())

      state[saveAs] = userProfile

      return state
    })
}
