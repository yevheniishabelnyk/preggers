/**
 *
 * Hourly tick
 *
 */

const functions = require('firebase-functions')
const chunk = require('lodash/chunk')

const getUsersByUTCOffset = require('src/helpers/getUsersByUTCOffset')
const FirebaseAdmin = require('src/app').FirebaseAdmin

const getUTCOffsetByCurrentHours = require('src/utils/getUTCOffsetByCurrentHours')

const config = require('config')

const Queue = require('src/services/Queue')

module.exports = functions.pubsub
  .topic('hourly-create-jobs-send-welcome-new-week-emails')
  .onPublish(() => {
    if (!config.MAILING__IS_ENABLED) {
      return Promise.reject('Mailing is desabled with config.')
    }

    const UTCOffset = getUTCOffsetByCurrentHours(config.MAILING__SENDING_HOURS)

    return getUsersByUTCOffset(FirebaseAdmin, UTCOffset).then(userIds => {
      if (userIds) {
        const jobs = chunk(
          userIds,
          config.JOBS__SEND_WELCOME_NEW_WEEK_EMAILS_BATCH_SIZE
        ).map(ids => ({
          type: 'sendWelcomeNewWeekEmails',
          payload: {
            userIds: JSON.stringify(ids),
            UTCOffset,
          },
        }))

        return Queue.addMany(jobs)
      }
    })
  })
