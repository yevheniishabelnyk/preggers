/**
 *
 * Cron jobs
 *
 */

module.exports = {
  hourlyCreateJobsSendWelcomeNewWeekEmails: require('./hourly-create-jobs-send-welcome-new-week-emails'),
  dailyUpdateAlgoliaIndices: require('./daily-update-algolia-indices'),
  slackDailySummary: require('./slack-daily-summary'),
  hourlyCheckQueueState: require('src/services/Queue/cron-jobs/hourly-check-queue-state'),
}
