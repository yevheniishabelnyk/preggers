/**
 *
 * Slack daily summary
 *
 */

const functions = require('firebase-functions')
const config = require('config')
const moment = require('moment')

const FirebaseAdmin = require('src/app').FirebaseAdmin
const getNewUsersInPeriod = require('src/helpers/getNewUsersInPeriod')
const sendDailyNewUsersReport = require('src/utils/Slack/sendDailyNewUsersReport')

module.exports = functions.pubsub.topic('slack-daily-summary').onPublish(() => {
  console.log('[SLACK-DAILY-SUMMARY]')

  if (config.database.environment !== 'production') {
    return Promise.resolve()
  }

  const yesterday = moment()
    .subtract(1, 'days')
    .format('YYYY-MM-DD')

  const initialState = {
    timezone: 'Europe/Stockholm',
    day: yesterday,
    app: FirebaseAdmin,
  }

  return getNewUsersInPeriod({ state: initialState }).then(state => {
    const { users } = state.data

    return sendDailyNewUsersReport({
      users,
      day: state.day,
      error: state.error,
    })
  })
})
