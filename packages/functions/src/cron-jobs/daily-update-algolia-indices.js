/**
 *
 * Daily update Algolia Indices
 *
 */

const functions = require('firebase-functions')

const updatePreggersKnowledgeItems = require('src/algolia/indices/preggers-knowledge-items/update')

module.exports = functions.pubsub
  .topic('daily-update-algolia-indices')
  .onPublish(() => {
    console.log('[DAILY-UPDATE-ALGOLIA-INDICES]')

    return updatePreggersKnowledgeItems().catch(err => {
      console.log('[DAILY-UPDATE-ALGOLIA-INDICES] Error: ', err)
    })
  })
