/**
 *
 * validateMigrationStatus middleware
 *
 */

module.exports = function validateMigrationStatus(req, res, next) {
  const FirebaseAdmin = require('src/app').FirebaseAdmin
  const values = require('lodash/values')

  const migrationId = req.params.id
  const { type: requestedMigrationType } = req.body

  FirebaseAdmin.database()
    .ref('/migrationsHistory')
    .orderByChild('migrationId')
    .equalTo(migrationId)
    .limitToLast(1)
    .once('value')
    .then(snap => {
      const lastQuery = snap.exists() ? values(snap.val())[0] : null

      let isValidRequest = true

      switch (requestedMigrationType) {
        case 'query':
          if (
            lastQuery &&
            !lastQuery.isIdempotent &&
            lastQuery.type === 'query'
          ) {
            // migration is done and is not idempotent
            isValidRequest = false
          }
          break

        case 'rollback':
          if (lastQuery && lastQuery.type === 'rollback') {
            // migration is rolled back already
            isValidRequest = false
          }
          break

        default:
          // not valid requested migration type
          isValidRequest = false
      }

      if (isValidRequest) {
        next()
      } else {
        res.status(403).json({
          code: 'migrations/invalid-request',
          message: "You can't run this migration.",
        })
      }
    })
}
