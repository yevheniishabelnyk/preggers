/**
 *
 * add-users-to-utc-offset-weekday-join-table query
 *
 */

module.exports = function() {
  const DB = require('src/services/DB')
  const PromisePool = require('es6-promise-pool')
  const getUTCOffsetWeekDay = require('src/utils/getUTCOffsetWeekDay')
  const merge = require('lodash/merge')
  const values = require('lodash/values')

  const MAX_CONCURRENT = 200

  return DB.get('/users').then(users => {
    const usersCollection = values(users)
    const databaseDiff = {}

    const pool = new PromisePool(
      () => createDatabaseDiffPromiseProducer(usersCollection, databaseDiff),
      MAX_CONCURRENT
    )

    return pool.start().then(() => databaseDiff)
  })

  function createDatabaseDiffPromiseProducer(users, databaseDiff) {
    if (!users.length) {
      return null
    }

    const user = users.pop()

    if (user.pregnancy && user.UTCOffset) {
      return DB.get(`pregnancies/${user.pregnancy}`).then(pregnancy => {
        const newUTCOffsetWeekday = getUTCOffsetWeekDay(
          pregnancy.dueDate,
          pregnancy.lengthType,
          user.UTCOffset
        )

        merge(databaseDiff, {
          joins: {
            usersByUTCOffsetWeekday: {
              [newUTCOffsetWeekday]: {
                [user.id]: true,
              },
            },

            UTCOffsetWeekdayByUser: {
              [user.id]: newUTCOffsetWeekday,
            },
          },
        })
      })
    } else {
      return Promise.resolve()
    }
  }
}
