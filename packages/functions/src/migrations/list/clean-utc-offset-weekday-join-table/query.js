/**
 *
 * clean-utc-offset-weekday-join-table query
 *
 */

module.exports = function() {
  return {
    joins: {
      UTCOffsetWeekdayByUser: null,

      usersByUTCOffsetWeekday: null,
    },
  }
}
