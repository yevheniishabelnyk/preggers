/**
 *
 * Create migration history ticket util
 *
 */

module.exports = function({ meta, type, migratedBy }) {
  const ServerTimestamp = require('src/models/shared/ServerTimestamp')
  const id = require('firebase-key').key()
  const appVersion = require('package.json').version

  return {
    id,
    type,
    migratedBy,
    appVersion,
    migratedAt: ServerTimestamp,

    migrationId: meta.id,
    description: meta.description,
    hasRollback: meta.hasRollback,
    isIdempotent: meta.isIdempotent,
  }
}
