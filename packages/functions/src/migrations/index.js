/**
 *
 * Migrations service
 *
 */

exports.run = function(migrationId, requestedMigrationType, requestUserEmail) {
  const fs = require('fs-extra')
  const path = require('path')

  const queryPath = path.join(
    __dirname,
    'list',
    migrationId,
    `${requestedMigrationType}.js`
  )

  const metaPath = path.join(__dirname, 'list', migrationId, 'meta.json')

  if (fs.existsSync(queryPath)) {
    const createDatabaseDiff = require(queryPath)

    return Promise.resolve(createDatabaseDiff())
      .then(diff => {
        const merge = require('lodash/merge')
        const DB = require('src/services/DB')
        const meta = require(metaPath)
        const createHistoryTicket = require('src/migrations/utils/createHistoryTicket')

        const historyTicket = createHistoryTicket({
          meta,
          type: requestedMigrationType,
          migratedBy: requestUserEmail,
        })

        const migrationsHistoryDiff = {
          migrationsHistory: {
            [historyTicket.id]: historyTicket,
          },
        }

        merge(diff, migrationsHistoryDiff)

        return DB.update(diff)
      })
      .catch(() => {
        return Promise.reject({
          status: 500,
          code: 'migrations/request-failed',
          message: 'Migration failed.',
        })
      })
  }

  return Promise.reject(`Missing query for ${migrationId}`)
}

exports.getMigrationsMeta = function() {
  const path = require('path')
  const fs = require('fs-extra')

  const migrationsDirPath = path.join(__dirname, 'list')

  let migrations = []

  if (fs.existsSync(migrationsDirPath)) {
    const isDirectory = source => fs.lstatSync(source).isDirectory()
    const getDirectories = source =>
      fs
        .readdirSync(source)
        .map(name => path.join(source, name))
        .filter(isDirectory)

    const migrationDirectories = getDirectories(migrationsDirPath)

    migrations = migrationDirectories
      .map(dirPath => {
        const migrationMetadataPath = path.join(dirPath, 'meta.json')

        if (fs.existsSync(migrationMetadataPath)) {
          return require(migrationMetadataPath)
        }

        return null
      })
      .filter(item => item !== null)
  }

  return migrations
}
