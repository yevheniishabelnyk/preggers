/**
 *
 * Migrations api
 *
 */

const functions = require('firebase-functions')
const cors = require('cors')
const express = require('express')
const validateFirebaseIdToken = require('src/middlewares/validateFirebaseIdToken')
const validateAdminRights = require('src/middlewares/validateAdminRights')
const validateMigrationStatus = require('src/migrations/middlewares/validateMigrationStatus')
const RuntimeConstants = require('src/constants/RuntimeConstants')
const app = express()

app.use(cors())
app.use(validateFirebaseIdToken)
app.use(validateAdminRights)
app.use('/:id', validateMigrationStatus)

let lastInvokationStartedAt

app.post('/:id', function(req, res, next) {
  if (
    !lastInvokationStartedAt ||
    Date.now() >
      lastInvokationStartedAt + RuntimeConstants.FUNCTION_EXECUTION_MAX_TIME
  ) {
    lastInvokationStartedAt = Date.now()

    const MigrationsService = require('src/migrations')

    const migrationId = req.params.id
    const requestUserEmail = req.user.email
    const { type: requestedMigrationType } = req.body

    MigrationsService.run(migrationId, requestedMigrationType, requestUserEmail)
      .then(() => {
        lastInvokationStartedAt = null

        res.status(200).send('Success!')

        next()
      })
      .catch(error => {
        console.error('[MIRGRATIONS] Error: ', error)

        lastInvokationStartedAt = null

        const status = error.status || 500

        res.status(status).json({ code: error.code, message: error.message })

        next()
      })
  } else {
    res.status(500).json({
      code: 'another-migration-is-currently-running',
      message: 'Another migration is currently running',
    })

    next()
  }
})

app.get('/', function(req, res, next) {
  const migrations = require('src/migrations').getMigrationsMeta()

  const appVersion = require('package.json').version

  migrations.forEach(item => (item.appVersion = appVersion))

  res.status(200).json({ migrations })

  next()
})

module.exports = functions.https.onRequest((req, res) => {
  if (!req.path) {
    // https://github.com/firebase/firebase-functions/issues/27

    req.url = `/${req.url}`
  }

  return app(req, res)
})
