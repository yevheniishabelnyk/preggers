# Migrations

## Description

Every migration should have a meta.json and a query.js file (rollback.js is optional). New migrations should be added in:

```
src/migrations/list
```

Methods relavant to migrations can be found in the MigrationsService

```
src/migrations/index.js
```

Methods relavant to firebase/database can be found in the DB service

```
src/services/DB
```

### Please have a look at and make use of these services when implementing a new migration.

## Testing

Execute in command line (make sure you are in `preggers/packages`)

```
npm run functions:start
```

Copy the url that the functions are run on and navigate to:

```
/preggers/packages/admin/.env.development

REACT_APP__API__URL="paste-your-local-url-here"
```

Execute in command line (make sure you are in `preggers/packages`)

```
npm run admin:start
```

Navigate to migrations page and you are ready.

## Meta.json example

The meta.json file has four requierd fields.

1.  id - String
2.  description - String
3.  hasRollback - Boolean
4.  isIdempotent - Boolean

```
{
  "id": "add-users-to-utc-offset-weekday-join-table",

  "description": "Calculates weekday of the first day of user pregnancy week, combines it with user UTC offset and writes result into two join tables with boolean values under the pathes /usersByUTCOffsetWeekday/{UTCOffset_weekday}/{userId} and /UTCOffsetWeekdayByUser/{userId}/{UTCOffset_weekday}. Does this for every user. Doesn't remove any data, only makes updates.",

  "hasRollback": false,

  "isIdempotent": true
}
```

## Query.js example

Every query should return a databaseDiffObject. The actual updating of the database is done in Migrations service's "run" function.

```
/**
 *
 * clean-utc-offset-weekday-join-table query
 *
 */

module.exports = function() {
 return {
    joins: {
      UTCOffsetWeekdayByUser: null,

      usersByUTCOffsetWeekday: null,
    },
  }
}
```

To see a more "complex" example visit this link:

```
https://bitbucket.org/strollerapp/preggers/commits/33c563e24a5aa4bb8bc7755c0cf83dc25d343ac6
```

## Rollback.js example

Every rollback should return a databaseDiffObject. The actual updating of the database is done in Migrations service "run" function.

```
We have not added any rollback yet.

Please insert example.
```
