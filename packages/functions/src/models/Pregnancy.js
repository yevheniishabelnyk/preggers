/**
 *
 * Pregnancy
 *
 */

const Model = require('objectmodel')
const moment = require('moment')

const ISODate = require('./shared/ISODate')
const Hour24Time = require('./shared/Hour24Time')
const ServerTimestamp = require('./shared/ServerTimestamp')
const Image = require('./shared/Image')
const Timestamped = require('./generic/Timestamped')

const Pregnancy = new Model.ObjectModel({
  id: String,
  dueDate: ISODate,
  childName: String,
  length: Number,
  status: String,
  createdBy: String,
  lengthType: String,
  birthday: [ISODate],
  birthPhoto: [Image],
  birthTime: [Hour24Time],
  lossDay: [ISODate],
  childGender: [String],
  child: [String],
})
  // .defaults({
  //   status: 'active',
  //   length: 280,
  // })
  .extend(Timestamped)

Pregnancy.create = function(props) {
  const defaults = {
    status: 'active',
    childName: 'My Baby',
    length: 280,
    createdAt: ServerTimestamp,
    updatedAt: ServerTimestamp,
    lengthType: '39+6',
  }

  if (!props.dueDate) {
    defaults.dueDate = moment()
      .add(279, 'days')
      .format('YYYY-MM-DD')
  }

  return new Pregnancy(Object.assign(defaults, props))
}

Pregnancy.prototype.isFirstWeekDay = function(UTCOffset) {
  const nowDate = moment()
    .utcOffset(Number(UTCOffset) * 60)
    .startOf('day')

  // console.info('isFirstWeekDay nowDate: ', nowDate.format('YYYY-MM-DD HH:mm'))

  const dueDateDate = moment(this.dueDate, 'YYYY-MM-DD').utcOffset(
    Number(UTCOffset) * 60
  )

  // console.info(
  //   'isFirstWeekDay dueDateDate: ',
  //   dueDateDate.format('YYYY-MM-DD HH:mm')
  // )

  const leftDays = Math.floor(
    moment.duration(dueDateDate.diff(nowDate)).asDays()
  )

  var passedDays = Math.abs(280 - leftDays)

  if (this.lengthType === '39+6') {
    passedDays = passedDays - 1 < 0 ? 0 : passedDays - 1
  }

  const currentWeekPassedDays = passedDays % 7

  // console.info('currentWeekPassedDays: ', currentWeekPassedDays)

  return currentWeekPassedDays === 0
}

Pregnancy.prototype.getCurrentWeek = function(UTCOffset) {
  var nowDate, dueDateDate

  if (UTCOffset) {
    nowDate = moment()
      .utcOffset(Number(UTCOffset) * 60)
      .startOf('day')

    // console.info('getCurrentWeek nowDate: ', nowDate.format('YYYY-MM-DD HH:mm'))

    dueDateDate = moment(this.dueDate, 'YYYY-MM-DD').utcOffset(
      Number(UTCOffset) * 60
    )

    // console.info(
    //   'getCurrentWeek dueDateDate: ',
    //   dueDateDate.format('YYYY-MM-DD HH:mm')
    // )
  } else {
    nowDate = moment().startOf('day')
    dueDateDate = moment(this.dueDate, 'YYYY-MM-DD')
  }

  var leftDays = Math.floor(moment.duration(dueDateDate.diff(nowDate)).asDays())

  var passedDays = Math.abs(280 - leftDays)

  var pregnancyWeekParametr

  switch (this.lengthType) {
    case '39+6':
      pregnancyWeekParametr = 1
      break

    case '40+0':
      pregnancyWeekParametr = 0
      break
    default:
      pregnancyWeekParametr = 1
  }

  return Math.floor((passedDays - pregnancyWeekParametr) / 7) + 1
}

module.exports = Pregnancy
