/**
 *
 * Image
 *
 */

const Model = require('objectmodel')

module.exports = new Model.ObjectModel({
  uri: String,
  type: String,
  preview: [String],
})
