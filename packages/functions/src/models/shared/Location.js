/**
 *
 * Location
 *
 */

const Model = require('objectmodel')

module.exports = new Model.ObjectModel({
  countryCode: String,
})
