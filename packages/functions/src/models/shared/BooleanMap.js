/**
 *
 * BooleanMap
 *
 */

const Model = require('objectmodel')

module.exports = Model.MapModel(String, Boolean)
