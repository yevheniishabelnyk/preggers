/**
 *
 * Server timestamp
 *
 */

const admin = require('firebase-admin')

module.exports = admin.database.ServerValue.TIMESTAMP
