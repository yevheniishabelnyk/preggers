/**
 *
 * Timestamped
 *
 */

const Model = require('objectmodel')
const ServerTimestamp = require('../shared/ServerTimestamp')

module.exports = new Model.ObjectModel({
  createdAt: [Number, ServerTimestamp],
  updatedAt: [Number, ServerTimestamp],
})
