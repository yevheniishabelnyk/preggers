/**
 *
 * on Contact Requests Create
 *
 */

const functions = require('firebase-functions')
const sendEmail = require('src/utils/Mandrill/sendEmail')
const config = require('config')

module.exports = functions.database
  .ref('/contactRequests/{id}')
  .onCreate(snap => {
    if (!config.MAILING__IS_ENABLED) {
      return Promise.reject(
        'Failed to send contact request email! Mailing is disabled in current environment.'
      )
    }

    const contactRequest = snap.val()

    return sendEmail({
      to: [
        {
          email: 'cs@preggers.life',
          name: '',
        },
      ],
      headers: {
        'Reply-To': contactRequest.email,
      },
      fromEmail: 'cs@preggers.life',
      fromName: contactRequest.userName,
      subject: `${contactRequest.subject} - ${contactRequest.title}`,
      text: `${contactRequest.message}

App: 'Preggers'
Version: ${contactRequest.appVersion}
User ID: ${contactRequest.userId}
Environment: ${config.database.environment}
      `,
    })
  })
