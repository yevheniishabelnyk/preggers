/**
 *
 * listeners entry point
 *
 */

module.exports = {
  users: require('./users'),
  pregnancies: require('./pregnancies'),
  contactRequests: require('./contactRequests'),
  auth: require('./auth'),
  content: require('./content'),
  userOwned: require('./userOwned'),
  derivedData: require('./derivedData'),
  competitions: require('./competitions'),
  queueJobsListener: require('src/services/Queue/listeners/jobsListener'),
}
