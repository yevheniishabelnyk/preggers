/**
 *
 * On Competition Update
 *
 */

const functions = require('firebase-functions')
const FirebaseAdmin = require('src/app').FirebaseAdmin
const flatten = require('src/utils/flatten')

module.exports = functions.database
  .ref('competitions/competitionList/{competitionId}/')
  .onUpdate(snap => {
    const { id, name } = snap.before._data

    const isActiveBefore = snap.before._data.isActive

    const isActiveAfter = snap.after._data.isActive

    const initialState = { id, name }

    if (!isActiveBefore && isActiveAfter) {
      console.info(
        `[COMPETITION ON UPDATE] ${name}'s Active status updated to ${isActiveAfter}`
      )

      return getActiveCompetition(initialState)
        .then(state => {
          return setCurrentActiveCompetitionStatusToFalse(state)
        })
        .then(state => {
          return updateDatabase(state)
        })
    }

    if (isActiveBefore && !isActiveAfter) {
      getActiveCompetition(initialState)
        .then(state => {
          if (state.activeCompetitionUid === id) {
            return updateDatabase(removeActiveCompetition(state))
          }
          return Promise.resolve()
        })
        .catch(err => {
          console.log('Error: ', err)
          return Promise.reject(err)
        })
    }

    return Promise.resolve()
  })

function getActiveCompetition(state, saveAs = 'activeCompetitionUid') {
  return FirebaseAdmin.database()
    .ref(`competitions/activeCompetitions`)
    .once('value')
    .then(snap => {
      const activeCompetition = snap.val()

      if (activeCompetition) {
        const activeCompetitionUid = activeCompetition.name

        state[saveAs] = activeCompetitionUid

        return state
      }

      return state
    })
}

function setCurrentActiveCompetitionStatusToFalse(
  state,
  saveAs = 'competitionToDeactivate'
) {
  if (state.activeCompetitionUid) {
    const updatedObj = {
      [state.activeCompetitionUid]: {
        isActive: false,
      },
    }

    state[saveAs] = updatedObj
  }

  return state
}

function removeActiveCompetition(state, saveAs = 'removeActiveCompetition') {
  const removeActiveCompetition = {
    activeCompetitions: {
      name: null,
    },
  }

  state[saveAs] = removeActiveCompetition
  return state
}

function updateDatabase(state) {
  let updatedObj = {}

  if (state.competitionToDeactivate) {
    Object.assign(updatedObj, {
      competitions: {
        competitionList: state.competitionToDeactivate,
        activeCompetitions: { name: state.id },
      },
    })
  }

  if (!state.activeCompetitionUid) {
    Object.assign(updatedObj, {
      competitions: {
        activeCompetitions: { name: state.id },
      },
    })
  }

  if (state.removeActiveCompetition) {
    Object.assign(updatedObj, { competitions: state.removeActiveCompetition })
  }

  const databaseDiff = flatten(updatedObj)

  return FirebaseAdmin.database()
    .ref()
    .update(databaseDiff)
    .then(() => {
      console.info(`[ACTIVE COMPETITION] updated to ${state.name} successfully`)
      return Promise.resolve()
    })
    .catch(err => {
      console.info('[ACTIVE COMPETITION] Error: ', err)
      return Promise.reject(err)
    })
}
