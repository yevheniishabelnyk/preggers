/**
 *
 * Firebase Auth on delete listener
 */

const functions = require('firebase-functions')

const FirebaseAdmin = require('src/app').FirebaseAdmin

module.exports = functions.auth.user().onDelete(user => {
  return FirebaseAdmin.database()
    .ref(`/users/${user.uid}`)
    .remove()
})
