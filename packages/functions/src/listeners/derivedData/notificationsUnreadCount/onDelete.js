/**
 *
 * On notification status change listener
 *
 */

const functions = require('firebase-functions')

module.exports = functions.database
  .ref('/derivedData/notificationUnreadCount/{userId}')
  .onDelete((snap, context) => {
    const counterRef = snap.ref

    const rootRef = counterRef.parent.parent.parent

    const userId = context.params.userId

    const collectionRef = rootRef
      .child('userOwned')
      .child('notifications')
      .child(userId)

    return rootRef
      .child('users')
      .child(userId)
      .once('value')
      .then(userSnap => {
        if (!userSnap.val()) {
          console.log(
            'User has been removed, exiting without updating notification count.'
          )

          return null
        }

        return collectionRef.once('value').then(notificationsSnap => {
          let unreadNotificationsCount = 0

          const notifications = notificationsSnap.val() || {}

          for (const key of Object.keys(notifications)) {
            const notification = notifications[key]

            if (notification.status === 'unread') {
              unreadNotificationsCount += 1
            }
          }

          return counterRef.set(unreadNotificationsCount)
        })
      })
  })
