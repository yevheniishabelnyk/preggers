/**
 *
 * derivedData listeners entry point
 *
 */

module.exports = {
  notificationsUnreadCount: require('./notificationsUnreadCount'),
}
