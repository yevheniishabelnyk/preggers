/**
 *
 * On tag delete listener
 *
 */

const functions = require('firebase-functions')
const has = require('lodash/has')

const FirebaseAdmin = require('src/app').FirebaseAdmin

const flatten = require('src/utils/flatten')
const getArticles = require('src/helpers/getArticles')
const getWeeks = require('src/helpers/getWeeks')

module.exports = functions.database
  .ref('/content/tags/{tagId}')
  .onDelete((snap, context) => {
    const tagId = context.params.tagId

    console.info('tagId: ', tagId)

    const initialState = { app: FirebaseAdmin, tagId }

    return getArticles(initialState)
      .then(getWeeks)
      .then(updateDatabase)
      .then(state => {
        console.log(`Tag ${state.tagId} was successfully removed from database`)

        return Promise.resolve()
      })
      .catch(function(err) {
        console.log('Removing tag error:', err)

        return Promise.resolve()
      })
  })

function updateDatabase(state) {
  const { tagId } = state

  if (state.articles || state.weeks) {
    const databaseDiffObj = {
      content: {
        articles: {},
        weeks: {},
      },
    }

    if (state.articles) {
      Object.keys(state.articles).forEach(articleId => {
        if (has(state.articles, `${articleId}.tags.${tagId}`)) {
          databaseDiffObj.content.articles[articleId] = {
            tags: {
              [tagId]: null,
            },
          }
        }
      })
    }

    if (state.weeks) {
      Object.keys(state.weeks).forEach(weekId => {
        if (has(state.weeks, `${weekId}.tags.${tagId}`)) {
          databaseDiffObj.content.weeks[weekId] = {
            tags: {
              [tagId]: null,
            },
          }
        }
      })
    }

    const databaseDiff = flatten(databaseDiffObj)

    console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

    return state.app
      .database()
      .ref()
      .update(databaseDiff)
      .then(() => {
        return state
      })
  }

  return state
}
