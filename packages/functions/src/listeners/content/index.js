/**
 *
 * content listeners entry point
 *
 */

module.exports = {
  tags: require('./tags'),
  articles: require('./articles'),
}
