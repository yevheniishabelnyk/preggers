/**
 *
 * Article on delete listener
 *
 */

const functions = require('firebase-functions')
const has = require('lodash/has')
const FirebaseAdmin = require('src/app').FirebaseAdmin

const flatten = require('src/utils/flatten')
const getWeeks = require('src/helpers/getWeeks')
const getArticleUserLikes = require('src/helpers/getArticleUserLikes')

const deleteObjectFromIndex = require('src/algolia/utils/deleteObjectFromIndex')
const AlgoliIndices = require('src/algolia/indices/list')

module.exports = functions.database
  .ref('/content/articles/{articleId}')
  .onDelete((snap, context) => {
    const articleId = context.params.articleId

    const article = snap.val()

    console.info('articleId: ', articleId)

    const initialState = { app: FirebaseAdmin, articleId }

    return getWeeks(initialState)
      .then(state => getArticleUserLikes(state, articleId))
      .then(updateDatabase)
      .then(state => {
        console.log(
          `Article ${state.articleId} was successfully removed from database`
        )

        return Promise.resolve()
      })
      .then(() => {
        console.log(
          `Article ${articleId} was successfully removed from Algolia Index`
        )

        return Promise.resolve()
      })
      .catch(function(err) {
        console.log('Removing article error:', err)

        return Promise.resolve()
      })
      .then(() =>
        deleteObjectFromIndex(
          AlgoliIndices.PREGGERS_KNOWLEDGE_ITEMS[article.language],
          articleId
        )
      )
  })

function updateDatabase(state) {
  const { articleId } = state

  const databaseDiffObj = {
    userLikesByArticle: {
      [articleId]: null,
    },
  }

  if (state.articleUserLikes) {
    databaseDiffObj.articleLikesByUser = Object.keys(
      state.articleUserLikes
    ).reduce((out, userId) => {
      out[userId] = {
        [articleId]: null,
      }

      return out
    }, {})
  }

  if (state.weeks) {
    databaseDiffObj.content = {
      weeks: {},
    }

    if (state.weeks) {
      Object.keys(state.weeks).forEach(weekId => {
        if (has(state.weeks, `${weekId}.articles.${articleId}`)) {
          databaseDiffObj.content.weeks[weekId] = {
            articles: {
              [articleId]: null,
            },
          }
        }
      })
    }
  }

  const databaseDiff = flatten(databaseDiffObj)

  console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

  return state.app
    .database()
    .ref()
    .update(databaseDiff)
    .then(() => {
      return state
    })
}
