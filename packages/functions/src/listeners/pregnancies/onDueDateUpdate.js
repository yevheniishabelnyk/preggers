/**
 *
 * On pregnancy update listener
 *
 */

const functions = require('firebase-functions')
const moment = require('moment')

const FirebaseAdmin = require('src/app').FirebaseAdmin

const getUsersByPregnancyId = require('src/helpers/getUsersByPregnancyId')
const getUserMailingSettings = require('src/helpers/getUserMailingSettings')
const sendWelcomeEmail = require('src/mailing/sendWelcomeEmail')
const Pregnancy = require('src/models/Pregnancy')

const config = require('config')

module.exports = functions.database
  .ref('/pregnancies/{pregnancyId}')
  .onWrite(change => {
    if (!config.MAILING__IS_ENABLED) {
      return Promise.reject('Mailing is desabled with config.')
    }

    if (!change.before.exists() || !change.after.exists()) {
      return null
    }

    const oldData = change.before.val()
    const newData = change.after.val()

    if (oldData.dueDate !== newData.dueDate) {
      const oldPregnancy = new Pregnancy(oldData)
      const newPregnancy = new Pregnancy(newData)

      if (oldPregnancy.getCurrentWeek() !== newPregnancy.getCurrentWeek()) {
        return FirebaseAdmin.database()
          .ref(`/users/${newPregnancy.createdBy}/UTCOffset`)
          .once('value')
          .then(snap => {
            if (snap.exists()) {
              const userUTCOffset = snap.val()

              if (!newPregnancy.isFirstWeekDay(userUTCOffset)) {
                console.log(
                  'It is not first day in the week and user cant recieve welcome new week email later today'
                )
                // It is not first day in the week and user can't recieve welcome new week email later today
                // Sending it now
                return sendWelcomeNewWeekEmailToPregnancyUsers(
                  newPregnancy,
                  userUTCOffset
                )
              }

              const nowUserHours = moment()
                .utcOffset(Number(userUTCOffset) * 60)
                .hours()

              if (nowUserHours >= config.MAILING__SENDING_HOURS) {
                console.log(
                  'User will not receive Welcome New Week email later today'
                )
                // User will not receive Welcome New Week email later today
                // Sending it now

                return sendWelcomeNewWeekEmailToPregnancyUsers(
                  newPregnancy,
                  userUTCOffset
                )
              }

              return Promise.resolve()
            }

            return Promise.reject("User doesn't have UTC offset in profile")
          })
      }
    }

    return null
  })

function sendWelcomeNewWeekEmailToPregnancyUsers(pregnancy) {
  const initialState = { app: FirebaseAdmin }

  return getUsersByPregnancyId(initialState, pregnancy.id)
    .then(state =>
      Promise.all(
        state.userIds.map(userId =>
          getUserMailingSettings({ state, userId })
            .then(state => {
              if (!state.isWeeklyEmailsEnabled) {
                console.log(`User ${userId} disabled mailing`)

                return Promise.reject({
                  status: 500,
                  message: `User ${userId} disabled mailing`,
                })
              }

              return state
            })
            .then(() =>
              sendWelcomeEmail({
                userId,
                isNewWeek: true,
                cache: {
                  pregnancies: {
                    [pregnancy.id]: pregnancy,
                  },
                },
              })
            )
            .catch(err => {
              console.info('err: ', err)
            })
        )
      )
    )
    .then(() => {
      console.log('New week welcome emails have been sent.')

      return Promise.resolve()
    })
    .catch(function(err) {
      console.log('Sending new week welcome emails error:', err)

      return Promise.resolve()
    })
}
