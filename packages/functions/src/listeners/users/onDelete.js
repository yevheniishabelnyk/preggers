/**
 *
 * /users on delete
 *
 */

const functions = require('firebase-functions')

const FirebaseAdmin = require('src/app').FirebaseAdmin

const flatten = require('src/utils/flatten')
const getConnectionById = require('src/helpers/getConnectionById')
const getUserChildrenIds = require('src/helpers/getUserChildrenIds')
const getUserPregnancies = require('src/helpers/getUserPregnancies')
const getUserBabynameLikes = require('src/helpers/getUserBabynameLikes')
const getUserArticleLikes = require('src/helpers/getUserArticleLikes')
const getUserFiles = require('src/helpers/getUserFiles')

module.exports = functions.database.ref('/users/{userId}').onDelete(snap => {
  const user = snap.val()

  console.info('user: ', user)

  const initialState = { app: FirebaseAdmin, user }

  return getUserChildrenIds(
    initialState,
    initialState.user.id,
    'userChildrenIds'
  )
    .then(state => getUserPregnancies(state, state.user.id, 'userPregnancies'))
    .then(state => {
      if (state.user.connection) {
        return getConnectionById(state, state.user.connection, 'userConnection')
      }

      return state
    })
    .then(state => getUserFiles(state, state.user.id))
    .then(
      state =>
        new Promise(resolve => {
          getUserBabynameLikes(state, state.user.id)
            .then(resolve)
            .catch(() => {
              resolve(state)
            })
        })
    )
    .then(
      state =>
        new Promise(resolve => {
          getUserArticleLikes(state, state.user.id)
            .then(resolve)
            .catch(() => {
              resolve(state)
            })
        })
    )
    .then(updateDatabase)
    .then(removeUserFilesFromStorage)
    .then(state => {
      console.log(
        `User with ID ${
          state.user.id
        } specific data was successfully removed from database`
      )

      return Promise.resolve()
    })
    .catch(function(err) {
      console.log('Error:', err)

      return Promise.resolve()
    })
})

function updateDatabase(state) {
  const databaseDiffObj = {
    babynameLikesByUser: {
      [state.user.id]: null,
    },

    articleLikesByUser: {
      [state.user.id]: null,
    },

    userOwned: {
      tokens: {
        [state.user.id]: null,
      },

      invitations: {
        [state.user.id]: null,
      },

      babyNames: {
        [state.user.id]: null,
      },

      settings: {
        [state.user.id]: null,
      },

      files: {
        [state.user.id]: null,
      },

      notifications: {
        [state.user.id]: null,
      },
    },

    childrenByUser: {
      [state.user.id]: null,
    },

    pregnanciesByUser: {
      [state.user.id]: null,
    },
  }

  if (state.user.UTCOffset) {
    databaseDiffObj.usersByUTCOffset = {
      [state.user.UTCOffset]: {
        [state.user.id]: null,
      },
    }
  }

  const rankRefs = []

  if (state.userBabynameLikes) {
    databaseDiffObj.countent = {
      countryBabyNames: Object.keys(state.userBabynameLikes).reduce(
        (out, babynameId) => {
          out[babynameId] = {
            stars: {
              [state.user.id]: null,
            },
          }

          const rankRef = state.app
            .database()
            .ref(`/content/countryBabyNames/${babynameId}/rank`)

          rankRefs.push(rankRef)

          return out
        },
        {}
      ),
    }
  }

  if (state.userArticleLikes) {
    databaseDiffObj.userLikesByArticle = Object.keys(
      state.userArticleLikes
    ).reduce((out, articleId) => {
      out[articleId] = {
        [state.user.id]: null,
      }

      const rankRef = state.app
        .database()
        .ref(`/content/articles/${articleId}/stars`)

      rankRefs.push(rankRef)

      return out
    }, {})
  }

  if (state.userConnection) {
    const connectedUserId =
      state.userConnection.toId === state.user.id
        ? state.userConnection.fromId
        : state.userConnection.toId

    Object.assign(databaseDiffObj, {
      connections: {
        [state.userConnection.id]: {
          status: 'canceled',
        },
      },

      users: {
        [connectedUserId]: {
          connection: null,
          partner: null,
        },
      },
    })
  }

  databaseDiffObj.usersByChild = {}

  state.userChildrenIds.forEach(childId => {
    Object.assign(databaseDiffObj.usersByChild, {
      [childId]: {
        [state.user.id]: null,
      },
    })
  })

  if (state.userPregnancies.length) {
    databaseDiffObj.pregnancies = {}
    databaseDiffObj.usersByPregnancy = {}

    state.userPregnancies.forEach(pregnancy => {
      if (pregnancy.createdBy === state.user.id) {
        databaseDiffObj.pregnancies[pregnancy.id] = null
      }

      Object.assign(databaseDiffObj.usersByPregnancy, {
        [pregnancy.id]: null,
      })
    })
  }

  databaseDiffObj.children = {}

  state.userChildrenIds.forEach(childId => {
    databaseDiffObj.children[childId] = null
  })

  const databaseDiff = flatten(databaseDiffObj)

  console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

  return state.app
    .database()
    .ref()
    .update(databaseDiff)
    .then(() => {
      if (rankRefs.length) {
        return Promise.all(
          rankRefs.map(ref =>
            ref.transaction(function(currentRank) {
              return currentRank - 1
            })
          )
        ).then(() => state)
      }

      return state
    })
}

function removeUserFilesFromStorage(state) {
  console.info('state.userFiles: ', state.userFiles)

  if (state.userFiles) {
    return Promise.all(
      state.userFiles.map(storagePath =>
        state.app
          .storage()
          .bucket()
          .file(storagePath)
          .delete()
      )
    ).then(() => state)
  }

  return state
}
