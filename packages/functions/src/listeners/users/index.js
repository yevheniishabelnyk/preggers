/**
 *
 * /users listeners
 *
 */

module.exports = {
  onDelete: require('./onDelete'),
  onCreate: require('./onCreate'),
}
