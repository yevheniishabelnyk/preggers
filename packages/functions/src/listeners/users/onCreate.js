/**
 *
 * on Users Create
 *
 */

const functions = require('firebase-functions')

const FirebaseAdmin = require('src/app').FirebaseAdmin
const values = require('lodash/values')
const compact = require('lodash/compact')
const flatten = require('src/utils/flatten')

module.exports = functions.database.ref('/users/{userId}').onCreate(snap => {
  const user = snap.val()

  console.info('[USERS ON CREATE] New user added: ', user)

  return FirebaseAdmin.database()
    .ref('/invitations')
    .orderByChild('toEmail')
    .equalTo(user.email)
    .once('value')
    .then(snap => {
      if (snap.exists()) {
        const userInvitations = compact(values(snap.val()))

        if (userInvitations.length) {
          const databaseDiff = getDatabaseDiffForUserInvitations(
            userInvitations,
            user
          )

          return FirebaseAdmin.database()
            .ref()
            .update(databaseDiff)
        }
      }

      return Promise.resolve()
    })
})

function getDatabaseDiffForUserInvitations(invitations, toUser) {
  console.log('[USERS ON CREATE] getDatabaseDiffForUserInvitations')

  const databaseDiffObj = {
    invitations: invitations.reduce((out, invitation) => {
      out[invitation.id] = {
        toId: toUser.id,
        toName: toUser.name || null,
        toPhoto: toUser.image ? toUser.image.uri : null,
      }

      return out
    }, {}),

    userOwned: {
      invitations: {
        [toUser.id]: invitations.reduce((out, invitation) => {
          out[invitation.id] = invitation.status

          return out
        }, {}),
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  console.info(
    '[USERS ON CREATE] diff: ',
    JSON.stringify(databaseDiff, null, 4)
  )

  return databaseDiff
}
