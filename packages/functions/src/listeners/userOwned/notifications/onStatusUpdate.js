/**
 *
 * On notification status change listener
 *
 */

const functions = require('firebase-functions')

module.exports = functions.database
  .ref('/userOwned/notifications/{userId}/{notificationId}/status')
  .onWrite((change, context) => {
    const userId = context.params.userId

    const collectionRef = change.after.ref.parent.parent

    const countRef = collectionRef.parent.parent.parent
      .child('derivedData')
      .child('notificationUnreadCount')
      .child(userId)

    let increment

    if (change.after.exists() && change.after.val() === 'unread') {
      increment = 1
    } else if (change.after.exists() && change.after.val() !== 'unread') {
      increment = -1
    } else if (!change.after.exists() && change.before.exists()) {
      increment = -1
    } else {
      console.log('Exiting without updating count')

      return null
    }

    console.log('Incrementing count with ', increment)

    return countRef.transaction(current => {
      return (current || 0) + increment
    })
  })
