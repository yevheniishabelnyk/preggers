/**
 *
 * Cloud functions entry
 *
 */

// Register __dirname as path aliase excluding node_modules
require('app-module-path/register')

// APIs
exports.twitter = require('./src/api/twitter')
exports.connect = require('./src/api/connect')
exports.mailing = require('./src/api/mailing')
exports.contentService = require('./src/api/privat/content-service')
exports.public = require('./src/api/public')
exports.jobs = require('./src/services/Queue/api/jobs')

// Privat APIs
exports.algolia = require('./src/api/privat/algolia')
exports.migrations = require('./src/migrations/api')

// Uncomment this if you need api to upload images to Firebase Storage
// exports.image = require('./src/api/image')

// Listeners
exports.listeners = require('./src/listeners')

// UTCOffsetWeekday join table listeners
exports.UTCOffsetWeekdayListeners = require('./src/joins/UTCOffsetWeekday/listeners')

// Slack actions endpoint
exports.slack = require('./src/slack')

// Algolia update indexes listeners
exports.cronJobs = require('./src/cron-jobs')
