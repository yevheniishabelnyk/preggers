import React from 'react'
import PropTypes from 'prop-types'
import FlatButton from 'material-ui/FlatButton'
import ContentTextFormat from 'material-ui/svg-icons/content/text-format'
import translate from 'admin-on-rest/lib/i18n/translate'

const ArticleButton = ({ label = '', style, onClick }) => (
  <FlatButton
    primary
    label={label && translate(label)}
    icon={<ContentTextFormat />}
    style={style}
    onClick={onClick}
  />
)

ArticleButton.propTypes = {
  label: PropTypes.string,
  style: PropTypes.object,
  translate: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default translate(ArticleButton)
