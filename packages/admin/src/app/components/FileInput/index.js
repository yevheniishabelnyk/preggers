import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { shallowEqual } from 'recompose'
import Dropzone from 'react-dropzone'

import FileInputPreview from 'admin-on-rest/lib/mui/input/FileInputPreview'
import translate from 'admin-on-rest/lib/i18n/translate'
import scaleImage from 'utils/scaleImage'

const defaultStyle = {
  dropZone: {
    background: '#efefef',
    cursor: 'pointer',
    padding: '1rem',
    textAlign: 'center',
    color: '#999',
  },
  preview: {
    float: 'left',
  },
}

export class FileInput extends Component {
  static propTypes = {
    accept: PropTypes.string,
    children: PropTypes.element,
    disableClick: PropTypes.bool,
    withPreview: PropTypes.bool,
    elStyle: PropTypes.object,
    input: PropTypes.object,
    itemStyle: PropTypes.object,
    labelMultiple: PropTypes.string,
    labelSingle: PropTypes.string,
    maxSize: PropTypes.number,
    minSize: PropTypes.number,
    multiple: PropTypes.bool,
    removeStyle: PropTypes.object,
    style: PropTypes.object,
    translate: PropTypes.func.isRequired,
    placeholder: PropTypes.node,
  }

  static defaultProps = {
    addLabel: true,
    addField: true,
    itemStyle: {},
    labelMultiple: 'aor.input.file.upload_several',
    labelSingle: 'aor.input.file.upload_single',
    multiple: false,
    withPreview: true,
    onUpload: () => {},
    removeStyle: { display: 'inline-block' },
  }

  constructor(props) {
    super(props)

    const image = props.input.value

    let files = []

    if (image) {
      files = [
        { uri: image.uri instanceof File ? image.uri.preview : image.uri },
      ]
    }

    this.state = {
      files: files.map(this.transformFile),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.input.value &&
      this.props.input.value !== nextProps.input.value
    ) {
      const image = nextProps.input.value

      const files = [
        { uri: image.uri instanceof File ? image.uri.preview : image.uri },
      ]

      this.setState({ files: files.map(this.transformFile) })
    }
  }

  onDrop = files => {
    const updatedFiles = this.props.multiple
      ? [...this.state.files, ...files.map(this.transformFile)]
      : [...files.map(this.transformFile)]

    this.setState({ files: updatedFiles })

    if (updatedFiles[0] && updatedFiles[0].rawFile) {
      const url = updatedFiles[0].rawFile.preview

      if (
        /^image/.test(updatedFiles[0].rawFile.type) &&
        this.props.withPreview
      ) {
        scaleImage(url, 10, 10)
          .then(base64 => {
            this.props.input.onChange({
              type: 'file',
              uri: updatedFiles[0].rawFile,
              preview: base64,
            })
          })
          .catch(err => {
            console.info('err: ', err)

            this.props.input.onChange({
              type: 'file',
              uri: updatedFiles[0].rawFile,
            })
          })
      } else {
        this.props.input.onChange({
          type: 'file',
          uri: updatedFiles[0].rawFile,
        })
      }
    }
  }

  onRemove = file => () => {
    const filteredFiles = this.state.files.filter(
      stateFile => !shallowEqual(stateFile, file)
    )

    this.setState({ files: filteredFiles })

    if (filteredFiles.length === 0) {
      this.props.input.onChange(null)
    } else {
      this.props.input.onChange({
        ...this.props.input.value,
        uri: filteredFiles[0].rawFile,
      })
    }
  }

  // turn a browser dropped file structure into expected structure
  transformFile = file => {
    if (!(file instanceof File)) {
      return file
    }

    const { source, title } = React.Children.toArray(
      this.props.children
    )[0].props

    const transformedFile = {
      rawFile: file,
      [source]: file.preview,
    }

    if (title) {
      transformedFile[title] = file.name
    }

    return transformedFile
  }

  label() {
    const { translate, placeholder, labelMultiple, labelSingle } = this.props

    if (placeholder) {
      return placeholder
    }

    if (this.props.multiple) {
      return <p>{translate(labelMultiple)}</p>
    }

    return <p>{translate(labelSingle)}</p>
  }

  render() {
    const {
      accept,
      children,
      disableClick,
      elStyle,
      itemStyle,
      maxSize,
      minSize,
      multiple,
      style,
      removeStyle,
    } = this.props

    const finalStyle = {
      ...defaultStyle,
      ...style,
    }

    return (
      <div style={elStyle}>
        <Dropzone
          onDrop={this.onDrop}
          accept={accept}
          disableClick={disableClick}
          maxSize={maxSize}
          minSize={minSize}
          multiple={multiple}
          style={finalStyle.dropZone}
        >
          {this.label()}
        </Dropzone>

        {children && (
          <div className="previews">
            {this.state.files.map((file, index) => (
              <FileInputPreview
                key={index}
                file={file}
                itemStyle={itemStyle}
                onRemove={this.onRemove(file)}
                removeStyle={removeStyle}
              >
                {React.cloneElement(children, {
                  record: file,
                  style: defaultStyle.preview,
                })}
              </FileInputPreview>
            ))}
          </div>
        )}
      </div>
    )
  }
}

export default translate(FileInput)
