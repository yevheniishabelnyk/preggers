/**
 *
 * Menu
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { MenuItemLink, DashboardMenuItem } from 'admin-on-rest'
import { getResources } from 'admin-on-rest/lib/reducer'

import Divider from 'material-ui/Divider'

import inflection from 'inflection'
import compose from 'recompose/compose'
import translate from 'admin-on-rest/lib/i18n/translate'

import AboutUsIcon from 'material-ui/svg-icons/social/mood'
import BuildIcon from 'material-ui/svg-icons/action/build'
import RatingDialogSettingsIcon from 'material-ui/svg-icons/action/grade'
import UsersListIcon from 'material-ui/svg-icons/action/assignment-ind'
import GeneralTermsIcon from 'material-ui/svg-icons/notification/event-note'
import PrivacyPolicyIcon from 'material-ui/svg-icons/action/feedback'
import AlgoliaIcon from 'material-ui/svg-icons/device/access-time'
import EmailIcon from 'material-ui/svg-icons/communication/email'
import EmailOutlineIcon from 'material-ui/svg-icons/communication/mail-outline'

const styles = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    height: '100%',
  },

  logoutWrapper: {
    marginTop: 'auto',
    paddingTop: 100,
  },

  menuItem: {
    paddingLeft: 58,
  },

  subMenu: {
    paddingLeft: 0,
    height: 36,
    minHeight: 36,
    lineHeight: '36px',
    fontSize: 13,
  },

  subsubMenu: {
    paddingLeft: 15,
    height: 36,
    minHeight: 36,
    lineHeight: '36px',
    fontSize: 13,
  },
}

const translatedResourceName = (resource, translate) => {
  if (!resource) {
    return ''
  }

  return translate(`resources.${resource.name}.name`, {
    smart_count: 2,
    _:
      resource.options && resource.options.label
        ? translate(resource.options.label, {
            smart_count: 2,
            _: resource.options.label,
          })
        : inflection.humanize(inflection.pluralize(resource.name)),
  })
}

function getCountryLink(countryCode) {
  return encodeURI(
    `/content/countryBabyNames?filter={"countryCode":["${countryCode}"]}&order=DESC&page=1&perPage=10&sort=id`
  )
}

const UKBabyNamesLink = getCountryLink('UK')
const USABabyNamesLink = getCountryLink('US')
const SwedenBabyNamesLink = getCountryLink('SE')
const NorwayBabyNamesLink = getCountryLink('NO')
const DenmarkBabyNamesLink = getCountryLink('DK')
const FinlandBabyNamesLink = getCountryLink('FI')

const Menu = ({ hasDashboard, onMenuTap, resources, translate, logout }) => {
  const foods = resources.filter(({ name }) => name === 'content/foods')[0]
  const babyNames = resources.filter(
    ({ name }) => name === 'content/countryBabyNames'
  )[0]

  return (
    <div style={styles.main}>
      {hasDashboard && <DashboardMenuItem onClick={onMenuTap} />}

      {resources
        .filter(
          r =>
            r.list &&
            r.name !== 'content/foods' &&
            r.name !== 'content/foodCategories' &&
            r.name !== 'content/countryBabyNames'
        )
        .map(resource => (
          <MenuItemLink
            key={resource.name}
            to={`/${resource.name}`}
            primaryText={translatedResourceName(resource, translate)}
            leftIcon={<resource.icon />}
            onClick={onMenuTap}
          />
        ))}

      <Divider />

      <MenuItemLink
        to="/content/about-us"
        primaryText="About Us"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<AboutUsIcon />}
      />

      <MenuItemLink
        to="/migrations"
        primaryText="Migrations"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<BuildIcon />}
      />

      <MenuItemLink
        to="/users-list"
        primaryText="Users List"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<UsersListIcon />}
      />

      <MenuItemLink
        to="/appVersion"
        primaryText="App Version"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<BuildIcon />}
      />

      <MenuItemLink
        to="/content/general-terms"
        primaryText="General Terms"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<GeneralTermsIcon />}
      />

      <MenuItemLink
        to="/content/privacy-policy"
        primaryText="Privacy Policy"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<PrivacyPolicyIcon />}
      />

      <MenuItemLink
        to="/rating-dialog-settings"
        primaryText="Rating Dialog Settings"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<RatingDialogSettingsIcon />}
      />

      <MenuItemLink
        to="/algolia"
        primaryText="Algolia Indices"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<AlgoliaIcon />}
      />

      <MenuItemLink
        to="/mailing/welcomeNewUser"
        primaryText="Welcome New User"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<EmailIcon />}
      />

      <MenuItemLink
        to="/mailing/invite"
        primaryText="Invite"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<EmailIcon />}
      />

      <MenuItemLink
        to="/mailing/testing"
        primaryText="Mailing Sandbox"
        onClick={onMenuTap}
        insetChildren
        leftIcon={<EmailOutlineIcon />}
      />

      <Divider />

      {foods ? (
        <div>
          <MenuItemLink
            to="/content/foods"
            primaryText={translatedResourceName(foods, translate)}
            leftIcon={<foods.icon />}
            onClick={onMenuTap}
            innerDivStyle={styles.menuItem}
          />

          <MenuItemLink
            to="/content/foods"
            primaryText="Foods"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />

          <MenuItemLink
            to="/content/foodCategories"
            primaryText="Food Categories"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />

          <MenuItemLink
            to="/content/foods-general-recommendations"
            primaryText="General Recommendations"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />
        </div>
      ) : null}

      <Divider />

      {babyNames ? (
        <div>
          <MenuItemLink
            to="/content/countryBabyNames"
            primaryText="Baby Names"
            leftIcon={<babyNames.icon />}
            onClick={onMenuTap}
            innerDivStyle={styles.menuItem}
          />

          <MenuItemLink
            to={USABabyNamesLink}
            primaryText="USA"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />

          <MenuItemLink
            to={UKBabyNamesLink}
            primaryText="UK"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />

          <MenuItemLink
            to={SwedenBabyNamesLink}
            primaryText="Sweden"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />

          <MenuItemLink
            to={NorwayBabyNamesLink}
            primaryText="Norway"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />
          <MenuItemLink
            to={DenmarkBabyNamesLink}
            primaryText="Denmark"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />
          <MenuItemLink
            to={FinlandBabyNamesLink}
            primaryText="Finland"
            onClick={onMenuTap}
            style={styles.subMenu}
            insetChildren
          />
        </div>
      ) : null}

      <Divider />

      <div style={styles.logoutWrapper}>{logout}</div>
    </div>
  )
}

Menu.propTypes = {
  hasDashboard: PropTypes.bool,
  logout: PropTypes.element,
  onMenuTap: PropTypes.func,
  resources: PropTypes.array.isRequired,
  translate: PropTypes.func.isRequired,
}

Menu.defaultProps = {
  onMenuTap: () => null,
}

const mapStateToProps = state => ({
  resources: getResources(state),
})

const enhance = compose(
  translate,
  connect(mapStateToProps)
)

export default enhance(Menu)
