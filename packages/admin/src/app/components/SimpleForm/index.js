/**
 *
 *
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { reduxForm } from 'redux-form'
import compose from 'recompose/compose'

import Toolbar from 'admin-on-rest/lib/mui/form/Toolbar'
import getDefaultValues from 'admin-on-rest/lib/mui/form/getDefaultValues'
import FormInput from 'admin-on-rest/lib/mui/form/FormInput'

import IStringTabs from 'app/components/IStringTabs'
import { APP_LOCALES } from 'config'

const formStyle = { padding: '0 1em 3em 1em' }

class SimpleForm extends React.Component {
  handleSubmitWithRedirect = (redirect = this.props.redirect) =>
    this.props.handleSubmit(values => this.props.save(values, redirect))

  render() {
    const {
      basePath,
      children,
      invalid,
      record,
      resource,
      submitOnEnter,
      toolbar,
      version,
    } = this.props

    return (
      <form className="simple-form">
        <div style={formStyle} key={version}>
          {React.Children.map(children, input => {
            if (!input) {
              return null
            }

            if (input.props.localized) {
              return (
                <IStringTabs
                  disableGoogleTranslate={input.props.disableGoogleTranslate}
                >
                  {APP_LOCALES.map(locale => {
                    const localizedInput = React.cloneElement(input, {
                      source: `${input.props.source}.${locale}`,
                      choices:
                        input.props.choices && input.props.choiceLocaleFilter
                          ? input.props.choices.filter(item =>
                              input.props.choiceLocaleFilter(item, locale)
                            )
                          : input.props.choices,
                    })

                    return (
                      <FormInput
                        key={locale}
                        basePath={basePath}
                        input={localizedInput}
                        locale={locale}
                        record={record}
                        resource={resource}
                      />
                    )
                  })}
                </IStringTabs>
              )
            }

            if (input.props.nestedForm) {
              return React.cloneElement(input, {
                basePath,
                record,
                resource,
                appLocales: APP_LOCALES,
                dispatch: this.props.dispatch,
              })
            }

            return (
              <FormInput
                basePath={basePath}
                input={input}
                record={record}
                resource={resource}
              />
            )
          })}
        </div>

        {toolbar &&
          React.cloneElement(toolbar, {
            handleSubmitWithRedirect: this.handleSubmitWithRedirect,
            invalid,
            submitOnEnter,
          })}
      </form>
    )
  }
}

SimpleForm.propTypes = {
  dispatch: PropTypes.func,
  basePath: PropTypes.string,
  children: PropTypes.node,
  defaultValue: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  handleSubmit: PropTypes.func, // passed by redux-form
  invalid: PropTypes.bool,
  record: PropTypes.object,
  resource: PropTypes.string,
  redirect: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  save: PropTypes.func, // the handler defined in the parent, which triggers the REST submission
  submitOnEnter: PropTypes.bool,
  toolbar: PropTypes.element,
  validate: PropTypes.func,
  version: PropTypes.number,
}

SimpleForm.defaultProps = {
  submitOnEnter: true,
  toolbar: <Toolbar />,
}

const enhance = compose(
  connect((state, props) => ({
    initialValues: getDefaultValues(state, props),
  })),
  reduxForm({
    form: 'record-form',
    enableReinitialize: true,
  })
)

export default enhance(SimpleForm)
