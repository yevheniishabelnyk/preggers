import React from 'react'
import PropTypes from 'prop-types'

import pure from 'recompose/pure'

const NameField = ({ record = {} }) => (
  <div style={{ display: 'inline-flex', alignItems: 'center' }}>
    <div
      style={{
        display: 'inline-block',
        width: 25,
        height: 25,
        borderRadius: 12.5,
        overflow: 'hidden',
        backgroundColor: 'darkgray',
        marginRight: 8,
        marginTop: 4,
      }}
    >
      {record.image ? (
        <img
          src={record.image.uri}
          style={{
            width: 25,
            height: 25,
            borderRadius: 12.5,
            backgroundSize: 'contain',
          }}
          alt=""
        />
      ) : null}
    </div>
    <div style={{ marginTop: 3 }}>{record.name}</div>
  </div>
)

const PureNameField = pure(NameField)

PureNameField.defaultProps = {
  source: 'name',
  label: 'Name',
}

NameField.propTypes = {
  record: PropTypes.object,
}

export default PureNameField
