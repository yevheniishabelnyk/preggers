/**
 *
 * Simulator
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import './style.css'

export default class Simulator extends React.Component {
  static propTypes = {
    isVisible: PropTypes.bool,
    article: PropTypes.string,
  }

  state = {
    isVisible: false,
    article: '<p></p>',
  }

  render() {
    const { isVisible, article } = this.props

    return (
      <div
        className={`iphone-simulator ${
          isVisible ? 'iphone-simulator--isVisible' : ''
        }`}
      >
        <div className="ql-editor">
          <div dangerouslySetInnerHTML={{ __html: article }} />
        </div>
      </div>
    )
  }
}
