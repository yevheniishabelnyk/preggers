import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import FlatButton from 'material-ui/FlatButton'
import ActionDelete from 'material-ui/svg-icons/action/delete'
import linkToRecord from 'admin-on-rest/lib/util/linkToRecord'
import translate from 'admin-on-rest/lib/i18n/translate'

const DeleteButton = ({
  basePath = '',
  label = 'aor.action.delete',
  record = {},
  translate,
  style,
}) => (
  <FlatButton
    secondary
    label={label && translate(label)}
    icon={<ActionDelete />}
    containerElement={
      <Link to={`${linkToRecord(basePath, record.id)}/delete`} />
    }
    style={style}
  />
)

DeleteButton.propTypes = {
  basePath: PropTypes.string,
  label: PropTypes.string,
  record: PropTypes.object,
  style: PropTypes.object,
  translate: PropTypes.func.isRequired,
}

export default translate(DeleteButton)
