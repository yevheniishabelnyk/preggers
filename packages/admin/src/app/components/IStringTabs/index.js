/**
 *
 * IStringTabs
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Tabs, Tab } from 'material-ui/Tabs'

import countryByLocale from './countryByLocale'

import { get } from 'lodash'

import translate from './translate'

import './styles.css'

import { APP_LOCALES } from 'config'

let languages = require('@cospired/i18n-iso-languages')
languages.registerLocale(require('@cospired/i18n-iso-languages/langs/en.json'))

class IStringTabs extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    formValues: PropTypes.any,
    dispatch: PropTypes.func,
    disableGoogleTranslate: PropTypes.bool,
  }

  state = {
    value: 0,
  }

  render() {
    const { children } = this.props

    return (
      <div className="istring-tabs-wrapper">
        <Tabs
          value={this.state.value}
          onChange={this._handleTabChange}
          style={{
            marginBottom: '30px',
          }}
          className={`istring-tabs-${children.length}`}
          inkBarStyle={{
            width: '80px',
            backgroundColor: 'rgb(0, 188, 212)',
          }}
          tabItemContainerStyle={{
            maxWidth: children.length * 80 + 'px',
            backgroundColor: 'white',
            borderBottom: '1px solid rgb(224, 224, 224)',
          }}
        >
          {React.Children.map(children, (child, index) => {
            const isActive = index === this.state.value

            if (
              !this.sourceDefaultLocaleKey &&
              child.props.locale === process.env.REACT_APP__DEFAULT_LOCALE
            ) {
              this.sourceDefaultLocaleKey = child.props.input.props.source
            }

            const icon = this._getLocaleIcon(child.props.locale, isActive)

            let label

            if (!icon) {
              label = languages.getName(child.props.locale, 'en')
            }

            return (
              <Tab
                key={child.props.locale}
                value={index}
                className="form-tab"
                style={{ width: '80px' }}
                buttonStyle={{
                  color: isActive ? 'rgb(0, 188, 212)' : 'rgb(117, 117, 117)',
                }}
                icon={icon}
                label={label}
              >
                {child}
              </Tab>
            )
          })}
        </Tabs>

        {!this.props.disableGoogleTranslate && this.state.value !== 0 ? (
          <div
            className="istring-tabs-google-translate-button"
            onClick={this._translateString}
          >
            <div className="istring-tabs-google-translate-icon" />
          </div>
        ) : (
          <div className="istring-tabs-google-translate-button-stub" />
        )}
      </div>
    )
  }

  _handleTabChange = value => {
    this.setState({ value })
  }

  _getLocaleIcon(locale, isActive) {
    const countryCode = countryByLocale[locale]

    if (countryCode) {
      let iconClass = `flag-icon flag-icon-${countryCode} aor-flag-icon`

      if (!isActive) {
        iconClass = iconClass + ' aor-flag-icon-not-active'
      }

      return <span className={iconClass} />
    }
  }

  _translateString = async () => {
    const tabIndex = this.state.value

    if (this.sourceDefaultLocaleKey) {
      const sourceDefaultLocaleValue = get(
        this.props.formValues,
        this.sourceDefaultLocaleKey
      )

      if (sourceDefaultLocaleValue) {
        // console.info('sourceDefaultLocaleValue: ', sourceDefaultLocaleValue)

        const locale = APP_LOCALES[tabIndex]

        if (locale && locale !== process.env.REACT_APP__DEFAULT_LOCALE) {
          // console.info('locale: ', locale)

          try {
            const response = await translate(sourceDefaultLocaleValue, {
              from: process.env.REACT_APP__DEFAULT_LOCALE,
              to: locale,
              engine: 'google',
              key: process.env.REACT_APP__GOOGLE_API_KEY,
            })

            // console.info('response: ', response)

            const pattern = new RegExp(
              `.${process.env.REACT_APP__DEFAULT_LOCALE}$`,
              'g'
            )

            const key = this.sourceDefaultLocaleKey.replace(
              pattern,
              `.${locale}`
            )

            this.props.dispatch({
              type: '@@redux-form/CHANGE',
              meta: {
                form: 'record-form',
                field: key,
                touch: false,
                persistentSubmitErrors: false,
              },
              payload: response,
            })
          } catch (err) {
            console.log('err', err)
          }
        }
      }
    }
  }
}

export default connect(state => ({
  formValues: state.form['record-form'].values,
}))(IStringTabs)
