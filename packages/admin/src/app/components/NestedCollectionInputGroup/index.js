/**
 *
 * NestedCollectionInputGroup
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import AddIcon from 'material-ui/svg-icons/content/add'
import RemoveIcon from 'material-ui/svg-icons/content/clear'
import IconButton from 'material-ui/IconButton'
import IStringTabs from 'app/components/IStringTabs'
import FormInput from 'admin-on-rest/lib/mui/form/FormInput'

import { get, uniq } from 'lodash'

import './styles.css'

const firebaseKey = require('firebase-key')

export default class NestedCollectionInputGroup extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    resource: PropTypes.string,
    source: PropTypes.string,
    basePath: PropTypes.string,
    inputComponent: PropTypes.any,
    appLocales: PropTypes.array,
    dispatch: PropTypes.func,
  }

  state = {
    newEntries: [],
    removedEntries: [],
  }

  render() {
    const {
      label,
      record,
      basePath,
      resource,
      source,
      inputComponent: Input,
      appLocales,
    } = this.props

    const collection = get(record, source) || {}

    const etriesIds = uniq(
      Object.keys(collection).concat(this.state.newEntries)
    ).filter(id => this.state.removedEntries.indexOf(id) === -1)

    return (
      <div className="nested-collection-input-group">
        <div className="nested-collection-input-group__header">
          <div className="nested-collection-input-group__label">{label}</div>

          <IconButton tooltip={`Add ${label}`} onClick={this._addEntry}>
            <AddIcon className="nested-collection-input-group__add-icon" />
          </IconButton>
        </div>

        {etriesIds.length ? (
          <div className="nested-collection-input-group__content">
            {etriesIds.map((id, index) => {
              if (Input.props.localized) {
                return (
                  <GroupEntry
                    key={id}
                    id={id}
                    appLocales={appLocales}
                    source={source}
                    input={Input}
                    index={index}
                    basePath={basePath}
                    record={record}
                    resource={resource}
                    label={label}
                    onRemove={this._removeEntry}
                  />
                )
              }

              const PatchedInput = React.cloneElement(Input, {
                source: `${source}.${id}`,
              })

              return (
                <FormInput
                  key={id}
                  basePath={basePath}
                  input={PatchedInput}
                  record={record}
                  label={`${source} ${index + 1}`}
                  resource={resource}
                />
              )
            })}
          </div>
        ) : null}
      </div>
    )
  }

  _addEntry = () =>
    this.setState(prevState => ({
      newEntries: [firebaseKey.key()].concat(prevState.newEntries),
    }))

  _removeEntry = id => {
    this.setState(prevState => ({
      removedEntries: prevState.removedEntries.concat(id),
    }))

    this.props.dispatch({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'record-form',
        field: `${this.props.source}.${id}`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: null,
    })
  }
}

class GroupEntry extends React.Component {
  static propTypes = {
    id: PropTypes.string,
    label: PropTypes.string,
    record: PropTypes.object,
    resource: PropTypes.string,
    source: PropTypes.string,
    basePath: PropTypes.string,
    input: PropTypes.any,
    appLocales: PropTypes.array,
    index: PropTypes.number,
    onRemove: PropTypes.func,
  }

  render() {
    const {
      id,
      appLocales,
      source,
      input,
      index,
      basePath,
      record,
      resource,
      label,
    } = this.props

    return (
      <div className="nested-collection-input-group__entry">
        <IStringTabs>
          {appLocales.map(locale => {
            const localizedInput = React.cloneElement(input, {
              source: `${source}.${id}.${locale}`,
              label: `${source} ${index + 1}`,
            })

            return (
              <FormInput
                key={locale}
                basePath={basePath}
                input={localizedInput}
                locale={locale}
                record={record}
                resource={resource}
              />
            )
          })}
        </IStringTabs>

        <IconButton tooltip={`Remove ${label}`} onClick={this._onRemove}>
          <RemoveIcon className="nested-collection-input-group__remove-icon" />
        </IconButton>
      </div>
    )
  }

  _onRemove = () => {
    const { id, onRemove } = this.props

    onRemove(id)
  }
}
