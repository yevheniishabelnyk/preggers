import React from 'react'
import PropTypes from 'prop-types'

import FormInput from 'admin-on-rest/lib/mui/form/FormInput'

import IStringTabs from 'app/components/IStringTabs'

import { APP_LOCALES } from 'config'

const FormTab = ({ children, resource, record, basePath, ...rest }) => (
  <span>
    {React.Children.map(children, input => {
      if (!input) {
        return null
      }

      if (input.props.localized) {
        return (
          <IStringTabs
            disableGoogleTranslate={input.props.disableGoogleTranslate}
          >
            {APP_LOCALES.map(locale => {
              const localizedInput = React.cloneElement(input, {
                source: `${input.props.source}.${locale}`,
                choices:
                  input.props.choices && input.props.choiceLocaleFilter
                    ? input.props.choices.filter(item =>
                        input.props.choiceLocaleFilter(item, locale)
                      )
                    : input.props.choices,
              })

              return (
                <FormInput
                  key={locale}
                  input={localizedInput}
                  locale={locale}
                  {...rest}
                />
              )
            })}
          </IStringTabs>
        )
      }

      if (input.props.nestedForm) {
        return React.cloneElement(input, {
          basePath,
          record,
          resource,
          appLocales: APP_LOCALES,
          dispatch: this.props.dispatch,
        })
      }

      return <FormInput input={input} {...rest} />
    })}
  </span>
)

FormTab.propTypes = {
  children: PropTypes.any,
  resource: PropTypes.string,
  record: PropTypes.object,
  basePath: PropTypes.string,
}

export default FormTab
