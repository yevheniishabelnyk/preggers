/**
 *
 * Audio field
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import ReactAudioPlayer from 'react-audio-player'

const AudioField = ({ source, record = {} }) => (
  <ReactAudioPlayer
    src={record[source]}
    controls
    style={{ marginBottom: '-4px' }}
  />
)

AudioField.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
}

AudioField.defaultProps = {
  label: 'test',
}

export default AudioField
