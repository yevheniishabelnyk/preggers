/**
 *
 * PageContent
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import './style.css'

export default class PageContent extends React.Component {
  static propTypes = {
    children: PropTypes.any,
  }

  render() {
    return <div className="page-content__container">{this.props.children}</div>
  }
}
