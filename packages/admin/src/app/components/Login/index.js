import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { propTypes, reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import compose from 'recompose/compose'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { Card, CardActions } from 'material-ui/Card'
import Avatar from 'material-ui/Avatar'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
// import CircularProgress from 'material-ui/CircularProgress'
import LockIcon from 'material-ui/svg-icons/action/lock-outline'
import { cyan500, pinkA200 } from 'material-ui/styles/colors'

import defaultTheme from 'admin-on-rest/lib/mui/defaultTheme'
import { userLogin as userLoginAction } from 'admin-on-rest'
import translate from 'admin-on-rest/lib/i18n/translate'
import Notification from 'admin-on-rest/lib/mui/layout/Notification'

import FacebookLogin from 'react-facebook-login'

import './style.css'

const styles = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    minWidth: 300,
  },
  avatar: {
    margin: '1em',
    textAlign: 'center ',
  },
  form: {
    padding: '0 1em 1em 1em',
  },
  input: {
    display: 'flex',
  },
  facebookButton: {
    backgroundColor: 'blue',
  },
}

function getColorsFromTheme(theme) {
  if (!theme) {
    return { primary1Color: cyan500, accent1Color: pinkA200 }
  }
  const {
    palette: { primary1Color, accent1Color },
  } = theme
  return { primary1Color, accent1Color }
}

// see http://redux-form.com/6.4.3/examples/material-ui/
const renderInput = ({
  meta: { touched, error } = {},
  input: { ...inputProps },
  ...props
}) => (
  <TextField
    errorText={touched && error}
    {...inputProps}
    {...props}
    fullWidth
  />
)

renderInput.propTypes = {
  meta: PropTypes.object,
  input: PropTypes.any,
}

class Login extends Component {
  _loginWithEmail = auth =>
    this.props.userLogin(
      Object.assign(auth, { providerId: 'password' }),
      this.props.location.state ? this.props.location.state.nextPathname : '/'
    )

  _loginWithFacebook = response => {
    if (
      !response.error &&
      response.status !== 'unknown' &&
      response.status !== 'not_authorized'
    ) {
      const auth = {
        providerId: 'facebook.com',
        token: response.accessToken,
        username: response.email,
      }

      this.props.userLogin(
        auth,
        this.props.location.state ? this.props.location.state.nextPathname : '/'
      )
    }
  }

  render() {
    const { handleSubmit, isLoading, theme, translate } = this.props
    const muiTheme = getMuiTheme(theme)
    const { primary1Color, accent1Color } = getColorsFromTheme(muiTheme)
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div style={{ ...styles.main, backgroundColor: primary1Color }}>
          <Card style={styles.card}>
            <div style={styles.avatar}>
              <Avatar
                backgroundColor={accent1Color}
                icon={<LockIcon />}
                size={60}
              />
            </div>
            <form onSubmit={handleSubmit(this._loginWithEmail)}>
              <div style={styles.form}>
                <div style={styles.input}>
                  <Field
                    name="username"
                    component={renderInput}
                    floatingLabelText={translate('aor.auth.username')}
                    disabled={isLoading}
                  />
                </div>
                <div style={styles.input}>
                  <Field
                    name="password"
                    component={renderInput}
                    floatingLabelText={translate('aor.auth.password')}
                    type="password"
                    disabled={isLoading}
                  />
                </div>
              </div>
              <CardActions>
                <RaisedButton
                  type="submit"
                  primary
                  // disabled={isLoading}
                  // icon={
                  //   isLoading && <CircularProgress size={25} thickness={2} />
                  // }
                  label={translate('aor.auth.sign_in')}
                  fullWidth
                />

                <div className="admin-login-form__buttons-devider">OR</div>

                {/*<RaisedButton
                  label="Facebook"
                  secondary={true}
                  fullWidth
                  className="admin-login-form__facebook-button"
                  icon={<i className="admin-login-form__facebook-icon" />}
                />*/}

                <FacebookLogin
                  appId={process.env.REACT_APP__FACEBOOK__APP_ID}
                  autoLoad={false}
                  scope="public_profile,email"
                  fields="name,email"
                  callback={this._loginWithFacebook}
                  cssClass="fb-auth-button"
                  textButton="Facebook"
                />
              </CardActions>
            </form>
          </Card>
          <Notification />
        </div>
      </MuiThemeProvider>
    )
  }
}

Login.propTypes = {
  ...propTypes,
  meta: PropTypes.object,
  input: PropTypes.any,
  authClient: PropTypes.func,
  previousRoute: PropTypes.string,
  theme: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired,
  userLogin: PropTypes.func.isRequired,
}

Login.defaultProps = {
  theme: defaultTheme,
}

const mapStateToProps = state => ({ isLoading: state.admin.loading > 0 })

const enhance = compose(
  translate,
  reduxForm({
    form: 'signIn',
    validate: (values, props) => {
      const errors = {}
      const { translate } = props
      if (!values.username) {
        errors.username = translate('aor.validation.required')
      }
      if (!values.password) {
        errors.password = translate('aor.validation.required')
      }
      return errors
    },
  }),
  connect(
    mapStateToProps,
    { userLogin: userLoginAction }
  )
)

export default enhance(Login)
