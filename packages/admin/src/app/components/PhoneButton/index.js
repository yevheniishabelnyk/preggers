import React from 'react'
import PropTypes from 'prop-types'
import FlatButton from 'material-ui/FlatButton'
import ActionList from 'material-ui/svg-icons/hardware/phone-iphone'
import translate from 'admin-on-rest/lib/i18n/translate'

const PhoneButton = ({ label = '', style, onClick }) => (
  <FlatButton
    primary
    label={label && translate(label)}
    icon={<ActionList />}
    style={style}
    onClick={onClick}
  />
)

PhoneButton.propTypes = {
  label: PropTypes.string,
  style: PropTypes.object,
  translate: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default translate(PhoneButton)
