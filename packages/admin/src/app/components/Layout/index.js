/**
 *
 * Layout
 *
 */

import React, { createElement, Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
// import CircularProgress from 'material-ui/CircularProgress'
import {
  AdminRoutes,
  AppBar,
  Menu,
  Notification,
  setSidebarVisibility,
  Sidebar,
} from 'admin-on-rest'

import { authCheck } from 'app/redux/auth'

import firebase from 'firebase'

import './style.css'

const styles = {
  wrapper: {
    // Avoid IE bug with Flexbox, see #467
    display: 'flex',
    flexDirection: 'column',
  },
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
  body: {
    backgroundColor: '#edecec',
    display: 'flex',
    flex: 1,
    overflowY: 'hidden',
    overflowX: 'scroll',
  },
  content: {
    flex: 1,
    padding: '2em',
    overflow: 'auto',
  },
  loader: {
    position: 'absolute',
    top: 0,
    right: 0,
    margin: 16,
    zIndex: 1200,
  },
}

class Layout extends Component {
  async componentWillMount() {
    this.props.setSidebarVisibility(true)

    firebase.auth().onAuthStateChanged(user => {
      this.props.authCheck({ isEmpty: !user })
    })
  }

  render() {
    const {
      children,
      customRoutes,
      dashboard,
      // isLoading,
      logout,
      menu,
      title,
    } = this.props

    return (
      <MuiThemeProvider>
        <div style={styles.wrapper}>
          <div style={styles.main}>
            <AppBar title={title} />
            <div className="body" style={styles.body}>
              <div style={styles.content}>
                <AdminRoutes customRoutes={customRoutes} dashboard={dashboard}>
                  {children}
                </AdminRoutes>
              </div>
              <Sidebar>
                {createElement(menu || Menu, {
                  logout,
                  hasDashboard: !!dashboard,
                })}
              </Sidebar>
            </div>
            <Notification />
            {/*isLoading && (
              <CircularProgress
                color="#fff"
                size={30}
                thickness={2}
                style={styles.loader}
              />
            )*/}
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.any,
  logout: PropTypes.any,
  authClient: PropTypes.func,
  customRoutes: PropTypes.array,
  dashboard: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  isLoading: PropTypes.bool.isRequired,
  menu: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
  resources: PropTypes.array,
  setSidebarVisibility: PropTypes.func.isRequired,
  authCheck: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
}

const mapStateToProps = state => ({
  isLoading: state.admin.loading > 0,
  isAuthEmpty: state.auth.isEmpty,
})
export default connect(
  mapStateToProps,
  { setSidebarVisibility, authCheck }
)(Layout)
