/**
 *
 * Admin on REST auth client
 *
 */

import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_CHECK } from 'admin-on-rest'
import firebase from 'firebase'

import { isValidEmail } from '../utils/validators'

export default async (type, params) => {
  let authIsInitialised = false

  firebase.auth().onAuthStateChanged(() => {
    if (!authIsInitialised) {
      authIsInitialised = true
    }
  })

  switch (type) {
    case AUTH_LOGOUT:
      // console.log('AUTH_LOGOUT')
      localStorage.removeItem('firebaseToken')
      return firebase.auth().signOut()

    case AUTH_CHECK: {
      // console.log('AUTH_CHECK')

      return new Promise((resolve, reject) => {
        const authIsInitialisedPrevValue = authIsInitialised

        const checkAuth = () => {
          // console.log('checkAuth')

          if (authIsInitialised !== authIsInitialisedPrevValue) {
            if (firebase.auth().currentUser) {
              resolve()
            } else {
              reject(new Error('User not found'))
            }
          } else {
            setTimeout(checkAuth, 20)
          }
        }

        setTimeout(checkAuth, 20)
      })
    }

    case AUTH_LOGIN: {
      // console.log('AUTH_LOGIN')
      const { providerId, username, password = '', token } = params
      console.info('params: ', params)

      if (providerId === 'password') {
        if (!isValidEmail(username)) {
          return Promise.reject(new Error('Not valid email!'))
        }

        if (password.length < 8) {
          return Promise.reject(
            new Error('Password should be minimum 8 characters!')
          )
        }
      }

      const adminsSnap = await firebase
        .database()
        .ref('/admins')
        .once('value')

      if (adminsSnap.exists()) {
        const admins = adminsSnap.val()
        let userId = null

        Object.keys(admins).forEach(key => {
          if (admins[key] === username) {
            userId = key
          }
        })

        if (admins[userId] !== username) {
          return Promise.reject(
            new Error('You are not one of the administrators!')
          )
        }
        try {
          let provider, credential

          if (providerId === 'facebook.com' && token) {
            provider = firebase.auth.FacebookAuthProvider
            credential = provider.credential(token)
          } else if (providerId === 'password' && username && password) {
            provider = firebase.auth.EmailAuthProvider
            credential = provider.credential(username, password)
          }

          if (provider && credential) {
            const auth = await firebase.auth().signInWithCredential(credential)

            console.info(`sign in with ${providerId}: `, auth)

            if (!auth.emailVerified) {
              auth.sendEmailVerification()
              firebase.auth().signOut()

              return Promise.reject(
                new Error(
                  'Email is not verified! We sent you a letter with instructions how to do it.'
                )
              )
            }

            const firebaseToken = await auth.getIdToken()

            localStorage.setItem('firebaseToken', firebaseToken)

            return Promise.resolve()
          }
          return Promise.reject(new Error('Not valid credentials!'))
        } catch (e) {
          console.info('e: ', e)

          if (e.code === 'auth/wrong-password') {
            return Promise.reject(new Error('Wrong password!'))
          }

          return Promise.reject(new Error('Sorry, something went wrong...'))
        }
      } else {
        return Promise.reject(
          new Error('You are not one of the administrators!')
        )
      }
    }

    default:
      return Promise.resolve()
  }
}
