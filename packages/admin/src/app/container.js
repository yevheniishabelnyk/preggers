/**
 *
 *  App
 *
 */

import React from 'react'

import { Admin, Resource, resolveBrowserLocale } from 'admin-on-rest'

import RestClient from './restClient/index.js'
import authClient from 'app/authClient'
import createHistory from 'history/createBrowserHistory'
// import { messages } from 'app/i18n'

import 'flag-icon-css/css/flag-icon.css'

import Popup from 'react-popup'
import Layout from 'app/components/Layout'
import Login from 'app/components/Login'
// import Dashboard from 'app/components/Dashboard'
import Menu from 'app/components/Menu'

import { list as trackedResources } from 'resources'

// custom routes
import customRoutes from 'app/routes'
import customReducers from 'app/redux/reducers'

// resources
import users from 'resources/users'
import competitions from 'resources/competitions'
import competitionSubmissions from 'resources/competitionSubmissions'
import children from 'resources/children'
import foods from 'resources/foods'
import foodCategories from 'resources/foodCategories'
import pregnancies from 'resources/pregnancies'
import faqs from 'resources/faqs'
import countryBabyNames from 'resources/countryBabyNames'
import checklistItems from 'resources/checklistItems'
import invitations from 'resources/invitations'
import connections from 'resources/connections'
import weeks from 'resources/weeks'
import dealCategories from 'resources/dealCategories'
// import videos from 'resources/videos'
// import videoTags from 'resources/videoTags'
import feedbacks from 'resources/feedbacks'
import dailyTips from 'resources/dailyTips'
import weeklyTips from 'resources/weeklyTips'
import tags from 'resources/tags'
import otherApps from 'resources/otherApps'
import mailingWeeks from 'resources/mailingWeeks'

import articles from 'resources/articles'

const firebaseConfig = {
  apiKey: process.env.REACT_APP__FIREBASE__API_KEY,
  authDomain: process.env.REACT_APP__FIREBASE__AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP__FIREBASE__DATABASE_URL,
  storageBucket: process.env.REACT_APP__FIREBASE__STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP__FIREBASE__MESSAGING_SENDER_ID,
  persistence: true,
}

const restClient = RestClient(trackedResources, firebaseConfig)

const history = createHistory()

export default class App extends React.Component {
  render() {
    return (
      <div className="app">
        <Admin
          locale={resolveBrowserLocale()}
          // messages={messages}
          customRoutes={customRoutes}
          customReducers={customReducers}
          restClient={restClient}
          // dashboard={Dashboard}
          authClient={authClient}
          loginPage={Login}
          history={history}
          appLayout={Layout}
          title="Preggers Admin"
          isLoading
          menu={Menu}
        >
          <Resource {...users} />
          <Resource {...competitions} />
          <Resource {...competitionSubmissions} />
          <Resource {...children} />
          <Resource {...foods} />
          <Resource {...foodCategories} />
          <Resource {...pregnancies} />
          <Resource {...checklistItems} />
          <Resource {...faqs} />
          <Resource {...countryBabyNames} />
          <Resource {...invitations} />
          <Resource {...connections} />
          <Resource {...weeks} />
          <Resource {...dealCategories} />
          {/*<Resource {...videos} />
          <Resource {...videoTags} />*/}
          <Resource {...feedbacks} />
          <Resource {...dailyTips} />
          <Resource {...weeklyTips} />
          <Resource {...articles} />
          <Resource {...tags} />
          <Resource {...otherApps} />
          <Resource {...mailingWeeks} />
        </Admin>

        <Popup />
      </div>
    )
  }
}
