/**
 *
 * App custom reducers
 *
 */

import { reducer as authReducer } from './auth'

export default {
  auth: authReducer,
}
