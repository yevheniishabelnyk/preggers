/**
 *
 * Auth
 *
 */

const actionTypes = {
  AUTH__CHECK: 'AUTH__CHECK',
}

const initialState = {
  isEmpty: true,
}

export function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.AUTH__CHECK:
      return {
        ...state,
        isEmpty: action.payload.isEmpty,
      }

    default:
      return state
  }
}

export const authCheck = (payload = {}) => ({
  type: actionTypes.AUTH__CHECK,
  payload,
})
