export default [
  {
    name: 'LOCALE',
    description: 'Letter language',
  },

  {
    name: 'WELCOME_TITLE',
    description: 'Welcome new user title',
  },

  {
    name: 'HAS_WELCOME_TITLE',
    description: 'Has welcome new user title',
  },

  {
    name: 'WELCOME_BUTTON_TITLE',
    description: 'Welcome new user button title',
  },

  {
    name: 'HAS_WELCOME_BUTTON_TITLE',
    description: 'Has welcome new user button title',
  },

  {
    name: 'WELCOME_BUTTON_LINK',
    description: 'Welcome new user button link',
  },

  {
    name: 'HAS_WELCOME_BUTTON_LINK',
    description: 'Has welcome new user button link',
  },

  {
    name: 'WELCOME_IMAGE',
    description: 'Welcome new user image',
  },

  {
    name: 'HAS_WELCOME_IMAGE',
    description: 'Has welcome new user image',
  },

  {
    name: 'WELCOME_INGRESS',
    description: 'Welcome new user ingress',
  },

  {
    name: 'HAS_WELCOME_INGRESS',
    description: 'Has welcome new user ingress',
  },

  {
    name: 'WELCOME_TEXT',
    description: 'Welcome new user text',
  },

  {
    name: 'HAS_WELCOME_TEXT',
    description: 'Has welcome new user text',
  },

  {
    name: 'WEEK_IMAGE',
    description: 'Week image',
  },

  {
    name: 'HAS_WEEK_IMAGE',
    description: 'Has week image',
  },

  {
    name: 'WEEK_TITLE',
    description: 'Mailing week title',
  },

  {
    name: 'HAS_WEEK_TITLE',
    description: 'Has mailing week title',
  },

  {
    name: 'WEEK_INGRESS',
    description: 'Mailing week ingress',
  },

  {
    name: 'HAS_WEEK_INGRESS',
    description: 'Has mailing week ingress',
  },

  {
    name: 'WEEK_TEXT',
    description: 'Mailing week text',
  },

  {
    name: 'HAS_WEEK_TEXT',
    description: 'Has mailing week text',
  },

  {
    name: 'MAILING_WEEK_IMAGE',
    content: 'Mailing week image',
  },

  {
    name: 'HAS_MAILING_WEEK_IMAGE',
    content: 'Has mailing week image',
  },

  {
    name: 'WEEK_BUTTON_TITLE',
    description: 'Week button title',
  },

  {
    name: 'HAS_WEEK_BUTTON_TITLE',
    description: 'Has week button title',
  },

  {
    name: 'WEEK_BUTTON_LINK',
    description: 'Week button link',
  },

  {
    name: 'HAS_WEEK_BUTTON_LINK',
    description: 'Has week button link',
  },

  {
    name: 'HAS_AD_HTML_TEMPLATE',
    description: 'Has Ad HTML template',
  },

  {
    name: 'AD_HTML_TEMPLATE',
    description: 'Ad HTML template',
  },

  {
    name: 'INVITE_TITLE',
    description: 'Invite title',
  },

  {
    name: 'HAS_INVITE_TITLE',
    description: 'Has invite title',
  },

  {
    name: 'INVITE_BUTTON_TITLE',
    description: 'Invite button title',
  },

  {
    name: 'HAS_INVITE_BUTTON_TITLE',
    description: 'Has invite button title',
  },

  {
    name: 'INVITE_BUTTON_LINK',
    description: 'Invite button link',
  },

  {
    name: 'HAS_INVITE_BUTTON_LINK',
    description: 'Has invite button link',
  },

  {
    name: 'INVITE_HEADER',
    description: 'Invite header',
  },

  {
    name: 'HAS_INVITE_HEADER',
    description: 'Has invite header',
  },

  {
    name: 'INVITE_TEXT',
    description: 'Invite text',
  },

  {
    name: 'HAS_INVITE_TEXT',
    description: 'Has invite text',
  },
  {
    name: 'INVITE_CODE_LABEL',
    description: 'Invite code label',
  },

  {
    name: 'HAS_INVITE_CODE_LABEL',
    description: 'Has invite code label',
  },
]
