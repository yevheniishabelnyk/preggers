export default [
  {
    name: 'IS_NEW_USER',
    description: 'Is new user welcome email',
  },

  {
    name: 'IS_NEW_WEEK',
    description: 'Is new week welcome email',
  },

  {
    name: 'IS_MOTHER',
    description: 'Is user are "mother" type',
  },

  {
    name: 'USER_NAME',
    description: 'User name',
  },

  {
    name: 'WEEK_NUMBER',
    description: 'User pregnancy current week number',
  },

  {
    name: 'HAS_PARTNER',
    description: 'Has user partner',
  },

  {
    name: 'PARTNER_NAME',
    description: 'User partner name',
  },

  {
    name: 'HAS_CHILD_NAME',
    description: 'Has user changed her child name from default "My Baby"',
  },

  {
    name: 'CHILD_NAME',
    description: 'User child name',
  },
]
