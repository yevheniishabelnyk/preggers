/**
 *
 * GET_ONE
 *
 */

export default async function getOne(resourceData, params) {
  const key = params.id

  if (key && resourceData[key]) {
    return Promise.resolve({ data: resourceData[key] })
  }
  return Promise.reject(new Error('Key not found'))
}
