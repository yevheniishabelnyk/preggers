/**
 *
 * DELETE
 *
 */

import firebase from 'firebase'
import { values } from 'lodash'
import findFiles from '../utils/findFiles'

export default async function deleteRequest(resourceData, params) {
  console.info('deleteRequest: ', deleteRequest)
  const recordPath = `${params.basePath}/${params.id}`
  const record = resourceData[params.id]

  console.info('record: ', record)

  const files = findFiles(record, recordPath)
  console.info('files: ', files)

  const removeFilePromises = values(files).map(file => async () => {
    const fileStoragePath = decodeURIComponent(
      file.uri.split(/[?#]/)[0].match(/[^/]*$/)[0]
    )

    const storageRef = firebase.storage().ref()

    console.info('fileStoragePath: ', fileStoragePath)

    const desertRef = storageRef.child(fileStoragePath)

    try {
      await desertRef.delete()
      console.info('file deleted: ', fileStoragePath)
      return Promise.resolve()
    } catch (err) {
      return Promise.reject(err)
    }
  })

  try {
    await Promise.all(removeFilePromises.map(promise => promise()))

    await firebase
      .database()
      .ref(params.basePath + '/' + params.id)
      .remove()

    return Promise.resolve({ data: params.id })
  } catch (err) {
    return Promise.reject(err)
  }
}
