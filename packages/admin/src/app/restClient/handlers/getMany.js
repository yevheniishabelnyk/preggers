/**
 *
 * GET_MANY
 *
 */

export default async function getMany(resourceData, params) {
  if (!params.ids) {
    console.error('Unexpected parameters: ', params)

    return Promise.reject(new Error('Error processing request'))
  }

  let ids = [],
    data = [],
    total = 0

  params.ids.forEach(key => {
    if (resourceData[key]) {
      ids.push(key)
      data.push(resourceData[key])
      total++
    }
  })

  return Promise.resolve({ data, ids, total })
}
