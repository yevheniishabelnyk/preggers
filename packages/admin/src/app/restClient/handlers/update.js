/**
 *
 * UPDATE
 *
 */

import firebase from 'firebase'
import ServerTimestamp from 'db/ServerTimestamp'
import { keys, set } from 'lodash'
import difference from '../utils/difference'
import uploadFile from '../utils/uploadFile'
import checkUpdates from '../utils/checkUpdates'

export default async function updateRequest(
  resourceData,
  params,
  resourcePath,
  parentKey
) {
  const newData = params.data
  let oldData = resourceData[params.id]
  const recordPath = `${params.basePath}/${params.id}`

  console.info(
    '[REST CLIENT][UPDATE] New data: \n\n',
    JSON.stringify(newData, null, 4)
  )

  console.info(
    '\n[REST CLIENT][UPDATE] Old data: \n\n',
    JSON.stringify(oldData, null, 4)
  )

  const diff = difference(newData, oldData)
  console.info(
    '\n[REST CLIENT][UPDATE] Data diff: \n\n',
    JSON.stringify(diff, null, 4)
  )

  let databaseDiff, filesToAdd, filesToRemove

  if (parentKey !== 'id' && newData[parentKey] !== oldData[parentKey]) {
    newData.id = String(newData[parentKey])
    const pathPrefix = `${params.basePath}/${newData[parentKey]}`
    ;({ filesToAdd, filesToRemove } = checkUpdates(
      newData,
      oldData,
      pathPrefix
    ))

    if (keys(filesToRemove).length) {
      keys(filesToRemove)
        .map(key => key.replace(`${pathPrefix}/`, ''))
        .forEach(fileProp => {
          newData[fileProp] = undefined
        })
    }

    ;({ data: databaseDiff } = checkUpdates(newData, {}, pathPrefix))

    databaseDiff[`${params.basePath}/${params.id}`] = null

    databaseDiff[
      `${params.basePath}/${newData[parentKey]}/updatedAt`
    ] = ServerTimestamp

    databaseDiff[
      `${params.basePath}/${newData[parentKey]}/createdAt`
    ] = ServerTimestamp
  } else {
    const pathPrefix = recordPath
    ;({ data: databaseDiff, filesToAdd, filesToRemove } = checkUpdates(
      newData,
      oldData,
      pathPrefix
    ))

    databaseDiff[`${recordPath}/updatedAt`] = ServerTimestamp
  }

  console.info(
    '\n[REST CLIENT][UPDATE] Files to add diff: \n\n',
    JSON.stringify(filesToAdd, null, 4)
  )

  console.info(
    '\n[REST CLIENT][UPDATE] Files to remove diff: \n\n',
    filesToRemove
  )

  try {
    if (Object.keys(filesToAdd).length || Object.keys(filesToRemove).length) {
      const fileToAddPromises = Object.keys(filesToAdd).map(key => async () => {
        const file = filesToAdd[key]
        const fileUrlDatabaseLocation = key
        const fileTypeDatabaseLocation = key.replace(/[^/]*$/, 'type')
        const fileUrlInRecordLocation = fileUrlDatabaseLocation
          .replace(recordPath + '/', '')
          .replace(/\//g, '.')
        const fileTypeInRecordLocation = fileTypeDatabaseLocation
          .replace(recordPath + '/', '')
          .replace(/\//g, '.')

        const fileUrl = await uploadFile(file, params.basePath)

        console.info(
          '\n[REST CLIENT][UPDATE] Updating image record in database...'
        )

        databaseDiff[fileUrlDatabaseLocation] = fileUrl
        databaseDiff[fileTypeDatabaseLocation] = 'file'

        console.info(
          '\n[REST CLIENT][UPDATE] Database was successfully updated with new new file url'
        )

        set(newData, fileUrlInRecordLocation, fileUrl)
        set(newData, fileTypeInRecordLocation, 'file')
      })

      const fileToRemovePromises = Object.keys(filesToRemove).map(
        fileDatabaseLocation => async () => {
          const file = filesToRemove[fileDatabaseLocation]

          const fileStoragePath = decodeURIComponent(
            file.uri.split(/[?#]/)[0].match(/[^/]*$/)[0]
          )

          const storageRef = firebase.storage().ref()

          // console.info('fileStoragePath: ', fileStoragePath)

          const desertRef = storageRef.child(fileStoragePath)

          await desertRef.delete()
          // console.info(`file ${fileStoragePath} deleted: `)
        }
      )

      await Promise.all(fileToAddPromises.map(promise => promise()))

      try {
        await Promise.all(fileToRemovePromises.map(promise => promise()))
      } catch (err) {
        console.log('err', err)
      }
    }

    console.info(
      '\n[REST CLIENT][UPDATE] Database diff: \n\n',
      JSON.stringify(databaseDiff, null, 4)
    )

    await firebase
      .database()
      .ref()
      .update(databaseDiff)

    const updatedData = Object.assign({}, oldData, newData, {
      updatedAt: Date.now(),
    })

    return Promise.resolve({ data: updatedData })
  } catch (err) {
    return Promise.reject(err)
  }
}
