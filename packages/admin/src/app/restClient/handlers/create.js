/**
 *
 * CREATE
 *
 */

import firebase from 'firebase'
import ServerTimestamp from 'db/ServerTimestamp'
import { set } from 'lodash'
import uploadFile from '../utils/uploadFile'
import checkUpdates from '../utils/checkUpdates'

export default async function create(
  resourceData,
  params,
  resourcePath,
  parentKey
) {
  let newItemKey = params.data.id

  if (parentKey !== 'id') {
    newItemKey = String(params.data[parentKey])
  }

  if (!newItemKey) {
    newItemKey = firebase
      .database()
      .ref()
      .child(resourcePath)
      .push().key
  } else if (resourceData && resourceData[newItemKey]) {
    return Promise.reject(new Error('ID already in use'))
  }

  const newData = Object.assign(params.data, { id: newItemKey })

  const recordPath = `${params.basePath}/${newItemKey}`

  console.info(
    '[REST CLIENT][CREATE] New data: \n\n',
    JSON.stringify(newData, null, 4)
  )

  const { data: databaseDiff, filesToAdd } = checkUpdates(
    newData,
    {},
    recordPath
  )

  databaseDiff[`${recordPath}/updatedAt`] = ServerTimestamp
  databaseDiff[`${recordPath}/createdAt`] = ServerTimestamp

  console.info(
    '\n[REST CLIENT][CREATE] Files diff: \n\n',
    JSON.stringify(filesToAdd, null, 4)
  )

  try {
    if (Object.keys(filesToAdd).length) {
      const filePromises = Object.keys(filesToAdd).map(key => async () => {
        const file = filesToAdd[key]
        const fileUrlDatabaseLocation = key
        const fileTypeDatabaseLocation = key.replace(/[^/]*$/, 'type')
        const fileUrlInRecordLocation = fileUrlDatabaseLocation
          .replace(recordPath + '/', '')
          .replace(/\//g, '.')
        const fileTypeInRecordLocation = fileTypeDatabaseLocation
          .replace(recordPath + '/', '')
          .replace(/\//g, '.')

        const fileUrl = await uploadFile(file, params.basePath)

        console.info(
          '\n[REST CLIENT][UPDATE] Updating image record in database...'
        )

        databaseDiff[fileUrlDatabaseLocation] = fileUrl
        databaseDiff[fileTypeDatabaseLocation] = 'file'

        console.info(
          '\n[REST CLIENT][UPDATE] Database was successfully updated with new new file url'
        )

        set(newData, fileUrlInRecordLocation, fileUrl)
        set(newData, fileTypeInRecordLocation, 'file')
      })

      await Promise.all(filePromises.map(promise => promise()))
    }

    console.info(
      '\n[REST CLIENT][CREATE] Database diff: \n\n',
      JSON.stringify(databaseDiff, null, 4)
    )

    await firebase
      .database()
      .ref()
      .update(databaseDiff)

    const createdData = Object.assign({}, newData, {
      createdAt: Date.now(),
      updatedAt: Date.now(),
    })

    return Promise.resolve({ data: createdData })
  } catch (err) {
    return Promise.reject(err)
  }
}
