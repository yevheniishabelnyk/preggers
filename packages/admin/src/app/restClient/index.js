/**
 *
 * REST client
 *
 */

import firebase from 'firebase'

import {
  GET_LIST,
  GET_ONE,
  GET_MANY,
  GET_MANY_REFERENCE,
  CREATE,
  UPDATE,
  DELETE,
} from 'admin-on-rest'
import * as Handlers from './handlers'

/**
 * @param {string[]|Object[]} trackedResources Array of resource names or array of Objects containing name and
 * optional path properties (path defaults to name)
 * @param {Object} firebaseConfig Optiona Firebase configuration
 */
export default (trackedResources = [], firebaseConfig = {}) => {
  /** TODO Move this to the Redux Store */
  const resourcesStatus = {}
  const resourcesReferences = {}
  const resourcesData = {}
  const resourcesPaths = {}
  const resourcesParentKeys = {}

  if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig)
  }

  trackedResources.forEach(resource => {
    if (typeof resource === 'object') {
      if (resource.parentKey) {
        resourcesParentKeys[resource.name] = resource.parentKey
      }

      if (!resource.name) {
        throw new Error(`name is missing from resource ${resource}`)
      }

      const path = resource.path || resource.name
      const name = resource.name

      // Check path ends with name so the initial children can be loaded from on 'value' below.
      const pattern = path.indexOf('/') >= 0 ? `/${name}$` : `${name}$`
      if (!path.match(pattern)) {
        throw new Error(`path ${path} must match ${pattern}`)
      }

      resourcesPaths[name] = path
      // eslint-disable-next-line no-param-reassign
      resource = name
    } else {
      resourcesPaths[resource] = resource
      resourcesParentKeys[resource] = 'id'
    }

    resourcesData[resource] = {}
    resourcesStatus[resource] = new Promise(resolve => {
      let ref = (resourcesReferences[resource] = firebase
        .database()
        .ref(resourcesPaths[resource]))

      ref.on('value', function(childSnapshot) {
        /** Uses "value" to fetch initial data. Avoid the AOR to show no results */
        if (childSnapshot.key === resource) {
          resourcesData[resource] = childSnapshot.val() || []
        }
        Object.keys(resourcesData[resource]).forEach(key => {
          resourcesData[resource][key].id = key
        })
        ref.on('child_added', function(childSnapshot) {
          resourcesData[resource][childSnapshot.key] = childSnapshot.val()
          resourcesData[resource][childSnapshot.key].id = childSnapshot.key
        })
        ref.on('child_removed', function(oldChildSnapshot) {
          if (resourcesData[resource][oldChildSnapshot.key]) {
            delete resourcesData[resource][oldChildSnapshot.key]
          }
        })
        ref.on('child_changed', function(childSnapshot) {
          resourcesData[resource][childSnapshot.key] = childSnapshot.val()
        })
        resolve()
      })
    })
  })

  /**
   * @param {string} type Request type, e.g GET_LIST
   * @param {string} resource Resource name, e.g. "posts"
   * @param {Object} payload Request parameters. Depends on the request type
   * @returns {Promise} the Promise for a REST response
   */
  return async (type, resource, params) => {
    await resourcesStatus[resource]

    switch (type) {
      case GET_LIST:
      case GET_MANY_REFERENCE:
        return Handlers.getList(resourcesData[resource], params)

      case GET_MANY:
        return Handlers.getMany(resourcesData[resource], params)

      case GET_ONE:
        return Handlers.getOne(resourcesData[resource], params)

      case DELETE:
        return Handlers.delete(resourcesData[resource], params)

      case UPDATE:
        return Handlers.update(
          resourcesData[resource],
          params,
          resourcesPaths[resource],
          resourcesParentKeys[resource]
        )

      case CREATE:
        return Handlers.create(
          resourcesData[resource],
          params,
          resourcesPaths[resource],
          resourcesParentKeys[resource]
        )

      default:
        console.error('Undocumented method: ', type)
        return { data: [] }
    }
  }
}
