/**
 *
 * Find files in record
 *
 */

import { isObject } from 'lodash'

export default function findFiles(record, pathPrefix = '') {
  const files = {}

  function findFilesByTypeProp(object, reference = '') {
    Object.keys(object).forEach(function(key) {
      const value = object[key]

      if (isObject(value)) {
        if (value.type === 'file') {
          files[`${reference}/${key}`] = value
        } else {
          findFilesByTypeProp(value, `${reference}/${key}`)
        }
      }
    })
  }

  findFilesByTypeProp(record, pathPrefix)

  return files
}
