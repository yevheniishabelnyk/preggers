/**
 *
 * Upload file util
 *
 */

import firebase from 'firebase'
import moment from 'moment'

export default async function uploadFile(file, bucket = '') {
  const storageRef = firebase.storage().ref()

  const fileName = file.name.split('.').reduce((out, str, index) => {
    if (index === 0) {
      return out + str + ' ' + moment().toISOString()
    }

    if (index === 1) {
      return out + '.' + str
    }

    return out
  }, '')

  let fileType = 'files'

  if (isImageFile(file.type)) {
    fileType = 'images'
  } else if (isAudioFile(file.type)) {
    fileType = 'audio'
  }

  console.info(
    '\n[REST CLIENT][UPDATE] File storage ref: ',
    `${bucket}/${fileType}/${fileName}`
  )

  console.info('\n[REST CLIENT][UPDATE] Uploading file to storage...\n', file)

  const fileRef = storageRef.child(`${bucket}/${fileType}/${fileName}`)

  const snap = await fileRef.put(file)

  // console.info('snap: ', snap)

  console.info(
    '\n[REST CLIENT][UPDATE] File was saved under url: ',
    snap.downloadURL
  )

  return Promise.resolve(snap.downloadURL)
}

function isImageFile(fileType) {
  return /image/g.test(fileType)
}

function isAudioFile(fileType) {
  return /audio/g.test(fileType)
}
