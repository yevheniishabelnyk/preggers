import { isEqual, isObject, uniq } from 'lodash'

/**
 * Creates a data update object and a files update object
 *
 * "data" is
 *   {
 *     "/cities/Paris/country": "France",
 *     "/cities/Paris/population": 2 229 621,
 *   }
 *
 * "files" is
 *   {
 *     "/cities/Paris/image/400x400": <File> instance,
 *     "/cities/Paris/image/800x800": <File> instance,
 *   }
 *
 * @param  {Object} newData      New data
 * @param  {Object} oldData      Old data
 * @param  {String} pathPrefix   Path prefix, for example /cities/Paris
 * @return {Object}              returns { data, files }
 */
export default function checkUpdates(newData, oldData, pathPrefix = '') {
  const data = {}
  const filesToAdd = {}
  const filesToRemove = {}

  function changes(object, base = {}, reference = '') {
    const keys = uniq([...Object.keys(object), ...Object.keys(base)])

    keys.forEach(function(key) {
      const newValue = object[key]
      const oldValue = base[key]

      if (!isEqual(newValue, oldValue)) {
        if (newValue instanceof File) {
          filesToAdd[`${reference}/${key}`] = newValue
          if (oldValue) {
            filesToRemove[`${reference}`] = {
              type: 'file',
              uri: oldValue,
            }
          }
        } else if (
          isObject(oldValue) &&
          oldValue.type === 'file' &&
          ((isObject(newValue) && newValue.uri === null) || newValue === null)
        ) {
          data[`${reference}/${key}`] = null
          filesToRemove[`${reference}/${key}`] = oldValue
        } else if ((isObject(oldValue) || !oldValue) && isObject(newValue)) {
          changes(newValue, oldValue, `${reference}/${key}`)
          // }
          // else if (!oldValue && isObject(newValue)) {
          //   console.info('reference: ', reference)
          //   console.info('key: ', key)
          //   console.info('newValue file: ', newValue)
          //   files[`${reference}/${key}`] = newValue
          //   flatten(newValue, `${reference}/${key}`, files)
        } else {
          data[`${reference}/${key}`] = newValue || null
        }
      }
    })
  }

  changes(newData, oldData, pathPrefix)

  return { data, filesToAdd, filesToRemove }
}
