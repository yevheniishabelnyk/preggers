import swedishMessages from 'aor-language-swedish'
import { englishMessages } from 'admin-on-rest'

import en from './en'
import sv from './sv'

export const messages = {
  sv: Object.assign({}, swedishMessages, sv),
  en: Object.assign({}, englishMessages, en),
}
