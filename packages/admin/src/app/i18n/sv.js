export const messages = {
  post: {
    form: {
      summary: 'Summary',
      body: 'Body',
      miscellaneous: 'Miscellaneous',
      comments: 'Comments',
    },
  },
}

export default messages
