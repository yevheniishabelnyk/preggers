import { Api } from './Api'
import * as firebase from 'firebase'

export class AlgoliaApi extends Api {
  static async updateIndex(indexId, locale) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/algolia/update-index', token)
      .send({
        indexId,
        locale,
      })
      .catch(this.catch)
  }
}
