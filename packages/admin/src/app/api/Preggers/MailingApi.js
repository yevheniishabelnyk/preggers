import { Api } from './Api'
import * as firebase from 'firebase'

export class MailingApi extends Api {
  static async sendWelcomeNewUserEmail(email, week) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/mailing/new-user', token)
      .send({ email, week })
      .catch(this.catch)
  }

  static async sendWelcomeNewWeekEmail(email, week) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/mailing/new-week', token)
      .send({ email, week })
      .catch(this.catch)
  }

  static async sendInviteEmail(email, week) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/connect/send-invitation', token)
      .send({ email, week })
      .catch(this.catch)
  }
}
