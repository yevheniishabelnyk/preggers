import { Api } from './Api'
import * as firebase from 'firebase'

export class MigrationsApi extends Api {
  static async run(id) {
    const token = await firebase.auth().currentUser.getIdToken(true)

    return this.post(`/migrations/${id}`, token)
      .send({ type: 'query' })
      .catch(this.catch)
  }

  static async rollback(id) {
    const token = await firebase.auth().currentUser.getIdToken(true)

    return this.post(`/migrations/${id}`, token)
      .send({ type: 'rollback' })
      .catch(this.catch)
  }

  static async fetchMigrations() {
    const token = await firebase.auth().currentUser.getIdToken(true)

    return this.get('/migrations', token).catch(this.catch)
  }
}
