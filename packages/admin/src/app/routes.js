/**
 *
 * Custom routes
 *
 */

import React from 'react'
import { Route } from 'react-router-dom'
import FoodsGeneralRecommendations from 'pages/FoodsGeneralRecommendations'
import AboutUs from 'pages/AboutUs'
import RatingDialogSettings from 'pages/RatingDialogSettings'
import GeneralTerms from 'pages/GeneralTerms'
import MailingTesting from 'pages/MailingTesting'
import PrivacyPolicy from 'pages/PrivacyPolicy'
import AlgoliaIndices from 'pages/AlgoliaIndices'
import MailingWelcomeNewUser from 'pages/MailingWelcomeNewUser'
import MailingInvite from 'pages/MailingInvite'
import Migrations from 'pages/Migrations'
import UsersList from 'pages/UsersList'
import AppVersion from 'pages/AppVersion'

export default [
  <Route
    key="FoodsGeneralRecomendations"
    exact
    path="/content/foods-general-recommendations"
    component={FoodsGeneralRecommendations}
  />,
  <Route key="Migrations" exact path="/migrations" component={Migrations} />,
  <Route key="AboutUs" exact path="/content/about-us" component={AboutUs} />,
  <Route key="AppVersion" exact path="/appVersion" component={AppVersion} />,
  <Route
    key="GeneralTerms"
    exact
    path="/content/general-terms"
    component={GeneralTerms}
  />,
  <Route
    key="PrivacyPolicy"
    exact
    path="/content/privacy-policy"
    component={PrivacyPolicy}
  />,
  <Route
    key="RatingDialogSettings"
    exact
    path="/rating-dialog-settings"
    component={RatingDialogSettings}
  />,
  <Route
    key="AlgoliaIndices"
    exact
    path="/algolia"
    component={AlgoliaIndices}
  />,
  <Route
    key="MailingWelcomeNewUser"
    exact
    path="/mailing/welcomeNewUser"
    component={MailingWelcomeNewUser}
  />,
  <Route
    key="MailingInvite"
    exact
    path="/mailing/invite"
    component={MailingInvite}
  />,
  <Route
    key="MailingTesting"
    exact
    path="/mailing/testing"
    component={MailingTesting}
  />,

  <Route key="UsersList" exact path="/users-list" component={UsersList} />,
]
