/**
 *
 * General Terms
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { ViewTitle, showNotification, required } from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import SimpleForm from 'app/components/SimpleForm'
import { Card } from 'material-ui/Card'

import translate from 'admin-on-rest/lib/i18n/translate'

import firebase from 'firebase'

class GeneralTerms extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  state = {
    initialized: false,
    record: {},
  }

  render() {
    if (this.state.initialized) {
      return (
        <div>
          <Card>
            <ViewTitle title="General Terms" />

            <SimpleForm
              save={this._onSave}
              record={this.state.record}
              translate={translate}
            >
              <RichTextInput
                label="Description"
                validate={required}
                source="description"
                localized
                storageDir="content/generalTerms"
              />
            </SimpleForm>
          </Card>
        </div>
      )
    }

    return null
  }

  async componentWillMount() {
    const db = firebase.database()

    const aboutUsSnap = await db.ref('/content/generalTerms').once('value')

    if (aboutUsSnap.exists()) {
      const aboutUs = aboutUsSnap.val()

      this.setState({
        initialized: true,
        record: aboutUs,
      })
    } else {
      this.setState({
        initialized: true,
      })
    }
  }

  _onSave = async values => {
    const db = firebase.database()

    try {
      await db.ref('/content/generalTerms').set(values)
    } catch (err) {
      console.log(err)
    }

    this.props.dispatch(showNotification('aor.notification.updated'))

    this.setState({ record: values })
  }
}

export default connect()(GeneralTerms)
