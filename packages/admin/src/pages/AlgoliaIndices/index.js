/**
 *
 * Algolia Settings
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { ViewTitle, showNotification } from 'admin-on-rest'
import RaisedButton from 'material-ui/RaisedButton'
import { Card } from 'material-ui/Card'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'

import { AlgoliaApi } from 'app/api/Preggers'
import { APP_LOCALES } from 'config'

const env =
  process.env.REACT_APP__ENVIRONMENT === 'production'
    ? ''
    : `__${process.env.REACT_APP__ENVIRONMENT}`

class AlgoliaIndices extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  tableState = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: false,
    selectable: false,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: true,
    showCheckboxes: false,
    height: '400px',
  }

  tableData = APP_LOCALES.map(locale => ({
    indexId: `preggers-knowledge-items--${locale}${env}`,
    locale,
  }))

  render() {
    return (
      <div>
        <Card style={{ minHeight: 500, paddingLeft: 35, paddingRight: 35 }}>
          <ViewTitle title="Algolia Indices" style={{ marginLeft: -10 }} />

          <Table
            height={this.tableState.height}
            selectable={this.tableState.selectable}
          >
            <TableHeader
              displaySelectAll={this.tableState.showCheckboxes}
              adjustForCheckbox={this.tableState.showCheckboxes}
              enableSelectAll={this.tableState.enableSelectAll}
            >
              <TableRow>
                <TableHeaderColumn style={{ width: 30 }}>№</TableHeaderColumn>
                <TableHeaderColumn>Index ID</TableHeaderColumn>
                <TableHeaderColumn>Actions</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={this.tableState.showCheckboxes}>
              {this.tableData.map((row, index) => (
                <TableRow key={index}>
                  <TableRowColumn style={{ width: 30 }}>
                    {index + 1}
                  </TableRowColumn>

                  <TableRowColumn>{row.indexId}</TableRowColumn>

                  <IndexActions
                    id={row.indexId}
                    locale={row.locale}
                    onOpen={this._openAlgoliaIndex}
                    onUpdate={this._updateAlgoliaIndex}
                  />
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Card>
      </div>
    )
  }

  _openAlgoliaIndex = indexId =>
    window.open(
      `https://www.algolia.com/apps/${
        process.env.REACT_APP__ALGOLIA__APP_ID
      }/explorer/browse/${indexId}`,
      '_blank'
    )

  _updateAlgoliaIndex = async (indexId, locale) => {
    try {
      await AlgoliaApi.updateIndex(indexId, locale)

      this.props.dispatch(showNotification('aor.notification.updated'))
    } catch (err) {
      console.log('err', err)

      if (err.message) {
        this.props.dispatch(showNotification(err.message))
      } else {
        this.props.dispatch(showNotification('Sorry, something went wrong.'))
      }
    }
  }
}

class IndexActions extends React.Component {
  static propTypes = {
    id: PropTypes.string,
    locale: PropTypes.string,
    onOpen: PropTypes.func,
    onUpdate: PropTypes.func,
  }

  render() {
    return (
      <TableRowColumn>
        <RaisedButton
          label="Open"
          onClick={this._open}
          style={{ marginRight: 15 }}
        />
        <RaisedButton label="Update" primary onClick={this._update} />
      </TableRowColumn>
    )
  }

  _open = () => {
    const { id, onOpen } = this.props

    onOpen(id)
  }

  _update = () => {
    const { id, locale, onUpdate } = this.props

    onUpdate(id, locale)
  }
}

export default connect()(AlgoliaIndices)
