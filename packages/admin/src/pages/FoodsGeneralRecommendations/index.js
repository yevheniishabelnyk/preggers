/**
 *
 * FoodsGeneralRecomendations
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { ViewTitle, showNotification } from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import SimpleForm from 'app/components/SimpleForm'
import { Card } from 'material-ui/Card'

import { isEmpty } from 'lodash'

import translate from 'admin-on-rest/lib/i18n/translate'

import firebase from 'firebase'

class FoodsGeneralRecomendations extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  state = {
    initialized: false,
    record: {},
  }

  render() {
    if (!this.state.initialized) {
      return null
    }

    return (
      <div>
        <Card>
          <ViewTitle title="Foods General Recommendations" />

          <SimpleForm
            save={this._onSave}
            record={this.state.record}
            translate={translate}
          >
            <RichTextInput
              label="Text"
              source="text"
              // validate={[required]}
              addLabel={false}
              localized
              storageDir="content/foodsGeneralRecommendations"
            />
          </SimpleForm>
        </Card>
      </div>
    )
  }

  async componentWillMount() {
    const db = firebase.database()

    const recommendationsSnap = await db
      .ref('/content/foodsGeneralRecommendations')
      .once('value')

    if (recommendationsSnap.exists()) {
      const recommendations = recommendationsSnap.val()

      this.setState({
        initialized: true,
        record: recommendations || {},
      })
    } else {
      this.setState({
        initialized: true,
        record: {},
      })
    }
  }

  _onSave = async values => {
    if (!isEmpty(values)) {
      const db = firebase.database()

      const data = Object.keys(values.text).reduce(
        (out, prop) => {
          if (values.text[prop] !== '') {
            out.text[prop] = values.text[prop]
          }

          return out
        },
        { text: {} }
      )

      await db.ref('/content/foodsGeneralRecommendations').set(data)

      if (!isEmpty(this.state.record)) {
        this.props.dispatch(showNotification('aor.notification.updated'))
      } else {
        this.props.dispatch(showNotification('aor.notification.created'))

        this.setState({
          record: data,
        })
      }
    }
  }
}

export default connect()(FoodsGeneralRecomendations)
