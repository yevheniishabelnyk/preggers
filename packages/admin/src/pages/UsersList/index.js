import React from 'react'

import firebase from 'firebase'

import Checkbox from 'material-ui/Checkbox'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'

import moment from 'moment'
import { sha256 } from '../../utils/sha256'

import { uniqBy, intersectionBy, difference, keys, values } from 'lodash'

const Json2csvParser = require('json2csv').Parser

const styles = {
  container: {
    maxWidth: '100%',
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 20,
  },
  itemWrapper: {
    display: 'flex',
    flexDirection: 'row',
    padding: 20,
    justifyContent: 'space-between',
  },
  item: {
    width: '23%',
    overflow: 'hidden',
  },
  h1: {
    paddingLeft: 20,
  },
  downloadButton: {
    marginLeft: 20,
  },
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  todosTitle: {
    fontSize: 16,
    margin: 0,
    marginBottom: 6,
  },
}

class UsersList extends React.Component {
  state = {
    hasActivePregnancy: true,
    fromPregnancyWeekValue: '',
    toPregnancyWeekValue: '',
    hasChild: true,
    fromChildAgeValue: '',
    toChildAgeValue: '',
    doneTodosList: [],
    notDoneTodosList: [],
    todos: [],
  }

  render() {
    const { doneTodosList, notDoneTodosList } = this.state

    const doneTodosListLength = Boolean(doneTodosList.length)
    const notDoneTodosListLength = Boolean(notDoneTodosList.length)

    return (
      <div style={styles.container}>
        <h1 style={styles.h1}>Generate CSV file by criteria</h1>

        <div style={styles.itemWrapper}>
          <div style={styles.item}>
            <Checkbox
              label="Has active pregnancy"
              checked={this.state.hasActivePregnancy}
              onCheck={this._setHasPregnancy}
              style={styles.checkbox}
            />

            <TextField
              id="pregnancy-week-from"
              hintText="In pregnancy week from(0-42)"
              value={this.state.fromPregnancyWeekValue}
              onChange={this._setFromPragnancyWeek}
            />

            <br />

            <TextField
              id="pregnancy-week-to"
              hintText="In pregnancy week to(0-42)"
              value={this.state.toPregnancyWeekValue}
              onChange={this._setToPregnancyWeek}
            />
          </div>

          <div style={styles.item}>
            <Checkbox
              label="Has child"
              checked={this.state.hasChild}
              onCheck={this._setHasChild}
              style={styles.checkbox}
            />

            <TextField
              id="text-field-controlled"
              hintText="Has child from X months"
              value={this.state.fromChildAgeValue}
              onChange={this._setFromChildAge}
            />

            <br />

            <TextField
              id="text-field-controlled"
              hintText="Has child up to X months"
              value={this.state.toChildAgeValue}
              onChange={this._setToChildAge}
            />
          </div>

          <div style={styles.item}>
            <p style={styles.todosTitle}>Todos are done</p>

            <SelectField
              multiple
              value={this.state.doneTodosList}
              onChange={this.handleChangeDoneTodos}
              selectionRenderer={this.selectionRenderer}
              disabled={notDoneTodosListLength ? true : false}
            >
              {this.doneTodosMenuItems(this.state.todos)}
            </SelectField>
          </div>

          <div style={styles.item}>
            <p style={styles.todosTitle}>Todos are not done</p>

            <SelectField
              multiple
              value={this.state.notDoneTodosList}
              onChange={this.handleChangeNotDoneTodos}
              selectionRenderer={this.selectionRenderer}
              disabled={doneTodosListLength ? true : false}
            >
              {this.notDoneTodosMenuItems(this.state.todos)}
            </SelectField>
          </div>
        </div>

        <RaisedButton
          label="Download CSV file"
          primary
          style={styles.downloadButton}
          onClick={this._downloadButtonPressed}
        />
      </div>
    )
  }

  async componentWillMount() {
    const checklistItemsSnap = await firebase
      .database()
      .ref('/content/checklistItems')
      .once('value')

    if (checklistItemsSnap.exists()) {
      const checklists = checklistItemsSnap.val()

      keys(checklists).map(key => {
        keys(checklists[key].todos).map(todoKey => {
          this.setState(prevState => ({
            todos: [
              ...prevState.todos,
              {
                value: todoKey,
                label: checklists[key].todos[todoKey].en,
              },
            ],
          }))
        })
      })
    }
  }

  doneTodosMenuItems(todos) {
    return todos.map(todo => (
      <MenuItem
        key={todo.value}
        insetChildren
        checked={this.state.doneTodosList.indexOf(todo.value) > -1}
        value={todo.value}
        primaryText={todo.label}
      />
    ))
  }

  handleChangeDoneTodos = (event, index, doneTodosList) =>
    this.setState({ doneTodosList })

  notDoneTodosMenuItems(todos) {
    return todos.map(todo => (
      <MenuItem
        key={todo.value}
        insetChildren
        checked={this.state.notDoneTodosList.indexOf(todo.value) > -1}
        value={todo.value}
        primaryText={todo.label}
      />
    ))
  }

  handleChangeNotDoneTodos = (event, index, notDoneTodosList) =>
    this.setState({ notDoneTodosList })

  selectionRenderer = values => {
    switch (values.length) {
      case 0:
        return ''
      default:
        return `${values.length} todos selected`
    }
  }

  _setValue = (key, event) => this.setState({ [key]: event.target.value })

  _setHasPregnancy = () => {
    this.setState(prevState => {
      return {
        hasActivePregnancy: !prevState.hasActivePregnancy,
      }
    })
  }

  _setHasChild = () => {
    this.setState(prevState => {
      return {
        hasChild: !prevState.hasChild,
      }
    })
  }

  _setFromPragnancyWeek = value =>
    this._setValue('fromPregnancyWeekValue', value)

  _setToPregnancyWeek = value => this._setValue('toPregnancyWeekValue', value)

  _setFromChildAge = value => this._setValue('fromChildAgeValue', value)

  _setToChildAge = value => this._setValue('toChildAgeValue', value)

  _getDataByCriterias = async ({
    hasActivePregnancy = true,
    fromPregnancyWeekValue = 0,
    toPregnancyWeekValue = 42,
    hasChild,
    fromChildAgeValue,
    toChildAgeValue,
  }) => {
    const fields = ['id', 'email']

    const usersSnap = await firebase
      .database()
      .ref('/users')
      .once('value')

    if (usersSnap.exists()) {
      const users = usersSnap.val()

      let pregnancyArray = [],
        childArray = [],
        todosArray = [],
        fromPregnancyWeekDueDateFormatted,
        toPregnancyWeekDueDateFormatted,
        fromChildAgeInMonthsFormatted,
        toChildAgeInMonthsFormatted

      fromPregnancyWeekDueDateFormatted = this._getStartPregnancyWeek(
        fromPregnancyWeekValue
      )

      toPregnancyWeekDueDateFormatted = this._getEndPregnancyWeek(
        toPregnancyWeekValue
      )

      if (fromChildAgeValue) {
        fromChildAgeInMonthsFormatted = this._getStartChildAge(
          fromChildAgeValue
        )
      }

      if (toChildAgeValue) {
        toChildAgeInMonthsFormatted = this._getEndChildAge(fromChildAgeValue)
      }

      if (hasActivePregnancy) {
        pregnancyArray = await this._getUsersWithPregnancy(
          users,
          toPregnancyWeekDueDateFormatted,
          fromPregnancyWeekDueDateFormatted
        )
      }

      if (hasChild) {
        childArray = await this._getUsersWithChild(
          users,
          fromChildAgeInMonthsFormatted,
          toChildAgeInMonthsFormatted
        )
      }

      const completedTodosRequested = this.state.doneTodosList.length
      const noneCompleteTodosRequested = this.state.notDoneTodosList.length

      todosArray = await this._getTodosList(
        users,
        completedTodosRequested,
        noneCompleteTodosRequested
      )

      const isEmptyTodosList = this._checkForEmptyTodosList(
        completedTodosRequested,
        noneCompleteTodosRequested,
        todosArray
      )

      let resultArray = []

      if (hasActivePregnancy && hasChild && todosArray.length) {
        resultArray = intersectionBy(
          pregnancyArray,
          childArray,
          todosArray,
          'id'
        )
      } else if (hasActivePregnancy && hasChild) {
        resultArray = intersectionBy(pregnancyArray, childArray, 'id')

        if (isEmptyTodosList) {
          resultArray = []
        }
      } else if (hasActivePregnancy && todosArray.length) {
        resultArray = intersectionBy(pregnancyArray, todosArray, 'id')
      } else if (hasActivePregnancy) {
        resultArray = uniqBy(pregnancyArray, 'id')

        if (isEmptyTodosList) {
          resultArray = []
        }
      } else if (hasChild && todosArray.length) {
        resultArray = intersectionBy(childArray, todosArray, 'id')
      } else if (hasChild) {
        resultArray = uniqBy(childArray, 'id')

        if (isEmptyTodosList) {
          resultArray = []
        }
      } else if (todosArray.length) {
        resultArray = uniqBy(todosArray, 'id')
      } else {
        values(users).map(user => {
          resultArray.push({
            id: user.id,
            email: sha256(user.email),
          })
        })
      }

      const json2csvParser = new Json2csvParser({ fields, quote: '' })
      const csv = json2csvParser.parse(resultArray)

      this._downloadCsvFile(csv)
    }
  }

  _getUsersWithPregnancy = async (
    users,
    toPregnancyWeekDueDateFormatted,
    fromPregnancyWeekDueDateFormatted
  ) => {
    const pregnanciesSnap = await firebase
      .database()
      .ref('/pregnancies')
      .once('value')

    let array = []

    if (pregnanciesSnap.exists()) {
      const pregnancies = pregnanciesSnap.val()

      values(pregnancies).map(pregnancy => {
        if (pregnancy && pregnancy.status === 'active') {
          if (
            moment(pregnancy.dueDate).isSameOrAfter(
              toPregnancyWeekDueDateFormatted
            ) &&
            moment(pregnancy.dueDate).isSameOrBefore(
              fromPregnancyWeekDueDateFormatted
            )
          ) {
            array.push({
              id: pregnancy.createdBy,
              email: users[pregnancy.createdBy]
                ? sha256(users[pregnancy.createdBy].email)
                : null,
            })
          }
        }
      })
    }

    return array
  }

  _getUsersWithChild = async (
    users,
    fromChildAgeInMonthsFormatted,
    toChildAgeInMonthsFormatted
  ) => {
    const childrenSnap = await firebase
      .database()
      .ref('/children')
      .once('value')

    let array = []

    if (childrenSnap.exists()) {
      const children = childrenSnap.val()

      values(children).map(child => {
        if (child) {
          const user = users[child.createdBy]

          const childBirthday = moment(child.birthday)

          if (fromChildAgeInMonthsFormatted && toChildAgeInMonthsFormatted) {
            if (
              childBirthday.isSameOrBefore(fromChildAgeInMonthsFormatted) &&
              childBirthday.isAfter(toChildAgeInMonthsFormatted)
            ) {
              array.push({
                id: user.id,
                email: sha256(user.email),
              })
            }
          } else if (fromChildAgeInMonthsFormatted) {
            if (childBirthday.isSameOrBefore(fromChildAgeInMonthsFormatted)) {
              array.push({
                id: user.id,
                email: sha256(user.email),
              })
            }
          } else if (toChildAgeInMonthsFormatted) {
            if (childBirthday.isAfter(toChildAgeInMonthsFormatted)) {
              array.push({
                id: user.id,
                email: sha256(user.email),
              })
            }
          } else {
            array.push({
              id: user.id,
              email: sha256(user.email),
            })
          }
        }
      })
    }

    return array
  }

  _getTodosList = async (
    users,
    completeTodosRequested,
    noneCompleteTodosRequested
  ) => {
    const todosSnap = await firebase
      .database()
      .ref('/userOwned/completedAdminTodosByUser')
      .once('value')

    let array = []

    if (todosSnap.exists()) {
      const todos = todosSnap.val()

      keys(todos).map(key => {
        let keysOfTodos = Object.keys(todos[key])

        if (completeTodosRequested && !noneCompleteTodosRequested) {
          const differenceLength = difference(
            this.state.doneTodosList,
            keysOfTodos
          ).length

          if (differenceLength === 0) {
            if (users[key]) {
              array.push({
                id: users[key].id,
                email: sha256(users[key].email),
              })
            }
          }
        }

        if (noneCompleteTodosRequested && !completeTodosRequested) {
          const differenceLength = difference(
            this.state.notDoneTodosList,
            keysOfTodos
          ).length

          if (differenceLength === noneCompleteTodosRequested) {
            if (users[key]) {
              array.push({
                id: users[key].id,
                email: sha256(users[key].email),
              })
            }
          }
        }
      })
    }

    return array
  }

  _getStartPregnancyWeek = startPregnancyWeek =>
    moment()
      .add(286 - 7 * startPregnancyWeek, 'days')
      .format('YYYY-MM-DD')

  _getEndPregnancyWeek = endPregnancyWeek =>
    moment()
      .add(280 - 7 * endPregnancyWeek, 'days')
      .format('YYYY-MM-DD')

  _getStartChildAge = startChildAge =>
    moment()
      .add(-startChildAge, 'months')
      .format('YYYY-MM-DD')

  _getEndChildAge = endChildAge =>
    moment()
      .add(-endChildAge - 1, 'months')
      .format('YYYY-MM-DD')

  _checkForEmptyTodosList = (
    completedTodosRequested,
    noneCompleteTodosRequested,
    todosArray
  ) => {
    if (
      (completedTodosRequested || noneCompleteTodosRequested) &&
      !todosArray.length
    ) {
      return true
    }
  }

  _downloadCsvFile = csv => {
    const csvLink = document.createElement('a')
    const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' })
    const url = URL.createObjectURL(blob)

    csvLink.href = url
    csvLink.setAttribute('download', `users-${moment()}.csv`)
    csvLink.click()
  }

  _downloadButtonPressed = () => {
    const {
      hasActivePregnancy,
      fromPregnancyWeekValue,
      toPregnancyWeekValue,
      hasChild,
      fromChildAgeValue,
      toChildAgeValue,
    } = this.state

    const criteria = {
      hasActivePregnancy: hasActivePregnancy,
      fromPregnancyWeekValue: Number(fromPregnancyWeekValue) || 0,
      toPregnancyWeekValue: Number(toPregnancyWeekValue) || 42,
      hasChild: hasChild,
      fromChildAgeValue: Number(fromChildAgeValue) || 0,
      toChildAgeValue: Number(toChildAgeValue) || 0,
    }

    this._getDataByCriterias(criteria)
  }
}

export default UsersList
