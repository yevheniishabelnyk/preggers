/**
 *
 * Rating Dialog Settings
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  FormTab,
  ViewTitle,
  showNotification,
  NumberInput,
} from 'admin-on-rest'
import TabbedForm from 'app/components/TabbedForm'
import { Card } from 'material-ui/Card'

import translate from 'admin-on-rest/lib/i18n/translate'

import firebase from 'firebase'

const TabTitle = props => <span>{props.title}</span>

TabTitle.propTypes = {
  title: PropTypes.string,
}

class RatingDialogSettings extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  state = {
    initialized: false,
    record: {},
  }

  render() {
    if (this.state.initialized) {
      return (
        <div>
          <Card>
            <ViewTitle title="Rating Settings" />

            <TabbedForm
              save={this._onSave}
              record={this.state.record}
              translate={translate}
            >
              <FormTab
                label={
                  <TabTitle title="Set days values for show Rating popup" />
                }
              >
                <NumberInput
                  label="First show after X days:"
                  source="firstShow"
                />

                <NumberInput
                  label="Second show after X days:"
                  source="secondShow"
                />

                <NumberInput
                  label="Turned off for X days:"
                  source="turnedOff"
                />
              </FormTab>
            </TabbedForm>
          </Card>
        </div>
      )
    }

    return null
  }

  async componentWillMount() {
    const db = firebase.database()

    const ratingSettingsSnap = await db
      .ref('/ratingDialogSettings')
      .once('value')

    if (ratingSettingsSnap.exists()) {
      const ratingSettings = ratingSettingsSnap.val()

      this.setState({
        initialized: true,
        record: ratingSettings,
      })
    } else {
      this.setState({
        initialized: true,
      })
    }
  }

  _onSave = async values => {
    const db = firebase.database()

    try {
      await db.ref('/ratingDialogSettings').set(values)
    } catch (err) {
      console.log(err)
    }

    this.props.dispatch(showNotification('aor.notification.updated'))

    this.setState({ record: values })
  }
}

export default connect()(RatingDialogSettings)
