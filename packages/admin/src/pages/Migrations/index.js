/**
 *
 * Migrations page
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import PageContent from 'app/components/PageContent'
import { Tabs, Tab } from 'material-ui/Tabs'
import MigrationsHistoryTable from 'shared/components/MigrationsHistoryTable'
import MigrationListTable from 'shared/components/MigrationListTable'

import { showNotification } from 'admin-on-rest'
import { MigrationsApi } from 'app/api/Preggers'

import { values } from 'lodash'

import * as firebase from 'firebase'

class Migrations extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    isAuthEmpty: PropTypes.bool,
  }

  state = {
    migrations: [],
    migrationsHistory: [],
  }

  render() {
    const { migrations, migrationsHistory } = this.state

    return (
      <PageContent>
        <Tabs>
          <Tab label="List">
            <MigrationListTable
              migrations={migrations}
              onRun={this._onRunMigration}
              onRollback={this._onRollbackMigration}
            />
          </Tab>

          <Tab label="History">
            <MigrationsHistoryTable migrationsHistory={migrationsHistory} />
          </Tab>
        </Tabs>
      </PageContent>
    )
  }

  componentWillMount() {
    this._listenMigrationHistory()

    if (!this.props.isAuthEmpty) {
      this._fetchMigrationList()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isAuthEmpty !== nextProps.isAuthEmpty) {
      this._fetchMigrationList()
    }
  }

  componentWillUnmount() {
    this._stopListeningMigrationHistory()
  }

  _updateMigrationStatuses = () => {
    const { migrationsHistory, migrations } = this.state

    this.setState({
      migrations: migrations.map(migration => {
        const lastMigrationHistoryTicket = migrationsHistory
          .filter(ticket => ticket.migrationId === migration.id)
          .sort((a, b) => a.migratedAt < b.migratedAt)[0]

        const status =
          lastMigrationHistoryTicket &&
          lastMigrationHistoryTicket.type === 'query'
            ? 'done'
            : 'pending'

        return {
          ...migration,
          status,
        }
      }),
    })
  }

  _onRunMigration = async migrationId => {
    const { showNotification } = this.props

    try {
      await MigrationsApi.run(migrationId)

      showNotification(`Success!`)
    } catch (err) {
      showNotification(
        `Error: ${
          err.message
            ? err.message
            : 'Something went wrong. Please try again later.'
        }`
      )
    }
  }

  _onRollbackMigration = async migrationId => {
    const { showNotification } = this.props

    try {
      await MigrationsApi.rollback(migrationId)

      showNotification(`Success!`)
    } catch (err) {
      showNotification(
        `Error: ${
          err.message
            ? err.message
            : 'Something went wrong. Please try again later.'
        }`
      )
    }
  }

  _listenMigrationHistory = () => {
    firebase
      .database()
      .ref('/migrationsHistory')
      .on('value', snap => {
        this.setState({ migrationsHistory: values(snap.val()) }, () =>
          this._updateMigrationStatuses()
        )
      })
  }

  _stopListeningMigrationHistory = () => {
    firebase
      .database()
      .ref('/migrationsHistory')
      .off()
  }

  _fetchMigrationList = async () => {
    try {
      const response = await MigrationsApi.fetchMigrations()

      const { migrations } = response.body

      this.setState({ migrations }, () => this._updateMigrationStatuses())
    } catch (err) {
      console.log('err', err)
    }
  }
}

export default connect(
  state => ({
    isAuthEmpty: state.auth.isEmpty,
  }),
  { showNotification }
)(Migrations)
