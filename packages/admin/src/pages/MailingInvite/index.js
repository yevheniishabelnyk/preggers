/**
 *
 * Mailing Invite
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  ViewTitle,
  showNotification,
  TextInput,
  required,
  LongTextInput,
} from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'

import checkUpdates from 'app/restClient/utils/checkUpdates'
import uploadFile from 'app/restClient/utils/uploadFile'

import { Card } from 'material-ui/Card'

import translate from 'admin-on-rest/lib/i18n/translate'

import MergeTags from 'resources/weeks/components/MergeTags'
import API_MERGE_TAGS from './API_MERGE_TAGS'
import TEMPLATE_MERGE_TAGS from './TEMPLATE_MERGE_TAGS'

import firebase from 'firebase'

const styles = {
  textInput: {
    width: 800,
  },
}

class MailingInvite extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  state = {
    initialized: false,
    record: {},
  }

  render() {
    if (this.state.initialized) {
      return (
        <div>
          <Card>
            <ViewTitle title="Invitation" />

            <SimpleForm
              save={this._onSave}
              record={this.state.record}
              translate={translate}
            >
              <LongTextInput
                label="Email subject"
                source="subject"
                validate={[required]}
                defaultValue="You are invited!"
                localized
                style={styles.textInput}
              />

              <LongTextInput
                label="INVITE_TITLE"
                source="title"
                localized
                style={styles.textInput}
              />

              <LongTextInput
                label="INVITE_HEADER"
                source="header"
                localized
                style={styles.textInput}
              />

              <LongTextInput
                label="INVITE_TEXT"
                source="text"
                localized
                style={styles.textInput}
              />

              <TextInput
                label="CODE_LABEL"
                source="codeLabel"
                localized
                style={styles.textInput}
              />

              <TextInput
                label="BUTTON_TITLE"
                source="buttonTitle"
                localized
                style={styles.textInput}
              />

              <TextInput
                label="BUTTON_LINK"
                source="buttonLink"
                style={styles.textInput}
              />
            </SimpleForm>
          </Card>

          <MergeTags
            title="Api Merge Tags"
            subtitle="Use Handlebars syntax for api tags"
            tags={API_MERGE_TAGS}
            handlebars
            hideBlackLines
          />

          <MergeTags
            title="MailChimp template Merge Tags"
            subtitle="All api tags are MailChimp tempate tags"
            tags={TEMPLATE_MERGE_TAGS}
          />
        </div>
      )
    }

    return null
  }

  async componentWillMount() {
    const db = firebase.database()

    const snap = await db.ref('/mailing/invite').once('value')

    if (snap.exists()) {
      const record = snap.val()

      this.setState({
        initialized: true,
        record,
      })
    } else {
      this.setState({
        initialized: true,
      })
    }
  }

  _onSave = async values => {
    const recordPath = 'mailing/invite'
    const newData = values
    const oldData = this.state.record
    const { data: databaseDiff, filesToAdd, filesToRemove } = checkUpdates(
      newData,
      oldData,
      recordPath
    )

    try {
      let fileUrl

      console.info('databaseDiff: ', databaseDiff)

      await firebase
        .database()
        .ref()
        .update(databaseDiff)

      const fileToAddPromises = Object.keys(filesToAdd).map(key => async () => {
        const file = filesToAdd[key]
        const fileUrlDatabaseLocation = key
        const fileTypeDatabaseLocation = key.replace(/[^/]*$/, 'type')

        fileUrl = await uploadFile(file, 'mailing/invite')

        await firebase
          .database()
          .ref()
          .update({
            [fileUrlDatabaseLocation]: fileUrl,
            [fileTypeDatabaseLocation]: 'file',
          })
      })

      const fileToRemovePromises = Object.keys(filesToRemove).map(
        fileDatabaseLocation => async () => {
          const file = filesToRemove[fileDatabaseLocation]

          const fileStoragePath = decodeURIComponent(
            file.uri.split(/[?#]/)[0].match(/[^/]*$/)[0]
          )

          const storageRef = firebase.storage().ref()

          // console.info('fileStoragePath: ', fileStoragePath)

          const desertRef = storageRef.child(fileStoragePath)

          await desertRef.delete()
          // console.info(`file ${fileStoragePath} deleted: `)
        }
      )

      await Promise.all([
        Promise.all(fileToAddPromises.map(promise => promise())),
        Promise.all(fileToRemovePromises.map(promise => promise())),
      ])

      this.setState({ record: newData })

      this.props.dispatch(showNotification('aor.notification.updated'))
    } catch (err) {
      console.log(err)

      this.props.dispatch(showNotification('Sorry, something went wrong!'))
    }
  }
}

export default connect()(MailingInvite)
