export default [
  {
    name: 'FROM_USER_NAME',
    description: 'From user name',
  },
  {
    name: 'HAS_FROM_USER_NAME',
    description: 'Has from user name',
  },
  {
    name: 'FROM_USER_LOCALE',
    description: 'User locale',
  },
  {
    name: 'HAS_FROM_USER_LOCALE',
    description: 'From user locale',
  },
  {
    name: 'FROM_USER_COUNTRY_CODE',
    description: 'From user counrty code',
  },
  {
    name: 'HAS_FROM_USER_COUNTRY_CODE',
    description: 'Has from user counrty code',
  },
  {
    name: 'TO_USER_RELATIONSHIP_TYPE',
    description: 'To user relationship type',
  },
  {
    name: 'HAS_TO_USER_RELATIONSHIP_TYPE',
    description: 'Has to user relationship type',
  },
  {
    name: 'CODE',
    description: 'Code',
  },
  {
    name: 'INVITE_TITLE',
    description: 'Invite title',
  },
  {
    name: 'HAS_INVITE_TITLE',
    description: 'Has invite title',
  },
  {
    name: 'INVITE_BUTTON_TITLE',
    description: 'Invite button title',
  },
  {
    name: 'HAS_INVITE_BUTTON_TITLE',
    description: 'Has invite button title',
  },
  {
    name: 'INVITE_BUTTON_LINK',
    description: 'Invite button link',
  },
  {
    name: 'HAS_INVITE_BUTTON_LINK',
    description: 'Has invite button link',
  },
  {
    name: 'INVITE_IMAGE',
    description: 'Invite image',
  },
  {
    name: 'HAS_INVITE_IMAGE',
    description: 'Has invite image',
  },
  {
    name: 'INVITE_HEADER',
    description: 'Invite header',
  },
  {
    name: 'HAS_INVITE_HEADER',
    description: 'Has invite header',
  },
  {
    name: 'INVITE_TEXT',
    description: 'Invite text',
  },
  {
    name: 'HAS_INVITE_TEXT',
    description: 'Has invite text',
  },
  {
    name: 'INVITE_CODE_LABEL',
    description: 'Invite code label',
  },
  {
    name: 'HAS_INVITE_CODE_LABEL',
    description: 'Has invite code label',
  },
]
