export default [
  {
    name: 'FROM_USER_NAME',
    description: 'From user name',
  },
  {
    name: 'TO_USER_RELATIONSHIP_TYPE',
    description: 'To user relationship type',
  },
]
