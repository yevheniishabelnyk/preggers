/**
 *
 * App Version
 *
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { ViewTitle, showNotification, required, TextInput } from 'admin-on-rest'

import SimpleForm from 'app/components/SimpleForm'

import { Card } from 'material-ui/Card'

import translate from 'admin-on-rest/lib/i18n/translate'

import firebase from 'firebase'

class AppVersion extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  state = {
    record: {},
  }

  render() {
    return (
      <div>
        <Card>
          <ViewTitle title="App Versions" />

          <SimpleForm
            save={this._onSave}
            record={this.state.record}
            translate={translate}
          >
            <TextInput label="IOS Version" source="ios" validate={required} />

            <TextInput
              label="Android Version"
              source="android"
              validate={required}
            />

            <TextInput
              label="Notification Text"
              source="updateAvailableNotification.text"
              validate={required}
              localized
            />

            <TextInput
              label="Notification Android link"
              source="updateAvailableNotification.link.android"
              validate={required}
            />

            <TextInput
              label="Notification IOS link"
              source="updateAvailableNotification.link.ios"
              validate={required}
            />
          </SimpleForm>
        </Card>
      </div>
    )
  }

  async componentWillMount() {
    const db = firebase.database()

    const versionData = await db.ref('/appVersion').once('value')

    if (versionData.exists()) {
      const version = versionData.val()

      this.setState({
        record: version,
      })
    }
  }

  _onSave = async values => {
    try {
      await firebase
        .database()
        .ref('appVersion')
        .update(values)

      this.props.dispatch(showNotification('aor.notification.updated'))
    } catch (err) {
      console.log('Error: ', err)
    }
  }
}

export default connect()(AppVersion)
