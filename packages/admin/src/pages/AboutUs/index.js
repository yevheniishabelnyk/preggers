/**
 *
 * About Us
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  ViewTitle,
  showNotification,
  ImageField,
  required,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import SimpleForm from 'app/components/SimpleForm'
import FileInput from 'app/components/FileInput'

import checkUpdates from 'app/restClient/utils/checkUpdates'
import uploadFile from 'app/restClient/utils/uploadFile'

import { Card } from 'material-ui/Card'

import translate from 'admin-on-rest/lib/i18n/translate'

import firebase from 'firebase'

class AboutUs extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  state = {
    initialized: false,
    record: {},
  }

  render() {
    if (this.state.initialized) {
      return (
        <div>
          <Card>
            <ViewTitle title="About Us" />

            <SimpleForm
              save={this._onSave}
              record={this.state.record}
              translate={translate}
            >
              <FileInput source="image" label="Image" accept="image/*">
                <ImageField source="uri" />
              </FileInput>

              <RichTextInput
                label="Description"
                source="description"
                validate={[required]}
                localized
                storageDir="content/aboutUs"
              />
            </SimpleForm>
          </Card>
        </div>
      )
    }

    return null
  }

  async componentWillMount() {
    const db = firebase.database()

    const aboutUsSnap = await db.ref('/content/aboutUs').once('value')

    if (aboutUsSnap.exists()) {
      const aboutUs = aboutUsSnap.val()

      this.setState({
        initialized: true,
        record: aboutUs,
      })
    } else {
      this.setState({
        initialized: true,
      })
    }
  }

  _onSave = async values => {
    const recordPath = 'content/aboutUs'
    const newData = values
    const oldData = this.state.record
    const { data: databaseDiff, filesToAdd, filesToRemove } = checkUpdates(
      newData,
      oldData,
      recordPath
    )

    try {
      let fileUrl

      await firebase
        .database()
        .ref()
        .update(databaseDiff)

      const fileToAddPromises = Object.keys(filesToAdd).map(key => async () => {
        const file = filesToAdd[key]
        const fileUrlDatabaseLocation = key
        const fileTypeDatabaseLocation = key.replace(/[^/]*$/, 'type')

        fileUrl = await uploadFile(file, 'content/aboutUs')

        await firebase
          .database()
          .ref()
          .update({
            [fileUrlDatabaseLocation]: fileUrl,
            [fileTypeDatabaseLocation]: 'file',
          })
      })

      const fileToRemovePromises = Object.keys(filesToRemove).map(
        fileDatabaseLocation => async () => {
          const file = filesToRemove[fileDatabaseLocation]

          const fileStoragePath = decodeURIComponent(
            file.uri.split(/[?#]/)[0].match(/[^/]*$/)[0]
          )

          const storageRef = firebase.storage().ref()

          // console.info('fileStoragePath: ', fileStoragePath)

          const desertRef = storageRef.child(fileStoragePath)

          await desertRef.delete()
          // console.info(`file ${fileStoragePath} deleted: `)
        }
      )

      await Promise.all([
        Promise.all(fileToAddPromises.map(promise => promise())),
        Promise.all(fileToRemovePromises.map(promise => promise())),
      ])

      newData.image = fileUrl ? { type: 'file', uri: fileUrl } : newData.image

      this.setState({ record: newData })

      this.props.dispatch(showNotification('aor.notification.updated'))
    } catch (err) {
      console.log(err)
    }
  }
}

export default connect()(AboutUs)
