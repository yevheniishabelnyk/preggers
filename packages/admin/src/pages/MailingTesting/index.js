/**
 *
 * Mailing Sandbox
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  ViewTitle,
  showNotification,
  TextInput,
  SelectInput,
  SaveButton,
  Toolbar,
  NumberInput,
} from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'
import translate from 'admin-on-rest/lib/i18n/translate'
import { Card, CardHeader } from 'material-ui/Card'

import { MailingApi } from 'app/api/Preggers'

const FormToolbar = props => (
  <Toolbar {...props}>
    <SaveButton label="Send" redirect={false} submitOnEnter />
  </Toolbar>
)

class MailingTesting extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  state = {
    record: {},
  }

  emailTypeChoices = [
    { id: 'welcome_new_user', name: 'Welcome new user' },
    { id: 'welcome_new_week', name: 'Weclome new week' },
    { id: 'invite', name: 'Invite' },
  ]

  render() {
    return (
      <Card>
        <ViewTitle title="Mailing Sandbox" />

        <CardHeader subtitle="Default email for sending is your email" />

        <SimpleForm
          save={this._onSave}
          record={this.state.record}
          translate={translate}
          toolbar={<FormToolbar />}
        >
          <SelectInput
            source="type"
            choices={this.emailTypeChoices}
            defaultValue="welcome_new_week"
          />

          <TextInput label="Email" source="email" type="email" />

          <NumberInput label="Week" source="week" />
        </SimpleForm>
      </Card>
    )
  }

  _onSave = async values => {
    try {
      const week =
        values.week && Number(values.week) ? Number(values.week) : undefined

      switch (values.type) {
        case 'welcome_new_user':
          await MailingApi.sendWelcomeNewUserEmail(values.email, week)
          break

        case 'welcome_new_week':
          await MailingApi.sendWelcomeNewWeekEmail(values.email, week)
          break

        case 'invite':
          await MailingApi.sendInviteEmail(values.email, week)
          break

        default:
          this.props.dispatch(showNotification('Please choose mailing type'))
          return
      }

      this.props.dispatch(showNotification('Email sent'))
    } catch (err) {
      console.log(err)

      this.props.dispatch(
        showNotification(
          `Error: ${err.message ? err.message : 'Try again later.'}`
        )
      )
    }

    this.setState({ record: values })
  }
}

export default connect()(MailingTesting)
