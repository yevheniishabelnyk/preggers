import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { TableRow, TableRowColumn } from 'material-ui/Table'

import TrueIcon from 'material-ui/svg-icons/action/done'
import FalseIcon from 'material-ui/svg-icons/content/clear'

import { blue500, red500 } from 'material-ui/styles/colors'

import styles from './styles'

export default class MigrationHistoryTicketTableRow extends React.Component {
  static propTypes = {
    migration: PropTypes.object,
  }

  render() {
    const {
      migration: {
        id,
        migrationId,
        isIdempotent,
        hasRollback,
        description,
        migratedBy,
        migratedAt,
        type,
        appVersion,
      },
    } = this.props

    const migratedAtDate = moment(migratedAt).format('YYYY-MM-DD HH:mm')

    return (
      <TableRow key={id} selectable={false}>
        <TableRowColumn style={styles.migrationId}>
          {migrationId}
        </TableRowColumn>

        <TableRowColumn style={styles.type}>{type}</TableRowColumn>

        <TableRowColumn style={styles.appVersion}>{appVersion}</TableRowColumn>

        <TableRowColumn style={styles.migratedBy}>{migratedBy}</TableRowColumn>

        <TableRowColumn style={styles.migratedAt}>
          {migratedAtDate}
        </TableRowColumn>

        <TableRowColumn style={styles.isIdempotent}>
          {isIdempotent ? (
            <TrueIcon color={blue500} style={styles.icon} />
          ) : (
            <FalseIcon color={red500} style={styles.icon} />
          )}
        </TableRowColumn>

        <TableRowColumn style={styles.hasRollback}>
          {hasRollback ? (
            <TrueIcon color={blue500} style={styles.icon} />
          ) : (
            <FalseIcon color={red500} style={styles.icon} />
          )}
        </TableRowColumn>

        <TableRowColumn style={styles.description}>
          {description}
        </TableRowColumn>
      </TableRow>
    )
  }
}
