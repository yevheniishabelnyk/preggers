const styles = {
  migrationId: {
    width: 150,
    whiteSpace: 'wrap',
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: '15px',
    lineHeight: '21px',
  },

  type: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
  },

  appVersion: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
  },

  migratedBy: {
    textAlign: 'center',
    width: 200,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
  },

  migratedAt: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
    whiteSpace: 'wrap',
  },

  isIdempotent: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
  },

  hasRollback: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
  },

  description: {
    whiteSpace: 'wrap',
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: '15px',
    lineHeight: '21px',
    maxHeight: 100,
  },
}

export default styles
