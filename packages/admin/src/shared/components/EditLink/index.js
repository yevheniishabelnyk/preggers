/**
 *
 * EditeLink
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'
import EditIcon from 'material-ui/svg-icons/editor/mode-edit'
import IconButton from 'material-ui/IconButton'

import './style.css'

const EditeLink = ({ bathPath, tooltip = 'Edit', id, source, record }) => {
  console.info('record: ', record)
  console.info('id: ', id)
  console.info('source: ', source)

  const itemId = id || record[source]

  return (
    <Link to={{ pathname: `/${bathPath}/${itemId}` }}>
      <IconButton tooltip={tooltip}>
        <EditIcon className="edit-link__icon" />
      </IconButton>
    </Link>
  )
}

EditeLink.propTypes = {
  bathPath: PropTypes.string.isRequired,
  source: PropTypes.string.isRequired,
  tooltip: PropTypes.string,
  record: PropTypes.object,
  id: PropTypes.string,
}

export default EditeLink
