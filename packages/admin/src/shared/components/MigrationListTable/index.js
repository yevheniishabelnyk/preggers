/**
 *
 * MigrationListTable
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
} from 'material-ui/Table'
import MigrationListTableRow from 'shared/components//MigrationListTableRow'

import styles from './styles'

export default class MigrationListTable extends React.Component {
  static propTypes = {
    migrations: PropTypes.array,
    onRun: PropTypes.func,
    onRollback: PropTypes.func,
  }

  static defaultProps = {
    migrations: [],
  }

  render() {
    const { migrations, onRun, onRollback } = this.props

    return (
      <Table>
        <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
          <TableRow>
            <TableHeaderColumn style={styles.id}>ID</TableHeaderColumn>

            <TableHeaderColumn style={styles.description}>
              Description
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.appVersion}>
              App Version
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.isIdempotent}>
              Idempotent
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.status}>Status</TableHeaderColumn>

            <TableHeaderColumn style={styles.actions}>
              Actions
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>

        <TableBody displayRowCheckbox={false}>
          {migrations.map(item => (
            <MigrationListTableRow
              key={item.id}
              migration={item}
              onRun={onRun}
              onRollback={onRollback}
            />
          ))}
        </TableBody>
      </Table>
    )
  }
}
