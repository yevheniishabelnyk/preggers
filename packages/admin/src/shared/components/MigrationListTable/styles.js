export default {
  id: {
    width: 150,
    textAlign: 'center',
  },

  description: {
    textAlign: 'center',
  },

  appVersion: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  isIdempotent: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  status: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  actions: {
    textAlign: 'center',
    width: 250,
    paddingLeft: 0,
    paddingRight: 0,
  },
}
