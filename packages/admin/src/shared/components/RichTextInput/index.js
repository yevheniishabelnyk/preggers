/**
 *
 * RichTextInput
 */

import debounce from 'lodash.debounce'
import React from 'react'
import PropTypes from 'prop-types'
import Quill from 'quill'
import { isFunction } from 'lodash'

import AdPlaceholder from './adPlaceholder'

import uploadFile from 'app/restClient/utils/uploadFile'

import './style.css'

let Link = Quill.import('formats/link')

Quill.register({
  'formats/slot': AdPlaceholder,
})

Link.sanitize = function(url) {
  if (url.startsWith('preggersapp://')) {
    return url
  }

  // prefix default protocol.
  let protocol = url.slice(0, url.indexOf(':'))
  if (this.PROTOCOL_WHITELIST.indexOf(protocol) === -1) {
    // eslint-disable-next-line no-param-reassign
    url = 'http://' + url
  }

  // Link._sanitize function
  let anchor = document.createElement('a')
  anchor.href = url
  protocol = anchor.href.slice(0, anchor.href.indexOf(':'))
  return this.PROTOCOL_WHITELIST.indexOf(protocol) > -1
    ? url
    : this.SANITIZED_URL
}

Quill.register(Link, true)

let Parchment = Quill.import('parchment')
let Delta = Quill.import('delta')
let Break = Quill.import('blots/break')
let Embed = Quill.import('blots/embed')
function lineBreakMatcher() {
  let newDelta = new Delta()
  newDelta.insert({ break: '' })
  return newDelta
}

Break.prototype.insertInto = function(parent, ref) {
  Embed.prototype.insertInto.call(this, parent, ref)
}
Break.prototype.length = function() {
  return 1
}
Break.prototype.value = function() {
  return '\n'
}

class RichTextInput extends React.Component {
  componentDidMount() {
    const {
      input: { value },
    } = this.props
    const toolbarOptions = [
      [{ header: [1, 2, 3, false] }],
      ['bold', 'italic', 'underline'],
      [{ list: 'ordered' }, { list: 'bullet' }],
      ['link', 'image'],
      ['clean'],
      ['showHtml'],
      ['slot'],
    ]

    this.quill = new Quill(this.divRef, {
      modules: {
        toolbar: {
          container: toolbarOptions,
          handlers: {
            image: this.quill_img_handler,
            showHtml: this.showHtml,
            slot: this.adPlaceholderHandler,
          },
        },
        clipboard: {
          matchers: [['BR', lineBreakMatcher]],
        },
        keyboard: {
          bindings: {
            handleEnter: {
              key: 13,
              handler: function(range, context) {
                if (range.length > 0) {
                  this.quill.scroll.deleteAt(range.index, range.length) // So we do not trigger text-change
                }
                let lineFormats = Object.keys(context.format).reduce(function(
                  lineFormats,
                  format
                ) {
                  if (
                    Parchment.query(format, Parchment.Scope.BLOCK) &&
                    !Array.isArray(context.format[format])
                  ) {
                    lineFormats[format] = context.format[format]
                  }
                  return lineFormats
                },
                {})
                let previousChar = this.quill.getText(range.index - 1, 1)
                // Earlier scroll.deleteAt might have messed up our selection,
                // so insertText's built in selection preservation is not reliable
                this.quill.insertText(
                  range.index,
                  '\n',
                  lineFormats,
                  Quill.sources.USER
                )
                if (previousChar === '' || previousChar === '\n') {
                  this.quill.setSelection(range.index + 2, Quill.sources.SILENT)
                } else {
                  this.quill.setSelection(range.index + 1, Quill.sources.SILENT)
                }
                // this.quill.selection.scrollIntoView()
                Object.keys(context.format).forEach(name => {
                  if (lineFormats[name] != null) {
                    return
                  }
                  if (Array.isArray(context.format[name])) {
                    return
                  }
                  if (name === 'link') {
                    return
                  }
                  this.quill.format(
                    name,
                    context.format[name],
                    Quill.sources.USER
                  )
                })
              },
            },
            linebreak: {
              key: 13,
              shiftKey: true,
              handler: function(
                range
                // , context
              ) {
                let nextChar = this.quill.getText(range.index + 1, 1)
                // let ee = this.quill.insertEmbed(
                //   range.index,
                //   'break',
                //   true,
                //   'user'
                // )
                if (nextChar.length === 0) {
                  // second line break inserts only at the end of parent element
                  // let ee = this.quill.insertEmbed(
                  //   range.index,
                  //   'break',
                  //   true,
                  //   'user'
                  // )
                }
                this.quill.setSelection(range.index + 1, Quill.sources.SILENT)
              },
            },
          },
        },
      },
      theme: 'snow',
    })

    this.quill.pasteHTML(value)

    this.quill.root.spellcheck = false

    this.editor = this.divRef.querySelector('.ql-editor')
    this.quill.on('text-change', debounce(this.onTextChange, 500))

    this.txtArea = document.createElement('textarea')

    this.txtArea.style.cssText =
      'width: 100%;margin: 0px;background: rgb(29, 29, 29);box-sizing: border-box;color: rgb(204, 204, 204);font-size: 15px;outline: none;padding: 20px;line-height: 24px;font-family: Consolas, Menlo, Monaco, &quot;Courier New&quot;, monospace;position: absolute;top: 0;bottom: 0;border: none;display:none'

    let htmlEditor = this.quill.addContainer('ql-custom')

    htmlEditor.appendChild(this.txtArea)
  }

  adPlaceholderHandler = () => {
    const range = this.quill.getSelection()
    if (range) {
      this.quill.insertEmbed(range.index, 'slot', 'ad')
    }
  }

  showHtml = () => {
    const textarea = this.txtArea

    textarea.style.display = textarea.style.display === 'none' ? '' : 'none'

    this.quill.on('text-change', () => {
      let html = this.quill.root.innerHTML

      textarea.value = html
    })

    if (textarea.style.display === '') {
      let html = this.quill.root.innerHTML

      this.quill.pasteHTML(html)
    } else {
      this.quill.pasteHTML(textarea.value)
    }
  }

  quill_img_handler = () => {
    const { basePath } = this.props
    const storageDir = basePath ? basePath : this.props.storageDir

    if (!storageDir) {
      alert('Storage directory is required to upload file')

      return
    }

    const parentElement = document.getElementById('toolbar')

    let fileInput = parentElement.querySelector('input.ql-image[type=file]')

    if (fileInput == null) {
      fileInput = document.createElement('input')

      fileInput.setAttribute('type', 'file')

      fileInput.setAttribute(
        'accept',
        'image/png, image/gif, image/jpeg, image/bmp, image/x-icon'
      )

      fileInput.classList.add('ql-image')

      fileInput.addEventListener('change', async () => {
        const files = fileInput.files
        const range = this.quill.getSelection(true)
        const imageLink = await uploadFile(files[0], storageDir)

        this.quill.insertEmbed(range.index, 'image', imageLink)

        if (!files || !files.length) {
          return
        }
      })

      parentElement.appendChild(fileInput)
    }

    fileInput.click()
  }

  componentWillUnmount() {
    this.quill.off('text-change', this.onTextChange)
    this.quill = null
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.input.value !== nextProps.input.value &&
      this.htmlValue !== nextProps.input.value
    ) {
      this.quill.pasteHTML(nextProps.input.value)
    }
  }

  onTextChange = () => {
    this.htmlValue = this.editor.innerHTML
      .replace(
        new RegExp('<li[^>]*>([\\s\\S]*?)<\\/li>', 'g'),
        '<li><p>$1</p></li>'
      )
      .replace(/<p><br><\/p>/g, '')
      .replace(
        new RegExp('<p[^>]*><img src="(.+)"><\\/p>', 'g'),
        '<img src="$1"/>'
      )

    this.props.input.onChange(this.htmlValue)

    if (isFunction(this.props.onChangeText)) {
      this.props.onChangeText(this.htmlValue)
    }
  }

  updateDivRef = ref => {
    this.divRef = ref
  }

  render() {
    return (
      <div className="aor-rich-text-input" id="toolbar">
        <div ref={this.updateDivRef} />
      </div>
    )
  }
}

RichTextInput.propTypes = {
  addField: PropTypes.bool.isRequired,
  addLabel: PropTypes.bool.isRequired,
  onChangeText: PropTypes.func,
  input: PropTypes.object,
  label: PropTypes.string,
  basePath: PropTypes.string,
  storageDir: PropTypes.string,
  options: PropTypes.object,
  source: PropTypes.string,
  toolbar: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
}

RichTextInput.defaultProps = {
  addField: true,
  addLabel: true,
  options: {},
  record: {},
  toolbar: true,
}

export default RichTextInput
