import Quill from 'quill'

let Embed = Quill.import('blots/block/embed')

class AdPlaceholder extends Embed {
  static create(value) {
    let node = super.create(value)

    node.setAttribute('class', 'ad-placeholder')

    node.setAttribute('contenteditable', false)

    return node
  }
}

AdPlaceholder.blotName = 'slot'

AdPlaceholder.tagName = 'slot'

export default AdPlaceholder
