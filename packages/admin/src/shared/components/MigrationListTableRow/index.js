/**
 *
 * MigrationListTableRow
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TableRow, TableRowColumn } from 'material-ui/Table'
import RaisedButton from 'material-ui/RaisedButton'
import TrueIcon from 'material-ui/svg-icons/action/done'
import FalseIcon from 'material-ui/svg-icons/content/clear'

import { blue500, red500 } from 'material-ui/styles/colors'
import { isFunction } from 'lodash'

import styles from './styles'

export default class MigrationListTableRow extends React.Component {
  static propTypes = {
    migration: PropTypes.object,
    onRun: PropTypes.func,
    onRollback: PropTypes.func,
  }

  render() {
    const {
      migration: {
        id,
        isIdempotent,
        hasRollback,
        status,
        description,
        appVersion,
      },
    } = this.props

    return (
      <TableRow selectable={false}>
        <TableRowColumn style={styles.id}>{id}</TableRowColumn>

        <TableRowColumn style={styles.description}>
          {description}
        </TableRowColumn>

        <TableRowColumn style={styles.appVersion}>{appVersion}</TableRowColumn>

        <TableRowColumn style={styles.isIdempotent}>
          {isIdempotent ? (
            <TrueIcon color={blue500} style={styles.icon} />
          ) : (
            <FalseIcon color={red500} style={styles.icon} />
          )}
        </TableRowColumn>

        <TableRowColumn style={styles.status}>{status}</TableRowColumn>

        <TableRowColumn style={styles.actions}>
          <RaisedButton
            primary
            label="RUN"
            onClick={this._runButtonClicked}
            disabled={status === 'done' && !isIdempotent}
            style={{ marginRight: 10 }}
          />

          <RaisedButton
            secondary
            label="ROLLBACK"
            disabled={!hasRollback || status !== 'done'}
            onClick={this._rollbackButtonClicked}
          />
        </TableRowColumn>
      </TableRow>
    )
  }

  _runButtonClicked = () => {
    const { onRun, migration } = this.props

    if (isFunction(onRun)) {
      onRun(migration.id)
    }
  }

  _rollbackButtonClicked = () => {
    const { onRollback, migration } = this.props

    if (isFunction(onRollback)) {
      onRollback(migration.id)
    }
  }
}
