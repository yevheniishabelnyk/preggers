const styles = {
  id: {
    width: 150,
    whiteSpace: 'wrap',
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: '15px',
    lineHeight: '21px',
  },

  description: {
    whiteSpace: 'wrap',
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: '15px',
    lineHeight: '21px',
  },

  appVersion: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
  },

  isIdempotent: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  status: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: '15px',
  },

  actions: {
    textAlign: 'center',
    width: 250,
    paddingLeft: 0,
    paddingRight: 0,
  },

  icon: {
    width: 30,
    height: 30,
  },
}

export default styles
