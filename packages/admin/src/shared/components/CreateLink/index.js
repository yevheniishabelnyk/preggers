/**
 *
 * CreateLink
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'
import AddIcon from 'material-ui/svg-icons/content/add'
import IconButton from 'material-ui/IconButton'

import './style.css'

const CreateLink = ({ bathPath, tooltip = 'Add' }) => (
  <Link to={{ pathname: `/${bathPath}/create` }}>
    <IconButton tooltip={tooltip}>
      <AddIcon className="create-link__icon" />
    </IconButton>
  </Link>
)

CreateLink.propTypes = {
  bathPath: PropTypes.string.isRequired,
  tooltip: PropTypes.string,
}

export default CreateLink
