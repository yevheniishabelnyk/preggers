import React from 'react'
import PropTypes from 'prop-types'
import Popup from 'react-popup'

/** The prompt content component */
class Prompt extends React.Component {
  static propTypes = {
    inputType: PropTypes.string,
    defaultValue: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    closePopup: PropTypes.func,
  }

  constructor(props) {
    super(props)

    this.state = {
      value: this.props.defaultValue,
    }

    this._onDocumentKeyDown = this._onDocumentKeyDown.bind(this)
    this._onChange = this._onChange.bind(this)
    this._onKeyDown = this._onKeyDown.bind(this)
  }

  componentWillMount() {
    document.addEventListener('keydown', this._onDocumentKeyDown)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.value !== this.state.value) {
      this.props.onChange(this.state.value)
    }
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this._onDocumentKeyDown)
  }

  _onChange(e) {
    this.setState({ value: e.target.value })
  }

  _onKeyDown(e) {
    if (e.keyCode === 13) {
      this.props.onSubmit(this.state.value)
    }
  }

  _onDocumentKeyDown(e) {
    if (e.keyCode === 27) {
      this.props.closePopup()
    }
  }

  render() {
    return (
      <input
        type={this.props.inputType}
        autoFocus
        placeholder={this.props.placeholder}
        className="mm-popup__input"
        value={this.state.value}
        onChange={this._onChange}
        onKeyDown={this._onKeyDown}
      />
    )
  }
}

/** Prompt plugin */
Popup.registerPlugin('prompt', function({
  title,
  placeholder,
  callback,
  detectError,
  inputType,
}) {
  let promptValue = ''

  let promptChange = function(value) {
    promptValue = value
  }

  let error = null

  function onSubmit(value) {
    error = detectError(value)

    if (!error) {
      Popup.close()
      callback(value)
    } else {
      Popup.close()
      Popup.alert(error)
      createPrompt()
    }
  }

  function onClose() {
    Popup.close()
  }

  const createPrompt = () =>
    this.create({
      title: title,
      content: (
        <Prompt
          inputType={inputType}
          onChange={promptChange}
          onSubmit={onSubmit}
          closePopup={onClose}
          placeholder={placeholder}
          defaultValue={promptValue}
          error={error}
        />
      ),
      buttons: {
        left: ['cancel'],
        right: [
          {
            text: 'Save',
            className: 'success',
            action: function() {
              error = detectError(promptValue)
              if (promptValue) {
                if (!error) {
                  Popup.close()
                  callback(promptValue)
                } else {
                  Popup.close()
                  Popup.alert(error)
                  createPrompt()
                }
              } else {
                Popup.close()
                Popup.alert('Field is required!')
                createPrompt()
              }
            },
          },
        ],
      },
    })

  createPrompt()
})
