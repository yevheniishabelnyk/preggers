import React from 'react'
import PropTypes from 'prop-types'

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
} from 'material-ui/Table'

import MigrationsHistoryTableRow from 'shared/components/MigrationsHistoryTableRow'

import styles from './styles'

export default class MigrationsHistoryTable extends React.Component {
  static propTypes = {
    migrationsHistory: PropTypes.array,
  }

  static defaultProps = {
    migrationsHistory: [],
  }

  render() {
    const { migrationsHistory } = this.props

    return (
      <Table>
        <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
          <TableRow>
            <TableHeaderColumn style={styles.id}>ID</TableHeaderColumn>

            <TableHeaderColumn style={styles.type}>Type</TableHeaderColumn>

            <TableHeaderColumn style={styles.appVersion}>
              App Version
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.migratedBy}>
              Migrated By
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.migratedAt}>
              Migrated At
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.isIdempotent}>
              Idempotent
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.hasRollback}>
              Has Rollback
            </TableHeaderColumn>

            <TableHeaderColumn style={styles.description}>
              Description
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>

        <TableBody displayRowCheckbox={false}>
          {migrationsHistory.map(migration => (
            <MigrationsHistoryTableRow
              key={migration.id}
              migration={migration}
            />
          ))}
        </TableBody>
      </Table>
    )
  }
}
