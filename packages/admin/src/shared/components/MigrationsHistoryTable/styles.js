export default {
  id: {
    width: 150,
    textAlign: 'center',
  },

  description: {
    textAlign: 'center',
  },

  appVersion: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  isIdempotent: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  hasRollback: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  status: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  type: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },

  migratedBy: {
    textAlign: 'center',
    width: 200,
    paddingLeft: 0,
    paddingRight: 0,
  },

  migratedAt: {
    textAlign: 'center',
    width: 100,
    paddingLeft: 0,
    paddingRight: 0,
  },
}
