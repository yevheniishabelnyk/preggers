/*
 *
 * Validators
 *
 */

export const isValidEmail = email => {
  /* eslint-disable */
  const emailRegExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  /* eslint-enable */
  return emailRegExp.test(email)
}

export const isValidMSISDN = msisdn => {
  const msisdnRegExp = /^\+[1-9]{1,3}\d{7,14}$/
  return msisdnRegExp.test(msisdn)
}
