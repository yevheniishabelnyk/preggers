export default function(url, width, height) {
  if (!url || typeof url !== 'string') {
    return Promise.reject()
  }

  return new Promise(resolve => {
    let img = new Image()

    img.onload = function() {
      let canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d')

      canvas.width = width
      canvas.height = height

      ctx.drawImage(this, 0, 0, width, height)

      const base64 = canvas.toDataURL('image/jpeg')

      resolve(base64)
    }

    img.src = url
  })
}
