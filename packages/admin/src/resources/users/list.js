/**
 *
 * UsersList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  EmailField,
  ReferenceField,
  EditButton,
  ShowButton,
} from 'admin-on-rest'
import NameField from 'app/components/NameField'

const UsersList = props => (
  <List {...props}>
    <Datagrid>
      <NameField />

      <EmailField source="email" />

      <ReferenceField
        label="Connected"
        source="connection"
        reference="connections"
      >
        <TextField source="id" />
      </ReferenceField>

      <ShowButton basePath="/users" />
      <EditButton basePath="/users" />
      {/*<DeleteButton basePath="/users" />*/}
    </Datagrid>
  </List>
)

export default UsersList
