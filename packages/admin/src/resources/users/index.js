/**
 *
 * Users resource props
 *
 */

import icon from 'material-ui/svg-icons/social/group'
import list from './list'
import edit from './edit'
import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'users',
  icon,
  list,
  edit,
  show,
  remove: Delete,
}
