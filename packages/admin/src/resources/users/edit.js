/**
 *
 * Users Edit
 *
 */

import React from 'react'

import { Edit, DisabledInput, SimpleForm } from 'admin-on-rest'

const UsersEdit = props => (
  <Edit title="name" {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <DisabledInput source="email" />
    </SimpleForm>
  </Edit>
)

export default UsersEdit
