/**
 *
 * FAQs list
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  List,
  Datagrid,
  TextField,
  EditButton,
  DeleteButton,
  ShowButton,
} from 'admin-on-rest'

const FullNameField = () => <div style={{ width: 10 }}>test</div>
FullNameField.defaultProps = { label: 'Name' }

FullNameField.propTypes = {
  record: PropTypes.object,
}

function index(obj, is, value) {
  if (typeof is === 'string') {
    return index(obj, is.split('.'), value)
  }
  if (is.length === 1 && value !== undefined) {
    return (obj[is[0]] = value)
  }
  if (is.length === 0) {
    return obj
  }
  return index(obj[is[0]], is.slice(1), value)
}

function strip(html) {
  let tmp = document.createElement('DIV')
  tmp.innerHTML = html
  return tmp.textContent || tmp.innerText || ''
}

const HTMLField = ({ source, record = {} }) => (
  <span>{strip(index(record, source))}</span>
)

HTMLField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.any,
}

const FAQsList = props => (
  <List {...props} title="FAQs">
    <Datagrid>
      <TextField source="question.en" label="Question" />
      <HTMLField source="answer.en" label="Answer" />
      <ShowButton basePath="/content/faqs" />
      <EditButton basePath="/content/faqs" />
      <DeleteButton basePath="/content/faqs" />
    </Datagrid>
  </List>
)

export default FAQsList
