/**
 *
 * FAQ Create
 *
 */

import React from 'react'

import { Create, TextInput, required } from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import SimpleForm from 'app/components/SimpleForm'

const FAQCreate = props => (
  <Create title="Create an FAQ" {...props}>
    <SimpleForm>
      <TextInput
        label="Question"
        source="question"
        validate={[required]}
        localized
      />

      <RichTextInput
        label="Answer"
        source="answer"
        validate={[required]}
        localized
      />
    </SimpleForm>
  </Create>
)

export default FAQCreate
