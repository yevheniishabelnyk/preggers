/**
 *
 * FAQs resource props
 *
 */

import icon from 'material-ui/svg-icons/social/school'
import list from './list'
import create from './create'
import edit from './edit'
import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/faqs',
  options: { label: 'FAQs' },
  icon,
  list,
  create,
  edit,
  show,
  remove: Delete,
}
