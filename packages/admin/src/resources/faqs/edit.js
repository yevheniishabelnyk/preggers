/**
 *
 * FAQ Edit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Edit, TextInput, required } from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import SimpleForm from 'app/components/SimpleForm'

const Title = ({ record = {} }) => <span>{record.question.en}</span>

Title.propTypes = {
  record: PropTypes.object,
}

const FAQEdit = props => (
  <Edit title={<Title />} {...props}>
    <SimpleForm>
      <TextInput
        label="Question"
        source="question"
        validate={[required]}
        localized
      />

      <RichTextInput
        label="Answer"
        source="answer"
        validate={[required]}
        localized
      />
    </SimpleForm>
  </Edit>
)

export default FAQEdit
