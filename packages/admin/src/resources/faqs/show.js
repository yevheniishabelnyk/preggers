/**
 *
 * FAQ Show
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Show, SimpleShowLayout } from 'admin-on-rest'
import { Card, CardTitle, CardText } from 'material-ui/Card'
import { Tabs, Tab } from 'material-ui/Tabs'

const Title = ({ record = {} }) => <span>{record.question.en}</span>

Title.propTypes = {
  record: PropTypes.object,
}

function index(obj, is, value) {
  if (typeof is === 'string') {
    return index(obj, is.split('.'), value)
  }
  if (is.length === 1 && value !== undefined) {
    return (obj[is[0]] = value)
  }
  if (is.length === 0) {
    return obj
  }
  return index(obj[is[0]], is.slice(1), value)
}

const HTMLField = ({ source, record = {} }) => (
  <div dangerouslySetInnerHTML={{ __html: index(record, source) }} />
)

HTMLField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.any,
}

HTMLField.defaultProps = {
  addLabel: true,
}

const ShowLayout = ({ record = {} }) => (
  <Tabs inkBarStyle={{ backgroundColor: 'gray' }}>
    <Tab label="English">
      <Card style={{ marginBottom: 20 }}>
        <CardTitle title={record.question.en} />
        <CardText>
          <div dangerouslySetInnerHTML={{ __html: record.answer.en }} />
        </CardText>
      </Card>
    </Tab>
    <Tab label="Swedish">
      <CardTitle title={record.question.sv} />
      <CardText>
        <div dangerouslySetInnerHTML={{ __html: record.answer.sv }} />
      </CardText>
    </Tab>
  </Tabs>
)

ShowLayout.propTypes = {
  record: PropTypes.object,
}

const FAQShow = props => (
  <Show title={<Title />} {...props}>
    <SimpleShowLayout>
      <ShowLayout />
    </SimpleShowLayout>
  </Show>
)

export default FAQShow
