/**
 *
 * Other Apps Create
 *
 */

import React from 'react'

import {
  Create,
  TextInput,
  ImageField,
  RadioButtonGroupInput,
} from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'
import FileInput from 'app/components/FileInput'
import RichTextInput from 'shared/components/RichTextInput'

export default class OtherAppsCreate extends React.Component {
  location = [{ id: 'grid', name: 'Grid' }, { id: 'list', name: 'List' }]

  render() {
    return (
      <Create title="Create app" {...this.props}>
        <SimpleForm>
          <TextInput source="title" label="Title" localized />

          <RadioButtonGroupInput source="location" choices={this.location} />

          <FileInput source="logo" label="Logo" accept="image/*">
            <ImageField source="uri" />
          </FileInput>

          <FileInput source="image" label="Image" accept="image/*">
            <ImageField source="uri" />
          </FileInput>

          <TextInput source="ingress" label="Ingress" localized />

          <RichTextInput source="text" label="Text" localized />

          <TextInput source="buttonText" label="Button text" localized />

          <TextInput source="buttonColor" label="Button color" />

          <TextInput type="url" label="Button url" source="buttonUrl" />

          <TextInput type="url" label="App store id" source="appStoreId" />

          <TextInput type="url" label="Play store id" source="playStoreId" />
        </SimpleForm>
      </Create>
    )
  }
}
