/**
 *
 * Other Apps resource props
 *
 */

import icon from 'material-ui/svg-icons/action/touch-app'
import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/otherApps',
  options: { label: 'Other apps' },
  icon,
  list,
  create,
  edit,
  remove: Delete,
}
