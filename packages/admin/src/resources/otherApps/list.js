/**
 *
 * Other Apps list
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  EditButton,
  DeleteButton,
} from 'admin-on-rest'

export default class OtherAppsList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Other apps">
        <Datagrid>
          <TextField source="title.en" label="Title" />

          <EditButton />

          <DeleteButton />
        </Datagrid>
      </List>
    )
  }
}
