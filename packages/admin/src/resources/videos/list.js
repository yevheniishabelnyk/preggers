/**
 *
 * VideosList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  SelectField,
  EditButton,
  DeleteButton,
} from 'admin-on-rest'

export default class VideosList extends React.Component {
  types = [
    { id: 'vimeo', name: 'Vimeo' },
    { id: 'youtube', name: 'YouTube' },
    { id: 'origin', name: 'Origin' },
  ]

  render() {
    return (
      <List {...this.props} title="Videos">
        <Datagrid>
          <TextField source="id" />

          <TextField source="url" />

          <SelectField source="type" choices={this.types} />

          <TextField source="title.en" />

          <EditButton label="" />

          <DeleteButton label="" />
        </Datagrid>
      </List>
    )
  }
}
