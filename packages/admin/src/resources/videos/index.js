/**
 *
 * Videos resource props
 *
 */

import icon from 'material-ui/svg-icons/av/video-library'
import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/videos',
  options: { label: 'Videos' },
  icon,
  list,
  create,
  edit,
  remove: Delete,
}
