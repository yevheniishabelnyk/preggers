/**
 *
 * VideosCreate
 *
 */

import React from 'react'

import {
  Create,
  SimpleForm,
  TextInput,
  SelectInput,
  required,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'

import CreateLink from 'shared/components/CreateLink'

import firebase from 'firebase'

export default class VideosCreate extends React.Component {
  types = [
    { id: 'vimeo', name: 'Vimeo' },
    { id: 'youtube', name: 'YouTube' },
    { id: 'origin', name: 'Origin' },
  ]

  state = {
    videoTags: [],
    initialised: false,
  }

  render() {
    if (this.state.initialised) {
      return (
        <Create title="Create a Video" {...this.props}>
          <SimpleForm>
            <TextInput source="url" validate={[required]} />

            <SelectInput
              source="type"
              defaultValue="vimeo"
              validate={[required]}
              choices={this.types}
            />

            <SelectInput source="tag" choices={this.state.videoTags} />
            <CreateLink bathPath="content/videoTags" />

            <TextInput source="title.en" />
            <TextInput source="title.sv" />

            <RichTextInput source="description.en" />
            <RichTextInput source="description.sv" />
          </SimpleForm>
        </Create>
      )
    }
    return null
  }

  async componentWillMount() {
    const snap = await firebase
      .database()
      .ref('/content/videoTags')
      .once('value')

    if (snap.exists()) {
      const videoTags = snap.val()

      this.setState({
        initialised: true,
        videoTags: videoTags
          ? Object.keys(videoTags).map(key => ({
              id: key,
              name: videoTags[key].name.en,
            }))
          : [],
      })

      console.info('videoTags: ', videoTags)
    }
  }
}
