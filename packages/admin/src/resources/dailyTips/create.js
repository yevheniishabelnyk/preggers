/**
 *
 * Daily Tips create
 *
 */

import React from 'react'

import {
  Create,
  TextInput,
  required,
  ImageField,
  BooleanInput,
  SelectInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import TabbedForm from 'app/components/TabbedForm'
import FileInput from 'app/components/FileInput'
import FormTab from 'app/components/FormTab'

import firebase from 'firebase'

import { difference, range, keys } from 'lodash'

export default class DailyTipsCreate extends React.Component {
  state = {
    initialised: false,
  }

  daysNumbers = []

  render() {
    if (this.state.initialised) {
      return (
        <Create title="Create tip of the day messages" {...this.props}>
          <TabbedForm>
            <FormTab label="General">
              <SelectInput
                source="day"
                validate={[required]}
                translateChoice={false}
                choices={this.daysNumbers}
                options={{
                  maxHeight: 200,
                }}
                defaultValue={
                  this.daysNumbers[0] ? this.daysNumbers[0].id : null
                }
              />
            </FormTab>

            <FormTab label="Mother">
              <TextInput label="Message" source="motherMessage" localized />

              <FileInput source="motherImage" label="Image" accept="image/*">
                <ImageField source="uri" />
              </FileInput>

              <RichTextInput
                label="Description"
                source="motherDescription"
                localized
              />

              <TextInput
                type="url"
                label="Read More link"
                source="motherLink"
              />

              <BooleanInput
                label="Use mother tip for all users"
                source="useMotherTipForAll"
                style={{ marginTop: 40 }}
              />
            </FormTab>

            <FormTab label="Partner">
              <TextInput label="Message" source="partnerMessage" localized />

              <FileInput source="partnerImage" label="Image" accept="image/*">
                <ImageField source="uri" />
              </FileInput>

              <RichTextInput
                label="Description"
                source="partnerDescription"
                localized
              />

              <TextInput
                type="url"
                label="Read More link"
                source="partnerLink"
                style={{ marginBottom: 40 }}
              />
            </FormTab>
          </TabbedForm>
        </Create>
      )
    }
    return null
  }

  async componentWillMount() {
    const daysSnap = await firebase
      .database()
      .ref('/content/dailyTips')
      .once('value')

    const allDaysNumbers = range(1, 295)
    let availableDaysNumbers = allDaysNumbers

    if (daysSnap.exists()) {
      const data = daysSnap.val()
      const busyDaysNumbers = keys(data)
        .filter(key => data[key] !== undefined)
        .map(day => Number(day))

      availableDaysNumbers = difference(allDaysNumbers, busyDaysNumbers)
    }

    this.daysNumbers = availableDaysNumbers.map(day => ({
      id: day,
      name: day,
    }))

    this.setState({
      initialised: true,
    })
  }
}
