/**
 *
 * Daily Tips resource props
 *
 */

import icon from 'material-ui/svg-icons/action/loyalty'
import list from './list'
import create from './create'
import edit from './edit'
// import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/dailyTips',
  options: { label: 'Daily tips' },
  icon,
  list,
  create,
  edit,
  // show,
  remove: Delete,
}
