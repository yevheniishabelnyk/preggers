/**
 *
 * Daily Tips edit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Edit,
  SelectInput,
  ImageField,
  TextInput,
  required,
  BooleanInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import TabbedForm from 'app/components/TabbedForm'
import FileInput from 'app/components/FileInput'
import FormTab from 'app/components/FormTab'

import firebase from 'firebase'
import { values, range, difference } from 'lodash'
import { toOrdinal } from 'ordinal-js'

const Title = ({ record }) => <span>{toOrdinal(record.day)} day</span>

Title.propTypes = {
  record: PropTypes.object,
}

export default class DailyTipsEdit extends React.Component {
  static propTypes = {
    match: PropTypes.object,
  }

  state = {
    initialised: false,
  }

  daysNumbers = []

  render() {
    if (this.state.initialised) {
      return (
        <div style={{ position: 'relative' }}>
          <Edit title={<Title />} {...this.props}>
            <TabbedForm>
              <FormTab label="General">
                <SelectInput
                  source="day"
                  validate={[required]}
                  translateChoice={false}
                  choices={this.daysNumbers}
                  options={{
                    maxHeight: 200,
                  }}
                />
              </FormTab>

              <FormTab label="Mother">
                <TextInput label="Message" source="motherMessage" localized />

                <FileInput source="motherImage" label="Image" accept="image/*">
                  <ImageField source="uri" />
                </FileInput>

                <RichTextInput
                  label="Description"
                  source="motherDescription"
                  localized
                />

                <TextInput
                  type="url"
                  label="Read More link"
                  source="motherLink"
                />

                <BooleanInput
                  label="Use mother tip for all users"
                  source="useMotherTipForAll"
                  style={{ marginTop: 40 }}
                />
              </FormTab>

              <FormTab label="Partner">
                <TextInput label="Message" source="partnerMessage" localized />

                <FileInput source="partnerImage" label="Image" accept="image/*">
                  <ImageField source="uri" />
                </FileInput>

                <RichTextInput
                  label="Description"
                  source="partnerDescription"
                  localized
                />

                <TextInput
                  type="url"
                  label="Read More link"
                  source="partnerLink"
                  style={{ marginBottom: 40 }}
                />
              </FormTab>
            </TabbedForm>
          </Edit>
        </div>
      )
    }
    return null
  }

  async componentWillMount() {
    const dayId = this.props.match.params.id

    const snap = await firebase
      .database()
      .ref('/content/dailyTips')
      .once('value')

    const allDaysNumbers = range(1, 295)
    let availableDaysNumbers = allDaysNumbers

    const days = snap.val()
    const busyDaysNumbers = values(days)
      .filter(day => day !== undefined && day.id !== dayId)
      .map(day => day.day)

    availableDaysNumbers = difference(allDaysNumbers, busyDaysNumbers)

    this.daysNumbers = availableDaysNumbers.map(number => ({
      id: number,
      name: number,
    }))

    this.setState({
      initialised: true,
    })
  }
}
