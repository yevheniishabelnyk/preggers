/**
 *
 * Tags resource props
 *
 */

import icon from 'material-ui/svg-icons/editor/text-fields'
import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/tags',
  options: { label: 'Tags' },
  icon,
  list,
  create,
  edit,
  remove: Delete,
}
