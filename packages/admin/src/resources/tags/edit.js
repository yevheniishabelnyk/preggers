/**
 *
 * Tag Edit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Edit, TextInput, required } from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'

const Name = ({ record }) => (
  <span>{record ? `Edit "${record.name.en}" tag` : ''}</span>
)

Name.propTypes = {
  record: PropTypes.object,
}

export default class TagEdit extends React.Component {
  render() {
    return (
      <Edit title={<Name />} {...this.props}>
        <SimpleForm>
          <TextInput
            source="name"
            label="Name"
            validate={[required]}
            localized
          />
        </SimpleForm>
      </Edit>
    )
  }
}
