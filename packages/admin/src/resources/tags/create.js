/**
 *
 * Tag Create
 *
 */

import React from 'react'

import { Create, TextInput, required } from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'

export default class TagCreate extends React.Component {
  render() {
    return (
      <Create title="Create a Tag" {...this.props}>
        <SimpleForm redirect={false}>
          <TextInput
            source="name"
            label="Name"
            validate={[required]}
            localized
          />
        </SimpleForm>
      </Create>
    )
  }
}
