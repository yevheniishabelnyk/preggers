/**
 *
 * TagsList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  EditButton,
  DeleteButton,
  Filter,
  TextInput,
} from 'admin-on-rest'

const TagsFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
  </Filter>
)

export default class TagsList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Tags" filters={<TagsFilter />}>
        <Datagrid>
          <TextField source="name.en" label="Name" />

          <TextField source="id" label="ID" />

          <EditButton />

          <DeleteButton />
        </Datagrid>
      </List>
    )
  }
}
