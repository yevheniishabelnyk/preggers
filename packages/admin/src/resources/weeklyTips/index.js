/**
 *
 * Weekly Tips resource props
 *
 */

import icon from 'material-ui/svg-icons/action/description'
import list from './list'
import create from './create'
import edit from './edit'
// import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/weeklyTips',
  options: { label: 'Weekly tips' },
  icon,
  list,
  create,
  edit,
  // show,
  remove: Delete,
}
