/**
 *
 * Weekly Tips edit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  // NumberField,
  Edit,
  SelectInput,
  ImageField,
  TextInput,
  required,
  BooleanInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import TabbedForm from 'app/components/TabbedForm'
import FileInput from 'app/components/FileInput'
import FormTab from 'app/components/FormTab'

import firebase from 'firebase'
import { values, range, difference } from 'lodash'
import { toOrdinal } from 'ordinal-js'

const Title = ({ record }) => <span>{toOrdinal(record.week)} week</span>

Title.propTypes = {
  record: PropTypes.object,
}

export default class WeeklyTipsEdit extends React.Component {
  static propTypes = {
    match: PropTypes.object,
  }

  state = {
    initialised: false,
  }

  weeksNumbers = []

  render() {
    if (this.state.initialised) {
      return (
        <div style={{ position: 'relative' }}>
          <Edit title={<Title />} {...this.props}>
            <TabbedForm>
              <FormTab label="General">
                <SelectInput
                  source="week"
                  validate={[required]}
                  translateChoice={false}
                  choices={this.weeksNumbers}
                  options={{
                    maxHeight: 200,
                  }}
                />
              </FormTab>

              <FormTab label="Mother">
                <TextInput label="Title" source="motherTitle" localized />

                <FileInput
                  source="motherImage"
                  label="Mother tip image"
                  accept="image/*"
                >
                  <ImageField source="uri" />
                </FileInput>

                <RichTextInput
                  label="Message"
                  source="motherMessage"
                  localized
                />

                <TextInput
                  label="Mother link to site/article"
                  source="motherLink"
                />

                <BooleanInput
                  label="Show this tip for another users?"
                  source="showMotherMessageForAnotherUsers"
                  style={{ marginTop: 40 }}
                />
              </FormTab>

              <FormTab label="Another Users">
                <TextInput label="Title" source="anotherUsersTitle" localized />

                <FileInput
                  source="anotherUsersImage"
                  label="Another users image"
                  accept="image/*"
                >
                  <ImageField source="uri" />
                </FileInput>

                <RichTextInput
                  label="Message"
                  source="anotherUsersMessage"
                  localized
                />

                <TextInput
                  label="Another users link"
                  source="anotherUsersLink"
                  style={{ marginBottom: 40 }}
                />
              </FormTab>
            </TabbedForm>
          </Edit>
        </div>
      )
    }
    return null
  }

  async componentWillMount() {
    const weekId = this.props.match.params.id

    const snap = await firebase
      .database()
      .ref('/content/weeklyTips')
      .once('value')

    const allWeeksNumbers = range(3, 44)
    let availableWeeksNumbers = allWeeksNumbers

    const weeks = snap.val()
    const busyWeeksNumbers = values(weeks)
      .filter(week => week !== undefined && week.id !== weekId)
      .map(week => week.week)

    availableWeeksNumbers = difference(allWeeksNumbers, busyWeeksNumbers)

    this.weeksNumbers = availableWeeksNumbers.map(week => ({
      id: week,
      name: week,
    }))

    this.setState({
      initialised: true,
    })
  }

  _setValue = (key, value) => this.setState({ [key]: value })
}
