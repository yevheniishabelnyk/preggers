/**
 *
 * Weekly Tips list
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { List, Datagrid, TextField, NumberField } from 'admin-on-rest'
import DeleteButton from 'app/components/DeleteButton'
import EditButton from 'app/components/EditButton'

const Actions = ({ record }) => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
    }}
    label="Actions"
  >
    <EditButton
      record={record}
      basePath="/content/weeklyTips"
      label=""
      style={{ minWidth: '44px' }}
    />

    <DeleteButton
      record={record}
      basePath="/content/weeklyTips"
      label=""
      style={{ minWidth: '44px' }}
    />
  </div>
)

Actions.propTypes = {
  record: PropTypes.object,
}

Actions.defaultProps = {
  title: 'Actions',
}

export default class WeeklyTipsList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Weekly tips">
        <Datagrid>
          <NumberField label="Week" source="week" />

          <TextField source="motherTitle.en" label="Mother Title" />

          <TextField
            source="anotherUsersTitle.en"
            label="Another Users Title"
          />

          <Actions title="Actions" />
        </Datagrid>
      </List>
    )
  }
}
