/**
 *
 * Weekly Tips create
 *
 */

import React from 'react'

import {
  Create,
  TextInput,
  required,
  ImageField,
  BooleanInput,
  SelectInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import TabbedForm from 'app/components/TabbedForm'
import FileInput from 'app/components/FileInput'
import FormTab from 'app/components/FormTab'

import firebase from 'firebase'

import { difference, range, keys } from 'lodash'

const Title = () => <span>Create week messages</span>

export default class WeeklyTipsCreate extends React.Component {
  state = {
    initialised: false,
  }

  weeksNumbers = []

  render() {
    if (this.state.initialised) {
      return (
        <Create title={<Title />} {...this.props}>
          <TabbedForm>
            <FormTab label="General">
              <SelectInput
                source="week"
                validate={[required]}
                translateChoice={false}
                choices={this.weeksNumbers}
                options={{
                  maxHeight: 200,
                }}
                defaultValue={
                  this.weeksNumbers[0] ? this.weeksNumbers[0].id : null
                }
              />
            </FormTab>

            <FormTab label="Mother">
              <TextInput label="Title" source="motherTitle" localized />

              <FileInput source="motherImage" label="Image" accept="image/*">
                <ImageField source="uri" />
              </FileInput>

              <RichTextInput label="Message" source="motherMessage" localized />

              <TextInput label="Link to internal article" source="motherLink" />

              <BooleanInput
                label="Show this tip for another users?"
                source="showMotherMessageForAnotherUsers"
                style={{ marginTop: 40 }}
                defaulValue={false}
              />
            </FormTab>

            <FormTab label="Another Users">
              <TextInput label="Title" source="anotherUsersTitle" localized />

              <FileInput
                source="anotherUsersImage"
                label="Image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <RichTextInput
                label="Message"
                source="anotherUsersMessage"
                localized
              />

              <TextInput
                label="Link to internal article"
                source="anotherUsersLink"
                style={{ marginBottom: 40 }}
              />
            </FormTab>
          </TabbedForm>
        </Create>
      )
    }
    return null
  }

  async componentWillMount() {
    const weeksSnap = await firebase
      .database()
      .ref('/content/weeklyTips')
      .once('value')

    const allWeeksNumbers = range(3, 44)
    let availableWeeksNumbers = allWeeksNumbers

    if (weeksSnap.exists()) {
      const data = weeksSnap.val()
      const busyWeeksNumbers = keys(data)
        .filter(key => data[key] !== undefined && data[key] !== null)
        .map(week => Number(week))

      availableWeeksNumbers = difference(
        availableWeeksNumbers,
        busyWeeksNumbers
      )
    }

    this.weeksNumbers = availableWeeksNumbers.map(week => ({
      id: week,
      name: week,
    }))

    this.setState({
      initialised: true,
    })
  }

  _setValue = (key, value) => this.setState({ [key]: value })
}
