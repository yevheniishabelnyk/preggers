/**
 *
 * Feedbacks
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  EmailField,
  ShowButton,
  ReferenceField,
  DateField,
} from 'admin-on-rest'

export default class FeedbacksList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Feedbacks">
        <Datagrid>
          <ReferenceField label="User Name" source="userId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <ReferenceField
            label="User Email"
            source="userId"
            reference="users"
            linkType={false}
          >
            <EmailField source="email" />
          </ReferenceField>

          <TextField label="User IP" source="userIp" />

          <TextField label="Text" source="text" />

          <TextField label="Rating" source="rating" />

          <TextField label="App Version" source="appVersion" />

          <DateField
            label="Date"
            source="createdAt"
            options={{
              year: 'numeric',
              month: 'long',
              day: 'numeric',
            }}
          />

          <ShowButton basePath="/feedbacks" />
        </Datagrid>
      </List>
    )
  }
}
