/**
 *
 * FeedbacksShow
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Show,
  SimpleShowLayout,
  TextField,
  EmailField,
  DateField,
  ReferenceField,
} from 'admin-on-rest'

const Name = ({ record }) => <span>{record ? record.userName : ''}</span>

Name.propTypes = {
  record: PropTypes.object,
}

export default class FeedbacksShow extends React.Component {
  render() {
    return (
      <Show title={<Name />} {...this.props}>
        <SimpleShowLayout>
          <ReferenceField label="User Name" source="userId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <ReferenceField
            label="User Email"
            source="userId"
            reference="users"
            linkType={false}
          >
            <EmailField source="email" />
          </ReferenceField>

          <TextField label="User IP" source="userIp" />

          <TextField label="Text" source="text" />

          <TextField label="Rating" source="rating" />

          <TextField label="App Version" source="appVersion" />

          <DateField
            label="Date"
            source="createdAt"
            options={{
              year: 'numeric',
              month: 'long',
              day: 'numeric',
            }}
          />
        </SimpleShowLayout>
      </Show>
    )
  }
}
