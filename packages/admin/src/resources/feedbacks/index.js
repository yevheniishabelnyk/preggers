/**
 *
 * Feedbacks resource props
 *
 */

import icon from 'material-ui/svg-icons/maps/rate-review'
import list from './list'
import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'feedbacks',
  icon,
  list,
  show,
  remove: Delete,
}
