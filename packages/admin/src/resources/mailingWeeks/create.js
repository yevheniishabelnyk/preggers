/**
 *
 * Mailing Week create
 *
 */

import React from 'react'

import {
  Create,
  LongTextInput,
  required,
  SelectInput,
  TextInput,
  ImageField,
} from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'

import firebase from 'firebase'

import { difference, range, keys } from 'lodash'

import MergeTags from 'resources/weeks/components/MergeTags'
import API_MERGE_TAGS from 'app/mailing/API_MERGE_TAGS'
import TEMPLATE_MERGE_TAGS from 'app/mailing/TEMPLATE_MERGE_TAGS'
import FileInput from 'app/components/FileInput'

const allWeekNumbers = range(3, 44)

const styles = {
  textInput: {
    width: 800,
  },
}

export default class MailingWeekCreate extends React.Component {
  state = {
    initialised: false,
  }

  render() {
    if (!this.state.initialised) {
      return null
    }

    return (
      <div>
        <Create title="Create mailing week merge tags" {...this.props}>
          <SimpleForm>
            <LongTextInput
              label="Email subject"
              source="subject"
              localized
              style={styles.textInput}
            />

            <SelectInput
              source="number"
              label="WEEK_NUMBER"
              validate={[required]}
              translateChoice={false}
              choices={this.state.weekNumbers}
              options={{
                maxHeight: 200,
              }}
              defaultValue={
                this.state.weekNumbers[0] ? this.state.weekNumbers[0].id : null
              }
            />

            <LongTextInput
              label="WEEK_TITLE"
              source="title"
              localized
              style={styles.textInput}
            />

            <FileInput
              source="image"
              label="MAILING_WEEK_IMAGE"
              accept="image/*"
            >
              <ImageField source="uri" />
            </FileInput>

            <LongTextInput
              label="WEEK_INGRESS"
              source="ingress"
              localized
              style={styles.textInput}
            />

            <LongTextInput
              label="WEEK_TEXT"
              source="text"
              localized
              style={styles.textInput}
            />

            <TextInput
              label="BUTTON_TITLE"
              source="buttonTitle"
              localized
              style={styles.textInput}
            />

            <TextInput
              label="BUTTON_LINK"
              source="buttonLink"
              style={styles.textInput}
            />
          </SimpleForm>
        </Create>

        <MergeTags
          title="Api Merge Tags"
          subtitle="Use Handlebars syntax for api tags"
          tags={API_MERGE_TAGS}
          handlebars
        />

        <MergeTags
          title="MailChimp template Merge Tags"
          subtitle="All api tags are MailChimp tempate tags"
          tags={TEMPLATE_MERGE_TAGS}
        />
      </div>
    )
  }

  async componentWillMount() {
    const mailingWeeksSnap = await firebase
      .database()
      .ref('/mailing/weeks')
      .once('value')

    let availableWeekNumbers = allWeekNumbers

    if (mailingWeeksSnap.exists()) {
      const weeks = mailingWeeksSnap.val()

      const busyWeekNumbers = keys(weeks).map(weekNumber => Number(weekNumber))

      availableWeekNumbers = difference(allWeekNumbers, busyWeekNumbers)
    }

    const weekNumbers = availableWeekNumbers.map(number => ({
      id: number,
      name: number,
    }))

    this.setState({
      initialised: true,
      weekNumbers,
    })
  }
}
