/**
 *
 * Mailing Weeks list
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { List, Datagrid, TextField, NumberField } from 'admin-on-rest'
import DeleteButton from 'app/components/DeleteButton'
import EditButton from 'app/components/EditButton'

const Actions = ({ record }) => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
    }}
    label="Actions"
  >
    <EditButton
      record={record}
      basePath="/mailing/weeks"
      label=""
      style={{ minWidth: '44px' }}
    />

    <DeleteButton
      record={record}
      basePath="/mailing/weeks"
      label=""
      style={{ minWidth: '44px' }}
    />
  </div>
)

Actions.propTypes = {
  record: PropTypes.object,
}

Actions.defaultProps = {
  title: 'Actions',
}

export default class MailingWeeksList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Mailing Weeks">
        <Datagrid>
          <NumberField source="number" label="WEEK_NUMBER" />

          <TextField source="title.en" label="WEEK_TITLE" />

          <TextField source="ingress.en" label="WEEK_INGRESS" />

          <Actions title="Actions" />
        </Datagrid>
      </List>
    )
  }
}
