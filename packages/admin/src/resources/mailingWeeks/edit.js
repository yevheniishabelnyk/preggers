/**
 *
 * Mailing Week create
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Edit,
  LongTextInput,
  required,
  SelectInput,
  TextInput,
  ImageField,
} from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'

import firebase from 'firebase'

import { difference, range, keys } from 'lodash'
import { toOrdinal } from 'ordinal-js'

import MergeTags from 'resources/weeks/components/MergeTags'
import API_MERGE_TAGS from 'app/mailing/API_MERGE_TAGS'
import TEMPLATE_MERGE_TAGS from 'app/mailing/TEMPLATE_MERGE_TAGS'
import FileInput from 'app/components/FileInput'

const allWeekNumbers = range(3, 44)

const Title = ({ record }) => (
  <span>{toOrdinal(record.number)} mailing week tags</span>
)

Title.propTypes = {
  record: PropTypes.object,
}

const styles = {
  textInput: {
    width: 800,
  },
}

export default class MailingWeekCreate extends React.Component {
  static propTypes = {
    match: PropTypes.object,
  }

  state = {
    initialised: false,
  }

  render() {
    if (!this.state.initialised) {
      return null
    }

    return (
      <div>
        <Edit title={<Title />} {...this.props}>
          <SimpleForm>
            <LongTextInput
              label="Email subject"
              source="subject"
              localized
              style={styles.textInput}
            />

            <FileInput
              source="image"
              label="MAILING_WEEK_IMAGE"
              accept="image/*"
            >
              <ImageField source="uri" />
            </FileInput>

            <SelectInput
              source="number"
              label="WEEK_NUMBER"
              validate={[required]}
              translateChoice={false}
              choices={this.state.weekNumbers}
              defaultValue={this.weekNumber}
              options={{
                maxHeight: 200,
              }}
            />

            <LongTextInput
              label="WEEK_TITLE"
              source="title"
              localized
              style={styles.textInput}
            />

            <LongTextInput
              label="WEEK_INGRESS"
              source="ingress"
              localized
              style={styles.textInput}
            />

            <LongTextInput
              label="WEEK_TEXT"
              source="text"
              localized
              style={styles.textInput}
            />

            <TextInput
              label="BUTTON_TITLE"
              source="buttonTitle"
              localized
              style={styles.textInput}
            />

            <TextInput
              label="BUTTON_LINK"
              source="buttonLink"
              style={styles.textInput}
            />
          </SimpleForm>
        </Edit>
        <MergeTags
          title="Api Merge Tags"
          subtitle="Use Handlebars syntax for api tags"
          tags={API_MERGE_TAGS}
          handlebars
        />

        <MergeTags
          title="MailChimp template Merge Tags"
          subtitle="All api tags are MailChimp tempate tags"
          tags={TEMPLATE_MERGE_TAGS}
        />
      </div>
    )
  }

  async componentWillMount() {
    const mailingWeeksSnap = await firebase
      .database()
      .ref('/mailing/weeks')
      .once('value')

    let availableWeekNumbers = allWeekNumbers

    if (mailingWeeksSnap.exists()) {
      const weeks = mailingWeeksSnap.val()

      const busyWeekNumbers = keys(weeks).map(weekNumber => Number(weekNumber))

      availableWeekNumbers = difference(allWeekNumbers, busyWeekNumbers)
    }

    const weekNumbers = availableWeekNumbers.map(number => ({
      id: number,
      name: number,
    }))

    const weekId = this.props.match.params.id

    this.weekNumber = Number(weekId)
    weekNumbers.unshift({
      id: this.weekNumber,
      name: this.weekNumber,
    })

    this.setState({
      initialised: true,
      weekNumbers,
    })
  }
}
