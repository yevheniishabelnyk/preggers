/**
 *
 * Mailing Weeks resource props
 *
 */

import icon from 'material-ui/svg-icons/communication/mail-outline'
import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'mailing/weeks',
  options: { label: 'Mailing Weeks' },
  icon,
  list,
  create,
  edit,
  remove: Delete,
}
