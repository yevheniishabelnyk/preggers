/**
 *
 * BabyNamesShow
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Show, SimpleShowLayout, TextField, SelectField } from 'admin-on-rest'

const Name = ({ record }) => <span>{record ? record.name : ''}</span>

Name.propTypes = {
  record: PropTypes.object,
}

export default class BabyNamesShow extends React.Component {
  countries = [
    { id: 'US', name: 'USA' },
    { id: 'UK', name: 'United Kingdom' },
    { id: 'SE', name: 'Sweden' },
    { id: 'NO', name: 'Norway' },
    { id: 'DK', name: 'Denmark' },
    { id: 'FI', name: 'Finland' },
  ]

  genders = [{ id: 'm', name: 'Boy' }, { id: 'f', name: 'Girl' }]

  render() {
    return (
      <Show title={<Name />} {...this.props}>
        <SimpleShowLayout>
          <TextField source="name" />

          <SelectField source="gender" choices={this.genders} />

          <SelectField source="countryCode" choices={this.countries} />

          <TextField source="rank" label="Stars" />
        </SimpleShowLayout>
      </Show>
    )
  }
}
