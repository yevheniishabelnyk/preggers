/**
 *
 * BabyNamesCreate
 *
 */

import React from 'react'

import { Create, SelectInput, TextInput, required } from 'admin-on-rest'

import SimpleForm from 'app/components/SimpleForm'

export default class BabyNamesCreate extends React.Component {
  countries = [
    { id: 'US', name: 'USA' },
    { id: 'UK', name: 'United Kingdom' },
    { id: 'SE', name: 'Sweden' },
    { id: 'NO', name: 'Norway' },
    { id: 'DK', name: 'Denmark' },
    { id: 'FI', name: 'Finland' },
  ]

  genders = [{ id: 'm', name: 'Boy' }, { id: 'f', name: 'Girl' }]

  render() {
    return (
      <Create title="Create a Baby Name" {...this.props}>
        <SimpleForm defaultValue={{ rank: 0 }}>
          <TextInput source="name" validate={[required]} />

          <SelectInput
            source="gender"
            validate={[required]}
            choices={this.genders}
          />

          <SelectInput
            source="countryCode"
            label="Country"
            validate={[required]}
            choices={this.countries}
          />
        </SimpleForm>
      </Create>
    )
  }
}
