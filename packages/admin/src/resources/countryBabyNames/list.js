/**
 *
 * BabyNamesList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  SelectField,
  NumberField,
  EditButton,
  Filter,
  TextInput,
  CheckboxGroupInput,
} from 'admin-on-rest'

const BabyNamesFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <CheckboxGroupInput
      source="countryCode"
      label="Country"
      choices={[
        { id: 'US', name: 'USA' },
        { id: 'UK', name: 'UK' },
        { id: 'SE', name: 'Sweden' },
        { id: 'NO', name: 'Norway' },
        { id: 'DK', name: 'Denmark' },
        { id: 'FI', name: 'Finland' },
      ]}
      alwaysOn
    />
  </Filter>
)

export default class BabyNamesList extends React.Component {
  countries = [
    { id: 'US', name: 'USA' },
    { id: 'UK', name: 'United Kingdom' },
    { id: 'SE', name: 'Sweden' },
    { id: 'NO', name: 'Norway' },
    { id: 'DK', name: 'Denmark' },
    { id: 'FI', name: 'Finland' },
  ]

  genders = [{ id: 'm', name: 'Boy' }, { id: 'f', name: 'Girl' }]

  render() {
    return (
      <List {...this.props} title="Baby names" filters={<BabyNamesFilter />}>
        <Datagrid>
          <TextField source="id" />

          <TextField source="name" />

          <SelectField source="gender" choices={this.genders} />

          <SelectField
            source="countryCode"
            choices={this.countries}
            label="Country"
          />

          <NumberField source="rank" label="Stars" />

          <EditButton basePath="/content/countryBabyNames" />
        </Datagrid>
      </List>
    )
  }
}
