/**
 *
 * BabyNamesEdit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Edit, TextInput, required, SelectInput } from 'admin-on-rest'

import SimpleForm from 'app/components/SimpleForm'

const Name = ({ record }) => (
  <span>{record ? `Edit "${record.name}" name` : ''}</span>
)

Name.propTypes = {
  record: PropTypes.object,
}

export default class BabyNamesEdit extends React.Component {
  countries = [
    { id: 'US', name: 'USA' },
    { id: 'UK', name: 'United Kingdom' },
    { id: 'SE', name: 'Sweden' },
    { id: 'NO', name: 'Norway' },
    { id: 'DK', name: 'Denmark' },
    { id: 'FI', name: 'Finland' },
  ]

  genders = [{ id: 'm', name: 'Boy' }, { id: 'f', name: 'Girl' }]

  render() {
    return (
      <Edit title={<Name />} {...this.props}>
        <SimpleForm>
          <TextInput source="name" validate={[required]} />

          <SelectInput
            source="gender"
            validate={[required]}
            choices={this.genders}
          />

          <SelectInput
            source="countryCode"
            label="Country"
            validate={[required]}
            choices={this.countries}
          />
        </SimpleForm>
      </Edit>
    )
  }
}
