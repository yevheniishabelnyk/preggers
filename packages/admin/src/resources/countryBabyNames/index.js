/**
 *
 * Country Baby Names resource props
 *
 */

import icon from 'material-ui/svg-icons/av/library-books'
import list from './list'
import create from './create'
import edit from './edit'
import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/countryBabyNames',
  options: { label: 'Baby Names' },
  icon,
  list,
  create,
  edit,
  show,
  remove: Delete,
}
