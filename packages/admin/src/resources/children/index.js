/**
 *
 * Children resource props
 *
 */

import icon from 'material-ui/svg-icons/places/child-care'
import list from './list'
// import create from './create'
// import show from './show'
// import edit from './edit'
// import { Delete } from 'admin-on-rest'

export default {
  name: 'children',
  icon,
  list,
  // create,
  // show,
  // edit,
  // remove: Delete,
}
