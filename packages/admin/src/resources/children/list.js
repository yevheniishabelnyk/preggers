/**
 *
 * ChildrenList
 *
 */

import React from 'react'
import { List, Datagrid, TextField } from 'admin-on-rest'

const ChildrenList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
    </Datagrid>
  </List>
)

export default ChildrenList
