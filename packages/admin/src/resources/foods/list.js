/**
 *
 * FoodList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  ReferenceField,
  Filter,
  TextInput,
  // CheckboxGroupInput,
  EditButton,
} from 'admin-on-rest'

const FoodFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    {/*<CheckboxGroupInput
      source="category"
      choices={[{ id: 'fruit', name: 'fruits' }, { id: 'meal', name: 'meals' }]}
      alwaysOn
    />*/}
  </Filter>
)

export default class FoodList extends React.Component {
  render() {
    return (
      <List
        {...this.props}
        // sort={{ field: 'name', order: 'DESC' }}
        title="Foods"
        filters={<FoodFilter />}
      >
        <Datagrid>
          <TextField source="name.en" />

          <ReferenceField
            label="Category"
            source="category"
            reference="content/foodCategories"
          >
            <TextField source="name.en" />
          </ReferenceField>

          <EditButton basePath="/content/foods" />
        </Datagrid>
      </List>
    )
  }
}
