/**
 *
 * FoodCreate
 *
 */

import React from 'react'

import {
  Create,
  TextInput,
  SelectInput,
  required,
  RadioButtonGroupInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import SimpleForm from 'app/components/SimpleForm'

import CreateLink from 'shared/components/CreateLink'

import firebase from 'firebase'

export default class FoodCreate extends React.Component {
  state = {
    categories: [],
    initialised: false,
  }

  render() {
    if (this.state.initialised) {
      return (
        <Create title="Create a Food" {...this.props}>
          <SimpleForm>
            <TextInput
              label="Name"
              source="name"
              validate={[required]}
              localized
            />

            <RichTextInput label="Description" source="description" localized />

            <SelectInput
              source="category"
              validate={[required]}
              choices={this.state.categories}
            />

            <CreateLink bathPath="content/foodCategories" />

            <RadioButtonGroupInput
              source="usefulness"
              defaultValue={2}
              choices={[
                { id: 0, name: 'Danger' },
                { id: 1, name: 'Warning' },
                { id: 2, name: 'Good' },
              ]}
            />
          </SimpleForm>
        </Create>
      )
    }
    return null
  }

  async componentWillMount() {
    const snap = await firebase
      .database()
      .ref('/content/foodCategories')
      .once('value')

    const categories = snap.val()

    this.setState({
      initialised: true,
      categories: categories
        ? Object.keys(categories).map(key => ({
            id: key,
            name: categories[key].name.en,
          }))
        : [],
    })

    console.info('categories: ', categories)
  }
}
