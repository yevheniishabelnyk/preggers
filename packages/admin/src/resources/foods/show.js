/**
 *
 * FoodShow
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Show, SimpleShowLayout, TextField, SelectField } from 'admin-on-rest'
import firebase from 'firebase'

const Name = ({ record }) => <span>{record ? record.name : ''}</span>

Name.propTypes = {
  record: PropTypes.object,
}

export default class FoodShow extends React.Component {
  async componentWillMount() {
    const snap = await firebase
      .database()
      .ref('/content/foodCategories')
      .once('value')

    console.info('foodCategories: ', snap.val())
  }

  render() {
    return (
      <Show title={<Name />} {...this.props}>
        <SimpleShowLayout>
          <TextField source="name" />

          <TextField source="description" />

          <TextField source="category" />

          <SelectField
            source="usefulness"
            choices={[
              { id: 0, name: 'Danger' },
              { id: 1, name: 'Warning' },
              { id: 2, name: 'Good' },
            ]}
          />
        </SimpleShowLayout>
      </Show>
    )
  }
}
