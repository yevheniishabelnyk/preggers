/**
 *
 * FoodEdit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Edit,
  RadioButtonGroupInput,
  TextInput,
  SelectInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import SimpleForm from 'app/components/SimpleForm'

import CreateLink from 'shared/components/CreateLink'
import EditLink from 'shared/components/EditLink'

import firebase from 'firebase'

const Name = ({ record }) => <span>{record ? record.name.en : ''}</span>

Name.propTypes = {
  record: PropTypes.object,
}

export default class FoodEdit extends React.Component {
  state = {
    categories: [],
    initialised: false,
    selectedCategory: null,
  }

  render() {
    if (this.state.initialised) {
      return (
        <Edit
          title={<Name />}
          {...this.props}
          options={{ selectedCategory: this.state.selectedCategory }}
        >
          <SimpleForm>
            <TextInput label="Name" source="name" localized />

            <RichTextInput label="Description" source="description" localized />

            <SelectInput
              source="category"
              choices={this.state.categories}
              onChange={this._categorySelected}
            />

            <CreateLink bathPath="content/foodCategories" />

            <EditLink
              bathPath="content/foodCategories"
              source="category"
              id={this.state.selectedCategory}
            />

            <RadioButtonGroupInput
              source="usefulness"
              defaultValue={2}
              choices={[
                { id: 0, name: 'Danger' },
                { id: 1, name: 'Warning' },
                { id: 2, name: 'Good' },
              ]}
            />
          </SimpleForm>
        </Edit>
      )
    }
    return null
  }

  async componentWillMount() {
    const snap = await firebase
      .database()
      .ref('/content/foodCategories')
      .once('value')

    const categories = snap.val()

    this.setState({
      initialised: true,
      categories: categories
        ? Object.keys(categories).map(key => ({
            id: key,
            name: categories[key].name.en,
          }))
        : [],
    })
  }

  _categorySelected = (data, value) => {
    this.setState({ selectedCategory: value })
  }
}
