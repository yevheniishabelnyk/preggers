/**
 *
 * Foods resource props
 *
 */

import icon from 'material-ui/svg-icons/maps/local-pizza'
import list from './list'
import create from './create'
import show from './show'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/foods',
  options: { label: 'Foods' },
  icon,
  list,
  create,
  show,
  edit,
  remove: Delete,
}
