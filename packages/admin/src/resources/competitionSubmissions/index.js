/**
 *
 * CompetitionSubmissions resource props
 *
 */

import list from './list'
import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'competitionSubmissions',
  options: { label: 'Competition Submissions' },
  list,
  show,
  remove: Delete,
}
