/**
 *
 * CompetitionSubmissions List
 *
 */

import React from 'react'

import {
  List,
  Datagrid,
  TextField,
  ShowButton,
  ReferenceField,
  DateField,
  TextInput,
  Filter,
  NumberField,
} from 'admin-on-rest'

import DeleteButton from 'app/components/DeleteButton'

const SubmissionFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
  </Filter>
)

export default class CompetitionSubmissionsList extends React.Component {
  render() {
    return (
      <List {...this.props} filters={<SubmissionFilter />}>
        <Datagrid>
          <TextField label="Competition" source="competitionName" />

          <ReferenceField label="User" source="userUid" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <ReferenceField label="email" source="userUid" reference="users">
            <TextField source="email" />
          </ReferenceField>

          <ReferenceField
            label="Due Date"
            source="userPregnancy"
            reference="pregnancies"
          >
            <DateField source="dueDate" />
          </ReferenceField>

          <DateField label="Created at" source="createdAt" />

          <NumberField source="answerQuestionOne" label="Answer" />

          <ShowButton basePath="/competitionSubmissions" />

          <DeleteButton basePath="/competitionSubmissions" />
        </Datagrid>
      </List>
    )
  }
}
