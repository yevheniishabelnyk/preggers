/*

CompetitionSubmissionShow

**/

import React from 'react'

import {
  Show,
  TextField,
  ReferenceField,
  DateField,
  SimpleShowLayout,
  RichTextField,
} from 'admin-on-rest'

const CompetitionSubmissionShow = props => (
  <Show title="Submission" {...props}>
    <SimpleShowLayout>
      <TextField label="Competition" source="competitionName" />

      <ReferenceField label="User" source="userUid" reference="users">
        <TextField source="name" />
      </ReferenceField>

      <ReferenceField label="email" source="userUid" reference="users">
        <TextField source="email" />
      </ReferenceField>

      <ReferenceField
        label="Due Date"
        source="userPregnancy"
        reference="pregnancies"
      >
        <DateField source="dueDate" />
      </ReferenceField>

      <DateField label="Created at" source="createdAt" />

      <TextField label="Answer" source="answerQuestionOne" />

      <RichTextField label="Motivation" source="answerQuestionTwo" />
    </SimpleShowLayout>
  </Show>
)

export default CompetitionSubmissionShow
