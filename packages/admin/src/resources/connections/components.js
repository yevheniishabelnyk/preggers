import React from 'react'
import PropTypes from 'prop-types'
import Chip from 'material-ui/Chip'

import { BABY_NAMES, CONTRACTION_TIMER, TIMELINE, CHECKLIST } from './constants'

const getShareItems = share => {
  const results = []

  if (share & BABY_NAMES) {
    results.push('Baby Names')
  }

  if (share & CONTRACTION_TIMER) {
    results.push('Contraction Timer')
  }

  if (share & TIMELINE) {
    results.push('Timeline')
  }

  if (share & CHECKLIST) {
    results.push('Checklist')
  }

  return results
}

const labelStyle = {
  lineHeight: '22px',
  fontFamily: 'Roboto, sans-serif',
  fontSize: '13px',
  marginLeft: '-10px',
  userSelect: 'none',
  color: 'rgba(0, 0, 0, 0.3)',
}

export const ShareField = ({ source, record, elStyle, label }) => (
  <div style={{ margin: 10 }}>
    {label ? (
      <label style={labelStyle}>
        <span>{label}</span>
      </label>
    ) : null}

    {getShareItems(record[source]).map(item => (
      <Chip key={item} style={elStyle}>
        {item}
      </Chip>
    ))}
  </div>
)

ShareField.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  elStyle: PropTypes.object,
  source: PropTypes.string.isRequired,
}

ShareField.defaultProps = {
  source: 'share',
  record: {},
  elStyle: { margin: 4 },
}
