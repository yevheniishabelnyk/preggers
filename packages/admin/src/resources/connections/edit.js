/**
 *
 * ConnectionEdit
 *
 */

import React from 'react'

import { Edit, DisabledInput, SimpleForm, TextInput } from 'admin-on-rest'

const ConnectionEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />

      <TextInput label="From Name" source="fromName" />
      <TextInput label="From Email" source="fromEmail" />

      <TextInput label="To Name" source="toName" />
      <TextInput label="To Email" source="toEmail" />

      <DisabledInput source="status" />
    </SimpleForm>
  </Edit>
)

export default ConnectionEdit
