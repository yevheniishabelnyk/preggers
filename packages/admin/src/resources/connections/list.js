/**
 *
 * ConnectionsList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  ReferenceField,
  EditButton,
  DeleteButton,
  ShowButton,
} from 'admin-on-rest'
import { ShareField } from 'resources/connections/components'

export default class ConnectionsList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Connections">
        <Datagrid>
          <ReferenceField
            label="Invitation"
            source="invitationId"
            reference="invitations"
          >
            <TextField source="id" />
          </ReferenceField>

          <ReferenceField label="From " source="fromId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <ReferenceField label="To" source="toId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <TextField label="From email" source="fromEmail" />

          <TextField label="To email" source="toEmail" />

          <ShareField />

          <TextField source="status" />

          <ShowButton basePath="/connections" />
          <EditButton basePath="/connections" />
          <DeleteButton basePath="/connections" />
        </Datagrid>
      </List>
    )
  }
}
