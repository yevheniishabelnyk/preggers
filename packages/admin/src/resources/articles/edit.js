/**
 *
 * Articles edit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Edit,
  ImageField,
  TextInput,
  required,
  SelectInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import FileInput from 'app/components/FileInput'
import SelectArrayInput from 'app/components/SelectArrayInput'
import SimpleForm from 'app/components/SimpleForm'

import { has } from 'lodash'

import firebase from 'firebase'

const ArticleTitle = ({ record }) => {
  return <span>{record ? `"${record.title}"` : ''}</span>
}

ArticleTitle.propTypes = {
  record: PropTypes.object,
}

export default class ArticlesEdit extends React.Component {
  languages = [{ id: 'en', name: 'English' }, { id: 'sv', name: 'Swedish' }]

  state = {
    initialised: false,
    tagsChoices: [],
  }

  render() {
    if (!this.state.initialised) {
      return null
    }

    return (
      <div style={{ position: 'relative' }}>
        <Edit title={<ArticleTitle />} {...this.props}>
          <SimpleForm>
            <SelectInput source="language" choices={this.languages} />

            <FileInput source="image" label="Image" accept="image/*">
              <ImageField source="uri" />
            </FileInput>

            <TextInput label="Title" source="title" validate={[required]} />

            <SelectArrayInput
              label="Tags"
              source="tags"
              options={{ onBlur: () => {} }}
              choices={this.state.tagsChoices}
              booleanMap
            />

            <TextInput label="Photo source/copyright" source="copyright" />

            <TextInput label="Sponsored by" source="sponsoredBy" />

            <TextInput label="Ingress" source="ingress" />

            <TextInput label="Written by" source="writtenBy" />

            <RichTextInput label="Text" source="text" />
          </SimpleForm>
        </Edit>
      </div>
    )
  }

  async componentWillMount() {
    const tagsSnap = await firebase
      .database()
      .ref('/content/tags')
      .once('value')

    let tagsChoices = []

    if (tagsSnap.exists()) {
      const tags = tagsSnap.val()

      Object.keys(tags).forEach(key => {
        if (has(tags[key], 'name.en')) {
          tagsChoices.push({
            id: key,
            name: tags[key].name.en,
          })
        }
      })
    }

    this.setState({
      initialised: true,
      tagsChoices,
    })
  }
}
