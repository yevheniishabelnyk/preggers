import React from 'react'
import PropTypes from 'prop-types'
import Chip from 'material-ui/Chip'
import { has } from 'lodash'

export const TagsField = ({ source, record, elStyle, tags }) => (
  <div style={{ margin: 10, width: 200, flexWrap: 'wrap', display: 'flex' }}>
    {record[source] && tags
      ? Object.keys(record[source])
          .map(
            tagId =>
              has(tags, `${tagId}.name.en`) ? (
                <Chip key={tagId} style={elStyle}>
                  {tags[tagId].name.en}
                </Chip>
              ) : null
          )
          .filter(item => item != null)
      : null}
  </div>
)

TagsField.propTypes = {
  record: PropTypes.object,
  elStyle: PropTypes.object,
  tags: PropTypes.object,
  source: PropTypes.string.isRequired,
}

TagsField.defaultProps = {
  source: 'share',
  record: {},
  tags: {},
  elStyle: { margin: 4, display: 'flex' },
}
