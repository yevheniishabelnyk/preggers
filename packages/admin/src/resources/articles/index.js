/**
 *
 * Articles resource props
 *
 */

import icon from 'material-ui/svg-icons/av/library-books'
import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/articles',
  options: { label: 'Articles' },
  icon,
  list,
  create,
  edit,
  remove: Delete,
}
