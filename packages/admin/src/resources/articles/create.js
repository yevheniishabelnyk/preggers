/**
 *
 * Articles create
 *
 */

import React from 'react'

import {
  Create,
  TextInput,
  required,
  ImageField,
  SelectInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import FileInput from 'app/components/FileInput'
import SelectArrayInput from 'app/components/SelectArrayInput'
import SimpleForm from 'app/components/SimpleForm'

import { has } from 'lodash'

import firebase from 'firebase'

export default class ArticlesCreate extends React.Component {
  languages = [{ id: 'en', name: 'English' }, { id: 'sv', name: 'Swedish' }]

  state = {
    initialised: false,
    tagsChoices: [],
  }

  render() {
    if (!this.state.initialised) {
      return null
    }

    return (
      <Create title="Create an article" {...this.props}>
        <SimpleForm>
          <SelectInput
            source="language"
            choices={this.languages}
            defaultValue="en"
          />

          <FileInput source="image" label="Image" accept="image/*">
            <ImageField source="uri" />
          </FileInput>

          <TextInput label="Title" source="title" validate={[required]} />

          <SelectArrayInput
            label="Tags"
            source="tags"
            options={{ onBlur: () => {} }}
            choices={this.state.tagsChoices}
            booleanMap
          />

          <TextInput label="Photo source/copyright" source="copyright" />

          <TextInput label="Sponsored by" source="sponsoredBy" />

          <TextInput label="Ingress" source="ingress" />

          <TextInput label="Written by" source="writtenBy" />

          <RichTextInput label="Text" source="text" />
        </SimpleForm>
      </Create>
    )
  }

  async componentWillMount() {
    const tagsSnap = await firebase
      .database()
      .ref('/content/tags')
      .once('value')

    let tagsChoices = []

    if (tagsSnap.exists()) {
      const tags = tagsSnap.val()

      Object.keys(tags).forEach(key => {
        if (has(tags[key], 'name.en')) {
          tagsChoices.push({
            id: key,
            name: tags[key].name.en,
          })
        }
      })
    }

    this.setState({
      initialised: true,
      tagsChoices,
    })
  }
}
