/**
 *
 * Articles list
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { List, Datagrid, TextField, Filter, TextInput } from 'admin-on-rest'
import DeleteButton from 'app/components/DeleteButton'
import EditButton from 'app/components/EditButton'

import firebase from 'firebase'

import { TagsField } from './components'
import SelectArrayInput from 'app/components/SelectArrayInput'
import { has } from 'lodash'

const Actions = ({ record }) => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
    }}
    label="Actions"
  >
    <EditButton
      record={record}
      basePath="/content/articles"
      label=""
      style={{ minWidth: '44px' }}
    />

    <DeleteButton
      record={record}
      basePath="/content/articles"
      label=""
      style={{ minWidth: '44px' }}
    />
  </div>
)

Actions.propTypes = {
  record: PropTypes.object,
}

const TagsFilter = props => (
  <Filter {...props}>
    <SelectArrayInput
      label="Tags"
      source="tags"
      options={{ onBlur: () => {} }}
      choices={props.tagsChoices}
      booleanMap
      alwaysOn
    />

    <TextInput label="Title" source="q" alwaysOn />
  </Filter>
)

TagsFilter.propTypes = {
  tagsChoices: PropTypes.array,
}

export default class ArticlesList extends React.Component {
  state = {
    initialised: false,
    tagsChoices: [],
    tags: {},
  }

  render() {
    if (!this.state.initialised) {
      return null
    }

    return (
      <List
        {...this.props}
        title="Articles"
        filters={<TagsFilter tagsChoices={this.state.tagsChoices} />}
      >
        <Datagrid>
          <TextField source="title" label="Title" />

          <TextField source="stars" label="Stars" />

          <TagsField source="tags" label="Tags" tags={this.state.tags} />

          <TextField source="writtenBy" label="Author" />

          <TextField source="language" label="Language" />

          <Actions title="Actions" />
        </Datagrid>
      </List>
    )
  }

  async componentWillMount() {
    const tagsSnap = await firebase
      .database()
      .ref('/content/tags')
      .once('value')

    let tagsChoices = []
    let tags = {}

    if (tagsSnap.exists()) {
      tags = tagsSnap.val()

      Object.keys(tags).forEach(key => {
        if (has(tags[key], 'name.en')) {
          tagsChoices.push({
            id: key,
            name: tags[key].name.en,
          })
        }
      })
    }

    this.setState({
      initialised: true,
      tagsChoices,
      tags,
    })
  }
}
