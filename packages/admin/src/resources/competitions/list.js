/**
 *
 * Competitions List
 *
 */

import React from 'react'

import {
  List,
  Datagrid,
  TextField,
  DateField,
  BooleanField,
} from 'admin-on-rest'

import DeleteButton from 'app/components/DeleteButton'
import EditButton from 'app/components/EditButton'

const CompetitionsList = props => {
  return (
    <List title="Competitions" {...props}>
      <Datagrid>
        <TextField source="name" />

        <BooleanField label="Active" source="isActive" />

        <DateField label="Created at" source="createdAt" />

        <EditButton basePath="/competitions/competitionList" />

        <DeleteButton basePath="/competitions/competitionList" />
      </Datagrid>
    </List>
  )
}

export default CompetitionsList
