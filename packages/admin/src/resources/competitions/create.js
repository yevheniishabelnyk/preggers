import React from 'react'

import { Create, TextInput, required } from 'admin-on-rest'

import SimpleForm from 'app/components/SimpleForm'

export default class CompetitionCreate extends React.Component {
  render() {
    const defaultValues = {
      isActive: false,
      answerOne: { uid: 1 },
      answerTwo: { uid: 2 },
      answerThree: { uid: 3 },
    }

    return (
      <Create title="Create an Competition" {...this.props}>
        <SimpleForm defaultValue={defaultValues}>
          <TextInput
            label="Image Url"
            source="bannerImageUrl"
            localized
            validate={[required]}
          />

          <TextInput label="Name" source="name" validate={[required]} />

          <TextInput
            label="Header"
            source="header"
            localized
            validate={[required]}
          />

          <TextInput
            label="Description"
            source="description"
            localized
            validate={[required]}
          />

          <TextInput
            label="Button Copy"
            source="buttonCopy"
            localized
            validate={[required]}
          />

          <TextInput
            label="Label"
            source="label"
            localized
            validate={[required]}
          />

          <TextInput
            label="Question One"
            source="questionOne"
            localized
            validate={[required]}
          />

          <TextInput
            label="Answer One"
            source="answerOne.text"
            localized
            validate={[required]}
          />

          <TextInput
            label="Answer Two"
            source="answerTwo.text"
            localized
            validate={[required]}
          />

          <TextInput
            label="Answer Three"
            source="answerThree.text"
            localized
            validate={[required]}
          />

          <TextInput
            label="Question Two"
            source="questionTwo"
            localized
            validate={[required]}
          />

          <TextInput
            label="Link to Terms"
            source="termsLink"
            localized
            validate={[required]}
          />

          <TextInput
            label="Terms Text"
            source="termsText"
            localized
            validate={[required]}
          />
        </SimpleForm>
      </Create>
    )
  }
}
