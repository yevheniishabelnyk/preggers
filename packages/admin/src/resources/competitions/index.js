/**
 *
 * Competitions resource props
 *
 */

import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'competitions/competitionList',
  options: { label: 'Competitions' },
  list,
  create,
  edit,
  remove: Delete,
}
