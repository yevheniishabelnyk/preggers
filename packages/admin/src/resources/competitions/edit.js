import React from 'react'
import PropTypes from 'prop-types'

import {
  Edit,
  TextInput,
  required,
  ImageField,
  SelectInput,
} from 'admin-on-rest'

import SimpleForm from 'app/components/SimpleForm'

const CompetitionTitle = ({ record }) => {
  return <span>{record ? `"${record.name}"` : ''}</span>
}

CompetitionTitle.propTypes = {
  record: PropTypes.object,
}

export default class CompetitionEdit extends React.Component {
  render() {
    const choices = [{ id: true, text: 'true' }, { id: false, text: 'false' }]

    return (
      <Edit title={<CompetitionTitle />} {...this.props}>
        <SimpleForm submitOnEnter={false}>
          <SelectInput
            label="Active Status"
            source="isActive"
            translateChoice={false}
            choices={choices}
            optionText="text"
          />

          <TextInput
            label="Image Url"
            source="bannerImageUrl"
            localized
            validate={[required]}
          />

          <ImageField source="bannerImageUrl.en" />

          <TextInput label="Name" source="name" validate={[required]} />

          <TextInput
            label="Header"
            source="header"
            localized
            validate={[required]}
          />

          <TextInput
            label="Description"
            source="description"
            localized
            validate={[required]}
          />

          <TextInput
            label="Button Copy"
            source="buttonCopy"
            localized
            validate={[required]}
          />

          <TextInput
            label="Label"
            source="label"
            localized
            validate={[required]}
          />

          <TextInput
            label="Question One"
            source="questionOne"
            localized
            validate={[required]}
          />

          <TextInput
            label="Answer One"
            source="answerOne.text"
            localized
            validate={[required]}
          />

          <TextInput
            label="Answer Two"
            source="answerTwo.text"
            localized
            validate={[required]}
          />

          <TextInput
            label="Answer Three"
            source="answerThree.text"
            localized
            validate={[required]}
          />

          <TextInput
            label="Question Two"
            source="questionTwo"
            localized
            validate={[required]}
          />

          <TextInput
            label="Link to Terms"
            source="termsLink"
            localized
            validate={[required]}
          />

          <TextInput
            label="Terms Text"
            source="termsText"
            localized
            validate={[required]}
          />
        </SimpleForm>
      </Edit>
    )
  }
}
