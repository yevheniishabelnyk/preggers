/**
 *
 * PregnanciesList
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  List,
  Datagrid,
  BooleanField,
  TextField,
  ReferenceField,
} from 'admin-on-rest'
import DoneIcon from 'material-ui/svg-icons/action/done'
import ClearIcon from 'material-ui/svg-icons/content/clear'

class ChildrenField extends React.Component {
  static propTypes = {
    record: PropTypes.object,
  }

  static defaultProps = { label: 'Children' }

  render() {
    const { record } = this.props

    return (
      <div>
        <span>{record.childName}</span>
      </div>
    )
  }
}

const LostField = ({ record = {} }) => (
  <div>{record.lossDay ? <DoneIcon /> : <ClearIcon />}</div>
)

LostField.propTypes = {
  record: PropTypes.object,
}

LostField.defaultProps = { label: 'Lost' }

const PregnanciesList = props => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />

      <TextField source="dueDate" label="Due Date" />

      <BooleanField source="canceled" />

      <LostField />

      <ReferenceField
        label="User"
        source="userId"
        reference="users"
        // linkType={false}
      >
        <TextField source="name" />
      </ReferenceField>

      <ChildrenField source="children" />
    </Datagrid>
  </List>
)

export default PregnanciesList
