export const list = [
  'content/countryBabyNames',
  'children',
  'content/checklistItems',
  'content/faqs',
  'content/foodCategories',
  'content/foods',
  'content/tags',
  'content/dealCategories',
  'pregnancies',
  'users',
  'connections',
  'invitations',
  'competitions/competitionList',
  'competitionSubmissions',
  {
    name: 'content/weeks',
    path: '/content/weeks',
    parentKey: 'number',
  },
  // 'content/videos',
  // 'content/videoTags',
  'feedbacks',
  { name: 'content/dailyTips', path: '/content/dailyTips', parentKey: 'day' },
  {
    name: 'content/weeklyTips',
    path: '/content/weeklyTips',
    parentKey: 'week',
  },
  'content/articles',
  'content/otherApps',
  {
    name: 'mailing/weeks',
    path: '/mailing/weeks',
    parentKey: 'number',
  },
]
