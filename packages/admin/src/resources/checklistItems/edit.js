/**
 *
 * Checklist Items edit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Edit,
  TextInput,
  SelectInput,
  NumberInput,
  required,
} from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'
import NestedCollectionInputGroup from 'app/components/NestedCollectionInputGroup'
import { CHECKLIST_ITEMS_OFFER_COLLECTIONS } from './'

const ChecklistItemTitle = ({ record }) => {
  return <span>{record ? `"${record.headline.en}"` : ''}</span>
}

ChecklistItemTitle.propTypes = {
  record: PropTypes.object,
}

export default class ChecklistItemEdit extends React.Component {
  render() {
    return (
      <div style={{ position: 'relative' }}>
        <Edit title={<ChecklistItemTitle />} {...this.props}>
          <SimpleForm>
            <TextInput
              label="Headline"
              source="headline"
              validate={[required]}
              localized
            />

            <TextInput label="Description" source="description" localized />

            <SelectInput
              allowEmpty
              source="offerCollectionKey"
              choices={CHECKLIST_ITEMS_OFFER_COLLECTIONS}
              label="Custom offers collection"
            />

            <NumberInput label="Sort priority" source="sortPriority" />

            <NestedCollectionInputGroup
              label="Todos"
              source="todos"
              inputComponent={<TextInput localized />}
              nestedForm
            />
          </SimpleForm>
        </Edit>
      </div>
    )
  }
}
