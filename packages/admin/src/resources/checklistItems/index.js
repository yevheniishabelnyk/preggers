/**
 *
 * Checklist Items resource props
 *
 */

import icon from 'material-ui/svg-icons/toggle/check-box'
import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export const MODERNA_PREGNANCY_INSURANCE_CAMPAIGN = 'ModernaPregnancyInsurance'
export const CHECKLIST_ITEMS_OFFER_COLLECTIONS = [
  {
    id: MODERNA_PREGNANCY_INSURANCE_CAMPAIGN,
    name: 'Moderna pregnancy insurance campaign',
  },
]

export default {
  name: 'content/checklistItems',
  options: { label: 'Checklist Items' },
  icon,
  list,
  create,
  edit,
  remove: Delete,
}
