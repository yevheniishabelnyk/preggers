/**
 *
 * Checklist Items list
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { List, Datagrid, TextField } from 'admin-on-rest'
import DeleteButton from 'app/components/DeleteButton'
import EditButton from 'app/components/EditButton'

const Actions = ({ record }) => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
    }}
    label="Actions"
  >
    <EditButton
      record={record}
      basePath="/content/checklistItems"
      label=""
      style={{ minWidth: '44px' }}
    />

    <DeleteButton
      record={record}
      basePath="/content/checklistItems"
      label=""
      style={{ minWidth: '44px' }}
    />
  </div>
)

Actions.propTypes = {
  record: PropTypes.object,
}

export default class ChecklistItemsList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Checklist Items">
        <Datagrid>
          <TextField source="headline.en" label="Headline" />
          <TextField source="description.en" label="Description" />
          <Actions title="Actions" />
        </Datagrid>
      </List>
    )
  }
}
