/**
 *
 * ChecklistItems create
 *
 */

import React from 'react'

import {
  Create,
  TextInput,
  SelectInput,
  NumberInput,
  required,
} from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'
import NestedCollectionInputGroup from 'app/components/NestedCollectionInputGroup'
import { CHECKLIST_ITEMS_OFFER_COLLECTIONS } from './'

export default class ChecklistItemsCreate extends React.Component {
  render() {
    return (
      <Create title="Create a cehcklist item" {...this.props}>
        <SimpleForm>
          <TextInput
            label="Headline"
            source="headline"
            validate={[required]}
            localized
          />

          <TextInput label="Description" source="description" localized />

          <SelectInput
            allowEmpty
            source="offersCollectionKey"
            choices={CHECKLIST_ITEMS_OFFER_COLLECTIONS}
            label="Custom offers collection"
          />

          <NumberInput label="Sort priority" source="sortPriority" />

          <NestedCollectionInputGroup
            label="Todos"
            source="todos"
            inputComponent={<TextInput localized />}
            nestedForm
          />
        </SimpleForm>
      </Create>
    )
  }
}
