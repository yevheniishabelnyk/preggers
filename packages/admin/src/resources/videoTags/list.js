/**
 *
 * VideoTagsList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  EditButton,
  DeleteButton,
} from 'admin-on-rest'

export default class VideoTagsList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Video Tags">
        <Datagrid>
          <TextField source="name.en" label="Name" />

          <EditButton />

          <DeleteButton />
        </Datagrid>
      </List>
    )
  }
}
