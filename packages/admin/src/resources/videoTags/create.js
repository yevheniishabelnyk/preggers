/**
 *
 * VideoTagCreate
 *
 */

import React from 'react'

import { Create, SimpleForm, TextInput, required } from 'admin-on-rest'

export default class VideoTagCreate extends React.Component {
  render() {
    return (
      <Create title="Create a Video Tag" {...this.props}>
        <SimpleForm redirect={false}>
          <TextInput source="name.en" validate={[required]} />
          <TextInput source="name.sv" validate={[required]} />
        </SimpleForm>
      </Create>
    )
  }
}
