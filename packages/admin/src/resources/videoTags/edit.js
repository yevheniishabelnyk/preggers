/**
 *
 * VideoTagEdit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Edit, SimpleForm, TextInput, required } from 'admin-on-rest'

const Name = ({ record }) => (
  <span>{record ? `Edit "${record.name.en}" video tag` : ''}</span>
)

Name.propTypes = {
  record: PropTypes.object,
}

export default class VideoTagEdit extends React.Component {
  render() {
    return (
      <Edit title={<Name />} {...this.props}>
        <SimpleForm redirect={false}>
          <TextInput source="name.en" validate={[required]} />
          <TextInput source="name.sv" validate={[required]} />
        </SimpleForm>
      </Edit>
    )
  }
}
