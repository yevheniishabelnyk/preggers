/**
 *
 * videoTags resource props
 *
 */

import list from './list'
import create from './create'
import edit from './edit'

export default {
  name: 'content/videoTags',
  options: { label: 'Video Tags' },
  list,
  create,
  edit,
}
