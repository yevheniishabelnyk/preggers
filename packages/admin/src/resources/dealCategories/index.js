import list from './list'
import create from './create'
import edit from './edit'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/dealCategories',
  options: { label: 'Deals Categories' },
  list,
  create,
  edit,
  remove: Delete,
}
