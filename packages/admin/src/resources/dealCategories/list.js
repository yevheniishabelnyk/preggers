/**
 *
 * DealCategoriesList
 *
 */

import React, { Component } from 'react'
import { List, TextField, Datagrid, DeleteButton } from 'admin-on-rest'
import EditButton from 'app/components/EditButton'

export default class DealCategoriesList extends Component {
  render() {
    return (
      <List {...this.props} title="Deals">
        <Datagrid>
          <TextField source="id" />

          <TextField source="name.en" label="Catagory" />

          <EditButton basePath="/content/deals" />

          <DeleteButton basePath="/content/deals" />
        </Datagrid>
      </List>
    )
  }
}
