/**
 *
 * DealCategoryCreate
 *
 */

import React from 'react'

import { Create, TextInput, required } from 'admin-on-rest'

import SimpleForm from 'app/components/SimpleForm'

export default class DealCategoryCreate extends React.Component {
  render() {
    return (
      <Create title="Create a Category" {...this.props}>
        <SimpleForm>
          <TextInput source="name" validate={[required]} localized />
        </SimpleForm>
      </Create>
    )
  }
}
