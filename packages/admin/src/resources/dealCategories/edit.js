/**
 *
 * DealCategoryEdit
 *
 */

import React from 'react'

import { Edit, TextInput, required } from 'admin-on-rest'

import SimpleForm from 'app/components/SimpleForm'

export default class DealCategoryEdit extends React.Component {
  render() {
    return (
      <Edit title="Edit a Catagory" {...this.props}>
        <SimpleForm>
          <TextInput source="name" validate={[required]} localized />
        </SimpleForm>
      </Edit>
    )
  }
}
