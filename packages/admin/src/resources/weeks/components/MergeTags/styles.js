const styles = {
  container: { marginTop: '2em' },

  header: {
    fontSize: '24px',
    lineHeight: '36px',
    letterSpacing: '-.025em',
    color: 'rgba(0, 0, 0, 0.87)',
    outline: '0',
    marginBottom: '0',
  },

  tag: {
    backgroundColor: '#f2f2f2',
    marginBottom: '18px',
    fontSize: '14px',
    lineHeight: '1.28571429em',
    fontFamily: 'Consolas,"Lucida Console",Courier,monospace',
    whiteSpace: 'pre-wrap',
    padding: '12px 15px',
    maxWidth: '635px',
  },

  tagsList: {
    padding: '0',
  },
}

export default styles
