/**
 *
 * MergeTags
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { List, ListItem } from 'material-ui/List'
import { CardHeader, Card } from 'material-ui/Card'

import styles from './styles'

import './sunburst.css'

export default class MergeTags extends React.Component {
  static propTypes = {
    tags: PropTypes.array,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    handlebars: PropTypes.bool,
    hideBlackLines: PropTypes.bool,
  }

  static defaultProps = {
    title: 'Merge Tags',
  }

  render() {
    const {
      tags = [],
      title,
      subtitle,
      handlebars,
      hideBlackLines,
    } = this.props

    return (
      <Card style={styles.container}>
        <CardHeader
          title={title}
          subtitle={subtitle}
          titleStyle={styles.header}
        />

        {handlebars ? (
          <div
            style={{
              maxWidth: 800,
              marginLeft: 20,
            }}
          >
            {!hideBlackLines ? (
              <div>
                <pre className="sunburst">
                  <span className="EmbeddedSourceBright">
                    {'{{USER_NAME}}'} welcome to Preggers!
                  </span>
                </pre>

                <pre className="sunburst">
                  {/*<span className="MetaTagAll">
                  <span className="MetaTagAll">&lt;</span>
                  <span className="MetaTagAll">div</span>{' '}
                  <span className="MetaTagAll">class</span>=<span className="String">
                    <span className="String">"</span>entry<span className="String">
                      "
                    </span>
                  </span>
                  <span className="MetaTagAll">&gt;</span>
                </span>*/}
                  <span className="EmbeddedSourceBright">
                    {'{{#if HAS_CHILD_NAME}}'}
                  </span>
                  {/*<span className="MetaTagAll">
                  <span className="MetaTagAll">&lt;</span>
                  <span className="MetaTagAll">h1</span>
                  <span className="MetaTagAll">&gt;</span>
                </span>*/}
                  <span className="EmbeddedSourceBright">
                    {'{{ CHILD_NAME }} '}
                  </span>
                  <span className="EmbeddedSourceBright">
                    is a size of a peach.
                  </span>
                  {/*<span className="MetaTagAll">
                  <span className="MetaTagAll">&lt;/</span>
                  <span className="MetaTagAll">h1</span>
                  <span className="MetaTagAll">&gt;</span>
                </span>*/}
                  <span className="EmbeddedSourceBright">{'{{/if}}'}</span>
                  {/*<span className="MetaTagAll">
                  <span className="MetaTagAll">&lt;/</span>
                  <span className="MetaTagAll">div</span>
                  <span className="MetaTagAll">&gt;</span>
                </span>*/}
                </pre>
              </div>
            ) : null}
          </div>
        ) : null}

        <List style={styles.tagsList}>
          {tags.map(tag => (
            <ListItem
              key={tag.name}
              primaryText={<div style={styles.tag}>*|{tag.name}|*</div>}
              secondaryText={tag.description}
              hoverColor="none"
            />
          ))}
        </List>
      </Card>
    )
  }
}
