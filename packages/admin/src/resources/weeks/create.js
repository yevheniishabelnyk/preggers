/**
 *
 * WeeksCreate
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Create,
  TextInput,
  required,
  ImageField,
  SelectInput,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import TabbedForm from 'app/components/TabbedForm'
import ListButton from 'app/components/ListButton'
// import ArticleButton from 'app/components/ArticleButton'
// import PhoneButton from 'app/components/PhoneButton'
import Simulator from 'app/components/Simulator'
import FileInput from 'app/components/FileInput'
import AudioField from 'app/components/AudioField'
import MergeTags from './components/MergeTags'
import { CardActions } from 'material-ui/Card'
import SelectArrayInput from 'app/components/SelectArrayInput'
import FormTab from 'app/components/FormTab'

import { difference, keys, range, has } from 'lodash'

import MERGE_TAGS from './MERGE_TAGS'

import firebase from 'firebase'

const allWeekNumbers = range(3, 44)

const WeekCreateActions = ({
  basePath,
  // data,
  // onToggleSimulator,
  // onAddArticle,
}) => {
  return (
    <CardActions
      style={{
        height: '68px',
        width: '400px',
        zIndex: 2,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        float: 'right',
      }}
    >
      {/*<ArticleButton
        onClick={onAddArticle}
        style={{ minWidth: '44px', marginRight: 0 }}
      />
      <PhoneButton
        onClick={() => onToggleSimulator(data)}
        style={{ minWidth: '44px', marginRight: 0 }}
      />*/}
      <ListButton
        basePath={basePath}
        label=""
        style={{ minWidth: '44px', marginRight: 0 }}
      />
    </CardActions>
  )
}

WeekCreateActions.propTypes = {
  basePath: PropTypes.string,
}

const Title = () => <span>Create a Week</span>

export default class WeeksCreate extends React.Component {
  state = {
    initialised: false,
    simulatorIsVisible: false,
    activeTab: 'general',
    babyArticles: [],
    articlesChoices: [],
    tagsChoices: [],
    weekNumbers: [],
  }

  tabs = ['general', 'baby', 'mother', 'partner']

  render() {
    if (!this.state.initialised) {
      return null
    }

    return (
      <div style={{ position: 'relative' }}>
        <Create
          title={<Title />}
          {...this.props}
          actions={
            <WeekCreateActions
              activeTab={this.state.activeTab}
              // onToggleSimulator={this._toggleSimulator}
              // onAddArticle={this._addArticle}
            />
          }
        >
          <TabbedForm onChangeTab={this._tabChanged}>
            <FormTab label="General">
              <SelectInput
                source="number"
                validate={[required]}
                translateChoice={false}
                choices={this.state.weekNumbers}
                options={{
                  maxHeight: 200,
                }}
                defaultValue={
                  this.state.weekNumbers[0]
                    ? this.state.weekNumbers[0].id
                    : null
                }
              />

              <SelectArrayInput
                label="Articles"
                source="articles"
                options={{ onBlur: () => {} }}
                choices={this.state.articlesChoices}
                booleanMap
              />

              <SelectInput
                label="Featured article"
                source="featuredArticle"
                allowEmpty
                choices={this.state.articlesChoices}
                choiceLocaleFilter={this._choiceLocaleFilter}
                localized
                disableGoogleTranslate
              />

              <SelectArrayInput
                label="Tags"
                source="tags"
                options={{ onBlur: () => {} }}
                choices={this.state.tagsChoices}
                booleanMap
              />

              <FileInput source="image" label="Week picture" accept="image/*">
                <ImageField source="uri" />
              </FileInput>

              <FileInput
                source="shareImage"
                label="Share picture"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput source="title" label="Title" localized />

              <RichTextInput
                source="description"
                label="Description"
                localized
              />
            </FormTab>

            <FormTab label="Baby">
              <TextInput source="babyLength" label="Length" />

              <TextInput source="babyWeight" label="Weight" />

              <FileInput
                source="babyTabImage"
                label="Baby Tab image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <FileInput
                source="babyTabShareImage"
                label="Baby Tab Share Image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput
                source="babyIngressText"
                label="Ingress text"
                localized
              />

              <RichTextInput source="babyArticle" label="Article" localized />

              <FileInput
                source="babyAudio"
                label="Baby Tab audio"
                accept="audio/*"
              >
                <AudioField source="uri" />
              </FileInput>
            </FormTab>

            <FormTab label="Mother">
              <FileInput
                source="motherTabImage"
                label="Mother Tab image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput
                source="motherIngressText"
                label="Ingress text"
                localized
              />

              <RichTextInput source="motherArticle" label="Article" localized />

              <FileInput
                source="motherAudio"
                label="Mother Tab audio"
                accept="audio/*"
              >
                <AudioField source="uri" />
              </FileInput>
            </FormTab>

            <FormTab label="Partner">
              <FileInput
                source="partnerTabImage"
                label="Partner Tab image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput
                source="partnerIngressText"
                label="Ingress text"
                localized
              />

              <RichTextInput
                source="partnerArticle"
                label="Article"
                localized
              />

              <FileInput
                source="partnerAudio"
                label="Partner Tab audio"
                accept="audio/*"
              >
                <AudioField source="uri" />
              </FileInput>
            </FormTab>
          </TabbedForm>
        </Create>

        <MergeTags tags={MERGE_TAGS} />

        <Simulator
          isVisible={
            this.state.activeTab !== 'general' && this.state.simulatorIsVisible
          }
          article={this._getArticle()}
        />
      </div>
    )
  }

  async componentWillMount() {
    const [weeksSnap, articlesSnap, tagsSnap] = await Promise.all([
      firebase
        .database()
        .ref('/content/weeks')
        .once('value'),

      firebase
        .database()
        .ref('/content/articles')
        .once('value'),

      firebase
        .database()
        .ref('/content/tags')
        .once('value'),
    ])

    let tagsChoices = []

    if (tagsSnap.exists()) {
      const tags = tagsSnap.val()

      Object.keys(tags).forEach(key => {
        if (has(tags[key], 'name.en')) {
          tagsChoices.push({
            id: key,
            name: tags[key].name.en,
          })
        }
      })
    }

    let availableWeekNumbers = allWeekNumbers

    if (weeksSnap.exists()) {
      const weeks = weeksSnap.val()

      const busyWeekNumbers = keys(weeks).map(weekNumber => Number(weekNumber))

      availableWeekNumbers = difference(allWeekNumbers, busyWeekNumbers)
    }

    const weekNumbers = availableWeekNumbers.map(number => ({
      id: number,
      name: number,
    }))

    let articlesChoices = []

    if (articlesSnap.exists()) {
      const articles = articlesSnap.val()

      articlesChoices = Object.keys(articles).map(key => ({
        id: key,
        name: articles[key].title ? articles[key].title : 'Unknown',
        language: articles[key].language ? articles[key].language : undefined,
      }))
    }

    this.setState({
      initialised: true,
      articlesChoices,
      weekNumbers,
      tagsChoices,
    })
  }

  _choiceLocaleFilter = (item, locale) => item && item.language === locale

  _tabChanged = index =>
    this.setState({
      activeTab: this.tabs[index],
    })

  _toggleSimulator = () => {
    switch (this.state.activeTab) {
      case 'baby':
      case 'mother':
      case 'partner':
        this.setState(prevState => ({
          simulatorIsVisible: !prevState.simulatorIsVisible,
        }))

        break

      default:
        return
    }
  }

  _addArticle = () =>
    this.setState(prevState => ({
      babyArticles: prevState.babyArticles.length
        ? prevState.babyArticles.concat(
            prevState.babyArticles[prevState.babyArticles.length - 1] + 1
          )
        : prevState.babyArticles.concat(0),
    }))

  _setValue = (key, value) => this.setState({ [key]: value })

  _getArticle = () => {
    const { activeTab } = this.state

    switch (activeTab) {
      case 'baby':
        return this.state.babyArticleEn

      case 'mother':
        return this.state.motherArticleEn

      case 'partner':
        return this.state.partnerArticleEn

      default:
        return ''
    }
  }
}
