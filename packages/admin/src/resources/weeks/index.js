/**
 *
 * babyWeeks resource props
 *
 */

import icon from 'material-ui/svg-icons/action/picture-in-picture'
import list from './list'
import create from './create'
import edit from './edit'
// import show from './show'
import { Delete } from 'admin-on-rest'

export default {
  name: 'content/weeks',
  options: { label: 'Weeks' },
  icon,
  list,
  create,
  edit,
  // show,
  remove: Delete,
}
