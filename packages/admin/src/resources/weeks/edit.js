/**
 *
 * WeeksEdit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Edit,
  SelectInput,
  ImageField,
  TextInput,
  required,
} from 'admin-on-rest'
import RichTextInput from 'shared/components/RichTextInput'
import TabbedForm from 'app/components/TabbedForm'
import FormTab from 'app/components/FormTab'
import FileInput from 'app/components/FileInput'
import AudioField from 'app/components/AudioField'
import MergeTags from './components/MergeTags'
import MERGE_TAGS from './MERGE_TAGS'
import SelectArrayInput from 'app/components/SelectArrayInput'

import firebase from 'firebase'
import { difference, range, keys, has } from 'lodash'
import { toOrdinal } from 'ordinal-js'

const allWeekNumbers = range(3, 44)

const Title = ({ record }) => <span>{toOrdinal(record.number)} week</span>

Title.propTypes = {
  record: PropTypes.object,
}

export default class WeeksEdit extends React.Component {
  static propTypes = {
    match: PropTypes.object,
  }

  state = {
    initialised: false,
    simulatorIsVisible: false,
    activeTab: 'general',
    babyArticles: [],
    articlesChoices: [],
    tagsChoices: [],
    weekNumbers: [],
  }

  tabs = ['general', 'baby', 'mother', 'partner']

  render() {
    if (!this.state.initialised) {
      return null
    }

    return (
      <div style={{ position: 'relative' }}>
        <Edit
          title={<Title />}
          {...this.props}
          // actions={
          //   <WeekActions
          //     activeTab={this.state.activeTab}
          //     onToggleSimulator={this._toggleSimulator}
          //     onAddArticle={this._addArticle}
          //   />
          // }
        >
          <TabbedForm onChangeTab={this._tabChanged} redirect={false}>
            <FormTab label="General">
              <SelectInput
                source="number"
                validate={[required]}
                translateChoice={false}
                choices={this.state.weekNumbers}
                defaultValue={this.weekNumber}
                options={{
                  maxHeight: 200,
                }}
              />

              <SelectArrayInput
                label="Articles"
                source="articles"
                options={{ onBlur: () => {} }}
                choices={this.state.articlesChoices}
                booleanMap
              />

              <SelectInput
                label="Featured article"
                source="featuredArticle"
                allowEmpty
                choices={this.state.articlesChoices}
                choiceLocaleFilter={this._choiceLocaleFilter}
                localized
                disableGoogleTranslate
              />

              <SelectArrayInput
                label="Tags"
                source="tags"
                options={{ onBlur: () => {} }}
                choices={this.state.tagsChoices}
                booleanMap
              />

              <FileInput source="image" label="Week picture" accept="image/*">
                <ImageField source="uri" />
              </FileInput>

              <FileInput
                source="shareImage"
                label="Share picture"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput source="title" label="Title" localized />

              <RichTextInput
                source="description"
                label="Description"
                localized
              />
            </FormTab>

            <FormTab label="Baby">
              <TextInput source="babyLength" label="Length" />

              <TextInput source="babyWeight" label="Weight" />

              <FileInput
                source="babyTabImage"
                label="Baby Tab image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <FileInput
                source="babyTabShareImage"
                label="Baby Tab Share Image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput
                source="babyIngressText"
                label="Ingress text"
                localized
              />

              <RichTextInput source="babyArticle" label="Article" localized />

              <FileInput
                source="babyAudio"
                label="Baby Tab audio"
                accept="audio/*"
              >
                <AudioField source="uri" />
              </FileInput>
            </FormTab>

            <FormTab label="Mother">
              <FileInput
                source="motherTabImage"
                label="Mother Tab image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput
                source="motherIngressText"
                label="Ingress text"
                localized
              />

              <RichTextInput source="motherArticle" label="Article" localized />

              <FileInput
                source="motherAudio"
                label="Mother Tab audio"
                accept="audio/*"
              >
                <AudioField source="uri" />
              </FileInput>
            </FormTab>

            <FormTab label="Partner">
              <FileInput
                source="partnerTabImage"
                label="Partner Tab image"
                accept="image/*"
              >
                <ImageField source="uri" />
              </FileInput>

              <TextInput
                source="partnerIngressText"
                label="Ingress text"
                localized
              />

              <RichTextInput
                source="partnerArticle"
                label="Article"
                localized
              />

              <FileInput
                source="partnerAudio"
                label="Partner Tab audio"
                accept="audio/*"
              >
                <AudioField source="uri" />
              </FileInput>
            </FormTab>
          </TabbedForm>
        </Edit>

        <MergeTags tags={MERGE_TAGS} />
      </div>
    )
  }

  async componentWillMount() {
    const [weeksSnap, articlesSnap, tagsSnap] = await Promise.all([
      firebase
        .database()
        .ref('/content/weeks')
        .once('value'),

      firebase
        .database()
        .ref('/content/articles')
        .once('value'),

      firebase
        .database()
        .ref('/content/tags')
        .once('value'),
    ])

    let tagsChoices = []

    if (tagsSnap.exists()) {
      const tags = tagsSnap.val()

      Object.keys(tags).forEach(key => {
        if (has(tags[key], 'name.en')) {
          tagsChoices.push({
            id: key,
            name: tags[key].name.en,
          })
        }
      })
    }

    let availableWeekNumbers = allWeekNumbers

    if (weeksSnap.exists()) {
      const weeks = weeksSnap.val()

      const busyWeekNumbers = keys(weeks).map(weekNumber => Number(weekNumber))

      availableWeekNumbers = difference(allWeekNumbers, busyWeekNumbers)
    }

    const weekNumbers = availableWeekNumbers.map(number => ({
      id: number,
      name: number,
    }))

    let articlesChoices = []

    if (articlesSnap.exists()) {
      const articles = articlesSnap.val()

      articlesChoices = Object.keys(articles).map(key => ({
        id: key,
        name: articles[key].title ? articles[key].title : 'Unknown',
        language: articles[key].language ? articles[key].language : undefined,
      }))
    }

    const weekId = this.props.match.params.id

    this.weekNumber = Number(weekId)
    weekNumbers.unshift({
      id: this.weekNumber,
      name: this.weekNumber,
    })

    this.setState({
      initialised: true,
      weekNumbers,
      articlesChoices,
      tagsChoices,
    })
  }

  _choiceLocaleFilter = (item, locale) => item && item.language === locale

  _tabChanged = index => this.setState({ activeTab: this.tabs[index] })

  _setValue = (key, value) => this.setState({ [key]: value })
}
