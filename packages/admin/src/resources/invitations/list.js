/**
 *
 * InvitationsList
 *
 */

import React from 'react'
import {
  List,
  Datagrid,
  TextField,
  ReferenceField,
  EditButton,
  BooleanField,
  DeleteButton,
  ShowButton,
} from 'admin-on-rest'
import { ShareField } from 'resources/connections/components'

export default class InvitationsList extends React.Component {
  render() {
    return (
      <List {...this.props} title="Invitations">
        <Datagrid>
          <ReferenceField
            label="Connection"
            source="connectionId"
            reference="connections"
          >
            <TextField source="id" />
          </ReferenceField>

          <ReferenceField label="From " source="fromId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <ReferenceField label="To" source="toId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <TextField label="From email" source="fromEmail" />

          <TextField label="To email" source="toEmail" />

          <ShareField />

          <TextField source="status" />

          <BooleanField source="verified" />

          <ShowButton basePath="/invitations" />
          <EditButton basePath="/invitations" />
          <DeleteButton basePath="/invitations" />
        </Datagrid>
      </List>
    )
  }
}
