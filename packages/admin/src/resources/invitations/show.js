/**
 *
 * InvitationShow
 *
 */

import React from 'react'
import {
  Show,
  SimpleShowLayout,
  TextField,
  BooleanField,
  ReferenceField,
} from 'admin-on-rest'
import { ShareField } from 'resources/connections/components'

export default class InvitationShow extends React.Component {
  render() {
    return (
      <Show {...this.props}>
        <SimpleShowLayout>
          <ReferenceField label="To Name" source="toId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <ReferenceField label="To Email" source="toId" reference="users">
            <TextField source="email" />
          </ReferenceField>

          <ReferenceField label="From Name" source="fromId" reference="users">
            <TextField source="name" />
          </ReferenceField>

          <ReferenceField label="From Email" source="fromId" reference="users">
            <TextField source="email" />
          </ReferenceField>

          <TextField source="status" />

          <BooleanField source="verified" elStyle={{ width: '40px' }} />

          <ShareField label="Share" />
        </SimpleShowLayout>
      </Show>
    )
  }
}
