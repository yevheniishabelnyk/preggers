/**
 *
 * FoodCategoriesList
 *
 */

import React from 'react'
import { List, Datagrid, TextField, EditButton } from 'admin-on-rest'

export default class FoodCategoriesList extends React.Component {
  render() {
    return (
      <List
        {...this.props}
        // sort={{ field: 'name', order: 'DESC' }}
        title="Food Categories"
      >
        <Datagrid>
          <TextField source="name.en" />

          <EditButton basePath="/content/foodCategories" />
        </Datagrid>
      </List>
    )
  }
}
