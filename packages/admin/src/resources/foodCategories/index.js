/**
 *
 * FoodCategories resource props
 *
 */

// import icon from 'material-ui/svg-icons/maps/local-pizza'
import list from './list'
import create from './create'
import edit from './edit'

export default {
  name: 'content/foodCategories',
  // icon,
  list,
  create,
  edit,
}
