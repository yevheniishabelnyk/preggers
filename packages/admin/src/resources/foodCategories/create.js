/**
 *
 * FoodCategoryCreate
 *
 */

import React from 'react'

import { Create, TextInput, required } from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'

export default class FoodCategoryCreate extends React.Component {
  render() {
    return (
      <Create title="Create a Food Category" {...this.props}>
        <SimpleForm redirect={false}>
          <TextInput
            label="Name"
            source="name"
            validate={[required]}
            localized
          />
        </SimpleForm>
      </Create>
    )
  }
}
