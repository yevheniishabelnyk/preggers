/**
 *
 * FoodCategoryEdit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Edit, TextInput, required } from 'admin-on-rest'
import SimpleForm from 'app/components/SimpleForm'

const Name = ({ record }) => (
  <span>{record ? `Edit "${record.name.en}" food category` : ''}</span>
)

Name.propTypes = {
  record: PropTypes.object,
}

export default class FoodCategoryEdit extends React.Component {
  render() {
    return (
      <Edit title={<Name />} {...this.props}>
        <SimpleForm redirect={false}>
          <TextInput
            label="Name"
            source="name"
            validate={[required]}
            localized
          />
        </SimpleForm>
      </Edit>
    )
  }
}
