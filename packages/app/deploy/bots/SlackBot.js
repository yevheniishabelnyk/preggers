/**
 *
 * SlackBot
 *
 */

'use strict'

const fs = require('fs')
const path = require('path')
const request = require('request')
const QRCodeToFile = require('../utils/QRCodeToFile')
const { WebClient } = require('@slack/client')
const config = require('../config')

class SlackBot {
  constructor(props) {
    this.token = props.token

    this.slack = new WebClient(props.token)

    this.sendMessage = this.sendMessage.bind(this)
    this.searchFilePublicUrl = this.searchFilePublicUrl.bind(this)
    this.sendDeploymentReport = this.sendDeploymentReport.bind(this)
    this.getQRCodeImageUrl = this.getQRCodeImageUrl.bind(this)
    this.uploadFile = this.uploadFile.bind(this)
  }

  sendMessage(data, releaseChannel) {
    let webhookUrl

    switch (releaseChannel) {
      case 'default':
      case 'prod-v1':
        webhookUrl = config.webhookUrls.deploy
        break

      case 'sandbox':
      case 'android-sandbox':
        webhookUrl = config.webhookUrls.deploy_stage
        break

      default:
        webhookUrl = config.webhookUrls.deploy
    }

    return new Promise((resolve, reject) => {
      request.post(
        webhookUrl,
        {
          form: {
            payload: JSON.stringify(data),
          },
        },
        function(err, response) {
          if (err) {
            return reject(err)
          }

          if (response.body !== 'ok') {
            return reject(new Error(response.body))
          }

          resolve(response.body)
        }
      )
    })
  }

  searchFilePublicUrl(fileName) {
    return new Promise((resolve, reject) =>
      this.slack.search
        .files(fileName)
        .then(data => {
          if (data.files.total > 0) {
            const file = data.files.matches[0]
            const publickUrl = file.permalink_public

            resolve(publickUrl)
          } else {
            reject()
          }
        })
        .catch(reject)
    )
  }

  sendDeploymentReport({ app, publisher, gitData, addReview }) {
    const deepLink = app.url.replace('https://exp.host', 'exp://expo.io')

    const message = {
      attachments: [],
      unfurl_links: 0,
    }

    const publishedDate = new Date(app.publishedTime)

    const appAttachment = {
      fallback: `${app.name} app published a new release!`,
      pretext: `${
        app.name
      } app published a new release! <${deepLink}|Open in Expo>`,
      fields: [
        {
          title: 'Version',
          value: app.version,
          short: true,
        },
        {
          title: 'Build Number',
          value: app.iOSBuildNumber,
          short: true,
        },
        {
          title: 'Channel',
          value: app.releaseChannel,
          short: true,
        },
        {
          title: 'Release ID',
          value: app.releaseId,
          short: true,
        },
        {
          title: 'Expo',
          value: app.sdkVersion,
          short: true,
        },
        {
          title: 'Privacy',
          value: app.privacy,
          short: true,
        },
        {
          title: 'Link',
          value: app.url,
          short: false,
        },
      ],
      footer: 'Expo',
      footer_icon: 'https://s3.amazonaws.com/exp-brand-assets/ExpoIcon_200.png',
      ts: publishedDate.getTime() / 1000,
    }

    const publisherAttachment = {
      fallback: '',
      fields: [
        {
          title: 'Publisher',
          value: `<@${publisher.slackUsername}>`,
          short: true,
        },
        {
          title: 'Git branch',
          value: `<${gitData.originUrl}/branch/${gitData.currentBranch}|${
            gitData.currentBranch
          }>`,
          short: true,
        },
      ].concat(
        gitData.lastCommits
          ? {
              title: 'Last commits',
              value: gitData.lastCommits,
              short: false,
            }
          : []
      ),
    }

    message.attachments.push(appAttachment)
    message.attachments.push(publisherAttachment)

    if (addReview) {
      const reviewAttachment = {
        fallback: 'What do you think about it?',
        title: 'What do you think about it?',
        callback_id: 'app_release_review_actions',
        color: '#3AA3E3',
        attachment_type: 'default',
        actions: [
          {
            name: 'review_action',
            text: ':+1: Approve',
            type: 'button',
            value: 'approved',
          },
          {
            name: 'review_action',
            text: ':x: Reject',
            type: 'button',
            value: 'rejected',
          },
        ],
      }

      message.attachments.push(reviewAttachment)
    }

    return new Promise((resolve, reject) => {
      this.getQRCodeImageUrl(app.url)
        .then(QRCodeImageUrl => {
          appAttachment.thumb_url = QRCodeImageUrl

          this.sendMessage(message, app.releaseChannel)
            .then(() => resolve(message))
            .catch(reject)
        })
        .catch(() => {
          this.sendMessage(message, app.releaseChannel)
            .then(resolve)
            .catch(reject)
        })
    })
  }

  getQRCodeImageUrl(string) {
    return new Promise((resolve, reject) => {
      const encodedString = encodeURIComponent(string)
      const fileName = `${encodedString}_qr_code.png`

      this.searchFilePublicUrl(fileName)
        .then(fileUrl => {
          resolve(fileUrl)
        })
        .catch(err => {
          console.info('searchFilePublicUrl err: ', err)

          const localFilePath = path.join(__dirname, fileName)

          QRCodeToFile(localFilePath, string)
            .then(() => {
              this.uploadFile(fileName, localFilePath)
                .then(data => {
                  fs.unlinkSync(localFilePath)

                  if (data.file.permalink_public) {
                    this.slack.files
                      .sharedPublicURL(data.file.id)
                      .then(() => {
                        resolve(data.file.permalink_public)
                      })
                      .catch(reject)
                  } else {
                    reject()
                  }
                })
                .catch(err => {
                  console.log('uploadFile error', err)
                  reject(err)
                })
            })
            .catch(err => {
              console.info('QRCodeToFile err: ', err)

              reject(err)
            })
        })
    })
  }

  uploadFile(filename, filePath) {
    return this.slack.files.upload(filename, {
      file: fs.createReadStream(filePath),
    })
  }
}

module.exports = SlackBot
