#!/bin/bash

if [[ "$1" != "" && "$1" != "sandbox" && "$1" != "prod-v1" && "$1" != "--build-ios" && "$1" != "--build-android" ]]; then
  echo
  echo "Release channel parameter is wrong. Run with the following parameter:"
  echo "deploy [release_channel] [--build-ios | --build-android]"
  echo "<release_channel> = null | sandbox | prod-v1"
  echo "--build-ios | --build-android = optional option to do a build of the standalone app for specific platform"
  echo

  exit
else
  releasechannel=$1
fi

if [[ "$releasechannel" == "" || "$releasechannel" == "--build-ios" || "$releasechannel" == "--build-android" ]]; then
  releasechannel="default"
fi

buildstandalone=false
if [[ "$1" == "--build-ios" || "$2" == "--build-ios" ||  "$1" == "--build-android" || "$2" == "--build-android" ]]; then
  buildstandalone=true
fi

platform=false
if [[ "$1" == "--build-ios" || "$2" == "--build-ios" ]]; then
  platform="ios"
else
  if [[ "$1" == "--build-android" || "$2" == "--build-android" ]]; then
    platform="android"
  fi
fi


# get buildNumber/versionCode from config

buildNumber="$(grep -o '"buildNumber": "[^"]*' app.json | grep -o '[^"]*$')"
versionCode="$(grep -o '"versionCode": [^,"]*' app.json | grep -o '[^": ]*$')"

environment="staging"
if [ "$releasechannel" == "prod-v1" ]; then
  environment="production"
else
  if [ "$releasechannel" == "sandbox" ]; then
    environment="staging"
  fi
fi

echo "Environment: $environment"
echo "Channel: $releasechannel"
echo "Publish: true"
echo "Build: $buildstandalone"

# check build number of version code if build

if [[ "$buildstandalone" == true && "$platform" == "ios" ]]; then
  echo "iOS build number: $buildNumber"

  echo
  read -p "Do you want to update iOS build number? (y/n) " -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo
    read -p "Enter new build number: " -n 3 -r

    if [ "$REPLY" -le "$buildNumber" ]; then
      echo "New build number should be greater then current."
      echo
      read -p "Enter new build number: " -n 3 -r

      if [ "$REPLY" -le "$buildNumber" ]; then
        echo "See you next time. :)"
        exit
      fi
    fi

    buildNumber=$REPLY

    echo
    echo "Environment: $environment"
    echo "Channel: $releasechannel"
    echo "Publish: true"
    echo "Build: $buildstandalone"
    echo "Platform: iOS"
    echo "iOS Build Number: $buildNumber"
  fi
fi

# check version code if Android build

if [[ "$buildstandalone" == true &&  "$platform" == "android" ]]; then
  echo "Android version code: $versionCode"

  echo
  read -p "Do you want to update the Android version code? (y/n) " -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo
    read -p "Enter new version code: " -n 3 -r

    if [ $REPLY -le $versionCode ]; then
      echo "New version code should be greater then current."
      echo
      read -p "Enter new version code: " -n 3 -r

      if [ $REPLY -le $versionCode ]; then
        echo "See you next time. :)"
        exit
      fi
    fi

    versionCode=$REPLY

    echo
    echo "Environment: $environment"
    echo "Channel: $releasechannel"
    echo "Publish: true"
    echo "Build: $buildstandalone"
    echo "Platform: Android"
    echo "Android Version Code: $versionCode"
  fi
fi

# Ask for confirmation
echo
read -p "Are you sure? (y/n)" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
    cp app.json original_app.json

    node > app.json <<EOF
      var appConfig = require('./original_app.json');
      var config = require('./config/$environment.json');

      appConfig.expo.ios.buildNumber = "$buildNumber"
      appConfig.expo.android.versionCode = $versionCode

      switch ("$environment") {
        case 'production':
          appConfig.expo.facebookDisplayName = "Preggers";
          appConfig.expo.facebookAppId = config.FACEBOOK__APP_ID;
          appConfig.expo.facebookScheme = 'fb' + config.FACEBOOK__APP_ID
          break;

        case 'staging':
          appConfig.expo.facebookDisplayName = "Preggers Staging";
          appConfig.expo.facebookAppId = config.FACEBOOK__APP_ID;
          appConfig.expo.facebookScheme = 'fb' + config.FACEBOOK__APP_ID
          break;
      }

      console.log(JSON.stringify(appConfig, null, 2));
EOF

  if [ "$buildstandalone" == true ]; then
    if [ "$platform" == "ios" ]; then
      echo "Building standalone iOS app for release channel $releasechannel"
      echo

      expo-cli build:ios --release-channel $releasechannel
    fi

    if [ "$platform" == "android" ]; then
      echo "Building standalone Android app for release channel $releasechannel"
      echo

      expo-cli build:android --release-channel $releasechannel
    fi
  else
    echo "Publishing to release channel $releasechannel"
    echo

    expo-cli publish --release-channel $releasechannel
  fi

    # Restore original app.json with last build number
    node > app.json <<EOF
      var appConfig = require('./original_app.json');

      appConfig.expo.ios.buildNumber = "$buildNumber"
      appConfig.expo.android.versionCode = $versionCode

      console.log(JSON.stringify(appConfig, null, 2));
EOF
    rm -rf original_app.json
    ../../node_modules/.bin/prettier --write app.json
fi

exit
