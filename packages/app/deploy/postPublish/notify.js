/**
 *
 * Expo post publish notification
 *
 */

const Git = require('../utils/Git')
const SlackBot = require('../bots/SlackBot')
const config = require('../config')
const path = require('path')
const _ = require('lodash')

const projectDir = path.join(__dirname, '..', '..')

const slackBot = new SlackBot({
  token: config.slackToken,
})

module.exports = ({ url, iosManifest } = {}) =>
  new Promise((resolve, reject) => {
    process.chdir(projectDir)

    return Promise.all([
      Git.getOriginUrl(),
      Git.getCurrentBranch(),
      Git.getUserEmail(),
    ])
      .then(([gitOriginUrl, gitCurrentBranch, gitUserEmail]) => {
        const gitData = {
          originUrl: gitOriginUrl,
          currentBranch: gitCurrentBranch,
          userEmail: gitUserEmail,
        }

        return new Promise(resolve => {
          Git.getLastTenCommits(gitCurrentBranch, 'master')
            .then(lastCommits => {
              gitData.lastCommits = lastCommits

              resolve(gitData)
            })
            .catch(err => {
              console.info('getLastTenCommits error: ', err)

              resolve(gitData)
            })
        })
      })
      .then(gitData => {
        const publisher = _.find(config.users, {
          gitUserEmail: gitData.userEmail,
        })

        let urlWithChannel = url

        if (iosManifest.releaseChannel !== 'default') {
          urlWithChannel = `${url}?release-channel=${
            iosManifest.releaseChannel
          }`
        }

        return slackBot
          .sendDeploymentReport({
            app: {
              name: iosManifest.name,
              version: iosManifest.version,
              iOSBuildNumber: iosManifest.ios.buildNumber,
              releaseId: iosManifest.revisionId,
              url: urlWithChannel,
              publishedTime: iosManifest.publishedTime,
              privacy: iosManifest.privacy,
              sdkVersion: iosManifest.sdkVersion,
              releaseChannel: iosManifest.releaseChannel,
            },
            publisher,
            gitData: gitData,
            addReview: process.env.RELEASE_REVIEW === 'true',
          })
          .then(() => resolve(JSON.stringify(iosManifest, null, 4)))
          .catch(err => reject(JSON.stringify(err, null, 4)))
      })
      .then(() => {
        resolve()
      })
      .catch(err => resolve('Error:\n' + JSON.stringify(err, null, 4)))
  })
