/**
 *
 * QRCodeToFile
 *
 */

const QRCode = require('qrcode')

module.exports = (filePath, string) =>
  new Promise((resolve, reject) => {
    QRCode.toFile(filePath, string, function(err) {
      if (err) {
        return reject(err)
      }

      return resolve()
    })
  })
