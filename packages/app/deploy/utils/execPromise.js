let cp = require('child_process')

module.exports = function(command, opts) {
  return new Promise(function(resolve, reject) {
    return cp.exec(command, opts, function(err, result) {
      if (err) {
        reject(err)
      } else {
        resolve(result)
      }
    })
  })
}
