/**
 *
 * getCurrentBranch
 *
 */

const git = require('simple-git')
const trim = require('trim')

module.exports = () =>
  new Promise((resolve, reject) => {
    const GitClient = git()

    GitClient.branchLocal((err, branchSummary) => {
      if (err) {
        return reject(__dirname)
      }

      if (!branchSummary.current) {
        return reject(process.cwd())
      }

      return resolve(trim(branchSummary.current))
    })
  })
