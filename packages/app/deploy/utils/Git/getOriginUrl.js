/**
 *
 * getOriginUrl
 *
 */

const git = require('simple-git')
const trim = require('trim')
const url = require('url')

module.exports = () =>
  new Promise((resolve, reject) => {
    const GitClient = git()

    GitClient._run(['config', '--get', 'remote.origin.url'], function(
      err,
      originUrl
    ) {
      if (err) {
        return reject(err)
      }

      let result = trim(originUrl).replace('.git', '')

      const urlObj = url.parse(result)
      delete urlObj.auth

      result = url.format(urlObj)

      return resolve(result)
    })
  })
