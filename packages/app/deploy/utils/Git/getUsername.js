/**
 *
 * getUsername
 *
 */

const git = require('simple-git')
const trim = require('trim')

module.exports = () =>
  new Promise((resolve, reject) => {
    const GitClient = git()

    GitClient._run(['config', '--global', 'user.name'], function(
      err,
      username
    ) {
      if (err) {
        return reject(err)
      }

      return resolve(trim(username))
    })
  })
