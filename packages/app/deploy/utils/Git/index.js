/**
 *
 * Git utils
 *
 */

exports.getUsername = require('./getUsername')
exports.getUserEmail = require('./getUserEmail')
exports.getOriginUrl = require('./getOriginUrl')
exports.getCurrentBranch = require('./getCurrentBranch')
exports.getLastTenCommits = require('./getLastTenCommits')
// exports.getCurrentBranchParent = require('./getCurrentBranchParent')
