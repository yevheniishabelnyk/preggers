/**
 *
 * getCurrentBranchParent
 *
 */
const exec = require('../execPromise')

module.exports = () =>
  exec(
    "git show-branch -a | grep '*' | grep -v `git rev-parse --abbrev-ref HEAD` | head -n1"
  )
    .then((grepResult = '') => grepResult.match(/\[(.*)\]/))
    .then((matchResult = []) => matchResult[1])
    .then(function(branch = '') {
      console.info('branch: ', branch)

      return branch
    })
    .catch(err => console.info('err: ', err))
