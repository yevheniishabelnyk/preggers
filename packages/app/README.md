![Preggers logo](/assets/icons/logo.png 'Preggers')

# 1. Preggers

> [https://expo.io/@stroller/preggers](https://expo.io/@stroller/preggers)

The complete app for your pregnancy and the time of your child.

- [1. Preggers](#markdown-header-1-preggers)
  - [1.1. Getting started](#markdown-header-11-getting-started)
  - [1.2. Developing](#markdown-header-12-developing)
    - [1.2.1. Deploying / Publishing](#markdown-header-121-deploying-publishing)
    - [1.2.2. Building](#markdown-header-122-building)

## 1.1. Getting started

To run a project on your local machine, execute follows

```shell
git clone https://bitbucket.org/strollerapp/preggers
cd preggers
npm install -g yarn
npm install -g exp
yarn
exp start --lan
exp ios
```

This will install all dependencies and start Expo development server. Then in new Terminal window it will start iOS simulator.

## 1.2. Developing

Run development server

```shell
exp start --lan
```

In new Terminal window open iOS simulator

```shell
exp ios
```

If you want to share you local environment start development server with tunneling and open share link on your phone

```shell
exp start --tunnel
```

### 1.2.1. Deploying / Publishing

If you want to publish or build the app, first make sure that you logged in as `stroller` user.

```shell
exp whoami
```

if not then run logout

```shell
exp logout
```

and login with this credentials:

- login: `stroller`
- password: `strollerdemo`

```shell
exp login
```

After that you can publish project with

```shell
exp publish --release-channel <your-release-channel>
```

If you do not specify a channel, you will publish to the `default` channel.

If you want to add release review attachment in Slack Bot notification message, add `RELEASE_REVIEW=true`

![Release reviews](/assets/readme-images/release-notification-message-with-review-attachment.png 'Release reviews')

```shell
RELEASE_REVIEW=true exp publish --release-channel sandbox
```

Use `sandbox` release channel for testing.

### 1.2.2. Building

If you want to publish or build the app, first make sure that you logged in as `stroller` user.

To build iOS standalone app run

```shell
exp build:ios --release-channel <your-release-channel>
```

## 1.3. Administration

### 1.3.1 iTunes Connect

1.  Install Homebrew if you don't have it already https://brew.sh/
2.  Install FastLane https://docs.fastlane.tools/#getting-started

```shell
brew cask install fastlane
```

3.  To update app name for Swedish language change `./meta/ITC/metadata/sv/name.txt` file and run

```shell
npm run itc:deliver
```

4.  To update app screenshots for Swedish AppStore change `./meta/ITC/screenshots/sv/` folder and run

```shell
npm run itc:deliver
```

5.  To upload .ipa file to TestFlight place it in project root directory and run

```shell
npm run itc:upload
```
