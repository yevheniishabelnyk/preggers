// This file created with CLI app, never change it manually!!!
module.exports.translations = {
  en: require('./en.json'),
  sv: require('./sv.json'),
}
module.exports.localesData = {
  en: require('react-intl/locale-data/en.js'),
  sv: require('react-intl/locale-data/sv.js'),
}
