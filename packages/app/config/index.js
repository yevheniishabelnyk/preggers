/*
 *
 * Merging configs
 *
 */

import { merge } from 'lodash'
import { Constants } from 'expo'

import defaults from './default'

let config

console.info('process.env.NODE_ENV: ', process.env.NODE_ENV)

switch (Constants.manifest.releaseChannel) {
  case 'default':
  case 'prod-v1':
    config = require('./production')
    break

  case 'sandbox':
  case 'android-sandbox':
    config = require('./staging')
    break

  default:
    config = require('./development')
}

module.exports = merge({}, defaults, config)
