/**
 *
 * registerForPushNotificationsAsync
 *
 */

import { Permissions, Notifications } from 'expo'
import * as firebase from 'firebase'
import { values } from 'lodash'

async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  )
  let finalStatus = existingStatus

  // console.info('existingStatus: ', existingStatus)

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
    finalStatus = status
  }

  // console.info('finalStatus: ', finalStatus)

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return
  }

  // Get the token that uniquely identifies this device
  // calling Notifications.getExpoPushToken() will throw an error if you don’t have permission to send notifications.
  // We recommend call Permissions.getAsync(Permissions.NOTIFICATIONS) and,
  // if needed and you haven’t asked before, Permissions.askAsync(Permissions.NOTIFICATIONS)
  // before getting push token.
  let token = await Notifications.getExpoPushTokenAsync()

  // console.info('token: ', token)

  const user = firebase.auth().currentUser

  if (user) {
    const userId = user.uid

    const expoPushTokensSnap = await firebase
      .database()
      .ref(`/userOwned/tokens/${userId}/expoPushTokens`)
      .once('value')

    if (expoPushTokensSnap.exists()) {
      const expoPushTokens = expoPushTokensSnap.val()

      const exists = values(expoPushTokens).find(t => t === token)

      if (!exists) {
        saveToken(token, userId)
      }
    } else {
      saveToken(token, userId)
    }
  }
}

async function saveToken(token, userId) {
  const tokenRef = firebase
    .database()
    .ref(`/userOwned/tokens/${userId}/expoPushTokens`)
    .push()

  try {
    await tokenRef.set(token)
  } catch (err) {
    console.log('err', err)
  }
}

export default registerForPushNotificationsAsync
