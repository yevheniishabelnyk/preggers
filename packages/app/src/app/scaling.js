import { Dimensions, StyleSheet } from 'react-native'
import { pick } from 'lodash'

const { width, height } = Dimensions.get('window')

//Guideline sizes are based on iPhone 6 screen
const guidelineBaseWidth = 375
const guidelineBaseHeight = 667

const defaultScaleFactor = width < guidelineBaseWidth ? 1 : 0.5

export const scale = size => (width / guidelineBaseWidth) * size

export const verticalScale = size => (height / guidelineBaseHeight) * size

export const moderateScale = (size, factor = defaultScaleFactor) =>
  size + (scale(size) - size) * factor

export const getImageByDeviceType = imageSet => {
  if (width < 768) {
    return imageSet.phone
  }

  return imageSet.tablet
}

export function createScalableStyles(inputStyles) {
  const styles = StyleSheet.create(inputStyles)
  const scalableStyles = {}
  const scalableStylesList = []
  let scale = 1

  return Object.keys(inputStyles).reduce(
    (out, styleName) => {
      const scalableStyle = getScalableStyle(inputStyles[styleName])

      if (scalableStyle) {
        scalableStylesList.push(styleName)

        scalableStyles[styleName] = scalableStyle
      }

      out[styleName] = styles[styleName]

      return out
    },
    {
      set scale(value) {
        if (scale !== value && typeof value === 'number') {
          scalableStylesList.forEach(styleName => {
            this[styleName] = scalableStyles[styleName]
              ? [
                  styles[styleName],
                  scaleStyle(scalableStyles[styleName], value),
                ]
              : styles[styleName]
          })

          scale = value
        }
      },

      get scale() {
        return scale
      },
    }
  )
}

const scalableProperties = [
  'width',
  'height',
  'borderRadius',
  'borderWidth',
  'borderTopWidth',
  'borderBottomWidth',
  'borderLeftWidth',
  'borderRightWidth',
  'fontSize',
  'padding',
  'marginTop',
  'marginLeft',
  'marginBottom',
  'marginRight',
  'marginVertical',
  'marginHorizontal',
  'paddingTop',
  'paddingBottom',
  'paddingLeft',
  'paddingRight',
  'paddingVertical',
  'paddingHorizontal',
  'padding',
  'lineHeight',
  'top',
  'left',
  'right',
  'bottom',
]

function getScalableStyle(style) {
  const propsToScale = Object.keys(style).filter(
    prop =>
      scalableProperties.includes(prop) &&
      style[prop] !== 0 &&
      typeof style[prop] === 'number'
  )

  if (propsToScale.length) {
    return pick(style, propsToScale)
  }
}

function scaleStyle(style, scale) {
  return Object.keys(style).reduce((out, key) => {
    out[key] = style[key] * scale

    return out
  }, {})
}

export function scalableStyles(styles) {
  return function(Component) {
    return class extends Component {
      styles = createScalableStyles(styles)
    }
  }
}
