/**
 *
 * Auth actions
 *
 */

import * as firebase from 'firebase'
import { Facebook, Constants } from 'expo'

import { FACEBOOK__APP_ID } from 'config'
import { updateUserWithFacebook } from '@/app/database/actions/users'
import * as TempActions from '@/app/redux/temp'
import * as SessionActions from '@/app/redux/session'

import { PERMISSIONS as FACEBOOK_PERMISSIONS } from '@/app/constants/Facebook'

export const authenticate = ({
  providerId,
  email,
  password,
  linkCredential,
} = {}) => async dispatch => {
  console.info('[AUTH] Start')
  console.info('[AUTH] providerId: ', providerId)
  email && console.info('[AUTH] email: ', email)
  password && console.info('[AUTH] password: ', password)
  linkCredential && console.info('[AUTH] linkCredential: ', linkCredential)

  let user, isNewUser, facebookAccessToken, facebookResponseType

  switch (providerId) {
    case 'facebook.com': {
      const options = {
        permissions: FACEBOOK_PERMISSIONS,
      }

      if (Constants.appOwnership === 'standalone') {
        options.behavior = 'native'
      }

      console.info('[AUTH] Starting Facebook login...')
      ;({
        type: facebookResponseType,
        token: facebookAccessToken,
      } = await Facebook.logInWithReadPermissionsAsync(
        FACEBOOK__APP_ID,
        options
      ))

      if (facebookResponseType === 'success') {
        console.info('[AUTH] Facebook login succeeded')

        dispatch(SessionActions.update({ signInProvider: 'facebook.com' }))
        dispatch(TempActions.set({ facebookAccessToken }))

        try {
          console.info(
            '[AUTH] Starting firebase authentication with Facebook...'
          )
          ;({ user, isNewUser } = await firebaseAuthentication({
            providerId,
            token: facebookAccessToken,
          }))

          dispatch(SessionActions.update({ isNewUser }))
        } catch (e) {
          console.info(
            '[AUTH] Error: firebase authentication with facebook failed!\n',
            e
          )

          dispatch(SessionActions.update({ signInProvider: undefined }))
          dispatch(TempActions.remove('facebookAccessToken'))

          return Promise.reject(e)
        }
      } else {
        if (facebookResponseType !== 'cancel') {
          console.info(
            '[AUTH] Error: facebook login failed!',
            facebookResponseType
          )

          return Promise.reject(
            'Error: facebook login failed!' + facebookResponseType
          )
        }
        console.info('[AUTH] User canceled authentication.')

        return Promise.reject({})
      }

      break
    }

    case 'password': {
      try {
        console.info(
          '[AUTH] Starting firebase authentication with email and password...'
        )

        dispatch(SessionActions.update({ signInProvider: 'password' }))
        ;({ user, isNewUser } = await firebaseAuthentication({
          providerId,
          email,
          password,
        }))

        dispatch(SessionActions.update({ isNewUser }))
      } catch (e) {
        console.info(
          '[AUTH] Error: firebase authentication with email and password failed!\n',
          e
        )

        dispatch(SessionActions.update({ signInProvider: undefined }))

        if (e.code === 'auth/user-not-found') {
          try {
            console.info('[AUTH] Creating user with email and password...')

            isNewUser = true

            dispatch(
              SessionActions.update({ signInProvider: 'password', isNewUser })
            )

            user = await firebase
              .auth()
              .createUserWithEmailAndPassword(email, password)
          } catch (e) {
            console.info(
              '[AUTH] Error: creating user with email and password faild!\n',
              e
            )

            dispatch(SessionActions.update({ signInProvider: undefined }))

            return Promise.reject(e)
          }
        } else if (e.code === 'auth/account-exists-with-different-credential') {
          const credential = firebase.auth.EmailAuthProvider.credential(
            email,
            password
          )
          e.credential = credential
          return Promise.reject(e)
        } else {
          return Promise.reject(e)
        }
      }

      break
    }
  }

  if (user) {
    if (linkCredential) {
      console.info(
        '[AUTH] Linking user with another credential...\n',
        linkCredential
      )

      user
        .linkWithCredential(linkCredential)
        .then(() => {
          console.log('[AUTH] Profile was linked with new auth provider')
          if (
            linkCredential.providerId === 'facebook.com' &&
            linkCredential.accessToken
          ) {
            dispatch(updateUserWithFacebook(linkCredential.accessToken))
          }
        })
        .catch(err =>
          console.log(
            '[AUTH] Error: linking user with another credential faild\n',
            err
          )
        )
    }

    return Promise.resolve({ facebookAccessToken })
  }
}

export const firebaseAuthentication = async ({
  providerId,
  token,
  email,
  password,
}) => {
  let provider, credential

  if (providerId === 'facebook.com' && token) {
    provider = firebase.auth.FacebookAuthProvider
    credential = provider.credential(token)
  } else if (providerId === 'password' && email && password) {
    provider = firebase.auth.EmailAuthProvider
    credential = provider.credential(email, password)
  }

  if (provider && credential) {
    try {
      const response = await firebase
        .auth()
        .signInAndRetrieveDataWithCredential(credential)

      return Promise.resolve({
        isNewUser: response.additionalUserInfo.isNewUser,
        user: response.user,
      })
    } catch (err) {
      console.info('err: ', err)

      if (err.code === 'auth/wrong-password') {
        const authProviders = await firebase
          .auth()
          .fetchProvidersForEmail(email)

        if (authProviders.indexOf('password') !== -1) {
          return Promise.reject(err)
        }

        if (authProviders.length) {
          return Promise.reject({
            code: 'auth/account-exists-with-different-credential',
          })
        }
      }

      return Promise.reject(err)
    }
  } else {
    return Promise.reject()
  }
}
