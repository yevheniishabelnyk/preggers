/**
 *
 * Device reducer
 *
 */

const DEVICE__UPDATE = 'DEVICE__UPDATE'
import { Constants } from 'expo'

const initialState = {
  locale: null,
  country: null,
  timezone: null,
  isConnected: undefined,
  isDevice: Constants.isDevice,
}

export default function deviceReducer(state = initialState, action) {
  switch (action.type) {
    case DEVICE__UPDATE:
      return Object.assign({}, state, action.payload)

    default:
      return state
  }
}

export const update = data => ({
  type: DEVICE__UPDATE,
  payload: data,
})
