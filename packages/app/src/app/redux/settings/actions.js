/**
 *
 * Settings actions
 *
 */

import config from 'config'
import { omit } from 'lodash'
import { ActionType } from './types'
import { AppLocale } from '@/app/types'
import Settings from './state'

export const updateLocal = payload => async (
  dispatch,
  getState,
  getFirebase
) => {
  if (payload.locale && config.APP_LOCALES.indexOf(payload.locale) === -1) {
    payload.locale = config.DEFAULT_LOCALE
  }

  dispatch({
    type: ActionType.SETTINGS__UPDATE,
    payload,
  })

  if (payload.locale) {
    const firebase = getFirebase()
    const profile = getState().firebase.profile

    if (profile.appLocale !== payload.locale) {
      firebase.updateProfile({ appLocale: payload.locale })
    }
  }
}

export const syncLocalRemote = () => async (
  dispatch,
  getState,
  getFirebase
) => {
  const {
    settings,
    firebase: { profile },
  } = getState()
  const firebase = getFirebase()

  try {
    await firebase.set(
      `userOwned/settings/${profile.id}`,
      omit(settings, '_persist')
    )
    await firebase.updateProfile({ appLocale: settings.locale })
  } catch (err) {
    console.info('err: ', err)
  }

  return Promise.resolve()
}

export const syncRemoteLocal = () => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const {
    firebase: { auth },
  } = getState()

  try {
    const response = await firebase.watchEvent(
      'once',
      `userOwned/settings/${auth.uid}`
    )

    if (response && response.data) {
      if (!Object.values(AppLocale).includes(response.data.locale)) {
        response.data.locale = config.DEFAULT_LOCALE
      }

      const state = new Settings(response.data)

      dispatch(updateLocal(state))
    }

    firebase.unWatchEvent('once', `userOwned/settings/${auth.uid}`)
  } catch (err) {
    console.info('err: ', err)
  }

  return Promise.resolve()
}
