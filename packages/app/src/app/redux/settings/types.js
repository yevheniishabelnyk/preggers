export const ActionType = {
  SETTINGS__UPDATE: 'SETTINGS__UPDATE',
  LOGOUT: '@@reactReduxFirebase/LOGOUT',
}
