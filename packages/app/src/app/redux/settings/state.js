import { AppLocale, LengthUnit, WeightUnit } from '@/app/types'

export default class Settings {
  locale = AppLocale.English
  lengthUnits = LengthUnit.Centimetre
  weightUnits = WeightUnit.Gramm
  isWeeklyEmailsEnabled = true

  constructor(fields) {
    if (fields) {
      Object.assign(this, fields)
    }
  }
}
