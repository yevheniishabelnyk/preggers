/**
 *
 * Settings persisted reducer
 *
 */

import config from 'config'

import { DangerZone } from 'expo'
const { Localization } = DangerZone

import settingsReducer from './reducer'
import Settings from './state'

import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'

const persistConfig = {
  key: 'settings',
  storage: storage,
  stateReconciler: hardSet,
  migrate: state => {
    if (
      !state ||
      !state.locale ||
      config.APP_LOCALES.indexOf(state.locale) === -1
    ) {
      return Localization.getCurrentLocaleAsync()
        .then(deviceLocale => {
          console.log(
            '\n[SETTINGS PERSISTED REDUCER] Device locale: ',
            deviceLocale
          )

          const locale = deviceLocale.substring(0, 2)

          let initialState

          if (config.APP_LOCALES.indexOf(locale) !== -1) {
            initialState = new Settings({ locale })
          } else {
            initialState = new Settings()
          }

          return initialState
        })
        .catch(err => {
          console.log(
            '\n[SETTINGS PERSISTED REDUCER] Get device locale error: ',
            err
          )

          return new Settings()
        })
    }

    return Promise.resolve(state)
  },
}

export default persistReducer(persistConfig, settingsReducer)
