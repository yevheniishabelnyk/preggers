/**
 *
 * Settings reducer
 *
 */

import { ActionType } from './types'
import Settings from './state'

const initialState = new Settings()

export default function settingsReducer(state = initialState, action) {
  switch (action.type) {
    case ActionType.SETTINGS__UPDATE:
      return Object.assign({}, state, action.payload)

    case ActionType.LOGOUT:
      return Object.assign({}, initialState, { locale: state.locale })

    default:
      return state
  }
}
