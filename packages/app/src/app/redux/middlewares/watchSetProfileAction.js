/**
 *
 * Watch SET_PROFILE action middleware
 *
 */

import { CacheManager } from '@/vendor/cache-manager'
import { actionTypes } from 'react-redux-firebase'

import * as firebase from 'firebase'

const watchSetProfileAction = api => next => action => {
  if (action.type === actionTypes.SET_PROFILE) {
    const isNewUser = api.getState().session.isNewUser

    if (!isNewUser && action.profile === null) {
      const user = firebase.auth().currentUser

      if (user) {
        CacheManager.clearUserCache(user.uid)

        user
          .delete()
          .then(function() {
            console.log('[APP] User has been removed from Firebase Auth')
          })
          .catch(function(err) {
            console.info('err: ', err)
            console.log('[APP] Faild to remove user from Firebase Auth')

            api.dispatch({ type: actionTypes.LOGOUT })
          })
      }
    }
  }

  return next(action)
}

export default watchSetProfileAction
