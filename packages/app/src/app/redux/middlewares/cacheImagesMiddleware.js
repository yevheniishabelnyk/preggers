/**
 *
 * Cache images middleware
 *
 */

import { CacheManager } from '@/vendor/cache-manager'
import * as DatabaseActions from '@/app/database/actions'
import { actionTypes } from 'react-redux-firebase'

const cacheImages = api => next => action => {
  let skipAction

  if (!action.cached) {
    const { uid } = api.getState().firebase.auth

    switch (action.type) {
      case actionTypes.SET_PROFILE:
        if (
          action.profile &&
          action.profile.image &&
          !action.profile.image.uri.startsWith('file')
        ) {
          skipAction = true

          // console.info('SET_PROFILE action: ', JSON.stringify(action, null, 4))

          CacheManager.cache({
            remoteUri: action.profile.image.uri,
            localDirectory: `users/${uid}/images`,
            listener: (localUri: string) => {
              // console.info('SET_PROFILE localUri: ', localUri)

              action.profile.image = {
                ...action.profile.image,
                uri: localUri,
                origin: action.profile.image.uri,
              }

              action.cached = true

              api.dispatch(action)
            },
          }).catch(err => {
            console.log('err', err)

            api.dispatch(action)
          })
        }
        break

      case actionTypes.PROFILE_UPDATE_SUCCESS:
        if (
          action.payload &&
          action.payload.image &&
          !action.payload.image.uri.startsWith('file')
        ) {
          skipAction = true

          // console.info(
          //   'PROFILE_UPDATE_SUCCESS action: ',
          //   JSON.stringify(action, null, 4)
          // )

          CacheManager.cache({
            remoteUri: action.payload.image.uri,
            localDirectory: `users/${uid}/images`,
            listener: (localUri: string) => {
              // console.info('SET_PROFILE localUri: ', localUri)

              action.payload.image = {
                ...action.payload.image,
                uri: localUri,
                origin: action.payload.image.uri,
              }

              action.cached = true

              api.dispatch(action)
            },
          }).catch(err => {
            console.log('err', err)

            api.dispatch(action)
          })
        }
        break

      case actionTypes.SET:
        if (
          /^(children)\/.+$/.test(action.path) &&
          action.data &&
          action.data.image &&
          !action.data.image.uri.startsWith('file')
        ) {
          // console.info('SET action: ', JSON.stringify(action, null, 4))

          CacheManager.cache({
            remoteUri: action.data.image.uri,
            localDirectory: `users/${uid}/images`,
            listener: (localUri: string) => {
              // console.info('SET localUri: ', localUri)

              api.dispatch(
                DatabaseActions.updateLocalState({
                  data: {
                    ...action.data.image,
                    uri: localUri,
                    origin: action.data.image.uri,
                  },
                  path: `${action.path}/image`,
                  cached: true,
                })
              )
            },
          }).catch(err => {
            console.log('err', err)
          })
        }

        break
    }
  }

  if (!skipAction) {
    return next(action)
  }
}

export default cacheImages
