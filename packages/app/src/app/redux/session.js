/**
 *
 * Session reducer
 *
 */

import jwtDecode from 'jwt-decode'
import { Api } from '@/app/api/Preggers/Api'
import { actionTypes } from 'react-redux-firebase'
import { Constants } from 'expo'

const SESSION__ADD = 'SESSION__ADD'
const SESSION__REMOVE = 'SESSION__REMOVE'
const SESSION__UPDATE = 'SESSION__UPDATE'
const LOGOUT = actionTypes.LOGOUT

const initialState = {
  expoSessionId: Constants.sessionId,
  expoVersion: Constants.expoVersion,
  expoAppOwnership: Constants.appOwnership,
}

export default function sessionReducer(state = initialState, action) {
  switch (action.type) {
    case SESSION__ADD:
      return Object.assign({}, state, action.session)

    case SESSION__UPDATE:
      return Object.assign({}, state, action.payload)

    case LOGOUT:
    case SESSION__REMOVE:
      return initialState

    default:
      return state
  }
}

export const update = payload => ({
  type: SESSION__UPDATE,
  payload,
})

export const add = jwtToken => {
  let session

  try {
    session = jwtDecode(jwtToken)
  } catch (err) {
    throw new Error('Error: faild decode ID token!')
  }

  // console.info('session: ', session)

  Api.setAccessToken(jwtToken)

  return {
    type: SESSION__ADD,
    session: {
      // authTime: session.auth_time,
      signInProvider: session.firebase.sign_in_provider,
    },
  }
}

export const remove = () => {
  Api.unsetAccessToken()

  return {
    type: SESSION__REMOVE,
  }
}
