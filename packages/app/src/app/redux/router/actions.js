/**
 *
 * Router actions
 *
 */

import { NavigationActions } from 'react-navigation'
import { Linking } from 'expo'

import URL from 'url'
import { isEmpty } from 'lodash'

import routerActionTypes from './actionTypes'
import * as DeepLinks from '@/app/constants/DeepLinks'

export function goToUrl(url) {
  const routes = url.match(/\/[^/]+/g)

  if (routes) {
    if (routes.length === 1) {
      return goToSimpleUrl(routes[0])
    }

    if (routes.length > 1) {
      return goToComplexUrl(url, routes)
    }
  }

  return {}
}

export const goToSimpleUrl = url => {
  const urlObj = URL.parse(url, true)

  return NavigationActions.navigate({
    routeName: urlObj.pathname,
    params: !isEmpty(urlObj.query) ? urlObj.query : undefined,
  })
}

export function goToComplexUrl(url, routes = url.match(/\/[^/]+/g)) {
  if (routes) {
    return routes.reduceRight((nestedAction, routeUrl) => {
      const action = goToSimpleUrl(routeUrl)

      if (nestedAction !== null) {
        action.action = nestedAction
      }

      return action
    }, null)
  }

  return {}
}

export function resetToUrl(urlList, index = urlList.length - 1) {
  return NavigationActions.reset({
    index,
    actions: urlList.map(url => goToUrl(url)),
  })
}

export const resetDeepLinkFlag = () => ({
  type: routerActionTypes.ROUTER__RESET_DEEP_LINK_FLAG,
})

export const setDeepLinkFlag = () => ({
  type: routerActionTypes.ROUTER__SET_DEEP_LINK_FLAG,
})

export const handleLink = link => dispatch => {
  if (link.startsWith('preggersapp://')) {
    dispatch(handleDeepLink(link))
  } else {
    Linking.openURL(link)
  }
}

export const handleNestedDeepLinks = (dispatch, link, routeName) => {
  const isNestedNavigationWithParams = routeName.includes('~knowledge')

  if (isNestedNavigationWithParams) {
    const linkWithParams = __DEV__
      ? link.replace('exp://localhost:19000/--/', '')
      : link.replace('preggersapp://', '')

    dispatch(resetToUrl([`/${linkWithParams}`]))

    return
  }

  dispatch(resetToUrl(['/~home']))

  dispatch(goToComplexUrl(routeName))

  return
}

export const handleDeepLink = link => (dispatch, getState) => {
  if (!link) {
    return null
  }

  const data = Linking.parse(link)

  if (!data) {
    return null
  }

  const { path, queryParams = {} } = data

  const routeName = `/${path}`

  const isNestedNavigation = path.includes('/')

  const auth = getState().firebase.auth

  if (
    isNestedNavigation &&
    DeepLinks.PRIVATE_ROUTES.includes(routeName) &&
    !auth.isEmpty
  ) {
    return handleNestedDeepLinks(dispatch, link, routeName)
  }

  if (DeepLinks.PRIVATE_ROUTES.includes(routeName)) {
    if (!auth.isEmpty) {
      dispatch(
        push({
          routeName,
          params: {
            isOpenedWithDeepLink: true,
            ...queryParams,
          },
          isOpenedWithDeepLink: true,
        })
      )
    }
  } else if (DeepLinks.PUBLIC_ROUTES.includes(routeName)) {
    dispatch(
      push({
        routeName,
        params: {
          isOpenedWithDeepLink: true,
          ...queryParams,
        },
        isOpenedWithDeepLink: true,
      })
    )
  }
}

export const push = ({ routeName, params, isOpenedWithDeepLink }) => (
  dispatch,
  getState
) => {
  const router = getState().router

  let currentRoute

  if (router && router.index != null && router.routes) {
    currentRoute = router.routes[router.index]
  }

  if (currentRoute) {
    if (currentRoute.routeName !== routeName) {
      dispatch({
        type: routerActionTypes.ROUTER__PUSH,
        isCurrentRouteOpenedWithDeepLink: isOpenedWithDeepLink,
        route: {
          routeName,
          params,
          key: `id-${Date.now()}-0`,
        },
      })
    } else {
      if (
        currentRoute.routeName === '/knowledge-article' &&
        currentRoute.params.articleId !== params.articleId
      ) {
        dispatch({
          type: NavigationActions.PUSH,
          routeName,
          params,
        })
      }

      dispatch(setDeepLinkFlag())
    }
  }
}

export const validatePersistedState = () => (dispatch, getState) => {
  const {
    router,
    firebase: { auth },
  } = getState()

  const startRoute = router.routes[router.index]

  if (startRoute && startRoute.routeName) {
    const routeName = startRoute.routeName

    if (auth.isEmpty) {
      if (
        [
          '/tools-menu',
          '/food-search',
          '/knowledge-search',
          '/knowledge-article',
          '/baby-names-country',
          '/baby-names-menu',
        ].indexOf(routeName) !== -1
      ) {
        return
      }

      if (!routeName.startsWith('/auth')) {
        dispatch(resetToNonAuthenitcatedInitialRoute())
      }
    } else if (router.routes[0].routeName !== '/~home') {
      dispatch(resetToAuthenitcatedInitialRoute())
    }
  } else {
    dispatch(resetToNonAuthenitcatedInitialRoute())
  }
}

export const resetToNonAuthenitcatedInitialRoute = () => ({
  type: routerActionTypes.ROUTER__NON_AUTHENTICATED_INITIAL_STATE,
})

export const resetToAuthenitcatedInitialRoute = () => ({
  type: routerActionTypes.ROUTER__AUTHENTICATED_INITIAL_STATE,
})

export const resetToErrorScreen = () => ({
  type: routerActionTypes.ROUTER__ERROR_STATE,
})
