/**
 *
 * Router reducer
 *
 */

import { NavigationActions } from 'react-navigation'

import routerActionTypes from './actionTypes'

const ACTION_TYPES = [
  NavigationActions.BACK,
  NavigationActions.INIT,
  NavigationActions.NAVIGATE,
  NavigationActions.POP,
  NavigationActions.POP_TO_TOP,
  NavigationActions.PUSH,
  NavigationActions.RESET,
  NavigationActions.REPLACE,
  NavigationActions.SET_PARAMS,
  NavigationActions.URI,
  NavigationActions.COMPLETE_TRANSITION,
]

import {
  nonAuthenticatedInitialRouteName,
  authenticatedInitialRouteName,
  errorRouteName,
} from '@/app/routes'

import AppNavigator from '@/app/navigator'

export const nonAuthenticatedInitialState = AppNavigator.router.getStateForAction(
  AppNavigator.router.getActionForPathAndParams(
    nonAuthenticatedInitialRouteName
  )
)

export const authenticatedInitialState = AppNavigator.router.getStateForAction(
  NavigationActions.navigate({
    routeName: authenticatedInitialRouteName,
  })
)

export const errorState = AppNavigator.router.getStateForAction(
  NavigationActions.navigate({ routeName: errorRouteName })
)

// router reducer
export default function routerReducer(
  state = nonAuthenticatedInitialState,
  action
) {
  switch (action.type) {
    case routerActionTypes.ROUTER__RESET_DEEP_LINK_FLAG:
      return {
        ...state,
        isCurrentRouteOpenedWithDeepLink: false,
      }

    case routerActionTypes.ROUTER__SET_DEEP_LINK_FLAG:
      return {
        ...state,
        isCurrentRouteOpenedWithDeepLink: true,
      }

    case routerActionTypes.ROUTER__AUTHENTICATED_INITIAL_STATE:
      return authenticatedInitialState

    case routerActionTypes.ROUTER__NON_AUTHENTICATED_INITIAL_STATE:
      return nonAuthenticatedInitialState

    case routerActionTypes.ROUTER__ERROR_STATE:
      return errorState

    case routerActionTypes.ROUTER__PUSH: {
      return {
        ...state,
        index: state.index + 1,
        isCurrentRouteOpenedWithDeepLink:
          action.isCurrentRouteOpenedWithDeepLink,
        routes: [...state.routes, action.route],
      }
    }

    case NavigationActions.NAVIGATE:
      if (
        action.routeName === state.routes[state.routes.length - 1].routeName
      ) {
        return state
      }
  }

  if (ACTION_TYPES.indexOf(action.type) !== -1) {
    if (
      action.type === NavigationActions.NAVIGATE ||
      action.type === NavigationActions.BACK ||
      action.type === NavigationActions.PUSH
    ) {
      delete state.isCurrentRouteOpenedWithDeepLink
    }

    return AppNavigator.router.getStateForAction(action, state)
  }

  return state
}
