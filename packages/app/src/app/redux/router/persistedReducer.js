/**
 *
 * Router persisted reducer
 *
 */

import routerReducer, {
  nonAuthenticatedInitialState,
  authenticatedInitialState,
} from './reducer'

import { persistReducer } from 'redux-persist'
import { DEFAULT_VERSION } from 'redux-persist/lib/constants'
import { AsyncStorage } from 'react-native'
import storage from 'redux-persist/lib/storage'
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import { NAVIGATION__PERSISTENCE_MAX_TIME } from 'config'

const migrations = {
  0: state => {
    const initialRoute = state.routes[0]

    if (initialRoute.routeName === '/~home') {
      const lastTab = initialRoute.routes[initialRoute.routes.length - 1]

      if (lastTab.routeName !== '/deal-section') {
        return {
          ...state,
          routes: [
            {
              ...state.routes[0],
              routes: state.routes[0].routes.concat({
                key: '/deal-section',
                routeName: '/deal-section',
              }),
            },
          ].concat(state.routes.slice(1)),
        }
      }
    }

    return state
  },
}

const persistConfig = {
  key: 'router',
  version: 0,
  storage: storage,
  stateReconciler: hardSet,
  migrate: async (state, currentVersion) => {
    if (
      !state ||
      !state.routes ||
      (state && state.routes && !state.routes[state.index])
    ) {
      return Promise.resolve(nonAuthenticatedInitialState)
    }

    const isUserAuthenticated = state.routes[0].routeName === '/~home'

    if (isUserAuthenticated) {
      try {
        const lastActiveTime = parseInt(
          await AsyncStorage.getItem('lastActiveTime')
        )

        if (
          lastActiveTime &&
          Date.now() - lastActiveTime > NAVIGATION__PERSISTENCE_MAX_TIME
        ) {
          const activeRoute = state.routes[state.index]

          let shouldReset

          if (activeRoute.routeName !== '/~home') {
            shouldReset = true
          } else {
            const homeActiveRoute = activeRoute.routes[activeRoute.index]

            if (homeActiveRoute !== '/~start-view') {
              shouldReset = true
            }
          }

          if (shouldReset) {
            return Promise.resolve(authenticatedInitialState)
          }
        }
      } catch (err) {
        return Promise.reject(err)
      }
    }

    const inboundVersion =
      state._persist && state._persist.version !== undefined
        ? state._persist.version
        : DEFAULT_VERSION

    if (currentVersion > inboundVersion) {
      const migrationKeys = Object.keys(migrations)
        .map(ver => parseInt(ver))
        .filter(key => currentVersion >= key && key > inboundVersion)
        .sort((a, b) => a - b)

      try {
        const migratedState = migrationKeys.reduce((state, versionKey) => {
          return migrations[versionKey](state)
        }, state)

        return Promise.resolve(migratedState)
      } catch (err) {
        return Promise.reject(err)
      }
    }

    return Promise.resolve(state)
  },
}

export default persistReducer(persistConfig, routerReducer)
