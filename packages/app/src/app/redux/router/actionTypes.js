/**
 *
 * Router action types
 *
 */

const actionTypes = {
  ROUTER__AUTHENTICATED_INITIAL_STATE: 'ROUTER__AUTHENTICATED_INITIAL_STATE',
  ROUTER__NON_AUTHENTICATED_INITIAL_STATE:
    'ROUTER__NON_AUTHENTICATED_INITIAL_STATE',
  ROUTER__ERROR_STATE: 'ROUTER__ERROR_STATE',
  ROUTER__PUSH: 'ROUTER__PUSH',
  ROUTER__RESET_DEEP_LINK_FLAG: 'ROUTER__RESET_DEEP_LINK_FLAG',
  ROUTER__SET_DEEP_LINK_FLAG: 'ROUTER__SET_DEEP_LINK_FLAG',
}

export default actionTypes
