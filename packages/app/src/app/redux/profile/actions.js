/**
 *
 * Profile actions
 *
 */
import Expo, { StoreReview } from 'expo'
import { actionTypes } from 'react-redux-firebase'
import { Platform } from 'react-native'

import * as FirebaseStorage from '@/app/firebase/storage'
import { CacheManager } from '@/vendor/cache-manager'

import { getRatingLastPromptedAt } from '@/app/redux/selectors'

import cropImage from '@/shared/utils/cropImage'
import getUTCOffset from '@/shared/utils/getUTCOffset'

export const maybeAskUserToRateApp = () => async (
  dispatch,
  getState,
  getFirebase
) => {
  if (Platform.OS === 'ios' && StoreReview.isSupported()) {
    const { profile } = getState().firebase

    if (profile.isEmpty) {
      return
    }

    const userAge = Date.now() - profile.createdAt

    const fourteenDaysInMilleseconds = 1209600000

    if (userAge > fourteenDaysInMilleseconds) {
      const firebase = getFirebase()

      if (
        !getState().firebase.requested[
          `userOwned/ratingLastPromptedAt/${profile.id}`
        ]
      ) {
        try {
          await firebase.watchEvent(
            'once',
            `userOwned/ratingLastPromptedAt/${profile.id}`
          )
        } catch (err) {
          console.log('Error: ', err)

          return
        }
      }

      const ratingLastPromptedAt = getRatingLastPromptedAt(
        getState(),
        profile.id
      )

      const thirtyDaysInMilleseconds = 2592000000

      if (
        !ratingLastPromptedAt ||
        Date.now() - ratingLastPromptedAt >= thirtyDaysInMilleseconds
      ) {
        try {
          const now = Date.now()

          await StoreReview.requestReview()

          await firebase.set(
            `userOwned/ratingLastPromptedAt/${profile.id}`,
            now
          )

          dispatch({
            type: actionTypes.SET,
            path: `userOwned/ratingLastPromptedAt/${profile.id}`,
            data: now,
            timestamp: Date.now(),
            requesting: false,
            requested: true,
          })
        } catch (err) {
          console.log('Error: ', err)
        }
      }
    }
  }
}

export const updateLocalState = (data, cached = false) => ({
  type: actionTypes.PROFILE_UPDATE_SUCCESS,
  payload: data,
  cached,
})

export const updateProfilePicture = localImage => async (
  dispatch,
  getState,
  getFirebase
) => {
  const { profile } = getState().firebase
  const firebase = getFirebase()
  const oldImageUri = profile.image ? profile.image.origin : null

  dispatch(
    updateLocalState(
      {
        image: {
          type: 'file',
          uri: localImage.uri,
        },
      },
      true
    )
  )

  const [remoteUri, previewResult] = await Promise.all([
    FirebaseStorage.putFile(
      firebase,
      localImage.uri,
      `users/${profile.id}/images`
    ),

    cropImage(localImage, {
      type: 'square',
      width: 10,
      base64: true,
    }),
  ])

  await firebase.updateProfile({
    image: {
      type: 'file',
      uri: remoteUri,
      preview: `data:image/jpeg;base64,${previewResult.base64}`,
    },
  })

  await dispatch(updateFirebaseProfilePhotoUrl(remoteUri))

  if (oldImageUri) {
    FirebaseStorage.deleteFile(firebase, oldImageUri)

    CacheManager.delete(oldImageUri, `users/${profile.id}/images`)
  }
}

export const updateFirebaseProfilePhotoUrl = uri => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()

  await firebase.auth().currentUser.updateProfile({
    photoURL: uri,
  })

  dispatch({
    type: actionTypes.AUTH_UPDATE_SUCCESS,
    auth: {
      photoURL: uri,
    },
    preserve: {
      auth: true,
    },
  })
}

export const updateFirebaseProfileEmail = email => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()

  await firebase.auth().currentUser.updateEmail(email)

  dispatch({
    type: actionTypes.AUTH_UPDATE_SUCCESS,
    auth: {
      email,
    },
    preserve: {
      auth: true,
    },
  })
}

export const updateFirebaseProfileDisplayName = name => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()

  await firebase.auth().currentUser.updateProfile({
    displayName: name,
  })

  dispatch({
    type: actionTypes.AUTH_UPDATE_SUCCESS,
    auth: {
      displayName: name,
    },
    preserve: {
      auth: true,
    },
  })
}

export const syncProfileDevice = () => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()

  const device =
    Platform.OS === 'ios' ? Expo.Constants.platform.ios.model : Platform.OS

  try {
    await firebase.updateProfile({ device })
  } catch (err) {
    console.log('err', err)
  }
}

export const syncProfileUTCOffset = () => async (
  dispatch,
  getState,
  getFirebase
) => {
  const profile = getState().firebase.profile
  const auth = getState().firebase.auth
  const currentUTCOffset = getUTCOffset()
  const profileUTCOffset = profile.UTCOffset

  if (!auth.isEmpty && profileUTCOffset !== currentUTCOffset) {
    const firebase = getFirebase()

    try {
      await Promise.all([
        firebase.updateProfile({ UTCOffset: currentUTCOffset }),

        firebase.set(
          `usersByUTCOffset/${profileUTCOffset}/${profile.id}`,
          null
        ),

        firebase.set(
          `usersByUTCOffset/${currentUTCOffset}/${profile.id}`,
          true
        ),
      ])
    } catch (err) {
      console.log('err', err)
    }
  }
}
