/**
 *
 * Temp data logic
 *
 */

import updateState from 'immutability-helper'
import { omit } from 'lodash'
import { actionTypes } from 'react-redux-firebase'

// Constants
const TEMP__SET = 'TEMP__SET'
const TEMP__INIT_COLLECTION = 'TEMP__INIT_COLLECTION'
const TEMP__UPDATE_COLLECTION = 'TEMP__UPDATE_COLLECTION'
const TEMP__RESET_COLLECTION = 'TEMP__RESET_COLLECTION'
const TEMP__REMOVE_COLLECTION = 'TEMP__REMOVE_COLLECTION'
const TEMP__CLEAR = 'TEMP__CLEAR'
const LOGOUT = actionTypes.LOGOUT

// Temp data reducer
export default function tempReducer(state = {}, action) {
  switch (action.type) {
    case TEMP__INIT_COLLECTION: {
      if (action.key && action.initialState) {
        return Object.assign({}, state, {
          [action.key]: action.initialState,

          initialStates: Object.assign({}, state.initialStates, {
            [action.key]: action.initialState,
          }),
        })
      }

      return state
    }

    case TEMP__UPDATE_COLLECTION: {
      if (state[action.key]) {
        return Object.assign({}, state, {
          [action.key]: updateState(state[action.key], action.diff),
        })
      }

      return state
    }

    case TEMP__RESET_COLLECTION: {
      if (state[action.key]) {
        return Object.assign({}, state, {
          [action.key]: state.initialStates[action.key],
        })
      }

      return state
    }

    case TEMP__REMOVE_COLLECTION: {
      if (state[action.key]) {
        return Object.assign(omit(state, action.key), {
          initialStates: omit(state.initialStates, action.key),
        })
      }

      return state
    }

    case TEMP__SET:
      return Object.assign({}, state, action.payload)

    case LOGOUT:
    case TEMP__CLEAR:
      return {}

    default:
      return state
  }
}

// Action creators
export const init = (key, initialState) => ({
  type: TEMP__INIT_COLLECTION,
  key,
  initialState,
})

export const update = (key, diff) => ({
  type: TEMP__UPDATE_COLLECTION,
  key,
  diff,
})

export const reset = key => ({
  type: TEMP__RESET_COLLECTION,
  key,
})

export const remove = key => ({
  type: TEMP__REMOVE_COLLECTION,
  key,
})

export const clear = () => ({
  type: TEMP__CLEAR,
})

export const set = payload => ({
  type: TEMP__SET,
  payload,
})
