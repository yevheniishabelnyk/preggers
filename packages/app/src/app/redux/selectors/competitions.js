import { get } from 'lodash'

export const getCompetitions = state =>
  get(state.firebase.data, 'competitions.competitionList')

export const getActiveCompetitionUid = state =>
  get(state.firebase.data, 'competitions.activeCompetitions')

export const getCompetition = (state, competitionName) =>
  get(state.firebase.data, `competitions.competitionList.${competitionName}`)
