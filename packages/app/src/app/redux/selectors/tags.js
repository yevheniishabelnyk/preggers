import { get, has } from 'lodash'

export const getTags = state => get(state.firebase.data, 'content.tags')

export const getTagNamesByIds = (state, ids, locale) => {
  if (!ids) {
    return
  }

  const tags = getTags(state)

  if (!tags) {
    return
  }

  const tagNames = []

  ids.forEach(tagId => {
    const tag = tags[tagId]

    if (has(tag, `name.${locale}`)) {
      tagNames.push({ id: tagId, label: tag.name[locale] })
    }
  })

  if (tagNames.length) {
    return tagNames
  }
}
