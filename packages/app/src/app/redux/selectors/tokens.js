import { get } from 'lodash'

export const getUserTokens = (state, userId, defaultValue) =>
  get(state.firebase.data, `userOwned.tokens.${userId}`) || defaultValue
