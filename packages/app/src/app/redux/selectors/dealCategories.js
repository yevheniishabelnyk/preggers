import { get } from 'lodash'
import { createSelector } from 'reselect'

const getDealCategories = (state, locale) => ({
  categories: get(state.firebase.data, 'content.dealCategories'),
  locale,
})

const setupCategoriesForFormSelect = ({ categories, locale }) => {
  if (categories) {
    return Object.values(categories).reduce((list, category) => {
      list.push({
        label: category.name[locale],
        value: String(category.id),
      })

      return list
    }, [])
  }

  return []
}

export const getCategoriesForDealAds = createSelector(
  getDealCategories,
  setupCategoriesForFormSelect
)
