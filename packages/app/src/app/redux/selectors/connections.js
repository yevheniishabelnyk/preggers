import { get } from 'lodash'

export const getConnection = (state, connectionId) =>
  get(state.firebase.data, `connections.${connectionId}`)

export const getPartnerConnections = state =>
  Object.values(get(state, 'firebase.data.connections', {})).filter(
    c => c.relationshipType === 'partner'
  )
