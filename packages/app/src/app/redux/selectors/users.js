import { get } from 'lodash'

export const getUserName = (state, userId) =>
  get(state.firebase.data, `users.${userId}.name`)

export const getUser = (state, userId) =>
  get(state.firebase.data, `users.${userId}`)

export const getRatingLastPromptedAt = (state, userId) =>
  get(state.firebase.data, `userOwned.ratingLastPromptedAt.${userId}`)
