import { get } from 'lodash'

export const getDailyTip = (state, day) =>
  get(state.firebase.data, `content.dailyTips.${day}`)
