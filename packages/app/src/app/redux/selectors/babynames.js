import { get, omitBy, transform, has } from 'lodash'

export const getUserBabynameLikes = (state, userId) =>
  get(state.firebase.data, `babynameLikesByUser.${userId}`)

export const getCountryBabynames = (state, countryCode) =>
  countryCode && has(state.firebase.data, 'content.countryBabyNames')
    ? omitBy(
        state.firebase.data.content.countryBabyNames,
        babyname => babyname.countryCode !== countryCode
      )
    : null

export const getUserFavoriteBabynames = (state, userId) => {
  const userBabynameLikes = getUserBabynameLikes(state, userId)
  const countryBabyNames = get(state.firebase.data, 'content.countryBabyNames')

  if (userBabynameLikes && countryBabyNames) {
    return transform(
      userBabynameLikes,
      (result, value, key) => {
        const babyname = countryBabyNames[key]

        if (babyname) {
          result[key] = babyname
        }
      },
      {}
    )
  }

  if (userBabynameLikes === null || countryBabyNames === null) {
    return null
  }

  return undefined
}

export const getUserOwnedBabynames = (state, userId) =>
  get(state.firebase.data, `userOwned.babyNames.${userId}`)
