import { get, compact } from 'lodash'

export const getArticles = state => get(state.firebase.data, 'content.articles')

export const getArticle = (state, articleId) =>
  get(state.firebase.data, `content.articles.${articleId}`)

export const getArticlesById = (state, ids = []) =>
  compact(
    ids.map(articleId =>
      get(state.firebase.data, `content.articles.${articleId}`)
    )
  )

export const getArticleLikesByUser = (state, userId) =>
  get(state.firebase.data, `articleLikesByUser.${userId}`)
