import { get } from 'lodash'

export const getOtherApps = state =>
  get(state.firebase.data, 'content.otherApps')

export const getOtherApp = (state, appId) =>
  get(state.firebase.data, `content.otherApps.${appId}`)
