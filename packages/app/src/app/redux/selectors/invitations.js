import { get, compact } from 'lodash'

export const getInvitation = (state, invitationId) =>
  get(state.firebase.data, `invitations.${invitationId}`)

export const getUserInvitations = (state, userId) => {
  const invitations = get(state.firebase.data, 'invitations')

  if (invitations) {
    const result = compact(Object.values(invitations)).filter(
      item => item.toId === userId
    )

    if (result.length) {
      return result
    }
  }
}

export const getUserUnreadInvitationsCount = (state, userId) => {
  const userInvitations = get(
    state.firebase.data,
    `userOwned.invitations.${userId}`
  )

  if (!userInvitations) {
    return 0
  }

  const userInvitationsStatuses = compact(
    Object.values(userInvitations)
  ).filter(status => status === 'unread')

  return userInvitationsStatuses.length
}
