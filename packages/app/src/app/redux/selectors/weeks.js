import { get } from 'lodash'

export const getWeeks = state => get(state.firebase.data, 'content.weeks')

export const getWeek = (state, weekNumber) =>
  get(state.firebase.data, `content.weeks.${weekNumber}`)

export const getWeekArticles = (state, weekNumber) =>
  get(state.firebase.data, `content.weeks.${weekNumber}`)
