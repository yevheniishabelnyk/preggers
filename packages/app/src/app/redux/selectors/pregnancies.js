import { get } from 'lodash'

export const getPregnancies = state => state.firebase.data.pregnancies

export const getPregnancy = (state, pregnancyId) =>
  get(state.firebase.data, `pregnancies.${pregnancyId}`)

export const getPregnancyPermission = (state, pregnancyId) =>
  get(
    state.firebase.data,
    `permissions.pregnancies.${pregnancyId}.${state.firebase.profile.id}`,
    null
  )

export const getUserPregnancies = (state, userId) =>
  get(state.firebase.data, `pregnanciesByUser.${userId}`)
