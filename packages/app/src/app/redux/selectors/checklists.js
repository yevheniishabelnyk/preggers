import { get, omit } from 'lodash'

export const getAdminCreatedChecklistItems = state => {
  const adminCreatedChecklists = {}
  const checklistItems =
    get(state.firebase.data, 'content.checklistItems') || {}
  const locale = get(state.settings, 'locale', 'en')
  const completed = Object.keys(
    get(
      state.firebase.data,
      `userOwned.completedAdminTodosByUser.${state.firebase.profile.id}`
    ) || {}
  )

  Object.keys(checklistItems).forEach(id => {
    const todos = {}
    const checklistItem = checklistItems[id]
    const headline = checklistItem.headline[locale]
    const description = checklistItem.description[locale]
    const { offerCollectionKey, sortPriority } = checklistItem

    Object.keys(checklistItem.todos || {}).forEach(key => {
      todos[key] = {
        completed: completed.includes(key) || null,
        text: checklistItem.todos[key][locale],
        id: key,
      }
    })

    adminCreatedChecklists[id] = {
      id,
      headline,
      description,
      todos,
      offerCollectionKey,
      sortPriority,
      isAdminCreated: true,
    }
  })

  return adminCreatedChecklists
}

export const getUserChecklistItem = (state, userId, checklistItemId) =>
  get(
    state.firebase.data,
    `userOwned.checklistItems.${userId}.${checklistItemId}`
  )

export const getUserCreatedChecklistItems = state => {
  const userCreatedChecklists = {}
  const checklistItems =
    get(
      state.firebase.data,
      `userOwned.checklistItems.${state.firebase.profile.id}`
    ) || {}

  Object.keys(checklistItems).forEach(id => {
    const todos = {}
    const checklistItem = checklistItems[id]

    if (checklistItem.todos) {
      Object.keys(checklistItem.todos).forEach(key => {
        todos[key] = {
          completed: checklistItem.todos[key],
          text: key,
          id: key,
        }
      })
    }

    userCreatedChecklists[id] = {
      ...checklistItem,
      todos,
    }
  })

  return userCreatedChecklists
}

export const getRemovedChecklistItems = state =>
  get(
    state.firebase.data,
    `checklistItemsRemovedByUser.${state.firebase.profile.id}`,
    {}
  )

export const getChecklistItemById = (state, id) => {
  const my = getUserCreatedChecklistItems(state)
  const shared = getAdminCreatedChecklistItems(state)
  const all = { ...my, ...shared }

  return all[id]
}

export const getVisibleChecklistItems = state => {
  const my = getUserCreatedChecklistItems(state)
  const shared = getAdminCreatedChecklistItems(state)
  const removed = Object.keys(getRemovedChecklistItems(state))
  const all = { ...my, ...shared }

  return omit(all, [removed])
}
