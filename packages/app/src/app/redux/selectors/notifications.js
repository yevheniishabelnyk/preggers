import { get } from 'lodash'

export const getNotifications = (state, userId) =>
  get(state.firebase.data, `userOwned.notifications.${userId}`)

export const getNotificationItem = (state, userId, notificationId) =>
  get(
    state.firebase.data,
    `userOwned.notifications.${userId}.${notificationId}`
  )

export const getLatestAppVersion = (state, platform) =>
  get(state.firebase.data, `appVersion.${platform}`)

export const getUpdateAvailableNotification = state =>
  get(state.firebase.data, `appVersion.updateAvailableNotification`)
