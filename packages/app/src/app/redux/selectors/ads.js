import {
  AD_TYPE_CHECKLIST_OFFER,
  AD_TYPE_WEEK_CARD,
  AD_TYPE_OFFER,
  AD_TYPE_DEALS,
  AD_TYPE_IMAGE_AD,
  MODERNA_PREGNANCY_INSURANCE_CAMPAIGN,
} from '@/app/redux/ads/constants'
import memoize from 'fast-memoize'

import { getAdminCreatedChecklistItems } from '@/app/redux/selectors/checklists'

import { upperFirst } from 'lodash'

import { createSelector } from 'reselect'

export const getAds = state => state.ads

export const getAdsByType = (state, type) => state.ads[type]

export const getImageAds = state =>
  Object.values(state.ads[AD_TYPE_IMAGE_AD] || {})

const imageAdsByTagsSelector = createSelector(getImageAds, ads =>
  memoize(tagIds =>
    ads.filter(ad => ad.tagIds && ad.tagIds.some(id => tagIds.includes(id)))
  )
)

export const getImageAdsByTags = (state, tagIds) =>
  imageAdsByTagsSelector(state)(tagIds)

export const getModernaFreeTodoId = state => getModernaTodoId(state, 'free')

export const getModernaLargeTodoId = state => getModernaTodoId(state, 'large')

export const getDealAds = state => state.ads && state.ads[AD_TYPE_DEALS]

export const getDealAdsList = createSelector(
  getDealAds,
  deals => deals && Object.values(deals)
)

const getModernaTodoId = (state, type) => {
  let candidates = getAdminCreatedChecklistItems(state) || {}
  candidates = Object.values(candidates)

  let modernaChecklist =
    candidates.filter(
      c => c.offerCollectionKey === MODERNA_PREGNANCY_INSURANCE_CAMPAIGN
    )[0] || false

  if (
    !modernaChecklist ||
    !modernaChecklist.todos ||
    Object.keys(modernaChecklist.todos).length < 2
  ) {
    return null
  }

  if (type === 'free') {
    return Object.keys(modernaChecklist.todos)[0]
  }

  if (type === 'large') {
    return Object.keys(modernaChecklist.todos)[1]
  }

  return null
}

export const getChecklistAdsById = (state, id) => {
  let candidates = state.ads[AD_TYPE_CHECKLIST_OFFER] || {}
  candidates = Object.values(candidates)

  return candidates.filter(
    c => c.checklistIds && c.checklistIds.split(',').includes(id)
  )
}

export const getWeekAdsByNumberAndType = (state, weekNumber, tabType) => {
  let candidates = state.ads[AD_TYPE_WEEK_CARD] || {}
  candidates = Object.values(candidates)

  candidates = candidates.filter(ad => {
    const currentTabTargetWeeksKey = `target${upperFirst(tabType)}Weeks`

    return ad[currentTabTargetWeeksKey]
      ? ad[currentTabTargetWeeksKey].includes(`"${weekNumber}"`)
      : false
  })

  return candidates
}

export const getAdsByCampaign = (state, campaign) => {
  const candidates = state.ads[AD_TYPE_OFFER]

  return Object.values(candidates || {}).filter(
    c => c.campaign && c.campaign === campaign
  )
}
