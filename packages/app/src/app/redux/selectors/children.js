import { get } from 'lodash'

export const getChildren = state => state.firebase.data.children

export const getChild = (state, childId) =>
  get(state.firebase.data, `children.${childId}`)

export const getUserChildren = (state, userId) =>
  get(state.firebase.data, `childrenByUser.${userId}`)
