/**
 *
 * Onboarding actions
 *
 */

import Pregnancy from '@/app/database/models/Pregnancy'

import * as PregnanciesActions from '@/app/database/actions/pregnancies'
import * as RouterActions from '@/app/redux/router/actions'
import * as TempActions from '@/app/redux/temp'
import * as ProfileActions from '@/app/redux/profile/actions'
import { NavigationActions } from 'react-navigation'

import { MailingApi } from '@/app/api/Preggers'

import { isEmpty } from 'lodash'

export const startApp = () => async (dispatch, getState, getFirebase) => {
  const firebase = getFirebase()
  const profile = getState().firebase.profile
  const onboarding = getState().temp.onboarding

  if (onboarding.userType && onboarding.pregnancyDueDate) {
    const userDiff = {}

    if (onboarding.userName && profile.name !== onboarding.userName) {
      userDiff.name = onboarding.userName
    }

    if (profile.type !== onboarding.userType) {
      userDiff.type = onboarding.userType
    }

    if (!isEmpty(userDiff)) {
      await firebase.updateProfile(userDiff)

      if (userDiff.name) {
        await dispatch(
          ProfileActions.updateFirebaseProfileDisplayName(userDiff.name)
        )
      }
    }

    let pregnancyId = profile.pregnancy

    if (!pregnancyId) {
      const pregnancy = await dispatch(
        PregnanciesActions.add({
          dueDate: onboarding.pregnancyDueDate,
        })
      )

      pregnancyId = pregnancy.id
    }

    const { pregnancyWeek } = Pregnancy.getProgress(onboarding.pregnancyDueDate)

    console.info('pregnancyWeek: ', pregnancyWeek)

    dispatch(TempActions.remove('onboarding'))

    dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: '/~home',
            action: NavigationActions.navigate({
              routeName: '/~start-view',
              action: NavigationActions.navigate({
                routeName: '/~pregnancy',
                params: {
                  pregnancyId,
                  pregnancyCurrentWeekNumber: pregnancyWeek,
                  activeWeekNumber: pregnancyWeek,
                },
              }),
            }),
          }),
        ],
      })
    )
  } else {
    dispatch(TempActions.remove('onboarding'))

    dispatch(RouterActions.resetToUrl(['/~home/~start-view/missing-pregnancy']))
  }

  MailingApi.sendNewUserWelcomeEmail().catch(err =>
    console.log('Error sending new user welcome email', err)
  )
}
