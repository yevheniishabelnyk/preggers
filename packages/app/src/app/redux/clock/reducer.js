/**
 *
 * Clock reducer
 *
 */

import { ActionType } from './types'
import Clock from './state'

const initialState = new Clock()

export default function clockReducer(state = initialState, action) {
  switch (action.type) {
    case ActionType.CLOCK__UPDATE:
      return Object.assign({}, state, action.payload)

    default:
      return state
  }
}
