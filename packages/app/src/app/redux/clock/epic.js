/**
 *
 * Clock epic
 *
 */

import 'rxjs/add/operator/mapTo'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/takeUntil'
import { timer } from 'rxjs/observable/timer'
import { ActionType } from './types'
import * as ClockActions from './actions'
import moment from 'moment'

const clockEpic = action$ =>
  action$.ofType(ActionType.CLOCK__START).switchMap(() => {
    const now = moment()
    const seconds = now.seconds()
    const milliseconds = now.milliseconds()

    const delay = (60 - seconds) * 1000 + (1000 - milliseconds)

    return timer(delay, 60000)
      .map(index => {
        if (index !== 0 && index % 10 === 0) {
          return ClockActions.restart()
        }

        return ClockActions.update()
      })
      .takeUntil(action$.ofType(ActionType.CLOCK__STOP))
  })

export default clockEpic
