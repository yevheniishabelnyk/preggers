import moment from 'moment'

export default class Clock {
  constructor() {
    const now = moment()

    this.date = now.format('YYYY-MM-DD')
    this.hours = now.hours()
    this.minutes = now.minutes()
  }
}
