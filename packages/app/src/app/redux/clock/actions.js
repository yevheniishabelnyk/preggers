/**
 *
 * Clock actions
 *
 */

import { ActionType } from './types'

import Clock from './state'

export const start = () => ({
  type: ActionType.CLOCK__START,
})

export const stop = () => ({
  type: ActionType.CLOCK__STOP,
})

export const update = () => (dispatch, getState) => {
  const currentDate = getState().clock.date

  const nextClock = new Clock()

  dispatch({
    type: ActionType.CLOCK__UPDATE,
    payload: nextClock,
  })

  if (currentDate !== nextClock.date) {
    dispatch(changeDate(nextClock.date, currentDate))
  }
}

export const restart = () => dispatch => {
  dispatch(update())

  dispatch(stop())

  setTimeout(() => {
    dispatch(start())
  }, 0)
}

export const changeDate = (newDate, prevDate) => ({
  type: ActionType.CLOCK__DATE_UPDATE,
  meta: {
    newDate,
    prevDate,
  },
})
