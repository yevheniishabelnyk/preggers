import { combineEpics } from 'redux-observable'
import clockEpic from '@/app/redux/clock/epic'
// import dateChangeEpic from './dateChangeEpic'
import { getFirebase } from 'react-redux-firebase'

const rootEpic = (...args) =>
  combineEpics(
    clockEpic
    // dateChangeEpic
  )(...args, getFirebase)

export default rootEpic
