/**
 *
 * Date change epic
 *
 */

import 'rxjs/add/operator/mapTo'
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/takeUntil'

import { ActionType } from '@/app/redux/clock/types'
import * as RouterActions from '@/app/redux/router/actions'
import Pregnancy from '@/app/database/models/Pregnancy'
import { getPregnancy } from '@/app/redux/selectors'

const dateChangeEpic = (action$, store) =>
  action$.ofType(ActionType.CLOCK__DATE_UPDATE).map(() => {
    const userPregnancyId = store.getState().firebase.profile.pregnancy

    if (userPregnancyId) {
      const pregnancy = getPregnancy(store.getState(), userPregnancyId)

      if (pregnancy) {
        const { passedDays: tipDay } = Pregnancy.getProgress(
          pregnancy.dueDate,
          pregnancy.lengthType
        )

        console.info('tipDay: ', tipDay)

        return RouterActions.goToSimpleUrl(`/daily-tip?day=${tipDay}`)
      }
    }
  })

export default dateChangeEpic
