//
// Set up redux-logger, and take into account that our
// reducers use immutablejs.
//
// SEE
// https://github.com/evgenyrodionov/redux-logger
//

import { createLogger } from 'redux-logger'
import { applyMiddleware } from 'redux'
import { omit } from 'lodash'

// log actions in development mode
const removeNavigation = state => {
  return omit(state, 'router')
}

const logger = createLogger({
  collapsed: true,
  // only log in development mode
  predicate: () => __DEV__,
  // transform state before print
  stateTransformer: state => removeNavigation(state),
  // transform immutable action payloads to plain objects
})

export default {
  enhancers: __DEV__ ? [applyMiddleware(logger)] : [],
}
