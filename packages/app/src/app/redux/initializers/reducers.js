//
// reducers
//
// Optionally you can include your reducer forming in one place as
// an initializer.
//
// In addition, if you want more modularity you can make an initializer
// per reducer. So if you were ever to kill the 'settings' module,
// you would only need to remove that initializer file.
//

import session from '@/app/redux/session'
import temp from '@/app/redux/temp'
import settings from '@/app/redux/settings/persistedReducer'
import device from '@/app/redux/device'
import clock from '@/app/redux/clock/reducer'
import ads from '@/app/redux/ads/reducer'

const reducers = {
  session,
  temp,
  settings,
  device,
  ads,
  clock,
}

export default {
  name: 'reducers',
  reducers: reducers,
}
