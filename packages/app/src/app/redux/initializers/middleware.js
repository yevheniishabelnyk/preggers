/**
 * Add async middleware, and other "core" middleware that
 * you choose to. `applyMiddleware` creates an enhancer,
 * so we can export that under `enhancers`.
 */

import { applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { getFirebase } from 'react-redux-firebase'

import rootEpic from '@/app/redux/epics'
import { createEpicMiddleware } from 'redux-observable'

const epicMiddleware = createEpicMiddleware(rootEpic)

const middlewares = [epicMiddleware, thunk.withExtraArgument(getFirebase)]

export default {
  name: 'middleware',
  enhancers: [applyMiddleware(...middlewares)],
}
