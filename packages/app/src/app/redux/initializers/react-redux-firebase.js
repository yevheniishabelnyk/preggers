/**
 *
 * react-redux-firebase initializer
 *
 */

import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase'
import { compose, applyMiddleware } from 'redux'
import * as firebase from 'firebase'
import config from 'config'

import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const firebaseConfig = {
  apiKey: config.FIREBASE__API_KEY,
  databaseURL: config.FIREBASE__DATABASE_URL,
  projectId: config.FIREBASE__PROJECT_ID,
  storageBucket: config.FIREBASE__STORAGE_BUCKET,
  messagingSenderId: config.FIREBASE__MESSAGING_SENDER_ID,
  persistence: true,
}

// initialize firebase instance
firebase.initializeApp(firebaseConfig) // <- new to v2.*.*

// initialize firestore
// firebase.firestore() // <- needed if using firestore

// react-redux-firebase config
const rrfConfig = {
  userProfile: 'users',
  enableRedirectHandling: false,
  updateProfileOnLogin: true,
  preserveOnLogin: {
    profile: function(profile) {
      return profile
    },
    auth: true,
  },
  preserveOnLogout: ['content'],
  profileParamsToPopulate: [
    { child: 'pregnancy', root: 'pregnancies' },
    { child: 'connection', root: 'connections' },
    { child: 'invitation', root: 'invitations' },
  ],
  // profileDecorator: (userData) => ({ email: userData.email }) // customize format of user profile
  // enableLogging: true, // enable/disable Firebase's database logging
  // useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
}

import cacheImagesMiddleware from '@/app/redux/middlewares/cacheImagesMiddleware'
import watchSetProfileAction from '@/app/redux/middlewares/watchSetProfileAction'

const rrfEnhancer = createStoreFn => {
  return compose(
    reactReduxFirebase(firebase, rrfConfig), // firebase instance as first argument
    applyMiddleware(cacheImagesMiddleware, watchSetProfileAction)
    // reduxFirestore(firebase) // <- needed if using firestore
  )(createStoreFn)
}

const persistConfig = {
  key: 'react-redux-firebase',
  storage: storage,
  whitelist: ['data', 'profile', 'auth'],
}

export default {
  name: 'react-redux-firebase',
  reducers: {
    firebase: persistReducer(persistConfig, firebaseReducer),
    // firestore: firestoreReducer // <- needed if using firestore
  },
  enhancers: [rrfEnhancer],
}
