/*
 *
 * react-navigation initializer
 *
 */

import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers'
import { applyMiddleware } from 'redux'

import persistedRouterReducer from '@/app/redux/router/persistedReducer'

const navigationMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.router
)

export default {
  name: 'react-navigation',
  enhancers: [applyMiddleware(navigationMiddleware)],
  reducers: {
    router: persistedRouterReducer,
  },
}
