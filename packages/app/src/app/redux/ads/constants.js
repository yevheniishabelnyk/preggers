/**
 *
 * Ad constants
 *
 */

import { CONTENT_SERVICE__API_BASE } from 'config'

// Action type definitions
export const ADS__CLEAR = 'ADS__CLEAR'
export const ADS__SET_ADS_BY_TYPE = 'ADS__SET_ADS_BY_TYPE'
export const ADS__ADD = 'ADS__ADD'
export const ADS__REMOVE = 'ADS__REMOVE'

// Ad values
export const AD_TYPE_ONBOARDING = 'ONBOARDING_SCREEN'
export const AD_TYPE_CHECKLIST_OFFER = 'CHECKLIST_OFFER'
export const AD_TYPE_DEALS = 'DEAL'
export const AD_TYPE_WEEK_CARD = 'WEEK_CARD'
export const AD_TYPE_OFFER = 'OFFER'
export const AD_TYPE_IMAGE_AD = 'IMAGE_AD'

export const USER_TYPE_MOTHER = 'MOTHER'
export const USER_TYPE_SINGLE_MOTHER = 'SINGLE_MOTHER'
export const USER_TYPE_FATHER = 'FATHER'
export const USER_TYPE_PARTNER = 'PARTNER'
export const USER_TYPE_GRANDPARENT = 'GRANDPARENT'
export const USER_TYPE_UNCLE = 'UNCLE'
export const USER_TYPE_AUNT = 'AUNT'
export const USER_TYPE_FRIEND = 'FRIEND'
export const USER_TYPE_FAMILY = 'FAMILY'

// Custom campaigns
export const MODERNA_PREGNANCY_INSURANCE_CAMPAIGN = 'ModernaPregnancyInsurance'
export const VALID_CAMPAIGNS = [MODERNA_PREGNANCY_INSURANCE_CAMPAIGN]

export const CAMPAIGN_ROUTE_MAP = {
  [MODERNA_PREGNANCY_INSURANCE_CAMPAIGN]: '/onboarding-safety-offer',
}

// Fetch constants
export const ADS_FETCH_TIMEOUT_MS = 15000
export const ADS_FETCH_URL = `${CONTENT_SERVICE__API_BASE}/content/fetch/`
export const ADS_FETCH_HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
}

// Client name in content service
export const ADS_CLIENT_PREGGERS = 'PREGGERS'

// Post field types (corresponding to content service documentation)
export const ADS_POST_FIELD_CLIENT = 'client'
export const ADS_POST_FIELD_TYPES = 'types'
export const ADS_POST_FIELD_AUTH_TOKEN = 'authToken'
export const ADS_POST_FIELD_USER_EMAIL = 'email'
export const ADS_POST_FIELD_USER_TYPE = 'userType'
export const ADS_POST_FIELD_GENDER = 'gender'
export const ADS_POST_FIELD_BIRTHDAY = 'birthday'
export const ADS_POST_FIELD_CHILDREN = 'children'
export const ADS_POST_FIELD_PREGNANCIES = 'pregnancies'
export const ADS_POST_FIELD_LOCALE = 'locale'
export const ADS_POST_FIELD_COUNTRY = 'country'
export const ADS_POST_FIELD_CHECKLIST_ID = 'checklistId'
export const ADS_POST_FIELD_CAMPAIGN = 'campaign'
export const ADS_POST_FIELD_PARTNER_EMAIL_ADDRESSES = 'excludeIfEmailConverted'
export const ADS_POST_FIELD_VERSION = 'clientVersion'
export const ADS_POST_FIELD_OS = 'operatingSystem'
export const ADS_POST_FIELD_TAG_IDS = 'tagIds'
export const ADS_POST_FIELD_WEEK_NUMBER = 'weekNumber'
export const ADS_POST_FIELD_WEEK_TAB_TYPE = 'weekTabType'
