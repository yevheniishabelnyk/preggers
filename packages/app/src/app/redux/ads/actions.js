/**
 *
 * Ad redux actions
 *
 */

import { CONTENT_SERVICE__API_BASE } from 'config'

import {
  FATHER,
  GRANDPARENT,
  PARTNER,
  ANT,
  FRIEND,
  MOTHER,
  SINGLE_MOTHER,
  UNCLE,
  FAMILY,
} from '@/app/constants/UserTypes'

import {
  ADS__ADD,
  ADS__CLEAR,
  ADS__SET_ADS_BY_TYPE,
  ADS__REMOVE,
  ADS_FETCH_URL,
  ADS_FETCH_HEADERS,
  ADS_POST_FIELD_CLIENT,
  ADS_CLIENT_PREGGERS,
  ADS_POST_FIELD_TYPES,
  ADS_POST_FIELD_AUTH_TOKEN,
  ADS_POST_FIELD_USER_EMAIL,
  ADS_POST_FIELD_GENDER,
  ADS_POST_FIELD_BIRTHDAY,
  ADS_POST_FIELD_LOCALE,
  ADS_POST_FIELD_COUNTRY,
  ADS_POST_FIELD_PARTNER_EMAIL_ADDRESSES,
  ADS_POST_FIELD_PREGNANCIES,
  ADS_POST_FIELD_CHILDREN,
  ADS_POST_FIELD_USER_TYPE,
  ADS_POST_FIELD_CHECKLIST_ID,
  ADS_POST_FIELD_WEEK_NUMBER,
  ADS_POST_FIELD_WEEK_TAB_TYPE,
  ADS_POST_FIELD_VERSION,
  ADS_POST_FIELD_OS,
  ADS_POST_FIELD_TAG_IDS,
  USER_TYPE_UNCLE,
  USER_TYPE_SINGLE_MOTHER,
  USER_TYPE_FATHER,
  USER_TYPE_PARTNER,
  USER_TYPE_AUNT,
  USER_TYPE_GRANDPARENT,
  USER_TYPE_MOTHER,
  USER_TYPE_FRIEND,
  USER_TYPE_FAMILY,
  VALID_CAMPAIGNS,
  ADS_POST_FIELD_CAMPAIGN,
  ADS_FETCH_TIMEOUT_MS,
} from './constants'

import Sentry from 'sentry-expo'
import * as ContentServiceApi from '@/app/api/ContentService'

import { Platform } from 'react-native'

import appData from '@/../app.json'

import promiseTimeout from '@/app/promiseTimeout'

const isValidRequest = request => typeof request === 'object'

const isValidCampaign = campaign => VALID_CAMPAIGNS.includes(campaign)

const createFetchBody = request => {
  const {
    authToken,
    types,
    email,
    partnerEmail,
    pregnancies,
    children,
    birthday,
    gender,
    locale,
    countryCode,
    userType,
    checklistId,
    weekNumber,
    weekTabType,
    campaign,
    tagIds,
  } = request

  const defaultLocale = 'en_GB'

  const localeMap = {
    en: 'en_GB',
    sv: 'sv_SE',
  }

  const userTypeMap = {
    [MOTHER]: USER_TYPE_MOTHER,
    [SINGLE_MOTHER]: USER_TYPE_SINGLE_MOTHER,
    [FATHER]: USER_TYPE_FATHER,
    [PARTNER]: USER_TYPE_PARTNER,
    [GRANDPARENT]: USER_TYPE_GRANDPARENT,
    [UNCLE]: USER_TYPE_UNCLE,
    [ANT]: USER_TYPE_AUNT,
    [FRIEND]: USER_TYPE_FRIEND,
    [FAMILY]: USER_TYPE_FAMILY,
  }

  const body = {
    [ADS_POST_FIELD_CLIENT]: ADS_CLIENT_PREGGERS,
    [ADS_POST_FIELD_TYPES]: types,
    [ADS_POST_FIELD_AUTH_TOKEN]: authToken,
    [ADS_POST_FIELD_USER_EMAIL]: email,
    [ADS_POST_FIELD_USER_TYPE]: userTypeMap[userType],
    [ADS_POST_FIELD_GENDER]: gender,
    [ADS_POST_FIELD_BIRTHDAY]: birthday,
    [ADS_POST_FIELD_LOCALE]: localeMap[locale] || defaultLocale,
    [ADS_POST_FIELD_COUNTRY]: countryCode,
    [ADS_POST_FIELD_PREGNANCIES]: pregnancies,
    [ADS_POST_FIELD_CHILDREN]: children,
    [ADS_POST_FIELD_PARTNER_EMAIL_ADDRESSES]: partnerEmail,
    [ADS_POST_FIELD_CHECKLIST_ID]: checklistId,
    [ADS_POST_FIELD_WEEK_NUMBER]: weekNumber,
    [ADS_POST_FIELD_WEEK_TAB_TYPE]: weekTabType,
    [ADS_POST_FIELD_CAMPAIGN]: campaign,
    [ADS_POST_FIELD_VERSION]: appData.expo.version,
    [ADS_POST_FIELD_OS]: Platform.OS,
    [ADS_POST_FIELD_TAG_IDS]: tagIds,
  }

  return JSON.stringify(body)
}

export const fetchAdsAndReplaceTypeState = request => fetchAds(request, true)

export const fetchAds = (request, replaceTypeState = false) => async (
  dispatch,
  getState
) => {
  const isConnected = getState().device.isConnected

  if (isConnected && isValidRequest(request)) {
    try {
      console.log(`[ADS] Fetching ads from ${ADS_FETCH_URL}`)

      const requestBody = createFetchBody(request)

      // console.log('[ADS] Fetching with request body', requestBody)

      const response = await promiseTimeout(
        ADS_FETCH_TIMEOUT_MS,
        fetch(ADS_FETCH_URL, {
          method: 'POST',
          headers: ADS_FETCH_HEADERS,
          body: requestBody,
        })
      )

      if (!response.ok) {
        console.log('[ADS] Server error when fetching ads', response.status)

        return Promise.reject('Error: Server-side error!')
      }

      let body = await response.json()

      body = Object.values(body || []).length > 0 ? body : []

      body = body.filter(
        ad => !ad.customCampaign || isValidCampaign(ad.customCampaign)
      )

      console.log('[ADS] Response successfully received and parsed')
      console.log(`[ADS] Adding ${body.length} ads to redux`)

      if (replaceTypeState) {
        const adsByUid = Object.values(body).reduce(
          (acc, cv) => ({ ...acc, [cv.uid]: cv }),
          {}
        )

        dispatch(setAdsByType(adsByUid, request.types[0]))
      } else {
        await Promise.all(body.map(ad => dispatch(add(ad))))
      }

      return Promise.resolve()
    } catch (err) {
      console.log('[ADS] Client error when fetching ads', err.message)

      if (!__DEV__) {
        Sentry.captureException(err)
      }

      return Promise.reject(err)
    }
  }

  if (!isValidRequest(request)) {
    console.log('[ADS] Client error with request data')

    return Promise.reject('Error: Not valid request!')
  }
}

export const add = ad => ({
  type: ADS__ADD,
  ad,
})

export const remove = ad => ({
  type: ADS__REMOVE,
  ad,
})

export const clear = () => ({
  type: ADS__CLEAR,
})

export const setAdsByType = (ads, adType) => ({
  type: ADS__SET_ADS_BY_TYPE,
  ads,
  adType,
})

export const markInteraction = (ad, type, source = false) => async (
  dispatch,
  getState
) => {
  const isConnected = getState().device.isConnected

  if (isConnected) {
    try {
      await ContentServiceApi.markInteraction(ad, type, source)
    } catch (err) {
      Sentry.captureException(err)
    }
  }
}

export const markInteractionByUidAndEmail = (
  adUid,
  email = '__UNDEFINED__',
  type,
  source = '__SOURCE__'
) => async (dispatch, getState) => {
  const allowedTypes = ['impression', 'click', 'conversion']

  if (adUid && email && allowedTypes.includes(type)) {
    const isConnected = getState().device.isConnected

    const interactionUrl = `${CONTENT_SERVICE__API_BASE}/interaction/${type}/${adUid}/${email}/${source}`

    const ad = {
      uid: adUid,
      [`${type}Endpoint`]: interactionUrl,
    }

    if (isConnected) {
      try {
        await ContentServiceApi.markInteraction(ad, type)
      } catch (err) {
        Sentry.captureException(err)
      }
    }
  }
}
