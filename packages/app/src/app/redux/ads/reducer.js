/**
 *
 * Ad reducer
 *
 */

import { omit } from 'lodash'
import {
  ADS__ADD,
  ADS__CLEAR,
  ADS__REMOVE,
  ADS__SET_ADS_BY_TYPE,
} from './constants'

const initialState = {}

export default function adsReducer(state = initialState, action) {
  const { ad, ads, adType } = action

  switch (action.type) {
    case ADS__ADD:
      return {
        ...state,
        [ad.type]: {
          ...state[ad.type],
          [ad.uid]: ad,
        },
      }

    case ADS__REMOVE:
      return {
        ...state,
        [ad.type]: omit({ ...state[ad.type] }, [ad.uid]),
      }

    case ADS__SET_ADS_BY_TYPE:
      return {
        ...state,
        [adType]: ads,
      }

    case ADS__CLEAR:
      return initialState

    default:
      return state
  }
}
