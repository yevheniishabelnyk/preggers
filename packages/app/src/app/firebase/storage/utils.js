/**
 *
 * Firebase Storage actions
 *
 */

import moment from 'moment'

import { Platform } from 'react-native'

export const putFile = async (firebase, uri, storageDir) => {
  const response = await fetch(uri)

  const blob = await response.blob()

  if (Platform.OS === 'android' && !blob.data.name) {
    blob.data.name = 'profilepic.jpeg'
  }

  const fileName = blob.data.name.substring(0, blob.data.name.lastIndexOf('.'))
  const ext = blob.data.name.substring(blob.data.name.lastIndexOf('.'))

  const storagePath = `${storageDir}/${fileName} ${moment().toISOString()}${ext}`

  const ref = firebase
    .storage()
    .ref()
    .child(storagePath)

  const task = ref.put(blob)

  return new Promise((resolve, reject) => {
    task.on(
      'state_changed',
      () => {
        // receives progress value
        // console.log('progress', progress)
        /* noop but you can track the progress here */
      },
      reject /* this is where you would put an error callback! */,
      () => {
        resolve(task.snapshot.downloadURL)

        firebase.update(`/userOwned/files/${firebase.auth().currentUser.uid}`, {
          [encodeURIComponent(storagePath).replace(/\./g, '%2E')]: true,
        })
      }
    )
  })
}

export const deleteFile = async (firebase, remoteUri) => {
  const fileStoragePath = decodeURIComponent(
    remoteUri.split(/[?#]/)[0].match(/[^/]*$/)[0]
  )

  const fileRef = firebase
    .storage()
    .ref()
    .child(fileStoragePath)

  try {
    await Promise.all([fileRef.delete()])

    await firebase.update(
      `/userOwned/files/${firebase.auth().currentUser.uid}`,
      {
        [encodeURIComponent(fileStoragePath).replace(/\./g, '%2E')]: null,
      }
    )

    return Promise.resolve()
  } catch (err) {
    console.info('err: ', err)

    return Promise.resolve()
  }
}
