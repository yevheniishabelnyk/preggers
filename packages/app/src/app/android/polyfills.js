/**
 *
 * Android polyfills
 *
 */

import { Platform } from 'react-native'

import Intl from 'intl'
import 'intl/locale-data/jsonp/en'
import 'intl/locale-data/jsonp/sv'

if (Platform.OS === 'android') {
  global.Intl = global.Intl || Intl
}
