/**
 *
 * Required permissions and fields from user Facebook profile
 *
 */

export const PERMISSIONS = ['public_profile', 'email']

export const REQUIRED_USER_FIELDS = [
  'id',
  'email',
  'name',
  'picture.width(400).height(400)',
  'locale',
  'gender',
].join()
