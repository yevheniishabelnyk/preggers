/**
 *
 *  User Types
 *
 */

export const MOTHER = 'mother'
export const SINGLE_MOTHER = 'single_mother'
export const FATHER = 'father'
export const PARTNER = 'partner'
export const GRANDPARENT = 'grandparent'
export const UNCLE = 'uncle'
export const ANT = 'ant'
export const FRIEND = 'friend'
export const FAMILY = 'family'
