/*
 *
 * DATABASE DATA FORMATS
 *
 */

export const TIME_FORMAT = 'HH:mm'
export const DATE_FORMAT = 'YYYY-MM-DD'
export const WEIGHT_UNITS = 'g'
export const LENGTH_UNITS = 'cm'
