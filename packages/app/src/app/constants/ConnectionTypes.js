/*
 *
 * With whom to connect
 *
 */

export const FATHER = 'father'
export const PARTNER = 'partner'
export const FRIEND = 'friend'
export const FAMILY = 'family'
