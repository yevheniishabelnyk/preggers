export const PUBLIC_ROUTES = [
  '/tools-menu',
  '/food-search',
  '/knowledge-search',
  '/knowledge-article',
  '/baby-names-country',
  '/baby-names-menu',
]

export const PRIVATE_ROUTES = [
  '/moderna-free-pregnancy-insurance-form',
  '/checklist-item',
  '/lekmer-discount-form',
  '/~start-view/~pregnancy/baby',
  '/~start-view/~pregnancy/mother',
  '/~start-view/~pregnancy/partner',
  '/~home/~knowledge/my-week',
]
