/*
 *
 * App routes
 *
 */

import homeNavigator from '@/home/navigator'
import errorViewRoutes from '@/error-view/routes'
import onboardingRoutes from '@/onboarding/routes'
import babyNamesRoutes from '@/baby-names/routes'
import knowledgeRoutes from '@/knowledge/routes'
import authRoutes from '@/auth/routes'
import settingsRoutes from '@/settings/routes'
import connectRoutes from '@/connect/routes'
import agreementRoutes from '@/agreement/routes'
import checklistRoutes from '@/checklist/routes'
import foodRoutes from '@/food/routes'
import notificationsRoutes from '@/notifications/routes'
import posterCompilerRoutes from '@/poster-compiler/routes'
import takePhotoRoutes from '@/take-photo/routes'
import formsRoutes from '@/forms/routes'
import pregnancyRoutes from '@/pregnancy/routes'
import childrenRoutes from '@/children/routes'
import contactUsRoutes from '@/contact-us/routes'
import tipsRoutes from '@/tips/routes'
import otherAppsRoutes from '@/other-apps/routes'
import insuranceRoutes from '@/insurance/routes'
import lekmerRoutes from '@/campaigns/lekmer-discount/routes'
import toolsRoutes from '@/tools/routes'
import competitionRoutes from '@/competitions/routes'
import dealRoutes from '@/deals/routes'

export const routes = Object.assign(
  {},
  { '/~home': homeNavigator },
  errorViewRoutes,
  onboardingRoutes,
  babyNamesRoutes,
  knowledgeRoutes,
  authRoutes,
  connectRoutes,
  settingsRoutes,
  agreementRoutes,
  checklistRoutes,
  foodRoutes,
  notificationsRoutes,
  posterCompilerRoutes,
  takePhotoRoutes,
  formsRoutes,
  pregnancyRoutes,
  childrenRoutes,
  contactUsRoutes,
  tipsRoutes,
  otherAppsRoutes,
  insuranceRoutes,
  toolsRoutes,
  lekmerRoutes,
  competitionRoutes,
  dealRoutes
)

export const nonAuthenticatedInitialRouteName = '/auth-login'
export const authenticatedInitialRouteName = '/~home'
export const errorRouteName = '/error'
