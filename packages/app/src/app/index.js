/*
 *
 * App entry point
 *
 */

import '@/app/android/polyfills'

import { Font } from 'expo'

const loadFontsPromise = Font.loadAsync({
  'Poppins-Bold': require('assets/fonts/Poppins-Bold.ttf'),
  'Poppins-SemiBold': require('assets/fonts/Poppins-SemiBold.ttf'),
  'Roboto-Regular': require('assets/fonts/Roboto-Regular.ttf'),
  'Roboto-Medium': require('assets/fonts/Roboto-Medium.ttf'),
  'Roboto-Bold': require('assets/fonts/Roboto-Bold.ttf'),
  'Roboto-Light': require('assets/fonts/Roboto-Light.ttf'),
  'Roboto-Italic': require('assets/fonts/Roboto-Italic.ttf'),
  'Roboto-LightItalic': require('assets/fonts/Roboto-LightItalic.ttf'),
  'Now-Regular': require('assets/fonts/Now-Regular.ttf'),
  'Now-Bold': require('assets/fonts/Now-Bold.ttf'),
  'Now-Medium': require('assets/fonts/Now-Medium.ttf'),
  'Now-Black': require('assets/fonts/Now-Black.ttf'),
})

import config from 'config'
import Sentry from 'sentry-expo'

if (!__DEV__) {
  Sentry.config(config.SENTRY__PUBLIC_DSN).install()
}

import React from 'react'
import { Provider } from 'react-redux'

import App from '@/app/containers/App'
import store from '@/app/store'

import LanguageProvider from '@/app/containers/LanguageProvider'
import { messages } from './i18n'

import { persistStore } from 'redux-persist'
import PersistGate from '@/app/containers/PersistGate'

let persistor = persistStore(store)

export default class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate
          persistor={persistor}
          store={store}
          loadFontsPromise={loadFontsPromise}
        >
          <LanguageProvider messages={messages}>
            <App />
          </LanguageProvider>
        </PersistGate>
      </Provider>
    )
  }
}
