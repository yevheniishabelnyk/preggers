/*
 *
 * LanguageProvider
 *
 * this component connects the redux state language locale to the
 * IntlProvider component and i18n messages (loaded from `/translations`)
 */

// eslint-disable-next-line
const consoleError = console.error.bind(console)
// eslint-disable-next-line
console.error = (message, ...args) => {
  if (
    typeof message === 'string' &&
    (message.startsWith('[React Intl] Missing message:') ||
      message.startsWith('[React Intl] Missing locale data for locale:'))
  ) {
    return
  }
  consoleError(message, ...args)
}

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { IntlProvider } from 'react-intl'
import config from 'config'

@connect(state => ({
  locale: state.settings.locale,
}))
export default class LanguageProvider extends React.Component {
  static propTypes = {
    locale: PropTypes.string,
    messages: PropTypes.object,
    children: PropTypes.element.isRequired,
  }

  render() {
    const { messages, children, locale } = this.props

    return (
      <IntlProvider
        locale={locale}
        messages={messages[locale]}
        defaultLocale={config.DEFAULT_LOCALE}
      >
        {React.Children.only(children)}
      </IntlProvider>
    )
  }
}
