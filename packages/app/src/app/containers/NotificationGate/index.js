/**
 *
 * NotificationGate
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Notifications } from 'expo'

import { View } from 'react-native'
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog'
import ConnectDialog from '@/connect/components/organisms/ConnectDialog'
import DisconnectedFrom from '@/connect/components/organisms/DisconnectedFrom'

import arpeggio from 'assets/sounds/arpeggio.mp3'

import playSound from '@/shared/utils/playSound'

import styles from './styles'

const bottomSlideAnimation = new SlideAnimation({
  slideFrom: 'bottom',
})

export default class NotificationGate extends React.Component {
  static propTypes = {
    profile: PropTypes.object,
    goTo: PropTypes.func,
    dispatch: PropTypes.func,
    children: PropTypes.any,
    router: PropTypes.object,
  }

  state = {
    push: null,
  }

  render() {
    const { push } = this.state

    if (!push) {
      return <View style={styles.container}>{this.props.children}</View>
    }

    const not = this._renderNotification(push)

    return (
      <View style={styles.container}>
        {this.props.children}

        {not ? (
          <PopupDialog
            ref={ref => (this._popup = ref)}
            dialogAnimation={bottomSlideAnimation}
            dialogStyle={styles.popup}
            overlayOpacity={0.7}
            onDismissed={this._popupDismissed}
          >
            {not}
          </PopupDialog>
        ) : null}
      </View>
    )
  }

  _renderNotification = push => {
    if (!push.data) {
      return null
    }

    switch (push.data.notificationType) {
      case 'invited-to-connect':
        return (
          <ConnectDialog
            isInPopup
            invitation={push.data}
            dispatch={this.props.dispatch}
            onClose={this._hidePopup}
            onDecline={this._hidePopup}
            onDone={this._hidePopup}
          />
        )

      case 'connection-cancelled':
        return (
          <DisconnectedFrom
            dispatch={this.props.dispatch}
            notification={push.data}
            onOk={this._hidePopup}
          />
        )

      default:
        return null
    }
  }

  componentWillMount() {
    Notifications.addListener(this._handlePushNotification)
  }

  _handlePushNotification = push => {
    console.log('[NOTIFICATION GATE] received push notification', push)

    if (!push.data) {
      return
    }

    switch (push.data.notificationType) {
      case 'invited-to-connect':
        this._handleInvitatedToConnect(push)
        break

      case 'connection-cancelled':
        this._handleConnectionCanceled(push)
        break
    }
  }

  _handleInvitatedToConnect = push => {
    const { profile, goTo } = this.props
    const { data, origin } = push

    if (profile.email !== data.toEmail) {
      return
    }

    if (origin === 'selected') {
      // push notification was selected when app was in background

      goTo('/connect-accept-invitation', {
        invitationId: data.id,
      })
    } else {
      // push notification received when app was in foreground

      this._showPopup(push)
    }
  }

  _handleConnectionCanceled = push => {
    const { goTo } = this.props
    const { data, origin } = push

    if (origin === 'selected') {
      // push notification was selected when app was in background

      goTo('/notification-item', {
        notificationId: data.id,
      })
    } else {
      // push notification received when app was in foreground

      this._showPopup(push)
    }
  }

  _showPopup = push => {
    console.log('_showPopup')

    const { router } = this.props
    const currentRoute = router.routes[router.index]

    if (currentRoute && !currentRoute.routeName.startsWith('/onboarding')) {
      playSound(arpeggio)

      this.setState({ push }, () => {
        setTimeout(() => {
          this._popup && this._popup.show()
        }, 0)
      })
    }
  }

  _hidePopup = () => {
    console.log('_hidePopup')

    this._popup.dismiss()
  }

  _popupDismissed = () => {
    console.log('_popupDismissed')

    setTimeout(() => {
      this.setState({ push: null })
    }, 500)
  }
}
