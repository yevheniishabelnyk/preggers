import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  popup: {
    flex: 1,
    maxWidth: moderateScale(331),
    maxHeight: moderateScale(547),
    overflow: 'hidden',
  },
})

export default styles
