/**
 *
 * PersistGate
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { NetInfo } from 'react-native'
import { AppLoading, DangerZone, Linking } from 'expo'
const { Localization } = DangerZone

import * as DeviceActions from '@/app/redux/device'
import * as RouterActions from '@/app/redux/router/actions'

@connect(state => ({
  authIsLoaded: state.firebase.auth.isLoaded,
}))
export default class PersistGate extends React.Component {
  static propTypes = {
    persistor: PropTypes.object,
    store: PropTypes.object,
    dispatch: PropTypes.func,
    authIsLoaded: PropTypes.bool,
    children: PropTypes.element.isRequired,
    loadFontsPromise: PropTypes.instanceOf(Promise),
  }

  state = {
    isLoading: true,
  }

  render() {
    if (this.state.isLoading) {
      return (
        <AppLoading
          startAsync={this._listenPersist}
          onError={console.log}
          onFinish={this._handleFinishLoading}
        />
      )
    }

    return this.props.children
  }

  componentWillMount() {
    Linking.getInitialURL()
      .then(url => {
        this.deepLink = url
        this.deepLinkLoaded = true
      })
      .catch(() => {
        this.deepLink = null
        this.deepLinkLoaded = true
      })

    this._unsubscribePersistorState = this.props.persistor.subscribe(
      this._handlePersistorState
    )
    this._handlePersistorState()

    this.props.loadFontsPromise.then(() => {
      console.log('\n[PERSIST GATE] Fonts loaded')

      this.resourcesCached = true
    })
  }

  componentDidMount() {
    this.handleConnectivityChange = isConnected =>
      this.props.dispatch(DeviceActions.update({ isConnected }))

    NetInfo.isConnected.fetch().done(isConnected => {
      this.props.dispatch(DeviceActions.update({ isConnected }))

      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this.handleConnectivityChange
      )
    })
  }

  componentWillUnmount() {
    this._unsubscribePersistorState && this._unsubscribePersistorState()

    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleConnectivityChange
    )
  }

  _listenPersist = () =>
    new Promise(resolve => {
      const interval = setInterval(() => {
        if (
          this.persistBootstrapped &&
          this.resourcesCached &&
          this.deepLinkLoaded &&
          this.props.authIsLoaded
        ) {
          clearInterval(interval)

          resolve()

          this._initDevice()
        }
      }, 20)
    })

  _handleFinishLoading = () => {
    console.log('\n[PERSIST GATE] Finished loading')

    this._handleDeepLink()

    this.props.dispatch(RouterActions.validatePersistedState())

    console.log('\n[PERSIST GATE] Entering the app...')

    this.setState({ isLoading: false })
  }

  _handlePersistorState = () => {
    if (this.props.persistor.getState().bootstrapped) {
      console.log('\n[PERSIST GATE] Persist bootstrapped')

      this.persistBootstrapped = true

      this._unsubscribePersistorState && this._unsubscribePersistorState()
    }
  }

  _handleDeepLink = () => {
    console.log('\n[PERSIST GATE] Handle deep linking', this.deepLink)

    if (this.deepLink) {
      this.props.dispatch(RouterActions.handleDeepLink(this.deepLink))
    }
  }

  _initDevice = async () => {
    try {
      const [deviceLocale, country, timezone] = await Promise.all([
        Localization.getCurrentLocaleAsync(),
        Localization.getCurrentDeviceCountryAsync(),
        Localization.getCurrentTimeZoneAsync(),
        // Localization.getISOCurrencyCodesAsync(),
      ])

      const locale = deviceLocale.substring(0, 2)

      console.log('\n[PERSIST GATE] Device initialized!')
      console.log('\n[PERSIST GATE] Device locale: ', locale)
      console.log('\n[PERSIST GATE] Device country: ', country)
      console.log('\n[PERSIST GATE] Device timezone: ', timezone)

      this.props.store.dispatch(
        DeviceActions.update({
          locale,
          country,
          timezone,
        })
      )
    } catch (err) {
      console.log('\n[PERSIST GATE] Initing device error: ', err)
    }
  }
}
