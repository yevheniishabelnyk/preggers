/**
 *
 * App
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import {
  AUTH__EMAIL_VERIFICATION_DAYS,
  AUTH__REMOVE_NOT_VERIFIED_ACCOUNT_DAY,
  NAVIGATION__PERSISTENCE_MAX_TIME,
} from 'config'

import { StatusBar, BackHandler, AsyncStorage, AppState } from 'react-native'

import { SecureStore, ScreenOrientation, Linking } from 'expo'
import { addNavigationHelpers, NavigationActions } from 'react-navigation'

import AppNavigator from '@/app/navigator'

import * as SettingsActions from '@/app/redux/settings/actions'
import * as RouterActions from '@/app/redux/router/actions'
import { initUser } from '@/app/database/actions/users'

import { withFirebase } from 'react-redux-firebase'

import registerForPushNotificationsAsync from '@/app/registerForPushNotificationsAsync'

import memoize from 'fast-memoize'
import moment from 'moment'

import * as SessionActions from '@/app/redux/session'
import * as ClockActions from '@/app/redux/clock/actions'
import * as ProfileActions from '@/app/redux/profile/actions'

import { createReduxBoundAddListener } from 'react-navigation-redux-helpers'

import NotificationGate from '@/app/containers/NotificationGate'
import ErrorBoundary from '@/app/containers/ErrorBoundary'

import { CacheManager } from '@/vendor/cache-manager'

@withFirebase
@connect(state => ({
  isNewUser: state.session.isNewUser,
  profile: state.firebase.profile,
  auth: state.firebase.auth,
  router: state.router,
  locale: state.settings.locale,
  signInProvider: state.session.signInProvider,
}))
export default class App extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    firebase: PropTypes.object,
    auth: PropTypes.object,
    profile: PropTypes.object,
    router: PropTypes.object,
    locale: PropTypes.string,
    signInProvider: PropTypes.string,
    isNewUser: PropTypes.bool,
  }

  constructor(props) {
    super(props)

    this.appState = AppState.currentState

    this.authLoadedWithReload = false

    this.addListener = createReduxBoundAddListener('root')
  }

  render() {
    return (
      <NotificationGate
        profile={this.props.profile}
        goTo={this._goTo}
        dispatch={this.props.dispatch}
        router={this.props.router}
      >
        <ErrorBoundary auth={this.props.auth} dispatch={this.props.dispatch}>
          <StatusBar barStyle="dark-content" />

          <AppNavigator
            navigation={addNavigationHelpers({
              dispatch: this.props.dispatch,
              state: this.props.router,
              addListener: this.addListener,
            })}
            screenProps={this._getScreenProps()}
          />
        </ErrorBoundary>
      </NotificationGate>
    )
  }

  componentWillMount() {
    if (!this.props.auth.isEmpty) {
      console.log('\n[APP] Auth loaded on app reload')

      this._watchProfileData(this.props.auth)

      this.authLoadedWithReload = true

      this._mapAuthToProfile(this.props.auth)

      this._handleSignIn(this.props.auth)
    }

    ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT)
  }

  componentDidMount() {
    Linking.addEventListener('url', this._handleRedirect)
    AppState.addEventListener('change', this._handleAppStateChange)
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)

    if (this.appState === 'active') {
      AsyncStorage.setItem('lastActiveTime', String(Date.now())).catch(err => {
        console.log('Error: ', err)
      })
    }

    setInterval(() => {
      if (this.appState === 'active') {
        AsyncStorage.setItem('lastActiveTime', String(Date.now())).catch(
          err => {
            console.log('Error: ', err)
          }
        )
      }
    }, 10000)
  }

  onBackPress = () => {
    const { dispatch, router } = this.props

    if (router.index === 0) {
      return false
    }

    dispatch(NavigationActions.back())

    return true
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.auth.isEmpty && !nextProps.auth.isEmpty) {
      // SIGN IN
      this._watchProfileData(nextProps.auth)
      this._mapAuthToProfile(nextProps.auth)

      setTimeout(() => {
        this._handleSignIn(nextProps.auth)
      }, 0)
    }

    if (!this.props.auth.isEmpty && nextProps.auth.isEmpty) {
      // LOGOUT
      this._handleLogout(nextProps)
    }

    if (
      !nextProps.auth.isEmpty &&
      !this.props.profile.isEmpty &&
      nextProps.profile.isEmpty
    ) {
      // USER HAS BEEN REMOVED FROM DATABASE
      console.log('\n[APP] The user has been removed from the database!!!')

      this._handleUserRemove(this.props.profile)
    }
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this._handleRedirect)

    AppState.removeEventListener('change', this._handleAppStateChange)

    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
  }

  _handleAppStateChange = async nextAppState => {
    console.info('[App] Next app state: ', nextAppState)

    if (this.props.auth.isEmpty) {
      return
    }

    if (
      this.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      try {
        const lastActiveTime = parseInt(
          await AsyncStorage.getItem('lastActiveTime')
        )

        if (
          lastActiveTime &&
          Date.now() - lastActiveTime > NAVIGATION__PERSISTENCE_MAX_TIME
        ) {
          if (this.props.router.isCurrentRouteOpenedWithDeepLink) {
            return this.props.dispatch(RouterActions.resetDeepLinkFlag())
          }

          const activeRoute = this.props.router.routes[this.props.router.index]

          let shouldReset

          if (activeRoute.routeName !== '/~home') {
            shouldReset = true
          } else {
            const homeActiveRoute = activeRoute.routes[activeRoute.index]

            if (homeActiveRoute !== '/~start-view') {
              shouldReset = true
            }
          }

          if (shouldReset) {
            this.props.dispatch(
              RouterActions.resetToAuthenitcatedInitialRoute()
            )
          }
        }
      } catch (err) {
        console.log('Error: ', err)
      }
    }

    this.appState = nextAppState
  }

  _handleSignIn = auth => {
    console.log('\n[APP] Auth added')
    console.log('\n[APP] Creating session...')
    console.info('\n[APP] Is new user: ', this.props.isNewUser)

    if (this.props.isNewUser) {
      this._handleFirstTimeSignIn(auth)

      setTimeout(() => {
        registerForPushNotificationsAsync()
      }, 20000)
    } else {
      this._handleLogin()

      setTimeout(() => {
        if (!this.props.profile.isEmpty) {
          this.props.dispatch(SettingsActions.syncRemoteLocal())
          this.props.dispatch(ProfileActions.syncProfileUTCOffset())
          this.props.dispatch(ProfileActions.syncProfileDevice())
        }
      }, 3000)

      setTimeout(() => {
        registerForPushNotificationsAsync()
      }, 5000)
    }

    CacheManager.init([`users/${auth.uid}`, 'content'])
      .then(() => console.info('\n[APP] Cache inited'))
      .catch(err => console.info('\n[APP] Error. Faild to init cache', err))

    setTimeout(() => {
      // this._verifyUser()
      this.props.dispatch(ClockActions.start())
    }, 500)

    if (this.authLoadedWithReload) {
      this.props.dispatch(SessionActions.add(auth.stsTokenManager.accessToken))
    } else {
      //Uncomment this if you need more info about current session
      /*setTimeout(() => {
        this.props.firebase
          .auth()
          .currentUser.getIdToken()
          .then(jwtToken => {
            this.props.dispatch(SessionActions.add(jwtToken))

            console.log('\n[APP] Session created!')
          })
          .catch(err => console.log('\n[APP] Error: session faild: ', err))
      }, 1000)*/
    }
  }

  _handleRedirect = event => {
    this.props.dispatch(RouterActions.handleDeepLink(event.url))
  }

  _handleUserRemove = profile => {
    CacheManager.clearUserCache(profile.id)

    this.props.firebase.logout()
  }

  _mapAuthToProfile = auth => {
    const data = {
      id: auth.uid,
      name: auth.displayName,
    }

    if (!this.authLoadedWithReload && auth.photoURL) {
      data.image = { type: 'file', uri: auth.photoURL }
    }

    this.props.dispatch(ProfileActions.updateLocalState(data))
  }

  _watchProfileData = auth => {
    const { firebase } = this.props

    firebase.watchEvent('value', `users/${auth.uid}`)
    firebase.watchEvent('value', `pregnanciesByUser/${auth.uid}`)
    firebase.watchEvent('value', `childrenByUser/${auth.uid}`)
  }

  _unWatchProfileData = auth => {
    const { firebase } = this.props

    firebase.unWatchEvent('value', `users/${auth.uid}`)
    firebase.unWatchEvent('value', `pregnanciesByUser/${auth.uid}`)
    firebase.unWatchEvent('value', `childrenByUser/${auth.uid}`)
  }

  _handleFirstTimeSignIn = auth => {
    console.log('\n[APP] Auth first time sign in')

    const { signInProvider } = this.props

    switch (signInProvider) {
      case 'password':
        this._goTo('/onboarding-enter-name')

        this._initUser()
        break

      case 'facebook.com':
        if (!auth.email) {
          this._goTo('/auth-enter-email', {
            onDone: email => {
              this._goTo('/onboarding-choose-type')

              this._initUser(email)
            },
            onDecline: this._removeUser,
          })
        } else {
          this._goTo('/onboarding-choose-type')

          this._initUser()
        }
        break
    }
  }

  _initUser = async email => {
    try {
      if (email) {
        await this.props.dispatch(
          ProfileActions.updateFirebaseProfileEmail(email)
        )
      }

      setTimeout(async () => {
        try {
          await this.props.dispatch(initUser())
        } catch (err) {
          console.log('[APP] Initing user error: ', err)
        }
      }, 0)
    } catch (err) {
      console.log('[APP] Initing user error: ', err)
    }

    setTimeout(async () => {
      if (!this.props.profile.isEmpty) {
        try {
          await SecureStore.deleteItemAsync('providersState')
          await this.props.dispatch(SettingsActions.syncLocalRemote())
        } catch (err) {
          console.log('err', err)
        }
      }
    }, 2000)
  }

  _removeUser = () => {
    const { firebase } = this.props

    const user = firebase.auth().currentUser

    if (user) {
      CacheManager.clearUserCache(user.uid)

      user
        .delete()
        .then(function() {
          console.log('[APP] User has been removed from Firebase Auth')
        })
        .catch(function(err) {
          console.log('[APP] Failed to remove user from Firebase Auth', err)

          firebase.logout()
        })
    }
  }

  _handleLogin = () => {
    console.log('\n[APP] Auth login')

    if (!this.authLoadedWithReload) {
      // this._goTo('/~home')
      // Hack!!! Fix me!!!
      this.props.dispatch(RouterActions.resetToUrl(['/~home']))
    }
  }

  _handleLogout = () => {
    console.log('\n[APP] User logout')

    this.props.dispatch(RouterActions.resetToUrl(['/auth-login']))

    this._unWatchProfileData(this.props.auth)

    this.props.dispatch(ClockActions.stop())

    this.authLoadedWithReload = false
  }

  _verifyUser = () => {
    console.log('\n[APP] Verifying user...')

    const {
      auth: { emailVerified, lastLoginAt, createdAt },
    } = this.props

    if (!emailVerified) {
      console.log('\n[APP] User email is not verified')

      const createdAtDate = moment.unix(Number(createdAt) / 1000)
      const now = moment()
      const currentDay = Math.ceil(
        moment.duration(now.diff(createdAtDate)).asDays()
      )

      console.log('\n[APP] Current day from account created: ', currentDay)

      if (currentDay > AUTH__REMOVE_NOT_VERIFIED_ACCOUNT_DAY) {
        // REMOVE ACCOUNT
        console.log(
          '\n[APP] Current day is after the day to remove not verified account'
        )
        console.log('\n[APP] Removing user profile...')

        //
        // TODO implement remove account api
        //
        /*this.props.firebase
          .auth()
          .currentUser.delete()
          .then(() => {
            // firebase
            //   .database()
            //   .ref('users')
            //   .child(currentUser.uid)
            //   .remove()
          })
          .catch(err => {
            console.info(err)

            this.props.firebase.logout()
          })*/
      } else if (AUTH__EMAIL_VERIFICATION_DAYS.indexOf(currentDay) !== -1) {
        // TODAY IS EMAIL VEFIFICATION DAY
        console.log('\n[APP] Today is email vefification day')

        const lastLoginDate = moment.unix(Number(lastLoginAt) / 1000)

        const daysFromLastLogin = Math.floor(
          moment.duration(now.diff(lastLoginDate)).asDays()
        )

        if (daysFromLastLogin > 0 || createdAt === lastLoginAt) {
          console.log(
            '\n[APP] This is first login in current day or first time user signs in'
          )

          setTimeout(() => {
            if (this.props.firebase.auth().currentUser) {
              this._sendEmailVerification()
            }
          }, 2000)
        } else {
          console.log('\n[APP] This is not first login in current day')
        }
      }
    }
  }

  _sendEmailVerification = () => {
    console.log('\n[APP] Auth send verification email')

    if (!__DEV__) {
      if (this.props.locale) {
        this.props.firebase.auth().languageCode = this.props.locale
      }

      this.props.firebase.auth().currentUser.sendEmailVerification()
    }
  }

  _getScreenProps = () =>
    this._getMemoizedScreenProps(this.props.profile, this.props.locale)

  _getMemoizedScreenProps = memoize((profile, locale) => ({
    profile,
    locale,
  }))

  _goTo = (routeName, params) => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      })
    )
  }
}
