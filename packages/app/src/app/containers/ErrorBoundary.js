/**
 *
 * ErrorBoundary
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import Sentry from 'sentry-expo'
import { Constants } from 'expo'

import * as RouterActions from '@/app/redux/router/actions'

export default class ErrorBoundary extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    auth: PropTypes.object,
    children: PropTypes.any,
  }

  render() {
    return this.props.children
  }

  componentDidCatch(error, errorInfo) {
    console.log('[ERROR BOUNDARY] Error!')

    const { dispatch, auth } = this.props

    if (/Error: There is no route defined for key/g.test(error.message)) {
      if (auth.isEmpty) {
        dispatch(RouterActions.resetToNonAuthenitcatedInitialRoute())
      } else {
        dispatch(RouterActions.resetToAuthenitcatedInitialRoute())
      }
    } else if (errorInfo && errorInfo.componentStack) {
      const componentsMatch = errorInfo.componentStack.match(/[^\n]+/g)

      if (componentsMatch && !/ErrorScreen/g.test(componentsMatch[0])) {
        dispatch(RouterActions.resetToErrorScreen())
      } else if (auth.isEmpty) {
        dispatch(RouterActions.resetToNonAuthenitcatedInitialRoute())
      } else {
        dispatch(RouterActions.resetToAuthenitcatedInitialRoute())
      }
    } else if (auth.isEmpty) {
      dispatch(RouterActions.resetToNonAuthenitcatedInitialRoute())
    } else {
      dispatch(RouterActions.resetToAuthenitcatedInitialRoute())
    }

    if (!__DEV__) {
      Sentry.captureException(error, {
        extra: errorInfo,
        tags: {
          'release-channel': Constants.manifest.releaseChannel,
          environment:
            Constants.manifest.releaseChannel === 'default' ||
            Constants.manifest.releaseChannel === 'prod-v1'
              ? 'production'
              : 'staging',
          'user-id': auth.uid || 'unauthorized',
        },
      })
    }
  }
}
