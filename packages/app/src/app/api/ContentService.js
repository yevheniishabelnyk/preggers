import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of'
import 'rxjs/add/observable/fromPromise'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/timeout'
import 'rxjs/add/operator/retry'

import { ADS_FETCH_TIMEOUT_MS } from '@/app/redux/ads/constants'

import Sentry from 'sentry-expo'

export const markInteraction = (ad, type, source = false) => {
  const allowedTypes = ['impression', 'click', 'conversion']

  if (ad && allowedTypes.includes(type) && ad[`${type}Endpoint`]) {
    let url = ad[`${type}Endpoint`]

    if (source) {
      url = url.replace('__SOURCE__', source)
    }

    Observable.fromPromise(fetch(url))
      .timeout(ADS_FETCH_TIMEOUT_MS)
      .retry(3)
      .subscribe(
        next => {
          if (!next.ok) {
            console.log('[ADS] Error when marking interaction', next.status)
          } else {
            console.log(
              `[ADS] Successfully marked ${type} interaction with ${ad.uid}`
            )
          }
        },
        error => {
          console.log(
            `[ADS] Error when marking ${type} interaction with ${ad.uid}`,
            error
          )

          if (!__DEV__) {
            Sentry.captureException(error, { extra: { errorInfo: error } })
          }
        }
      )
  }
}
