import { Api } from './Api'
import * as firebase from 'firebase'

export class ConnectApi extends Api {
  static async sendInvitation({
    email,
    relationshipType,
    shareContractionTimer,
    shareTimeline,
    shareChecklist,
    shareBabyNames,
  }) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/connect/send-invitation', token)
      .send({
        email,
        relationshipType,
        shareContractionTimer,
        shareTimeline,
        shareChecklist,
        shareBabyNames,
      })
      .catch(this.catch)
  }

  static async cancelConnection(connectionId) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/connect/cancel-connection', token)
      .send({ connectionId })
      .catch(this.catch)
  }

  static async cancelInvitation(invitationId) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/connect/cancel-invitation', token)
      .send({ invitationId })
      .catch(this.catch)
  }

  static async declineInvitation(invitationId) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/connect/decline-invitation', token)
      .send({
        invitationId,
      })
      .catch(this.catch)
  }

  static async acceptInvitation(invitationId, code) {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/connect/accept-invitation', token)
      .send({
        invitationId,
        code,
      })
      .catch(this.catch)
  }
}
