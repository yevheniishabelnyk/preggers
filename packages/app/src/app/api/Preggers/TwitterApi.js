import { Api } from './Api'
import * as firebase from 'firebase'
import { API__URL } from 'config'

export class TwitterApi extends Api {
  static getAuthUrl(callbackUrl) {
    return this.post('/twitter/get-auth-url').send({ callbackUrl })
  }

  static getAccessToken(oauthVerifier, oauthToken, oauthSecretToken) {
    return this.post('/twitter/get-access-token').send({
      oauthVerifier,
      oauthToken,
      oauthSecretToken,
    })
  }

  static async postTweet({ status, accessTokenKey, accessTokenSecret, file }) {
    const token = await firebase.auth().currentUser.getIdToken()

    let formData = new FormData()

    formData.append('photo', file)
    formData.append('status', status)
    formData.append('accessTokenKey', accessTokenKey)
    formData.append('accessTokenSecret', accessTokenSecret)

    let options = {
      method: 'POST',
      body: formData,
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    }

    return fetch(`${API__URL}/twitter/post-tweet`, options).then(response =>
      response.json()
    )
  }
}
