import { Api } from './Api'
import * as firebase from 'firebase'

export class MailingApi extends Api {
  static async sendNewUserWelcomeEmail() {
    const token = await firebase.auth().currentUser.getIdToken()

    return this.post('/mailing/new-user', token)
      .send({})
      .catch(this.catch)
  }
}
