import { Api } from './Api'
import * as firebase from 'firebase'
import { API__URL } from 'config'
import moment from 'moment'

export class ImageApi extends Api {
  static async upload(uri) {
    const token = await firebase.auth().currentUser.getIdToken()

    const url = `${API__URL}/image/upload`

    let fileType = 'jpg'
    let uriParts = uri.split('/')

    let fileName = uriParts[uriParts.length - 1]
    fileName = fileName.split('.').reduce((out, str, index) => {
      if (index === 0) {
        return out + str + ' ' + moment().toISOString()
      }

      if (index === 1) {
        fileType = str
        return out + '.' + str
      }

      return out
    }, '')

    let formData = new FormData()

    formData.append('photo', {
      uri,
      name: fileName,
      type: `image/${fileType}`,
    })

    let options = {
      method: 'POST',
      body: formData,
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    }

    return fetch(url, options)
      .then(response => response.json())
      .then(meta => {
        return firebase
          .storage()
          .ref(meta.name)
          .getDownloadURL()
      })
  }
}
