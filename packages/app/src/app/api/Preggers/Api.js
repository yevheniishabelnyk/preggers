import superagent from 'superagent'
import { API__URL } from 'config'

export class Api {
  constructor() {
    this.accessToken = null
    this.apiUrl = API__URL
  }

  static setAccessToken(accessToken) {
    this.accessToken = accessToken
  }

  static unsetAccessToken() {
    this.accessToken = null
  }

  static setApiUrl(apiUrl) {
    this.apiUrl = apiUrl
  }

  static getUrl(path) {
    let url

    if (this.apiUrl) {
      url = `${this.apiUrl}${path}`
    } else {
      url = `${API__URL}${path}`
    }

    return url
  }

  static getClient(method, url, token) {
    if (token) {
      return superagent(method, url).set('Authorization', `Bearer ${token}`)
    }
    return superagent(method, url)
  }

  static get(path, token) {
    return this.getClient('get', this.getUrl(path), token)
  }

  static post(path, token) {
    return this.getClient('post', this.getUrl(path), token)
  }

  static put(path, token) {
    return this.getClient('put', this.getUrl(path), token)
  }

  static delete(path, token) {
    return this.getClient('delete', this.getUrl(path), token)
  }

  static catch(err) {
    if (/network is offline/g.test(err.message)) {
      return Promise.reject({
        status: 503,
        code: 'network/offline',
        message: 'Network unavailable',
      })
    }

    return Promise.reject({
      status: err.status,
      code: err.response.body.code,
      message: err.response.body.message,
    })
  }
}
