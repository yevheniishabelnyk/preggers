/*
 *
 * Vimeo constants and helpers
 *
 */

const VIMEO__API_URL = 'http://vimeo.com/api/v2'

const getApiUrl = id => {
  return `${VIMEO__API_URL}/video/${id}.json`
}

export const getVideoData = async videoId => {
  const url = getApiUrl(videoId)

  const response = await fetch(url)
  const data = await response.json()

  const videoData = data.find(item => item.id == videoId)

  if (videoData) {
    return Promise.resolve({
      title: videoData.title,
      description: videoData.description,
      thumb: {
        url: videoData.thumbnail_large,
      },
      duration: videoData.duration,
    })
  }
  return Promise.reject()
}
