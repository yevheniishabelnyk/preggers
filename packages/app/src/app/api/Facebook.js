/*
 *
 * Facebook constants and helpers
 *
 */

export const getUserData = async (access_token, fields) => {
  try {
    const response = await fetch(
      `https://graph.facebook.com/v2.9/me?access_token=${access_token}&fields=${fields}`
    )

    if (response.ok) {
      return response.json()
    }
    return Promise.reject(response)
  } catch (err) {
    console.log('err', err)

    return Promise.reject(err)
  }
}

export const checkFBToken = async (token, appId) => {
  try {
    const response = await fetch(
      `https://graph.facebook.com/oauth/access_token_info?client_id=${appId}&access_token=${token}`
    )

    const result = await response.json()

    if (result.error) {
      return Promise.reject(result.error)
    }
  } catch (err) {
    console.log('err', err)

    return Promise.reject(err)
  }

  return Promise.resolve()
}
