/*
 *
 * Youtube constants and helpers
 *
 */

import { YOUTUBE__API_KEY } from 'config'
import toQueryString from '@/shared/utils/toQueryString'

const YOUTUBE__API_URL = 'https://www.googleapis.com/youtube/v3'

const defaultFields = [
  'items/snippet/title',
  'items/snippet/description',
  'items/snippet/thumbnails/standard',
  'items/contentDetails/duration',
]

const getApiUrl = (
  id,
  part = 'snippet,contentDetails',
  fields = defaultFields,
  ...rest
) => {
  const queryString = toQueryString({
    part,
    id,
    fields,
    key: YOUTUBE__API_KEY,
    ...rest,
  })

  return `${YOUTUBE__API_URL}/videos${queryString}`
}

export const getVideoData = async videoId => {
  const url = getApiUrl(videoId)

  const response = await fetch(url)
  const data = await response.json()
  // console.info('data: ', data)
  return Promise.resolve({
    title: data.items[0].snippet.title,
    description: data.items[0].snippet.description,
    thumb:
      data.items[0].snippet.thumbnails &&
      data.items[0].snippet.thumbnails.standard
        ? data.items[0].snippet.thumbnails.standard
        : null,
    duration: data.items[0].contentDetails.duration,
  })
}

export const getRandomVideoIds = async (query = 'pregnancy', count = 1) => {
  const urlQueries = {
    maxResults: count,
    key: YOUTUBE__API_KEY,
    fields: ['items/id/videoId'],
    type: 'video',
    q: query,
    part: 'id',
  }

  const url = `${YOUTUBE__API_URL}/search${toQueryString(urlQueries)}`

  const response = await fetch(url)
  const data = await response.json()

  const ids = data.items.map(item => item.id.videoId)

  return Promise.resolve(ids)
}
