/**
 *
 * User
 *
 */
import Expo from 'expo'
import { Platform } from 'react-native'
import { ServerTimestamp } from './shared'
import * as UserTypes from '@/app/constants/UserTypes'
import getUTCOffset from '@/shared/utils/getUTCOffset'

export default class User {
  constructor(fields) {
    if (!fields.id || !fields.email) {
      throw new Error('Missing required fields in User data!')
    }

    this.id = fields.id
    this.email = fields.email
    this.name = fields.name || ''
    this.UTCOffset = fields.UTCOffset || getUTCOffset()
    this.type = fields.type || UserTypes.MOTHER
    this.createdAt = ServerTimestamp
    this.updatedAt = ServerTimestamp

    if (fields.appLocale) {
      this.appLocale = fields.appLocale
    }

    if (fields.facebookId) {
      this.facebookId = fields.facebookId
    }

    if (fields.gender) {
      this.gender = fields.gender
    }

    if (fields.image) {
      this.image = fields.image
    }

    if (fields.location) {
      this.location = fields.location
    }

    if (fields.device) {
      this.device = fields.device
    } else {
      this.device =
        Platform.OS === 'ios' ? Expo.Constants.platform.ios.model : Platform.OS

      if (!this.device) {
        this.device = null
      }
    }
  }
}

User.hasPartnerType = function(profile) {
  return (
    profile.type !== UserTypes.MOTHER &&
    profile.type !== UserTypes.SINGLE_MOTHER
  )
}
