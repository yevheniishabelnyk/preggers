/**
 *
 * Child
 *
 */

import { ServerTimestamp } from './shared'

export default class Child {
  constructor(fields) {
    if (!fields.id || !fields.createdBy) {
      throw new Error('Missing required fields in Child data!')
    }

    this.id = fields.id
    this.createdBy = fields.createdBy
    this.name = fields.name || 'My baby'
    this.createdAt = ServerTimestamp
    this.updatedAt = ServerTimestamp

    if (fields.birthday) {
      this.birthday = fields.birthday
    }

    if (fields.birthTime) {
      this.birthTime = fields.birthTime
    }

    if (fields.birthWeight) {
      this.birthWeight = fields.birthWeight
    }

    if (fields.birthLength) {
      this.birthLength = fields.birthLength
    }

    if (fields.birthPhoto) {
      this.birthPhoto = fields.birthPhoto
    }

    if (fields.gender) {
      this.gender = fields.gender
    }

    if (fields.image) {
      this.image = fields.image
    }

    if (fields.pregnancy) {
      this.pregnancy = fields.pregnancy
    }
  }
}
