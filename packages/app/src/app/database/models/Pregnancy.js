/**
 *
 * Pregnancy
 *
 */

import { ServerTimestamp } from './shared'
import moment from 'moment'
import memoize from 'fast-memoize'
import { PREGNANCY__DEFAULT_BABY_NAME } from '@/app/constants/BabyNames'
import PregnancyConstants from '@/app/constants/Pregnancy'

export default class Pregnancy {
  constructor(fields) {
    if (!fields.id || !fields.createdBy) {
      throw new Error('Missing required fields in Pregnancy data!')
    }

    this.id = fields.id
    this.createdBy = fields.createdBy
    this.childName = fields.childName || PREGNANCY__DEFAULT_BABY_NAME
    this.status = fields.status || 'active'
    this.length = fields.length || PregnancyConstants.LENGTH
    this.lengthType = fields.length || '39+6'
    this.createdAt = ServerTimestamp
    this.updatedAt = ServerTimestamp
    this.dueDate =
      fields.dueDate ||
      moment()
        .add(279, 'days')
        .format('YYYY-MM-DD')

    if (fields.childGender) {
      this.childGender = fields.childGender
    }

    if (fields.birthday) {
      this.birthday = fields.birthday
    }

    if (fields.birthTime) {
      this.birthTime = fields.birthTime
    }

    if (fields.lossDay) {
      this.lossDay = fields.lossDay
    }
  }
}

Pregnancy.LENGTH = PregnancyConstants.LENGTH

Pregnancy.makeGetProgress = function() {
  return memoize(Pregnancy.getProgress)
}

Pregnancy.getProgress = function(dueDate, lengthType, animationValue, full) {
  const value = animationValue || 1
  const result = {}

  const nowDate = moment().startOf('day')
  const dueDateDate = moment(dueDate, 'YYYY-MM-DD')

  result.leftDays = Math.round(
    moment.duration(dueDateDate.diff(nowDate)).asDays()
  )

  result.passedDays = Math.abs(PregnancyConstants.LENGTH - result.leftDays)

  switch (lengthType) {
    case '39+6':
      result.countByPregnancyLengthType =
        result.passedDays - 1 < 0 ? 0 : result.passedDays - 1
      result.calendarMonthParametr = 14
      result.pregnancyWeekParametr = 1
      break
    case '40+0':
      result.countByPregnancyLengthType = result.passedDays
      result.calendarMonthParametr = 13
      result.pregnancyWeekParametr = 0
      break
    default:
      result.countByPregnancyLengthType =
        result.passedDays - 1 < 0 ? 0 : result.passedDays - 1
      result.calendarMonthParametr = 14
      result.pregnancyWeekParametr = 1
  }

  result.pregnancyWeek =
    Math.floor(
      ((result.passedDays - result.pregnancyWeekParametr) * value) / 7
    ) + 1

  result.passedWeeks = Math.floor(
    (result.countByPregnancyLengthType * value) / 7
  )
  result.passedWeeksDays = Math.round(
    ((result.countByPregnancyLengthType * value) / 7 - result.passedWeeks) * 7
  )

  if (full) {
    result.calendarMonth = Math.ceil(
      ((result.passedDays - result.calendarMonthParametr) * value) / 30
    )

    result.pregnancyMonth = Math.ceil((result.pregnancyWeek * value) / 4)

    if (result.pregnancyWeek <= 14) {
      result.trimester = Math.floor(1 * value)
    } else if (result.pregnancyWeek <= 28) {
      result.trimester = Math.floor(2 * value)
    } else {
      result.trimester = Math.floor(3 * value)
    }
  }

  return result
}
