import flatten from '@/shared/utils/flatten'
import { ServerTimestamp } from '@/app/database/models/shared'

export const addToOwned = checklistItem => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const profile = getState().firebase.profile

  const checklistItemId = firebase
    .database()
    .ref(`/userOwned/checklistItems/${profile.id}`)
    .push().key

  const data = Object.assign(checklistItem, {
    id: checklistItemId,
    createdBy: profile.id,
    createdAt: ServerTimestamp,
    updatedAt: ServerTimestamp,
  })

  const databaseDiffObj = {
    userOwned: {
      checklistItems: {
        [profile.id]: {
          [checklistItemId]: data,
        },
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const updateOwned = (oldData, newData) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()

  let todos

  if (oldData.todos && newData.todos) {
    const removedTodos = Object.keys(oldData.todos).reduce((out, todo) => {
      if (typeof newData.todos[todo] === 'undefined') {
        out[todo] = null
      }

      return out
    }, {})

    todos = Object.assign(newData.todos, removedTodos)
  } else if (!oldData.todos) {
    todos = newData.todos
  } else {
    todos = oldData.todos
  }

  const data = Object.assign(newData, { todos, updatedAt: ServerTimestamp })

  const databaseDiffObj = {
    userOwned: {
      checklistItems: {
        [oldData.createdBy]: {
          [oldData.id]: data,
        },
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  console.info('databaseDiff: ', databaseDiff)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const toggleCompletedTodo = (todo, checklistItem) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const { profile } = getState().firebase

  const isAdminType = checklistItem.createdBy !== profile.id

  let nextValue, path

  if (isAdminType) {
    path = `/userOwned/completedAdminTodosByUser/${profile.id}/${todo.id}`
    nextValue = todo.completed ? null : true
  } else {
    path = `/userOwned/checklistItems/${profile.id}/${checklistItem.id}/todos/${
      todo.id
    }`

    nextValue = !todo.completed
  }

  firebase.set(path, nextValue)
}

export const completeAdminTodo = todoId => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const { profile } = getState().firebase
  const path = `/userOwned/completedAdminTodosByUser/${profile.id}/${todoId}`
  firebase.set(path, true)
}
