/**
 *
 * Pregnancies actions
 *
 */

import flatten from '@/shared/utils/flatten'
import { ServerTimestamp } from '@/app/database/models/shared'

export const addToFavorites = babyname => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const profile = getState().firebase.profile

  let databaseDiffObj

  if (babyname.createdBy) {
    const stars = babyname.stars
      ? Object.assign(babyname.stars, {
          [profile.id]: true,
        })
      : {
          [profile.id]: true,
        }

    databaseDiffObj = {
      userOwned: {
        babyNames: {
          [profile.id]: {
            [babyname.id]: {
              ...babyname,
              stars,
              createdAt: ServerTimestamp,
              updatedAt: ServerTimestamp,
            },
          },

          [profile.partner]: {
            [babyname.id]: {
              stars,
            },
          },
        },
      },
    }
  } else {
    databaseDiffObj = {
      content: {
        countryBabyNames: {
          [babyname.id]: {
            rank: babyname.rank ? babyname.rank + 1 : 1,
            stars: {
              [profile.id]: true,
            },
          },
        },
      },

      babynameLikesByUser: {
        [profile.id]: {
          [babyname.id]: true,
        },
      },
    }
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('databaseDiff: ', databaseDiff)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const removeFromFavorites = babyname => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const profile = getState().firebase.profile

  const databaseDiffObj = {
    content: {
      countryBabyNames: {
        [babyname.id]: {
          rank: babyname.rank && babyname.rank - 1 >= 0 ? babyname.rank - 1 : 0,
          stars: {
            [profile.id]: null,
          },
        },
      },
    },

    babynameLikesByUser: {
      [profile.id]: {
        [babyname.id]: null,
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('databaseDiff: ', databaseDiff)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const addToOwned = (name, gender) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const profile = getState().firebase.profile

  const babynameId = firebase
    .database()
    .ref(`/userOwned/babyNames/${profile.id}`)
    .push().key

  const databaseDiffObj = {
    userOwned: {
      babyNames: {
        [profile.id]: {
          [babynameId]: {
            id: babynameId,
            name: name,
            gender,
            stars: {
              [profile.id]: true,
            },
            createdBy: profile.id,
            createdAt: ServerTimestamp,
            updatedAt: ServerTimestamp,
          },
        },
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('databaseDiff: ', databaseDiff)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const removeFromOwned = babyname => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()
  const profile = getState().firebase.profile

  const babynamesDiffObj = {
    [profile.id]: {
      [babyname.id]: null,
    },
  }

  if (profile.partner) {
    babynamesDiffObj[profile.partner] = {
      [babyname.id]: {
        stars: {
          [profile.id]: null,
        },
      },
    }
  }

  const databaseDiffObj = {
    userOwned: {
      babyNames: babynamesDiffObj,
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('databaseDiff: ', databaseDiff)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}
