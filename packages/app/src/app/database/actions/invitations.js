/**
 *
 * Invitations actions
 *
 */

import flatten from '@/shared/utils/flatten'
import { ConnectApi } from '@/app/api/Preggers'

export const updateStatus = (invitation, status) => (
  dispatch,
  getState,
  getFirebase
) => {
  if (invitation && invitation.status !== status) {
    const { profile } = getState().firebase

    if (
      invitation.toId === profile.id ||
      invitation.toEmail === profile.email
    ) {
      const firebase = getFirebase()

      const databaseDiffObj = {
        invitations: {
          [invitation.id]: {
            status,
          },
        },

        userOwned: {
          invitations: {
            [profile.id]: {
              [invitation.id]: status,
            },
          },
        },
      }

      const databaseDiff = flatten(databaseDiffObj)

      return firebase.update('/', databaseDiff)
    }
  }

  return Promise.resolve()
}

export const accept = invitation => async (dispatch, getState, getFirebase) => {
  const firebase = getFirebase()

  try {
    const response = await ConnectApi.acceptInvitation(invitation.id)

    const { connectionId, pregnancyId } = response.body

    await firebase.updateProfile({
      connection: connectionId,
      pregnancy: pregnancyId,
    })
  } catch (err) {
    console.info('err: ', err)

    return Promise.reject(err)
  }
}

export const decline = invitation => () => {
  return ConnectApi.declineInvitation(invitation.id)
}
