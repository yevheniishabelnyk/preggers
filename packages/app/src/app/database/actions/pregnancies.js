/**
 *
 * Pregnancies actions
 *
 */

import flatten from '@/shared/utils/flatten'
import Child from '@/app/database/models/Child'
import Pregnancy from '@/app/database/models/Pregnancy'
import { ServerTimestamp } from '@/app/database/models/shared'

export const update = (pregnancy, data) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const profile = getState().firebase.profile
  const firebase = getFirebase()

  const databaseDiffObj = {
    pregnancies: {
      [pregnancy.id]: data,
    },

    pregnanciesByUser: {
      [profile.id]: {
        [pregnancy.id]: data.childName,
      },
    },
  }

  if (data.childName) {
    databaseDiffObj.pregnanciesByUser = {
      [profile.id]: {
        [pregnancy.id]: data.childName,
      },
    }

    if (profile.partner) {
      Object.assign(databaseDiffObj.pregnanciesByUser, {
        [profile.partner]: {
          [pregnancy.id]: data.childName,
        },
      })
    }
  }

  if (profile.partner) {
    const updatedValues = {}

    if (!pregnancy.childGender && data.childGender) {
      updatedValues.childGender = data.childGender
    } else if (pregnancy.childGender && !data.childGender) {
      updatedValues.childGender = 'unknown'
    } else if (data.childGender && pregnancy.childGender !== data.childGender) {
      updatedValues.childGender = data.childGender
    }

    if (pregnancy.childName !== data.childName) {
      updatedValues.childName = data.childName
    }

    if (pregnancy.dueDate !== data.dueDate) {
      updatedValues.dueDate = data.dueDate
    }

    if (Object.keys(updatedValues).length > 0) {
      const notificationId = firebase
        .database()
        .ref(`userOwned/notifications/${profile.partner}`)
        .push().key

      databaseDiffObj.userOwned = {
        notifications: {
          [profile.partner]: {
            [notificationId]: {
              id: notificationId,
              notificationType: 'child-updated',
              status: 'unread',
              payload: {
                updatedValues,
                changedByUser: profile.name,
              },
              createdAt: ServerTimestamp,
              updatedAt: ServerTimestamp,
            },
          },
        },
      }
    }
  }

  // console.info('databaseDiffObj: ', databaseDiffObj)

  const databaseDiff = flatten(databaseDiffObj)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve(data)
  } catch (err) {
    return Promise.reject(err)
  }
}

export const childIsBorn = (childData, pregnancy) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const profile = getState().firebase.profile
  const firebase = getFirebase()

  const childRef = firebase.push('children')

  try {
    const child = new Child(
      Object.assign(childData, {
        id: childRef.key,
        pregnancy: pregnancy.id,
        createdBy: profile.id,
      })
    )

    const databaseDiffObj = {
      children: {
        [child.id]: child,
      },

      pregnancies: {
        [pregnancy.id]: {
          status: 'child_is_born',
          birthday: child.birthday,
          birthTime: child.birthTime,
          child: child.id,
          childName: child.name,
          childGender: child.gender || null,
          birthPhoto: child.image || null,
          updatedAt: ServerTimestamp,
        },
      },

      pregnanciesByUser: {
        [profile.id]: {
          [pregnancy.id]: child.name,
        },
      },

      usersByChild: {
        [child.id]: {
          [profile.id]: profile.name,
        },
      },

      childrenByUser: {
        [profile.id]: {
          [child.id]: child.name,
        },
      },

      users: {
        [profile.id]: {
          pregnancy: null,
          updatedAt: ServerTimestamp,
        },
      },
    }

    if (profile.partner) {
      Object.assign(databaseDiffObj.pregnanciesByUser, {
        [profile.partner]: {
          [pregnancy.id]: child.name,
        },
      })

      Object.assign(databaseDiffObj.users, {
        [profile.partner]: {
          pregnancy: null,
          updatedAt: ServerTimestamp,
        },
      })

      const {
        firebase: { data },
      } = getState()

      if (data.connections && data.connections[profile.connection]) {
        const partnerName = data.connections[profile.connection].toName

        Object.assign(databaseDiffObj.usersByChild[child.id], {
          [profile.partner]: partnerName,
        })
      }

      const notificationId = firebase
        .database()
        .ref(`userOwned/notifications/${profile.partner}`)
        .push().key

      databaseDiffObj.userOwned = {
        notifications: {
          [profile.partner]: {
            [notificationId]: {
              id: notificationId,
              notificationType: 'child-is-born',
              status: 'unread',
              createdAt: ServerTimestamp,
              updatedAt: ServerTimestamp,
            },
          },
        },
      }
    }

    // console.info('databaseDiffObj: ', databaseDiffObj)

    const databaseDiff = flatten(databaseDiffObj)

    // console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

    await firebase.update('/', databaseDiff)

    return Promise.resolve(child)
  } catch (err) {
    console.info('err: ', err)

    return Promise.reject(err)
  }
}

export const remove = pregnancyId => async (
  dispatch,
  getState,
  getFirebase
) => {
  const profile = getState().firebase.profile
  const firebase = getFirebase()

  const databaseDiffObj = {
    pregnancies: {
      [pregnancyId]: null,
    },

    usersByPregnancy: {
      [pregnancyId]: null,
    },

    pregnanciesByUser: {
      [profile.id]: {
        [pregnancyId]: null,
      },
    },

    users: {
      [profile.id]: {
        pregnancy: null,
      },
    },
  }

  if (profile.partner) {
    Object.assign(databaseDiffObj.pregnanciesByUser, {
      [profile.partner]: {
        [pregnancyId]: null,
      },
    })

    Object.assign(databaseDiffObj.users, {
      [profile.partner]: {
        pregnancy: null,
        updatedAt: ServerTimestamp,
      },
    })

    const notificationId = firebase
      .database()
      .ref(`userOwned/notifications/${profile.partner}`)
      .push().key

    databaseDiffObj.userOwned = {
      notifications: {
        [profile.partner]: {
          [notificationId]: {
            id: notificationId,
            notificationType: 'pregnancy-deleted',
            status: 'unread',
            payload: {
              endedBy: profile.name,
            },
            createdAt: ServerTimestamp,
            updatedAt: ServerTimestamp,
          },
        },
      },
    }
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const cancel = (pregnancyId, lossDay) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const profile = getState().firebase.profile
  const firebase = getFirebase()

  const databaseDiffObj = {
    pregnancies: {
      [pregnancyId]: {
        status: 'canceled',
        lossDay,
      },
    },

    users: {
      [profile.id]: {
        pregnancy: null,
        updatedAt: ServerTimestamp,
      },
    },
  }

  if (profile.partner) {
    Object.assign(databaseDiffObj.users, {
      [profile.partner]: {
        pregnancy: null,
        updatedAt: ServerTimestamp,
      },
    })

    const notificationId = firebase
      .database()
      .ref(`userOwned/notifications/${profile.partner}`)
      .push().key

    databaseDiffObj.userOwned = {
      notifications: {
        [profile.partner]: {
          [notificationId]: {
            id: notificationId,
            notificationType: 'pregnancy-ended',
            status: 'unread',
            payload: {
              endedBy: profile.name,
            },
            createdAt: ServerTimestamp,
            updatedAt: ServerTimestamp,
          },
        },
      },
    }
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const add = ({
  childName,
  childGender,
  dueDate,
  pregnancyLengthType,
}) => async (dispatch, getState, getFirebase) => {
  const profile = getState().firebase.profile
  const firebase = getFirebase()

  const pregnancyRef = firebase.push('pregnancies')

  const pregnancyId = pregnancyRef.key

  const data = {
    id: pregnancyId,
    createdBy: profile.id,
  }

  if (childName) {
    data.childName = childName
  }

  if (dueDate) {
    data.dueDate = dueDate
  }

  if (childGender) {
    data.childGender = childGender
  }

  if (pregnancyLengthType) {
    data.lengthType = pregnancyLengthType
  }

  const pregnancyData = new Pregnancy(data)

  const databaseDiffObj = {
    pregnancies: {
      [pregnancyId]: pregnancyData,
    },

    usersByPregnancy: {
      [pregnancyId]: {
        [profile.id]: profile.name,
      },
    },

    pregnanciesByUser: {
      [profile.id]: {
        [pregnancyId]: pregnancyData.childName,
      },
    },

    users: {
      [profile.id]: {
        pregnancy: pregnancyId,
        updatedAt: ServerTimestamp,
      },
    },
  }

  if (profile.partner) {
    Object.assign(databaseDiffObj.pregnanciesByUser, {
      [profile.partner]: {
        [pregnancyId]: pregnancyData.childName,
      },
    })

    Object.assign(databaseDiffObj.users, {
      [profile.partner]: {
        pregnancy: pregnancyId,
        updatedAt: ServerTimestamp,
      },
    })

    Object.assign(databaseDiffObj, {
      permissions: {
        pregnancies: {
          [pregnancyId]: {
            [profile.partner]: {
              level: 10,
              createdBy: profile.id,
              updatedAt: ServerTimestamp,
              createdAt: ServerTimestamp,
            },
          },
        },
      },
    })

    const {
      firebase: { data },
    } = getState()

    if (data.connections && data.connections[profile.connection]) {
      const partnerName = data.connections[profile.connection].toName

      Object.assign(databaseDiffObj.usersByPregnancy[pregnancyId], {
        [profile.partner]: partnerName,
      })
    }
  }

  // console.info('databaseDiffObj: ', databaseDiffObj)

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

  try {
    await firebase.update('/', databaseDiff)

    await firebase.updateProfile({ pregnancy: pregnancyId })

    return Promise.resolve(pregnancyData)
  } catch (err) {
    console.info('err: ', err)
    return Promise.reject(err)
  }
}
