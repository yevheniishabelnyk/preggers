/**
 *
 * Children actions
 *
 */

import Child from '@/app/database/models/Child'
import flatten from '@/shared/utils/flatten'
import * as FirebaseStorage from '@/app/firebase/storage'
import { CacheManager } from '@/vendor/cache-manager'
import cropImage from '@/shared/utils/cropImage'

export const add = (childData = {}) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const {
    firebase: { profile },
  } = getState()
  const firebase = getFirebase()

  const childRef = firebase.push('children')

  try {
    const child = new Child(
      Object.assign(childData, { id: childRef.key, createdBy: profile.id })
    )

    const databaseDiffObj = {
      children: {
        [child.id]: child,
      },

      usersByChild: {
        [child.id]: {
          [profile.id]: profile.name,
        },
      },

      childrenByUser: {
        [profile.id]: {
          [child.id]: childData.name,
        },
      },
    }

    const databaseDiff = flatten(databaseDiffObj)

    // console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

    await firebase.update('/', databaseDiff)

    return Promise.resolve(child)
  } catch (err) {
    console.info('err: ', err)

    return Promise.reject(err)
  }
}

export const update = (childId, updatedData) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const {
    firebase: { profile },
  } = getState()
  const firebase = getFirebase()

  const databaseDiffObj = {
    children: {
      [childId]: updatedData,
    },
  }

  if (updatedData.name) {
    databaseDiffObj.childrenByUser = {
      [profile.id]: {
        [childId]: updatedData.name,
      },
    }
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('databaseDiff: ', databaseDiff)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}

export const updatePicture = (child, image, withBirthPhoto) => async (
  dispatch,
  getState,
  getFirebase
) => {
  const { profile } = getState().firebase
  const firebase = getFirebase()

  try {
    const [remoteUri, previewResult] = await Promise.all([
      FirebaseStorage.putFile(
        firebase,
        image.uri,
        `users/${profile.id}/images`
      ),

      cropImage(image, {
        type: 'square',
        width: 10,
        base64: true,
      }),
    ])

    let oldImageUri

    if (child.image) {
      if (!child.image.uri.startsWith('file')) {
        oldImageUri = child.image.uri
      } else if (child.image.origin) {
        oldImageUri = child.image.origin
      }
    }

    const updatedData = {
      image: {
        type: 'file',
        uri: remoteUri,
        preview: `data:image/jpeg;base64,${previewResult.base64}`,
      },
    }

    if (withBirthPhoto) {
      updatedData.birthPhoto = {
        type: 'file',
        uri: remoteUri,
        preview: `data:image/jpeg;base64,${previewResult.base64}`,
      }
    }

    await dispatch(update(child.id, updatedData))

    if (oldImageUri) {
      FirebaseStorage.deleteFile(firebase, oldImageUri)

      CacheManager.delete(oldImageUri, `users/${profile.id}/images`)
    }
  } catch (err) {
    console.info('err: ', err)
  }
}

export const remove = child => async (dispatch, getState, getFirebase) => {
  const {
    firebase: { profile },
  } = getState()
  const firebase = getFirebase()

  const databaseDiffObj = {
    children: {
      [child.id]: null,
    },

    usersByChild: {
      [child.id]: null,
    },

    childrenByUser: {
      [profile.id]: {
        [child.id]: null,
      },
    },
  }

  if (child.pregnancy) {
    Object.assign(databaseDiffObj, {
      pregnancies: {
        [child.pregnancy]: null,
      },

      usersByPregnancy: {
        [child.pregnancy]: null,
      },

      pregnanciesByUser: {
        [profile.id]: {
          [child.pregnancy]: null,
        },
      },
    })
  }

  const databaseDiff = flatten(databaseDiffObj)

  // console.info('flatten diff: ', JSON.stringify(databaseDiff, null, 4))

  let oldImageUri

  if (child.image) {
    if (!child.image.uri.startsWith('file')) {
      oldImageUri = child.image.uri
    } else if (child.image.origin) {
      oldImageUri = child.image.origin
    }
  }

  try {
    await firebase.update('/', databaseDiff)

    if (oldImageUri) {
      FirebaseStorage.deleteFile(firebase, oldImageUri)

      CacheManager.delete(oldImageUri, `users/${profile.id}/images`)
    }

    return Promise.resolve()
  } catch (err) {
    return Promise.reject(err)
  }
}
