/**
 *
 * User actions
 *
 */
import * as FirebaseStorage from '@/app/firebase/storage'

import User from '@/app/database/models/User'

import { camelizeKeys } from 'humps'
import { has } from 'lodash'
import moment from 'moment'

import * as FacebookApi from '@/app/api/Facebook'
import { REQUIRED_USER_FIELDS as FACEBOOK_REQUIRED_USER_FIELDS } from '@/app/constants/Facebook'

import * as TempActions from '@/app/redux/temp'
import * as ProfileActions from '@/app/redux/profile/actions'

export const initUser = () => async (dispatch, getState, getFirebase) => {
  const {
    session: { signInProvider },
    temp: { facebookAccessToken },
    firebase: {
      auth: { email, uid },
    },
    device: { country: deviceContryCode },
    settings: { locale: appLocale },
  } = getState()
  const firebase = getFirebase()

  let newUserData

  const userId = uid
  const userEmail = email

  if (signInProvider === 'facebook.com' && facebookAccessToken) {
    console.info('[AUTH] Receiving user data from Facebook...')

    let facebookRawData = await FacebookApi.getUserData(
      facebookAccessToken,
      FACEBOOK_REQUIRED_USER_FIELDS
    )

    let image = null

    if (has(facebookRawData, 'picture.data.url')) {
      const downloadUrl = await FirebaseStorage.putFile(
        firebase,
        facebookRawData.picture.data.url,
        `users/${userId}/images`
      )

      image = { type: 'file', uri: downloadUrl }
    }

    facebookRawData = camelizeKeys(facebookRawData)

    if (deviceContryCode) {
      facebookRawData.location = {
        countryCode: deviceContryCode,
      }
    }

    if (email) {
      facebookRawData.email = email
    }

    if (facebookRawData.birthday) {
      facebookRawData.birthday = moment(
        facebookRawData.birthday,
        'MM/DD/YYYY'
      ).format('YYYY-MM-DD')
    }

    console.info('[AUTH] Received user data from Facebook')

    console.info('[AUTH] Applying user model to Facebook data...')

    try {
      newUserData = new User({
        appLocale,
        ...facebookRawData,
        image,
        id: userId,
        facebookId: facebookRawData.id,
        gender: facebookRawData.gender === 'male' ? 'm' : 'f',
      })
    } catch (err) {
      console.warn('[AUTH] Error: Applying user model faild!\n', err)

      return Promise.reject(err)
    }

    console.info(
      '[AUTH] New user data created successfully',
      JSON.stringify(newUserData, null, 4)
    )
  } else if (signInProvider === 'password') {
    console.info('[AUTH] Applying user model...')

    try {
      const data = {
        id: userId,
        email: userEmail,
        appLocale,
      }

      if (deviceContryCode) {
        data.location = {
          countryCode: deviceContryCode,
        }
      }

      newUserData = new User(data)
    } catch (err) {
      console.warn('[AUTH] Error: Applying user model faild!\n', err)

      return Promise.reject(err)
    }

    console.info('[AUTH] New user data created successfully', newUserData)
  }

  if (newUserData) {
    try {
      console.info('[AUTH] Saving user data in database...')

      await firebase.updateProfile(newUserData)

      if (newUserData.image) {
        dispatch(
          ProfileActions.updateFirebaseProfilePhotoUrl(newUserData.image.uri)
        )
      }

      if (newUserData.name) {
        dispatch(
          ProfileActions.updateFirebaseProfileDisplayName(newUserData.name)
        )
      }

      await firebase.set(
        `usersByUTCOffset/${newUserData.UTCOffset}/${newUserData.id}`,
        true
      )

      console.info('[AUTH] User data was successfully saved in the database')

      dispatch(TempActions.remove('facebookAccessToken'))

      return Promise.resolve()
    } catch (e) {
      console.warn('[AUTH] Error: saving user data in database faild!\n', e)

      return Promise.reject(e)
    }
  }
}

export const updateUserWithFacebook = accessToken => async (
  dispatch,
  getState,
  getFirebase
) => {
  const {
    firebase: { auth },
  } = getState()
  const firebase = getFirebase()

  const existingUserDataSnap = await firebase
    .database()
    .ref(`/users/${auth.uid}`)
    .once('value')

  if (existingUserDataSnap.exists()) {
    const existingUserData = existingUserDataSnap.val()

    let facebookRawData = await FacebookApi.getUserData(
      accessToken,
      FACEBOOK_REQUIRED_USER_FIELDS
    )

    facebookRawData = camelizeKeys(facebookRawData)

    const newData = {}

    if (!existingUserData.image && has(facebookRawData, 'picture.data.url')) {
      const downloadUrl = await FirebaseStorage.putFile(
        firebase,
        facebookRawData.picture.data.url,
        `users/${auth.uid}/images`
      )

      newData.image = { type: 'file', uri: downloadUrl }
    }

    if (!existingUserData.birthday && facebookRawData.birthday) {
      newData.birthday = moment(facebookRawData.birthday, 'MM/DD/YYYY').format(
        'YYYY-MM-DD'
      )
    }

    if (!existingUserData.gender && facebookRawData.gender) {
      newData.gender = facebookRawData.gender === 'male' ? 'm' : 'f'
    }

    try {
      await firebase.updateProfile(newData)

      if (newData.image) {
        await dispatch(
          ProfileActions.updateFirebaseProfilePhotoUrl(newData.image.uri)
        )
      }

      console.info('[AUTH] New user data created successfully')
    } catch (err) {
      console.info('[AUTH] Error: Applying user model faild!\n', err)

      return Promise.reject(err)
    }
  } else {
    return Promise.resolve()
  }
}
