/**
 *
 * Database actions
 *
 */

import { actionTypes } from 'react-redux-firebase'

export const updateLocalState = ({ data, path, ...rest }) => ({
  type: actionTypes.SET,
  data,
  path,
  ordered: null,
  ...rest,
})
