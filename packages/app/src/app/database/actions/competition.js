import flatten from '@/shared/utils/flatten'

export const submit = submission => async (dispatch, getState, getFirebase) => {
  const firebase = getFirebase()

  const databaseDiffObj = {
    competitionSubmissions: {
      [submission.id]: submission,
    },

    competitions: {
      competitionList: {
        [submission.competitionUid]: {
          submissions: {
            [submission.id]: true,
          },
        },
      },
    },

    submissionByUser: {
      [submission.userUid]: {
        [submission.id]: true,
      },
    },
  }

  const databaseDiff = flatten(databaseDiffObj)

  try {
    await firebase.update('/', databaseDiff)

    return Promise.resolve()
  } catch (err) {
    console.log('Error: ', err)

    return Promise.reject(err)
  }
}
