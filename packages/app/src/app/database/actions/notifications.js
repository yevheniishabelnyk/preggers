import { ServerTimestamp } from '@/app/database/models/shared'

export const markAsRead = notificationId => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()

  const { profile } = getState().firebase

  const path = `/userOwned/notifications/${profile.id}/${notificationId}`

  firebase.update(path, {
    status: 'read',
    updatedAt: ServerTimestamp,
  })
}

export const markAsDone = notificationId => async (
  dispatch,
  getState,
  getFirebase
) => {
  const firebase = getFirebase()

  const { profile } = getState().firebase

  const path = `/userOwned/notifications/${profile.id}/${notificationId}`

  firebase.update(path, {
    status: 'done',
    updatedAt: ServerTimestamp,
  })
}
