// Reference: https://github.com/github/fetch/issues/175#issuecomment-216791333
const promiseTimeout = (ms, promise) =>
  new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      reject(new Error('promise timeout'))
    }, ms)

    promise
      .then(res => {
        clearTimeout(timer)

        resolve(res)
      })
      .catch(err => {
        clearTimeout(timer)

        reject(err)
      })
  })

export default promiseTimeout
