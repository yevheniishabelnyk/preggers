export const LengthUnit = {
  Centimetre: 'cm',
  Inch: 'in',
}

export const WeightUnit = {
  Gramm: 'g',
  Pound: 'lb',
}

export const AppLocale = {
  English: 'en',
  Swedish: 'sv',
}
