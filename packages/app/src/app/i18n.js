/**
 *
 * i18n
 * This will setup the i18n language files and locale data.
 *
 */

import { addLocaleData } from 'react-intl'
import { translations, localesData } from 'translations'
import config from 'config'

config.APP_LOCALES.forEach(locale => {
  addLocaleData(localesData[locale])
})

export const messages = config.APP_LOCALES.reduce((out, locale) => {
  if (__DEV__ && locale === config.DEFAULT_LOCALE) {
    // Do not use default locale translations to use default messages in development
    // It prevents bugs when you change existing default message and do not see changes in the app as the translation is still old one
    out[locale] = {}
  } else {
    out[locale] = translations[locale] || {}
  }

  return out
}, {})
