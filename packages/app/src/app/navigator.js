/**
 *
 * App navigator
 *
 */

import { StackNavigator } from 'react-navigation'

import { routes, nonAuthenticatedInitialRouteName } from '@/app/routes'

const NavigatorConfig = {
  headerMode: 'none',
  initialRouteName: nonAuthenticatedInitialRouteName,
}

StackNavigator(routes, NavigatorConfig)

export default StackNavigator(routes, NavigatorConfig)
