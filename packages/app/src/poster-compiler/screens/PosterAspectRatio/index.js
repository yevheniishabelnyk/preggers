/**
 *
 * PosterAspectRatio
 *
 */

import React from 'react'

import { View, Dimensions, PixelRatio } from 'react-native'
import { ImageManipulator } from 'expo'
import BaseScreen from '@/app/base/components/BaseScreen'
import AspectRatioSelect from '@/poster-compiler/components/molecules/AspectRatioSelect'
import ImageTelescope from '@/poster-compiler/components/molecules/ImageTelescope'
import PosterCompilerHeader from '@/poster-compiler/components/molecules/PosterCompilerHeader'
import PosterCompilerScreenWrapper from '@/poster-compiler/components/atoms/PosterCompilerScreenWrapper'

import AspectRatios from '@/poster-compiler/containers/Canvas/AspectRatios'

import styles from './styles'

import { previewWidth } from '@/poster-compiler/components/atoms/PhotoFilterPreview/styles'

import { POSTER_WIDTH } from '@/poster-compiler/screens/PosterCompiler/constants'

const { width: canvasWidth } = Dimensions.get('window')
const pixelRatio = PixelRatio.get()

export default class PosterAspectRatio extends BaseScreen {
  state = {
    aspectRatio: AspectRatios.DEFAULT,
    isImageTelescopeLayoutReady: false,
  }

  render() {
    const { aspectRatio, isImageTelescopeLayoutReady } = this.state

    const image = this.props.navigation.getParam('image')

    return (
      <PosterCompilerScreenWrapper style={styles.container}>
        <PosterCompilerHeader
          onCloseButtonPress={this._goBack}
          onNextButtonPress={this._doneButtonPressed}
        />

        <ImageTelescope
          ref={ref => (this._imageTelescope = ref)}
          image={image}
          viewAspectRatio={AspectRatios.DEFAULT}
          aspectRatio={aspectRatio}
          onLayoutReady={this._onImageTelescopeLayoutReady}
        />

        <View
          style={[
            styles.aspectRatioSelectContainer,
            isImageTelescopeLayoutReady ? styles.maxSpace : null,
          ]}
        >
          <AspectRatioSelect
            options={AspectRatios.LIST}
            value={aspectRatio}
            onChange={this._aspectRatioChanged}
          />
        </View>
      </PosterCompilerScreenWrapper>
    )
  }

  _onImageTelescopeLayoutReady = () =>
    this.setState({ isImageTelescopeLayoutReady: true })

  _doneButtonPressed = async () => {
    const { aspectRatio } = this.state

    try {
      const croppedImage = await this._imageTelescope.cropImageToActiveArea()

      console.info('croppedImage.uri: ', croppedImage.uri)
      console.info('croppedImage.width: ', croppedImage.width)
      console.info('croppedImage.height: ', croppedImage.height)

      const backgroundImagePreviewResizedWidth = Math.ceil(
        previewWidth * pixelRatio
      )
      const backgroundImagePreviewResizedHeight = Math.ceil(
        backgroundImagePreviewResizedWidth / aspectRatio
      )

      let backgroundImagePreviewCroppedWidth,
        backgroundImagePreviewCropOriginX,
        backgroundImagePreviewCropOriginY

      if (
        backgroundImagePreviewResizedWidth > backgroundImagePreviewResizedHeight
      ) {
        backgroundImagePreviewCroppedWidth = backgroundImagePreviewResizedHeight

        backgroundImagePreviewCropOriginX = Math.ceil(
          (backgroundImagePreviewResizedWidth -
            backgroundImagePreviewResizedHeight) /
            2
        )
        backgroundImagePreviewCropOriginY = 0
      } else {
        backgroundImagePreviewCroppedWidth = backgroundImagePreviewResizedWidth

        Math.ceil(
          (backgroundImagePreviewCropOriginY =
            (backgroundImagePreviewResizedHeight -
              backgroundImagePreviewResizedWidth) /
            2)
        )
        backgroundImagePreviewCropOriginX = 0
      }

      const [
        canvasBackgroundImage,
        posterBackgroundImage,
        backgroundImagePreview,
      ] = await Promise.all([
        ImageManipulator.manipulate(croppedImage.uri, [
          {
            resize: {
              width: canvasWidth * pixelRatio,
              height: (canvasWidth * pixelRatio) / aspectRatio,
            },
          },
        ]),

        ImageManipulator.manipulate(croppedImage.uri, [
          {
            resize: {
              width: POSTER_WIDTH,
              height: POSTER_WIDTH / aspectRatio,
            },
          },
        ]),

        ImageManipulator.manipulate(croppedImage.uri, [
          {
            resize: {
              width: backgroundImagePreviewResizedWidth,
              height: backgroundImagePreviewResizedHeight,
            },
          },
          {
            crop: {
              originX: backgroundImagePreviewCropOriginX,
              originY: backgroundImagePreviewCropOriginY,
              width: backgroundImagePreviewCroppedWidth,
              height: backgroundImagePreviewCroppedWidth,
            },
          },
        ]),
      ])

      this._goTo('/poster-compiler', {
        aspectRatio,
        canvasBackgroundImage,
        posterBackgroundImage,
        backgroundImagePreview,
      })
    } catch (err) {
      console.log('err', err)
    }
  }

  _aspectRatioChanged = aspectRatio => this.setState({ aspectRatio })
}
