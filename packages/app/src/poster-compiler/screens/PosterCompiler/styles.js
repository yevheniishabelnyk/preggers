import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const canvasContainerBackgroundColor = '#ffffff'

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
    backgroundColor: '#f9fafb',
  },

  header: {
    backgroundColor: '#f9fafb',
  },

  canvasWrapper: {
    backgroundColor: '#f9fafb',
    flex: 1,
    position: 'relative',
  },

  canvasOverlay: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  savedIcon: {
    width: moderateScale(17),
    height: moderateScale(13),
    marginLeft: moderateScale(8),
  },
})

export default styles
