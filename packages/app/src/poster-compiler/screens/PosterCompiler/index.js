/**
 *
 * PosterCompiler
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Platform, CameraRoll, Share } from 'react-native'
import { MediaLibrary } from 'expo'
import BaseScreen from '@/app/base/components/BaseScreen'
import Canvas from '@/poster-compiler/containers/Canvas'
import ImageOverlay from '@/poster-compiler/components/atoms/ImageOverlay'
import PosterCompilerHeader from '@/poster-compiler/components/molecules/PosterCompilerHeader'
import CanvasBackgroundView from '@/poster-compiler/components/molecules/CanvasBackgroundView'
import PosterCompilerControls from '@/poster-compiler/components/organisms/PosterCompilerControls'
import TransparentButton from '@/poster-compiler/components/atoms/TransparentButton'
import PosterCompilerScreenWrapper from '@/poster-compiler/components/atoms/PosterCompilerScreenWrapper'
import Badges from '@/poster-compiler/components/organisms/Badges'

import checkLibraryPermission from '@/shared/utils/checkLibraryPermission'

import { POSTER_WIDTH } from './constants'
import CanvasModes from '@/poster-compiler/containers/Canvas/Modes'

import messages from './messages'

import styles from './styles'

export default class PosterCompiler extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    filter: 'Normal',
    isInitalRenderingFinished: false,
  }

  constructor(props) {
    super(props)

    const { navigation } = props

    this.aspectRatio = navigation.getParam('aspectRatio')
    this.canvasBackgroundImage = navigation.getParam('canvasBackgroundImage')
    this.posterBackgroundImage = navigation.getParam('posterBackgroundImage')
    this.backgroundImagePreview = navigation.getParam('backgroundImagePreview')

    console.info('this.canvasBackgroundImage: ', this.canvasBackgroundImage)
    console.info('this.posterBackgroundImage: ', this.posterBackgroundImage)
    console.info('this.backgroundImagePreview: ', this.backgroundImagePreview)

    if (
      !this.aspectRatio ||
      !this.canvasBackgroundImage ||
      !this.posterBackgroundImage ||
      !this.backgroundImagePreview
    ) {
      throw new Error('Error: Aspect ratio and background image are required!')
    }

    this.poster = {
      width: POSTER_WIDTH,
      aspectRatio: this.aspectRatio,
      // content: [
      //   {
      //     width: 0.3,
      //     aspectRatio: this.aspectRatio,
      //     selfAlignment: 1,
      //     source: {
      //       uri:
      //         'https://i2-prod.mirror.co.uk/incoming/article11812659.ece/ALTERNATES/s1200/The-Feline-World-Gathers-For-The-Supreme-Cat-Show-2017.jpg',
      //     },
      //     content: 'Image',
      //     // resizeMode: 'cover',
      //   },
      //   {
      //     width: 0.4,
      //     aspectRatio: this.aspectRatio,
      //     selfAlignment: 9,
      //     source: {
      //       uri: 'https://www.what-dog.net/Images/faces2/scroll006.jpg',
      //     },
      //     content: 'Image',
      //     resizeMode: 'cover',
      //     isSelectable: false,
      //   },
      //   {
      //     width: 0.5,
      //     selfAlignment: 5,
      //     content: 'PregnancyProgressFruit',
      //     isRotatable: false,
      //     backgroundColor: '#ffffff',
      //   },
      //   {
      //     width: 0.6,
      //     selfAlignment: 9,
      //     content: 'BTDPeekaboo',
      //   },
      // ],
    }
  }

  render() {
    const {
      isRendering,
      isProcessing,
      hasError,
      isSaved,
      filter,
      canvasWidth,
      savedImageLocalUri,
      isInitalRenderingFinished,
    } = this.state
    const { formatMessage } = this.context.intl
    const { profile } = this.props.screenProps

    const isShareButtonVisible = Platform.OS === 'ios' || !profile.isEmpty

    return (
      <PosterCompilerScreenWrapper style={styles.container}>
        <PosterCompilerHeader
          style={styles.header}
          onBackButtonPress={this._goBack}
          onSaveButtonPress={this._saveButtonPressed}
          onShareButtonPress={
            isShareButtonVisible ? this._shareButtonPressed : undefined
          }
        />

        <View style={styles.canvasWrapper}>
          <CanvasBackgroundView
            ref={ref => (this._canvasBackgroundPhoto = ref)}
            width={canvasWidth}
            aspectRatio={this.aspectRatio}
            image={this.canvasBackgroundImage}
            posterImage={this.posterBackgroundImage}
            filter={filter}
            isInitalRenderingFinished={isInitalRenderingFinished}
          />

          <Canvas
            ref={ref => (this.canvas = ref)}
            initialState={this.poster}
            onLayout={this._onCanvasLayout}
            rootContext={{ pregnancyId: profile.pregnancy }}
          />

          {do {
            if (isProcessing)
              <View style={styles.canvasOverlay}>
                {do {
                  if (isRendering)
                    <TransparentButton
                      title={formatMessage(messages.processingMessage)}
                    />
                  else if (isSaved)
                    <TransparentButton
                      title={formatMessage(messages.savedMessage)}
                      icon={
                        <Image
                          style={styles.savedIcon}
                          source={require('assets/icons/correct.png')}
                        />
                      }
                    />
                  else if (hasError)
                    <TransparentButton
                      title={formatMessage(messages.errorMessage)}
                    />
                }}
              </View>
          }}

          {__DEV__ ? (
            <ImageOverlay
              uri={savedImageLocalUri}
              width={canvasWidth}
              height={canvasWidth / this.aspectRatio}
              onRemove={this._resultImageOverlayRemoved}
            />
          ) : null}
        </View>

        {isInitalRenderingFinished ? (
          <Badges
            ref={ref => (this._badges = ref)}
            onSelect={this._addElement}
          />
        ) : null}

        <PosterCompilerControls
          photoPreview={this.backgroundImagePreview}
          activePhotoFilter={filter}
          onPhotoFilterSelect={this._photoFilterSelected}
          onBadgesButtonPress={this._badgesButtonPressed}
        />
      </PosterCompilerScreenWrapper>
    )
  }

  componentWillMount() {
    setTimeout(() => this.setState({ isInitalRenderingFinished: true }), 1000)
  }

  _resultImageOverlayRemoved = () => this.setState({ savedImageLocalUri: null })

  _badgesButtonPressed = () => this._badges && this._badges.expand()

  _addElement = elementId =>
    this.canvas.push({
      content: elementId,
      isSelectable: true,
      isRotatable: false,
      backgroundColor: 'transparent',
      theme: '#000000',
    })

  _photoFilterSelected = filter => this.setState({ filter })

  _onCanvasLayout = canvasWidth => this.setState({ canvasWidth })

  _saveButtonPressed = async () => {
    if (!this.state.isProcessing) {
      let shouldShiftCanvas

      try {
        this.setState({ isProcessing: true })

        await checkLibraryPermission(this.context.intl.formatMessage)

        this.setState({ isRendering: true })

        const scaledBackgroundPhoto = await this._canvasBackgroundPhoto.takeScaledSnapshot(
          this.poster.width
        )

        this.canvas.unshift({
          width: 1,
          aspectRatio: this.aspectRatio,
          content: 'Image',
          source: scaledBackgroundPhoto,
          resizeMode: 'cover',
          isSelectable: false,
          canvasModeToRender: CanvasModes.RENDER,
        })

        shouldShiftCanvas = true

        const localUri = await this.canvas.takeScaledSnapshot(this.poster.width)

        this.canvas.shift()

        shouldShiftCanvas = false

        console.info('localUri: ', localUri)

        const asset = await this._saveFile(localUri)

        this.setState({ isRendering: false, isSaved: true, hasError: false })

        setTimeout(() => {
          this.setState({
            isSaved: false,
            isProcessing: false,
            savedImageLocalUri: localUri,
          })
        }, 300)

        console.info('asset: ', asset)
      } catch (err) {
        console.log('err', err)

        this.setState({ isRendering: false, isSaved: false, hasError: true })

        setTimeout(() => {
          this.setState({
            isProcessing: false,
            isRendering: false,
            isSaved: false,
            hasError: false,
          })
        }, 400)

        if (shouldShiftCanvas) {
          this.canvas.shift()
        }
      }
    }
  }

  _shareButtonPressed = async () => {
    if (!this.state.isProcessing) {
      let shouldShiftCanvas

      try {
        this.setState({ isProcessing: true, isRendering: true })

        const scaledBackgroundPhoto = await this._canvasBackgroundPhoto.takeScaledSnapshot(
          this.poster.width
        )

        this.canvas.unshift({
          width: 1,
          aspectRatio: this.aspectRatio,
          content: 'Image',
          source: scaledBackgroundPhoto,
          resizeMode: 'cover',
          isSelectable: false,
          canvasModeToRender: CanvasModes.RENDER,
        })

        shouldShiftCanvas = true

        const localUri = await this.canvas.takeScaledSnapshot(this.poster.width)

        this.canvas.shift()

        shouldShiftCanvas = false

        this.setState({ isRendering: false, isProcessing: false })

        console.info('localUri: ', localUri)

        await this._shareImage(localUri)
      } catch (err) {
        console.log('err', err)

        this.setState({ isRendering: false, isSaved: false, hasError: true })

        setTimeout(() => {
          this.setState({
            isRendering: false,
            isSaved: false,
            hasError: false,
            isProcessing: false,
          })
        }, 400)

        if (shouldShiftCanvas) {
          this.canvas.shift()
        }
      }
    }
  }

  _saveFile = uri => {
    if (Platform.OS === 'android') {
      return CameraRoll.saveToCameraRoll(uri, 'photo')
    }

    return MediaLibrary.createAssetAsync(uri)
  }

  _shareImage = url => {
    if (Platform.OS === 'android') {
      this._goTo('/share-photo', { photoUri: url })

      return Promise.resolve()
    }

    return Share.share({ url })
  }
}
