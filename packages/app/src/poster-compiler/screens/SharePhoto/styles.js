import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { Constants } from 'expo'

const imageWidth = moderateScale(132)

const gutter = moderateScale(22)

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: moderateScale(22),
    flex: 1,
    paddingTop: Constants.statusBarHeight,
  },

  dialog: {
    width: moderateScale(330),
    height: 'auto',
    overflow: 'hidden',
  },

  preview: {
    marginRight: moderateScale(14),
  },

  content: {
    paddingHorizontal: moderateScale(13),
    paddingVertical: moderateScale(16),
    marginBottom: moderateScale(22),
    flexDirection: 'row',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: moderateScale(8),
    overflow: 'hidden',
  },

  redBorder: {
    borderColor: 'rgb(250,65,105)',
  },

  placeholder: {
    height: imageWidth,
    flex: 1,
  },

  placeholderText: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    color: 'rgb(131,146,167)',
    flex: 1,
  },

  header: {
    flexDirection: 'row',
    width: moderateScale(375),
    height: moderateScale(78),
    marginHorizontal: -gutter,
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(249,250,252)',
    ...ifIphoneX({
      top: moderateScale(10),
    }),
  },

  headerTitle: {
    color: 'rgb(59,72,89)',
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(13),
    textAlign: 'center',
  },

  shareButton: {
    position: 'absolute',
    right: moderateScale(10),
    top: moderateScale(20),
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(14),
  },

  shareButtonTitle: {
    color: 'rgb(59,124,255)',
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(14),
  },
})

export default styles
