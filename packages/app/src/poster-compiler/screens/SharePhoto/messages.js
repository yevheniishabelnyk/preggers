import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Share photo',
  shareButtonTitle: 'Share',
  sharePregnancyPopupText: 'Sharing your photo...',
  shareErorr: 'Sorry, something went wrong. Do you want to try again?',
  requiredProviderErorr: 'Please select at least one provider to share',
  requiredTweetStatusErorr: 'Plase add caption to your tweet',
  successFeedback: 'You have successfuly shared your photo!',
  yesButtonTitle: 'Yes',
  okButtonTitle: 'Ok',
  noButtonTitle: 'No',
  pregnancyPosterCaptionInputPlaceholder: 'Add a caption to the picture',
})
