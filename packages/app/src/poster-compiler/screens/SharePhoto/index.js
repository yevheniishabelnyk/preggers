/**
 *
 * SharePhoto screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { AuthSession, SecureStore } from 'expo'
import { FACEBOOK__APP_ID } from 'config'

import { TwitterApi } from '@/app/api/Preggers'

import { View, TouchableOpacity, TextInput, Keyboard } from 'react-native'
import PopupDialog from 'react-native-popup-dialog'
import Progress from '@/shared/components/Progress'
import Prompt from '@/shared/components/Prompt'
import BaseScreen from '@/app/base/components/BaseScreen'
import GoBackButton from '@/onboarding/components/GoBackButton'
import ProviderList from '@/poster-compiler/components/organisms/ProviderList'
import ImagePreview from '@/poster-compiler/components/molecules/ImagePreview'
import FormError from '@/forms/components/atoms/FormError'

import { FACEBOOK_ON, TWITTER_ON } from './constants'
import * as FacebookApi from '@/app/api/Facebook'

import { firebaseConnect } from 'react-redux-firebase'
import { getUserTokens } from '@/app/redux/selectors'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import moment from 'moment'

import styles from './styles'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    return [`/userOwned/tokens/${profile.id}`]
  }
})
@connect((state, props) => {
  const { profile } = props.screenProps

  let tokens

  if (!profile.isEmpty) {
    tokens = getUserTokens(state, profile.id, undefined)
  }

  return {
    tokens,
  }
})
export default class SharePhoto extends BaseScreen {
  static propTypes = {
    tokens: PropTypes.shape({
      facebookAccessToken: PropTypes.string,
      twitterAccessTokenKey: PropTypes.string,
      twitterAccessTokenSecret: PropTypes.string,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    tokens: {},
  }

  state = {
    providers: [
      // {
      //   name: 'Facebook',
      //   icon: 'facebook',
      //   active: false,
      //   disabled: false,
      // },
      {
        name: 'Twitter',
        icon: 'twitter',
        active: false,
        disabled: false,
      },
    ],
    text: '',
    error: null,
    isShareInProgress: false,
    shareError: false,
  }

  constructor(props) {
    super(props)

    this.photoUri = props.navigation.getParam('photoUri')
  }

  render() {
    const { error, requiredTextError } = this.state
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <GoBackButton onPress={this._goBackButtonPressed} />

          <FormattedMessage
            {...messages.headerTitle}
            style={styles.headerTitle}
          />

          <TouchableOpacity
            style={styles.shareButton}
            activeOpacity={0.95}
            onPress={this._sharePhoto}
          >
            <FormattedMessage
              {...messages.shareButtonTitle}
              style={styles.shareButtonTitle}
            />
          </TouchableOpacity>
        </View>

        <View
          style={[styles.content, requiredTextError ? styles.redBorder : null]}
        >
          {this.photoUri ? (
            <ImagePreview
              style={styles.preview}
              images={[
                {
                  source: {
                    uri: this.photoUri,
                    preview: this.photoUri,
                  },
                },
              ]}
              source={{ uri: this.photoUri }}
            />
          ) : null}

          <TouchableOpacity
            style={styles.placeholder}
            activeOpacity={0.95}
            onPress={this._sharePhoto}
          >
            <TextInput
              placeholder={formatMessage(
                messages.pregnancyPosterCaptionInputPlaceholder
              )}
              placeholderTextColor="rgb(131,146,167)"
              multiline
              numberOfLines={4}
              onChangeText={this._captionChanged}
              value={this.state.text}
              style={styles.placeholderText}
            />
          </TouchableOpacity>
        </View>

        <ProviderList
          style={styles.providersList}
          providers={this.state.providers}
          onToggleProvider={this._onToggleProvider}
        />

        <FormError isVisible={Boolean(error)} title={error} />

        <PopupDialog
          key="progressDialog"
          ref={ref => (this._progressDialog = ref)}
          dialogStyle={styles.dialog}
          overlayOpacity={0.7}
          dismissOnTouchOutside={this.state.shareError}
          onDismissed={this._progressDialogDismissed}
        >
          {this.state.isShareInProgress ? (
            <Progress text={formatMessage(messages.sharePregnancyPopupText)} />
          ) : (
            <Prompt
              text={
                this.state.shareError
                  ? formatMessage(messages.shareErorr)
                  : null
              }
              caption={
                !this.state.shareError
                  ? formatMessage(messages.successFeedback)
                  : null
              }
              positiveButton={{
                title: this.state.shareError
                  ? formatMessage(messages.yesButtonTitle)
                  : formatMessage(messages.okButtonTitle),
                onPress: this.state.shareError
                  ? this._sharePhoto
                  : this._hideDialog,
              }}
              negativeButton={
                this.state.shareError
                  ? {
                      title: formatMessage(messages.noButtonTitle),
                      onPress: this._hideDialog,
                    }
                  : null
              }
            />
          )}
        </PopupDialog>
      </View>
    )
  }

  componentWillMount() {
    this._loadSavedState()

    const { tokens } = this.props

    if (tokens.facebookAccessToken) {
      FacebookApi.checkFBToken(tokens.facebookAccessToken, FACEBOOK__APP_ID)
        .then(() =>
          this.setState({ facebookAccessToken: tokens.facebookAccessToken })
        )
        .catch(err => {
          console.log('err', err)

          this.setState(prevState => ({
            providers: prevState.providers.map(item => ({
              ...item,
              active: item.name === 'Facebook' ? false : item.active,
            })),
          }))
        })
    }

    if (tokens.twitterAccessTokenKey && tokens.twitterAccessTokenSecret) {
      this.setState({
        twitterAccessTokenKey: tokens.twitterAccessTokenKey,
        twitterAccessTokenSecret: tokens.twitterAccessTokenSecret,
      })
    }
  }

  _goBackButtonPressed = () => {
    Keyboard.dismiss()

    this._goBack()
  }

  _sharePhoto = async () => {
    Keyboard.dismiss()

    const { isShareInProgress, text } = this.state

    if (isShareInProgress) {
      return
    }

    const TwitterProvider = this.state.providers.find(
      pr => pr.name === 'Twitter'
    )
    const isShareWithTwitterActive = TwitterProvider && TwitterProvider.active

    const FacebookProvider = this.state.providers.find(
      pr => pr.name === 'Facebook'
    )
    const isShareWithFacebookActive =
      FacebookProvider && FacebookProvider.active

    const { formatMessage } = this.context.intl

    if (!isShareWithTwitterActive && !isShareWithFacebookActive) {
      this.setState(prevState => ({
        providers: prevState.providers.map(item => ({ ...item, error: true })),
        error: formatMessage(messages.requiredProviderErorr),
      }))

      return
    }

    if (isShareWithTwitterActive && !text) {
      this.setState(prevState => ({
        providers: prevState.providers.map(item => ({
          ...item,
          error: item.name === 'Twitter',
        })),
        error: formatMessage(messages.requiredTweetStatusErorr),
        requiredTextError: true,
      }))

      return
    }

    this.setState({ isShareInProgress: true, shareError: false })

    setTimeout(() => {
      this._progressDialog.show()
    }, 0)

    try {
      if (isShareWithTwitterActive && isShareWithFacebookActive) {
        const [twitterResponse, facebookResponse] = await Promise.all([
          this._shareToTwitter(),
          this._shareToFacebook(),
        ])

        console.info('facebookResponse: ', facebookResponse)
        console.info('twitterResponse: ', twitterResponse)

        // const tweetUrl =
        //   twitterResponse.tweet.extended_entities.media[0].expanded_url

        // const facebookPostUrl = `https://www.facebook.com/photo.php?fbid=${
        //   facebookResponse.id
        // }`
      } else if (isShareWithTwitterActive) {
        const twitterResponse = await this._shareToTwitter()

        console.info('twitterResponse: ', twitterResponse)

        // const tweetUrl =
        //   twitterResponse.tweet.extended_entities.media[0].expanded_url
      } else if (isShareWithFacebookActive) {
        const facebookResponse = await this._shareToFacebook()
        console.info('facebookResponse: ', facebookResponse)

        // const facebookPostUrl = `https://www.facebook.com/photo.php?fbid=${
        //   facebookResponse.id
        // }`
      }

      this.setState({ shareError: false })
    } catch (err) {
      console.info('err: ', err)

      this.setState({ shareError: true })
    } finally {
      this.setState({ isShareInProgress: false })
    }
  }

  _captionChanged = value =>
    this.setState(prevState => ({
      text: value,
      requiredTextError: null,
      error: null,
      providers: prevState.providers.map(item => ({ ...item, error: false })),
    }))

  _progressDialogDismissed = () => this.setState({ shareError: false })

  _loadSavedState = async () => {
    try {
      let providersState = await SecureStore.getItemAsync('providersState')

      if (providersState !== undefined) {
        providersState = Number(providersState)

        this.setState(prevState => ({
          providers: prevState.providers.map(pr => {
            let active = false

            switch (pr.name) {
              case 'Facebook':
                active = Boolean(providersState & FACEBOOK_ON)
                break

              case 'Twitter':
                active = Boolean(providersState & TWITTER_ON)
                break
            }

            return Object.assign({}, pr, { active })
          }),
        }))
      }
    } catch (err) {
      console.log('err', err)
    }
  }

  _saveProvidersState = providerName => {
    const providersState = this.state.providers.reduce((out, provider) => {
      switch (provider.name) {
        case 'Facebook':
          if (
            (provider.active && providerName !== 'Facebook') ||
            (!provider.active && providerName === 'Facebook')
          ) {
            return out | FACEBOOK_ON
          }
          break

        case 'Twitter':
          if (
            (provider.active && providerName !== 'Twitter') ||
            (!provider.active && providerName === 'Twitter')
          ) {
            return out | TWITTER_ON
          }
          break
      }

      return out
    }, 0)

    SecureStore.setItemAsync('providersState', String(providersState))
  }

  _onToggleProvider = async (providerName, active) => {
    this.setState(prevState => ({
      providers: prevState.providers.map(item => ({
        ...item,
        active: providerName === item.name ? active : item.active,
        error: false,
      })),
      requiredTextError: null,
      error: null,
    }))

    if (active) {
      const { profile } = this.props.screenProps
      const { tokens, firebase } = this.props

      switch (providerName) {
        case 'Facebook': {
          if (!tokens.facebookAccessToken) {
            try {
              let redirectUrl = AuthSession.getRedirectUrl()

              let result = await AuthSession.startAsync({
                authUrl:
                  'https://www.facebook.com/v2.10/dialog/oauth?response_type=token,granted_scopes&scope=publish_actions' +
                  `&client_id=${FACEBOOK__APP_ID}` +
                  `&redirect_uri=${encodeURIComponent(redirectUrl)}`,
              })

              if (result.type !== 'success') {
                this.setState(prevState => ({
                  providers: prevState.providers.map(item => {
                    if (providerName === 'Facebook') {
                      return {
                        ...item,
                        active: false,
                      }
                    }
                    return item
                  }),
                }))

                return
              }

              const token = result.params.access_token
              const granted_scopes = result.params.granted_scopes.split(',')

              if (granted_scopes.indexOf('publish_actions') !== -1) {
                firebase.update(`userOwned/tokens/${profile.id}`, {
                  facebookAccessToken: token,
                })
              }
            } catch (e) {
              console.info('e: ', e)

              this.setState(prevState => ({
                providers: prevState.providers.map(item => {
                  if (providerName === 'Facebook') {
                    return {
                      ...item,
                      active: false,
                    }
                  }
                  return item
                }),
              }))
            }
          }
          break
        }

        case 'Twitter': {
          if (
            !tokens.twitterAccessTokenKey ||
            !tokens.twitterAccessTokenSecret
          ) {
            let redirectUrl = AuthSession.getRedirectUrl()

            try {
              const response = await TwitterApi.getAuthUrl(redirectUrl)
              const getAuthUrlResult = response.body

              if (response.status === 200) {
                const result = await AuthSession.startAsync({
                  authUrl: response.body.redirectURL,
                })

                if (result.type === 'success') {
                  const verifier = result.params.oauth_verifier

                  const response = await TwitterApi.getAccessToken(
                    verifier,
                    getAuthUrlResult.token,
                    getAuthUrlResult.secretToken
                  )

                  if (
                    response.status === 200 &&
                    response.body.oauth_token &&
                    response.body.oauth_token_secret
                  ) {
                    firebase.update(`userOwned/tokens/${profile.id}`, {
                      twitterAccessTokenKey: response.body.oauth_token,
                      twitterAccessTokenSecret:
                        response.body.oauth_token_secret,
                    })
                  }
                } else {
                  this.setState(prevState => ({
                    providers: prevState.providers.map(item => {
                      if (providerName === 'Twitter') {
                        return {
                          ...item,
                          active: false,
                        }
                      }
                      return item
                    }),
                  }))
                }
              } else {
                this.setState(prevState => ({
                  providers: prevState.providers.map(item => {
                    if (providerName === 'Twitter') {
                      return {
                        ...item,
                        active: false,
                      }
                    }
                    return item
                  }),
                }))
              }
            } catch (err) {
              console.log(err)

              this.setState(prevState => ({
                providers: prevState.providers.map(item => {
                  if (providerName === 'Twitter') {
                    return {
                      ...item,
                      active: false,
                    }
                  }
                  return item
                }),
              }))
            }
          }
          break
        }
      }
    }

    this._saveProvidersState(providerName)
  }

  _shareToTwitter = async () => {
    const { tokens } = this.props

    if (
      this.photoUri &&
      this.state.text &&
      tokens.twitterAccessTokenSecret &&
      tokens.twitterAccessTokenKey
    ) {
      console.log('post tweet')

      try {
        const data = {
          uri: this.photoUri,
          name: `Preggers ${moment().format('YYYY-MM-DD HH:mm')}.png`,
          type: 'image/png',
        }

        const response = await TwitterApi.postTweet({
          status: this.state.text,
          accessTokenKey: tokens.twitterAccessTokenKey,
          accessTokenSecret: tokens.twitterAccessTokenSecret,
          file: data,
        })

        return Promise.resolve(response.tweet)
      } catch (err) {
        return Promise.reject(err)
      }
    } else {
      return Promise.reject(null)
    }
  }

  _shareToFacebook = async () => {
    const { tokens } = this.props

    if (tokens.facebookAccessToken && this.photoUri) {
      console.log('post fb')

      try {
        const formData = new FormData()

        const data = {
          uri: this.photoUri,
          name: `Preggers ${moment().format('YYYY-MM-DD HH:mm')}.png`,
          type: 'image/png',
        }

        formData.append('data', data)

        if (this.state.text) {
          formData.append('message', this.state.text)
        }

        const host = 'https://graph.facebook.com/v2.10'

        const headers = new Headers({
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        })

        const res = await fetch(
          `${host}/me/photos?access_token=${tokens.facebookAccessToken}`,
          {
            method: 'POST',
            body: formData,
            headers,
          }
        )

        return Promise.resolve(await res.json())
      } catch (err) {
        return Promise.reject(err)
      }
    } else {
      return Promise.reject(null)
    }
  }

  _hideDialog = () => this._progressDialog.dismiss()
}
