import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'
import CanvasAspectRatios from '@/poster-compiler/containers/Canvas/AspectRatios'

const cameraViewWidth = moderateScale(375, 0.72)
const cameraViewHeight = cameraViewWidth / CanvasAspectRatios.DEFAULT

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  content: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    alignItems: 'center',
    position: 'relative',
  },

  camera: {
    width: cameraViewWidth,
    height: cameraViewHeight,
  },

  controls: {
    marginTop: 'auto',
  },

  closeButton: {
    position: 'absolute',
    left: moderateScale(18.5, 0.3),
    top: moderateScale(18.5, 0.3),
  },
})

export default styles
