/**
 *
 * PosterBackground
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import Camera from '@/poster-compiler/components/atoms/Camera'
import PosterCompilerScreenWrapper from '@/poster-compiler/components/atoms/PosterCompilerScreenWrapper'
import PosterBackgroundControls from '@/poster-compiler/components/organisms/PosterBackgroundControls'
import CloseButton from '@/pregnancy/components/atoms/CloseButton'
import { ImagePicker } from 'expo'

import checkLibraryPermission from '@/shared/utils/checkLibraryPermission'

import styles from './styles'

export default class PosterBackground extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    return (
      <PosterCompilerScreenWrapper style={styles.container} statusBar="hidden">
        <View style={styles.content}>
          <CloseButton style={styles.closeButton} onPress={this._goBack} />

          <Camera style={styles.camera} ref={ref => (this._camera = ref)} />

          <PosterBackgroundControls
            style={styles.controls}
            onChangeCameraFlashMode={this._changeCameraFlashModeButtonPressed}
            onTakePhoto={this._takePhotoButtonPressed}
            onSwitchCamera={this._switchCameraButtonPressed}
            onOpenPhotoLibrary={this._openPhotoLibraryButtonPressed}
          />
        </View>
      </PosterCompilerScreenWrapper>
    )
  }

  _changeCameraFlashModeButtonPressed = () => this._camera.toggleFlashMode()

  _switchCameraButtonPressed = () => this._camera.switchCamera()

  _takePhotoButtonPressed = async () => {
    try {
      const result = await this._camera.takePictureAsync()

      console.info('result: ', result)

      this._goTo('/poster-aspect-ratio', { image: result })
    } catch (err) {
      console.log('err', err)
    }
  }

  _openPhotoLibraryButtonPressed = async () => {
    try {
      await checkLibraryPermission(this.context.intl.formatMessage)

      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
      })

      if (result.cancelled) {
        return
      }

      console.info('result: ', result)

      this._goTo('/poster-aspect-ratio', { image: result })
    } catch (err) {
      console.log('err', err)
    }
  }
}
