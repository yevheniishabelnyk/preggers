import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'
import { Constants } from 'expo'
const { statusBarHeight } = Constants

export const canvasContainerBackgroundColor = '#ffffff'

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
    backgroundColor: '#f9fafb',
    paddingTop: statusBarHeight,
  },

  header: {
    backgroundColor: '#f9fafb',
  },

  footer: {
    marginTop: 'auto',
    backgroundColor: '#f9fafb',
  },

  palette: {
    marginVertical: moderateScale(8),
  },

  canvas: {
    backgroundColor: canvasContainerBackgroundColor,
    // maxWidth: 200,
    // maxHeight: 200,
  },

  maxSpace: {
    flex: 1,
    alignSelf: 'stretch',
    position: 'relative',
  },

  canvasOverlay: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  savedIcon: {
    width: moderateScale(17),
    height: moderateScale(13),
    marginLeft: moderateScale(8),
  },
})

export default styles
