/**
 *
 * Poster screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Share, Platform, CameraRoll, Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import Canvas from '@/poster-compiler/containers/Canvas'
import Palette from '@/poster-compiler/components/molecules/Palette'
import PosterCompilerHeader from '@/poster-compiler/components/molecules/PosterCompilerHeader'
import TransparentButton from '@/poster-compiler/components/atoms/TransparentButton'
import PosterCompilerScreenWrapper from '@/poster-compiler/components/atoms/PosterCompilerScreenWrapper'

import { MediaLibrary } from 'expo'

import posters from '@/poster-compiler/posters'

// import randomColor from 'randomcolor'

import CanvasModes from '@/poster-compiler/containers/Canvas/Modes'

import checkLibraryPermission from '@/shared/utils/checkLibraryPermission'

import messages from './messages'

import styles, { canvasContainerBackgroundColor } from './styles'

export default class Poster extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    const posterId = props.navigation.getParam('posterId')
    const posterContext = props.navigation.getParam('posterContext')

    this.poster = posters[posterId]
    this.posterContext = posterContext

    const canvasBackground = this.poster
      ? this.poster.backgroundColor || canvasContainerBackgroundColor
      : canvasContainerBackgroundColor

    this.state = {
      canvasContainerBackgroundColor: canvasBackground,
      paletteColors: [canvasBackground],
    }
  }

  render() {
    const {
      canvasContainerBackgroundColor,
      paletteColors,
      isRendering,
      isSaved,
      isProcessing,
      hasError,
    } = this.state
    const { formatMessage } = this.context.intl

    return (
      <PosterCompilerScreenWrapper style={styles.container}>
        <PosterCompilerHeader
          style={styles.header}
          onBackButtonPress={this._goBack}
          onSaveButtonPress={this._saveButtonPressed}
          onShareButtonPress={this._shareButtonPressed}
        />

        <View style={styles.maxSpace}>
          {this.poster ? (
            <Canvas
              containerStyle={[
                styles.canvas,
                { backgroundColor: canvasContainerBackgroundColor },
              ]}
              ref={ref => (this.canvas = ref)}
              initialState={this.poster}
              rootContext={this.posterContext}
              mode={CanvasModes.EDIT_EXTERNAL}
            />
          ) : null}

          {do {
            if (isProcessing)
              <View style={styles.canvasOverlay}>
                {do {
                  if (isRendering)
                    <TransparentButton
                      title={formatMessage(messages.processingMessage)}
                    />
                  else if (isSaved)
                    <TransparentButton
                      title={formatMessage(messages.savedMessage)}
                      icon={
                        <Image
                          style={styles.savedIcon}
                          source={require('assets/icons/correct.png')}
                        />
                      }
                    />
                  else if (hasError)
                    <TransparentButton
                      title={formatMessage(messages.errorMessage)}
                    />
                }}
              </View>
          }}
        </View>

        <View style={styles.footer}>
          <Palette
            colors={paletteColors}
            activeColor={canvasContainerBackgroundColor}
            onSelect={this._backgroundColorSelected}
            scrollDirection="horizontal"
            style={styles.palette}
          />
        </View>
      </PosterCompilerScreenWrapper>
    )
  }

  componentDidMount() {
    const { paletteColors, canvasContainerBackgroundColor } = this.state

    let nextPaletteColors = paletteColors

    if (this.poster && this.poster.backgroundColorList) {
      nextPaletteColors = paletteColors.concat(
        Object.keys(this.poster.backgroundColorList).filter(
          color => color !== canvasContainerBackgroundColor
        )
      )
    }

    // uncomment this if you want user to have random colors in the pallet
    /*if (nextPaletteColors.length < 100) {
      const requiredColorsLength = 100 - nextPaletteColors.length
      const thirdPortionLength = Math.floor(requiredColorsLength / 3)

      nextPaletteColors = nextPaletteColors
        .concat(
          randomColor({
            count: thirdPortionLength,
            luminosity: 'light',
            hue: 'random',
          })
        )
        .concat(
          randomColor({
            count: thirdPortionLength,
            luminosity: 'bright',
            hue: 'random',
          })
        )
        .concat(
          randomColor({
            count: requiredColorsLength - thirdPortionLength * 2,
            luminosity: 'dark',
            hue: 'random',
          })
        )
    }*/

    this.setState({ paletteColors: nextPaletteColors })
  }

  _backgroundColorSelected = color => {
    const { isRendering } = this.state

    console.log('backgroundColor: ', color)

    if (!isRendering && this.canvas) {
      this.canvas.setBackgroundColor(color)
    }

    this.setState({ canvasContainerBackgroundColor: color })
  }

  _saveButtonPressed = async () => {
    if (!this.state.isProcessing) {
      try {
        this.setState({ isProcessing: true })

        await checkLibraryPermission(this.context.intl.formatMessage)

        this.setState({ isRendering: true })

        const localUri = await this.canvas.takeScaledSnapshot(this.poster.width)

        console.info('localUri: ', localUri)

        const asset = await this._saveFile(localUri)

        this.setState({ isRendering: false, isSaved: true })

        setTimeout(
          () => this.setState({ isProcessing: false, isSaved: false }),
          300
        )

        console.info('asset: ', asset)
      } catch (err) {
        console.log('err', err)

        this.setState({ isRendering: false, isSaved: false, hasError: true })

        setTimeout(() => {
          this.setState({
            isProcessing: false,
            isRendering: false,
            isSaved: false,
            hasError: false,
          })
        }, 400)
      }
    }
  }

  _shareButtonPressed = async () => {
    if (!this.state.isProcessing) {
      try {
        this.setState({ isProcessing: true, isRendering: true })

        const localUri = await this.canvas.takeScaledSnapshot(this.poster.width)

        this.setState({ isRendering: false, isProcessing: false })

        console.info('localUri: ', localUri)

        await this._shareImage(localUri)
      } catch (err) {
        console.log('err', err)

        this.setState({ isRendering: false, isSaved: false, hasError: true })

        setTimeout(() => {
          this.setState({
            isRendering: false,
            isSaved: false,
            hasError: false,
            isProcessing: false,
          })
        }, 400)
      }
    }
  }

  _saveFile = uri => {
    if (Platform.OS === 'android') {
      return CameraRoll.saveToCameraRoll(uri, 'photo')
    }

    return MediaLibrary.createAssetAsync(uri)
  }

  _shareImage = url => {
    if (Platform.OS === 'android') {
      this._goTo('/share-photo', { photoUri: url })

      return Promise.resolve()
    }

    return Share.share({ url })
  }
}
