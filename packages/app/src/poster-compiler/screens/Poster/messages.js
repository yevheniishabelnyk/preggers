import { defineMessages } from 'react-intl'

export default defineMessages({
  processingMessage: 'Processing...',
  savedMessage: 'Saved',
  errorMessage: 'Error',
})
