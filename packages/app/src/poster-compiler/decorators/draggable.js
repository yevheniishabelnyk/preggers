/**
 *
 * draggable decorator
 * Style from props should only have layout styles (margins, top, left, etc.)!!!
 */

import React from 'react'
import PropTypes from 'prop-types'

import { GestureHandler } from 'expo'
import { Animated } from 'react-native'

import { isFunction } from 'lodash'

const { PanGestureHandler, State } = GestureHandler

export default function(options = { horizontal: true, vertical: true }) {
  return function(WrappedComponent) {
    return class DraggableComponent extends React.Component {
      static propTypes = {
        style: PropTypes.any,
        onPanChange: PropTypes.func,
        horizontal: PropTypes.bool,
        vertical: PropTypes.bool,
      }

      static defaultProps = {
        horizontal: options.horizontal,
        vertical: options.vertical,
      }

      constructor(props) {
        super(props)

        if (props.horizontal) {
          this._translateX = new Animated.Value(0)
        }

        if (props.vertical) {
          this._translateY = new Animated.Value(0)
        }

        this._lastOffset = { x: 0, y: 0 }

        this._onGestureEvent = Animated.event(
          [
            {
              nativeEvent: {
                translationX: this._translateX,
                translationY: this._translateY,
              },
            },
          ],
          { useNativeDriver: true }
        )
      }

      _onHandlerStateChange = event => {
        if (event.nativeEvent.oldState === State.ACTIVE) {
          if (this.props.horizontal) {
            this._lastOffset.x += event.nativeEvent.translationX
            this._translateX.setOffset(this._lastOffset.x)
            this._translateX.setValue(0)
          }

          if (this.props.vertical) {
            this._lastOffset.y += event.nativeEvent.translationY
            this._translateY.setOffset(this._lastOffset.y)
            this._translateY.setValue(0)
          }

          const { onPanChange } = this.props

          if (isFunction(onPanChange)) {
            onPanChange({ top: this._lastOffset.y, left: this._lastOffset.x })
          }
        }
      }

      render() {
        const { style, horizontal, vertical, ...otherProps } = this.props

        const transformStyle = { transform: [] }

        if (horizontal) {
          transformStyle.transform.push({ translateX: this._translateX })
        }

        if (vertical) {
          transformStyle.transform.push({ translateY: this._translateY })
        }

        return (
          <PanGestureHandler
            onGestureEvent={this._onGestureEvent}
            onHandlerStateChange={this._onHandlerStateChange}
          >
            <Animated.View style={[style, transformStyle]}>
              <WrappedComponent {...otherProps} />
            </Animated.View>
          </PanGestureHandler>
        )
      }
    }
  }
}
