import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  hiddenContainer: {
    width: 0,
    height: 0,
    position: 'relative',
    overflow: 'hidden',
  },

  scaledContentContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
})

export default styles
