/**
 *
 * renderer decorator
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, PixelRatio } from 'react-native'

import { takeSnapshotAsync } from 'expo'
import { isFunction } from 'lodash'

import styles from './styles'

const pixelRatio = PixelRatio.get()

export default function renderer({
  scalableStateValues = [],
  renderableProps = {},
  takeScaledSnapshotMethodName = 'takeScaledSnapshot',
}) {
  return function(Content) {
    class EnhancedContent extends Content {
      [takeScaledSnapshotMethodName] = argv =>
        this.props.takeScaledSnapshot(argv)
    }

    class Renderer extends React.Component {
      static propTypes = {
        forwardedRef: PropTypes.func,
      }

      state = {}

      render() {
        const { isScaledContentRendered, scaledContentWidth } = this.state
        const { forwardedRef } = this.props

        const originalContent = (
          <EnhancedContent
            {...this.props}
            key="original"
            ref={ref => {
              this.originalContent = ref
              forwardedRef(ref)
            }}
            takeScaledSnapshot={this.takeScaledContentSnapshot}
            isRendering={isScaledContentRendered}
          />
        )

        if (!isScaledContentRendered) {
          return originalContent
        }

        const { scaledState, scale } = this._getScaledState(
          this.originalContent.state,
          scaledContentWidth
        )

        const scaledContent = (
          <EnhancedContent
            {...this.props}
            {...renderableProps}
            key="scaled"
            ref={ref => (this._scaledContent = ref)}
            initialState={scaledState}
            scale={scale}
          />
        )

        return (
          <React.Fragment>
            {originalContent}

            <View style={styles.hiddenContainer}>
              <View style={styles.scaledContentContainer}>{scaledContent}</View>
            </View>
          </React.Fragment>
        )
      }

      takeScaledContentSnapshot = scaledContentWidth =>
        new Promise((resolve, reject) => {
          this.setState(
            { isScaledContentRendered: true, scaledContentWidth },
            () => {
              const takeSnapshot = () => {
                requestAnimationFrame(() => {
                  takeSnapshotAsync(this._scaledContent, {
                    format: 'png',
                    result: 'file',
                    quality: 1.0,
                  })
                    .then(localUri => {
                      resolve(localUri)

                      this.setState({ isScaledContentRendered: false })
                    })
                    .catch(reject)
                })
              }

              if (isFunction(this._scaledContent.waitFor)) {
                this._scaledContent
                  .waitFor()
                  .then(() => takeSnapshot())
                  .catch(reject)
              } else {
                takeSnapshot()
              }
            }
          )
        })

      _getScaledState(originalState, newWidth) {
        const scaledState = {}
        const scale = newWidth / originalState.width / pixelRatio

        Object.keys(originalState).forEach(key => {
          if (scalableStateValues.includes(key)) {
            scaledState[key] = originalState[key] * scale
          } else {
            scaledState[key] = originalState[key]
          }
        })

        return { scaledState, scale }
      }
    }

    return React.forwardRef((props, ref) => (
      <Renderer {...props} forwardedRef={ref} />
    ))
  }
}
