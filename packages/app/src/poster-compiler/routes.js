/**
 *
 * Poster Compiler routes
 *
 */

import Poster from './screens/Poster'
import PosterAspectRatio from './screens/PosterAspectRatio'
import PosterBackground from './screens/PosterBackground'
import PosterCompiler from './screens/PosterCompiler'
import SharePhoto from './screens/SharePhoto'

export default {
  '/poster': {
    screen: Poster,
  },

  '/poster-background': {
    screen: PosterBackground,
  },

  '/poster-aspect-ratio': {
    screen: PosterAspectRatio,
    navigationOptions: {
      gesturesEnabled: false,
    },
  },

  '/poster-compiler': {
    screen: PosterCompiler,
    navigationOptions: {
      gesturesEnabled: false,
    },
  },

  '/share-photo': {
    screen: SharePhoto,
  },
}
