import backgroundColors from '@/pregnancy/components/organisms/PregnancyProgressPercentage/backgroundColors'

export default {
  width: 1080,
  aspectRatio: 0.675,
  backgroundColor: backgroundColors.default,
  backgroundColorList: backgroundColors.list,
  content: [
    {
      width: 0.7,
      selfAlignment: 5,
      content: 'PregnancyProgressPercentage',
    },
  ],
}
