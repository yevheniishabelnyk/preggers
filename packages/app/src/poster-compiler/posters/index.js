/**
 *
 * posters
 *
 */

import pregnancy_progress_fruit from './pregnancy_progress_fruit'
import pregnancy_progress_day from './pregnancy_progress_day'
import pregnancy_progress_baby from './pregnancy_progress_baby'
import pregnancy_progress_week from './pregnancy_progress_week'
import pregnancy_progress_summary from './pregnancy_progress_summary'
import pregnancy_progress_percentage from './pregnancy_progress_percentage'
import btd_peekaboo from './btd_peekaboo'

const posters = {
  pregnancy_progress_fruit,
  pregnancy_progress_day,
  pregnancy_progress_percentage,
  pregnancy_progress_week,
  pregnancy_progress_baby,
  pregnancy_progress_summary,
  btd_peekaboo,
}

export default posters
