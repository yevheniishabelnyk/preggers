/**
 *
 * PosterBackgroundControls
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import PosterBackgroundControlsButton from '@/poster-compiler/components/atoms/PosterBackgroundControlsButton'
import TakePhotoButton from '@/poster-compiler/components/atoms/TakePhotoButton'

import styles from './styles'

export default class PosterBackgroundControls extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    onChangeCameraFlashMode: PropTypes.func,
    onTakePhoto: PropTypes.func,
    onSwitchCamera: PropTypes.func,
    onOpenPhotoLibrary: PropTypes.func,
  }

  render() {
    const {
      style,
      onOpenPhotoLibrary,
      onChangeCameraFlashMode,
      onTakePhoto,
      onSwitchCamera,
    } = this.props

    return (
      <View style={[styles.container, style]}>
        <View style={styles.column}>
          <PosterBackgroundControlsButton
            style={styles.photoLibraryButton}
            icon={require('assets/icons/photo-library-black.png')}
            onPress={onOpenPhotoLibrary}
          />

          <PosterBackgroundControlsButton
            style={styles.cameraFlashButton}
            icon={require('assets/icons/camera-flash-black.png')}
            onPress={onChangeCameraFlashMode}
          />
        </View>

        <TakePhotoButton onPress={onTakePhoto} />

        <View style={styles.column}>
          <PosterBackgroundControlsButton
            style={styles.switchCameraButton}
            icon={require('assets/icons/switch-camera-black.png')}
            onPress={onSwitchCamera}
          />
        </View>
      </View>
    )
  }
}
