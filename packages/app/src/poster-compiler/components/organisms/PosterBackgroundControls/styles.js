import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    height: moderateScale(155, resizeFactor),
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
  },

  column: {
    flex: 1,
    flexDirection: 'row',
  },

  photoLibraryButton: {
    marginLeft: 'auto',
  },

  cameraFlashButton: {
    paddingVertical: moderateScale(7.5, resizeFactor),
    marginLeft: moderateScale(10, resizeFactor),
    marginRight: moderateScale(15, resizeFactor),
  },

  switchCameraButton: {
    paddingVertical: moderateScale(7.5, resizeFactor),
    paddingHorizontal: moderateScale(7, resizeFactor),
    marginLeft: moderateScale(20, resizeFactor),
    marginRight: 'auto',
  },
})

export default styles
