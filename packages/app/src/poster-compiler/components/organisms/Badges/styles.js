import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'

const headerHeight = moderateScale(53)
export const screenHeight = Dimensions.get('window').height

const styles = StyleSheet.create({
  content: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 10,
  },

  container: {
    flex: 1,
  },

  header: {
    height: headerHeight,
    backgroundColor: '#f9fafb',
  },

  hideButton: {
    height: headerHeight,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },

  arrowDownIcon: {
    width: moderateScale(59),
    height: moderateScale(13),
  },

  darkBackground: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
})

export default styles
