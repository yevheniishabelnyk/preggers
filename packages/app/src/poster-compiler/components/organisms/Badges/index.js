/**
 *
 * Badges
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  View,
  Animated,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native'
import { GestureHandler, BlurView, Constants } from 'expo'
import ProgressBarSelect from '@/poster-compiler/components/organisms/ProgressBarSelect'

import { isFunction } from 'lodash'
import isIPhoneX from '@/shared/utils/isIPhoneX'

import styles, { screenHeight } from './styles'

const {
  PanGestureHandler,
  NativeViewGestureHandler,
  State,
  TapGestureHandler,
} = GestureHandler

const SNAP_POINTS_FROM_TOP = [
  isIPhoneX() ? Constants.statusBarHeight : 0,
  screenHeight,
]

export default class Badges extends React.Component {
  static propTypes = {
    onSelect: PropTypes.func,
  }

  masterdrawer = React.createRef()
  drawer = React.createRef()
  drawerheader = React.createRef()
  scroll = React.createRef()

  constructor(props) {
    super(props)

    this._startPoint = SNAP_POINTS_FROM_TOP[0]
    this._endPoint = SNAP_POINTS_FROM_TOP[SNAP_POINTS_FROM_TOP.length - 1]

    this.state = {
      lastSnap: this._endPoint,
    }

    this._lastScrollYValue = 0
    this._lastScrollY = new Animated.Value(0)
    this._onRegisterLastScroll = Animated.event(
      [{ nativeEvent: { contentOffset: { y: this._lastScrollY } } }],
      { useNativeDriver: true }
    )

    this._lastScrollY.addListener(({ value }) => {
      this._lastScrollYValue = value
    })

    this._dragY = new Animated.Value(0)
    this._onGestureEvent = Animated.event(
      [{ nativeEvent: { translationY: this._dragY } }],
      { useNativeDriver: true }
    )

    this._reverseLastScrollY = Animated.multiply(
      new Animated.Value(-1),
      this._lastScrollY
    )

    this._translateYOffset = new Animated.Value(this._endPoint)
    this._translateY = Animated.add(
      this._translateYOffset,
      Animated.add(this._dragY, this._reverseLastScrollY)
    ).interpolate({
      inputRange: [this._startPoint, this._endPoint],
      outputRange: [this._startPoint, this._endPoint],
      extrapolate: 'clamp',
    })
  }

  render() {
    return (
      <TapGestureHandler
        maxDurationMs={100000}
        ref={this.masterdrawer}
        maxDeltaY={this.state.lastSnap - this._startPoint}
      >
        <View style={styles.content} pointerEvents="box-none">
          <Animated.View
            style={[
              StyleSheet.absoluteFillObject,
              {
                transform: [{ translateY: this._translateY }],
              },
            ]}
          >
            {Platform.OS === 'ios' ? (
              <BlurView
                tint="dark"
                style={StyleSheet.absoluteFill}
                intensity={100}
              />
            ) : (
              <View style={styles.darkBackground} />
            )}

            <PanGestureHandler
              ref={this.drawerheader}
              simultaneousHandlers={[this.scroll, this.masterdrawer]}
              shouldCancelWhenOutside={false}
              onGestureEvent={this._onGestureEvent}
              onHandlerStateChange={this._onHeaderHandlerStateChange}
            >
              <Animated.View style={styles.header}>
                <TouchableOpacity
                  style={styles.hideButton}
                  activeOpacity={0.95}
                  onPress={this.hide}
                >
                  <Image
                    style={styles.arrowDownIcon}
                    source={require('assets/icons/big-arrow-down.png')}
                  />
                </TouchableOpacity>
              </Animated.View>
            </PanGestureHandler>
            <PanGestureHandler
              ref={this.drawer}
              simultaneousHandlers={[this.scroll, this.masterdrawer]}
              shouldCancelWhenOutside={false}
              onGestureEvent={this._onGestureEvent}
              onHandlerStateChange={this._onHandlerStateChange}
            >
              <Animated.View style={styles.container}>
                <NativeViewGestureHandler
                  ref={this.scroll}
                  waitFor={this.masterdrawer}
                  simultaneousHandlers={this.drawer}
                >
                  <Animated.ScrollView
                    style={[
                      styles.scrollView,
                      { marginBottom: this._startPoint },
                    ]}
                    bounces={false}
                    onScrollBeginDrag={this._onRegisterLastScroll}
                    scrollEventThrottle={1}
                  >
                    <ProgressBarSelect onSelect={this._elementSelected} />
                  </Animated.ScrollView>
                </NativeViewGestureHandler>
              </Animated.View>
            </PanGestureHandler>
          </Animated.View>
        </View>
      </TapGestureHandler>
    )
  }

  expand = () => {
    this._dragY.setValue(0)
    this._lastScrollY.setValue(0)

    Animated.timing(this._translateYOffset, {
      toValue: this._startPoint,
      duration: 300,
      useNativeDriver: true,
    }).start(() => this.setState({ lastSnap: this._startPoint }))
  }

  hide = () => {
    this._lastScrollY.setValue(0)
    this._dragY.setValue(0)

    Animated.timing(this._translateYOffset, {
      toValue: this._endPoint + this._lastScrollYValue,
      duration: 300,
      useNativeDriver: true,
    }).start(() => this.setState({ lastSnap: this._endPoint }))
  }

  _elementSelected = elementId => {
    const { onSelect } = this.props

    if (isFunction(onSelect)) {
      onSelect(elementId)
    }

    this.hide()
  }

  _onHeaderHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.oldState === State.BEGAN) {
      this._lastScrollY.setValue(0)
    }

    this._onHandlerStateChange({ nativeEvent })
  }

  _onHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.oldState === State.ACTIVE) {
      let { velocityY, translationY } = nativeEvent
      translationY -= this._lastScrollYValue
      const dragToss = 0.05
      const endOffsetY =
        this.state.lastSnap + translationY + dragToss * velocityY

      let destSnapPoint = SNAP_POINTS_FROM_TOP[0]

      for (let i = 0; i < SNAP_POINTS_FROM_TOP.length; i++) {
        const snapPoint = SNAP_POINTS_FROM_TOP[i]
        const distFromSnap = Math.abs(snapPoint - endOffsetY)

        if (distFromSnap < Math.abs(destSnapPoint - endOffsetY)) {
          destSnapPoint = snapPoint
        }
      }

      this.setState({ lastSnap: destSnapPoint })
      this._translateYOffset.extractOffset()
      this._translateYOffset.setValue(translationY)
      this._translateYOffset.flattenOffset()
      this._dragY.setValue(0)

      Animated.spring(this._translateYOffset, {
        velocity: velocityY,
        tension: 68,
        friction: 12,
        toValue: destSnapPoint,
        useNativeDriver: true,
      }).start()
    }
  }
}
