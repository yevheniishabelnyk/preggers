/**
 *
 * ProviderList
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import ProviderListItem from '@/poster-compiler/components/molecules/ProviderListItem'

import styles from './styles'

export default class ProviderList extends React.Component {
  static propTypes = {
    providers: PropTypes.array,
    onToggleProvider: PropTypes.func,
  }

  render() {
    const { providers, onToggleProvider } = this.props

    return (
      <View style={styles.container}>
        {providers.map(item => (
          <ProviderListItem
            key={item.name}
            name={item.name}
            icon={item.icon}
            active={item.active}
            error={item.error}
            disabled={item.disabled}
            onToggle={onToggleProvider}
          />
        ))}
      </View>
    )
  }
}
