/**
 *
 * ProgressBarSelect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, TouchableOpacity } from 'react-native'
import PregnancyProgressBars from '@/poster-compiler/elements/pregnancy-progress-bars'

import Pregnancy from '@/app/database/models/Pregnancy'

import { getPregnancy } from '@/app/redux/selectors'

import styles, { badgeWidth } from './styles'

const getPregnancyProgress = Pregnancy.makeGetProgress()

@connect(state => {
  const userPregnancyId = state.firebase.profile.pregnancy

  if (userPregnancyId) {
    const pregnancy = getPregnancy(state, userPregnancyId)

    let isPregnancyOverDue, isPregnancyFifteenDaysOverDue

    if (pregnancy) {
      const pregnancyProgress = getPregnancyProgress(
        pregnancy.dueDate,
        pregnancy.lengthType
      )

      isPregnancyOverDue = pregnancyProgress.passedDays > 280
      isPregnancyFifteenDaysOverDue = pregnancyProgress.passedDays > 294
    }

    return {
      userPregnancyId,
      isPregnancyOverDue,
      isPregnancyFifteenDaysOverDue,
    }
  }

  return {}
})
export default class ProgressBarSelect extends React.Component {
  static propTypes = {
    userPregnancyId: PropTypes.string,
    isPregnancyOverDue: PropTypes.bool,
    isPregnancyFifteenDaysOverDue: PropTypes.bool,
    onSelect: PropTypes.func,
  }

  render() {
    const {
      userPregnancyId,
      isPregnancyOverDue,
      isPregnancyFifteenDaysOverDue,
    } = this.props

    if (!userPregnancyId || isPregnancyFifteenDaysOverDue) {
      return null
    }

    const context = {
      pregnancyId: userPregnancyId,
      width: badgeWidth,
    }

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <TouchableOpacity
            style={styles.oddItem}
            activeOpacity={0.95}
            onPress={this._fruitBarPressed}
          >
            <PregnancyProgressBars.PregnancyProgressFruit
              context={context}
              width={1}
              theme="#000000"
              backgroundColor="transparent"
            />
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={0.95} onPress={this._weekBarPressed}>
            <PregnancyProgressBars.PregnancyProgressWeek
              context={context}
              width={1}
              theme="#000000"
              backgroundColor="transparent"
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.oddItem}
            activeOpacity={0.95}
            onPress={this._dayBarPressed}
          >
            <PregnancyProgressBars.PregnancyProgressDay
              context={context}
              width={1}
              theme="#000000"
              backgroundColor="transparent"
            />
          </TouchableOpacity>

          {!isPregnancyOverDue ? (
            <TouchableOpacity
              activeOpacity={0.95}
              onPress={this._percentageBarPressed}
            >
              <PregnancyProgressBars.PregnancyProgressPercentage
                context={context}
                width={1}
                theme="#000000"
                backgroundColor="transparent"
              />
            </TouchableOpacity>
          ) : null}

          <TouchableOpacity
            style={!isPregnancyOverDue ? styles.oddItem : null}
            activeOpacity={0.95}
            onPress={this._babyBarPressed}
          >
            <PregnancyProgressBars.PregnancyProgressBaby
              context={context}
              width={1}
              theme="#000000"
              backgroundColor="transparent"
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={isPregnancyOverDue ? styles.oddItem : null}
            activeOpacity={0.95}
            onPress={this._summaryBarPressed}
          >
            <PregnancyProgressBars.PregnancyProgressSummary
              context={context}
              width={1}
              theme="#000000"
              backgroundColor="transparent"
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _fruitBarPressed = () => this.props.onSelect('PregnancyProgressFruit')

  _weekBarPressed = () => this.props.onSelect('PregnancyProgressWeek')

  _dayBarPressed = () => this.props.onSelect('PregnancyProgressDay')

  _percentageBarPressed = () =>
    this.props.onSelect('PregnancyProgressPercentage')

  _babyBarPressed = () => this.props.onSelect('PregnancyProgressBaby')

  _summaryBarPressed = () => this.props.onSelect('PregnancyProgressSummary')
}
