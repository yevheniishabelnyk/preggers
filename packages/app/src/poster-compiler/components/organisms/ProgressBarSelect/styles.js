import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'

const screenWidth = Dimensions.get('window').width
const contentWidth = moderateScale(318.75, 0.9)
const oddItemMarginRight = (screenWidth - contentWidth) / 2
export const badgeWidth = (screenWidth - oddItemMarginRight * 3) / 2 - 1

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },

  content: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: contentWidth,
    paddingTop: moderateScale(42),
  },

  oddItem: {
    marginRight: oddItemMarginRight,
    marginBottom: moderateScale(52),
  },
})

export default styles
