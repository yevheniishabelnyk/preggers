import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {},

  buttons: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: moderateScale(17),
  },

  palette: {
    marginTop: moderateScale(9),
    marginBottom: moderateScale(5),
  },

  lastButton: {
    marginLeft: moderateScale(18),
    marginRight: moderateScale(15),
  },
})

export default styles
