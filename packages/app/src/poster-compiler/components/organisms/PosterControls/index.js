/**
 *
 * PosterControls
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import Button from '@/poster-compiler/components/atoms/Button'
import Palette from '@/poster-compiler/components/molecules/Palette'

import randomColor from 'randomcolor'

import { isFunction } from 'lodash'

import messages from './messages'

import styles from './styles'

export default class PosterControls extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    onSaveButtonPress: PropTypes.func,
    onShareButtonPress: PropTypes.func,
    onBackgroundColorSelect: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    activeBackgroundColor: '#ffffff',
  }

  _paletteColors = ['#ffffff'].concat(
    randomColor({
      count: 100,
      luminosity: 'random',
      hue: 'random',
    })
  )

  render() {
    const { style, onSaveButtonPress, onShareButtonPress } = this.props
    const { activeBackgroundColor } = this.state

    const { formatMessage } = this.context.intl

    return (
      <View style={[styles.container, style]}>
        <View style={styles.buttons}>
          <Button
            onPress={onSaveButtonPress}
            title={formatMessage(messages.saveButtonTitle)}
          />

          <Button
            style={styles.lastButton}
            onPress={onShareButtonPress}
            title={formatMessage(messages.shareButtonTitle)}
          />
        </View>

        <Palette
          colors={this._paletteColors}
          activeColor={activeBackgroundColor}
          onSelect={this._backgroundColorSelected}
          scrollDirection="horizontal"
          style={styles.palette}
        />
      </View>
    )
  }

  _backgroundColorSelected = color => {
    const { onBackgroundColorSelect } = this.props

    this.setState({ activeBackgroundColor: color })

    if (isFunction(onBackgroundColorSelect)) {
      onBackgroundColorSelect(color)
    }
  }
}
