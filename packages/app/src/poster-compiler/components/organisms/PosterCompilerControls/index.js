/**
 *
 * PosterCompilerControls
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import ControlsSectionButton from '@/poster-compiler/components/atoms/ControlsSectionButton'
// import Palette from '@/poster-compiler/components/molecules/Palette'
import PhotoFilters from '@/poster-compiler/components/molecules/PhotoFilters'

import randomColor from 'randomcolor'

import { isFunction } from 'lodash'

import styles from './styles'

export default class PosterCompilerControls extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    onPhotoFilterSelect: PropTypes.func,
    onBadgesButtonPress: PropTypes.func,
    activePhotoFilter: PropTypes.string,
    onAddElement: PropTypes.func,
    onUpdateSelectedElement: PropTypes.func,
    activeSection: PropTypes.string,
    photoPreview: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    activeSection: this.props.activeSection || 'photo-filters',
  }

  _paletteColors = ['#ffffff'].concat(
    randomColor({
      count: 100,
      luminosity: 'random',
      hue: 'random',
    })
  )

  render() {
    const {
      style,
      photoPreview,
      onPhotoFilterSelect,
      activePhotoFilter,
    } = this.props
    const { activeSection } = this.state

    return (
      <View style={[styles.container, style]}>
        <View style={styles.sections}>
          <ControlsSectionButton
            style={styles.sectionButton}
            icon={require('assets/icons/magic-wand-black.png')}
            activeIcon={require('assets/icons/magic-wand-white.png')}
            onPress={this._photoFiltersSectionButtonPressed}
            isActive={activeSection === 'photo-filters'}
          />

          <ControlsSectionButton
            style={styles.sectionButton}
            icon={require('assets/icons/smile-black.png')}
            activeIcon={require('assets/icons/smile-white.png')}
            onPress={this._badgesSectionButtonPressed}
            isActive={activeSection === 'badges'}
          />

          {/*<ControlsSectionButton
            style={styles.sectionButton}
            iconStyle={styles.textsSectionIcon}
            icon={require('assets/icons/text-black.png')}
            activeIcon={require('assets/icons/text-white.png')}
            onPress={this._textsSectionButtonPressed}
            isActive={activeSection === 'texts'}
          />*/}
        </View>

        <View style={styles.sectionControls}>
          <PhotoFilters
            photo={photoPreview}
            onSelect={onPhotoFilterSelect}
            activePhotoFilter={activePhotoFilter}
          />
          {/*<Palette
            colors={this._paletteColors}
            // activeColor={activeBackgroundColor}
            onSelect={this._backgroundColorSelected}
            scrollDirection="horizontal"
            style={styles.palette}
          />*/}
        </View>
      </View>
    )
  }

  _photoFiltersSectionButtonPressed = () => {
    const { activeSection } = this.state

    if (activeSection !== 'photo-filters') {
      this.setState({ activeSection: 'photo-filters' })
    }
  }

  _badgesSectionButtonPressed = () => {
    const { activeSection } = this.state

    if (activeSection !== 'badges') {
      this.setState({ activeSection: 'badges' })
    }

    const { onBadgesButtonPress } = this.props

    if (isFunction(onBadgesButtonPress)) {
      onBadgesButtonPress()
    }
  }

  _textsSectionButtonPressed = () => {
    const { activeSection } = this.state

    if (activeSection !== 'texts') {
      this.setState({ activeSection: 'texts' })
    }
  }
}
