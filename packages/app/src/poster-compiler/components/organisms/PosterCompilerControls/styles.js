import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {},

  sections: {
    height: moderateScale(72),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f9fafb',
    justifyContent: 'flex-start',
    paddingLeft: moderateScale(24),
  },

  sectionButton: {
    marginRight: moderateScale(26),
  },

  textsSectionIcon: {
    width: moderateScale(39),
    height: moderateScale(22),
  },

  sectionControls: {
    height: moderateScale(121),
    backgroundColor: 'white',
    justifyContent: 'center',
  },
})

export default styles
