/**
 *
 * ViewPort
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Animated } from 'react-native'

import styles, { borderWidth } from './styles'

const borders = borderWidth * 2

export default class ViewPort extends React.Component {
  static propTypes = {
    aligment: PropTypes.string,
    viewWidth: PropTypes.number.isRequired,
    viewHeight: PropTypes.number.isRequired,
    viewAspectRatio: PropTypes.number,
    activeAreaAspectRatio: PropTypes.number,
    style: PropTypes.any,
  }

  static defaultProps = {
    aligment: 'center',
    activeAreaAspectRatio: 1,
    viewAspectRatio: 1,
  }

  constructor(props) {
    super(props)

    const viewSize = this._getViewSize(props.activeAreaAspectRatio)

    this._viewWidthValue = new Animated.Value(viewSize.width)
    this._viewHeightValue = new Animated.Value(viewSize.height)
  }

  render() {
    const { viewWidth, viewHeight, style } = this.props

    const animatedViewSize = {
      width: this._viewWidthValue,
      height: this._viewHeightValue,
    }

    return (
      <View
        style={[
          styles.container,
          style,
          { width: viewWidth + borders, height: viewHeight + borders },
        ]}
      >
        <View style={styles.edge} />

        <View style={styles.row}>
          <View style={styles.edge} />

          <Animated.View style={[styles.view, animatedViewSize]} />

          <View style={styles.edge} />
        </View>

        <View style={styles.edge} />
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.activeAreaAspectRatio !== nextProps.activeAreaAspectRatio) {
      this._animate(nextProps.activeAreaAspectRatio)
    }
  }

  _animate = newActiveAreaAspectRatio => {
    const newViewSize = this._getViewSize(newActiveAreaAspectRatio)

    Animated.parallel([
      Animated.timing(this._viewWidthValue, {
        toValue: newViewSize.width,
        duration: 300,
      }),
      Animated.timing(this._viewHeightValue, {
        toValue: newViewSize.height,
        duration: 300,
      }),
    ]).start()
  }

  _getViewSize = activeAreaAspectRatio => {
    const { viewWidth, viewHeight, viewAspectRatio } = this.props

    let width, height

    if (activeAreaAspectRatio < viewAspectRatio) {
      height = viewHeight + borders
      width = height * activeAreaAspectRatio
    } else {
      width = viewWidth + borders
      height = width / activeAreaAspectRatio
    }

    return { width, height }
  }
}
