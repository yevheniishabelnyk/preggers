import { StyleSheet } from 'react-native'

export const borderWidth = 1

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 2,
    alignItems: 'stretch',
    overflow: 'hidden',
    marginLeft: -borderWidth,
    marginTop: -borderWidth,
  },

  view: {
    borderWidth,
    borderColor: 'white',
  },

  edge: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },

  row: {
    flexDirection: 'row',
  },
})

export default styles
