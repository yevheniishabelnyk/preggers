/**
 *
 * PosterBackgroundControlsButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity, Image } from 'react-native'

import styles from './styles'

const PosterBackgroundControlsButton = ({ icon, style, onPress }) => (
  <TouchableOpacity
    style={[styles.container, style]}
    onPress={onPress}
    activeOpacity={0.95}
  >
    <View style={styles.iconContainer}>
      <Image style={styles.icon} source={icon} resizeMode="contain" />
    </View>
  </TouchableOpacity>
)

PosterBackgroundControlsButton.propTypes = {
  onPress: PropTypes.func,
  style: PropTypes.any,
  icon: PropTypes.any,
}

export default PosterBackgroundControlsButton
