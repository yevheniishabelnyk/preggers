import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const buttonWidth = moderateScale(45)

const styles = StyleSheet.create({
  container: {
    width: buttonWidth,
    height: buttonWidth,
    alignItems: 'center',
    justifyContent: 'center',
    padding: moderateScale(5),
  },

  iconContainer: {
    flex: 1,
    alignSelf: 'stretch',
  },

  icon: {
    flex: 1,
    width: null,
    height: null,
  },
})

export default styles
