/**
 *
 * TransparentButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Text } from 'react-native'

import styles from './styles'

const TransparentButton = ({ onPress, title, icon }) => (
  <TouchableOpacity
    onPress={onPress}
    style={styles.container}
    activeOpacity={1}
  >
    <Text style={styles.title}>{title}</Text>

    {icon}
  </TouchableOpacity>
)

TransparentButton.propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.string,
  icon: PropTypes.element,
}

export default TransparentButton
