import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    width: moderateScale(203),
    height: moderateScale(47),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.62)',
    borderRadius: moderateScale(23.5),
    overflow: 'hidden',
    flexDirection: 'row',
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(14),
    color: 'white',
  },
})

export default styles
