/**
 *
 * AspectRatioSelectOption
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Animated, TouchableOpacity } from 'react-native'

import { isFunction } from 'lodash'

import styles, {
  defaultBackgroundColor,
  activeBackgroundColor,
  defaultBorderColor,
  defaultFontColor,
} from './styles'

export default class AspectRatioSelectOption extends React.Component {
  static propTypes = {
    containerHeight: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    isSelected: PropTypes.bool,
    style: PropTypes.any,
    onSelect: PropTypes.func,
  }

  constructor(props) {
    super(props)

    const initialBackgroundValue = props.isSelected ? 1 : 0

    this.animatedValue = new Animated.Value(initialBackgroundValue)
  }

  render() {
    const { style, width, height, containerHeight } = this.props

    const aspectRatio = width / height

    const backgroundColor = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [defaultBackgroundColor, activeBackgroundColor],
    })

    const borderColor = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [defaultBorderColor, activeBackgroundColor],
    })

    const fontColor = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [defaultFontColor, defaultBackgroundColor],
    })

    const animatedBackgroundStyle = { backgroundColor, borderColor }
    const animatedFontStyle = { color: fontColor }

    return (
      <TouchableOpacity
        style={[
          styles.container,
          style,
          { width: containerHeight * aspectRatio },
        ]}
        activeOpacity={0.95}
        onPress={this._optionPressed}
      >
        <Animated.View
          style={[styles.contentContainer, animatedBackgroundStyle]}
        >
          <Animated.Text style={[styles.title, animatedFontStyle]}>
            {width}:{height}
          </Animated.Text>
        </Animated.View>
      </TouchableOpacity>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isSelected !== nextProps.isSelected) {
      this._animateSelection(nextProps.isSelected)
    }
  }

  _optionPressed = () => {
    const { onSelect, width, height } = this.props

    if (isFunction(onSelect)) {
      onSelect(width / height)
    }
  }

  _animateSelection = isSelected =>
    Animated.timing(this.animatedValue, {
      toValue: isSelected ? 1 : 0,
      duration: 200,
    }).start()
}
