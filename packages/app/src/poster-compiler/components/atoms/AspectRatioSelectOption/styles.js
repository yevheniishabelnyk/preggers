import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const activeBackgroundColor = 'rgb(255, 68, 102)'
export const defaultBackgroundColor = 'rgb(255, 255, 255)'
export const defaultBorderColor = 'rgb(151, 151, 151)'
export const defaultFontColor = 'rgb(15, 15, 15)'

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },

  contentContainer: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#9f9f9f',
    backgroundColor: defaultBackgroundColor,
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    color: defaultFontColor,
  },
})

export default styles
