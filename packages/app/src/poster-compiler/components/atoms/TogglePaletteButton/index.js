/**
 *
 * TogglePaletteButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import styles, { buttonWidth } from './styles'

export default class TogglePaletteButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    value: PropTypes.bool,
  }

  static width = buttonWidth

  render() {
    return (
      <TouchableOpacity
        style={styles.button}
        activeOpacity={1}
        onPress={this._buttonPressed}
      >
        <Image
          source={require('assets/icons/palette-white.png')}
          style={styles.icon}
        />
      </TouchableOpacity>
    )
  }

  _buttonPressed = () => {
    const { value, onPress } = this.props

    onPress(!value)
  }
}
