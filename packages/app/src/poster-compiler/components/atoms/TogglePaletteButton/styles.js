import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const iconWidth = moderateScale(22)
export const buttonWidth = moderateScale(37)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },

  icon: {
    width: iconWidth,
    height: iconWidth,
    marginBottom: moderateScale(3),
  },
})

export default styles
