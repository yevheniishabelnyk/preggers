import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const buttonWidth = moderateScale(89)
const whiteCircleWidth = moderateScale(66)

const styles = StyleSheet.create({
  container: {
    width: buttonWidth,
    height: buttonWidth,
    overflow: 'hidden',
  },

  grayCircle: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    backgroundColor: '#cccccc',
    alignItems: 'center',
    justifyContent: 'center',
  },

  whiteCircle: {
    width: whiteCircleWidth,
    height: whiteCircleWidth,
    borderRadius: whiteCircleWidth / 2,
    backgroundColor: 'white',
  },
})

export default styles
