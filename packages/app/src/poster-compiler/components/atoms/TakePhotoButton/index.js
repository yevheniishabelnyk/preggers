/**
 *
 * TakePhotoButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import styles from './styles'

export default class TakePhotoButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { style, onPress } = this.props

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <View style={styles.grayCircle}>
          <View style={styles.whiteCircle} />
        </View>
      </TouchableOpacity>
    )
  }
}
