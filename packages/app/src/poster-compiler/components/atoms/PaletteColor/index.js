/**
 *
 * PaletteColor
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View } from 'react-native'

import { isFunction } from 'lodash'

import styles, { spaceWidth } from './styles'

export default class PaletteColor extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    value: PropTypes.string.isRequired,
    isActive: PropTypes.bool,
  }

  static width = spaceWidth

  render() {
    const { value, isActive } = this.props

    return (
      <TouchableOpacity
        style={[styles.button, isActive ? styles.active : null]}
        activeOpacity={0.95}
        onPress={this._colorPressed}
      >
        <View
          style={[
            styles.color,
            {
              backgroundColor: value,
            },
          ]}
        />
      </TouchableOpacity>
    )
  }

  _colorPressed = () => {
    const { onPress, value } = this.props

    if (isFunction(onPress)) {
      onPress(value)
    }
  }
}
