import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const spaceWidth = moderateScale(52)
const buttonWidth = moderateScale(43)
const colorWidth = moderateScale(35)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: 'transparent',
    marginLeft: moderateScale(9),
    marginVertical: moderateScale(3),
    alignItems: 'center',
    justifyContent: 'center',
  },

  active: {
    borderColor: 'black',
  },

  color: {
    width: colorWidth,
    height: colorWidth,
    borderRadius: colorWidth / 2,
    overflow: 'hidden',
  },
})

export default styles
