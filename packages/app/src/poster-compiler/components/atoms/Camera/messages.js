import { defineMessages } from 'react-intl'

export default defineMessages({
  denideOnCameraRoll:
    'Hey! You might want to enable Camera Roll for Preggers in your phone settings.',
  denideOnCamera:
    'Hey! You might want to enable Camera for Preggers in your phone settings.',
})
