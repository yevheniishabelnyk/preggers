import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const defaultWidth = moderateScale(375)

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    backgroundColor: 'black',
    width: defaultWidth,
    height: defaultWidth,
  },

  camera: {
    ...StyleSheet.absoluteFillObject,
  },
})

export default styles
