/**
 *
 * CameraView
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Platform } from 'react-native'
import { Camera, Permissions } from 'expo'

import messages from './messages'

import styles from './styles'

export default class CameraView extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    forwardedRef: PropTypes.func,
    type: PropTypes.number,
    flashMode: PropTypes.number,
    autoFocus: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
    zoom: PropTypes.number,
    whiteBalance: PropTypes.number,
    focusDepth: PropTypes.number,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    type: Camera.Constants.Type.back,
    flashMode: Camera.Constants.FlashMode.off,
    autoFocus: Camera.Constants.AutoFocus.on,
    zoom: 0,
    whiteBalance: Camera.Constants.WhiteBalance.auto,
    focusDepth: 0,
    ratio: '16:9',
    // ratios: [],
  }

  state = {
    hasCameraPermission: false,
    type: this.props.type,
    flashMode: this.props.flashMode,
  }

  render() {
    const { style, autoFocus, zoom, whiteBalance, focusDepth } = this.props
    const { hasCameraPermission, type, flashMode } = this.state

    return (
      <View style={[styles.container, style]}>
        {hasCameraPermission ? (
          <Camera
            style={styles.camera}
            ref={ref => (this._cameraInstance = ref)}
            type={type}
            flashMode={flashMode}
            autoFocus={autoFocus}
            zoom={zoom}
            whiteBalance={whiteBalance}
            focusDepth={focusDepth}
            // ratio={this.state.ratio}
            // onCameraReady={(d) => console.info(d)}
          />
        ) : null}
      </View>
    )
  }

  async componentDidMount() {
    try {
      const { status } = await Permissions.askAsync(Permissions.CAMERA)

      this.setState({ hasCameraPermission: status === 'granted' })

      if (status !== 'granted') {
        const { formatMessage } = this.context.intl

        alert(formatMessage(messages.denideOnCamera))
      }
    } catch (err) {
      console.log('err', err)
    }

    if (Platform.OS === 'android') {
      try {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)

        this.setState({ hasCameraPermission: status === 'granted' })

        if (status !== 'granted') {
          const { formatMessage } = this.context.intl

          alert(formatMessage(messages.denideOnCamera))
        }
      } catch (err) {
        console.log('err', err)
      }
    }
  }

  takePictureAsync = options => {
    if (this._cameraInstance) {
      return this._cameraInstance.takePictureAsync(options)
    }

    return Promise.reject('Error: No Camera instance!')
  }

  toggleFlashMode = () =>
    this.setState(prevState => ({
      flashMode:
        prevState.flashMode === Camera.Constants.FlashMode.off
          ? Camera.Constants.FlashMode.on
          : Camera.Constants.FlashMode.off,
    }))

  switchCamera = () =>
    this.setState({
      type:
        this.state.type === Camera.Constants.Type.back
          ? Camera.Constants.Type.front
          : Camera.Constants.Type.back,
    })
}
