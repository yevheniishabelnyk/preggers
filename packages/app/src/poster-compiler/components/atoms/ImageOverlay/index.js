/**
 *
 * ImageOverlay
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Animated, PanResponder, Image } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class ImageOverlay extends React.Component {
  static propTypes = {
    uri: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    onRemove: PropTypes.func,
  }

  constructor(props) {
    super(props)

    this.opacityValue = new Animated.Value(1)

    this.photoTouchResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderStart: () => {
        this.opacityValue.setValue(0)
      },
      onPanResponderRelease: () => {
        this.opacityValue.setValue(1)

        const now = Date.now()
        const DOUBLE_PRESS_DELAY = 300

        if (this.lastTap && now - this.lastTap < DOUBLE_PRESS_DELAY) {
          const { onRemove } = this.props

          if (isFunction(onRemove)) {
            onRemove()
          }
        } else {
          this.lastTap = now
        }
      },
    })
  }

  render() {
    const { uri } = this.props

    if (!uri) {
      return null
    }

    const { width, height } = this.props

    return (
      <Animated.View
        style={[styles.container, { opacity: this.opacityValue }]}
        {...this.photoTouchResponder.panHandlers}
      >
        <Image
          style={{
            width,
            height,
          }}
          source={{ uri }}
          resizeMode="contain"
          fadeDuration={0}
        />
      </Animated.View>
    )
  }
}
