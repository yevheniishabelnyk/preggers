/**
 *
 * PosterCompilerScreenWrapper
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { StatusBar, View, SafeAreaView } from 'react-native'

import isIPhoneX from '@/shared/utils/isIPhoneX'

export default class PosterCompilerScreenWrapper extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    children: PropTypes.any,
    statusBar: PropTypes.string,
  }

  constructor(props) {
    super(props)

    this._isIPhoneX = isIPhoneX()
  }

  render() {
    const { children, style } = this.props

    if (this._isIPhoneX) {
      return <SafeAreaView style={style}>{children}</SafeAreaView>
    }

    return <View style={style}>{children}</View>
  }

  componentWillMount() {
    if (!this._isIPhoneX && this.props.statusBar === 'hidden') {
      StatusBar.setHidden(true)
    }
  }

  componentWillUnmount() {
    if (!this._isIPhoneX && this.props.statusBar === 'hidden') {
      StatusBar.setHidden(false)
    }
  }
}
