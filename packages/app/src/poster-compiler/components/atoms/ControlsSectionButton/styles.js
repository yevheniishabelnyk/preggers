import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    width: moderateScale(61),
    height: moderateScale(51),
    borderRadius: moderateScale(25.5),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: { height: 6, width: -2 },
  },

  iconContainer: {
    width: moderateScale(30),
    height: moderateScale(30),
  },

  icon: {
    flex: 1,
    width: null,
    height: null,
  },

  pink: {
    backgroundColor: '#fa426a',
  },
})

export default styles
