/**
 *
 * PosterBackgroundControlsButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity, Image } from 'react-native'

import styles from './styles'

const PosterBackgroundControlsButton = ({
  isActive,
  icon,
  activeIcon,
  style,
  iconStyle,
  onPress,
}) => (
  <TouchableOpacity
    style={[styles.container, isActive ? styles.pink : null, style]}
    onPress={onPress}
    activeOpacity={0.95}
  >
    <View style={[styles.iconContainer, iconStyle]}>
      <Image style={styles.icon} source={isActive ? activeIcon : icon} />
    </View>
  </TouchableOpacity>
)

PosterBackgroundControlsButton.propTypes = {
  isActive: PropTypes.bool,
  onPress: PropTypes.func,
  style: PropTypes.any,
  iconStyle: PropTypes.any,
  icon: PropTypes.any,
  activeIcon: PropTypes.any,
}

export default PosterBackgroundControlsButton
