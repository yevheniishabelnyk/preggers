/**
 *
 * colors
 *
 */

import chroma from 'chroma-js'
import predefinedColors from './backgroundColors'

const predefinedColorsList = predefinedColors.list || {}

const defaultBackgroundColor = predefinedColors.default
const defaultColorSchema = predefinedColorsList[defaultBackgroundColor] || {}

export const defaults = {
  backgroundColor: defaultBackgroundColor,
  ...defaultColorSchema,
}

export function getColors(themeInput, backgroundColor) {
  const theme = themeInput || backgroundColor || predefinedColors.default

  if (typeof predefinedColorsList[theme] === 'object') {
    if (backgroundColor) {
      return {
        ...predefinedColorsList[theme],
        backgroundColor,
      }
    }

    return predefinedColorsList[theme]
  }

  let logoColor = defaultColorSchema.logoColor,
    logoHeartColor = defaultColorSchema.logoHeartColor,
    brandColor

  const hasHighContrast = chroma.contrast(theme, 'white') > 3.4

  if (hasHighContrast) {
    brandColor = '#ffffff'
  } else {
    brandColor = chroma(theme)
      .darken(3)
      .hex()
  }

  return {
    brandColor,
    logoHeartColor,
    logoColor,
    backgroundColor,
  }
}
