import { moderateScale } from '@/app/scaling'

export const width = moderateScale(122)
export const height = moderateScale(44)
