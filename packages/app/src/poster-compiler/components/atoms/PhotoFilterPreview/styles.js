import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const previewWidth = moderateScale(60)
export const previewContainerWidth = moderateScale(66)

const styles = StyleSheet.create({
  preview: {
    width: previewWidth,
    height: previewWidth,
    borderRadius: moderateScale(5),
    overflow: 'hidden',
  },

  content: {
    paddingTop: moderateScale(3),
    paddingLeft: moderateScale(3),
    position: 'relative',
    width: previewContainerWidth,
    height: moderateScale(80),
  },

  activeBorders: {
    width: previewContainerWidth,
    height: previewContainerWidth,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(7),
    borderColor: 'rgb(31,31,31)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },

  filterName: {
    color: 'rgb(15,15,15)',
    fontSize: moderateScale(10),
    fontFamily: 'Now-Medium',
    textAlign: 'center',
    height: moderateScale(14),
    lineHeight: moderateScale(14),
    marginTop: moderateScale(5),
  },
})

export default styles
