/**
 *
 * PhotoFilterPreview
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image, View, Text } from 'react-native'
import { Surface } from 'gl-react-expo'
import * as Filters from '@/take-photo/filters'

import { isFunction } from 'lodash'
import decamelize from '@/shared/utils/decamelize'

import styles from './styles'

export default class PhotoFilterPreview extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    onPress: PropTypes.func,
    filterName: PropTypes.string.isRequired,
    isActive: PropTypes.bool,
    photo: PropTypes.oneOfType([PropTypes.number, PropTypes.object]).isRequired,
  }

  render() {
    const { style, photo, filterName, isActive } = this.props

    let preview, borders

    if (filterName === 'Normal') {
      preview = <Image style={styles.preview} source={photo} />
    } else {
      const Filter = Filters[filterName]

      preview = (
        <Surface style={styles.preview}>
          <Filter>{photo}</Filter>
        </Surface>
      )
    }

    if (isActive) {
      borders = <View style={styles.activeBorders} />
    }

    return (
      <TouchableOpacity
        style={style}
        onPress={this._previewPressed}
        activeOpacity={0.95}
      >
        <View style={styles.content}>
          {preview}

          {borders}

          <Text style={styles.filterName}>{decamelize(filterName)}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  _previewPressed = () => {
    const { onPress, filterName } = this.props

    if (isFunction(onPress)) {
      onPress(filterName)
    }
  }
}
