/**
 *
 * Button
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Text } from 'react-native'

import styles from './styles'

export default class Button extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    title: PropTypes.string,
    style: PropTypes.any,
  }

  render() {
    const { onPress, title, style } = this.props

    return (
      <TouchableOpacity
        style={[styles.button, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Text style={styles.buttonText}>{title}</Text>
      </TouchableOpacity>
    )
  }
}
