import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'white',
    paddingHorizontal: moderateScale(30),
    paddingVertical: moderateScale(15),
    borderRadius: moderateScale(98),
    alignSelf: 'center',
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: { height: 6, width: -2 },
  },

  buttonText: {
    fontFamily: 'Now-Bold',
    backgroundColor: 'transparent',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(20),
    color: 'rgb(33,32,32)',
    letterSpacing: 0,
  },
})

export default styles
