import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const containerHeight = moderateScale(69)

const styles = StyleSheet.create({
  scrollWrapper: {
    height: containerHeight + moderateScale(50),
  },

  container: {
    overflow: 'visible',
  },

  contentContainer: {
    flexDirection: 'row',
    height: containerHeight,
    alignItems: 'stretch',
    paddingHorizontal: moderateScale(35),
    marginVertical: moderateScale(25),
  },

  marginRight: {
    marginRight: moderateScale(35),
  },
})

export default styles
