/**
 *
 * AspectRatioSelect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { ScrollView, View } from 'react-native'
import AspectRatioSelectOption from '@/poster-compiler/components/atoms/AspectRatioSelectOption'

import { isFunction } from 'lodash'

import styles, { containerHeight } from './styles'

export default class AspectRatioSelect extends React.Component {
  static propTypes = {
    options: PropTypes.array,
    value: PropTypes.number,
    onChange: PropTypes.func,
    style: PropTypes.any,
  }

  static defatulProps = {
    options: [],
  }

  render() {
    const { style, value, options } = this.props

    return (
      <View style={[styles.scrollWrapper, style]}>
        <ScrollView
          horizontal
          style={[styles.container]}
          contentContainerStyle={styles.contentContainer}
        >
          {options.map((option = [], index) => (
            <AspectRatioSelectOption
              key={index}
              style={index !== options.length - 1 ? styles.marginRight : null}
              width={option[0]}
              height={option[1]}
              isSelected={value === option[0] / option[1]}
              onSelect={this._valueSelected}
              containerHeight={containerHeight}
            />
          ))}
        </ScrollView>
      </View>
    )
  }

  _valueSelected = newValue => {
    const { value, onChange } = this.props

    if (newValue !== value && isFunction(onChange)) {
      onChange(newValue)
    }
  }
}
