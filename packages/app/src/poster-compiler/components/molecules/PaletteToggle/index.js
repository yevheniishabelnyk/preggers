/**
 *
 * PaletteToggle
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Animated, TouchableOpacity, Image } from 'react-native'
import Palette from '@/poster-compiler/components/molecules/Palette'

import styles, { colorsMaxWidth } from './styles'

export default class PaletteToggle extends React.Component {
  static propTypes = {
    onSelect: PropTypes.func,
    colors: PropTypes.array,
    activeColor: PropTypes.string,
    isExpanded: PropTypes.bool,
    style: PropTypes.any,
  }

  static defaultProps = {
    colors: [],
  }

  constructor(props) {
    super(props)

    this.isExpanded = props.isExpanded

    this.colorsMaxWidthValue = new Animated.Value(
      this.isExpanded ? colorsMaxWidth : 0
    )
  }

  render() {
    const { colors, onSelect, activeColor, style } = this.props

    return (
      <View style={[styles.container, style]}>
        <TouchableOpacity
          style={styles.togglePaletteButton}
          activeOpacity={0.95}
          onPress={this._togglePaletteButtonPressed}
        >
          <Image
            source={require('assets/icons/palette-white.png')}
            style={styles.paletteIcon}
          />
        </TouchableOpacity>

        <Animated.View style={{ maxWidth: this.colorsMaxWidthValue }}>
          <Palette
            colors={colors}
            activeColor={activeColor}
            onSelect={onSelect}
            scrollDirection="horizontal"
          />
        </Animated.View>
      </View>
    )
  }

  _togglePaletteButtonPressed = () => {
    this.isExpanded = !this.isExpanded

    Animated.timing(this.colorsMaxWidthValue, {
      toValue: this.isExpanded ? colorsMaxWidth : 0,
      duration: 300,
    }).start()
  }
}
