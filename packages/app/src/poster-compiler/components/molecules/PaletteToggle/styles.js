import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const containerHeight = moderateScale(37)
const iconWidth = moderateScale(22)
export const togglePaletteButtonWidth = moderateScale(37)
export const colorsMaxWidth = moderateScale(240)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'flex-start',
    height: containerHeight,
    flexDirection: 'row',
    borderRadius: containerHeight / 2,
    backgroundColor: 'rgba(0,0,0,0.7)',
    overflow: 'hidden',
  },

  togglePaletteButton: {
    width: togglePaletteButtonWidth,
    height: togglePaletteButtonWidth,
    borderRadius: togglePaletteButtonWidth / 2,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },

  paletteIcon: {
    width: iconWidth,
    height: iconWidth,
    marginBottom: moderateScale(3),
  },
})

export default styles
