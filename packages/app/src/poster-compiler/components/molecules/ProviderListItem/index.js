/**
 *
 * ProviderListItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Text, Switch } from 'react-native'

const fbIcon = require('assets/icons/facebook.png')
const strollerIcon = require('assets/icons/stroller-logo.png')
const twitterIcon = require('assets/icons/twitter.png')

import { isFunction } from 'lodash'

import styles from './styles'

export default class ProviderListItem extends React.Component {
  static propTypes = {
    icon: PropTypes.string,
    name: PropTypes.string,
    onToggle: PropTypes.func,
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    error: PropTypes.bool,
  }

  getIcon(iconName) {
    switch (iconName) {
      case 'facebook': {
        return fbIcon
      }
      case 'stroller': {
        return strollerIcon
      }
      case 'twitter': {
        return twitterIcon
      }
      default:
        return fbIcon
    }
  }

  render() {
    const { icon, name, active, error, disabled = false } = this.props

    return (
      <View style={[styles.container, error ? styles.redBorder : null]}>
        <View style={styles.iconWrapper}>
          {icon ? (
            <Image
              source={this.getIcon(icon)}
              style={styles.icon}
              resizeMode="contain"
            />
          ) : null}
        </View>

        <Text style={styles.title}>{name}</Text>

        <Switch
          onTintColor="#40E0BE"
          style={styles.switch}
          value={active}
          onValueChange={this._valueChanged}
          disabled={disabled}
        />
      </View>
    )
  }

  _valueChanged = value => {
    const { name, onToggle } = this.props

    if (isFunction(onToggle)) {
      onToggle(name, value)
    }
  }
}
