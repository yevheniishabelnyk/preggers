import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    height: moderateScale(60),
    backgroundColor: 'white',
    borderRadius: moderateScale(8),
    overflow: 'hidden',
    marginBottom: moderateScale(13),
    paddingRight: moderateScale(15),
    borderWidth: 1,
    borderColor: 'transparent',
  },

  redBorder: {
    borderColor: 'rgb(250,65,105)',
  },

  title: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(14),
    color: 'rgb(131,146,167)',
    letterSpacing: 0.7,
    backgroundColor: 'transparent',
    marginLeft: moderateScale(3),
  },

  icon: {
    flex: 1,
    width: null,
    height: null,
  },

  iconWrapper: {
    width: moderateScale(26),
    height: moderateScale(26),
    marginHorizontal: moderateScale(15),
    alignItems: 'stretch',
    justifyContent: 'center',
  },

  switch: {
    marginLeft: 'auto',
  },
})

export default styles
