import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  palette: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginRight: moderateScale(9),
    marginVertical: moderateScale(3),
  },

  nowrap: {
    flexWrap: 'nowrap',
  },

  scrollableContent: {
    alignItems: 'center',
  },
})

export default styles
