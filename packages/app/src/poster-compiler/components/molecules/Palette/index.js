/**
 *
 * Palette
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { ScrollView, View } from 'react-native'

import PaletteColor from '@/poster-compiler/components/atoms/PaletteColor'

import styles from './styles'

export default class Palette extends React.Component {
  static propTypes = {
    colors: PropTypes.array,
    onSelect: PropTypes.func,
    activeColor: PropTypes.string,
    scrollDirection: PropTypes.string,
    style: PropTypes.any,
    contentContainerStyle: PropTypes.any,
    itemsInRow: PropTypes.number,
  }

  static defaultProps = {
    colors: [],
  }

  state = {}

  render() {
    const {
      style,
      contentContainerStyle,
      colors,
      activeColor,
      onSelect,
      scrollDirection,
    } = this.props

    const itemsInRow = this.state.itemsInRow || this.props.itemsInRow

    const palette = (
      <View
        style={[
          styles.palette,
          itemsInRow
            ? { maxWidth: itemsInRow * PaletteColor.width }
            : styles.nowrap,
        ]}
      >
        {colors.map((color, index) => (
          <PaletteColor
            key={index}
            value={color}
            isActive={color === activeColor}
            onPress={onSelect}
          />
        ))}
      </View>
    )

    if (scrollDirection === 'horizontal') {
      return (
        <ScrollView
          style={style}
          contentContainerStyle={[
            styles.scrollableContent,
            contentContainerStyle,
          ]}
          horizontal
        >
          {palette}
        </ScrollView>
      )
    }

    if (scrollDirection === 'vertical') {
      return (
        <ScrollView
          onLayout={this._onVerticalDirectionContainerLayout}
          style={style}
          contentContainerStyle={[
            styles.scrollableContent,
            contentContainerStyle,
          ]}
        >
          {this.state.itemsInRow ? palette : null}
        </ScrollView>
      )
    }

    if (scrollDirection === 'both') {
      return (
        <ScrollView style={style} contentContainerStyle={contentContainerStyle}>
          <ScrollView horizontal>{palette}</ScrollView>
        </ScrollView>
      )
    }

    return (
      <View
        style={[style, contentContainerStyle]}
        onLayout={this._onVerticalDirectionContainerLayout}
      >
        {this.state.itemsInRow ? palette : null}
      </View>
    )
  }

  _onVerticalDirectionContainerLayout = event => {
    const width = event.nativeEvent.layout.width

    this.setState({ itemsInRow: Math.floor(width / PaletteColor.width) })
  }
}
