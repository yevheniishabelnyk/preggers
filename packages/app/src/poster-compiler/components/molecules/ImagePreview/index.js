/**
 *
 * ImagePreview
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import imageGalleryModal from '@/shared/decorators/imageGalleryModal'

import styles from './styles'

@imageGalleryModal
export default class ImagePreview extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    source: PropTypes.any,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { onPress, style, source } = this.props

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Image source={source} style={styles.image} resizeMode="cover" />
      </TouchableOpacity>
    )
  }
}
