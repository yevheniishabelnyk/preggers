import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const imageWidth = moderateScale(132)

const styles = StyleSheet.create({
  container: {
    width: imageWidth,
    height: imageWidth,
  },

  image: {
    width: imageWidth,
    height: imageWidth,
    borderRadius: 8,
  },
})

export default styles
