import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(18.5, 0.3)

const styles = StyleSheet.create({
  container: {
    height: moderateScale(77, 0.3),
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    position: 'relative',
    justifyContent: 'flex-end',
    paddingHorizontal: gutter,
  },

  goBackButton: {
    position: 'absolute',
    left: gutter,
    top: moderateScale(18.5, 0.3),
  },

  lastButton: {
    marginLeft: moderateScale(18),
  },
})

export default styles
