import { defineMessages } from 'react-intl'

export default defineMessages({
  shareButtonTitle: 'Share',
  saveButtonTitle: 'Save',
  nextButtonTitle: 'Next',
})
