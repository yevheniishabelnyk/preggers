/**
 *
 * PosterCompilerHeader
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import GoBackButton from '@/onboarding/components/GoBackButton'
import CloseButton from '@/pregnancy/components/atoms/CloseButton'
import Button from '@/poster-compiler/components/atoms/Button'

import messages from './messages'

import styles from './styles'

export default class PosterCompilerHeader extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    onBackButtonPress: PropTypes.func,
    onSaveButtonPress: PropTypes.func,
    onShareButtonPress: PropTypes.func,
    onNextButtonPress: PropTypes.func,
    onCloseButtonPress: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const {
      style,
      onBackButtonPress,
      onSaveButtonPress,
      onShareButtonPress,
      onNextButtonPress,
      onCloseButtonPress,
    } = this.props
    const { formatMessage } = this.context.intl

    return (
      <View style={[styles.container, style]}>
        {onCloseButtonPress ? (
          <CloseButton
            style={styles.goBackButton}
            onPress={onCloseButtonPress}
          />
        ) : null}

        {onBackButtonPress ? (
          <GoBackButton
            style={styles.goBackButton}
            onPress={onBackButtonPress}
          />
        ) : null}

        {onSaveButtonPress ? (
          <Button
            onPress={onSaveButtonPress}
            title={formatMessage(messages.saveButtonTitle)}
          />
        ) : null}

        {onShareButtonPress ? (
          <Button
            style={styles.lastButton}
            onPress={onShareButtonPress}
            title={formatMessage(messages.shareButtonTitle)}
          />
        ) : null}

        {onNextButtonPress ? (
          <Button
            style={styles.lastButton}
            onPress={onNextButtonPress}
            title={formatMessage(messages.nextButtonTitle)}
          />
        ) : null}
      </View>
    )
  }
}
