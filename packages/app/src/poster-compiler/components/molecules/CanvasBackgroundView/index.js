/**
 *
 * CanvasBackgroundView
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, PixelRatio } from 'react-native'
import { Surface } from 'gl-react-expo'

import * as Filters from '@/take-photo/filters'

import { GLView } from 'expo'

import styles from './styles'

const pixelRatio = PixelRatio.get()

export default class CanvasBackgroundView extends React.Component {
  static propTypes = {
    width: PropTypes.number,
    aspectRatio: PropTypes.number,
    filter: PropTypes.string,
    isInitalRenderingFinished: PropTypes.bool,
    image: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    posterImage: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  }

  static defaultProps = {
    filter: 'Normal',
    aspectRatio: 1,
  }

  state = {
    isSerfaceLoadedOnce: false,
  }

  cache = {}

  preload = [this.props.image]

  render() {
    const { width } = this.props

    if (!width) {
      return null
    }

    const {
      aspectRatio,
      image,
      posterImage,
      filter,
      isInitalRenderingFinished,
    } = this.props
    const {
      isSerfaceLoadedOnce,
      isScaledContentRendered,
      scaledWidth,
    } = this.state

    const Filter = Filters[filter]

    const photoStyle = { width, height: width / aspectRatio }

    return (
      <View style={styles.container}>
        {!isSerfaceLoadedOnce || filter === 'Normal' ? (
          <View style={styles.overlay}>
            <Image style={photoStyle} source={image} />
          </View>
        ) : null}

        {isInitalRenderingFinished ? (
          <Surface
            ref={ref => (this._surface = ref)}
            style={photoStyle}
            preload={this.preload}
            onLoad={this._serfaceLoaded}
          >
            <Filter>{image}</Filter>
          </Surface>
        ) : null}

        {isScaledContentRendered ? (
          <View style={styles.hiddenContainer}>
            <View style={styles.scaledContentContainer}>
              <Surface
                ref={ref => (this._scaledSurface = ref)}
                style={{
                  width: scaledWidth,
                  height: scaledWidth / aspectRatio,
                }}
                preload={[posterImage, ...Filter.preload]}
                onLoad={this._scaledSerfaceLoaded}
              >
                <Filter>{posterImage}</Filter>
              </Surface>
            </View>
          </View>
        ) : null}
      </View>
    )
  }

  _serfaceLoaded = () => {
    console.log('_serfaceLoaded')

    this.setState({ isSerfaceLoadedOnce: true })
  }

  _scaledSerfaceLoaded = async () => {
    console.log('_scaledSerfaceLoaded')

    this._isScaledSurfaceLoaded = true
  }

  takeScaledSnapshot = scaledWidth =>
    new Promise((resolve, reject) => {
      if (!scaledWidth) {
        return reject(new Error('Error: scaled width is required!'))
      }

      const { filter, posterImage } = this.props

      if (filter === 'Normal') {
        return resolve(posterImage)
      }

      const cachedResult = this.cache[`${filter}_${scaledWidth}`]

      if (cachedResult) {
        return resolve(cachedResult)
      }

      this.setState(
        {
          isScaledContentRendered: true,
          scaledWidth: scaledWidth / pixelRatio,
        },
        () => {
          this._listenScaledSufraceLoading()
            .then(() => {
              setTimeout(() => {
                GLView.takeSnapshotAsync(this._scaledSurface.gl)
                  .then(result => {
                    resolve(result)

                    this.setState({ isScaledContentRendered: false })

                    this.cache[`${filter}_${scaledWidth}`] = result
                  })
                  .catch(err => {
                    reject(err)

                    this.setState({ isScaledContentRendered: false })
                  })
              }, 0)
            })
            .catch(err => {
              reject(err)

              this.setState({ isScaledContentRendered: false })
            })
        }
      )
    })

  _listenScaledSufraceLoading = () =>
    new Promise(resolve => {
      const interval = setInterval(() => {
        if (this._isScaledSurfaceLoaded) {
          clearInterval(interval)

          resolve()
        }
      }, 20)
    })
}
