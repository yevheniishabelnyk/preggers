import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },

  overlay: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  hiddenContainer: {
    width: 0,
    height: 0,
    position: 'relative',
    overflow: 'hidden',
  },

  scaledContentContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
})

export default styles
