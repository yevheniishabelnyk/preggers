import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    backgroundColor: 'black',
    position: 'relative',
  },

  wrapper: {
    flex: 1,
  },

  maxSpace: {
    flex: 1,
    alignSelf: 'stretch',
  },

  image: {
    flex: 1,
    width: null,
    height: null,
  },
})

export default styles
