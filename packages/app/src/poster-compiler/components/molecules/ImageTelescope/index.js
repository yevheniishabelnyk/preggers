/**
 *
 * ImageTelescope
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Animated } from 'react-native'
import ViewPort from '@/poster-compiler/components/atoms/ViewPort'

import { GestureHandler, ImageManipulator } from 'expo'
const { PanGestureHandler, State } = GestureHandler

import { clamp, isFunction } from 'lodash'

import styles from './styles'

export default class ImageTelescope extends React.Component {
  static propTypes = {
    image: PropTypes.shape({
      uri: PropTypes.string,
      width: PropTypes.number,
      height: PropTypes.number,
    }),
    aspectRatio: PropTypes.number,
    viewAspectRatio: PropTypes.number,
    onLayoutReady: PropTypes.func,
  }

  static defaultProps = {
    aspectRatio: 1,
    viewAspectRatio: 1,
  }

  panRef = React.createRef()

  state = {
    activeAreaCoords: { x1: 0, y1: 0, x2: 10000, y2: 10000 },
  }

  constructor(props) {
    super(props)

    /* Panning */
    this._lastOffset = { x: 0, y: 0 }
    this.currentOffsetX = 0
    this.currentOffsetY = 0
    this._dragY = new Animated.Value(0)
    this._dragX = new Animated.Value(0)
    this._translateYOffset = new Animated.Value(0)
    this._translateXOffset = new Animated.Value(0)
    this._onPanGestureEvent = Animated.event(
      [
        {
          nativeEvent: {
            translationX: this._dragX,
            translationY: this._dragY,
          },
        },
      ],
      { useNativeDriver: true }
    )

    console.info('props.image: ', JSON.stringify(props.image, null, 4))
  }

  render() {
    const { image, aspectRatio, viewAspectRatio } = this.props
    const { viewWidth, viewHeight } = this.state

    if (!viewWidth) {
      return <View style={styles.maxSpace} onLayout={this._onGetMaxLayout} />
    }

    const containerSize = { width: viewWidth, height: viewHeight }

    if (!image || !image.uri) {
      return <View style={[styles.container, containerSize]} />
    }

    const {
      imageSizeStyle,
      animatedTranslateX,
      animatedTranslateY,
    } = this.state

    const transformStyle = {
      transform: [
        { translateX: animatedTranslateX },
        { translateY: animatedTranslateY },
      ],
    }

    return (
      <View style={containerSize}>
        <PanGestureHandler
          ref={this.panRef}
          onGestureEvent={this._onPanGestureEvent}
          onHandlerStateChange={this._onPanHandlerStateChange}
          // minDist={10}
          maxPointers={2}
          avgTouches
        >
          <Animated.View
            style={[styles.container, containerSize]}
            collapsable={false}
          >
            <Animated.View style={[imageSizeStyle, transformStyle]}>
              <View style={imageSizeStyle}>
                <Image style={styles.image} source={image} />
              </View>
            </Animated.View>

            <ViewPort
              viewWidth={containerSize.width}
              viewHeight={containerSize.height}
              activeAreaAspectRatio={aspectRatio}
              viewAspectRatio={viewAspectRatio}
            />
          </Animated.View>
        </PanGestureHandler>
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.aspectRatio !== nextProps.aspectRatio) {
      this.setState(prevState => {
        const { viewWidth, viewHeight, imageSizeStyle } = prevState

        const activeAreaCoords = this._getActiveAreaCoords(
          nextProps.aspectRatio,
          viewWidth,
          viewHeight
        )

        const stateUpdates = { activeAreaCoords }

        const nextMinTranslateX = activeAreaCoords.x2 - imageSizeStyle.width
        const nextMaxTranslateX = activeAreaCoords.x1
        const nextMinTranslateY = activeAreaCoords.y2 - imageSizeStyle.height
        const nextMaxTranslateY = activeAreaCoords.y1

        const nextOffsetX = clamp(
          this.currentOffsetX,
          nextMinTranslateX,
          nextMaxTranslateX
        )
        const nextOffsetY = clamp(
          this.currentOffsetY,
          nextMinTranslateY,
          nextMaxTranslateY
        )

        console.info('\n\ncurrentOffsetX: ', this.currentOffsetX)
        console.info('nextOffsetX: ', nextOffsetX)
        console.log('-------------')
        console.info('currentOffsetY: ', this.currentOffsetY)
        console.info('nextOffsetY: ', nextOffsetY)

        console.info('nextMinTranslateX: ', nextMinTranslateX)
        console.info('nextMaxTranslateX: ', nextMaxTranslateX)
        console.info('nextMinTranslateY: ', nextMinTranslateY)
        console.info('nextMaxTranslateY: ', nextMaxTranslateY)

        const animations = []

        if (nextOffsetX !== this.currentOffsetX) {
          console.log('animate translateX toValue', nextOffsetX)

          animations.push(
            Animated.timing(this._translateXOffset, {
              toValue: nextOffsetX,
              duration: 300,
              useNativeDriver: true,
            })
          )
        }

        if (nextOffsetY !== this.currentOffsetY) {
          console.log('animate translateY toValue', nextOffsetY)

          animations.push(
            Animated.timing(this._translateYOffset, {
              toValue: nextOffsetY,
              duration: 300,
              useNativeDriver: true,
            })
          )
        }

        let nextAnimatedTranslateY, nextAnimatedTranslateX

        if (nextMinTranslateY === nextMaxTranslateY) {
          nextAnimatedTranslateY = this._translateYOffset
        } else {
          nextAnimatedTranslateY = Animated.add(
            this._translateYOffset,
            this._dragY
          ).interpolate({
            inputRange: [nextMinTranslateY, nextMaxTranslateY],
            outputRange: [nextMinTranslateY, nextMaxTranslateY],
            extrapolate: 'clamp',
          })
        }

        if (nextMinTranslateX === nextMaxTranslateX) {
          nextAnimatedTranslateX = this._translateXOffset
        } else {
          nextAnimatedTranslateX = Animated.add(
            this._translateXOffset,
            this._dragX
          ).interpolate({
            inputRange: [nextMinTranslateX, nextMaxTranslateX],
            outputRange: [nextMinTranslateX, nextMaxTranslateX],
            extrapolate: 'clamp',
          })
        }

        if (animations.length) {
          Animated.parallel(animations).start(() => {
            this.currentOffsetY = nextOffsetY
            this.currentOffsetX = nextOffsetX

            this.setState({
              minTranslateX: nextMinTranslateX,
              maxTranslateX: nextMaxTranslateX,
              minTranslateY: nextMinTranslateY,
              maxTranslateY: nextMaxTranslateY,
              animatedTranslateX: nextAnimatedTranslateX,
              animatedTranslateY: nextAnimatedTranslateY,
            })
          })
        } else {
          Object.assign(stateUpdates, {
            minTranslateX: nextMinTranslateX,
            maxTranslateX: nextMaxTranslateX,
            minTranslateY: nextMinTranslateY,
            maxTranslateY: nextMaxTranslateY,
            animatedTranslateX: nextAnimatedTranslateX,
            animatedTranslateY: nextAnimatedTranslateY,
          })
        }

        return stateUpdates
      })
    }
  }

  _onGetMaxLayout = event => {
    const { viewAspectRatio } = this.props

    let viewWidth = event.nativeEvent.layout.width
    let viewHeight = viewWidth / viewAspectRatio

    if (viewHeight > event.nativeEvent.layout.height) {
      viewHeight = event.nativeEvent.layout.height
      viewWidth = viewHeight * viewAspectRatio
    }

    console.info('viewWidth: ', viewWidth)
    console.info('viewHeight', viewHeight)

    const { image } = this.props
    const imageAspectRatio = image.width / image.height

    const imageSizeStyle = {}

    let translateY = 0,
      translateX = 0,
      animatedTranslateX,
      animatedTranslateY

    if (imageAspectRatio === viewAspectRatio) {
      imageSizeStyle.width = viewWidth
      imageSizeStyle.height = viewHeight
    } else if (imageAspectRatio < viewAspectRatio) {
      imageSizeStyle.width = viewWidth
      imageSizeStyle.height = imageSizeStyle.width / imageAspectRatio

      translateY = -(imageSizeStyle.height - viewHeight) / 2
    } else if (imageAspectRatio > viewAspectRatio) {
      imageSizeStyle.height = viewHeight
      imageSizeStyle.width = imageSizeStyle.height * imageAspectRatio

      translateX = -(imageSizeStyle.width - viewWidth) / 2
    }

    console.info('imageSizeStyle: ', JSON.stringify(imageSizeStyle, null, 4))

    const activeAreaCoords = this._getActiveAreaCoords(
      this.props.aspectRatio,
      viewWidth,
      viewHeight
    )

    console.info(
      'activeAreaCoords: ',
      JSON.stringify(activeAreaCoords, null, 4)
    )

    const minTranslateX = activeAreaCoords.x2 - imageSizeStyle.width
    const maxTranslateX = activeAreaCoords.x1
    const minTranslateY = activeAreaCoords.y2 - imageSizeStyle.height
    const maxTranslateY = activeAreaCoords.y1

    this.currentOffsetX = clamp(
      this.currentOffsetX + translateX,
      minTranslateX,
      maxTranslateX
    )

    console.info('currentOffsetX: ', this.currentOffsetX)
    console.info('currentOffsetY: ', this.currentOffsetY)

    this.currentOffsetY = clamp(
      this.currentOffsetY + translateY,
      minTranslateY,
      maxTranslateY
    )

    this._translateYOffset.setValue(this.currentOffsetY)

    if (minTranslateY === maxTranslateY) {
      animatedTranslateY = this._translateYOffset
    } else {
      animatedTranslateY = Animated.add(
        this._translateYOffset,
        this._dragY
      ).interpolate({
        inputRange: [minTranslateY, maxTranslateY],
        outputRange: [minTranslateY, maxTranslateY],
        extrapolate: 'clamp',
      })
    }

    this._translateXOffset.setValue(this.currentOffsetX)

    console.info('minTranslateX: ', minTranslateX)
    console.info('maxTranslateX: ', maxTranslateX)

    if (minTranslateX === maxTranslateX) {
      animatedTranslateX = this._translateXOffset
    } else {
      // const translateX = Animated.add(this._translateXOffset, this._dragX)

      // animatedTranslateX = Animated.add(
      //   translateX,
      //   Animated.multiply(
      //     Animated.diffClamp(
      //       Animated.add(translateX, -minTranslateX),
      //       -10000,
      //       0
      //     ),
      //     -1
      //   )
      // )

      animatedTranslateX = Animated.add(
        this._translateXOffset,
        this._dragX
      ).interpolate({
        inputRange: [minTranslateX, maxTranslateX],
        outputRange: [minTranslateX, maxTranslateX],
        extrapolate: 'clamp',
      })
    }

    this.setState(
      {
        viewWidth,
        viewHeight,
        imageSizeStyle,
        activeAreaCoords,
        minTranslateX,
        maxTranslateX,
        minTranslateY,
        maxTranslateY,
        animatedTranslateX,
        animatedTranslateY,
      },
      () => {
        const { onLayoutReady } = this.props

        if (isFunction(onLayoutReady)) {
          onLayoutReady()
        }
      }
    )
  }

  _onPanHandlerStateChange = event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      const {
        minTranslateY,
        maxTranslateY,
        minTranslateX,
        maxTranslateX,
      } = this.state

      this.currentOffsetX = clamp(
        this.currentOffsetX + event.nativeEvent.translationX,
        minTranslateX,
        maxTranslateX
      )

      this.currentOffsetY = clamp(
        this.currentOffsetY + event.nativeEvent.translationY,
        minTranslateY,
        maxTranslateY
      )

      console.info('currentOffsetX: ', this.currentOffsetX)
      console.info('currentOffsetY: ', this.currentOffsetY)

      this._translateYOffset.setValue(this.currentOffsetY)
      this._dragY.setValue(0)

      this._translateXOffset.setValue(this.currentOffsetX)
      this._dragX.setValue(0)
    }
  }

  cropImageToActiveArea = () => {
    const { image } = this.props

    const {
      minTranslateY,
      maxTranslateY,
      minTranslateX,
      maxTranslateX,
    } = this.state

    if (minTranslateY === maxTranslateY && minTranslateX === maxTranslateX) {
      return Promise.resolve(image)
    }

    const cropOptions = this._getCropOptions()

    console.info('cropOptions: ', JSON.stringify(cropOptions, null, 4))

    return ImageManipulator.manipulate(image.uri, [
      {
        crop: cropOptions,
      },
    ])
  }

  _getCropOptions() {
    const { imageSizeStyle, activeAreaCoords } = this.state
    const originalImageWidth = this.props.image.width

    console.info('currentOffsetX: ', this.currentOffsetX)
    console.info('currentOffsetY: ', this.currentOffsetY)

    const x1 = activeAreaCoords.x1 - this.currentOffsetX
    const y1 = activeAreaCoords.y1 - this.currentOffsetY
    const x2 = activeAreaCoords.x2 - this.currentOffsetX
    const y2 = activeAreaCoords.y2 - this.currentOffsetY

    console.info('x1: ', x1)
    console.info('y1: ', y1)
    console.info('x2: ', x2)
    console.info('y2: ', y2)

    const width = Math.floor(x2 - x1)
    const height = Math.floor(y2 - y1)

    console.info('width: ', width)
    console.info('height: ', height)

    const coordsScale = originalImageWidth / imageSizeStyle.width

    return {
      originX: x1 * coordsScale,
      originY: y1 * coordsScale,
      width: width * coordsScale,
      height: height * coordsScale,
    }
  }

  _getActiveAreaCoords(resultImageAspectRatio, viewWidth, viewHeight) {
    const visibleAreaAspectRatio = viewWidth / viewHeight
    const coords = {}
    let marginVertical = 0,
      marginHorizontal = 0

    if (resultImageAspectRatio < visibleAreaAspectRatio) {
      marginHorizontal = (viewWidth - viewHeight * resultImageAspectRatio) / 2
    } else if (resultImageAspectRatio > visibleAreaAspectRatio) {
      marginVertical = (viewHeight - viewWidth / resultImageAspectRatio) / 2
    }

    coords.y1 = 0 + marginVertical
    coords.x1 = 0 + marginHorizontal
    coords.y2 = viewHeight - marginVertical
    coords.x2 = viewWidth - marginHorizontal

    return coords
  }
}
