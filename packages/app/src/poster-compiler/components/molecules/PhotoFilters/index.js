/**
 *
 * PhotoFilterSelect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { ScrollView } from 'react-native'

import PhotoFilterPreview from '@/poster-compiler/components/atoms/PhotoFilterPreview'

import filtersList from '@/take-photo/filters/list'

import styles, { visibleFiltersNumber } from './styles'

export default class PhotoFilterSelect extends React.Component {
  static propTypes = {
    photo: PropTypes.any,
    onSelect: PropTypes.func,
    activePhotoFilter: PropTypes.string,
  }

  state = {
    activeFilter: filtersList[0],
    filtersList: filtersList.slice(0, visibleFiltersNumber),
  }

  render() {
    const { photo, onSelect, activePhotoFilter } = this.props

    if (!photo) {
      return null
    }

    const { filtersList } = this.state

    return (
      <ScrollView contentContainerStyle={styles.contentContainer} horizontal>
        {filtersList.map(name => (
          <PhotoFilterPreview
            key={name}
            style={styles.preview}
            photo={photo}
            filterName={name}
            onPress={onSelect}
            isActive={name === activePhotoFilter}
          />
        ))}
      </ScrollView>
    )
  }

  componentDidMount() {
    setTimeout(() => this.setState({ filtersList }), 1000)
  }
}
