import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'
import { previewContainerWidth } from '@/poster-compiler/components/atoms/PhotoFilterPreview/styles'

const { width } = Dimensions.get('window')

const gutter = moderateScale(6)
const itemMarginLeft = moderateScale(15)
const oneFilterAreaWidth = previewContainerWidth + itemMarginLeft
export const visibleFiltersNumber = Math.ceil(
  (width - gutter) / oneFilterAreaWidth
)

const styles = StyleSheet.create({
  contentContainer: {
    height: moderateScale(121),
    alignItems: 'center',
    paddingRight: gutter + itemMarginLeft,
    paddingLeft: gutter,
  },

  preview: {
    marginLeft: itemMarginLeft,
  },
})

export default styles
