/**
 *
 * ElementContent
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import memoize from 'fast-memoize'

export default class ElementContent extends React.Component {
  static propTypes = {
    context: PropTypes.object,
    width: PropTypes.number,
  }

  static defaultProps = {
    width: 1,
  }

  _getScale = () => {
    const { width, context } = this.props

    const elementWidth = width * context.width

    return this._memoizedGetScale(elementWidth)
  }

  _memoizedGetScale = memoize(elementWidth => {
    return elementWidth / this.defaultWidth
  })
}
