/**
 *
 * invertAspectRatio
 *
 */

export default function(aspectRatio) {
  return 1 + (1 - aspectRatio) / aspectRatio
}
