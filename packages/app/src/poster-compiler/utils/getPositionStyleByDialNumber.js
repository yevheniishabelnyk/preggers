/**
 *
 * getPositionStyleByDialNumber
 *
 */

export default function(dial, elementWidth, elementHeight) {
  const { top, left } = getPositionByDialNumber(
    dial,
    elementWidth,
    elementHeight
  )

  return {
    position: 'absolute',
    top: top * 100 + '%',
    left: left * 100 + '%',
  }
}

export function getPositionByDialNumber(dial, elementWidth, elementHeight) {
  let top, left

  switch (dial) {
    case 1:
      top = 0
      left = 0
      break

    case 2:
      top = 0
      left = (1 - elementWidth) / 2
      break

    case 3:
      top = 0
      left = 1 - elementWidth
      break

    case 4:
      top = (1 - elementHeight) / 2
      left = 0
      break

    case 5:
      top = (1 - elementHeight) / 2
      left = (1 - elementWidth) / 2
      break

    case 6:
      top = (1 - elementHeight) / 2
      left = 1 - elementWidth
      break

    case 7:
      top = 1 - elementHeight
      left = 0
      break

    case 8:
      top = 1 - elementHeight
      left = (1 - elementWidth) / 2
      break

    case 9:
      top = 1 - elementHeight
      left = 1 - elementWidth
      break

    default:
      top = 0
      left = 0
  }

  return { top, left }
}
