/**
 *
 * Canvas
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  View,
  Animated,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native'
import Element from '@/poster-compiler/containers/Element'
import { GestureHandler } from 'expo'
const {
  PanGestureHandler,
  PinchGestureHandler,
  RotationGestureHandler,
  State,
} = GestureHandler

import elements from '@/poster-compiler/elements'

import renderer from '@/poster-compiler/decorators/renderer'

import Modes from './Modes'
import AspectRatios from './AspectRatios'

import guid from '@/shared/utils/guid'
import { isFunction, clamp } from 'lodash'
import { getPositionByDialNumber } from '@/poster-compiler/utils/getPositionStyleByDialNumber'

import styles from './styles'

const ROOT_ELEMENT_ID = 'ROOT_ELEMENT_ID'
const INTERACTABLE_ELEMENTS_WRAPPER_ID = 'INTERACTABLE_ELEMENTS_WRAPPER_ID'

class Canvas extends React.Component {
  static propTypes = {
    initialState: PropTypes.shape({
      width: PropTypes.number,
      aspectRatio: PropTypes.number,
      backgroundColor: PropTypes.string,
      content: PropTypes.oneOfType([
        PropTypes.shape({
          width: PropTypes.number,
          aspectRatio: PropTypes.number,
          position: PropTypes.array,
          selfAlignment: PropTypes.number,
          backgroundColor: PropTypes.string,
          content: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.object,
            PropTypes.string,
          ]),
        }),
        PropTypes.string,
        PropTypes.array,
      ]),
    }),
    mode: PropTypes.string,
    containerStyle: PropTypes.any,
    rootContext: PropTypes.object,
    isDisabledWhileRendering: PropTypes.bool,
    isRendering: PropTypes.bool,
    onLayout: PropTypes.func,
    scale: PropTypes.number,
  }

  static defaultProps = {
    mode: Modes.EDIT,
    isDisabledWhileRendering: true,
    scale: 1,
  }

  constructor(props) {
    super(props)

    let width,
      aspectRatio,
      initialContent,
      backgroundColor,
      rootElement,
      animatedTransformScale,
      selectedElementId = ROOT_ELEMENT_ID

    if (props.initialState) {
      ;({
        width,
        aspectRatio,
        backgroundColor,
        content: initialContent,
      } = props.initialState)
    }

    if (typeof content === 'string' && elements[initialContent]) {
      aspectRatio = elements[initialContent].aspectRatio
    }

    if (!aspectRatio) {
      aspectRatio = AspectRatios.DEFAULT
    }

    this.aspectRatio = aspectRatio

    if (props.mode === Modes.EDIT) {
      this.panRef = React.createRef()
      this.pinchRef = React.createRef()
      this.rotationRef = React.createRef()
      this.panHandlerSimultaneousHandlers = [this.pinchRef, this.rotationRef]
      this.rotateHandlerSimultaneousHandlers = [this.pinchRef, this.panRef]
      this.pinchHandlerSimultaneousHandlers = [this.panRef, this.rotationRef]

      /* Panning */
      this._translateFactor = new Animated.Value(1)
      this._transformScaleFactor = 1

      this._translateXBase = new Animated.Value(0)
      this._translateXDiff = new Animated.Value(0)
      this._translateX = Animated.add(
        this._translateXBase,
        Animated.multiply(this._translateXDiff, this._translateFactor)
      )

      this._translateYBase = new Animated.Value(0)
      this._translateYDiff = new Animated.Value(0)
      this._translateY = Animated.add(
        this._translateYBase,
        Animated.multiply(this._translateYDiff, this._translateFactor)
      )

      this._onPanGestureEvent = Animated.event(
        [
          {
            nativeEvent: {
              translationX: this._translateXDiff,
              translationY: this._translateYDiff,
            },
          },
        ],
        { useNativeDriver: true }
      )

      /* Pinching */
      this.minElementScale = 0.05
      this.maxElementScale = 1
      this.activeElementMinScale = 0.5
      this.activeElementMaxScale = 2

      this._baseScale = new Animated.Value(1)
      this._pinchScale = new Animated.Value(1)
      animatedTransformScale = Animated.multiply(
        this._baseScale,
        this._pinchScale
      ).interpolate({
        inputRange: [this.activeElementMinScale, this.activeElementMaxScale],
        outputRange: [this.activeElementMinScale, this.activeElementMaxScale],
        extrapolate: 'clamp',
      })

      this._onPinchGestureEvent = Animated.event(
        [{ nativeEvent: { scale: this._pinchScale } }],
        { useNativeDriver: true }
      )

      /* Rotation */
      this._rotate = new Animated.Value(0)
      this._rotateStr = this._rotate.interpolate({
        inputRange: [-100, 100],
        outputRange: ['-100rad', '100rad'],
      })
      this._lastRotate = 0
      this._onRotateGestureEvent = Animated.event(
        [{ nativeEvent: { rotation: this._rotate } }],
        { useNativeDriver: true }
      )
    }

    if (initialContent) {
      if (initialContent.id !== ROOT_ELEMENT_ID) {
        if (!Array.isArray(initialContent)) {
          throw new Error('Error: Canvas content is not an array!')
        }

        let rootContent

        if (props.mode === Modes.EDIT) {
          const {
            interaclabeElements,
            nonInteractableElements,
          } = this._filterInitialContent(initialContent)

          if (Platform.OS === 'ios' && interaclabeElements.length) {
            selectedElementId = INTERACTABLE_ELEMENTS_WRAPPER_ID
          }

          const interaclabeElementsWrapper = {
            id: INTERACTABLE_ELEMENTS_WRAPPER_ID,
            width: 1,
            aspectRatio,
            isSelected:
              Platform.OS === 'ios' && Boolean(interaclabeElements.length),
            isRotatable: false,
            isSelectable:
              Platform.OS === 'ios' && Boolean(interaclabeElements.length),
            content: interaclabeElements,
            // backgroundColor: 'rgba(99, 188, 211, 0.4)',
          }

          rootContent = nonInteractableElements.concat(
            interaclabeElementsWrapper
          )
        } else {
          rootContent = initialContent
        }

        this._compileInitialContent(rootContent)

        rootElement = {
          backgroundColor,
          id: ROOT_ELEMENT_ID,
          width: 1,
          aspectRatio,
          content: rootContent,
          context: props.rootContext || {},
          isSelectable: false,
        }
      } else if (props.mode === Modes.RENDER) {
        rootElement = initialContent
      }
    } else {
      rootElement = {
        backgroundColor,
        id: ROOT_ELEMENT_ID,
        width: 1,
        aspectRatio,
        content: [
          {
            id: INTERACTABLE_ELEMENTS_WRAPPER_ID,
            width: 1,
            aspectRatio,
            isSelected: false,
            isRotatable: false,
            isSelectable: false,
            content: [],
            // backgroundColor: 'rgba(99, 188, 211, 0.4)',
          },
        ],
        context: props.rootContext || {},
        isSelectable: false,
      }
    }

    this.state = {
      width: props.mode !== Modes.RENDER ? null : width,
      aspectRatio,
      content: rootElement,
      selectedElementId,
      animatedTransformScale,
    }

    console.info('this.state: ', JSON.stringify(this.state, null, 4))
  }

  render() {
    const { containerStyle, mode, scale } = this.props
    const { width, content, aspectRatio } = this.state

    let canvas

    if (width) {
      const context = {
        width,
        mode,
        aspectRatio,
        height: width / aspectRatio,
        ...(content ? content.context : {}),
        transformTranslateScale: scale,
      }

      canvas = (
        <View
          style={[
            styles.canvas,
            { width: context.width, height: context.height },
          ]}
          collapsable={false}
        >
          {this._compileElement(content, context)}
        </View>
      )

      if (mode === Modes.RENDER) {
        return canvas
      }

      if (mode === Modes.EDIT) {
        canvas = (
          <PanGestureHandler
            ref={this.panRef}
            simultaneousHandlers={this.panHandlerSimultaneousHandlers}
            onGestureEvent={this._onPanGestureEvent}
            onHandlerStateChange={this._onPanHandlerStateChange}
            maxPointers={2}
            avgTouches
          >
            <Animated.View>
              <RotationGestureHandler
                ref={this.rotationRef}
                simultaneousHandlers={this.rotateHandlerSimultaneousHandlers}
                onGestureEvent={this._onRotateGestureEvent}
                onHandlerStateChange={this._onRotateHandlerStateChange}
              >
                <Animated.View>
                  <PinchGestureHandler
                    ref={this.pinchRef}
                    simultaneousHandlers={this.pinchHandlerSimultaneousHandlers}
                    onGestureEvent={this._onPinchGestureEvent}
                    onHandlerStateChange={this._onPinchHandlerStateChange}
                  >
                    <Animated.View>
                      <TouchableWithoutFeedback
                        onPress={this._rootElementPressed}
                      >
                        {canvas}
                      </TouchableWithoutFeedback>
                    </Animated.View>
                  </PinchGestureHandler>
                </Animated.View>
              </RotationGestureHandler>
            </Animated.View>
          </PanGestureHandler>
        )
      }
    }

    return (
      <View
        style={[styles.container, containerStyle]}
        onLayout={this._onLayout}
      >
        {canvas}
      </View>
    )
  }

  _compileElement(element, context) {
    const { mode } = this.props

    if (
      !element ||
      (element.canvasModeToRender && element.canvasModeToRender !== mode)
    ) {
      return null
    }

    const elementAspectRatio = this._getElementAspectRatio(element)

    const elementWidth = element.width || 1

    const elementHeight =
      (elementWidth * context.aspectRatio) / elementAspectRatio

    const elementContext = {
      ...context,
      ...element.context,
      width: elementWidth * context.width,
      height: elementHeight * context.height,
      backgroundColor: element.backgroundColor || context.backgroundColor,
      aspectRatio: elementAspectRatio,
    }

    const isSelected =
      element.isSelectable !== false &&
      element.id === this.state.selectedElementId

    const isNotInteractableElementsWrapper =
      element.id !== INTERACTABLE_ELEMENTS_WRAPPER_ID

    return (
      <Element
        ref={ref => (this[`element_container_${element.id}`] = ref)}
        key={element.id}
        id={element.id}
        width={elementWidth}
        height={elementHeight}
        elementWidth={elementContext.width}
        elementHeight={elementContext.height}
        aspectRatio={elementAspectRatio}
        position={element.position}
        transformTranslateX={element.offsetX}
        transformTranslateY={element.offsetY}
        transformTranslateScale={context.transformTranslateScale}
        transformScale={element.transformScale}
        transformRotate={element.transformRotate}
        transformScaleFactor={this._transformScaleFactor}
        animatedTransformTranslateX={
          isSelected && mode === Modes.EDIT ? this._translateX : undefined
        }
        animatedTransformTranslateY={
          isSelected && mode === Modes.EDIT ? this._translateY : undefined
        }
        animatedTransformScale={
          isSelected && mode === Modes.EDIT
            ? this.state.animatedTransformScale
            : undefined
        }
        animatedTransformScaleValue={
          isSelected && mode === Modes.EDIT ? this._pinchScale : undefined
        }
        animatedTransformRotate={
          isSelected && mode === Modes.EDIT ? this._rotateStr : undefined
        }
        animatedTransformRotateValue={
          isSelected && mode === Modes.EDIT ? this._rotate : undefined
        }
        isRotatable={element.isRotatable}
        isSelected={isSelected}
        isIsSelectedStateVisible={isNotInteractableElementsWrapper}
        isSelectable={mode === Modes.EDIT && element.isSelectable}
        selfAlignment={element.selfAlignment}
        backgroundColor={elementContext.backgroundColor}
        onPress={this._elementSelected}
        onRemove={this._elementRemoved}
        onRotationEnd={this._onRotationEnd}
        onScalingEnd={this._onScalingEnd}
      >
        {do {
          if (element.content)
            if (Array.isArray(element.content))
              element.content.map(contentItem =>
                this._compileElement(contentItem, elementContext)
              )
            else if (typeof element.content === 'object')
              this._compileElement(element.content, elementContext)
            else if (typeof element.content === 'string')
              this._renderElementContent(element, context)
        }}
      </Element>
    )
  }

  _renderElementContent(element, context) {
    const Content = elements[element.content]

    if (!Content) {
      return null
    }

    return (
      <Content
        {...element}
        context={context}
        ref={ref => (this[`element_${element.id}`] = ref)}
      />
    )
  }

  _onLayout = event => {
    let width

    const layoutWidth = event.nativeEvent.layout.width
    const layoutHeight = event.nativeEvent.layout.height
    const layoutAspectRatio = layoutWidth / layoutHeight

    const { aspectRatio, width: prevWidth } = this.state

    if (layoutAspectRatio < aspectRatio) {
      width = layoutWidth
    } else {
      width = layoutHeight * aspectRatio
    }

    if (width !== prevWidth) {
      this.setState({ width })

      const { onLayout } = this.props

      if (isFunction(onLayout)) {
        onLayout(width, width / aspectRatio)
      }
    }
  }

  _onPanHandlerStateChange = event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      const { content, selectedElementId } = this.state

      if (selectedElementId) {
        const interactableContent = content.content.find(
          item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
        )

        if (interactableContent) {
          let selectedElement,
            translateFactor = 1

          if (selectedElementId === INTERACTABLE_ELEMENTS_WRAPPER_ID) {
            selectedElement = interactableContent
          } else {
            selectedElement = interactableContent.content.find(
              item => item.id === selectedElementId
            )

            translateFactor = this._transformScaleFactor

            // need this for resize button animation
            const elementContainerRef = this[
              `element_container_${selectedElementId}`
            ]
            elementContainerRef && elementContainerRef.findCenterCoords()
          }

          if (selectedElement) {
            selectedElement.offsetX =
              (selectedElement.offsetX || 0) +
              event.nativeEvent.translationX * translateFactor

            selectedElement.offsetY =
              (selectedElement.offsetY || 0) +
              event.nativeEvent.translationY * translateFactor
          }
        }
      }

      this._translateXDiff.extractOffset()
      this._translateYDiff.extractOffset()
    }
  }

  _onPinchHandlerStateChange = event => {
    if (
      event.nativeEvent.oldState === State.ACTIVE ||
      event.nativeEvent.state === State.CANCELED
    ) {
      this._onScalingEnd(event.nativeEvent.scale)
    }
  }

  _onScalingEnd = diff => {
    const { content, selectedElementId } = this.state

    let lastScale = 1

    if (selectedElementId) {
      const interactableContent = content.content.find(
        item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
      )

      if (interactableContent) {
        let selectedElement

        if (selectedElementId === INTERACTABLE_ELEMENTS_WRAPPER_ID) {
          selectedElement = interactableContent
        } else {
          selectedElement = interactableContent.content.find(
            item => item.id === selectedElementId
          )
        }

        if (selectedElement) {
          const currentTransformScale =
            selectedElement.transformScale !== undefined
              ? selectedElement.transformScale
              : 1

          const nextTransformScale = currentTransformScale * diff

          console.info('nextTransformScale: ', nextTransformScale)

          selectedElement.transformScale = clamp(
            nextTransformScale,
            this.activeElementMinScale,
            this.activeElementMaxScale
          )

          lastScale = selectedElement.transformScale
        }
      }
    }

    console.info('lastScale: ', lastScale)

    if (selectedElementId === INTERACTABLE_ELEMENTS_WRAPPER_ID) {
      this._transformScaleFactor = 1 / lastScale
    }

    this._baseScale.setValue(lastScale)
    this._pinchScale.setValue(1)
  }

  _onRotateHandlerStateChange = event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      this._onRotationEnd(event.nativeEvent.rotation)
    }
  }

  _onRotationEnd = diff => {
    const { content, selectedElementId } = this.state

    if (selectedElementId) {
      const interactableContent = content.content.find(
        item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
      )

      if (interactableContent) {
        let selectedElement

        if (selectedElementId === INTERACTABLE_ELEMENTS_WRAPPER_ID) {
          selectedElement = interactableContent
        } else {
          selectedElement = interactableContent.content.find(
            item => item.id === selectedElementId
          )
        }

        if (selectedElement && selectedElement.isRotatable !== false) {
          selectedElement.transformRotate =
            (selectedElement.transformRotate || 0) + diff

          console.info('last rotation: ', selectedElement.transformRotate)
        }
      }
    }

    this._rotate.extractOffset()
  }

  _compileInitialContent(element) {
    if (element && typeof element !== 'string') {
      if (Array.isArray(element)) {
        element.forEach(el => this._compileInitialContent(el))
      } else if (typeof element === 'object') {
        if (!element.id) {
          element.id = guid()
        }

        const { isSelectable, isScalable = true } = element

        if (element.isSelected) {
          delete element.isSelected
        }

        if (
          isSelectable &&
          isScalable &&
          element.id !== INTERACTABLE_ELEMENTS_WRAPPER_ID
        ) {
          const { selfAlignment = 1, position } = element
          const elementWidth = element.width

          element.width = elementWidth * 2
          element.transformScale = 0.5

          if (!position && selfAlignment !== 5) {
            const elementAspectRatio = this._getElementAspectRatio(element)
            const elementHeight =
              (elementWidth * this.aspectRatio) / elementAspectRatio

            const verticalDiff = elementHeight / 2
            const horizontalDiff = elementWidth / 2

            const { top, left } = getPositionByDialNumber(
              selfAlignment,
              elementWidth,
              elementHeight
            )

            element.position = [top - verticalDiff, left - horizontalDiff]
          }
        }

        this._compileInitialContent(element.content)
      }
    }
  }

  _getElementAspectRatio(element) {
    if (
      typeof element.content === 'string' &&
      elements[element.content] &&
      elements[element.content].aspectRatio
    ) {
      return elements[element.content].aspectRatio
    }

    return element.aspectRatio || 1
  }

  _filterInitialContent(elements) {
    const interaclabeElements = []
    const nonInteractableElements = []

    elements.forEach(element => {
      let isSelectable = element.isSelectable

      if (isSelectable === undefined) {
        const {
          isMovable = true,
          isScalable = true,
          isRotatable = true,
        } = element

        isSelectable = isMovable || isScalable || isRotatable
      }

      if (isSelectable) {
        element.isSelectable = true

        interaclabeElements.push(element)
      } else {
        element.isSelectable = false

        nonInteractableElements.push(element)
      }
    })

    return {
      interaclabeElements,
      nonInteractableElements,
    }
  }

  _elementSelected = id => {
    const { mode } = this.props
    const { selectedElementId } = this.state

    if (mode === Modes.EDIT && id !== selectedElementId) {
      this.setState({ selectedElementId: null }, () => {
        if (id === null) {
          // simple element deselecting case
          return
        }

        const { content } = this.state

        let nextSelectedElement, nextMinScale, nextMaxScale

        const interactableContent = this.state.content.content.find(
          item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
        )

        // console.info('this._transformScaleFactor: ', this._transformScaleFactor)

        if (interactableContent) {
          if (id === INTERACTABLE_ELEMENTS_WRAPPER_ID) {
            if (interactableContent.isSelectable === false) {
              return
            }

            nextSelectedElement = interactableContent

            const {
              minElementTransformScaleValue,
              maxElementTransformScaleValue,
            } = interactableContent.content.reduce((out, element) => {
              const { isScalable = true } = element

              if (isScalable) {
                if (!out.minElementTransformScaleValue) {
                  out.minElementTransformScaleValue = element.transformScale
                  out.maxElementTransformScaleValue = element.transformScale
                } else {
                  if (
                    out.minElementTransformScaleValue > element.transformScale
                  ) {
                    out.minElementTransformScaleValue = element.transformScale
                  }

                  if (
                    out.maxElementTransformScaleValue < element.transformScale
                  ) {
                    out.maxElementTransformScaleValue = element.transformScale
                  }
                }
              }

              return out
            }, {})

            // console.info(
            //   'minElementTransformScaleValue: ',
            //   minElementTransformScaleValue
            // )
            // console.info(
            //   'maxElementTransformScaleValue: ',
            //   maxElementTransformScaleValue
            // )

            nextMinScale = this.minElementScale / minElementTransformScaleValue
            nextMaxScale = this.maxElementScale / maxElementTransformScaleValue

            this._translateFactor.setValue(1)
          } else {
            nextSelectedElement = interactableContent.content.find(
              item => item.id === id
            )

            nextMinScale = this.minElementScale * this._transformScaleFactor
            nextMaxScale = this.maxElementScale * this._transformScaleFactor

            this._translateFactor.setValue(this._transformScaleFactor)
          }

          if (nextSelectedElement) {
            const stateUpdates = { selectedElementId: id }

            const offsetX = nextSelectedElement.offsetX || 0
            const offsetY = nextSelectedElement.offsetY || 0
            const transformScale = nextSelectedElement.transformScale || 1
            const transformRotate = nextSelectedElement.transformRotate || 0

            this._translateXBase.setOffset(offsetX)
            this._translateXDiff.setOffset(0)
            this._translateXDiff.setValue(0)
            this._translateYBase.setOffset(offsetY)
            this._translateYDiff.setOffset(0)
            this._translateYDiff.setValue(0)

            this._baseScale.setValue(transformScale)
            this._pinchScale.setValue(1)

            // console.info('nextMinScale: ', nextMinScale)
            // console.info('nextMaxScale: ', nextMaxScale)

            if (
              nextMinScale !== this.activeElementMinScale ||
              nextMaxScale !== this.activeElementMaxScale
            ) {
              this.activeElementMinScale = nextMinScale
              this.activeElementMaxScale = nextMaxScale

              if (this.activeElementMinScale === this.activeElementMaxScale) {
                console.log('disable scaling!!!')

                stateUpdates.animatedTransformScale = this._baseScale
              } else {
                stateUpdates.animatedTransformScale = Animated.multiply(
                  this._baseScale,
                  this._pinchScale
                ).interpolate({
                  inputRange: [
                    this.activeElementMinScale,
                    this.activeElementMaxScale,
                  ],
                  outputRange: [
                    this.activeElementMinScale,
                    this.activeElementMaxScale,
                  ],
                  extrapolate: 'clamp',
                })
              }
            }

            this._rotate.setOffset(transformRotate)
            this._rotate.setValue(0)

            if (id !== INTERACTABLE_ELEMENTS_WRAPPER_ID) {
              interactableContent.content = interactableContent.content
                .filter(item => item.id !== id)
                .concat(nextSelectedElement)

              stateUpdates.content = {
                ...content,
                content: content.content
                  .filter(item => item.id !== INTERACTABLE_ELEMENTS_WRAPPER_ID)
                  .concat(interactableContent),
              }
            }

            this.setState(stateUpdates)
          }
        }
      })
    }
  }

  _rootElementPressed = () =>
    this._elementSelected(
      Platform.OS === 'ios' ? INTERACTABLE_ELEMENTS_WRAPPER_ID : null
    )

  _elementRemoved = id =>
    this.setState(
      prevState => {
        const stateUpdates = {
          selectedElementId: null,
        }

        const interactableContent = prevState.content.content.find(
          item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
        )

        interactableContent.content = interactableContent.content.filter(
          item => item.id !== id
        )

        stateUpdates.content = {
          ...prevState.content,
          content: prevState.content.content
            .filter(item => item.id !== INTERACTABLE_ELEMENTS_WRAPPER_ID)
            .concat(interactableContent),
        }

        return stateUpdates
      },
      () => {
        if (Platform.OS === 'ios') {
          const interactableContent = this.state.content.content.find(
            item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
          )

          if (interactableContent) {
            const stateUpdates = {
              selectedElementId: INTERACTABLE_ELEMENTS_WRAPPER_ID,
            }

            let offsetX, offsetY, transformScale, nextMinScale, nextMaxScale

            if (interactableContent.content.length) {
              offsetX = interactableContent.offsetX || 0
              offsetY = interactableContent.offsetY || 0
              transformScale = interactableContent.transformScale || 1

              const {
                minElementTransformScaleValue,
                maxElementTransformScaleValue,
              } = interactableContent.content.reduce((out, element) => {
                const { isScalable = true } = element

                if (isScalable) {
                  if (!out.minElementTransformScaleValue) {
                    out.minElementTransformScaleValue = element.transformScale
                    out.maxElementTransformScaleValue = element.transformScale
                  } else {
                    if (
                      out.minElementTransformScaleValue > element.transformScale
                    ) {
                      out.minElementTransformScaleValue = element.transformScale
                    }

                    if (
                      out.maxElementTransformScaleValue < element.transformScale
                    ) {
                      out.maxElementTransformScaleValue = element.transformScale
                    }
                  }
                }

                return out
              }, {})

              console.info(
                'minElementTransformScaleValue: ',
                minElementTransformScaleValue
              )
              console.info(
                'maxElementTransformScaleValue: ',
                maxElementTransformScaleValue
              )

              nextMinScale = minElementTransformScaleValue
                ? this.minElementScale / minElementTransformScaleValue
                : transformScale
              nextMaxScale = minElementTransformScaleValue
                ? this.maxElementScale / maxElementTransformScaleValue
                : transformScale

              console.info('nextMinScale: ', nextMinScale)
              console.info('nextMaxScale: ', nextMaxScale)
            } else {
              // there are no elements in interactable content wrapper
              // reseting its position and scale
              offsetX = 0
              offsetY = 0
              transformScale = 1

              nextMinScale = 0.5
              nextMaxScale = 2

              stateUpdates.selectedElementId = null
              interactableContent.offsetX = offsetX
              interactableContent.offsetY = offsetY
              interactableContent.transformScale = transformScale
              interactableContent.isSelectable = false

              this._transformScaleFactor = 1
            }

            this._translateFactor.setValue(1)
            this._translateXBase.setOffset(offsetX)
            this._translateXDiff.setOffset(0)
            this._translateXDiff.setValue(0)
            this._translateYBase.setOffset(offsetY)
            this._translateYDiff.setOffset(0)
            this._translateYDiff.setValue(0)

            this._baseScale.setValue(transformScale)
            this._pinchScale.setValue(1)

            if (
              nextMinScale !== this.activeElementMinScale ||
              nextMaxScale !== this.activeElementMaxScale
            ) {
              this.activeElementMinScale = nextMinScale
              this.activeElementMaxScale = nextMaxScale

              if (this.activeElementMinScale === this.activeElementMaxScale) {
                console.log('disable scaling!!!')

                stateUpdates.animatedTransformScale = this._baseScale
              } else {
                stateUpdates.animatedTransformScale = Animated.multiply(
                  this._baseScale,
                  this._pinchScale
                ).interpolate({
                  inputRange: [
                    this.activeElementMinScale,
                    this.activeElementMaxScale,
                  ],
                  outputRange: [
                    this.activeElementMinScale,
                    this.activeElementMaxScale,
                  ],
                  extrapolate: 'clamp',
                })
              }
            }

            this._rotate.setOffset(0)
            this._rotate.setValue(0)

            this.setState(stateUpdates)
          }
        }
      }
    )

  waitFor = () => {
    const { content } = this.state

    return this._waitForElement(content)
  }

  _waitForElement = element => {
    if (!element) {
      return Promise.resolve()
    }

    if (typeof element.content === 'string') {
      const elementRef = this[`element_${element.id}`]

      if (elementRef && elementRef.waitFor) {
        return elementRef.waitFor()
      }
    }

    if (Array.isArray(element.content)) {
      return Promise.all(
        element.content.map(item => this._waitForElement(item))
      )
    }

    if (typeof element.content === 'object') {
      return this._waitForElement(element.content)
    }

    return Promise.resolve()
  }

  setBackgroundColor = color => {
    const { isDisabledWhileRendering, isRendering } = this.props

    if (isRendering && isDisabledWhileRendering) {
      return
    }

    this.setState(prevState => {
      if (prevState.selectedElementId === ROOT_ELEMENT_ID) {
        return {
          content: {
            ...prevState.content,
            backgroundColor: color,
          },
        }
      }

      if (Array.isArray(prevState.content.content)) {
        return {
          content: {
            ...prevState.content,
            content: prevState.content.content.map(element => {
              if (element.id === prevState.selectedElementId) {
                element.backgroundColor = color
              }

              return element
            }),
          },
        }
      }

      return prevState
    })
  }

  unshift = element => {
    element.id = guid()

    let isSelectable = element.isSelectable

    if (isSelectable === undefined) {
      const {
        isMovable = true,
        isScalable = true,
        isRotatable = true,
      } = element

      isSelectable = isMovable || isScalable || isRotatable
    }

    element.isSelectable = isSelectable

    if (isSelectable) {
      const interactableContent = this.state.content.content.find(
        item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
      )

      if (interactableContent) {
        if (Platform.OS === 'ios') {
          interactableContent.isSelectable = true
        }

        const interactableContentScale = interactableContent.transformScale || 1
        const interactableContentOffsetX = interactableContent.offsetX || 0
        const interactableContentOffsetY = interactableContent.offsetY || 0

        const interactableContentLeft =
          interactableContentOffsetX / this.state.width

        const interactableContentTop =
          interactableContentOffsetY / (this.state.width / this.aspectRatio)

        const elementAspectRatio = this._getElementAspectRatio(element)

        let shouldFitToView = false

        if (!element.width) {
          shouldFitToView = true

          element.width = 0.5
        }

        let elementHeight =
          (element.width * this.aspectRatio) / elementAspectRatio

        if (shouldFitToView && elementHeight > 0.8) {
          elementHeight = 0.8
          element.width =
            (elementHeight / this.aspectRatio) * elementAspectRatio
        }

        const elementWidth = element.width

        const { selfAlignment = 5 } = element
        let top, left

        element.width = element.width * 2
        element.transformScale = 0.5 / interactableContentScale

        if (!element.position) {
          ;({ top, left } = getPositionByDialNumber(
            selfAlignment,
            elementWidth,
            elementHeight
          ))

          const verticalDiff = elementHeight / 2
          const horizontalDiff = elementWidth / 2

          top -= verticalDiff
          left -= horizontalDiff
        } else {
          ;[top, left] = element.position
        }

        element.position = [
          top - interactableContentTop / interactableContentScale,
          left - interactableContentLeft / interactableContentScale,
        ]

        interactableContent.content.unshift(element)

        this.forceUpdate()
      }
    } else {
      this.setState(prevState => ({
        content: {
          ...prevState.content,
          content: [element].concat(prevState.content.content),
        },
      }))
    }
  }

  shift = () =>
    this.setState(prevState => ({
      content: {
        ...prevState.content,
        content: prevState.content.content.slice(1),
      },
    }))

  push = element => {
    element.id = guid()

    let isSelectable = element.isSelectable

    if (isSelectable === undefined) {
      const {
        isMovable = true,
        isScalable = true,
        isRotatable = true,
      } = element

      isSelectable = isMovable || isScalable || isRotatable
    }

    element.isSelectable = isSelectable

    if (isSelectable) {
      const interactableContent = this.state.content.content.find(
        item => item.id === INTERACTABLE_ELEMENTS_WRAPPER_ID
      )

      if (interactableContent) {
        if (Platform.OS === 'ios') {
          interactableContent.isSelectable = true
        }

        const interactableContentScale = interactableContent.transformScale || 1
        const interactableContentOffsetX = interactableContent.offsetX || 0
        const interactableContentOffsetY = interactableContent.offsetY || 0

        const interactableContentLeft =
          interactableContentOffsetX / this.state.width

        const interactableContentTop =
          interactableContentOffsetY / (this.state.width / this.aspectRatio)

        const elementAspectRatio = this._getElementAspectRatio(element)

        let shouldFitToView = false

        if (!element.width) {
          shouldFitToView = true

          element.width = 0.5
        }

        let elementHeight =
          (element.width * this.aspectRatio) / elementAspectRatio

        if (shouldFitToView && elementHeight > 0.8) {
          elementHeight = 0.8
          element.width =
            (elementHeight / this.aspectRatio) * elementAspectRatio
        }

        const elementWidth = element.width

        const { selfAlignment = 5 } = element
        let top, left

        element.width = element.width * 2
        element.transformScale = 0.5 / interactableContentScale

        if (!element.position) {
          ;({ top, left } = getPositionByDialNumber(
            selfAlignment,
            elementWidth,
            elementHeight
          ))

          const verticalDiff = elementHeight / 2
          const horizontalDiff = elementWidth / 2

          top -= verticalDiff
          left -= horizontalDiff
        } else {
          ;[top, left] = element.position
        }

        element.position = [
          top - interactableContentTop / interactableContentScale,
          left - interactableContentLeft / interactableContentScale,
        ]

        interactableContent.content.push(element)

        this._elementSelected(element.id)
      }
    } else {
      this.setState(prevState => ({
        content: {
          ...prevState.content,
          content: prevState.content.content.concat(element),
        },
      }))
    }
  }
}

export default renderer({
  scalableStateValues: ['width'],
  renderableProps: {
    mode: Modes.RENDER,
  },
})(Canvas)
