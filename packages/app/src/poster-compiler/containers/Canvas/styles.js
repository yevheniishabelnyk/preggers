import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },

  canvas: {
    overflow: 'hidden',
    position: 'relative',
  },

  wrapper: {
    flex: 1,
  },
})

export default styles
