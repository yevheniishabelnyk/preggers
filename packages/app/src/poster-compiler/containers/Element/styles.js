import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const buttonWidth = moderateScale(45)
const buttonIconWidth = moderateScale(30)
const borderWidth = moderateScale(2)

const styles = StyleSheet.create({
  borders: {
    position: 'absolute',
    top: -borderWidth / 1.5,
    left: -borderWidth / 1.5,
    bottom: -borderWidth / 1.5,
    right: -borderWidth / 1.5,
    overflow: 'hidden',
    borderRadius: moderateScale(1.5),
  },

  borderTop: {
    height: borderWidth,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fa426a',
  },

  borderLeft: {
    width: borderWidth,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    backgroundColor: '#fa426a',
  },

  borderRight: {
    width: borderWidth,
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    backgroundColor: '#fa426a',
  },

  borderBottom: {
    height: borderWidth,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fa426a',
  },

  bordersWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 0,
  },

  topLeftButton: {
    position: 'absolute',
    top: -2,
    left: -2,
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    overflow: 'hidden',
    zIndex: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },

  bottomRightButton: {
    position: 'absolute',
    bottom: -2,
    right: -2,
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    overflow: 'hidden',
    zIndex: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonIcon: {
    width: buttonIconWidth,
    height: buttonIconWidth,
    borderRadius: buttonIconWidth / 2,
  },

  maxSpace: {
    flex: 1,
    alignSelf: 'stretch',
  },

  elementCenter: {
    width: 0,
    height: 0,
    position: 'absolute',
    top: '50%',
    left: '50%',
  },
})

export default styles
