/**
 *
 * Element
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  View,
  TouchableWithoutFeedback,
  Animated,
  PanResponder,
  Image,
  Platform,
} from 'react-native'

import { inRange, isFunction } from 'lodash'

import getPositionStyleByDialNumber from '@/poster-compiler/utils/getPositionStyleByDialNumber'

import memoize from 'fast-memoize'

import styles, { buttonWidth } from './styles'

export default class Element extends React.Component {
  static propTypes = {
    isSelectable: PropTypes.bool,
    isRotatable: PropTypes.bool,
    isScalable: PropTypes.bool,
    isIsSelectedStateVisible: PropTypes.bool,
    isSelected: PropTypes.bool,
    children: PropTypes.any,
    width: PropTypes.number,
    height: PropTypes.number,
    elementWidth: PropTypes.number,
    elementHeight: PropTypes.number,
    position: PropTypes.array,
    selfAlignment: PropTypes.number,
    transformTranslateX: PropTypes.number,
    transformTranslateY: PropTypes.number,
    transformTranslateScale: PropTypes.number,
    transformScale: PropTypes.number,
    transformRotate: PropTypes.number,
    backgroundColor: PropTypes.string,
    onPress: PropTypes.func,
    onRemove: PropTypes.func,
    onRotationEnd: PropTypes.func,
    onScalingEnd: PropTypes.func,
    id: PropTypes.string.isRequired,
    animatedTransformTranslateX: PropTypes.object,
    animatedTransformTranslateY: PropTypes.object,
    animatedTransformScale: PropTypes.object,
    animatedTransformRotate: PropTypes.object,
    animatedTransformRotateValue: PropTypes.object,
    animatedTransformScaleValue: PropTypes.object,
    transformScaleFactor: PropTypes.number,
  }

  static defaultProps = {
    aspectRatio: 1,
    spaceAspectRatio: 1,
    isSelectable: true,
    isRotatable: true,
    isScalable: true,
    isIsSelectedStateVisible: true,
    backgroundColor: 'transparent',
    transformTranslateX: 0,
    transformTranslateY: 0,
    transformTranslateScale: 1,
    transformScale: 1,
    transformRotate: 0,
    transformScaleFactor: 1,
  }

  constructor(props) {
    super(props)

    const {
      isIsSelectedStateVisible,
      isSelectable,
      isScalable,
      isRotatable,
    } = props

    if (
      isSelectable &&
      isIsSelectedStateVisible &&
      (isScalable || isRotatable)
    ) {
      this._isResizeButtonGestureEnabled = true

      this._resizeButtonTouchResponder = PanResponder.create({
        onStartShouldSetPanResponder: event =>
          event.nativeEvent.touches.length === 1 &&
          this._centerX !== undefined &&
          this._centerY !== undefined,

        onPanResponderStart: event => {
          const { pageX, pageY } = event.nativeEvent

          const diffX = pageX - this._centerX
          const diffY = pageY - this._centerY

          const { isScalable, isRotatable } = this.props

          if (isScalable) {
            this._resizeGestureVectorInitialLength = Math.hypot(diffX, diffY)
          }

          if (isRotatable) {
            this._resizeGestureVectorInitialAngle = Math.atan2(diffX, -diffY)
          }
        },

        onPanResponderMove: this._getResizeButtonOnMoveResponder(
          isScalable,
          isRotatable
        ),

        onPanResponderRelease: event => {
          const { pageX, pageY } = event.nativeEvent

          const diffX = pageX - this._centerX
          const diffY = pageY - this._centerY

          if (this.props.isScalable) {
            const scaleDiff =
              Math.hypot(diffX, diffY) / this._resizeGestureVectorInitialLength

            this.props.animatedTransformScaleValue.setValue(scaleDiff)

            this.props.onScalingEnd(scaleDiff)
          }

          if (this.props.isRotatable) {
            const rotationDiff =
              Math.atan2(diffX, -diffY) - this._resizeGestureVectorInitialAngle

            this.props.animatedTransformRotateValue.setValue(rotationDiff)

            this.props.onRotationEnd(rotationDiff)
          }
        },
      })
    }
  }

  render() {
    const {
      children,
      isSelectable,
      isRotatable,
      isScalable,
      isIsSelectedStateVisible,
      isSelected,
      width,
      height,
      position,
      selfAlignment,
      backgroundColor,
      transformTranslateX,
      transformTranslateY,
      transformTranslateScale,
      transformScale,
      transformRotate,
      animatedTransformTranslateX,
      animatedTransformTranslateY,
      animatedTransformScale,
      animatedTransformRotate,
      transformScaleFactor,
    } = this.props

    const positionStyle = this._getElementPositionStyle(
      width,
      height,
      position,
      selfAlignment
    )

    const elementSpace = {
      width: width * 100 + '%',
      height: height * 100 + '%',
    }

    let transformStyle, UISpaceTransformStyle

    if (
      isSelectable &&
      isSelected &&
      animatedTransformTranslateX &&
      animatedTransformTranslateY &&
      animatedTransformScale
    ) {
      let translateX, translateY

      translateX = animatedTransformTranslateX
      translateY = animatedTransformTranslateY

      transformStyle = {
        transform: [
          { translateX },
          { translateY },
          { scale: animatedTransformScale },
        ],
      }

      UISpaceTransformStyle = {
        transform: [{ translateX }, { translateY }],
      }

      if (isRotatable) {
        if (isSelected && isIsSelectedStateVisible) {
          UISpaceTransformStyle.transform.push({
            rotate: animatedTransformRotate,
          })
        } else {
          transformStyle.transform.push({ rotate: animatedTransformRotate })
        }
      }
    } else {
      const translateX = transformTranslateX * transformTranslateScale
      const translateY = transformTranslateY * transformTranslateScale

      transformStyle = {
        transform: [{ translateX }, { translateY }, { scale: transformScale }],
      }

      if (isRotatable) {
        transformStyle.transform.push({ rotate: `${transformRotate}rad` })
      }
    }

    if (isSelectable) {
      if (isSelected && isIsSelectedStateVisible) {
        const { elementWidth, elementHeight } = this.props

        const UISpaceScaleXFactor = Animated.multiply(
          Animated.divide(
            Animated.add(
              Animated.multiply(elementWidth, animatedTransformScale),
              buttonWidth * transformScaleFactor
            ),
            Animated.multiply(elementWidth, animatedTransformScale)
          ),
          animatedTransformScale
        )

        const UISpaceScaleYFactor = Animated.multiply(
          Animated.divide(
            Animated.add(
              Animated.multiply(elementHeight, animatedTransformScale),
              buttonWidth * transformScaleFactor
            ),
            Animated.multiply(elementHeight, animatedTransformScale)
          ),
          animatedTransformScale
        )

        UISpaceTransformStyle.transform.push(
          { scaleX: UISpaceScaleXFactor },
          { scaleY: UISpaceScaleYFactor }
        )

        const contentScaleX = Animated.divide(
          animatedTransformScale,
          UISpaceScaleXFactor
        )

        const contentScaleY = Animated.divide(
          animatedTransformScale,
          UISpaceScaleYFactor
        )

        const contentTransform = {
          transform: [
            {
              scaleX: contentScaleX,
              scaleY: contentScaleY,
            },
          ],
        }

        const selectedUIScaleX = Animated.multiply(
          Animated.divide(1, UISpaceScaleXFactor),
          transformScaleFactor
        )

        const selectedUIScaleY = Animated.multiply(
          Animated.divide(1, UISpaceScaleYFactor),
          transformScaleFactor
        )

        const topLeftButtonTranslateFactor = Animated.multiply(
          Animated.add(
            -1,
            Animated.divide(animatedTransformScale, transformScaleFactor)
          ),
          -buttonWidth / 2
        )

        const bottomRightButtonTranslateFactor = Animated.multiply(
          topLeftButtonTranslateFactor,
          -1
        )

        return (
          <TouchableWithoutFeedback>
            <Animated.View
              style={[
                {
                  width: elementWidth,
                  height: elementHeight,
                  // backgroundColor: 'skyblue',
                  // overflow: 'hidden',
                },
                positionStyle,
                UISpaceTransformStyle,
              ]}
              pointerEvents="box-none"
            >
              <Animated.View
                style={[
                  styles.maxSpace,
                  {
                    backgroundColor,
                    // overflow: 'hidden',
                  },
                  contentTransform,
                ]}
              >
                {children}
              </Animated.View>

              <View
                style={styles.elementCenter}
                ref={ref => (this._selectedElementCenter = ref)}
                collapsable={false}
              />

              <Animated.View style={[styles.borders, contentTransform]}>
                <Animated.View
                  style={[
                    styles.borderTop,
                    { transform: [{ scaleY: selectedUIScaleY }] },
                  ]}
                />

                <Animated.View
                  style={[
                    styles.borderLeft,
                    { transform: [{ scaleX: selectedUIScaleX }] },
                  ]}
                />

                <Animated.View
                  style={[
                    styles.borderRight,
                    { transform: [{ scaleX: selectedUIScaleX }] },
                  ]}
                />

                <Animated.View
                  style={[
                    styles.borderBottom,
                    { transform: [{ scaleY: selectedUIScaleY }] },
                  ]}
                />
              </Animated.View>

              <TouchableWithoutFeedback onPress={this._removeButtonPressed}>
                <Animated.View
                  style={[
                    styles.topLeftButton,
                    {
                      transform: [
                        {
                          scaleX: selectedUIScaleX,
                          scaleY: selectedUIScaleY,
                        },
                        {
                          translateY: topLeftButtonTranslateFactor,
                        },
                        {
                          translateX: topLeftButtonTranslateFactor,
                        },
                      ],
                    },
                  ]}
                >
                  <Image
                    style={styles.buttonIcon}
                    source={require('assets/icons/remove.png')}
                    fadeDuration={0}
                  />
                </Animated.View>
              </TouchableWithoutFeedback>

              {isScalable || isRotatable ? (
                <Animated.View
                  style={[
                    styles.bottomRightButton,
                    {
                      transform: [
                        {
                          scaleX: selectedUIScaleX,
                          scaleY: selectedUIScaleY,
                        },
                        {
                          translateY: bottomRightButtonTranslateFactor,
                        },
                        {
                          translateX: bottomRightButtonTranslateFactor,
                        },
                      ],
                    },
                  ]}
                  {...this._resizeButtonTouchResponder.panHandlers}
                >
                  <Image
                    style={styles.buttonIcon}
                    source={require('assets/icons/resize.png')}
                    fadeDuration={0}
                  />
                </Animated.View>
              ) : null}
            </Animated.View>
          </TouchableWithoutFeedback>
        )
      }

      return (
        <TouchableWithoutFeedback onPress={this._elementPressed}>
          <Animated.View style={[elementSpace, positionStyle, transformStyle]}>
            <Animated.View
              style={[
                styles.maxSpace,
                {
                  // overflow: 'hidden',
                  backgroundColor,
                },
              ]}
            >
              {children}
            </Animated.View>

            <View
              style={styles.elementCenter}
              ref={ref => (this._notSelectedElementCenter = ref)}
              collapsable={false}
            />
          </Animated.View>
        </TouchableWithoutFeedback>
      )
    }

    return (
      <View
        style={[
          elementSpace,
          positionStyle,
          transformStyle,
          { backgroundColor },
        ]}
      >
        {children}
      </View>
    )
  }

  componentDidMount() {
    if (this._isResizeButtonGestureEnabled) {
      setTimeout(() => {
        this.findCenterCoords()
      }, 0)
    }
  }

  _getResizeButtonOnMoveResponder = (isScalable, isRotatable) => {
    if (isScalable && isRotatable) {
      return event => {
        const { pageX, pageY } = event.nativeEvent.touches[0]

        const diffX = pageX - this._centerX
        const diffY = pageY - this._centerY

        this.props.animatedTransformScaleValue.setValue(
          Math.hypot(diffX, diffY) / this._resizeGestureVectorInitialLength
        )
        this.props.animatedTransformRotateValue.setValue(
          Math.atan2(diffX, -diffY) - this._resizeGestureVectorInitialAngle
        )
      }
    }

    if (isScalable) {
      return event => {
        const { pageX, pageY } = event.nativeEvent.touches[0]

        const diffX = pageX - this._centerX
        const diffY = pageY - this._centerY

        this.props.animatedTransformScaleValue.setValue(
          Math.hypot(diffX, diffY) / this._resizeGestureVectorInitialLength
        )
      }
    }

    return event => {
      const { pageX, pageY } = event.nativeEvent.touches[0]

      const diffX = pageX - this._centerX
      const diffY = pageY - this._centerY

      this.props.animatedTransformRotateValue.setValue(
        Math.atan2(diffX, -diffY) - this._resizeGestureVectorInitialAngle
      )
    }
  }

  _elementPressed = () => {
    const { onPress, id, isSelectable } = this.props

    console.log('_elementPressed', id)

    if (isSelectable && isFunction(onPress)) {
      onPress(id)

      if (this._isResizeButtonGestureEnabled) {
        this.findCenterCoords()
      }
    }
  }

  findCenterCoords = () => {
    console.log('findCenterCoords')

    if (Platform.OS === 'android' && !this.props.isSelected) {
      this._notSelectedElementCenter &&
        this._notSelectedElementCenter.measure(
          (fx, fy, width, height, pageX, pageY) => {
            this._centerX = pageX
            this._centerY = pageY
          }
        )
    } else {
      setTimeout(() => {
        this._selectedElementCenter &&
          this._selectedElementCenter.measure(
            (fx, fy, width, height, pageX, pageY) => {
              this._centerX = pageX
              this._centerY = pageY
            }
          )
      }, 0)
    }
  }

  _removeButtonPressed = () => {
    console.log('_removeButtonPressed')

    const { onRemove, id } = this.props

    if (isFunction(onRemove)) {
      onRemove(id)
    }
  }

  _getElementPositionStyle(
    elementWidth,
    elementHeight,
    elementPosition,
    elementSelfAligment
  ) {
    return this._memoizedGetElementPositionStyle(
      elementWidth,
      elementHeight,
      elementPosition,
      elementSelfAligment
    )
  }

  _memoizedGetElementPositionStyle = memoize(
    (elementWidth, elementHeight, elementPosition, elementSelfAligment) => {
      if (elementPosition && elementPosition.length === 2) {
        const top = elementPosition[0]
        const left = elementPosition[1]

        return {
          position: 'absolute',
          top: top * 100 + '%',
          left: left * 100 + '%',
        }
      }

      if (inRange(elementSelfAligment, 1, 10)) {
        return getPositionStyleByDialNumber(
          elementSelfAligment,
          elementWidth,
          elementHeight
        )
      }

      return {
        position: 'absolute',
        top: 0,
        left: 0,
      }
    }
  )
}
