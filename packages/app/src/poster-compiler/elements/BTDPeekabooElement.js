/**
 *
 * BTDPosterElement
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import Peekaboo from '@/poster-compiler/components/BTD/Peekaboo'
import ElementContent from '@/poster-compiler/components/base/ElementContent'

export default class BTDPeekabooElement extends ElementContent {
  static propTypes = {
    context: PropTypes.object,
    backgroundColor: PropTypes.string,
  }

  static aspectRatio = Peekaboo.aspectRatio
  static defaultWidth = Peekaboo.defaultWidth

  aspectRatio = Peekaboo.aspectRatio
  defaultWidth = Peekaboo.defaultWidth

  render() {
    const { backgroundColor, context } = this.props

    const scale = this._getScale()

    return (
      <Peekaboo
        {...context}
        backgroundColor={backgroundColor || context.backgroundColor}
        scale={scale}
      />
    )
  }
}
