import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  image: {
    width: null,
    height: null,
    flex: 1,
  },
})

export default styles
