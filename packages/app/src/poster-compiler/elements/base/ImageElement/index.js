/**
 *
 * BTDPosterElement
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Platform } from 'react-native'

import CanvasModes from '@/poster-compiler/containers/Canvas/Modes'

import styles from './styles'

export default class ImageElement extends React.Component {
  static propTypes = {
    source: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    resizeMode: PropTypes.string,
    aspectRatio: PropTypes.number,
    width: PropTypes.number,
    context: PropTypes.object,
  }

  static defaultProps = {
    resizeMode: 'contain',
  }

  render() {
    const { source, resizeMode, width, context } = this.props

    if (!source) {
      return null
    }

    const containerWidth = context.width * width
    const containerHeight = containerWidth / context.aspectRatio

    return (
      <View style={{ width: containerWidth, height: containerHeight }}>
        <Image
          style={styles.image}
          source={source}
          resizeMode={resizeMode}
          onLoad={this._imageLoaded}
          onError={this._imageLoadedWithError}
          fadeDuration={context.mode === CanvasModes.RENDER ? 0 : 300}
        />
      </View>
    )
  }

  _imageLoaded = () => {
    this.isRendered = true
    this.hasRenderingError = false
  }

  _imageLoadedWithError = () => {
    this.isRendered = true
    this.hasRenderingError = true
  }

  waitFor = () => {
    if (this.isRendered) {
      if (this.hasRenderingError) {
        return Promise.reject()
      }

      return Promise.resolve()
    }

    return this._listenRendering().then(() => {
      if (this.hasRenderingError) {
        return Promise.reject()
      }
    })
  }

  _listenRendering = () => {
    if (this.isRendered) {
      return Promise.resolve()
    }

    return new Promise(resolve => {
      const interval = setInterval(() => {
        if (this.isRendered) {
          clearInterval(interval)

          if (
            Platform.OS === 'android' &&
            this.props.context.mode !== CanvasModes.RENDER
          ) {
            setTimeout(() => {
              resolve()
            }, 300)
          } else {
            resolve()
          }
        }
      }, 20)
    })
  }
}
