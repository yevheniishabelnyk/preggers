import { moderateScale } from '@/app/scaling'
import PregnancyProgressWeek from '@/pregnancy/components/organisms/PregnancyProgressWeek'

export const defaultWidth =
  PregnancyProgressWeek.defaultWidth + moderateScale(4)
const height = PregnancyProgressWeek.defaultHeight + moderateScale(58)
export const aspectRatio = defaultWidth / height

export default {
  container: {
    width: defaultWidth,
    height,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: moderateScale(2),
    borderColor: 'transparent',
  },
}
