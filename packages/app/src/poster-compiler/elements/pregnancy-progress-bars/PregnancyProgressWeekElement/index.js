/**
 *
 * PregnancyProgressWeekElement
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { getPregnancy } from '@/app/redux/selectors'

import { View } from 'react-native'
import LogoAndBrand from '@/poster-compiler/components/atoms/LogoAndBrand'
import PregnancyProgressWeek from '@/pregnancy/components/organisms/PregnancyProgressWeek'
import ElementContent from '@/poster-compiler/components/base/ElementContent'

import forwardRef from '@/shared/decorators/forwardRef'

import { scalableStyles } from '@/app/scaling'

import rawStyles, { defaultWidth, aspectRatio } from './styles'

@forwardRef
@connect((state, props) => {
  const { pregnancyId } = props.context

  if (pregnancyId) {
    const pregnancy = getPregnancy(state, pregnancyId)

    return {
      pregnancy,
    }
  }

  return {}
})
@firebaseConnect(props => {
  const { pregnancyId } = props.context

  if (pregnancyId && !props.pregnancy) {
    return [`pregnancies/${pregnancyId}`]
  }
})
@scalableStyles(rawStyles)
export default class PregnancyProgressWeekElement extends ElementContent {
  static propTypes = {
    pregnancy: PropTypes.object,
    context: PropTypes.object,
    backgroundColor: PropTypes.string,
    forwardedRef: PropTypes.func,
  }

  static aspectRatio = aspectRatio
  static defaultWidth = defaultWidth

  aspectRatio = aspectRatio
  defaultWidth = defaultWidth

  render() {
    const {
      pregnancy,
      backgroundColor,
      theme,
      context,
      forwardedRef,
    } = this.props

    const background = backgroundColor || context.backgroundColor

    const scale = this._getScale()

    this.styles.scale = scale

    return (
      <View style={this.styles.container}>
        <PregnancyProgressWeek
          pregnancy={pregnancy}
          {...context}
          backgroundColor={background}
          theme={theme}
          scale={scale}
          ref={forwardedRef}
        />

        <LogoAndBrand
          scale={scale}
          backgroundColor={background}
          theme={theme}
        />
      </View>
    )
  }
}
