import PregnancyProgressSummaryElement from './PregnancyProgressSummaryElement'
import PregnancyProgressDayElement from './PregnancyProgressDayElement'
import PregnancyProgressWeekElement from './PregnancyProgressWeekElement'
import PregnancyProgressBabyElement from './PregnancyProgressBabyElement'
import PregnancyProgressFruitElement from './PregnancyProgressFruitElement'
import PregnancyProgressPercentageElement from './PregnancyProgressPercentageElement'

export default {
  PregnancyProgressSummary: PregnancyProgressSummaryElement,
  PregnancyProgressPercentage: PregnancyProgressPercentageElement,
  PregnancyProgressDay: PregnancyProgressDayElement,
  PregnancyProgressWeek: PregnancyProgressWeekElement,
  PregnancyProgressBaby: PregnancyProgressBabyElement,
  PregnancyProgressFruit: PregnancyProgressFruitElement,
}
