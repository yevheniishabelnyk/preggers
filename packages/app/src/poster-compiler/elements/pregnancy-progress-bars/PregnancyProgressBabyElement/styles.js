import { moderateScale } from '@/app/scaling'
import PregnancyProgressBaby from '@/pregnancy/components/organisms/PregnancyProgressBaby'

export const defaultWidth =
  PregnancyProgressBaby.defaultWidth + moderateScale(4)
const height = PregnancyProgressBaby.defaultHeight + moderateScale(58)
export const aspectRatio = defaultWidth / height

export default {
  container: {
    width: defaultWidth,
    height,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: moderateScale(2),
    borderColor: 'transparent',
  },
}
