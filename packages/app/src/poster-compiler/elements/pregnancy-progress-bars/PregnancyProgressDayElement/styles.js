import { moderateScale } from '@/app/scaling'
import PregnancyProgressDay from '@/pregnancy/components/organisms/PregnancyProgressDay'

export const defaultWidth = PregnancyProgressDay.defaultWidth + moderateScale(4)
const height = PregnancyProgressDay.defaultHeight + moderateScale(58)
export const aspectRatio = defaultWidth / height

export default {
  container: {
    width: defaultWidth,
    height,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: moderateScale(2),
    borderColor: 'transparent',
  },
}
