import { moderateScale } from '@/app/scaling'
import PregnancyProgressSummary from '@/pregnancy/components/organisms/PregnancyProgressSummary'

export const defaultWidth =
  PregnancyProgressSummary.defaultWidth + moderateScale(4)
const height = PregnancyProgressSummary.defaultHeight + moderateScale(58)
export const aspectRatio = defaultWidth / height

export default {
  container: {
    width: defaultWidth,
    height,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: moderateScale(2),
    borderColor: 'transparent',
  },
}
