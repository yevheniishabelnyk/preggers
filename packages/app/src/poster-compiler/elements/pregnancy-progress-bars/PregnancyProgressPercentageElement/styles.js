import { moderateScale } from '@/app/scaling'
import PregnancyProgressPercentage from '@/pregnancy/components/organisms/PregnancyProgressPercentage'

export const defaultWidth =
  PregnancyProgressPercentage.defaultWidth + moderateScale(4)
const height = PregnancyProgressPercentage.defaultHeight + moderateScale(58)
export const aspectRatio = defaultWidth / height

export default {
  container: {
    width: defaultWidth,
    height,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: moderateScale(2),
    borderColor: 'transparent',
  },
}
