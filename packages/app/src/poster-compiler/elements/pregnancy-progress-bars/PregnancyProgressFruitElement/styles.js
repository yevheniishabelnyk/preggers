import { moderateScale } from '@/app/scaling'
import PregnancyProgressFruit from '@/pregnancy/components/organisms/PregnancyProgressFruit'

export const defaultWidth =
  PregnancyProgressFruit.defaultWidth + moderateScale(4)
const height = PregnancyProgressFruit.defaultHeight + moderateScale(58)
export const aspectRatio = defaultWidth / height

export default {
  container: {
    width: defaultWidth,
    height,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: moderateScale(2),
    borderColor: 'transparent',
  },
}
