/**
 *
 * isApplicationUpdateAvailable
 *
 * Supports Semantic Versioning eg. '1.3.10', '10.3.0', etc.
 *
 * Check to see if user has downloaded latest version of the application
 *
 * Example:
 *
 * isApplicationUpdateAvailable('1.3.4', '1.3.5')
 *
 * returns true
 *
 * isApplicationUpdateAvailable('1.3.4', '1.3.4')
 *
 * returns false
 *
 *
 */
export default function isApplicationUpdateAvailable(
  installedVersion,
  latestVersion
) {
  const a = installedVersion.split('.').map(number => Number(number))
  const b = latestVersion.split('.').map(number => Number(number))

  if (a.length === 2) {
    a[2] = 0
  }

  if (a[0] > b[0]) return false
  if (a[0] < b[0]) return true

  if (a[1] > b[1]) return false
  if (a[1] < b[1]) return true

  if (a[2] > b[2]) return false
  if (a[2] < b[2]) return true

  return false
}
