/**
 *
 * formatWeight
 *
 */

const formattingTokens = /((\[[^[]*\]\s?)|(\\)?(kg\s?|g\s?|lb\s?|oz\s?))/g

export default function formatWeight(values, format = '') {
  if (values && format) {
    const { kg = 0, g = 0, lb = 0, oz = 0 } = values

    const matches = format.match(formattingTokens)

    if (matches) {
      const result = matches.reduce(function(out, str) {
        if (!str.startsWith('[')) {
          return (
            out +
            str
              .replace(/kg/g, kg)
              .replace(/g/g, g)
              .replace(/lb/g, lb || '')
              .replace(/oz/g, oz)
          )
        }

        if (!lb && /\[lb\]/g.test(str)) {
          return out
        }

        return out + str
      }, '')

      return result.replace(/\[|\]/g, '')
    }

    return ''
  }

  return ''
}
