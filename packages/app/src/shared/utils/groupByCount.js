/**
 * Goup array by count
 *
 * groupByCount([11, 22, 33, 44, 55], 2) // [[11, 22], [33, 44], [55]]
 */

const groupByCount = (arr, count) => {
  let group = []

  for (let i = 0, j = 0; i < arr.length; i++) {
    if (i >= count && i % count === 0) {
      j++
    }
    group[j] = group[j] || []
    group[j].push(arr[i])
  }

  return group
}

export default groupByCount
