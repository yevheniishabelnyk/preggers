/**
 *
 * checkLibraryPermission util
 *
 */

import { Platform, Alert, AlertIOS } from 'react-native'
import { Permissions } from 'expo'
import messages from './messages'

export default async function(formatMessage) {
  if (!this.hasLibraryPermission) {
    let isDenied = this.hasLibraryPermission === false

    if (!isDenied) {
      try {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)

        this.hasLibraryPermission = status === 'granted'

        isDenied = status === 'denied'
      } catch (err) {
        isDenied = true
      }
    }

    if (isDenied) {
      const message =
        Platform.OS === 'ios'
          ? messages.denideOnCameraRoll
          : messages.denideOnCamera

      if (Platform.OS === 'ios') {
        AlertIOS.alert(formatMessage(message))
      } else {
        Alert.alert(formatMessage(message))
      }
    }

    if (!this.hasLibraryPermission) {
      return Promise.reject(
        'Missing read and write permissions for photo library'
      )
    }
  }
}
