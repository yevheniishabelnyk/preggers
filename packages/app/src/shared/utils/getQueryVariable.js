/**
 *
 * getQueryVariable
 *
 */

export default function getQueryVariable(url, name) {
  let query = url

  if (url.indexOf('?') == 0) {
    query = url.substr(1)
  }

  let pairs = query.split('&')

  for (let i = 0; i < pairs.length; i++) {
    let pair = pairs[i].split('=')

    if (pair[0] == name) {
      return pair[1]
    }
  }

  return ''
}
