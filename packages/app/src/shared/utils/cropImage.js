/**
 *
 * crop image
 *
 */

import { ImageManipulator } from 'expo'

export default function cropImage(image, options = {}) {
  console.log('cropImage')

  if (options.type === 'square') {
    const actions = []

    const aspectRation = image.width / image.height

    if (aspectRation !== 1) {
      console.info('aspectRation: ', aspectRation)

      const originX = aspectRation > 1 ? (image.width - image.height) / 2 : 0
      const originY = aspectRation > 1 ? 0 : (image.height - image.width) / 2
      const newWidth = aspectRation > 1 ? image.height : image.width
      console.info('originX: ', originX)
      console.info('originY: ', originY)
      console.info('newWidth: ', newWidth)

      actions.push({
        crop: {
          originX: Math.round(originX),
          originY: Math.round(originY),
          width: newWidth,
          height: newWidth,
        },
      })
    }

    actions.push({
      resize: { width: options.width, height: options.width },
    })

    return ImageManipulator.manipulate(image.uri, actions, {
      format: options.format || 'jpeg',
      base64: options.base64 || undefined,
    })
  }

  return image
}
