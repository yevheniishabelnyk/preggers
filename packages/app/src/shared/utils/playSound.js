/**
 *
 * Play sound util
 *
 */

import { Audio } from 'expo'

export default async function(sound) {
  const soundObject = new Audio.Sound()

  try {
    await soundObject.loadAsync(sound)
    await soundObject.playAsync()
  } catch (err) {
    console.info('err: ', err)
  }

  return Promise.resolve()
}
