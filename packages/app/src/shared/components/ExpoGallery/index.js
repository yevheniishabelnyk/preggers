/**
 *
 * ExpoGallery
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Gallery } from '@/vendor/react-native-image-gallery'
import { ExpoImage } from '@/vendor/cache-manager'

const ExpoGalleryImage = ({ source, resizeMode, style }) => (
  <ExpoImage source={source} resizeMode={resizeMode} style={style} cacheImage />
)

ExpoGalleryImage.propTypes = {
  source: PropTypes.object,
  style: PropTypes.any,
  resizeMode: PropTypes.string,
}

ExpoGalleryImage.defaultProps = {
  resizeMode: 'contain',
}

const ExpoGallery = ({ style, images }) => (
  <Gallery style={style} images={images} imageComponent={ExpoGalleryImage} />
)

ExpoGallery.propTypes = {
  images: PropTypes.array,
  style: PropTypes.any,
}

ExpoGallery.defaultProps = {
  images: [],
}

export default ExpoGallery
