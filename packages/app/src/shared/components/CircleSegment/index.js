/*
 *
 * CircleByPieces
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Svg } from 'expo'

const CircleSegment = ({ width, strokeWidth, strokes }) => (
  <Svg width={width} height={width}>
    <Svg.Defs>
      <Svg.LinearGradient id="linear-pink" x1="0%" y1="0%" x2="100%" y2="0%">
        <Svg.Stop offset="0%" stopColor="#e91d88" />
        <Svg.Stop offset="100%" stopColor="#b61de9" />
      </Svg.LinearGradient>
      <Svg.LinearGradient id="linear-teal" x1="0%" y1="0%" x2="100%" y2="0%">
        <Svg.Stop offset="0%" stopColor="#1dc4e9" />
        <Svg.Stop offset="100%" stopColor="#1de9b6" />
      </Svg.LinearGradient>
    </Svg.Defs>

    {strokes.map((stroke, index, array) => (
      <Svg.Path
        key={index}
        fill="none"
        stroke={stroke.gradient ? `url(#${stroke.color})` : stroke.color}
        strokeWidth={strokeWidth}
        d={describeArc(
          width / 2,
          width / 2,
          (width - strokeWidth - 2) / 2,
          index === 0
            ? 1
            : 360 *
              array
                .slice(0, index)
                .reduce((sum, item) => sum + item.segment, 0),
          index === 0
            ? 360 * stroke.segment
            : 360 *
                array
                  .slice(0, index)
                  .reduce((sum, item) => sum + item.segment, 0) +
              360 * stroke.segment
        )}
      />
    ))}
  </Svg>
)

CircleSegment.propTypes = {
  width: PropTypes.number,
  strokeWidth: PropTypes.number,
  strokes: PropTypes.array,
}

const describeArc = (x, y, radius, startAngle, endAngle) => {
  let start = polarToCartesian(x, y, radius, endAngle)
  let end = polarToCartesian(x, y, radius, startAngle)

  let largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1'

  let d = [
    'M',
    start.x,
    start.y,
    'A',
    radius,
    radius,
    0,
    largeArcFlag,
    0,
    end.x,
    end.y,
  ].join(' ')

  return d
}

const polarToCartesian = (centerX, centerY, radius, angleInDegrees) => {
  let angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0

  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  }
}

export default CircleSegment
