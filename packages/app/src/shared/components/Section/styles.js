import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.2

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(26, resizeFactor),
    color: 'rgb(41,53,69)',
    marginBottom: moderateScale(17, resizeFactor),
  },
})

export default styles
