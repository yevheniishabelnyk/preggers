/**
 *
 * Section
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'

import styles from './styles'

export default class Section extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    children: PropTypes.any,
    style: PropTypes.any,
  }

  render() {
    const { title, children, style } = this.props

    return (
      <View style={[styles.container, style]}>
        <Text style={styles.title}>{title}</Text>

        {children}
      </View>
    )
  }
}
