import { defineMessages } from 'react-intl'

export default defineMessages({
  footerText: 'Please, wait...',
})
