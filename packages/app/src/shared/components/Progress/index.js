/**
 *
 * Progress
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, ActivityIndicator } from 'react-native'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class Progress extends React.Component {
  static propTypes = {
    text: PropTypes.string,
  }

  render() {
    const { text } = this.props

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.text}>{text}</Text>
        </View>

        <View style={styles.footer}>
          <ActivityIndicator animating style={styles.loader} />

          <FormattedMessage
            {...messages.footerText}
            style={styles.footerText}
          />
        </View>
      </View>
    )
  }
}
