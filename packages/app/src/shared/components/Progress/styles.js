import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 8,
  },

  content: {
    paddingHorizontal: moderateScale(10, resizeFactor),
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: moderateScale(100),
  },

  footer: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: 'rgb(231,231,231)',
    paddingVertical: moderateScale(20, resizeFactor),
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    color: 'rgb(131, 146, 167)',
    fontSize: moderateScale(18, resizeFactor),
    fontFamily: 'Now-Regular',
    lineHeight: moderateScale(24, resizeFactor),
    textAlign: 'center',
    backgroundColor: 'transparent',
  },

  footerText: {
    textAlign: 'center',
    fontSize: moderateScale(14, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(131, 146, 167)',
    marginLeft: moderateScale(8),
  },
})

export default styles
