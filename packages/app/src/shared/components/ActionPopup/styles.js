import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.4

const styles = StyleSheet.create({
  dialog: {
    flex: 1,
    maxWidth: moderateScale(331, resizeFactor),
    maxHeight: moderateScale(547, resizeFactor),
    overflow: 'hidden',
  },

  container: {
    flex: 1,
    paddingHorizontal: moderateScale(28, resizeFactor),
    paddingTop: moderateScale(28, resizeFactor),
    alignItems: 'center',
  },

  image: {
    width: moderateScale(171, resizeFactor),
    height: moderateScale(194, resizeFactor),
  },

  smallTitle: {
    marginTop: moderateScale(31),
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(12, resizeFactor),
    lineHeight: moderateScale(22, resizeFactor),
    color: 'rgb(29,29,29)',
    textAlign: 'center',
  },

  largeTitle: {
    marginTop: moderateScale(24),
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(22, resizeFactor),
    lineHeight: moderateScale(27, resizeFactor),
    color: 'rgb(43,56,87)',
    textAlign: 'center',
  },

  text: {
    marginTop: moderateScale(28),
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(12, resizeFactor),
    lineHeight: moderateScale(22, resizeFactor),
    color: 'rgb(43,56,87)',
    textAlign: 'center',
  },

  buttonWrapper: {
    marginTop: 'auto',
    alignSelf: 'stretch',
  },

  okButton: {
    paddingVertical: moderateScale(20, resizeFactor),
    alignSelf: 'stretch',
  },

  smallButtonText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12, resizeFactor),
    textAlign: 'center',
    color: 'rgb(154,153,159)',
    backgroundColor: 'transparent',
  },
})

export default styles
