/**
 *
 * ActionPopup
 *
 */

import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'

import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog'
import ActionButton from '@/settings/components/atoms/ActionButton'

import styles from './styles'

export default class ActionPopup extends React.Component {
  static propTypes = {
    image: Image.propTypes.source,
    smallTitle: PropTypes.string,
    largeTitle: PropTypes.string,
    text: PropTypes.string,
    buttonText: PropTypes.string,
    smallButtonText: PropTypes.string,
    actionButton: PropTypes.object,
    okButton: PropTypes.object,
  }

  render() {
    const {
      image,
      smallTitle,
      largeTitle,
      text,
      actionButton,
      okButton,
    } = this.props

    return (
      <PopupDialog
        ref={ref => (this._dialog = ref)}
        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
        dialogStyle={styles.dialog}
        overlayOpacity={0.7}
      >
        <View style={styles.container}>
          {image ? (
            <Image source={this.props.image} style={styles.image} />
          ) : null}

          {smallTitle ? (
            <Text style={styles.smallTitle}>
              {this.props.smallTitle.toUpperCase()}
            </Text>
          ) : null}

          {largeTitle ? (
            <Text style={styles.largeTitle}>{this.props.largeTitle}</Text>
          ) : null}

          {text ? <Text style={styles.text}>{this.props.text}</Text> : null}

          {actionButton ? (
            <View style={styles.buttonWrapper}>
              <ActionButton
                title={actionButton.title}
                onPress={actionButton.onPress}
              />
            </View>
          ) : null}

          {okButton ? (
            <TouchableOpacity
              style={styles.okButton}
              onPress={okButton.onPress}
              activeOpacity={0.95}
            >
              <Text style={styles.smallButtonText}>{okButton.title}</Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </PopupDialog>
    )
  }

  _show = () => this._dialog.show()

  _hide = () => this._dialog.dismiss()
}
