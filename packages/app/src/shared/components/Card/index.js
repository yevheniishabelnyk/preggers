/**
 *
 * Card
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'

import HTML from 'react-native-render-html'

import styles, { tagStyles } from './styles'

export default class Card extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
    html: PropTypes.bool,
    onLinkPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { html, title = '', content = '', style } = this.props

    return (
      <View style={[styles.container, style]}>
        <Text style={styles.title}>{title}</Text>

        {html ? (
          <HTML
            html={content}
            // eslint-disable-next-line react/jsx-no-bind
            customWrapper={RNContent => (
              <View style={styles.contentConteiner}>{RNContent}</View>
            )}
            onLinkPress={this._linkPressed}
            tagsStyles={tagStyles}
            renderers={{
              img: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                return (
                  <View style={styles.imageTagWrapper} key={passProps.key}>
                    <Image
                      source={{ uri: htmlAttribs.src }}
                      style={styles.imageTag}
                      resizeMode="contain"
                    />
                  </View>
                )
              },
              ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, (child, index) => {
                  return (
                    <View style={styles.olLiStyle}>
                      <Text style={styles.olLiPrefix}>{index + 1}.</Text>
                      {child}
                    </View>
                  )
                })

                return <View key={passProps.key}>{content}</View>
              },

              ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, child => {
                  return (
                    <View style={styles.ulLiStyle}>
                      <View style={styles.ulLiPrefix} />
                      {child}
                    </View>
                  )
                })

                return (
                  <View key={passProps.key} style={styles.ulContent}>
                    {content}
                  </View>
                )
              },
            }}
          />
        ) : (
          <Text style={styles.content}>{content}</Text>
        )}
      </View>
    )
  }

  _linkPressed = (evt, href) => {
    const { onLinkPress } = this.props

    onLinkPress(href)
  }
}
