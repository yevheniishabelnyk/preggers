import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const ulLiPrefixWidth = moderateScale(6, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    backgroundColor: 'white',
    borderRadius: 8,
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    paddingTop: moderateScale(15, resizeFactor),
    paddingBottom: moderateScale(27, resizeFactor),
    alignSelf: 'stretch',
  },

  title: {
    fontSize: moderateScale(16, resizeFactor),
    letterSpacing: 1,
    lineHeight: moderateScale(23, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(59,72,89)',
    textAlign: 'left',
    marginBottom: moderateScale(13, resizeFactor),
  },

  content: {
    fontSize: moderateScale(13, resizeFactor),
    lineHeight: moderateScale(27, resizeFactor),
    fontFamily: 'Now-Medium',
    color: '#8392a7',
    textAlign: 'left',
  },

  olStyle: {
    marginTop: 0,
  },

  olLiStyle: {
    flexDirection: 'row',
  },

  ulLiStyle: {
    flexDirection: 'row',
  },

  olLiPrefix: {
    fontFamily: 'Roboto-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: 'rgb(59,72,89)',
    lineHeight: moderateScale(24, resizeFactor),
    width: moderateScale(17, resizeFactor),
    alignSelf: 'flex-start',
  },

  ulContent: {
    marginTop: moderateScale(2, resizeFactor),
  },

  ulLiPrefix: {
    marginVertical: moderateScale(9, resizeFactor),
    marginRight: moderateScale(8, resizeFactor),
    width: ulLiPrefixWidth,
    height: ulLiPrefixWidth,
    borderRadius: ulLiPrefixWidth / 2,
    backgroundColor: 'rgb(59,72,89)',
  },

  contentConteiner: {
    paddingBottom: moderateScale(28, resizeFactor),
  },
})

export const tagStyles = {
  p: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(131,146,167)',
  },

  strong: {
    fontFamily: 'Roboto-Bold',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(76,82,91)',
  },

  em: {
    fontFamily: 'Roboto-LightItalic',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(131,146,167)',
  },

  h1: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17, resizeFactor),
  },

  h2: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17, resizeFactor),
  },

  h3: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17, resizeFactor),
  },
}

export default styles
