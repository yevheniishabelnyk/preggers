/**
 *
 * CancelButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View } from 'react-native'

import styles from './styles'

export default class CancelButton extends React.Component {
  static propTypes = {
    white: PropTypes.bool,
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { white, onPress, style } = this.props
    const whiteLineStyle = white && styles.whiteLine

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <View style={[styles.crossLine, whiteLineStyle]} />
        <View
          style={[styles.crossLine, styles.secondCrossLine, whiteLineStyle]}
        />
      </TouchableOpacity>
    )
  }
}
