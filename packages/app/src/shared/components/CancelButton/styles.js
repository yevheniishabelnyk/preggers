import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const containerWidth = moderateScale(30)

const styles = StyleSheet.create({
  container: {
    width: containerWidth,
    height: containerWidth,
    position: 'relative',
  },

  crossLine: {
    width: containerWidth * 1.4,
    height: moderateScale(4),
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
    borderRadius: moderateScale(8),
    transform: [{ rotate: '45deg' }],
    position: 'absolute',
    top: containerWidth / 2.1,
    left: -containerWidth / 5.1,
  },

  secondCrossLine: {
    transform: [{ rotate: '-45deg' }],
    top: containerWidth / 2.1,
    left: -containerWidth / 5.1,
  },

  whiteLine: {
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
  },
})

export default styles
