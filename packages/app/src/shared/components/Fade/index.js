/**
 *
 * Fade
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Animated } from 'react-native'

export default class Fade extends React.Component {
  static propTypes = {
    visible: PropTypes.bool,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
  }

  constructor(props) {
    super(props)
    this.state = {
      visible: props.visible,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) {
      this.setState({ visible: true })
    }
    Animated.timing(this._visibility, {
      toValue: nextProps.visible ? 1 : 0,
      duration: 500,
    }).start(() => {
      this.setState({ visible: nextProps.visible })
    })
  }

  componentWillMount() {
    this._visibility = new Animated.Value(this.props.visible ? 1 : 0)
  }

  render() {
    const { style, children, ...rest } = this.props

    const containerStyle = {
      opacity: this._visibility.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
      }),
      transform: [
        {
          scale: this._visibility.interpolate({
            inputRange: [0, 1],
            outputRange: [1.1, 1],
          }),
        },
      ],
    }

    const combinedStyle = [containerStyle, style]
    return (
      <Animated.View style={combinedStyle} {...rest}>
        {children}
      </Animated.View>
    )
  }
}
