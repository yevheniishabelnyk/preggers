import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const defaultFontSize = moderateScale(18)
export const defaultLineHeight = moderateScale(26)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    alignSelf: 'stretch',
  },

  contentWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: -1000,
    alignSelf: 'stretch',
  },

  content: {},

  text: {
    fontSize: defaultFontSize,
    lineHeight: defaultLineHeight,
    fontFamily: 'Now-Medium',
  },
})

export default styles
