/**
 *
 * ViewportText
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Animated, StyleSheet } from 'react-native'

import { isFunction } from 'lodash'

import styles, { defaultFontSize, defaultLineHeight } from './styles'

export default class ViewportText extends React.PureComponent {
  static propTypes = {
    alignCenter: PropTypes.bool,
    disableLineHeightScaling: PropTypes.bool,
    text: PropTypes.string,
    textColor: PropTypes.string,
    textStyle: PropTypes.any,
    contentContainerStyle: PropTypes.any,
    onFinishRendering: PropTypes.func,
  }

  static defaultProps = {
    text: '',
  }

  state = {
    textScale: 1,
    contentMarginTop: 0,
  }

  originalFontSize = defaultFontSize
  originalLineHeight = defaultLineHeight

  constructor(props) {
    super(props)

    this._contentOpacity = new Animated.Value(0)

    if (props.textStyle) {
      const flattenTextStyle = StyleSheet.flatten(props.textStyle)

      if (flattenTextStyle.fontSize) {
        this.originalFontSize = flattenTextStyle.fontSize
      }

      if (flattenTextStyle.fontSize) {
        this.originalLineHeight = flattenTextStyle.lineHeight
      }
    }
  }

  render() {
    const {
      text,
      contentContainerStyle,
      textStyle,
      textColor,
      disableLineHeightScaling,
    } = this.props
    const { textScale, contentMarginTop } = this.state

    return (
      <View style={styles.container} onLayout={this._onLayoutContainer}>
        <Animated.View
          style={[styles.contentWrapper, { opacity: this._contentOpacity }]}
        >
          <View
            style={[
              styles.content,
              contentContainerStyle,
              { marginTop: contentMarginTop },
            ]}
          >
            {text ? (
              <Text
                style={[
                  styles.text,
                  textStyle,
                  textColor ? { color: textColor } : null,
                  {
                    fontSize: this.originalFontSize * textScale,
                    lineHeight: disableLineHeightScaling
                      ? this.originalLineHeight
                      : this.originalLineHeight * textScale,
                  },
                ]}
                onLayout={this._onLayoutText}
              >
                {text}
              </Text>
            ) : null}
          </View>
        </Animated.View>
      </View>
    )
  }

  _onLayoutContainer = e => {
    if (!this._isTextFitsViewport) {
      this.containerHeight = e.nativeEvent.layout.height

      if (this.textHeight) {
        this._fitText(this.textHeight, this.containerHeight)
      }
    }
  }

  _onLayoutText = e => {
    if (!this._isTextFitsViewport) {
      if (!this.textHeight) {
        this.initialTextHeight = e.nativeEvent.layout.height
      }

      this.textHeight = e.nativeEvent.layout.height

      if (this.containerHeight) {
        this._fitText(this.textHeight, this.containerHeight)
      }
    }
  }

  _fitText = (textHeight, containerHeight) => {
    if (!this._isFitting && textHeight > containerHeight) {
      this._isFitting = true

      this.setState(
        prevState => ({
          textScale: prevState.textScale * 0.95,
        }),
        () => (this._isFitting = false)
      )
    } else {
      this._isTextFitsViewport = true

      const updates = {
        textScale: textHeight / this.initialTextHeight - 0.02,
      }

      if (this.props.alignCenter) {
        updates.contentMarginTop = (containerHeight - textHeight) / 2
      }

      this.setState(updates)

      Animated.timing(this._contentOpacity, {
        toValue: 1,
        duration: 250,
      }).start(() => {
        const { onFinishRendering } = this.props

        if (isFunction(onFinishRendering)) {
          onFinishRendering()
        }
      })
    }
  }
}
