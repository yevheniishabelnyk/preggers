import { StyleSheet, Dimensions } from 'react-native'
import { Constants } from 'expo'
import { moderateScale } from '@/app/scaling'

const { width, height } = Dimensions.get('window')
const { statusBarHeight } = Constants

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
  },

  keyboardAwareContainer: {
    position: 'relative',
    flex: 1,
    alignItems: 'center',
  },

  scrollableInKeybardAwareContainer: {
    flex: 1,
    alignSelf: 'stretch',
  },

  scrollableContainer: {
    position: 'relative',
    alignItems: 'center',
    minHeight: height - 18,
  },

  header: {
    width,
    height: 'auto',
    zIndex: 100,
  },

  deviceIndicators: {
    height: statusBarHeight,
  },

  main: {
    flex: 1,
  },

  keyboardAwareNestedContainer: {
    height,
    position: 'absolute',
    top: moderateScale(-18),
    right: 0,
    left: 0,
    bottom: 0,
    paddingTop: moderateScale(18),
    alignItems: 'center',
    overflow: 'visible',
  },
})

export default styles
