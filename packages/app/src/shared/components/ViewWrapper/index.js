/*
 *
 * ViewWrapper
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { ScrollView, View, KeyboardAvoidingView } from 'react-native'

import styles from './styles'

export default class ScreenWrapper extends React.Component {
  static propTypes = {
    backButtonClicked: PropTypes.func,
    keyboard: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
    headerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    mainStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    contentContainerStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    deviceIndicators: PropTypes.string,
    scrollable: PropTypes.bool,
    goBackButton: PropTypes.bool,
    scrollToTop: PropTypes.bool,
    previousScreen: PropTypes.string,
    behavior: PropTypes.string,
    getInput: PropTypes.func,
    inputsLength: PropTypes.number,
    headerHeight: PropTypes.number,
    keyboardVerticalOffset: PropTypes.number,
    components: PropTypes.object,
    scrollViewProps: PropTypes.object,
  }

  static defaultProps = {
    headerHeight: 0,
  }

  render() {
    const {
      keyboard,
      children,
      headerStyle,
      mainStyle,
      scrollable,
      deviceIndicators,
      components = {},
      headerHeight,
      contentContainerStyle,
      scrollViewProps,
      behavior,
      keyboardVerticalOffset,
    } = this.props

    return (
      <View style={styles.container}>
        {components.popups}

        <View style={[styles.header, headerStyle]}>
          {deviceIndicators ? (
            <View
              style={[
                styles.deviceIndicators,
                { backgroundColor: deviceIndicators },
              ]}
            />
          ) : null}

          {headerHeight ? <View>{components.header}</View> : null}
        </View>

        {keyboard ? (
          <KeyboardAvoidingView
            style={[styles.main, mainStyle]}
            enabled
            behavior={behavior || 'position'}
            contentContainerStyle={styles.keyboardAwareContainer}
            keyboardVerticalOffset={keyboardVerticalOffset || 0}
          >
            <ScrollView
              {...scrollViewProps}
              style={styles.scrollableInKeybardAwareContainer}
              keyboardShouldPersistTaps="handled"
              contentContainerStyle={contentContainerStyle}
              scrollEnabled={scrollable}
            >
              {children}
            </ScrollView>
          </KeyboardAvoidingView>
        ) : scrollable ? (
          <ScrollView
            ref={ref => (this._scrollView = ref)}
            style={[styles.main, mainStyle]}
            contentContainerStyle={[
              styles.scrollableContainer,
              {
                paddingBottom: headerHeight,
              },
              contentContainerStyle,
            ]}
          >
            {children}
          </ScrollView>
        ) : (
          <View style={[styles.main, { paddingTop: headerHeight }, mainStyle]}>
            {children}
          </View>
        )}
        <View>{components.footer}</View>
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.scrollToTop &&
      this.props.scrollToTop !== nextProps.scrollToTop
    ) {
      this._scrollView.scrollTo({ y: 0 })
    }
  }
}
