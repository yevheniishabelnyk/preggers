/**
 *
 * Prompt
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, TouchableOpacity } from 'react-native'

import styles from './styles'

export default class Prompt extends React.Component {
  static propTypes = {
    question: PropTypes.string,
    caption: PropTypes.string,
    text: PropTypes.string,
    negativeButton: PropTypes.object,
    positiveButton: PropTypes.object,
  }

  render() {
    const {
      question,
      text,
      caption,
      negativeButton,
      positiveButton,
    } = this.props

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          {question ? <Text style={styles.question}>{question}</Text> : null}

          {text ? <Text style={styles.text}>{text}</Text> : null}

          {caption ? <Text style={styles.caption}>{caption}</Text> : null}
        </View>

        <View style={styles.footer}>
          {negativeButton ? (
            <TouchableOpacity
              style={[styles.button, styles.leftButton]}
              onPress={negativeButton.onPress}
              activeOpacity={0.95}
            >
              <Text style={[styles.buttonTitle, styles.negativeButtonTitle]}>
                {negativeButton.title}
              </Text>
            </TouchableOpacity>
          ) : null}

          {positiveButton ? (
            <TouchableOpacity
              style={styles.button}
              onPress={positiveButton.onPress}
              activeOpacity={0.95}
            >
              <Text style={[styles.buttonTitle, styles.positiveButtonTitle]}>
                {positiveButton.title}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    )
  }
}
