import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 8,
  },

  content: {
    paddingHorizontal: moderateScale(20, resizeFactor),
    paddingBottom: moderateScale(32, resizeFactor),
  },

  footer: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: 'rgb(231,231,231)',
  },

  question: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(25, resizeFactor),
    lineHeight: moderateScale(42, resizeFactor),
    paddingHorizontal: moderateScale(20, resizeFactor),
    paddingBottom: moderateScale(15, resizeFactor),
    textAlign: 'center',
  },

  text: {
    color: 'rgb(131, 146, 167)',
    fontSize: moderateScale(12, resizeFactor),
    fontFamily: 'Now-Medium',
    lineHeight: moderateScale(24, resizeFactor),
    textAlign: 'center',
    backgroundColor: 'transparent',
  },

  caption: {
    color: 'rgb(131, 146, 167)',
    fontSize: moderateScale(18, resizeFactor),
    fontFamily: 'Now-Regular',
    lineHeight: moderateScale(24, resizeFactor),
    paddingTop: moderateScale(30, resizeFactor),
    textAlign: 'center',
    backgroundColor: 'transparent',
  },

  button: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: moderateScale(20, resizeFactor),
  },

  negativeButtonTitle: {
    color: 'rgb(255, 80, 118)',
  },

  leftButton: {
    borderRightWidth: 1,
    borderRightColor: '#dbdbdb',
  },

  buttonTitle: {
    textAlign: 'center',
    fontSize: moderateScale(14, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(59, 124, 255)',
  },
})

export default styles
