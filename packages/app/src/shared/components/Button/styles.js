import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: moderateScale(55),
    width: moderateScale(300),
    borderRadius: moderateScale(31),
    backgroundColor: 'rgb(12, 196, 155)',
    shadowColor: 'rgba(45, 140, 82, 0.25)',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: moderateScale(14),
  },

  buttonLabel: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16),
    color: 'rgb(255, 255, 255)',
    letterSpacing: 0,
  },
})

export default styles
