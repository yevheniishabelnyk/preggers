/**
 *
 * TextDropDown
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableOpacity, Animated } from 'react-native'
import HTML from 'react-native-render-html'

import styles from './styles'

export default class TextDropDown extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    style: PropTypes.any,
    readMoreButtonText: PropTypes.string,
  }

  state = {
    hiddenContentHeight: 0,
    visibleContentHeight: 0,
    contentIsVisible: false,
  }

  render() {
    const { text = '', style, readMoreButtonText } = this.props

    let visibleContent = '',
      hiddenText = ''

    if (text) {
      ;[visibleContent, hiddenText] = text.split(/<\/p>(.+)/)
      visibleContent = visibleContent + '</p>'
    }

    const animatedStyle = {
      height: this.containerHeight,
    }

    return (
      <Animated.View style={[styles.container, style, animatedStyle]}>
        <HTML
          html={visibleContent}
          // eslint-disable-next-line react/jsx-no-bind
          customWrapper={RNContent => (
            <View
              style={styles.visibleContentContainer}
              onLayout={this._onVisibleContentLayout}
            >
              {RNContent}
            </View>
          )}
          tagsStyles={{
            p: styles.contentParagraph,
            strong: styles.boldText,
            em: styles.emText,
            h1: styles.headingText,
            h2: styles.headingText,
            h3: styles.headingText,
            a: styles.linkText,
          }}
          renderers={{
            img: (htmlAttribs, children, convertedCSSStyles, passProps) => {
              return (
                <View style={styles.imageTagWrapper} key={passProps.key}>
                  <Image
                    source={{ uri: htmlAttribs.src }}
                    style={styles.imageTag}
                    resizeMode="contain"
                  />
                </View>
              )
            },
            ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
              const content = React.Children.map(children, (child, index) => {
                return (
                  <View style={styles.olLiStyle}>
                    <Text style={styles.olLiPrefix}>{index + 1}.</Text>
                    {child}
                  </View>
                )
              })

              return <View key={passProps.key}>{content}</View>
            },

            ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
              const content = React.Children.map(children, child => {
                return (
                  <View style={styles.ulLiStyle}>
                    <View style={styles.ulLiPrefix} />
                    {child}
                  </View>
                )
              })

              return (
                <View key={passProps.key} style={styles.ulWrapper}>
                  {content}
                </View>
              )
            },
          }}
        />

        {!this.state.contentIsVisible && hiddenText ? (
          <TouchableOpacity
            style={styles.readMoreButton}
            onPress={this._toggleContent}
            activeOpacity={0.95}
          >
            <View style={styles.readMoreButtonContent}>
              <Text style={styles.readMoreButtonTitle}>
                {readMoreButtonText}
              </Text>
            </View>
          </TouchableOpacity>
        ) : null}

        {hiddenText ? (
          <HTML
            html={hiddenText.replace(
              new RegExp('<li[^>]*>([\\s\\S]*?)<\\/li>', 'g'),
              '<li><p>$1</p></li>'
            )}
            // eslint-disable-next-line react/jsx-no-bind
            customWrapper={RNContent => (
              <View
                style={styles.hiddenContentContainer}
                onLayout={this._onHiddenContentLayout}
              >
                {RNContent}
              </View>
            )}
            tagsStyles={{
              p: styles.contentParagraph,
              strong: styles.boldText,
              em: styles.emText,
              h1: styles.headingText,
              h2: styles.headingText,
              h3: styles.headingText,
              a: styles.linkText,
            }}
            renderers={{
              ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, (child, index) => {
                  return (
                    <View style={styles.olLiStyle}>
                      <Text style={styles.olLiPrefix}>{index + 1}.</Text>
                      {child}
                    </View>
                  )
                })

                return <View key={passProps.key}>{content}</View>
              },

              ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, child => {
                  return (
                    <View style={styles.ulLiStyle}>
                      <View style={styles.ulLiPrefix} />
                      {child}
                    </View>
                  )
                })

                return (
                  <View key={passProps.key} style={styles.ulWrapper}>
                    {content}
                  </View>
                )
              },
            }}
          />
        ) : null}
      </Animated.View>
    )
  }

  _toggleContent = () => {
    const { contentIsVisible } = this.state

    if (contentIsVisible) {
      this.setState({ contentIsVisible: false })
      setTimeout(() => {
        this._animateSlideUp()
      }, 0)
    } else {
      this.setState({ contentIsVisible: true })
      setTimeout(() => {
        this._animateSlideDown()
      }, 0)
    }
  }

  _onVisibleContentLayout = e => {
    const { text } = this.props

    const [, hiddenText] = text.split(/<\/p>(.+)/)

    const visibleContentHeight =
      e.nativeEvent.layout.height + (hiddenText ? 40 : 0)

    this.containerHeight = new Animated.Value(visibleContentHeight)

    this.setState({ visibleContentHeight })
  }

  _onHiddenContentLayout = e => {
    const hiddenContentHeight = e.nativeEvent.layout.height

    this.setState({ hiddenContentHeight })
  }

  _animateSlideUp = () =>
    Animated.timing(this.containerHeight, {
      toValue: this.state.visibleContentHeight,
      duration: 200,
    }).start()

  _animateSlideDown = () =>
    Animated.timing(this.containerHeight, {
      toValue:
        this.state.visibleContentHeight + this.state.hiddenContentHeight - 30,
      duration: 200,
    }).start()
}
