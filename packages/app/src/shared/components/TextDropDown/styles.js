import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
const ulLiPrefixWidth = (6 / 375) * width

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    alignSelf: 'stretch',
  },

  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: (14 / 375) * width,
    lineHeight: (23 / 375) * width,
    color: '#8998ac',
  },

  readMoreButton: {
    marginTop: (17 / 375) * width,
  },

  readMoreButtonContent: {
    flexDirection: 'row',
  },

  readMoreButtonTitle: {
    fontFamily: 'Now-Medium',
    fontSize: (12 / 375) * width,
    letterSpacing: 0.6,
    color: '#131416',
    textAlign: 'left',
  },

  contentParagraph: {
    fontFamily: 'Roboto-Light',
    fontSize: (14 / 667) * height,
    lineHeight: (24 / 667) * height,
    color: 'rgb(131,146,167)',
    paddingBottom: 0,
    marginBottom: -5,
  },

  linkText: {
    fontFamily: 'Roboto-Light',
    fontSize: (14 / 667) * height,
    lineHeight: (24 / 667) * height,
    color: 'rgb(41,123,237)',
  },

  boldText: {
    fontFamily: 'Roboto-Bold',
    fontSize: (14 / 667) * height,
    lineHeight: (24 / 667) * height,
    color: 'rgb(76,82,91)',
  },

  emText: {
    fontFamily: 'Roboto-Lightitalic',
    fontSize: (14 / 667) * height,
    lineHeight: (24 / 667) * height,
    color: 'rgb(131,146,167)',
  },

  headingText: {
    fontFamily: 'Now-Medium',
    fontSize: (14 / 667) * height,
    lineHeight: (24 / 667) * height,
    color: 'rgb(59,72,89)',
    marginVertical: (17 / 667) * height,
  },

  olStyle: {
    marginTop: 0,
  },

  olLiStyle: {
    flexDirection: 'row',
  },

  ulLiStyle: {
    flexDirection: 'row',
  },

  olLiPrefix: {
    fontFamily: 'Roboto-Medium',
    fontSize: (14 / 667) * height,
    color: 'rgb(59,72,89)',
    lineHeight: (24 / 667) * height,
    width: (17 / 375) * width,
    alignSelf: 'flex-start',
  },

  ulLiPrefix: {
    marginVertical: (9 / 375) * width,
    marginRight: (8 / 375) * width,
    width: ulLiPrefixWidth,
    height: ulLiPrefixWidth,
    borderRadius: ulLiPrefixWidth / 2,
    backgroundColor: 'rgb(59,72,89)',
  },

  hiddenContentContainer: {
    marginTop: (8 / 375) * width,
  },

  visibleContentContainer: {},

  ulWrapper: {
    marginTop: 2,
  },
})

export default styles
