/**
 *
 * Empty
 *
 */

import React from 'react'

import { View } from 'react-native'

import styles from './styles'

const Empty = () => <View style={styles.empty} />

export default Empty
