import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  empty: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'white',
  },
})

export default styles
