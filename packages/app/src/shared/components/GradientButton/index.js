/*
 *
 * GradientButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableHighlight, View, Text } from 'react-native'
import { LinearGradient } from 'expo'

import styles from './styles'

const GradientButton = ({
  title,
  onPress,
  style,
  textStyle,
  active = true,
}) => (
  <TouchableHighlight
    onPress={onPress}
    underlayColor="rgba(255,255,255,.5)"
    style={[styles.button, active ? styles.buttonActive : null, style]}
  >
    <View style={styles.container}>
      {active ? (
        <LinearGradient
          colors={['#7E34A9', '#F63669']}
          start={[0, 0.4]}
          end={[1, 0.6]}
          style={styles.buttonOverlay}
        />
      ) : null}

      <Text
        style={[
          styles.buttonText,
          active ? styles.buttonTextActive : null,
          textStyle,
        ]}
      >
        {title}
      </Text>
    </View>
  </TouchableHighlight>
)

GradientButton.propTypes = {
  title: PropTypes.string,
  style: PropTypes.any,
  onPress: PropTypes.func,
  textStyle: PropTypes.any,
  active: PropTypes.bool,
}

export default GradientButton
