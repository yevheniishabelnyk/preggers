import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    width: moderateScale(340),
    height: moderateScale(55),
    borderRadius: moderateScale(100),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: 'red',
    borderColor: 'rgba(151, 151, 151, 0.46)',
    borderWidth: 1,
    shadowOpacity: 0.2,
    shadowRadius: 8,
    shadowColor: '#111723',
    shadowOffset: { height: 12, width: -2 },
  },

  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: moderateScale(100),
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonActive: {
    opacity: 1,
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderWidth: 0,
  },

  buttonOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: moderateScale(100),
  },

  buttonText: {
    color: 'white',
    fontSize: moderateScale(16, 0.4),
    backgroundColor: 'transparent',
    letterSpacing: 0,
    paddingHorizontal: moderateScale(20),
  },

  buttonTextActive: {
    color: '#FFFFFF',
    fontFamily: 'Now-Medium',
  },
})

export default styles
