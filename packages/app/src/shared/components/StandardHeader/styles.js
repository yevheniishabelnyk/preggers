import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

export const containerHeight = height * 0.095

const styles = StyleSheet.create({
  container: {
    height: containerHeight,
    width,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
    justifyContent: 'center',
  },

  shadow: {
    shadowOpacity: 0.5,
    shadowRadius: 6,
    shadowColor: '#E6E8F1',
    shadowOffset: { height: 5, width: -2 },
  },

  backButton: {
    width: containerHeight,
    height: containerHeight,
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },

  backIcon: {
    height: containerHeight * 0.2625,
    width: containerHeight * 0.2625 * 0.6,
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: containerHeight * 0.234,
    color: '#703499',
    letterSpacing: 0,
    backgroundColor: 'transparent',
    alignSelf: 'center',
  },
})

export default styles
