/**
 *
 * StandardHeader
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  View,
  Text,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native'

import styles from './styles'

export { containerHeight as height } from './styles'

export default class StandardHeader extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onBackButtonPress: PropTypes.func,
    shadow: PropTypes.bool,
    textStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    children: PropTypes.any,
    onPressTitle: PropTypes.func,
    textContainterStyle: PropTypes.any,
  }

  render() {
    const {
      textStyle,
      title,
      onBackButtonPress,
      shadow = true,
      children,
      onPressTitle,
      textContainterStyle,
    } = this.props

    return (
      <View style={[styles.container, shadow ? styles.shadow : null]}>
        {onBackButtonPress ? (
          <TouchableHighlight
            style={styles.backButton}
            onPress={onBackButtonPress}
            underlayColor="transparent"
          >
            <Image
              style={styles.backIcon}
              source={require('assets/icons/back.png')}
            />
          </TouchableHighlight>
        ) : null}

        <TouchableOpacity
          style={({ alignSelf: 'center' }, textContainterStyle)}
          onPress={onPressTitle}
          activeOpacity={0.95}
        >
          <Text style={[styles.title, textStyle]}>{title}</Text>
        </TouchableOpacity>

        {children}
      </View>
    )
  }
}
