/**
 *
 * TextField
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TextInput, Text } from 'react-native'

import styles from './styles'

export default class TextField extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    secureTextEntry: PropTypes.bool,
    required: PropTypes.bool,
    error: PropTypes.bool,
    autoCapitalize: PropTypes.string,
    placeholder: PropTypes.string,
    defaultValue: PropTypes.string,
    keyboardType: PropTypes.string,
    style: PropTypes.any,
    onChangeText: PropTypes.func,
    autoCorrect: PropTypes.bool,
    onBlur: PropTypes.func,
    clearTextOnFocus: PropTypes.bool,
    selectTextOnFocus: PropTypes.bool,
    returnKeyType: PropTypes.string,
    onSubmitEditing: PropTypes.func,
    enablesReturnKeyAutomatically: PropTypes.bool,
    onKeyPress: PropTypes.func,
  }

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TextInput
          underlineColorAndroid="transparent"
          ref="text_input"
          style={[styles.input, this.props.error ? styles.inputError : null]}
          keyboardType={this.props.keyboardType}
          placeholder={this.props.placeholder}
          autoCapitalize={this.props.autoCapitalize}
          secureTextEntry={this.props.secureTextEntry}
          defaultValue={this.props.defaultValue}
          placeholderTextColor={'#8A8EAC'}
          onChangeText={this.props.onChangeText}
          autoCorrect={this.props.autoCorrect}
          onBlur={this.props.onBlur}
          clearTextOnFocus={this.props.clearTextOnFocus}
          selectTextOnFocus={this.props.selectTextOnFocus}
          returnKeyType={this.props.returnKeyType}
          onSubmitEditing={this.props.onSubmitEditing}
          enablesReturnKeyAutomatically={
            this.props.enablesReturnKeyAutomatically
          }
          onKeyPress={this.props.onKeyPress}
        />
        {this.props.required ? (
          <Text
            style={[
              styles.requiredMark,
              this.props.error ? styles.errorText : null,
            ]}
          >
            *
          </Text>
        ) : null}
      </View>
    )
  }
}
