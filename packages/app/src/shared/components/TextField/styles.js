import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    width: width * 0.8,
    marginHorizontal: 10,
    marginVertical: 10,
  },

  input: {
    width: '100%',
    height: height * 0.08,
    alignItems: 'center',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: 'rgba(151, 151, 151, 0.56)',
    borderRadius: 100,
    paddingLeft: 26,
    paddingRight: 26,
    fontFamily: 'Now-Medium',
    fontSize: 15,
    lineHeight: 20,
  },

  inputError: {
    borderColor: '#D8367A',
    fontFamily: 'Roboto-Light',
  },

  requiredMark: {
    position: 'absolute',
    right: 20,
    top: height * 0.04 - 7,
    fontSize: 15,
    color: '#8A8EAC',
  },

  errorText: {
    color: '#D8367A',
  },
})

export default styles
