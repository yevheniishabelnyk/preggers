/*
 *
 * Slider
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Animated, PanResponder, Image } from 'react-native'

import arrowImage from 'assets/icons/arrow-right.png'
import { width, styles } from './styles'

export default class Slider extends React.Component {
  static propTypes = {
    slides: PropTypes.array,
    indicators: PropTypes.bool,
    onChange: PropTypes.func,
    scrollContainerStyle: PropTypes.any,
    slideContainerStyle: PropTypes.any,
  }

  state = {
    currentView: 0,
  }

  componentWillMount() {
    const clamp = (num, min, max) => {
      if (num <= min) {
        return min
      }
      if (num >= max) {
        return max
      }
      return num
    }

    this.pan = new Animated.Value(0)

    this.scrollResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,

      onPanResponderGrant: () => {
        this.setState({ leftArrowHide: true, rightArrowHide: true })

        this.pan.setOffset(this.pan._value)
        this.pan.setValue(0)
      },

      onPanResponderMove: Animated.event([null, { dx: this.pan }]),

      onPanResponderRelease: (e, { vx }) => {
        this.pan.flattenOffset()

        let move = Math.round(this.pan._value / width) * width

        if (Math.abs(vx) > 0.25) {
          const direction = vx / Math.abs(vx)

          const scrollPos =
            direction > 0
              ? Math.ceil(this.pan._value / width)
              : Math.floor(this.pan._value / width)

          move = scrollPos * width
        }

        const minScroll = (this.props.slides.length - 1) * -width

        const nextView = Math.round(-clamp(move, minScroll, 0) / width)

        if (nextView > 0) {
          this.setState({ leftArrowHide: false, rightArrowHide: false })
        } else {
          this.setState({ rightArrowHide: false })
        }

        if (nextView !== this.state.currentView) {
          this.setState({
            currentView: Math.abs(nextView),
          })

          if (typeof this.props.onChange === 'function') {
            this.props.onChange(Math.abs(nextView))
          }
        }

        Animated.spring(this.pan, {
          toValue: clamp(move, minScroll, 0),
          bounciness: 0,
        }).start()
      },
    })
  }

  _renderArrows(index, slidesLength) {
    if (slidesLength === 1) {
      return null
    }

    if (index === 0) {
      return (
        <View
          style={[
            styles.rightArrowWrap,
            this.state.rightArrowHide ? styles.hideArrow : null,
          ]}
        >
          <Image source={arrowImage} />
        </View>
      )
    }

    if (index === slidesLength - 1) {
      return (
        <View
          style={[
            styles.leftArrowWrap,
            this.state.leftArrowHide ? styles.hideArrow : null,
          ]}
        >
          <Image source={arrowImage} style={styles.rotateArrow} />
        </View>
      )
    }

    return (
      <React.Fragment>
        <View
          style={[
            styles.leftArrowWrap,
            this.state.leftArrowHide ? styles.hideArrow : null,
          ]}
        >
          <Image source={arrowImage} style={styles.rotateArrow} />
        </View>

        <View
          style={[
            styles.rightArrowWrap,
            this.state.rightArrowHide ? styles.hideArrow : null,
          ]}
        >
          <Image source={arrowImage} />
        </View>
      </React.Fragment>
    )
  }

  render() {
    const {
      slides = [],
      indicators,
      scrollContainerStyle,
      slideContainerStyle,
    } = this.props
    const { currentView } = this.state
    const scrollerWidth = slides.length * width

    const animatedStyles = {
      transform: [{ translateX: this.pan }],
    }

    return (
      <View style={styles.container}>
        <Animated.View
          style={[
            styles.scrollContainer,
            scrollContainerStyle,
            animatedStyles,
            { width: scrollerWidth },
          ]}
          {...this.scrollResponder.panHandlers}
        >
          {slides.map((slide, index) => (
            <View
              key={index}
              style={[styles.slideContainer, slideContainerStyle]}
            >
              {slide}

              {this._renderArrows(index, slides.length)}
            </View>
          ))}
        </Animated.View>

        {indicators ? (
          <View style={styles.indicatorsContainer}>
            {slides.map((slide, i) => (
              <View
                key={i}
                style={[
                  styles.indicator,
                  currentView === i ? styles.activeIndicator : null,
                ]}
              />
            ))}
          </View>
        ) : null}
      </View>
    )
  }
}
