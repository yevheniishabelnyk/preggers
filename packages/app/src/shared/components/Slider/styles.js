import { Dimensions, StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

import { footerHeight } from '@/auth/screens/Login/styles'

export const { width, height } = Dimensions.get('window')

export const indicatorSize = moderateScale(12, 0.35)

export const indicatorMargin = moderateScale(15, 0.35)

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    maxWidth: width,
  },

  scrollContainer: {
    flex: 1,
    flexDirection: 'row',
  },

  indicatorsContainer: {
    position: 'absolute',
    bottom: footerHeight + indicatorMargin,
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    zIndex: 0,
  },

  indicator: {
    borderRadius: moderateScale(6, 0.35),
    width: indicatorSize,
    height: indicatorSize,
    marginHorizontal: moderateScale(5),
    backgroundColor: '#ECE5FB',
  },

  activeIndicator: {
    backgroundColor: 'rgb(250, 65, 105)',
  },

  slideContainer: {
    width,
    flex: 1,
    position: 'relative',
  },

  whiteArrow: {
    width: moderateScale(10),
    height: moderateScale(17),
  },

  rotateArrow: {
    width: moderateScale(8),
    height: moderateScale(16),
    transform: [{ rotate: '180deg' }],
  },

  rightArrowWrap: {
    position: 'absolute',
    top: moderateScale(170, 0.9),
    right: moderateScale(15),
    alignItems: 'flex-end',
  },

  leftArrowWrap: {
    position: 'absolute',
    top: moderateScale(170, 0.9),
    left: moderateScale(15),
    alignItems: 'flex-start',
  },

  hideArrow: {
    opacity: 0,
  },
})
