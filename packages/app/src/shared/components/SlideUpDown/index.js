/**
 *
 * SlideUpDown
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Animated, Easing } from 'react-native'

import styles from './styles'

import { isFunction } from 'lodash'

export default class SlideUpDown extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    isVisible: PropTypes.bool,
    onSlideUp: PropTypes.func,
    onSlideDown: PropTypes.func,
    height: PropTypes.number,
  }

  render() {
    const { children } = this.props

    const animatedStyle = {
      marginBottom: this.marginBottomValue,
    }

    return (
      <Animated.View style={[styles.container, animatedStyle]}>
        {children}
      </Animated.View>
    )
  }

  componentWillMount() {
    const { height, isVisible } = this.props

    if (isVisible) {
      this.marginBottomValue = new Animated.Value(0)
    } else {
      this.marginBottomValue = new Animated.Value(-height)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isVisible && nextProps.isVisible) {
      this._slideUp()
    }

    if (this.props.isVisible && !nextProps.isVisible) {
      this._slideDown()
    }
  }

  _slideUp = () => {
    console.log('_slideUp')

    Animated.timing(this.marginBottomValue, {
      toValue: 0,
      duration: 300,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start(() => {
      const { onSlideUp } = this.props

      if (isFunction(onSlideUp)) {
        onSlideUp()
      }
    })
  }

  _slideDown = () => {
    console.log('_slideDown')

    const { height } = this.props

    Animated.timing(this.marginBottomValue, {
      toValue: -height,
      duration: 300,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start(() => {
      const { onSlideDown } = this.props

      if (isFunction(onSlideDown)) {
        onSlideDown()
      }
    })
  }
}
