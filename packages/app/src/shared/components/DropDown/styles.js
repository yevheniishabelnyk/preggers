import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.2

const circleWith = moderateScale(20, resizeFactor)
export const titleBlockHeight = moderateScale(60, resizeFactor)

const ulLiPrefixWidth = moderateScale(6, resizeFactor)

const styles = StyleSheet.create({
  container: {
    height: titleBlockHeight,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    borderRadius: 8,
    overflow: 'hidden',
    paddingHorizontal: moderateScale(20, resizeFactor),
  },

  titleBlock: {
    height: titleBlockHeight,
    flexDirection: 'row',
    alignItems: 'center',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: 'rgb(59,72,89)',
    letterSpacing: 0.7,
    backgroundColor: 'transparent',
    marginRight: 'auto',
    paddingRight: moderateScale(50, resizeFactor),
  },

  arrowDownIcon: {
    width: moderateScale(12, resizeFactor),
    height: moderateScale(6, resizeFactor),
  },

  rotateGrayArrow: {
    transform: [{ rotate: '180deg' }],
  },

  indicator: {
    width: circleWith,
    height: circleWith,
    borderRadius: circleWith / 2,
    backgroundColor: 'white',
    marginRight: moderateScale(12, resizeFactor),
  },

  green: {
    backgroundColor: 'rgb(64,224,190)',
  },

  orange: {
    backgroundColor: 'rgb(255,201,60)',
  },

  red: {
    backgroundColor: 'rgb(242,83,117)',
  },

  olStyle: {
    marginTop: 0,
  },

  olLiStyle: {
    flexDirection: 'row',
  },

  ulContent: {
    marginTop: moderateScale(2, resizeFactor),
  },

  ulLiStyle: {
    flexDirection: 'row',
  },

  olLiPrefix: {
    fontFamily: 'Roboto-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: 'rgb(59,72,89)',
    lineHeight: moderateScale(24, resizeFactor),
    width: moderateScale(17, resizeFactor),
    alignSelf: 'flex-start',
  },

  ulLiPrefix: {
    marginVertical: moderateScale(9, resizeFactor),
    marginRight: moderateScale(8, resizeFactor),
    width: ulLiPrefixWidth,
    height: ulLiPrefixWidth,
    borderRadius: ulLiPrefixWidth / 2,
    backgroundColor: 'rgb(59,72,89)',
  },

  contentContainerWrapper: {
    position: 'relative',
  },

  contentContainerWrapperAbsoluteFill: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: -1000,
  },

  contentContainer: {
    paddingBottom: moderateScale(13, resizeFactor),
  },

  imageTagWrapper: {
    marginHorizontal: moderateScale(-20, resizeFactor),
    alignSelf: 'stretch',
    height: moderateScale(200, resizeFactor),
  },

  imageTag: {
    flex: 1,
    width: null,
    height: null,
    marginHorizontal: moderateScale(-20, resizeFactor),
  },
})

export const tagStyles = {
  p: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(23, resizeFactor),
    color: 'rgb(131,146,167)',
    paddingBottom: 0,
    marginBottom: moderateScale(15, resizeFactor),
    flexWrap: 'wrap',
  },

  a: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(41,123,237)',
  },

  strong: {
    fontFamily: 'Roboto-Bold',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(76,82,91)',
  },

  em: {
    fontFamily: 'Roboto-LightItalic',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(131,146,167)',
  },

  h1: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17, resizeFactor),
  },

  h2: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17, resizeFactor),
  },

  h3: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17, resizeFactor),
  },
}

export default styles
