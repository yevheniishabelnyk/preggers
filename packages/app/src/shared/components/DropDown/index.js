/**
 *
 * DropDown
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, TouchableOpacity, Image, Animated } from 'react-native'

import HTML from 'react-native-render-html'

import arrowDownIcon from 'assets/icons/arrow-down-bold.png'

import styles, { titleBlockHeight, tagStyles } from './styles'

export default class DropDown extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
    onLinkPress: PropTypes.func,
    indicator: PropTypes.number,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    expanded: PropTypes.bool,
  }

  contentHeight = 0

  state = {
    contentIsVisible: this.props.expanded,
  }

  render() {
    const { style, title, content, indicator, expanded } = this.props
    const { contentIsVisible } = this.state

    const animatedStyle = {
      height: this.containerHeight,
    }

    return (
      <Animated.View style={[styles.container, style, animatedStyle]}>
        {content && !expanded ? (
          <TouchableOpacity
            style={styles.titleBlock}
            onPress={this._toggleContent}
            activeOpacity={0.95}
          >
            {this._renderIndicator(indicator)}

            <Text style={styles.title}>{title}</Text>

            <Image
              source={arrowDownIcon}
              style={[
                styles.arrowDownIcon,
                contentIsVisible ? styles.rotateGrayArrow : null,
              ]}
            />
          </TouchableOpacity>
        ) : (
          <View style={styles.titleBlock}>
            {this._renderIndicator(indicator)}

            <Text style={styles.title} selectable>
              {title}
            </Text>
          </View>
        )}

        {content ? (
          <HTML
            html={content}
            textSelectable
            // eslint-disable-next-line react/jsx-no-bind
            customWrapper={RNContent => (
              <View style={styles.contentContainerWrapper}>
                <View style={styles.contentContainerWrapperAbsoluteFill}>
                  <View
                    style={styles.contentContainer}
                    onLayout={this._onContentLayout}
                  >
                    {RNContent}
                    <View />
                  </View>
                </View>
              </View>
            )}
            tagsStyles={tagStyles}
            onLinkPress={this._linkPressed}
            renderers={{
              img: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                return (
                  <View style={styles.imageTagWrapper} key={passProps.key}>
                    <Image
                      source={{ uri: htmlAttribs.src }}
                      style={styles.imageTag}
                      resizeMode="contain"
                    />
                  </View>
                )
              },
              ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, (child, index) => {
                  return (
                    <View style={styles.olLiStyle}>
                      <Text style={styles.olLiPrefix}>{index + 1}.</Text>
                      {child}
                    </View>
                  )
                })

                return <View key={passProps.key}>{content}</View>
              },

              ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, child => {
                  return (
                    <View style={styles.ulLiStyle}>
                      <View style={styles.ulLiPrefix} />
                      {child}
                    </View>
                  )
                })

                return (
                  <View key={passProps.key} style={styles.ulContent}>
                    {content}
                  </View>
                )
              },
            }}
          />
        ) : null}
      </Animated.View>
    )
  }

  componentWillMount() {
    this.containerHeight = new Animated.Value(titleBlockHeight)
  }

  _linkPressed = (evt, href) => {
    const { onLinkPress } = this.props

    onLinkPress(href)
  }

  _onContentLayout = e => {
    const { expanded } = this.props

    this.contentHeight = e.nativeEvent.layout.height

    if (expanded) {
      this.containerHeight.setValue(titleBlockHeight + this.contentHeight)
    }
  }

  _toggleContent = () => {
    const { expanded } = this.props

    if (!expanded) {
      const { contentIsVisible } = this.state

      if (contentIsVisible) {
        this.setState({ contentIsVisible: false })
        setTimeout(() => {
          this._animateSlideUp()
        }, 0)
      } else {
        this.setState({ contentIsVisible: true })
        setTimeout(() => {
          this._animateSlideDown()
        }, 0)
      }
    }
  }

  _animateSlideUp = () =>
    Animated.timing(this.containerHeight, {
      toValue: titleBlockHeight,
      duration: 300,
    }).start()

  _animateSlideDown = () =>
    Animated.timing(this.containerHeight, {
      toValue: titleBlockHeight + this.contentHeight,
      duration: 300,
    }).start()

  _renderIndicator = indicator => {
    if (indicator !== undefined) {
      switch (indicator) {
        case 0:
          return <View style={[styles.indicator, styles.red]} />
        case 2:
          return <View style={[styles.indicator, styles.green]} />
        case 1:
          return <View style={[styles.indicator, styles.orange]} />
        default:
          return null
      }
    } else {
      return null
    }
  }
}
