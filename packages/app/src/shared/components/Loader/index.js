/**
 *
 * Loader
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image } from 'react-native'

import icon from 'assets/icons/loader.gif'

import styles from './styles'

const Loader = ({ style, background }) => (
  <View
    style={[styles.wrapper, { backgroundColor: background || 'white' }, style]}
  >
    <View style={styles.iconWrapper}>
      <Image source={icon} style={styles.icon} />
    </View>
  </View>
)

Loader.propTypes = {
  style: PropTypes.any,
  background: PropTypes.string,
}

export default Loader
