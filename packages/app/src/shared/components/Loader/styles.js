import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const iconWidth = moderateScale(67.5)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },

  icon: {
    width: iconWidth,
    height: iconWidth,
    borderRadius: iconWidth / 2,
  },

  iconWrapper: {
    marginTop: moderateScale(-36),
    width: iconWidth - 1,
    height: iconWidth - 1,
    borderRadius: (iconWidth - 1) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
})

export default styles
