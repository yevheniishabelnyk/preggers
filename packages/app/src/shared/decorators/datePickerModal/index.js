/*
 *
 * datePickerModal decorator
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
  DatePickerAndroid,
  DatePickerIOS,
  TouchableHighlight,
  View,
  Modal,
  Platform,
  TimePickerAndroid,
} from 'react-native'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import moment from 'moment'
import { isFunction } from 'lodash'

import styles from './styles'

import { DATE_FORMAT as DATABASE_DATE_FORMAT } from '@/app/constants/databaseDataFormats'

export default function datePickerModal(WrappedButton) {
  class FormDatePicker extends React.Component {
    static propTypes = {
      value: PropTypes.string,
      format: PropTypes.string,
      locale: PropTypes.string,
      maximumDateLimit: PropTypes.object,
      minimumDateLimit: PropTypes.object,
      onSubmit: PropTypes.func,
      title: PropTypes.string,
      mode: PropTypes.string,
      style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
        PropTypes.number,
      ]),
      contentContainerStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
        PropTypes.number,
      ]),
      initialDate: PropTypes.object,
    }

    static defaultProps = {
      format: DATABASE_DATE_FORMAT,
      mode: 'date',
    }

    constructor(props) {
      super(props)

      const initialDate = props.value
        ? moment(props.value, props.format).toDate()
        : props.initialDate || new Date()

      this.state = {
        date: initialDate,
        pickerIsVisible: false,
        previousDate: null,
      }
    }

    render() {
      const {
        title,
        value,
        contentContainerStyle,
        style,
        mode,
        locale,
      } = this.props
      const { pickerIsVisible, date } = this.state

      return (
        <View style={[styles.container, contentContainerStyle]}>
          <WrappedButton
            style={style}
            showDatePicker={this._show}
            title={title}
            value={value}
            mode={mode}
          />

          <Modal
            animationType="fade"
            transparent
            visible={pickerIsVisible}
            onRequestClose={this._hide}
          >
            <TouchableHighlight
              style={styles.cancelMask}
              onPress={this._cancel}
              underlayColor="rgba(255,255,255,.5)"
            >
              <View style={styles.modalContent}>
                {Platform.OS === 'ios' && (
                  <DatePickerIOS
                    style={styles.picker}
                    date={date}
                    mode={mode}
                    locale={locale}
                    onDateChange={this._onDateChange}
                  />
                )}

                <TouchableHighlight
                  style={styles.selectButton}
                  onPress={this._submit}
                  underlayColor="rgba(99, 65, 251, 0.5)"
                >
                  <FormattedMessage
                    {...messages.selectButtonTitle}
                    style={styles.selectButtonTitle}
                  />
                </TouchableHighlight>

                <TouchableHighlight
                  style={styles.cancelButton}
                  onPress={this._cancel}
                  underlayColor="rgba(255,255,255,.5)"
                >
                  <FormattedMessage
                    {...messages.cancelButtonTitle}
                    style={styles.cancelButtonTitle}
                  />
                </TouchableHighlight>
              </View>
            </TouchableHighlight>
          </Modal>
        </View>
      )
    }

    componentWillReceiveProps(nextProps) {
      if (
        this.props.value !== nextProps.value &&
        this.value !== nextProps.value
      ) {
        const nextDate = moment(nextProps.value, this.props.format).toDate()

        this.setState({ date: nextDate })
      }
    }

    _onDateChange = async date =>
      new Promise(resolve => this.setState({ date }, resolve()))

    _hide = () => this.setState({ pickerIsVisible: false })

    _show = async () => {
      const { mode } = this.props

      if (Platform.OS === 'ios') {
        this.setState({ pickerIsVisible: true, previousDate: this.state.date })
      } else if (Platform.OS === 'android') {
        if (mode === 'time') {
          try {
            const { action, hour, minute } = await TimePickerAndroid.open({
              is24Hour: true,
              date: this.state.date,
            })
            if (action !== TimePickerAndroid.dismissedAction) {
              await this._onDateChange(new Date(0, 0, 0, hour, minute))
              this._submit()
            }
          } catch ({ code, message }) {
            console.warn('Cannot open time picker', message)
          }
        } else {
          try {
            const { action, year, month, day } = await DatePickerAndroid.open({
              date: this.state.date,
            })

            if (action !== DatePickerAndroid.dismissedAction) {
              await this._onDateChange(new Date(year, month, day, 0, 0, 0, 0))
              this._submit()
            }
          } catch ({ code, message }) {
            console.warn('Cannot open date picker', message)
          }
        }
      }
    }

    _cancel = () => {
      const { previousDate } = this.state

      if (previousDate) {
        this.setState({ date: previousDate })
      }

      this._hide()
    }

    _submit = () => {
      const { onSubmit, format } = this.props
      const { date } = this.state

      let formattedDueDate,
        formattedDueDateMinDate,
        formattedDueDateMaxDate,
        isDateBetween = false

      formattedDueDate = moment(date).endOf('day')
      formattedDueDateMinDate = moment(this.props.minimumDateLimit).endOf('day')
      formattedDueDateMaxDate = moment(this.props.maximumDateLimit).endOf('day')

      if (
        formattedDueDate.isSameOrBefore(formattedDueDateMaxDate) &&
        formattedDueDate.isSameOrAfter(formattedDueDateMinDate)
      ) {
        isDateBetween = true
      }

      if (isFunction(onSubmit)) {
        this.value = moment(date).format(format)

        onSubmit(this.value, date, isDateBetween)
      }

      this._hide()
    }
  }

  return connect(state => ({ locale: state.settings.locale }))(FormDatePicker)
}
