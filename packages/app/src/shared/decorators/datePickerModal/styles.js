import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },

  cancelMask: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,.5)',
  },

  picker: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width,
    backgroundColor: 'white',
    paddingTop: moderateScale(50),
  },

  selectButton: {
    position: 'absolute',
    backgroundColor: '#6341fb',
    bottom: 210,
    right: moderateScale(15),
    borderRadius: moderateScale(20),
    paddingHorizontal: moderateScale(10, 1),
    paddingVertical: moderateScale(10, 0.3),
    alignItems: 'center',
    justifyContent: 'center',
  },

  selectButtonTitle: {
    fontSize: moderateScale(13),
    backgroundColor: 'transparent',
    color: 'white',
  },

  cancelButton: {
    backgroundColor: 'rgba(0,0,0,0)',
    position: 'absolute',
    bottom: 210,
    left: moderateScale(15),
    borderRadius: moderateScale(20),
    paddingHorizontal: moderateScale(10, 1),
    paddingVertical: moderateScale(10, 0.3),
    alignItems: 'center',
    justifyContent: 'center',
  },

  cancelButtonTitle: {
    fontSize: moderateScale(13),
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#8A8EAC',
  },

  formField: {
    flexDirection: 'row',
    paddingHorizontal: moderateScale(19),
    alignItems: 'center',
    height: moderateScale(60),
    borderRadius: 8,
    backgroundColor: 'white',
    overflow: 'hidden',
  },

  value: {
    fontSize: moderateScale(14),
    letterSpacing: 0.7,
    marginLeft: 'auto',
    fontFamily: 'Now-Medium',
    color: 'rgb(235,65,185)',
  },

  placeholderColor: {
    color: 'rgb(112,52,153)',
  },

  title: {
    fontSize: moderateScale(14),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },
})

export default styles
