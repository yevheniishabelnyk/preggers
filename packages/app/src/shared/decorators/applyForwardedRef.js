/**
 *
 * applyForwardedRef HOC
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import hoistStatics from 'hoist-non-react-statics'

export default function(WrappedComponent) {
  class ResultComponent extends React.Component {
    static propTypes = {
      forwardedRef: PropTypes.func,
    }

    render() {
      const { forwardedRef, ...otherProps } = this.props

      return <WrappedComponent {...otherProps} ref={forwardedRef} />
    }
  }

  return hoistStatics(ResultComponent, WrappedComponent)
}
