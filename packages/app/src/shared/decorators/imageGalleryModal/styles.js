import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.75)',
    paddingHorizontal: (15 / 375) * width,
    paddingVertical: (60 / 667) * height,
  },

  closeButton: {
    position: 'absolute',
    top: (70 / 667) * height,
    right: (25 / 375) * width,
  },

  gallery: {
    borderRadius: (6 / 375) * width,
    backgroundColor: 'white',
    padding: (20 / 375) * width,
  },
})

export default styles
