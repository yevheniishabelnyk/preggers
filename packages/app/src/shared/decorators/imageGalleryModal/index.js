/*
 *
 * imageGalleryModal decorator
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Modal } from 'react-native'
import CloseButton from '@/pregnancy/components/atoms/CloseButton'
import ExpoGallery from '@/shared/components/ExpoGallery'

import styles from './styles'

export default function imageGalleryModal(WrappedButton) {
  class ImageGallery extends React.Component {
    static propTypes = {
      images: PropTypes.array,
      style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
        PropTypes.number,
      ]),
      contentContainerStyle: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array,
        PropTypes.number,
      ]),
    }

    static defaultProps = {
      images: [],
    }

    constructor(props) {
      super(props)

      this.state = {
        galleryIsVisible: false,
      }
    }

    render() {
      const { contentContainerStyle, style, images, ...otherProps } = this.props
      const { galleryIsVisible } = this.state

      return (
        <View style={[styles.container, contentContainerStyle]}>
          <WrappedButton style={style} onPress={this.show} {...otherProps} />

          <Modal animationType="fade" visible={galleryIsVisible} transparent>
            <View style={styles.modalContent}>
              <CloseButton onPress={this.hide} style={styles.closeButton} />

              <ExpoGallery style={styles.gallery} images={images} />
            </View>
          </Modal>
        </View>
      )
    }

    hide = () => this.setState({ galleryIsVisible: false })

    show = () =>
      this.props.images.length && this.setState({ galleryIsVisible: true })
  }

  return ImageGallery
}
