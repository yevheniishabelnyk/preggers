/**
 *
 * forwardRef HOC
 *
 */

import React from 'react'
import hoistStatics from 'hoist-non-react-statics'

export default function(WrappedComponent) {
  return hoistStatics(
    React.forwardRef((props, ref) => (
      <WrappedComponent {...props} forwardedRef={ref} />
    )),
    WrappedComponent
  )
}
