/**
 *
 * Lb picker modal decorator
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Modal,
  Picker,
  TouchableHighlight,
  View,
  Text,
  Platform,
} from 'react-native'

import { isFunction } from 'lodash'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { BoxShadow } from '@/vendor/react-native-shadow'

import formatWeight from '@/shared/utils/formatWeight'

import convertUnits from 'convert-units'

import styles, { containerShadow } from './styles'

export default function lbPickerModal(WrappedButton) {
  return class LbPickerContainer extends React.Component {
    static propTypes = {
      title: PropTypes.string,
      placeholder: PropTypes.string,
      outputFormat: PropTypes.string,
      valueUnits: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.object,
      ]),
      onChange: PropTypes.func,
      style: PropTypes.any,
    }

    static contextTypes = {
      intl: PropTypes.object.isRequired,
    }

    lbsUnits = [
      { label: '0', value: 0 },
      { label: '1', value: 1 },
      { label: '2', value: 2 },
      { label: '3', value: 3 },
      { label: '4', value: 4 },
      { label: '5', value: 5 },
      { label: '6', value: 6 },
      { label: '7', value: 7 },
      { label: '8', value: 8 },
      { label: '9', value: 9 },
      { label: '10', value: 10 },
      { label: '11', value: 11 },
      { label: '12', value: 12 },
      { label: '13', value: 13 },
      { label: '14', value: 14 },
    ]

    ozUnits = [
      { label: '0', value: 0 },
      { label: '1', value: 1 },
      { label: '2', value: 2 },
      { label: '3', value: 3 },
      { label: '4', value: 4 },
      { label: '5', value: 5 },
      { label: '6', value: 6 },
      { label: '7', value: 7 },
      { label: '8', value: 8 },
      { label: '9', value: 9 },
      { label: '10', value: 10 },
      { label: '11', value: 11 },
      { label: '12', value: 12 },
      { label: '13', value: 13 },
      { label: '14', value: 14 },
      { label: '15', value: 15 },
    ]

    constructor(props) {
      super(props)
      this.labelLbsUnits = this.lbsUnits.reduce((out, item) => {
        out[item.value] = item.label
        return out
      }, {})

      this.labelOzUnits = this.ozUnits.reduce((out, item) => {
        out[item.value] = item.label
        return out
      }, {})

      this.state = this._getInitalState()
    }

    render() {
      const {
        style,
        value,
        title,
        outputFormat,
        valueUnits,
        placeholder,
      } = this.props
      const { isPickerVisible } = this.state

      let output

      if (value && outputFormat) {
        const values = { [valueUnits]: value }

        if (isPickerVisible) {
          values.lb = this.initialLbValue
          values.oz = this.initialOzValue
        } else {
          values.lb = this.state.lb
          values.oz = this.state.oz
        }

        output = formatWeight(values, outputFormat)
      } else {
        output = placeholder
      }

      return (
        <View style={[styles.container, style]}>
          <WrappedButton
            style={style}
            showLbPicker={this._show}
            title={title}
            value={output}
          />

          <Modal animationType="fade" transparent visible={isPickerVisible}>
            <TouchableHighlight
              style={styles.cancelMask}
              onPress={this._cancel}
              underlayColor="rgba(255,255,255,.5)"
            >
              {Platform.OS === 'ios' ? (
                <View style={styles.pickerWrapper}>
                  <View style={styles.pickerContainer}>
                    {this._renderLbPicker()}

                    <Text style={styles.unitsName}>lb</Text>
                  </View>

                  <View
                    style={[styles.pickerContainer, styles.ozPickerContainer]}
                  >
                    {this._renderOzPicker()}

                    <Text style={styles.unitsName}>oz</Text>
                  </View>

                  <TouchableHighlight
                    style={styles.selectButton}
                    onPress={this._select}
                    underlayColor="rgba(99,65,251,.8)"
                  >
                    <FormattedMessage
                      {...messages.selectButtonTitle}
                      style={styles.selectButtonTitle}
                    />
                  </TouchableHighlight>

                  <TouchableHighlight
                    style={styles.cancelButton}
                    onPress={this._cancel}
                  >
                    <FormattedMessage
                      {...messages.cancelButtonTitle}
                      style={styles.cancelButtonTitle}
                    />
                  </TouchableHighlight>
                </View>
              ) : (
                <View style={styles.androidPickerWrapper}>
                  <View style={styles.androidPickerContainer}>
                    <BoxShadow setting={containerShadow}>
                      {this._renderLbPicker()}

                      <Text style={styles.androidUnitsName}>lb</Text>
                    </BoxShadow>
                  </View>
                  <View style={styles.androidPickerContainer}>
                    <BoxShadow setting={containerShadow}>
                      {this._renderOzPicker()}

                      <Text style={styles.androidUnitsName}>oz</Text>
                    </BoxShadow>
                  </View>

                  <View style={styles.androidButtonsWrapper}>
                    <TouchableHighlight
                      style={styles.androidCancelButton}
                      onPress={this._cancel}
                    >
                      <FormattedMessage
                        {...messages.cancelButtonTitle}
                        style={styles.cancelButtonTitle}
                      />
                    </TouchableHighlight>

                    <TouchableHighlight
                      style={styles.androidSelectButton}
                      onPress={this._select}
                      underlayColor="rgba(99,65,251,.8)"
                    >
                      <FormattedMessage
                        {...messages.selectButtonTitle}
                        style={styles.selectButtonTitle}
                      />
                    </TouchableHighlight>
                  </View>
                </View>
              )}
            </TouchableHighlight>
          </Modal>
        </View>
      )
    }

    _renderLbPicker = () => {
      return (
        <Picker
          style={styles.picker}
          selectedValue={this.state.lb}
          onValueChange={this._setLb}
          itemStyle={styles.lbPickerItem}
        >
          {this.lbsUnits.map(item => (
            <Picker.Item
              key={item.value}
              label={item.label}
              value={item.value}
            />
          ))}
        </Picker>
      )
    }

    _renderOzPicker = () => {
      return (
        <Picker
          style={[styles.picker, styles.ozPicker]}
          selectedValue={this.state.oz}
          onValueChange={this._setOz}
          itemStyle={styles.ozPickerItem}
        >
          {this.ozUnits.map(item => (
            <Picker.Item
              key={item.value}
              label={item.label}
              value={item.value}
            />
          ))}
        </Picker>
      )
    }

    componentWillReceiveProps(nextProps) {
      if (
        !this.state.lb &&
        !this.state.oz &&
        !this.props.value &&
        nextProps.value
      ) {
        const initialState = this._getInitalState()

        this.setState(initialState)
      }
    }

    _select = () => {
      this._hide()

      const { onChange } = this.props

      if (isFunction(onChange)) {
        const { valueUnits } = this.props
        const { lb, oz } = this.state

        let value = null

        if (lb || oz) {
          switch (valueUnits) {
            case 'g':
              value = convertUnits(lb * 16 + oz)
                .from('oz')
                .to('g')
              break

            case 'kg':
              value = convertUnits(lb * 16 + oz)
                .from('oz')
                .to('kg')
              break

            case 'lb':
              value =
                lb +
                convertUnits(oz)
                  .from('oz')
                  .to('lb')
              break

            case 'oz':
              value =
                oz +
                convertUnits(lb)
                  .from('lb')
                  .to('oz')
              break
          }
        }

        onChange(value)
      }
    }

    _cancel = () => {
      if (
        this.initialLbValue !== this.state.lb ||
        this.initialOzValue !== this.state.oz
      ) {
        this.setState({
          lb: this.initialLbValue,
          oz: this.initialOzValue,
          isPickerVisible: false,
        })
      } else {
        this.setState({ isPickerVisible: false })
      }
    }

    _show = () => {
      this.initialLbValue = this.state.lb
      this.initialOzValue = this.state.oz

      this.setState({ isPickerVisible: true })
    }

    _hide = () => this.setState({ isPickerVisible: false })

    _setLb = lb => this.setState({ lb })

    _setOz = oz => this.setState({ oz })

    _getInitalState = () => {
      const { value, valueUnits } = this.props

      let lb = 0
      let oz = 0

      if (value) {
        switch (valueUnits) {
          case 'kg':
          case 'g': {
            const poundsWithDecimals = convertUnits(value)
              .from(valueUnits)
              .to('lb')

            let decimals = poundsWithDecimals - Math.floor(poundsWithDecimals)
            let decimalPlaces = poundsWithDecimals.toString().split('.')[1]
              .length
            decimals = decimals.toFixed(decimalPlaces)

            lb = Math.floor(poundsWithDecimals)
            oz = Math.round(
              convertUnits(decimals)
                .from('lb')
                .to('oz')
            )

            break
          }

          case 'lb': {
            const poundsWithDecimals = value

            let decimals = poundsWithDecimals - Math.floor(poundsWithDecimals)
            let decimalPlaces = poundsWithDecimals.toString().split('.')[1]
              .length
            decimals = decimals.toFixed(decimalPlaces)

            lb = Math.floor(poundsWithDecimals)
            oz = convertUnits(decimals)
              .from('lb')
              .to('oz')

            break
          }

          case 'oz':
            lb = Math.floor(value / 16)
            oz = value % 16
        }
      }

      return {
        isPickerVisible: false,
        lb,
        oz,
      }
    }
  }
}
