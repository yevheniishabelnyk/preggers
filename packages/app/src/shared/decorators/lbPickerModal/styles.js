import { StyleSheet, Platform, Dimensions } from 'react-native'

import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')

const resizeFactor = 0.3

export const containerShadow = {
  width: width - 40,
  height: 40,
  color: '#000000',
  radius: 8,
  opacity: 0.035,
  border: 8,
  x: 0,
  y: 8,
  style: { marginVertical: 8 },
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
  },

  pickerWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },

  pickerContainer: {
    position: 'absolute',
    bottom: 0,
    left: '-5%',
    width: '65%',
    backgroundColor: 'white',
    paddingTop: '8%',
    zIndex: 3,
  },

  ozPickerContainer: {
    left: '30%',
    zIndex: 2,
  },

  picker: {
    backgroundColor: Platform.OS === 'android' ? 'white' : 'transparent',
  },

  unitsName: {
    position: 'absolute',
    top: '50%',
    marginTop: moderateScale(7),
    height: moderateScale(21, resizeFactor),
    right: '28%',
    fontSize: moderateScale(21, resizeFactor),
    color: 'rgba(0,0,0,0.8)',
    zIndex: -1,
  },

  selectButton: {
    position: 'absolute',
    backgroundColor: '#6341fb',
    bottom: 195,
    right: 15,
    borderRadius: 20,
    padding: moderateScale(10, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 5,
    shadowOpacity: 0.1,
    shadowRadius: 8,
    shadowColor: '#111723',
    shadowOffset: { height: 6, width: -2 },
  },

  selectButtonTitle: {
    color: 'white',
  },

  cancelButton: {
    position: 'absolute',
    bottom: 195,
    left: 15,
    borderRadius: 20,
    padding: moderateScale(10, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 5,
  },

  cancelButtonTitle: {
    color: '#8A8EAC',
  },

  cancelMask: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,.5)',
  },

  androidPickerWrapper: {
    flex: 1,
    justifyContent: 'center',
  },

  androidPickerContainer: {
    paddingHorizontal: moderateScale(20, resizeFactor),
  },

  androidButtonsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingHorizontal: moderateScale(20, resizeFactor),
    paddingTop: moderateScale(30, resizeFactor),
    marginTop: moderateScale(10, resizeFactor),
  },

  androidCancelButton: {
    borderRadius: moderateScale(20, resizeFactor),
    padding: moderateScale(10, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 5,
  },

  androidSelectButton: {
    backgroundColor: '#6341fb',
    borderRadius: moderateScale(20, resizeFactor),
    padding: moderateScale(10, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 5,
    shadowOpacity: 0.1,
    shadowRadius: 8,
    shadowColor: '#111723',
    shadowOffset: { height: 6, width: -2 },
  },

  androidUnitsName: {
    position: 'absolute',
    marginTop: moderateScale(7),
    height: moderateScale(25, resizeFactor),
    right: moderateScale(25, resizeFactor),
    fontSize: moderateScale(21, resizeFactor),
    color: 'rgba(0,0,0,0.8)',
    zIndex: 2,
  },
})

export default styles
