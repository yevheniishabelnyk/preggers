import { defineMessages } from 'react-intl'

export default defineMessages({
  heading: 'Get 100 SEK to shop for',
  offer: 'EXCLUSIVE OFFER',
  text:
    'Sign up for Lekmer’s newsletter and recive a voucher. Minimum spend is 100 SEK',
})
