import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  body: {
    marginTop: moderateScale(214),
    paddingHorizontal: moderateScale(23),
  },

  heading: {
    color: 'rgb(0, 0, 0)',
    fontSize: moderateScale(25),
    fontFamily: 'Now-Bold',
    marginVertical: moderateScale(9),
    textAlign: 'center',
  },

  subheading: {
    color: 'rgb(29, 29, 29)',
    fontSize: moderateScale(12),
    lineHeight: moderateScale(22),
    fontFamily: 'Now-Regular',
    textAlign: 'center',
    marginTop: moderateScale(15),
  },

  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(27),
    color: 'rgb(15, 15, 15)',
    textAlign: 'center',
  },
})

export default styles
