/*
 *
 * LekmerDiscountForm screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Linking } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import LekmerHeader from '@/campaigns/lekmer-discount/components/LekmerHeader'
import LekmerForm from '@/campaigns/lekmer-discount/components/LekmerForm'
import LekmerResponseModal from '@/campaigns/lekmer-discount/components/LekmerResponseModal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { connect } from 'react-redux'
import { getPregnancy } from '@/app/redux/selectors'
import * as AdActions from '@/app/redux/ads/actions'

import { ADS_FETCH_HEADERS } from '@/app/redux/ads/constants'
import { CONTENT_SERVICE__API_BASE } from 'config'

import messages from './messages'

import styles from './styles'

const SUCCESS_URL =
  'http://clk.tradedoubler.com/click?p=191876&a=3040757&g=20374390&epi=deals_signup200sek'

@connect((state, props) => ({
  locale: state.settings.locale,
  deviceCountryCode: state.device.country,
  userPregnancy: getPregnancy(state, props.screenProps.profile.pregnancy),
}))
export default class LekmerDiscountForm extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    error: false,
    success: false,
    title: false,
    text: false,
    label: false,
    isFetching: true,
  }

  render() {
    const { formatMessage } = this.context.intl

    const { isFetching, error, success, title, text, label } = this.state

    const defaultDueDate = this.props.userPregnancy
      ? this.props.userPregnancy.dueDate
      : ''

    if (isFetching) {
      return null
    }

    return (
      <KeyboardAwareScrollView style={styles.container}>
        <LekmerHeader goBack={this._goBack} />

        <View style={styles.body}>
          <Text style={styles.subheading}>
            {label || formatMessage(messages.offer)}
          </Text>

          <Text style={styles.heading}>
            {title || formatMessage(messages.heading)}
          </Text>

          <Text style={styles.text}>
            {text || formatMessage(messages.text)}
          </Text>
        </View>

        <LekmerForm
          navigation={this.props.navigation}
          onSubmit={this._onSubmit}
          defaultEmail={this.props.screenProps.profile.email}
          defaultName={this.props.screenProps.profile.name}
          defaultDueDate={defaultDueDate}
          interactionAction={this._markClickInteraction}
        />

        <LekmerResponseModal
          visible={error || success}
          type={success ? 'success' : 'error'}
          onClose={this._onCloseModal}
          onOk={this._onModalOkButtonPressed}
        />
      </KeyboardAwareScrollView>
    )
  }

  componentDidMount() {
    const { navigation, screenProps } = this.props

    const { email } = screenProps.profile

    const connectedAdUid = navigation.getParam('connectedAdUid')

    const adSource = navigation.getParam('adSource')

    this._fetchTexts()

    if (connectedAdUid && email) {
      this._(
        AdActions.markInteractionByUidAndEmail(
          connectedAdUid,
          email,
          'impression',
          adSource
        )
      )
    }
  }

  _fetchTexts = async () => {
    const { locale, navigation } = this.props

    const localeMap = {
      en: 'en_GB',
      sv: 'sv_SE',
    }

    const localeMapped = localeMap[locale]

    const connectedAdUid = navigation.getParam('connectedAdUid')

    if (connectedAdUid) {
      try {
        const response = await fetch(
          `${CONTENT_SERVICE__API_BASE}/content/fetch/${connectedAdUid}`,
          {
            method: 'POST',
            headers: ADS_FETCH_HEADERS,
            body: JSON.stringify({
              locale: localeMapped,
            }),
          }
        )

        if (response.ok) {
          const json = await response.json()

          this.setState({
            title: json.title,
            text: json.text,
            label: json.label,
            isFetching: false,
          })
        } else {
          this.setState({ isFetching: false })
        }
      } catch (err) {
        console.log('Error when fetching lekmer copy', err)

        this.setState({ isFetching: false })
      }
    } else {
      this.setState({ isFetching: false })
    }
  }

  _markClickInteraction = () => {
    const { profile } = this.props.screenProps
    const { navigation } = this.props

    const connectedAdUid = navigation.getParam('connectedAdUid')
    const adSource = navigation.getParam('adSource')

    if (connectedAdUid && profile.email) {
      this._(
        AdActions.markInteractionByUidAndEmail(
          connectedAdUid,
          profile.email,
          'click',
          adSource
        )
      )
    }
  }

  _onSubmit = async formData => {
    const { profile } = this.props.screenProps

    const { locale, deviceCountryCode, navigation } = this.props

    const { id } = profile

    const connectedAdUid = navigation.getParam('connectedAdUid')

    const adSource = navigation.getParam('adSource')

    const { phoneNumber, email, name, dueDate } = formData

    try {
      const response = await fetch(
        `${CONTENT_SERVICE__API_BASE}/lekmer/new-100-discount/`,
        {
          method: 'POST',
          headers: ADS_FETCH_HEADERS,
          body: JSON.stringify({
            contentUid: connectedAdUid,
            id,
            name,
            email,
            userEmail: profile.email,
            phoneNumber,
            locale,
            dueDate,
            adSource,
            countryCode: profile.location
              ? profile.location.countryCode
              : deviceCountryCode,
          }),
        }
      )

      if (response.ok) {
        const json = await response.json()

        if (Number(json.code) === 201) {
          this.setState({ success: true })
        } else {
          this.setState({ error: true })
        }
      } else {
        this.setState({ error: true })
      }
    } catch (err) {
      this.setState({ error: true })
    }
  }

  _onCloseModal = async () => {
    if (this.state.error) {
      this.setState({ error: false })
    }

    if (this.state.success) {
      this.setState({ success: false }, this._goBack)
    }
  }

  _onModalOkButtonPressed = async () => {
    if (this.state.error) {
      this.setState({ error: false })
    }

    if (this.state.success) {
      this.setState({ success: false })

      const supported = Linking.canOpenURL(SUCCESS_URL)

      if (supported) {
        await Linking.openURL(SUCCESS_URL)
      }

      this._goBack()
    }
  }
}
