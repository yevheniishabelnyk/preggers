/*
 *
 * Lekmer campaign screen routes
 *
 */

import LekmerDiscountForm from './screens/LekmerDiscountForm'

export default {
  '/lekmer-discount-form': {
    screen: LekmerDiscountForm,
  },
}
