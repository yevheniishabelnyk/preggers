import React from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import { WebBrowser } from 'expo'
import { isFunction } from 'lodash'

import ActionButton from '@/settings/components/atoms/ActionButton'
import OnboardingTextInput from '@/onboarding/components/OnboardingTextInput'
import SettingsDatePicker from '@/settings/components/molecules/SettingsDatePicker'
import FormError from '@/forms/components/atoms/FormError'

import moment from 'moment'

import messages from './messages'
import styles from './styles'

const LEKMER_TERMS_URL =
  'https://firebasestorage.googleapis.com/v0/b/preggers-production.appspot.com/o/lekmer%2FLekmers%20Integritetspolicy.pdf?alt=media&token=e64747ef-1784-42fc-ac30-1a8cd3654aff'

export default class LekmerForm extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static propTypes = {
    onSubmit: PropTypes.func,
    onAfterSubmit: PropTypes.func,
    interactionAction: PropTypes.func,
    navigation: PropTypes.object,
    defaultEmail: PropTypes.string,
    defaultName: PropTypes.string,
    defaultDueDate: PropTypes.string,
    isLarge: PropTypes.bool,
  }

  state = {
    name: '',
    nameError: false,
    phoneNumber: '',
    phoneNumberError: false,
    email: '',
    emailError: false,
    dueDate: '',
    dueDateError: false,
    canSubmit: false,
    isSubmitting: false,
  }

  constructor(props) {
    super(props)

    this.dueDateMinDate = new Date()

    this.dueDateMaxDate = moment()
      .add(266, 'days')
      .toDate()
  }

  componentDidMount() {
    const { defaultEmail, defaultName, defaultDueDate } = this.props

    if (defaultEmail || defaultName || defaultDueDate) {
      this.setState({
        email: defaultEmail,
        name: defaultName,
        dueDate: defaultDueDate,
      })
    }
  }

  render() {
    const { formatMessage } = this.context.intl

    const {
      name,
      nameError,
      phoneNumber,
      phoneNumberError,
      email,
      emailError,
      isSubmitting,
      dueDate,
    } = this.state

    return (
      <View style={styles.container}>
        <OnboardingTextInput
          placeholder={formatMessage(messages.name)}
          value={name}
          onChangeText={this._onChangeName}
          titleStyle={styles.input}
          inputStyle={styles.input}
          style={styles.inputContainer}
          error={nameError}
          autoCorrect={false}
          returnKeyType="done"
        />

        <FormError
          style={styles.formError}
          isVisible={nameError}
          title={formatMessage(messages.nameError)}
        />

        <OnboardingTextInput
          placeholder={formatMessage(messages.email)}
          value={email}
          onChangeText={this._onChangeEmail}
          keyboardType="email-address"
          titleStyle={styles.input}
          inputStyle={styles.input}
          style={styles.inputContainer}
          error={emailError}
          autoCorrect={false}
          returnKeyType="done"
        />

        <FormError
          style={styles.formError}
          isVisible={emailError}
          title={formatMessage(messages.emailError)}
        />

        <OnboardingTextInput
          placeholder={formatMessage(messages.phoneNumber)}
          value={phoneNumber}
          onChangeText={this._onChangePhoneNumber}
          keyboardType="phone-pad"
          titleStyle={styles.input}
          inputStyle={styles.input}
          style={styles.inputContainer}
          error={phoneNumberError}
          autoCorrect={false}
          returnKeyType="done"
        />

        <FormError
          style={styles.formError}
          isVisible={phoneNumberError}
          title={formatMessage(messages.phoneNumberError)}
        />

        <SettingsDatePicker
          title={formatMessage(messages.dueDatePlaceholder)}
          value={dueDate}
          onSubmit={this._onChangeDueDate}
          initialDate={
            this.props.defaultDueDate
              ? new Date(this.props.defaultDueDate)
              : undefined
          }
          minimumDateLimit={this.dueDateMinDate}
          maximumDateLimit={this.dueDateMaxDate}
          style={[styles.inputContainer, styles.datePicker]}
          error={this.state.dueDateError}
        />

        <View style={styles.buttonContainer}>
          <ActionButton
            title={formatMessage(messages.buttonText)}
            onPress={this._onSubmit}
            textStyle={styles.buttonText}
            style={styles.button}
            buttonColor="rgb(51, 212, 176)"
            loader={isSubmitting}
          />
        </View>

        <Text style={styles.terms} onPress={this._onTermsPress}>
          {formatMessage(messages.terms)}
        </Text>
      </View>
    )
  }

  _onTermsPress = () => WebBrowser.openBrowserAsync(LEKMER_TERMS_URL)

  _updateCanSubmit = async () => {
    const { defaultDueDate } = this.props

    const { phoneNumber, name, email, dueDate } = this.state

    let [nameError, emailError, phoneNumberError, dueDateError] = [
      false,
      false,
      false,
      false,
    ]

    // Swedish phone number
    if (!phoneNumber.match(/^(\+46|0046|0)7[0-9]{8}$/)) {
      phoneNumberError = true
    }

    // Name exists
    if (name.length < 2) {
      nameError = true
    }

    if (!email.match(/\S+@\S+\.\S+/)) {
      emailError = true
    }

    if (defaultDueDate && !dueDate) {
      dueDateError = true
    }

    const canSubmit =
      !phoneNumberError && !nameError && !emailError && !dueDateError

    await this._setStateAsync({
      canSubmit,
      phoneNumberError,
      emailError,
      nameError,
      dueDateError,
    })
  }

  _setStateAsync = data => new Promise(resolve => this.setState(data, resolve))

  _onChangePhoneNumber = phoneNumber =>
    this._onChangeValue('phoneNumber', phoneNumber)

  _onChangeDueDate = dueDate => this._onChangeValue('dueDate', dueDate)

  _onChangeEmail = email => this._onChangeValue('email', email)

  _onChangeName = name => this._onChangeValue('name', name)

  _onChangeValue = (key, value) => this.setState({ [key]: value })

  _onSubmit = async () => {
    const { name, phoneNumber, email, dueDate } = this.state

    const { interactionAction } = this.props

    if (isFunction(interactionAction)) {
      interactionAction()
    }

    await this._updateCanSubmit()

    if (this.state.canSubmit) {
      this.setState({ isSubmitting: true })

      await this.props.onSubmit({
        name,
        phoneNumber,
        email,
        dueDate,
      })

      this.setState({ isSubmitting: false })
    }
  }
}
