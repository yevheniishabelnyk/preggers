import { defineMessages } from 'react-intl'

export default defineMessages({
  buttonText: 'Send',
  name: 'Name',
  nameError: 'Please enter a name',
  email: 'E-mail',
  emailError: 'Please enter a valid e-mail address',
  phoneNumber: 'Mobile number',
  phoneNumberError: 'Please enter a valid Swedish mobile number',
  terms:
    'By clicking "Send", I confirm that I\'ve read and accepted Lekmer\'s privacy policy',
  dueDatePlaceholder: 'Due date',
})
