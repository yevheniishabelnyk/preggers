import { StyleSheet, Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,.65)',
    alignItems: 'center',
    justifyContent: 'center',
    width,
    height,
  },

  dialog: {
    width: moderateScale(331),
    maxHeight: moderateScale(547),
    overflow: 'hidden',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: moderateScale(8),
  },

  errorImage: {
    marginTop: moderateScale(46),
    width: moderateScale(123),
    height: moderateScale(148),
    marginBottom: moderateScale(20),
  },

  successImage: {
    marginTop: moderateScale(30),
    width: moderateScale(165),
    height: moderateScale(179),
    marginBottom: moderateScale(20),
  },

  text: {
    fontFamily: 'Roboto-Regular',
    lineHeight: moderateScale(24),
    marginTop: moderateScale(20),
    fontSize: moderateScale(14),
    color: 'rgb(52, 56, 62)',
    textAlign: 'center',
    width: moderateScale(263),
  },

  largeText: {
    fontFamily: 'Now-Bold',
    marginTop: moderateScale(20),
    fontSize: moderateScale(25),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    marginHorizontal: moderateScale(35),
  },

  buttonText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16),
    color: 'white',
  },

  button: {
    marginHorizontal: moderateScale(15),
    marginBottom: moderateScale(29),
    marginTop: moderateScale(35),
    alignItems: 'center',
    justifyContent: 'center',
  },

  closeModalButton: {
    position: 'absolute',
    top: moderateScale(5),
    right: moderateScale(5),
    width: moderateScale(40),
    height: moderateScale(40),
    alignItems: 'center',
    justifyContent: 'center',
  },

  closeModalIcon: {
    width: moderateScale(16),
    height: moderateScale(16),
  },
})

export default styles
