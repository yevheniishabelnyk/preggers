/**
 *
 * LekmerResponseModal
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { isFunction } from 'lodash'

import { View, Text, Image, Modal, TouchableOpacity } from 'react-native'
import ActionButton from '@/settings/components/atoms/ActionButton'

import styles from './styles'

import messages from './messages'

export default class LekmerResponseModal extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static propTypes = {
    type: PropTypes.string,
    visible: PropTypes.bool,
    onClose: PropTypes.func,
    onOk: PropTypes.func,
  }

  render() {
    return (
      <Modal animationType="fade" transparent visible={this.props.visible}>
        <View style={styles.container}>
          <View style={styles.dialog}>
            <TouchableOpacity
              onPress={this.props.onClose}
              style={styles.closeModalButton}
            >
              <Image
                source={require('assets/icons/cross-gray.png')}
                style={styles.closeModalIcon}
              />
            </TouchableOpacity>

            {this._renderGraphic()}

            <Text style={styles.largeText}>{this._getTitle()}</Text>

            <Text style={styles.text}>{this._getText()}</Text>

            <ActionButton
              title={this._getButtonText()}
              onPress={this._onPress}
              style={styles.button}
              textStyle={styles.buttonText}
            />
          </View>
        </View>
      </Modal>
    )
  }

  _renderGraphic = () => {
    const { type } = this.props
    let source = require('assets/img/connection-faild.png')
    let style = styles.errorImage

    if (type === 'success') {
      source = require('assets/img/connection-success.png')
      style = styles.successImage
    }

    return <Image resizeMode="contain" source={source} style={style} />
  }

  _getTitle = () => {
    const { formatMessage } = this.context.intl

    const { type } = this.props

    if (type === 'success') {
      return formatMessage(messages.successTitle)
    }

    return formatMessage(messages.errorTitle)
  }

  _getText = () => {
    const { formatMessage } = this.context.intl

    const { type } = this.props

    if (type === 'success') {
      return formatMessage(messages.successText)
    }

    return formatMessage(messages.errorText)
  }

  _getButtonText = () => {
    const { formatMessage } = this.context.intl

    const { type } = this.props

    if (type === 'success') {
      return formatMessage(messages.successButtonText)
    }

    return formatMessage(messages.errorButtonText)
  }

  _onPress = () => {
    const { onOk } = this.props

    if (isFunction(onOk)) {
      onOk()
    }
  }
}
