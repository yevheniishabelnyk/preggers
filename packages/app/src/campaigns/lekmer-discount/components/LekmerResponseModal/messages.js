import { defineMessages } from 'react-intl'

export default defineMessages({
  errorTitle: 'Oh no!',
  errorText: 'Something went wrong.',
  errorButtonText: 'Ok',
  successTitle: 'Woop woop!',
  successText: 'You will get your voucher to your e-mail.',
  successButtonText: 'Use your code',
})
