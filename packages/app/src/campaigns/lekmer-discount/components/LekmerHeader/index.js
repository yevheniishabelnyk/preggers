import React from 'react'
import PropTypes from 'prop-types'
import { View, Image } from 'react-native'

import GoBackButton from '@/onboarding/components/GoBackButton'

import styles from './styles'

class LekmerHeader extends React.Component {
  static propTypes = {
    goBack: PropTypes.func,
  }

  render() {
    const { goBack } = this.props

    return (
      <View style={styles.header}>
        <GoBackButton onPress={goBack} style={styles.backButton} />

        <View style={styles.headerImageWrapper}>
          <Image
            source={require('assets/img/lekmer-form-header.png')}
            style={styles.headerImage}
          />
        </View>
      </View>
    )
  }
}

export default LekmerHeader
