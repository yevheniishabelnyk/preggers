import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'
const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: moderateScale(214),
    width,
    alignItems: 'center',
    justifyContent: 'center',
  },

  backButton: {
    top: moderateScale(30),
  },

  headerImageWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
  },

  headerImage: {
    flex: 1,
    width: null,
    height: null,
  },
})

export default styles
