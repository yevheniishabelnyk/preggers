/**
 *
 * ErrorScreen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Image } from 'react-native'
import { Updates } from 'expo'
import ActionButton from '@/settings/components/atoms/ActionButton'
import LogoutButton from '@/settings/components/molecules/LogoutButton'

import * as RouterActions from '@/app/redux/router/actions'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

import { withFirebase } from 'react-redux-firebase'

@withFirebase
@connect(state => ({
  auth: state.firebase.auth,
}))
export default class ErrorScreen extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    auth: PropTypes.object,
    firebase: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    updateIsAvailable: false,
  }

  render() {
    const { auth } = this.props
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <Image source={require('assets/img/cup.png')} style={styles.image} />

        <FormattedMessage {...messages.errorMessage} style={styles.message} />

        <ActionButton
          title={formatMessage(messages.homeButtonTitle)}
          onPress={this._resetToStart}
          style={styles.homeButton}
        />

        <ActionButton
          title={formatMessage(messages.reloadButtonTitle)}
          onPress={this._reload}
          style={styles.reloadButton}
          gray={!this.state.updateIsAvailable}
        />

        {!auth.isEmpty ? (
          <LogoutButton style={styles.logoutButton} onPress={this._logout} />
        ) : null}
      </View>
    )
  }

  async componentWillMount() {
    try {
      const update = await Updates.checkForUpdateAsync()

      if (update.isAvailable) {
        this.setState({ updateIsAvailable: update.isAvailable })

        await Updates.fetchUpdateAsync()
      }
    } catch (err) {
      console.info('err: ', err)
    }
  }

  _logout = () => this.props.firebase.logout()

  _reload = () => {
    if (this.state.updateIsAvailable) {
      Updates.reload()
    }
  }

  _resetToStart = () => {
    const { auth, dispatch } = this.props

    if (auth.isEmpty) {
      dispatch(RouterActions.resetToNonAuthenitcatedInitialRoute())
    } else {
      dispatch(RouterActions.resetToAuthenitcatedInitialRoute())
    }
  }
}
