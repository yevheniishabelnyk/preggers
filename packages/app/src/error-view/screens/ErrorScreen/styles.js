import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const gutter = 0.05 * width

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: gutter,
    alignItems: 'center',
    paddingTop: (30 / 375) * width,
    paddingBottom: (20 / 667) * height,
  },

  image: {
    width: (335 / 375) * width,
    height: (145 / 375) * width,
    backgroundColor: 'transparent',
    marginTop: (65 / 667) * height,
    marginBottom: (53 / 667) * height,
  },

  message: {
    fontFamily: 'Now-Medium',
    fontSize: (18 / 667) * height,
    lineHeight: (27 / 667) * height,
    color: 'rgb(43,56,87)',
    maxWidth: (255 / 375) * width,
    textAlign: 'center',
  },

  homeButton: {
    marginTop: 'auto',
    marginBottom: (19 / 667) * height,
  },

  reloadButton: {
    marginBottom: (19 / 667) * height,
  },

  logoutButton: {
    marginBottom: (19 / 667) * height,
  },
})

export default styles
