import { defineMessages } from 'react-intl'

export default defineMessages({
  errorMessage: 'Ooops! Seems like something went wrong.',
  homeButtonTitle: 'Continue from Start',
  reloadButtonTitle: 'Reload app',
})
