/*
 *
 * Error routes
 *
 */

import ErrorScreen from './screens/ErrorScreen'

export default {
  '/error': {
    screen: ErrorScreen,
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
}
