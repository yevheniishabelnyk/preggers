/**
 *
 * Home Navigator
 *
 */

import { TabNavigator } from 'react-navigation'
import { createTabBarLabel } from './components/Tab'

import StartViewNavigator from '@/start-view/navigator'
import Checklist from '@/checklist/screens/Checklist'
import ToolsMenu from '@/tools/screens/ToolsMenu'
import KnowledgeNavigator from '@/knowledge/navigator'
import DealSection from '@/deals/screens/DealSection'
import * as RouterActions from '@/app/redux/router/actions'

import startViewIcon from 'assets/icons/logo-gray.png'
import startViewActiveIcon from 'assets/icons/logo-emerald.png'
import checklistIcon from 'assets/icons/checklist.png'
import checklistActiveIcon from 'assets/icons/checklist-emerald.png'
import toolsIcon from 'assets/icons/tools.png'
import toolsActiveIcon from 'assets/icons/tools-emerald.png'
import knowledgeIcon from 'assets/icons/knowledge.png'
import knowledgeActiveIcon from 'assets/icons/knowledge-emerald.png'
import dealsIcon from 'assets/icons/deal-icon.png'
import activeDealIcon from 'assets/icons/active-deal-icon.png'

import { isIPhoneXs, isIPhoneXsMax } from '@/shared/utils/isIPhoneX'

import { moderateScale } from '@/app/scaling'

let height

if (isIPhoneXs()) {
  height = moderateScale(50)
} else if (isIPhoneXsMax()) {
  height = moderateScale(90)
} else {
  height = moderateScale(60)
}

export default TabNavigator(
  {
    '/~start-view': {
      screen: StartViewNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarOnPress: ({ scene: { route }, previousScene }) => {
          if (previousScene.routeName === route.routeName) {
            const activeNestedRoute = previousScene.routes[previousScene.index]

            if (
              activeNestedRoute.routeName === '/~pregnancy' &&
              activeNestedRoute.index !== 0
            ) {
              navigation.dispatch(
                RouterActions.goToComplexUrl(
                  route.routeName +
                    activeNestedRoute.routeName +
                    activeNestedRoute.routes[0].routeName
                )
              )
            }
          } else {
            navigation.navigate(route.routeName)
          }
        },

        tabBarLabel: createTabBarLabel({
          messageId: 'start',
          activeIcon: startViewActiveIcon,
          inactiveIcon: startViewIcon,
        }),
      }),
    },

    '/~knowledge': {
      screen: KnowledgeNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarOnPress: ({ scene: { route }, previousScene }) => {
          if (previousScene.routeName === route.routeName) {
            if (previousScene.index !== 0) {
              navigation.dispatch(
                RouterActions.goToComplexUrl(
                  route.routeName + route.routes[0].routeName
                )
              )
            }
          } else {
            navigation.navigate(route.routeName)
          }
        },

        tabBarLabel: createTabBarLabel({
          messageId: 'knowledge',
          activeIcon: knowledgeActiveIcon,
          inactiveIcon: knowledgeIcon,
        }),
      }),
    },

    '/checklist': {
      screen: Checklist,
      navigationOptions: {
        tabBarLabel: createTabBarLabel({
          messageId: 'checklist',
          activeIcon: checklistActiveIcon,
          inactiveIcon: checklistIcon,
        }),
      },
    },

    '/tools-menu': {
      screen: ToolsMenu,
      navigationOptions: {
        tabBarLabel: createTabBarLabel({
          messageId: 'tools',
          activeIcon: toolsActiveIcon,
          inactiveIcon: toolsIcon,
        }),
      },
    },

    '/deal-section': {
      screen: DealSection,
      navigationOptions: {
        tabBarLabel: createTabBarLabel({
          messageId: 'deal',
          activeIcon: activeDealIcon,
          inactiveIcon: dealsIcon,
        }),
      },
    },
  },
  {
    initialRouteName: '/~start-view',
    order: [
      '/~start-view',
      '/~knowledge',
      '/checklist',
      '/tools-menu',
      '/deal-section',
    ],
    navigationOptions: {
      gesturesEnabled: false,
    },
    swipeEnabled: false,
    animationEnabled: true,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      showLabel: true,
      indicatorStyle: { backgroundColor: 'transparent' },
      tabStyle: {
        marginTop: -moderateScale(10, 1),
        height: moderateScale(60),
        margin: 0,
        marginBottom: isIPhoneXs() ? -14 : null,
      },
      style: {
        backgroundColor: 'white',
        height,
        padding: 0,
        paddingBottom: isIPhoneXsMax() ? moderateScale(20) : 0,
        margin: 0,
      },
    },
  }
)
