/**
 *
 * Create Tab
 *
 */

import React from 'react'

import { View, Image } from 'react-native'

import memoize from 'fast-memoize'
import Label from '../Label'

import styles from './styles'

export function createTabBarLabel({ messageId, activeIcon, inactiveIcon }) {
  return memoize(({ focused }) => {
    return (
      <View style={styles.labelWrap}>
        <View style={styles.iconWrapper}>
          <Image
            style={styles.icon}
            source={focused ? activeIcon : inactiveIcon}
            resizeMode="contain"
          />
        </View>

        <Label focused={focused} messageId={messageId} />
      </View>
    )
  })
}
