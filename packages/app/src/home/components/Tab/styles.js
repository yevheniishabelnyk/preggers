import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  labelWrap: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: moderateScale(60),
    marginTop: moderateScale(20),
  },

  iconWrapper: {
    width: moderateScale(26, 0.5),
    height: moderateScale(26, 0.5),
    marginBottom: moderateScale(5, 0.5),
  },

  icon: {
    width: null,
    height: null,
    flex: 1,
  },
})

export default styles
