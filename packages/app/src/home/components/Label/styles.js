import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  label: {
    fontSize: moderateScale(8),
    fontFamily: 'Now-Medium',
    color: 'rgb(49,38,56)',
    textAlign: 'center',
  },

  labelActiveColor: {
    color: 'rgb(64,224,190)',
  },
})

export default styles
