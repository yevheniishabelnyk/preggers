/**
 *
 * Home navigator tab bar label
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'

import memoize from 'fast-memoize'

import messages from './messages'

import styles from './styles'

export default class Label extends React.Component {
  static propTypes = {
    focused: PropTypes.bool,
    messageId: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { focused, messageId } = this.props
    const { formatMessage } = this.context.intl

    let label = ''

    if (messages[messageId]) {
      label = formatMessage(messages[messageId]).toUpperCase()
    }

    return this._render(focused, label)
  }

  _render = memoize((focused, label) => (
    <Text style={[styles.label, focused ? styles.labelActiveColor : null]}>
      {label}
    </Text>
  ))
}
