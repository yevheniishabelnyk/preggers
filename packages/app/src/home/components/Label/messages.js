import { defineMessages } from 'react-intl'

export default defineMessages({
  start: 'Start',
  checklist: 'Checklist',
  knowledge: 'Knowledge',
  tools: 'Tools',
  deal: 'Deals',
})
