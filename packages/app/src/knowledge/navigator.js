/**
 *
 * Knowledge Navigator
 *
 */

import { TabNavigator } from 'react-navigation'

import MyWeek from '@/knowledge/screens/MyWeek'
import Articles from '@/knowledge/screens/Articles'
import Videos from '@/knowledge/screens/Videos'
import Podcasts from '@/knowledge/screens/Podcasts'
import TabBar from '@/knowledge/components/organisms/TabBar'

export default TabNavigator(
  {
    '/my-week': {
      screen: MyWeek,
    },

    '/articles': {
      screen: Articles,
    },

    '/videos': {
      screen: Videos,
    },

    '/podcasts': {
      screen: Podcasts,
    },
  },
  {
    initialRouteName: '/my-week',
    order: ['/my-week', '/articles', '/videos', '/podcasts'],
    navigationOptions: {
      gesturesEnabled: false,
    },
    animationEnabled: true,
    tabBarPosition: 'top',
    tabBarComponent: TabBar,
  }
)
