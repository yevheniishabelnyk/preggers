/*
 *
 * Knowledge routes
 *
 */

import Article from '@/knowledge/screens/Article'
import Search from '@/knowledge/screens/Search'

export default {
  '/knowledge-article': {
    screen: Article,
  },

  '/knowledge-search': {
    screen: Search,
  },
}
