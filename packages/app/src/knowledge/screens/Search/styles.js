import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    alignItems: 'stretch',
  },

  header: {
    backgroundColor: 'rgb(249,250,252)',
    zIndex: 1,
  },

  headerTitleWrapper: {
    alignItems: 'center',
  },

  headerTitle: {
    textAlign: 'center',
    color: 'rgb(59,72,89)',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, 0.6),
    marginVertical: moderateScale(30),
  },
})

export default styles
