/**
 *
 * Search
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Text, View } from 'react-native'

import { NavigationActions } from 'react-navigation'

import GoBackButton from '@/onboarding/components/GoBackButton'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'

import KnowledgeItemsSearch from '@/algolia-search/preggers-knowledge-items'

import messages from './messages'

import styles from './styles'

@connect(state => ({
  router: state.router,
}))
export default class Search extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.tags = this.props.navigation.getParam('tags')

    if (this.tags) {
      this.refinementList = { _tags: this.tags }
    }
  }

  render() {
    const { locale } = this.props.screenProps
    const { formatMessage } = this.context.intl
    const headerTitle = this.tags
      ? this.tags[0]
      : formatMessage(messages.articles)

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
      >
        <View style={styles.header}>
          <GoBackButton
            onPress={this._goBack}
            style={styles.stickyBackButton}
          />

          <View style={styles.headerTitleWrapper}>
            <Text style={styles.headerTitle}>{headerTitle}</Text>
          </View>
        </View>

        <KnowledgeItemsSearch
          locale={locale}
          onClose={this._goBack}
          onItemPress={this._itemPressed}
          searchPlaceholder={formatMessage(messages.searchInputPlaceholder)}
          refinementList={this.refinementList}
        />
      </ViewWrapper>
    )
  }

  _itemPressed = item => {
    const { router } = this.props
    const prevRoute = router.routes[router.index - 1]

    if (prevRoute && prevRoute.routeName === '/knowledge-article') {
      this.props.navigation.goBack()

      this.props.navigation.dispatch({
        type: NavigationActions.SET_PARAMS,
        key: prevRoute.key,
        params: {
          articleId: item.id,
        },
      })
    } else {
      this._goTo('/knowledge-article', { articleId: item.id })
    }
  }
}
