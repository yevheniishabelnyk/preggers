import { defineMessages } from 'react-intl'

export default defineMessages({
  searchInputPlaceholder: 'Search',
  articles: 'Articles',
})
