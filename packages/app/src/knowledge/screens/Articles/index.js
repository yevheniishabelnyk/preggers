/**
 *
 * Articles
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
// import { scrollHandler } from '@/knowledge/components/organisms/TabBar'

import KnowledgeItemsSearch from '@/algolia-search/preggers-knowledge-items'
import * as KnowledgeItemTypes from '@/app/constants/KnowledgeItemTypes'

import messages from './messages'

import styles from './styles'

export default class Articles extends BaseScreen {
  static propTypes = {
    screenProps: PropTypes.shape({
      locale: PropTypes.string.isRequired,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { locale } = this.props.screenProps
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
      >
        <KnowledgeItemsSearch
          locale={locale}
          onItemPress={this._itemPressed}
          searchPlaceholder={formatMessage(messages.searchInputPlaceholder)}
          refinementList={{ type: [KnowledgeItemTypes.ARTICLE] }}
          showSearchField
        />
      </ViewWrapper>
    )
  }

  _itemPressed = item => {
    this._goTo('/knowledge-article', { articleId: item.id })
  }
}
