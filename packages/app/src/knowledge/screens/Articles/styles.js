import { StyleSheet, Platform } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    alignItems: 'stretch',
    paddingTop: Platform.OS === 'ios' ? moderateScale(70, 0.7) : null,
    paddingBottom: moderateScale(70),
    ...ifIphoneX({
      paddingTop: moderateScale(45),
    }),
  },
})

export default styles
