import { defineMessages } from 'react-intl'

export default defineMessages({
  emptyContentText: 'No videos here yet, check back soon again!',
})
