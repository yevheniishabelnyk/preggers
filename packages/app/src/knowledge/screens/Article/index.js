/**
 *
 * Article
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import ContentServiceAdvertisement from '@/ads/components/molecules/ContentServiceAdvertisement'
// import RedButton from '@/knowledge/components/atoms/RedButton'
import Tag from '@/knowledge/components/atoms/Tag'
import FavoritesButton from '@/knowledge/components/atoms/FavoritesButton'
import { ExpoImage } from '@/vendor/cache-manager'
import ActionPopup from '@/shared/components/ActionPopup'

import * as RouterActions from '@/app/redux/router/actions'
import * as ProfileActions from '@/app/redux/profile/actions'

import { shuffle } from 'lodash'

import HTML from 'react-native-render-html'

import { firebaseConnect } from 'react-redux-firebase'
import {
  getArticle,
  getArticleLikesByUser,
  getTagNamesByIds,
  getImageAdsByTags,
  getPregnancy,
} from '@/app/redux/selectors'
import * as AdActions from '@/app/redux/ads/actions'
import { AD_TYPE_IMAGE_AD } from '@/app/redux/ads/constants'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles, { tagsStyles, imageMaxWidth } from './styles'

@firebaseConnect(props => {
  const listeners = ['content/tags']

  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    listeners.push(`articleLikesByUser/${profile.id}`)
  }

  const articleId = props.navigation.getParam('articleId')

  if (articleId) {
    listeners.push(`content/articles/${articleId}`)
  }

  return listeners
})
@connect((state, props) => {
  const articleId = props.navigation.getParam('articleId')

  if (articleId) {
    const { profile } = props.screenProps

    const article = getArticle(state, articleId)

    let isFavorite = false

    if (!profile.isEmpty) {
      const userFavoriteArticles = getArticleLikesByUser(state, profile.id)

      isFavorite =
        (userFavoriteArticles && userFavoriteArticles[articleId]) || false
    }

    let articleTags

    let ads = []

    if (article && article.tags) {
      articleTags = getTagNamesByIds(
        state,
        Object.keys(article.tags),
        article.language
      )

      ads = getImageAdsByTags(state, Object.keys(article.tags))
    }

    return {
      isFetching: article === undefined,
      article,
      isFavorite,
      articleTags,
      userPregnancy: getPregnancy(state, profile.pregnancy),
      ads,
    }
  }

  return {
    isFetching: false,
    isFavorite: false,
  }
})
export default class Article extends BaseScreen {
  static propTypes = {
    article: PropTypes.object,
    articleTags: PropTypes.array,
    isFavorite: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    scrollToTop: false,
    ad: false,
  }

  render() {
    const { article, isFavorite, articleTags } = this.props

    const { ad } = this.state

    const { formatMessage } = this.context.intl

    return (
      <View style={styles.wrapper}>
        <GoBackButton
          onPress={this._customGoBack}
          style={styles.stickyBackButton}
        />

        {article ? (
          <FavoritesButton
            style={styles.shaddowButton}
            onPress={this._addToFavoriteButtonPressed}
            isFavorite={isFavorite}
          />
        ) : null}

        <ViewWrapper
          scrollable
          mainStyle={styles.container}
          contentContainerStyle={styles.contentContainerStyle}
          deviceIndicators="rgb(249,250,252)"
          scrollToTop={this.state.scrollToTop}
          components={{
            popups: [
              <ActionPopup
                key="useOurFunctionsDialog"
                ref={ref => (this._useOurFunctionsDialog = ref)}
                image={require('assets/img/create-account-popup.png')}
                largeTitle={formatMessage(messages.actionPopupTitle)}
                text={formatMessage(messages.actionPopupText)}
                actionButton={{
                  title: formatMessage(messages.actionPopupActionButton),
                  onPress: this._goToLoginPage,
                }}
                okButton={{
                  title: formatMessage(messages.actionPopupOkButton),
                  onPress: this._dismissActionDialog,
                }}
              />,
            ],
          }}
        >
          {article ? (
            <View>
              <View style={styles.mainPosterWrap}>
                {article.image ? (
                  <ExpoImage
                    source={article.image}
                    style={styles.mainPoster}
                    cacheImage
                  />
                ) : null}

                {article.copyright ? (
                  <Text style={styles.photoSign} selectable>
                    {formatMessage(messages.photo)} {article.copyright}
                  </Text>
                ) : null}
              </View>

              <View style={styles.content}>
                {article.sponsoredBy ? (
                  <Text style={styles.sponsoredText} selectable>
                    {article.sponsoredBy}
                  </Text>
                ) : null}

                {article.title ? (
                  <Text style={styles.title} selectable>
                    {article.title}
                  </Text>
                ) : null}

                {article.ingress ? (
                  <Text style={styles.ingressText} selectable>
                    {article.ingress}
                  </Text>
                ) : null}

                {article.text ? (
                  <HTML
                    html={article.text.replace(
                      new RegExp('<li[^>]*>([\\s\\S]*?)<\\/li>', 'g'),
                      '<li>$1</li>'
                    )}
                    // eslint-disable-next-line react/jsx-no-bind
                    customWrapper={RNContent => (
                      <View style={styles.contentConteiner}>{RNContent}</View>
                    )}
                    onLinkPress={this._linkPressed}
                    tagsStyles={tagsStyles}
                    textSelectable
                    imagesMaxWidth={imageMaxWidth}
                    renderers={{
                      ol: (
                        htmlAttribs,
                        children,
                        convertedCSSStyles,
                        passProps
                      ) => {
                        const content = React.Children.map(
                          children,
                          (child, index) => {
                            return (
                              <View style={styles.olLiStyle}>
                                <Text style={styles.olLiPrefix}>
                                  {index + 1}.
                                </Text>
                                {child}
                              </View>
                            )
                          }
                        )

                        return (
                          <View key={passProps.key} style={styles.listWrapper}>
                            {content}
                          </View>
                        )
                      },

                      ul: (
                        htmlAttribs,
                        children,
                        convertedCSSStyles,
                        passProps
                      ) => {
                        const content = React.Children.map(children, child => {
                          return (
                            <View style={styles.ulLiStyle}>
                              <View style={styles.ulLiPrefix} />
                              {child}
                            </View>
                          )
                        })

                        return (
                          <View key={passProps.key} style={styles.listWrapper}>
                            {content}
                          </View>
                        )
                      },

                      slot: (
                        htmlAttribs,
                        children,
                        convertedCSSStyles,
                        passProps
                      ) =>
                        ad ? (
                          <ContentServiceAdvertisement
                            key={passProps.key}
                            ad={ad}
                            dispatch={this._}
                          />
                        ) : null,
                    }}
                  />
                ) : null}

                <View style={styles.writtenByWrap}>
                  {article.writtenBy ? (
                    <Text style={styles.writtenByText} selectable>
                      {formatMessage(messages.writtenBy)} {article.writtenBy}
                    </Text>
                  ) : null}
                </View>

                {articleTags ? (
                  <View style={styles.tagsWrap}>
                    <FormattedMessage
                      style={styles.tagsTitle}
                      {...messages.tags}
                    />

                    <View style={styles.tagsRow}>
                      {articleTags.map(tag => (
                        <Tag
                          key={tag.id}
                          tag={tag}
                          onPress={this._articleTagPressed}
                        />
                      ))}
                    </View>
                  </View>
                ) : null}
              </View>

              {/*<RedButton
              title={formatMessage(messages.markViewedButtonTitle)}
              onPress={this._markViewedButtonPressed}
            />*/}
            </View>
          ) : null}
        </ViewWrapper>
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.article &&
      nextProps.article &&
      this.props.article.id !== nextProps.article.id
    ) {
      this.setState({ scrollToTop: true })
    } else {
      this.setState({ scrollToTop: false })
    }

    if (nextProps.ads !== undefined) {
      const prevUids = this.props.ads.map(ad => ad.uid)

      const nextUids = nextProps.ads.map(ad => ad.uid)

      const difference = nextUids.filter(uid => !prevUids.includes(uid))

      if (difference.length) {
        this.setState({ ad: shuffle(nextProps.ads)[0] })
      }
    }
  }

  componentDidMount() {
    this._fetchAds()

    const { ads } = this.props

    if (ads.length) {
      this.setState({ ad: shuffle(ads)[0] })
    }
  }

  _customGoBack = () => {
    this._(ProfileActions.maybeAskUserToRateApp())

    this._goBack()
  }

  _markViewedButtonPressed = () => {
    console.log('_markViewedButtonPressed')
  }

  _articleTagPressed = tag => {
    this.props.navigation.navigate('/knowledge-search', { tags: [tag.label] })
  }

  _linkPressed = (evt, href) => {
    if (href.indexOf('articleId') > -1) {
      const articleId = href.split('=')[1]

      this.props.navigation.push('/knowledge-article', { articleId })
    } else {
      this._(RouterActions.handleLink(href))
    }
  }

  _addToFavoriteButtonPressed = async () => {
    const { profile } = this.props.screenProps

    if (!profile.isEmpty) {
      const { firebase, article, isFavorite } = this.props

      const articleStarsRef = firebase
        .database()
        .ref(`/content/articles/${article.id}/stars`)

      try {
        if (!isFavorite) {
          await Promise.all([
            firebase.set(
              `articleLikesByUser/${profile.id}/${article.id}`,
              true
            ),
            firebase.set(
              `userLikesByArticle/${article.id}/${profile.id}`,
              true
            ),

            articleStarsRef.transaction(function(currentRank) {
              return currentRank + 1
            }),
          ])
        } else {
          await Promise.all([
            firebase.set(
              `articleLikesByUser/${profile.id}/${article.id}`,
              null
            ),
            firebase.set(
              `userLikesByArticle/${article.id}/${profile.id}`,
              null
            ),

            articleStarsRef.transaction(function(currentRank) {
              return currentRank - 1
            }),
          ])
        }
      } catch (err) {
        console.log(err)
      }
    } else {
      this._useOurFunctionsDialog._show()
    }
  }

  _fetchAds = async () => {
    const { profile, locale } = this.props.screenProps

    const { deviceCountryCode, userPregnancy, article } = this.props

    const pregnancies = userPregnancy ? [userPregnancy.dueDate] : []

    const articleTags = article && article.tags ? Object.keys(article.tags) : []

    const requestBody = {
      types: [AD_TYPE_IMAGE_AD],
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      pregnancies,
      tagIds: articleTags,
    }

    try {
      await this._(AdActions.fetchAdsAndReplaceTypeState(requestBody))
    } catch (err) {
      console.log(err)
    }
  }

  _goToLoginPage = () => this._(RouterActions.resetToUrl(['/auth-login']))

  _dismissActionDialog = () => {
    this._useOurFunctionsDialog._hide()
  }
}
