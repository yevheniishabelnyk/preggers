import { StyleSheet, Dimensions } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')

const resizeFactor = 0.3

const ulLiPrefixWidth = moderateScale(4, resizeFactor)

const gutter = moderateScale(22, 4)

export const imageMaxWidth = width - 2 * gutter

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },

  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  stickyBackButton: {
    ...ifIphoneX(
      {
        top: moderateScale(65, resizeFactor),
      },
      {
        top: moderateScale(45, resizeFactor),
      }
    ),
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowColor: 'black',
    shadowOffset: { height: 0, width: 0 },
  },

  contentContainerStyle: {
    backgroundColor: 'rgb(249,250,252)',
    paddingBottom: moderateScale(35, resizeFactor),
  },

  header: {
    backgroundColor: 'rgb(249,250,252)',
  },

  mainPoster: {
    width: '100%',
    height: moderateScale(249, resizeFactor),
  },

  photoSign: {
    fontSize: moderateScale(12, resizeFactor),
    fontFamily: 'Roboto-Italic',
    textAlign: 'right',
    fontStyle: 'italic',
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(187, 187, 187)',
    paddingRight: moderateScale(9, resizeFactor),
  },

  sponsoredText: {
    fontSize: moderateScale(15, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    fontFamily: 'Roboto-Regular',
    color: 'rgb(152, 150, 150)',
  },

  content: {
    paddingHorizontal: gutter,
  },

  title: {
    fontSize: moderateScale(28, resizeFactor),
    lineHeight: moderateScale(38, resizeFactor),
    fontFamily: 'Now-Black',
    color: 'rgb(35, 40, 46)',
    marginBottom: moderateScale(23, resizeFactor),
  },

  ingressText: {
    fontSize: moderateScale(20, resizeFactor),
    lineHeight: moderateScale(26, resizeFactor),
    fontFamily: 'Roboto-Medium',
    color: 'rgb(42, 44, 46)',
    marginBottom: moderateScale(35, resizeFactor),
  },

  subText: {
    marginTop: moderateScale(11, resizeFactor),
  },

  writtenByWrap: {
    marginTop: moderateScale(25, resizeFactor),
    borderTopWidth: 1,
    borderColor: 'rgb(152, 150, 150)',
  },

  writtenByText: {
    fontSize: moderateScale(15, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    fontFamily: 'Roboto-Regular',
    color: 'rgb(152, 150, 150)',
    paddingTop: moderateScale(8, resizeFactor),
  },

  tagsWrap: {
    marginTop: moderateScale(8, resizeFactor),
    marginBottom: moderateScale(60, resizeFactor),
  },

  tagsTitle: {
    fontSize: moderateScale(16, resizeFactor),
    lineHeight: moderateScale(25, resizeFactor),
    fontFamily: 'Now-Bold',
    color: 'rgb(35, 40, 46)',
  },

  tagsRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  olStyle: {
    marginTop: 0,
  },

  olLiStyle: {
    flexDirection: 'row',
    paddingRight: moderateScale(20, resizeFactor),
    marginBottom: moderateScale(-20, resizeFactor),
  },

  ulLiStyle: {
    flexDirection: 'row',
    paddingRight: moderateScale(20, resizeFactor),
    marginBottom: moderateScale(-20, resizeFactor),
  },

  olLiPrefix: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(18, resizeFactor),
    color: 'rgb(59,72,89)',
    alignSelf: 'flex-start',
    marginTop: moderateScale(2, resizeFactor),
  },

  ulLiPrefix: {
    marginVertical: moderateScale(11, resizeFactor),
    marginRight: moderateScale(8, resizeFactor),
    width: ulLiPrefixWidth,
    height: ulLiPrefixWidth,
    borderRadius: ulLiPrefixWidth / 2,
    backgroundColor: 'rgb(59,72,89)',
  },

  listWrapper: {
    marginTop: moderateScale(11, resizeFactor),
    marginBottom: moderateScale(11, resizeFactor),
  },
})

export const tagsStyles = {
  p: {
    fontSize: moderateScale(18, resizeFactor),
    lineHeight: moderateScale(25, resizeFactor),
    fontFamily: 'Roboto-Light',
    color: 'rgb(59, 70, 85)',
    marginBottom: moderateScale(25, resizeFactor),
  },
  strong: {
    fontSize: moderateScale(18, resizeFactor),
    lineHeight: moderateScale(25, resizeFactor),
    fontFamily: 'Roboto-Bold',
    color: 'rgb(59, 70, 85)',
  },
  em: {
    fontSize: moderateScale(18, resizeFactor),
    lineHeight: moderateScale(25, resizeFactor),
    fontFamily: 'Roboto-LightItalic',
    color: 'rgb(59, 70, 85)',
  },
  h1: {
    fontSize: moderateScale(28, resizeFactor),
    lineHeight: moderateScale(38, resizeFactor),
    fontFamily: 'Now-Black',
    color: 'rgb(35, 40, 46)',
  },
  h2: {
    fontSize: moderateScale(25, resizeFactor),
    lineHeight: moderateScale(38, resizeFactor),
    fontFamily: 'Now-Black',
    color: 'rgb(35, 40, 46)',
  },
}

export default styles
