import { defineMessages } from 'react-intl'

export default defineMessages({
  markViewedButtonTitle: 'Mark as viewed',
  tags: 'Tags',
  writtenBy: 'Written by',
  photo: 'Photo:',
  actionPopupTitle:
    'Want to use all our functions? Create an account, it’s free!',
  actionPopupText:
    'In order to use all the functions in our app you need to create an account. Just click the link below',
  actionPopupActionButton: 'Of course I want to!',
  actionPopupOkButton: 'No thanks',
})
