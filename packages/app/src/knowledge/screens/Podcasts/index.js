/**
 *
 * Podcasts
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { ScrollView } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import EmptyContent from '@/knowledge/components/molecules/EmptyContent'
import { scrollHandler } from '@/knowledge/components/organisms/TabBar'

import messages from './messages'

import styles from './styles'

export default class Podcasts extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        onScroll={scrollHandler}
        scrollEventThrottle={16}
      >
        <EmptyContent title={formatMessage(messages.emptyContentText)} />
      </ScrollView>
    )
  }
}
