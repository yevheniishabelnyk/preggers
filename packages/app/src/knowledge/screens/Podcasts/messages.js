import { defineMessages } from 'react-intl'

export default defineMessages({
  emptyContentText: 'No podcasts here yet, check back soon again!',
})
