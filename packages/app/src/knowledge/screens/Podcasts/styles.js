import { StyleSheet, Dimensions, Platform } from 'react-native'
import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')
const resizeFactor = 0.3
const gutter = moderateScale(23, resizeFactor)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainerStyle: {
    paddingBottom: moderateScale(85, resizeFactor),
    paddingHorizontal: gutter,
    paddingTop:
      Platform.OS === 'android'
        ? moderateScale(25, resizeFactor)
        : moderateScale(111, resizeFactor),
  },

  header: {
    paddingHorizontal: 0.06 * width,
    backgroundColor: 'rgb(249,250,252)',
  },

  content: {
    width: moderateScale(336, resizeFactor),
  },

  search: {
    width: moderateScale(336, resizeFactor),
    marginTop: moderateScale(15, resizeFactor),
  },

  subItems: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
})

export default styles
