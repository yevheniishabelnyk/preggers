/**
 *
 * MyWeek
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, ActivityIndicator, Animated, Platform } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import WeekBar from '@/start-view/components/molecules/WeekBar'
import KnowledgeBankWeekSlide from '@/knowledge/components/organisms/KnowledgeBankWeekSlide'
import { scrollHandler, scrollY } from '@/knowledge/components/organisms/TabBar'

import algoliasearch from 'algoliasearch'

import Swiper from '@/vendor/react-native-swiper'

import {
  getWeeks,
  getTags,
  getPregnancy,
  getArticleLikesByUser,
} from '@/app/redux/selectors'

import { range, difference, get, keys } from 'lodash'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.7

import { firebaseConnect } from 'react-redux-firebase'

import styles from './styles'

import config, { MIN_PREGNANCY_WEEK, MAX_PREGNANCY_WEEK } from 'config'

const envSuffix =
  config.ENVIRONMENT === 'production' ? '' : `__${config.ENVIRONMENT}`

const HEADER_MAX_HEIGHT = moderateScale(90, resizeFactor)
const HEADER_MIN_HEIGHT = 0
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT

const weekNumbers = range(MIN_PREGNANCY_WEEK, MAX_PREGNANCY_WEEK + 1)

import Pregnancy from '@/app/database/models/Pregnancy'

const getPregnancyProgress = Pregnancy.makeGetProgress()

@firebaseConnect(props => {
  const { profile } = props.screenProps

  const listeners = ['content/tags']

  if (!profile.isEmpty) {
    listeners.push(`articleLikesByUser/${profile.id}`)
  }

  return listeners
})
@connect((state, props) => {
  const { profile } = props.screenProps
  const pregnancy = getPregnancy(state, profile.pregnancy)
  const weeks = getWeeks(state)
  const tags = getTags(state)
  const userFavoriteArticles = getArticleLikesByUser(state, profile.id)

  const isAccessedFromDeeplink = Boolean(props.navigation.getParam('deeplink'))

  let pregnancyCurrentWeekNumber

  if (!profile.isEmpty && !profile.pregnancy) {
    pregnancyCurrentWeekNumber = 3
  }

  if (pregnancy) {
    const pregnancyProgress = getPregnancyProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    pregnancyCurrentWeekNumber = pregnancyProgress.pregnancyWeek
  }

  if (isAccessedFromDeeplink) {
    pregnancyCurrentWeekNumber = Number(props.navigation.getParam('weekNumber'))
  }

  return {
    pregnancy,
    pregnancyCurrentWeekNumber,
    weeks,
    tags,
    userFavoriteArticles,
  }
})
export default class MyWeek extends BaseScreen {
  static propTypes = {
    tags: PropTypes.object,
    userFavoriteArticles: PropTypes.object,
  }

  constructor(props) {
    super(props)

    const activeWeekNumber = Number(props.navigation.getParam('weekNumber'))

    const { pregnancyCurrentWeekNumber } = props

    if (!activeWeekNumber) {
      if (pregnancyCurrentWeekNumber) {
        props.navigation.setParams({ weekNumber: pregnancyCurrentWeekNumber })

        this.weekBarInitialWeek = pregnancyCurrentWeekNumber
      } else {
        this.weekBarInitialWeek = MIN_PREGNANCY_WEEK + 1
      }
    } else {
      this.weekBarInitialWeek = activeWeekNumber
    }

    this.algoliaClient = algoliasearch(
      config.ALGOLIA__APP_ID,
      config.ALGOLIA__API_KEY
    )

    const indexId = `preggers-knowledge-items--${
      props.screenProps.locale
    }${envSuffix}`

    this.state = {
      swiperScrollEnabled: false,
      algoliaIndex: this.algoliaClient.initIndex(indexId),
      favoriteArticles: [],
    }
  }

  render() {
    const { locale } = this.props.screenProps
    const { weeks = {}, profile, pregnancy } = this.props
    const { swiperScrollEnabled, favoriteArticles } = this.state

    let header = null

    if (swiperScrollEnabled) {
      header = (
        <WeekBar
          ref={ref => (this._weekBar = ref)}
          minWeek={MIN_PREGNANCY_WEEK}
          maxWeek={MAX_PREGNANCY_WEEK}
          onWeekChange={this._onWeekChange}
          initialWeek={this.weekBarInitialWeek}
        />
      )
    }

    const headerHeight = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    })

    const androidHeaderHeight = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MIN_HEIGHT, -HEADER_MAX_HEIGHT],
      extrapolate: 'clamp',
    })

    return (
      <View style={styles.container}>
        <Animated.View
          style={[
            styles.headerContainer,
            {
              top:
                Platform.OS === 'android' ? androidHeaderHeight : headerHeight,
            },
          ]}
        >
          {header}
        </Animated.View>

        {swiperScrollEnabled ? (
          <Swiper
            ref={ref => (this._swiper = ref)}
            style={styles.swiper}
            loop={false}
            scrollEnabled={this.state.swiperScrollEnabled}
            showsPagination={false}
            index={this.swiperInitialIndex}
            onIndexChanged={this._swiperIndexChanged}
            loadMinimal
            loadMinimalSize={1}
            loadMinimalLoader={
              <View style={styles.loaderWrapper}>
                <ActivityIndicator
                  animating
                  size="large"
                  color="#f9fafc"
                  style={styles.loader}
                />
              </View>
            }
          >
            {weekNumbers.map(number => (
              <KnowledgeBankWeekSlide
                key={number}
                weekNumber={number}
                week={weeks[number]}
                weekTags={this._getWeekTags(weeks[number])}
                algoliaIndex={this.state.algoliaIndex}
                favoriteArticles={favoriteArticles}
                goTo={this._goTo}
                profile={profile}
                locale={locale}
                onBackToCurrentWeek={this._backToCurrentPregnancyWeek}
                pregnancyCurrentWeekNumber={
                  this.props.pregnancyCurrentWeekNumber
                }
                pregnancy={pregnancy}
                disableParrentScroll={this._disableSwiperScroll}
                enableParrentScroll={this._enableSwiperScroll}
                onScroll={scrollHandler}
                scrollEventThrottle={16}
              />
            ))}
          </Swiper>
        ) : null}
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.pregnancyCurrentWeekNumber !==
      nextProps.pregnancyCurrentWeekNumber
    ) {
      setTimeout(() => {
        if (this._swiper) {
          const diff =
            nextProps.pregnancyCurrentWeekNumber -
            (this.currentSwiperIndex + MIN_PREGNANCY_WEEK)

          this.currentSwiperIndex = this.currentSwiperIndex + diff

          this._swiper.scrollBy(diff)
        }
      }, 0)
    }

    if (
      this.props.navigation.getParam('weekNumber') !==
      nextProps.navigation.getParam('weekNumber')
    ) {
      const nextActiveWeekNumber = nextProps.navigation.getParam('weekNumber')

      if (nextActiveWeekNumber) {
        setTimeout(() => {
          const diff =
            nextActiveWeekNumber -
            (this.currentSwiperIndex + MIN_PREGNANCY_WEEK)

          this.currentSwiperIndex = this.currentSwiperIndex + diff

          this._swiper && this._swiper.scrollBy(diff)

          setTimeout(() => {
            this.props.navigation.setParams({ weekNumber: null })
          }, 0)
        }, 0)
      }
    }

    if (this.props.screenProps.locale !== nextProps.screenProps.locale) {
      const indexId = `preggers-knowledge-items--${
        nextProps.screenProps.locale
      }${envSuffix}`

      this.setState(
        {
          algoliaIndex: this.algoliaClient.initIndex(indexId),
        },
        () => this._queryArticles(this.props.userFavoriteArticles)
      )
    }

    if (this.props.userFavoriteArticles !== nextProps.userFavoriteArticles) {
      this._queryArticles(
        nextProps.userFavoriteArticles,
        this.props.userFavoriteArticles
      )
    }

    if (
      this.props.screenProps.profile.isEmpty &&
      !nextProps.screenProps.profile.isEmpty &&
      !nextProps.screenProps.profile.pregnancy
    ) {
      setTimeout(() => {
        this._initSwiper()
      }, 0)
    }
  }

  componentDidMount() {
    const { pregnancy, userFavoriteArticleIds } = this.props
    const { profile } = this.props.screenProps
    const timeoutDelay = Platform.OS === 'android' ? 400 : 0

    if (pregnancy) {
      setTimeout(() => {
        this._initSwiper()
      }, timeoutDelay)
    }

    if (!profile.isEmpty && !profile.pregnancy) {
      setTimeout(() => {
        this._initSwiper()
      }, timeoutDelay)
    }

    this._queryArticles(userFavoriteArticleIds)
  }

  componentWillUnmount() {
    this._unWatchActiveWeeks()
  }

  _queryArticles = articlesMap => {
    const ids = keys(articlesMap)

    if (ids.length) {
      this.state.algoliaIndex.getObjects(ids).then(response => {
        if (response.results && response.results.length) {
          this.setState({ favoriteArticles: response.results })
        }
      })
    } else {
      this.setState({ favoriteArticles: [] })
    }
  }

  _getWeekTags = week => {
    if (week && week.tags) {
      const { tags } = this.props

      if (tags === undefined) {
        return undefined
      }

      if (tags === null) {
        return null
      }

      const { locale } = this.props.screenProps

      const weekTags = []

      Object.keys(week.tags).forEach(tagId => {
        const tagName = get(tags, `${tagId}.name.${locale}`)
        if (tagName) {
          weekTags.push(tagName)
        }
      })

      if (weekTags.length) {
        return weekTags
      }

      return null
    }

    if (week && !week.tags) {
      return null
    }
  }

  _initSwiper = () => {
    const { pregnancyCurrentWeekNumber } = this.props

    let activeWeekNumber = pregnancyCurrentWeekNumber || MIN_PREGNANCY_WEEK + 1

    if (activeWeekNumber < MIN_PREGNANCY_WEEK) {
      activeWeekNumber = MIN_PREGNANCY_WEEK
    } else if (activeWeekNumber > MAX_PREGNANCY_WEEK) {
      activeWeekNumber = MAX_PREGNANCY_WEEK
    }

    this.swiperInitialIndex =
      activeWeekNumber - MIN_PREGNANCY_WEEK > 0
        ? activeWeekNumber - MIN_PREGNANCY_WEEK
        : 0

    this.currentSwiperIndex = this.swiperInitialIndex

    this.setState({ swiperScrollEnabled: true })

    if (activeWeekNumber !== this.weekBarInitialWeek) {
      setTimeout(() => {
        if (this._weekBar) {
          this._weekBar.setWeek(activeWeekNumber)
        }
      }, 0)
    }
  }

  _onWeekChange = async weekNumber => {
    if (!this.isSwipingContent) {
      this.isSwipingWeekBar = true

      const diff = weekNumber - (this.currentSwiperIndex + MIN_PREGNANCY_WEEK)

      this.currentSwiperIndex = this.currentSwiperIndex + diff

      const currentActiveWeekNumber = this.state.activeWeekNumber

      this.setState({ activeWeekNumber: weekNumber })

      this._swiper && this._swiper.scrollBy(diff)

      setTimeout(() => {
        this._watchActiveWeeks(weekNumber, currentActiveWeekNumber)
      }, 0)
    } else {
      this.isSwipingContent = false
    }
  }

  _swiperIndexChanged = index => {
    if (!this.isSwipingWeekBar) {
      this.isSwipingContent = true

      this.currentSwiperIndex = index

      const currentActiveWeekNumber = this.state.activeWeekNumber
      const nextActiveWeekNumber = index + MIN_PREGNANCY_WEEK

      this.setState({ activeWeekNumber: nextActiveWeekNumber })

      if (this._weekBar) {
        this._weekBar.setWeek(nextActiveWeekNumber)
      }

      this.isSwipingContent = false

      setTimeout(() => {
        this._watchActiveWeeks(nextActiveWeekNumber, currentActiveWeekNumber)
      }, 0)
    } else {
      this.isSwipingWeekBar = false
    }
  }

  _disableSwiperScroll = () => this.setState({ swiperScrollEnabled: false })

  _enableSwiperScroll = () => this.setState({ swiperScrollEnabled: true })

  _watchActiveWeeks = (nextActive, currentActive) => {
    const { firebase } = this.props

    const currentActiveWeeks = []
    const nextActiveWeeks = [nextActive]

    if (currentActive) {
      currentActiveWeeks.push(currentActive)
      if (currentActive !== MIN_PREGNANCY_WEEK) {
        currentActiveWeeks.push(currentActive - 1)
      }
      if (currentActive !== MAX_PREGNANCY_WEEK) {
        currentActiveWeeks.push(currentActive + 1)
      }
    }

    if (nextActive !== MIN_PREGNANCY_WEEK) {
      nextActiveWeeks.push(nextActive - 1)
    }
    if (nextActive !== MAX_PREGNANCY_WEEK) {
      nextActiveWeeks.push(nextActive + 1)
    }

    // console.info('currentActiveWeeks: ', currentActiveWeeks)
    // console.info('nextActiveWeeks: ', nextActiveWeeks)

    const newWatchedWeeks = difference(nextActiveWeeks, currentActiveWeeks)

    // console.info('newWatchedWeeks: ', newWatchedWeeks)

    newWatchedWeeks.forEach(weekNumber =>
      firebase.watchEvent('value', `content/weeks/${weekNumber}`)
    )

    if (currentActive) {
      const unWatchedWeeks = difference(currentActiveWeeks, nextActiveWeeks)

      // console.info('unWatchedWeeks: ', unWatchedWeeks)

      unWatchedWeeks.forEach(weekNumber =>
        firebase.unWatchEvent('value', `content/weeks/${weekNumber}`)
      )
    }
  }

  _unWatchActiveWeeks = () => {
    const { activeWeekNumber } = this.state

    if (activeWeekNumber) {
      const { firebase } = this.props

      firebase.unWatchEvent('value', `content/weeks/${activeWeekNumber}`)

      if (activeWeekNumber !== MIN_PREGNANCY_WEEK) {
        firebase.unWatchEvent('value', `content/weeks/${activeWeekNumber - 1}`)
      }

      if (activeWeekNumber !== MAX_PREGNANCY_WEEK) {
        firebase.unWatchEvent('value', `content/weeks/${activeWeekNumber + 1}`)
      }
    }
  }
}
