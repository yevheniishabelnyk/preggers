import { defineMessages } from 'react-intl'

export default defineMessages({
  favoritesTitle: 'My Favorites',
  emptyFavoritesText:
    'No favorites so far, click the star icon on an episode or article to save it here',
  emptyArticlesText: 'No article here yet, check back soon again!',
  loadMoreButtonTitle: 'Load more',
})
