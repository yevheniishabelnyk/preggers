import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  headerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 5,
    backgroundColor: 'rgb(249, 250, 252)',
    height: moderateScale(90, resizeFactor),
    paddingTop: moderateScale(10, resizeFactor),
  },

  contentContainerStyle: {
    paddingBottom: moderateScale(85, 0.3),
    alignItems: 'center',
    paddingTop: moderateScale(85, 0.3),
  },

  // loaderWrapper: {
  //   // height: 430 / 375 * width,
  //   width,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   // backgroundColor: 'blue',
  // },

  loader: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  swiper: {
    overflow: 'visible',
  },
})

export default styles
