import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: moderateScale(10, resizeFactor),
  },

  image: {
    width: moderateScale(200, resizeFactor),
    height: moderateScale(226, resizeFactor),
  },

  text: {
    width: moderateScale(243, resizeFactor),
    fontSize: moderateScale(18, resizeFactor),
    fontFamily: 'Now-Medium',
    lineHeight: moderateScale(29, resizeFactor),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    marginTop: moderateScale(34, resizeFactor),
  },
})

export default styles
