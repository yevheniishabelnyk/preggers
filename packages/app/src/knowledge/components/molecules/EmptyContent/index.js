/**
 *
 * Empty Content component
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, View, Image } from 'react-native'

import styles from './styles'

export default class EmptyContent extends React.Component {
  static propTypes = {
    title: PropTypes.string,
  }

  render() {
    const { title } = this.props

    return (
      <View style={styles.container}>
        <Image
          source={require('assets/img/empty-knowledge.png')}
          style={styles.image}
        />

        <Text style={styles.text}>{title}</Text>
      </View>
    )
  }
}
