import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.4

const styles = StyleSheet.create({
  subItem: {
    width: moderateScale(158, 0.65),
    backgroundColor: 'white',
    borderRadius: moderateScale(3, resizeFactor),
    shadowOpacity: 0.15,
    shadowRadius: 10,
    shadowColor: '#111723',
    shadowOffset: { height: 8, width: -2 },
  },

  subItemImage: {
    width: moderateScale(158, 0.65),
    height: moderateScale(85, 0.65),
  },

  subItemIcon: {
    position: 'absolute',
    bottom: moderateScale(-12.5, resizeFactor),
    right: moderateScale(10, resizeFactor),
    width: moderateScale(25, resizeFactor),
    height: moderateScale(25, resizeFactor),
  },

  subItemDescription: {
    marginTop: moderateScale(5, resizeFactor),
    paddingHorizontal: moderateScale(13, resizeFactor),
    paddingBottom: moderateScale(17, resizeFactor),
  },

  subItemTitle: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(16, resizeFactor),
    lineHeight: moderateScale(20, resizeFactor),
    color: 'rgb(41, 37, 37)',
    marginTop: moderateScale(10, resizeFactor),
    height: moderateScale(60, resizeFactor),
  },
})

export default styles
