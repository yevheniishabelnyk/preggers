/**
 *
 * Sub Item component
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, View, Image, TouchableOpacity } from 'react-native'
import { ExpoImage } from '@/vendor/cache-manager'

import { isFunction } from 'lodash'

import styles from './styles'

export default class SubItem extends React.Component {
  static propTypes = {
    article: PropTypes.object,
    onPress: PropTypes.func,
  }

  render() {
    const { article } = this.props

    return (
      <TouchableOpacity onPress={this._itemPressed} activeOpacity={0.95}>
        <View style={styles.subItem}>
          <View style={styles.subItemPoster}>
            {article.image ? (
              <ExpoImage
                source={article.image}
                style={styles.subItemImage}
                cacheImage
              />
            ) : null}

            <Image
              source={require('assets/icons/article-active.png')}
              style={styles.subItemIcon}
            />
          </View>

          <View style={styles.subItemDescription}>
            <Text style={styles.subItemTitle} numberOfLines={3} selectable>
              {article.title ? article.title : null}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _itemPressed = () => {
    const { article, onPress } = this.props

    if (isFunction(onPress)) {
      onPress(article.id)
    }
  }
}
