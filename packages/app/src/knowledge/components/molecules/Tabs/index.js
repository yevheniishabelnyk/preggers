/**
 *
 * Tabs
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'

import styles from './styles'

const Tabs = ({ children }) => <View style={styles.container}>{children}</View>

Tabs.propTypes = {
  children: PropTypes.any,
}

export default Tabs
