import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    width: moderateScale(336, 0.7),
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    height: moderateScale(85, resizeFactor),
    borderRadius: moderateScale(8, resizeFactor),
  },
})

export default styles
