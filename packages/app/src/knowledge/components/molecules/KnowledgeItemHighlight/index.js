/**
 *
 * KnowledgeItemHighlight
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableOpacity } from 'react-native'
import { ExpoImage } from '@/vendor/cache-manager'

import { isFunction } from 'lodash'

import styles from './styles'

export default class KnowledgeItemHighlight extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    article: PropTypes.object,
    large: PropTypes.bool,
  }

  render() {
    const { article, large } = this.props

    return (
      <TouchableOpacity
        onPress={this._itemPressed}
        activeOpacity={0.8}
        style={[styles.container, large ? styles.largeContainer : null]}
      >
        <View style={styles.content}>
          {article.image ? (
            <ExpoImage source={article.image} style={styles.image} cacheImage />
          ) : null}

          <View style={styles.titleContainer}>
            <Image
              source={require('assets/icons/article-active.png')}
              style={[styles.icon, large ? styles.largeIcon : null]}
            />

            <Text style={[styles.title, large ? styles.largeTitle : null]}>
              {article.title || ''}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _itemPressed = () => {
    const { onPress, article } = this.props

    if (isFunction(onPress)) {
      onPress(article.id)
    }
  }
}
