import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    marginBottom: moderateScale(21),
    backgroundColor: 'white',
    borderRadius: 3,
    shadowOpacity: 0.15,
    shadowRadius: 10,
    shadowColor: '#111723',
    shadowOffset: { height: 8, width: -2 },
    width: '50%',
  },

  largeContainer: {
    width: '100%',
  },

  image: {
    width: '100%',
    height: moderateScale(175, 0.7),
  },

  titleContainer: {
    paddingHorizontal: moderateScale(16),
    paddingTop: moderateScale(22, 0.2),
    paddingBottom: moderateScale(22, 0.2),
    position: 'relative',
  },

  title: {
    alignSelf: 'stretch',
    fontFamily: 'Now-Black',
    fontSize: moderateScale(16, 0.4),
    lineHeight: moderateScale(22, 0.4),
    textAlign: 'left',
    color: 'rgb(54, 57, 83)',
  },

  largeTitle: {
    fontSize: moderateScale(25, 0.2),
    lineHeight: moderateScale(32, 0.2),
  },

  icon: {
    position: 'absolute',
    right: moderateScale(10),
    top: moderateScale(-12.5),
    width: moderateScale(25),
    height: moderateScale(25),
  },

  largeIcon: {
    top: moderateScale(-15),
    width: moderateScale(30),
    height: moderateScale(30),
  },
})

export default styles
