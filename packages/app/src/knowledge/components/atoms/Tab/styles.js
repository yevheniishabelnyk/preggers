import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  menuIcon: {
    width: moderateScale(40, resizeFactor),
    height: moderateScale(40, resizeFactor),
    alignSelf: 'center',
    overflow: 'visible',
  },

  menuText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12, resizeFactor),
    color: 'rgb(172, 186, 206)',
    marginTop: moderateScale(5, resizeFactor),
  },

  menuTextActive: {
    color: 'rgb(35, 40, 46)',
  },
})

export default styles
