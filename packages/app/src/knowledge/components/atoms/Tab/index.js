/**
 *
 * Tab
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, View, Image, TouchableHighlight, Keyboard } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class Tab extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    icon: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }

  render() {
    const { title, icon } = this.props

    return (
      <TouchableHighlight
        style={styles.menuItem}
        underlayColor="rgba(255,255,255,.5)"
        onPress={this._onButtonPressed}
      >
        <View>
          <Image source={icon} style={styles.menuIcon} />

          <Text style={[styles.menuText, styles.menuTextActive]}>{title}</Text>
        </View>
      </TouchableHighlight>
    )
  }

  _onButtonPressed = () => {
    const { onPress } = this.props

    if (isFunction(onPress)) {
      onPress()
    }

    Keyboard.dismiss()
  }
}
