import { StyleSheet } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  button: {
    position: 'absolute',
    width: moderateScale(40, resizeFactor),
    height: moderateScale(40, resizeFactor),
    backgroundColor: 'white',
    borderRadius: moderateScale(31, resizeFactor),
    ...ifIphoneX(
      {
        top: moderateScale(65, resizeFactor),
      },
      {
        top: moderateScale(45, resizeFactor),
      }
    ),
    right: moderateScale(20, resizeFactor),
    zIndex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowColor: 'black',
    shadowOffset: { height: 0, width: 0 },
  },

  icon: {
    width: moderateScale(18, resizeFactor),
    height: moderateScale(25, resizeFactor),
  },
})

export default styles
