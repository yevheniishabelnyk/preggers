/**
 *
 * Favorites button component
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import bookmarkIcon from 'assets/icons/bookmark.png'
import bookmarkActiveIcon from 'assets/icons/bookmark-active.png'

import styles from './styles'

export default class FavoritesButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    isFavorite: PropTypes.bool.isRequired,
  }

  render() {
    const { onPress, isFavorite } = this.props

    return (
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Image
          source={isFavorite ? bookmarkActiveIcon : bookmarkIcon}
          style={styles.icon}
        />
      </TouchableOpacity>
    )
  }
}
