import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  button: {
    width: (298 / 375) * width,
    height: (55 / 375) * width,
    borderRadius: (31 / 375) * width,
    backgroundColor: 'rgb(250, 65, 105)',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  buttonText: {
    fontSize: (16 / 667) * height,
    fontFamily: 'Now-Medium',
    color: 'white',
  },
})

export default styles
