/**
 *
 * Red button component
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, TouchableOpacity } from 'react-native'

import styles from './styles'

export default class RedButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
  }

  render() {
    const { title, onPress } = this.props

    return (
      <TouchableOpacity
        style={styles.button}
        activeOpacity={0.95}
        onPress={onPress}
      >
        <Text style={styles.buttonText}>{title}</Text>
      </TouchableOpacity>
    )
  }
}
