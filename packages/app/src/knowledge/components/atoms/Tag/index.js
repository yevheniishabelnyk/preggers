/**
 *
 * Tag component
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, TouchableOpacity } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class Tag extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    tag: PropTypes.object,
    onPress: PropTypes.func,
  }

  static defaultProps = {
    tag: {
      label: '',
    },
  }

  render() {
    const { tag } = this.props

    return (
      <TouchableOpacity
        style={styles.button}
        activeOpacity={0.95}
        onPress={this._tagPressed}
      >
        <Text style={styles.buttonText}>{tag.label}</Text>
      </TouchableOpacity>
    )
  }

  _tagPressed = () => {
    const { tag, onPress } = this.props

    if (isFunction(onPress)) {
      onPress(tag)
    }
  }
}
