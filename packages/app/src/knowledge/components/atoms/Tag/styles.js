import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  button: {
    marginTop: moderateScale(10, resizeFactor),
    marginRight: moderateScale(5, resizeFactor),
  },

  buttonText: {
    fontSize: moderateScale(11, resizeFactor),
    fontFamily: 'Now-Bold',
    color: 'rgb(35, 40, 46)',
    borderRadius: moderateScale(14, resizeFactor),
    borderWidth: 1,
    borderColor: 'rgb(250, 65, 105)',
    paddingHorizontal: moderateScale(14, resizeFactor),
    paddingVertical: moderateScale(6, resizeFactor),
    alignSelf: 'flex-start',
  },
})

export default styles
