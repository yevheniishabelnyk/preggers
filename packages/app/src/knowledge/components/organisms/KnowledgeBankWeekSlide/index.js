/**
 *
 * KnowledgeBankWeekSlide
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, ScrollView, ActivityIndicator, Image } from 'react-native'
import SubItem from '@/knowledge/components/molecules/SubItem'
import EmptyContent from '@/knowledge/components/molecules/EmptyContent'
import KnowledgeItemHighlight from '@/knowledge/components/molecules/KnowledgeItemHighlight'

import ActionButton from '@/settings/components/atoms/ActionButton'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

import { difference } from 'lodash'

export default class KnowledgeBankWeekSlide extends React.Component {
  static propTypes = {
    profile: PropTypes.object,
    week: PropTypes.object,
    weekArticles: PropTypes.array,
    firebase: PropTypes.object,
    pregnancy: PropTypes.object,
    onBackToCurrentWeek: PropTypes.func,
    disableParrentScroll: PropTypes.func,
    enableParrentScroll: PropTypes.func,
    weekNumber: PropTypes.number.isRequired,
    pregnancyCurrentWeekNumber: PropTypes.number,
    weekTags: PropTypes.array,
    favoriteArticles: PropTypes.array,
    goTo: PropTypes.func,
    onScroll: PropTypes.func,
    scrollEventThrottle: PropTypes.number,
    algoliaIndex: PropTypes.object,
    locale: PropTypes.string,
  }

  static defaultProps = {
    favoriteArticles: [],
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    fetchedArticles: [],
    isFetchingArticles: true,
    areAllArticlesLoaded: false,
  }

  articlesPage = -1

  render() {
    const { week, favoriteArticles } = this.props
    const {
      fetchedArticles,
      isFetchingArticles,
      areAllArticlesLoaded,
      featuredArticle,
    } = this.state
    const { formatMessage } = this.context.intl

    if (week === undefined) {
      // is fetching

      return (
        <View style={styles.loaderWrapper}>
          <ActivityIndicator
            animating
            size="large"
            color="gray"
            style={styles.loader}
          />
        </View>
      )
    }

    const { onScroll, scrollEventThrottle } = this.props

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        onScroll={onScroll}
        scrollEventThrottle={scrollEventThrottle}
      >
        <View style={styles.subItems}>
          {featuredArticle ? (
            <KnowledgeItemHighlight
              large
              onPress={this._articleItemPressed}
              article={featuredArticle}
            />
          ) : null}

          {fetchedArticles.map(article => (
            <SubItem
              onPress={this._articleItemPressed}
              key={article.id}
              article={article}
            />
          ))}
        </View>

        {!isFetchingArticles && !featuredArticle && !fetchedArticles.length ? (
          <EmptyContent title={formatMessage(messages.emptyArticlesText)} />
        ) : null}

        {!isFetchingArticles && !areAllArticlesLoaded ? (
          <ActionButton
            title={formatMessage(messages.loadMoreButtonTitle)}
            onPress={this._loadMoreArticlesButtonPressed}
            style={styles.loadMoreButton}
            titleStyle={styles.loadMoreButtonTitle}
            loader={false}
          />
        ) : null}

        <View style={styles.favorites}>
          <View style={styles.favoritesTitleWrap}>
            <Image
              source={require('assets/icons/bookmark-active.png')}
              style={styles.favoritesIcon}
            />

            <FormattedMessage
              {...messages.favoritesTitle}
              style={styles.favoritesTitle}
            />
          </View>

          {favoriteArticles.length ? (
            <View style={styles.subItems}>
              {favoriteArticles.map(
                article =>
                  article ? (
                    <SubItem
                      onPress={this._articleItemPressed}
                      key={article.id}
                      article={article}
                    />
                  ) : null
              )}
            </View>
          ) : (
            <View style={styles.emptyFavorites}>
              <FormattedMessage
                {...messages.emptyFavoritesText}
                style={styles.emptyFavoritesText}
              />

              <Image
                source={require('assets/img/empty-knowledge.png')}
                style={styles.emptyFavoritesImage}
              />
            </View>
          )}
        </View>
      </ScrollView>
    )
  }

  componentWillMount() {
    const { weekTags, week } = this.props

    if (week && week.featuredArticle) {
      this.featuredArticleId = week.featuredArticle[this.props.locale]

      if (this.featuredArticleId) {
        this._queryFeaturedArticle()
      }
    }

    this._queryTaggedArticles(weekTags)
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.week && nextProps.week) {
      if (nextProps.week && nextProps.week.featuredArticle) {
        this.featuredArticleId =
          nextProps.week.featuredArticle[nextProps.locale]

        if (this.featuredArticleId) {
          this._queryFeaturedArticle()
        }
      }
    }

    if (this.props.locale !== nextProps.locale) {
      if (nextProps.week && nextProps.week.featuredArticle) {
        this.featuredArticleId =
          nextProps.week.featuredArticle[nextProps.locale]

        if (this.featuredArticleId) {
          setTimeout(() => {
            this._queryFeaturedArticle()
          }, 0)
        }
      }
    }

    if (
      (!this.props.weekTags && nextProps.weekTags) ||
      (this.props.weekTags &&
        nextProps.weekTags &&
        difference(this.props.weekTags, nextProps.weekTags).length)
    ) {
      this._queryTaggedArticles(nextProps.weekTags)
    }
  }

  _loadMoreArticlesButtonPressed = () => {
    if (!this.areTaggedArticlesMisssing) {
      this._queryTaggedArticles(this.props.weekTags)
    } else {
      this._queryArticles()
    }
  }

  _queryFeaturedArticle = () => {
    if (this.featuredArticleId) {
      const { algoliaIndex } = this.props

      algoliaIndex.getObject(this.featuredArticleId).then(response => {
        if (response) {
          this.setState({ featuredArticle: response })
        }
      })
    }
  }

  _queryTaggedArticles = tags => {
    if (tags === null) {
      this.areTaggedArticlesMisssing = true
      this._queryArticles()
      return
    }

    const { areAllArticlesLoaded } = this.state

    if (!areAllArticlesLoaded) {
      const { algoliaIndex } = this.props

      if (tags && tags.length) {
        const excludeFeaturedArticleFilter = this.featuredArticleId
          ? `objectID:-${this.featuredArticleId}`
          : []

        algoliaIndex
          .search({
            facetFilters: tags
              .map(tagName => `_tags:${tagName}`)
              .concat(excludeFeaturedArticleFilter),
            page: this.articlesPage + 1,
            hitsPerPage: 4,
          })
          .then(res => {
            if (res.nbPages === 0) {
              this.areTaggedArticlesMisssing = true
              this._queryArticles()
            } else {
              this.articlesPage = this.articlesPage + 1

              this.setState(prevState => ({
                fetchedArticles: prevState.fetchedArticles.concat(res.hits),
                areAllArticlesLoaded:
                  res.nbPages === 0 || res.nbPages === this.articlesPage + 1,
                isFetchingArticles: false,
              }))
            }
          })
          .catch(err => {
            console.info('err: ', err)

            this.setState({ isFetchingArticles: false })
          })
      }
    }
  }

  _queryArticles = () => {
    const { algoliaIndex } = this.props

    const excludeFeaturedArticleFilter = this.featuredArticleId
      ? [`objectID:-${this.featuredArticleId}`]
      : []

    algoliaIndex
      .search({
        facetFilters: excludeFeaturedArticleFilter,
        page: this.articlesPage + 1,
        hitsPerPage: 4,
      })
      .then(res => {
        this.articlesPage = this.articlesPage + 1

        this.setState(prevState => ({
          fetchedArticles: prevState.fetchedArticles.concat(res.hits),
          areAllArticlesLoaded:
            res.nbPages === 0 ||
            this.articlesPage === 3 ||
            res.nbPages === this.articlesPage + 1,
          isFetchingArticles: false,
        }))
      })
      .catch(err => {
        console.info('err: ', err)

        this.setState({ isFetchingArticles: false })
      })
  }

  _articleItemPressed = articleId =>
    this.props.goTo('/knowledge-article', { articleId })
}
