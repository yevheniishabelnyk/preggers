import { StyleSheet, Dimensions, Platform } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(19, 4.2)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width,
    paddingHorizontal: gutter,
    position: 'relative',
    paddingTop:
      Platform.OS === 'android' ? moderateScale(70) : moderateScale(150, 0.7),
  },

  contentContainer: {
    paddingTop: moderateScale(30),
    paddingBottom: moderateScale(200),
  },

  loaderWrapper: {
    height: moderateScale(430, resizeFactor),
    width,
    alignItems: 'center',
    justifyContent: 'center',
  },

  loader: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  noData: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  subItems: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },

  loadMoreButton: {
    width: moderateScale(182, resizeFactor),
    height: moderateScale(40, resizeFactor),
    backgroundColor: 'rgb(250, 65, 105)',
    borderRadius: moderateScale(30, resizeFactor),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: moderateScale(20, resizeFactor),
  },

  loadMoreButtonTitle: {
    color: 'white',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15, resizeFactor),
    letterSpacing: 0.8,
  },

  favorites: {
    marginTop: moderateScale(50, resizeFactor),
  },

  favoritesTitleWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  favoritesIcon: {
    width: moderateScale(18, resizeFactor),
    height: moderateScale(25, resizeFactor),
    marginRight: moderateScale(9, resizeFactor),
  },

  favoritesTitle: {
    fontFamily: Platform.OS === 'android' ? 'Now-Medium' : 'Now-Bold',
    fontSize: moderateScale(26, resizeFactor),
    color: 'rgb(41, 53, 69)',
  },

  emptyFavoritesText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18, resizeFactor),
    color: 'rgb(52, 57, 65)',
    lineHeight: moderateScale(29, resizeFactor),
    marginTop: moderateScale(18, resizeFactor),
  },

  emptyFavoritesImage: {
    width: moderateScale(200, resizeFactor),
    height: moderateScale(226, resizeFactor),
    alignSelf: 'center',
    marginTop: moderateScale(42, resizeFactor),
  },
})

export default styles
