import { StyleSheet, Platform } from 'react-native'
import isIPhoneX from '@/shared/utils/isIPhoneX'
import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const marginTop =
  Platform.OS === 'ios'
    ? moderateScale(isIPhoneX() ? -73 : -49, resizeFactor)
    : null

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249,250,252)',
    alignItems: 'center',
    marginTop: marginTop,
    paddingTop: Platform.OS === 'ios' ? moderateScale(50, resizeFactor) : null,
    zIndex: 1,
  },

  tabBarContainer: {
    position: Platform.OS === 'ios' ? 'absolute' : 'relative',
    overflow: Platform.OS === 'ios' ? 'hidden' : 'visible',
  },

  header: {
    height: isIPhoneX()
      ? moderateScale(150, resizeFactor)
      : moderateScale(102, resizeFactor),
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
    zIndex: 2,
  },
})

export default styles
