import { defineMessages } from 'react-intl'

export default defineMessages({
  articles: 'Articles',
  myWeek: 'My Week',
  videos: 'Videos',
  podcasts: 'Podcasts',
})
