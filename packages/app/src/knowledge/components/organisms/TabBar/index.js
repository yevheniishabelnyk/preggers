/**
 *
 * KnowledgeTabBar
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import isIPhoneX from '@/shared/utils/isIPhoneX'

import { View, Animated, Platform } from 'react-native'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import Tabs from '@/knowledge/components/molecules/Tabs'
import Tab from '@/knowledge/components/atoms/Tab'

import StartIcon from 'assets/icons/knowledge-start.png'
import StartIconActive from 'assets/icons/knowledge-start-active.png'
import ArticlesIcon from 'assets/icons/article.png'
import ArticlesIconActive from 'assets/icons/article-active.png'
import VideosIcon from 'assets/icons/video.png'
import VideosIconActive from 'assets/icons/video-active.png'
import PodcastsIcon from 'assets/icons/audio.png'
import PodcastsIconActive from 'assets/icons/audio-active.png'

import messages from './messages'

import styles from './styles'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.4

export let scrollY = new Animated.Value(0)

let headerHeight

isIPhoneX() ? (headerHeight = 200) : (headerHeight = 150)

const HEADER_MAX_HEIGHT = moderateScale(headerHeight, resizeFactor)
const HEADER_MIN_HEIGHT = 0
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT

export const scrollHandler = Animated.event([
  { nativeEvent: { contentOffset: { y: scrollY } } },
])

export default class TabBar extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const activeTabIndex = this.props.navigation.state.index

    const { formatMessage } = this.context.intl

    const tabTitles = [
      formatMessage(messages.myWeek),
      formatMessage(messages.articles),
      formatMessage(messages.videos),
      formatMessage(messages.podcasts),
    ]

    const headerHeight = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    })

    const androidHeaderHeight = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MIN_HEIGHT, -HEADER_MAX_HEIGHT],
      extrapolate: 'clamp',
    })

    return (
      <View style={styles.container}>
        <SettingsHeader
          title={tabTitles[activeTabIndex]}
          // onBackButtonPress={this._goBack}
          // onSearchButtonPress={this._searchButtonPressed}
          style={styles.header}
          withInidcators
        />

        <Animated.View
          style={[
            styles.tabBarContainer,
            {
              top:
                Platform.OS === 'android' ? androidHeaderHeight : headerHeight,
            },
          ]}
        >
          <Tabs>
            <Tab
              title={tabTitles[0]}
              icon={activeTabIndex === 0 ? StartIconActive : StartIcon}
              onPress={this._firstTabPressed}
            />

            <Tab
              title={tabTitles[1]}
              icon={activeTabIndex === 1 ? ArticlesIconActive : ArticlesIcon}
              onPress={this._secondTabPressed}
            />

            <Tab
              title={tabTitles[2]}
              icon={activeTabIndex === 2 ? VideosIconActive : VideosIcon}
              onPress={this._thirdTabPressed}
            />

            <Tab
              title={tabTitles[3]}
              icon={activeTabIndex === 3 ? PodcastsIconActive : PodcastsIcon}
              onPress={this._forthTabPressed}
            />
          </Tabs>
        </Animated.View>
      </View>
    )
  }

  _firstTabPressed = () => this._tabPressed(0)

  _secondTabPressed = () => this._tabPressed(1)

  _thirdTabPressed = () => this._tabPressed(2)

  _forthTabPressed = () => this._tabPressed(3)

  _tabPressed = tabIndex => {
    const { navigation } = this.props

    const activeTabIndex = navigation.state.index

    if (tabIndex !== activeTabIndex) {
      const nextRoute = navigation.state.routes[tabIndex]

      navigation.navigate(nextRoute.routeName)
    }
  }

  _goBack = () => this.props.navigation.goBack()

  _searchButtonPressed = () =>
    this.props.navigation.navigate('/knowledge-search')
}
