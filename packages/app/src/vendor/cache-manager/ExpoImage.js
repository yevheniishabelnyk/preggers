/**
 *
 * ExpoImage
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Image as RNImage,
  Animated,
  StyleSheet,
  View,
  Platform,
} from 'react-native'
import { BlurView } from 'expo'

import CacheManager from './CacheManager'

import { has, transform, pickBy, isFunction } from 'lodash'

const styles = StyleSheet.create({
  androidBlurView: {
    backgroundColor: 'white',
  },
})

export default class Image extends React.Component {
  static propTypes = {
    source: PropTypes.shape({
      uri: PropTypes.string,
      type: PropTypes.string,
      preview: PropTypes.string,
    }),
    cacheImage: PropTypes.bool,
    findInCache: PropTypes.bool,
    resizeMode: PropTypes.string,
    style: PropTypes.any,
    onLoad: PropTypes.func,
    onError: PropTypes.func,
  }

  static defaultProps = {
    cacheImage: false,
    resizeMode: 'cover',
  }

  subscribedToCache = true
  isCaching = false

  constructor(props) {
    super(props)

    if (
      props.source &&
      props.source.uri &&
      props.source.uri.startsWith('file')
    ) {
      this.intensity = new Animated.Value(0)

      this.state = {
        localUri: props.source.uri,
        isImageReady: true,
      }
    } else {
      this.intensity = new Animated.Value(100)

      this.state = {
        localUri: undefined,
        isImageReady: false,
      }
    }
  }

  render() {
    const { style: computedStyle } = this
    const { style, ...otherProps } = this.props

    const { source, cacheImage, resizeMode, findInCache } = this.props
    const { localUri, isImageReady } = this.state

    const hasSource = Boolean(source)
    const hasPreview = has(source, 'preview')

    if (!cacheImage && hasSource && !hasPreview) {
      return <RNImage source={source} style={style} {...otherProps} />
    }

    if (hasSource && hasPreview) {
      let opacity

      if (Platform.OS === 'android') {
        opacity = this.intensity.interpolate({
          inputRange: [0, 100],
          outputRange: [0, 0.5],
        })
      }

      const uri = localUri || source.uri

      return (
        <View style={style}>
          {!isImageReady && (
            <RNImage
              source={{ uri: source.preview }}
              style={computedStyle}
              blurRadius={Platform.OS === 'android' ? 0.5 : 0}
            />
          )}

          {(!cacheImage && !findInCache) || isImageReady ? (
            <RNImage
              source={{ uri: uri }}
              style={computedStyle}
              resizeMode={resizeMode}
              onLoad={this._onImageLoad}
              onError={this._onImageLoadError}
            />
          ) : null}

          {Platform.OS === 'ios' && (
            <AnimatedBlurView
              tint="light"
              style={computedStyle}
              intensity={this.intensity}
            />
          )}

          {Platform.OS === 'android' && (
            <Animated.View
              style={[computedStyle, styles.androidBlurView, { opacity }]}
            />
          )}
        </View>
      )
    }

    return null
  }

  componentWillMount() {
    this._loadStyle(this.props)

    this._maybeCacheImage(this.props)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.style !== nextProps.style) {
      this._loadStyle(nextProps)
    }

    if (
      nextProps.source &&
      (!this.props.source || this.props.source.uri !== nextProps.source.uri)
    ) {
      this._maybeCacheImage(nextProps)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { source } = this.props

    if (
      source &&
      source.preview &&
      this.state.localUri &&
      prevState.localUri === undefined
    ) {
      Animated.timing(this.intensity, {
        duration: 300,
        toValue: 0,
        useNativeDriver: Platform.OS === 'android',
      }).start(() => {
        const { onLoad } = this.props

        if (isFunction(onLoad)) {
          setTimeout(onLoad, 50)
        }
      })
    }
  }

  componentWillUnmount() {
    this.subscribedToCache = false
  }

  _onImageLoad = () => {
    if (!this.props.cacheImage) {
      this.setState({ isImageReady: true })

      Animated.timing(this.intensity, {
        duration: 300,
        toValue: 0,
        useNativeDriver: Platform.OS === 'android',
      }).start(() => {
        const { onLoad } = this.props

        if (isFunction(onLoad)) {
          setTimeout(onLoad, 50)
        }
      })
    }
  }

  _onImageLoadError = () => {
    const { onError } = this.props

    if (isFunction(onError)) {
      onError()
    }
  }

  _loadStyle(props) {
    const { style } = props

    this.style = [
      StyleSheet.absoluteFill,
      transform(
        pickBy(
          StyleSheet.flatten(style),
          (value, key) => propsToCopy.indexOf(key) !== -1
        ),
        (result, value, key) =>
          Object.assign(result, {
            [key]: value - (style.borderWidth || 0),
          })
      ),
    ]
  }

  _maybeCacheImage = nextProps => {
    const { source, cacheImage, findInCache } = nextProps

    if (
      !this.isCaching &&
      cacheImage &&
      source &&
      source.preview &&
      source.uri &&
      !source.uri.startsWith('file')
    ) {
      this.isCaching = true

      CacheManager.cache({
        remoteUri: source.uri,
        localDirectory: 'content',
        listener: this._onImageCached,
      })
    }

    if (findInCache && source && source.uri && !source.uri.startsWith('file')) {
      CacheManager.getCacheEntry(source.uri, 'content')
        .then(({ exists, path }) => {
          if (exists) {
            this.setState({ localUri: path, isImageReady: true })
          } else {
            this.setState({ isImageReady: true })
          }
        })
        .catch(err => {
          console.info('err: ', err)

          this.setState({ isImageReady: true })
        })
    }
  }

  _onImageCached = localUri => {
    this.isCaching = false

    if (this.subscribedToCache) {
      this.setState({ localUri, isImageReady: true })
    }
  }
}

const propsToCopy = [
  'borderRadius',
  'borderBottomLeftRadius',
  'borderBottomRightRadius',
  'borderTopLeftRadius',
  'borderTopRightRadius',
]

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView)
