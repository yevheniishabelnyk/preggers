/**
 *
 * Cache manager
 *
 */

import { FileSystem } from 'expo'
import SHA1 from 'crypto-js/sha1'

const TEMP_DIR = FileSystem.cacheDirectory

export default class CacheManager {
  static listeners = {}
  static started = {}
  static directories = {}

  static init(folders) {
    // console.info('\n[CACHE MANAGER] <init>')
    // console.info('\n[CACHE MANAGER] <init> Directories: ', folders)

    return Promise.all(
      folders.map(folderName => {
        const DIR_PATH = `${TEMP_DIR}${folderName}/`

        return FileSystem.getInfoAsync(DIR_PATH).then(info => {
          if (!info.exists) {
            return FileSystem.makeDirectoryAsync(DIR_PATH, {
              intermediates: true,
            }).then(() => {
              this.directories[folderName] = {
                name: folderName,
                path: DIR_PATH,
                preserveOnLogout: true,
              }
            })
          }

          this.directories[folderName] = {
            name: folderName,
            path: DIR_PATH,
            preserveOnLogout: true,
          }
        })
      })
    )
  }

  static async cache({ remoteUri, localDirectory, listener }) {
    if (this.started[remoteUri]) {
      // console.log('already started')

      addListener(remoteUri, listener)

      return
    }

    this.started[remoteUri] = true

    // console.info('\n[CACHE MANAGER] <cache>')
    // console.info('\n[CACHE MANAGER] <cache> From remote: \n\n', remoteUri)
    // console.info('\n[CACHE MANAGER] <cache> To dir: ', localDirectory)

    if (isDownloading(remoteUri)) {
      // console.info(`\n[CACHE MANAGER] <cache> Is downloading \n${remoteUri}`)

      addListener(remoteUri, listener)
    } else {
      const { path, exists } = await this.getCacheEntry(
        remoteUri,
        localDirectory
      )

      if (exists) {
        // console.info(
        //   `\n[CACHE MANAGER] <cache> Remote uri \n\n${remoteUri} \n\nexists under path \n\n${path}`
        // )

        addListener(remoteUri, listener)
        // listener(path)
        notifyAll(remoteUri, path)

        unsubscribe(remoteUri)
      } else {
        // console.info(`\n[CACHE MANAGER] <cache> Does not exist ${remoteUri}`)
        // console.info('\n[CACHE MANAGER] <cache> Add listener')

        addListener(remoteUri, listener)

        try {
          // console.info(
          //   `\n[CACHE MANAGER] <cache> Start downloading ${remoteUri}`
          // )

          await FileSystem.downloadAsync(remoteUri, path)

          // console.info(`\n[CACHE MANAGER] <cache> Cached ${remoteUri}`)

          notifyAll(remoteUri, path)
        } catch (err) {
          // console.info(
          //   `\n[CACHE MANAGER] <cache> Error caching ${remoteUri}: `,
          //   err
          // )

          notifyAll(remoteUri, err)
        }

        unsubscribe(remoteUri)
      }

      // console.log('remoteUri ended', remoteUri)
      this.started[remoteUri] = false
    }
  }

  static async getCacheEntry(remoteUri, localDirectory) {
    // console.info('\n[CACHE MANAGER] <getCacheEntry>')
    // console.info(
    //   `\n[CACHE MANAGER] <getCacheEntry> From remote \n\n${remoteUri}`
    // )
    // console.info('\n[CACHE MANAGER] <getCacheEntry> To dir: ', localDirectory)

    if (!this.directories[localDirectory]) {
      await this.init([localDirectory])
    }

    const fileName = remoteUri.substring(
      remoteUri.lastIndexOf('/'),
      remoteUri.indexOf('?') === -1 ? remoteUri.length : remoteUri.indexOf('?')
    )

    const ext =
      fileName.indexOf('.') === -1
        ? '.jpg'
        : fileName.substring(fileName.lastIndexOf('.'))

    const path = this.directories[localDirectory].path + SHA1(remoteUri) + ext

    // console.info('\n[CACHE MANAGER] <getCacheEntry> Entry path: \n\n', path)

    const info = await FileSystem.getInfoAsync(path)

    if (info.exists) {
      // console.info('\n[CACHE MANAGER] <getCacheEntry> Entry exists')
    }

    return { exists: info.exists, path }
  }

  static async delete(remoteUri, localDirectory) {
    // console.info('\n[CACHE MANAGER] <delete>')
    // console.info(`\n[CACHE MANAGER] <delete> From remote \n\n${remoteUri}`)
    // console.info('\n[CACHE MANAGER] <delete> To dir: ', localDirectory)

    const { exists, path: localCachePath } = await CacheManager.getCacheEntry(
      remoteUri,
      localDirectory
    )

    if (exists) {
      // console.info('\n[CACHE MANAGER] <delete> Entry exists')
      // console.info(
      //   '\n[CACHE MANAGER] <delete> Entry path: \n\n',
      //   localCachePath
      // )
      // console.info('\n[CACHE MANAGER] <delete> Deleting entry...')

      try {
        await FileSystem.deleteAsync(localCachePath, { idempotent: true })

        // console.info(
        //   '\n[CACHE MANAGER] <delete> Entry successfully deleted\n\n',
        //   localCachePath
        // )

        return Promise.resolve()
      } catch (err) {
        // console.log('\n[CACHE MANAGER] <delete> Error: ', err)
        // console.info(
        //   '\n[CACHE MANAGER] <delete> Error deleting entry \n\n',
        //   localCachePath
        // )

        return Promise.resolve()
      }
    } else {
      // console.info("\n[CACHE MANAGER] <delete> Doesn't exist")

      return Promise.resolve()
    }
  }

  static async clearUserCache(userId) {
    // console.info('\n[CACHE MANAGER] <clearUserCache>')
    // console.info('\n[CACHE MANAGER] <clearUserCache> User ID: ', userId)

    try {
      // console.info(
      //   `\n[CACHE MANAGER] <clearUserCache> Deleting directory "/users/${userId}}" from cache...`
      // )

      await FileSystem.deleteAsync(`${TEMP_DIR}users/${userId}`, {
        idempotent: true,
      })

      // console.info(
      //   `\n[CACHE MANAGER] <clearUserCache> Directory "/users/${userId}}" successfully deleted`
      // )

      return Promise.resolve()
    } catch (err) {
      // console.log('\n[CACHE MANAGER] <clearUserCache> Error: ', err)
      // console.info(
      //   `\n[CACHE MANAGER] <delete> Error deleting directory "/users/${userId}}" `
      // )

      return Promise.resolve()
    }
  }
}

const unsubscribe = uri => {
  // console.log('\n[CACHE MANAGER] <unsubscribe> uri: ', uri)

  delete CacheManager.listeners[uri]
}

const notifyAll = (uri, path) => {
  // console.log('\n[CACHE MANAGER] <notifyAll> uri: ', uri)
  // console.log(
  //   '\n[CACHE MANAGER] <notifyAll> all listeners: ',
  //   CacheManager.listeners[uri]
  // )

  CacheManager.listeners[uri].forEach(listener => listener(path))
}

const addListener = (uri, listener) => {
  if (!CacheManager.listeners[uri]) {
    CacheManager.listeners[uri] = []
  }

  CacheManager.listeners[uri].push(listener)
}

const isDownloading = uri => CacheManager.listeners[uri] !== undefined
