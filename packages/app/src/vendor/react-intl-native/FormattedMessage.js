import React from 'react'
import PropTypes from 'prop-types'
import { Text } from 'react-native'
import { FormattedMessage } from 'react-intl'

export default class RNFormattedMessage extends React.Component {
  static propTypes = {
    style: PropTypes.any,
  }

  render() {
    // console.log('render RNFormattedMessage')
    // console.log('props: ', this.props)

    return (
      <FormattedMessage {...this.props}>
        {(...nodes) => {
          const newNodes = nodes.map(node => {
            if (!React.isValidElement(node)) {
              // console.log('NODE: ', node)

              return (
                <Text
                  ref={component => (this._root = component)}
                  style={this.props.style}
                >
                  {node}
                </Text>
              )
            }

            return node
          })

          return React.createElement(
            Text,
            {
              style: this.props.style,
              ref: component => (this._root = component),
            },
            ...newNodes
          )
        }}
      </FormattedMessage>
    )
  }

  setNativeProps = nativeProps => {
    this._root.setNativeProps(nativeProps)
  }
}
