import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  errorContainer: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },

  errorText: {
    color: 'white',
    fontSize: 15,
    fontStyle: 'italic',
  },
})

export default styles
