import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  rowContainer: {
    alignItems: 'flex-end',
  },

  container: {
    flex: 1,
  },

  listContainer: {
    flex: 1,
  },
})

export default styles
