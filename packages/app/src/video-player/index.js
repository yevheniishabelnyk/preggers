/**
 *
 * Video Player
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Dimensions } from 'react-native'
import { Video, ScreenOrientation } from 'expo'
import VideoPlayer from './vendor/expo-videoplayer'

export default class Player extends React.Component {
  static propTypes = {
    videoStyle: PropTypes.any,
    videoWidth: PropTypes.number,
    videoHeight: PropTypes.number,
  }

  render() {
    return (
      <VideoPlayer
        videoProps={{
          shouldPlay: false,
          resizeMode: Video.RESIZE_MODE_CONTAIN,
          source: {
            uri:
              'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
          },
          isMuted: false,
          style: this.props.videoStyle,
          width: this.state.videoWidth || this.props.videoWidth,
          height: this.state.videoHeight || this.props.videoHeight,
          // ref: component => {
          //   this._playbackInstance = component
          // },
        }}
        showControlsOnLoad
        isPortrait={this.state.isPortrait}
        switchToLandscape={this.switchToLandscape}
        switchToPortrait={this.switchToPortrait}
        playFromPositionMillis={0}
      />
    )
  }

  state = {
    isPortrait: true,
  }

  componentDidMount() {
    Dimensions.addEventListener('change', this.orientationChangeHandler)
  }

  componentWillUnmount() {
    Dimensions.removeEventListener('change', this.orientationChangeHandler)
  }

  orientationChangeHandler = dims => {
    const { width, height } = dims.window

    const isLandscape = width > height

    this.setState({
      isPortrait: !isLandscape,
      videoWidth: isLandscape ? width : null,
      videoHeight: isLandscape ? height : null,
    })
  }

  switchToLandscape() {
    ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE)
  }

  switchToPortrait() {
    ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT)
  }
}
