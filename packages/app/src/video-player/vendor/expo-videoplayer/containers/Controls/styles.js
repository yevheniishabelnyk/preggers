import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'stretch',
    opacity: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  slider: {
    marginRight: 10,
    marginLeft: 10,
    flex: 1,
  },

  toggleFullScreenButton: {
    backgroundColor: 'transparent',
  },
})

export default styles
