/**
 *
 * Controls
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Animated, TouchableWithoutFeedback, Slider } from 'react-native'

import Time from '../../components/Time'
import Duration from '../../components/Duration'
import ToggleFullScreenButton from '../../components/ToggleFullScreenButton'

import styles from './styles'

export default class Controls extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    textStyle: PropTypes.any,
    fullscreenEnterIcon: PropTypes.any,
    fullscreenExitIcon: PropTypes.any,
    trackImage: PropTypes.any,
    thumbImage: PropTypes.any,
    isPortrait: PropTypes.bool,
    video: PropTypes.any,
    onToggleFullSceen: PropTypes.func,
    disabled: PropTypes.bool,
    sliderDisabled: PropTypes.bool,
    onSliderValueChange: PropTypes.func,
    onSlidingComplete: PropTypes.func,
    onSliderLayout: PropTypes.func,
    onSliderPress: PropTypes.func,
  }

  render() {
    const {
      style,
      textStyle,
      fullscreenEnterIcon,
      fullscreenExitIcon,
      trackImage,
      thumbImage,
      isPortrait,
      video,
      onToggleFullSceen,
      disabled,
      sliderDisabled,
      onSliderValueChange,
      onSlidingComplete,
      onSliderLayout,
      onSliderPress,
    } = this.props

    return (
      <Animated.View
        pointerEvents={disabled ? 'none' : 'auto'}
        style={[styles.container, style]}
      >
        <Time
          style={textStyle}
          value={this._getMMSSFromMillis(video.position)}
        />

        <TouchableWithoutFeedback
          onLayout={onSliderLayout}
          onPress={onSliderPress}
        >
          <Slider
            style={styles.slider}
            trackImage={trackImage}
            thumbImage={thumbImage}
            value={this._getSeekSliderPosition()}
            onValueChange={onSliderValueChange}
            onSlidingComplete={onSlidingComplete}
            disabled={sliderDisabled}
          />
        </TouchableWithoutFeedback>

        <Duration
          style={textStyle}
          value={this._getMMSSFromMillis(video.duration)}
        />

        <ToggleFullScreenButton
          style={styles.toggleFullScreenButton}
          fullscreenEnterIcon={fullscreenEnterIcon}
          fullscreenExitIcon={fullscreenExitIcon}
          isPortrait={isPortrait}
          callback={onToggleFullSceen}
        />
      </Animated.View>
    )
  }

  _getSeekSliderPosition() {
    const { video } = this.props

    if (video.position != null && video.duration != null) {
      return video.position / video.duration
    }

    return 0
  }

  _getMMSSFromMillis = ms => {
    const totalSeconds = ms / 1000
    const seconds = Math.floor(totalSeconds % 60)
    const minutes = Math.floor(totalSeconds / 60)

    return padWithZero(minutes) + ':' + padWithZero(seconds)

    function padWithZero(number) {
      const string = number.toString()

      if (number < 10) {
        return '0' + string
      }

      return string
    }
  }
}
