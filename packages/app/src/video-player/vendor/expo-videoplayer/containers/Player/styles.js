import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
  },

  content: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 35,
    paddingTop: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },

  error: {
    alignSelf: 'stretch',
  },
})

export default styles
