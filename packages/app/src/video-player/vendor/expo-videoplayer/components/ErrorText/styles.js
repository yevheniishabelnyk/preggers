import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    width: 0,
    marginRight: 20,
    marginLeft: 20,
  },

  errorText: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 12,
  },
})

export default styles
