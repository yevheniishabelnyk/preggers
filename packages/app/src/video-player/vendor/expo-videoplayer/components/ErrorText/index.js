/**
 *
 * ErrorText
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'

import styles from './styles'

const ErrorText = ({ style, textStyle, error }) => (
  <View style={[styles.container, style]}>
    <Text style={[textStyle, styles.errorText]}>{error}</Text>
  </View>
)

ErrorText.propTypes = {
  error: PropTypes.string,
  style: PropTypes.any,
  textStyle: PropTypes.any,
}

export default ErrorText
