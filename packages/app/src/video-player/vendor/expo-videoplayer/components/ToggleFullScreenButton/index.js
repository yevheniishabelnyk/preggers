/**
 *
 * ToggleFullScreenButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import { FullscreenEnterIcon, FullscreenExitIcon } from '../../assets/icons'

import { isFunction } from 'lodash'

// import styles from './styles'

export default class ToggleFullScreenButton extends React.Component {
  static propTypes = {
    callback: PropTypes.func,
    style: PropTypes.any,
    contentStyle: PropTypes.any,
    icon: PropTypes.any,
    fullscreenEnterIcon: PropTypes.any,
    fullscreenExitIcon: PropTypes.any,
    isPortrait: PropTypes.bool,
  }

  static defaultProps = {
    fullscreenEnterIcon: FullscreenEnterIcon,
    fullscreenExitIcon: FullscreenExitIcon,
  }

  hitSlop = { top: 20, left: 20, bottom: 20, right: 20 }

  render() {
    const {
      isPortrait,
      style,
      contentStyle,
      fullscreenEnterIcon: FullscreenEnterIcon,
      fullscreenExitIcon: FullscreenExitIcon,
    } = this.props

    return (
      <TouchableOpacity
        style={style}
        hitSlop={this.hitSlop}
        onPress={this._controlPressed}
        activeOpacity={0.95}
      >
        <View style={contentStyle}>
          {isPortrait ? <FullscreenEnterIcon /> : <FullscreenExitIcon />}
        </View>
      </TouchableOpacity>
    )
  }

  _controlPressed = () => {
    const { callback } = this.props

    if (isFunction(callback)) {
      callback()
    }
  }
}
