/**
 *
 * Time
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'

import styles from './styles'

const Time = ({ style, value }) => (
  <Text style={[styles.textStyle, style]}>{value}</Text>
)

Time.propTypes = {
  value: PropTypes.string,
  style: PropTypes.any,
}

export default Time
