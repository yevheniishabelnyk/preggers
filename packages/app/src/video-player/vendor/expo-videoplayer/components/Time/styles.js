import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  textStyle: {
    backgroundColor: 'transparent',
    color: '#FFFFFF',
    fontSize: 12,
    marginLeft: 5,
  },
})

export default styles
