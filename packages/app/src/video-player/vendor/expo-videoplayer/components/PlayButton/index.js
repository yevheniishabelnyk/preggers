/**
 *
 * PlayButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import { PlayIcon } from '../../assets/icons'

import { isFunction } from 'lodash'

// import styles from './styles'

export default class PlayButton extends React.Component {
  static propTypes = {
    callback: PropTypes.func,
    style: PropTypes.any,
    contentStyle: PropTypes.any,
    icon: PropTypes.any,
  }

  static defaultProps = {
    icon: PlayIcon,
  }

  hitSlop = { top: 20, left: 20, bottom: 20, right: 20 }

  render() {
    const { style, contentStyle, icon: Icon } = this.props

    return (
      <TouchableOpacity
        style={style}
        hitSlop={this.hitSlop}
        onPress={this._controlPressed}
        activeOpacity={0.95}
      >
        <View style={contentStyle}>
          <Icon />
        </View>
      </TouchableOpacity>
    )
  }

  _controlPressed = () => {
    const { callback } = this.props

    if (isFunction(callback)) {
      callback()
    }
  }
}
