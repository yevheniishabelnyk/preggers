/**
 *
 * Duration
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'

import styles from './styles'

const Duration = ({ style, value }) => (
  <Text style={[styles.textStyle, style]}>{value}</Text>
)

Duration.propTypes = {
  value: PropTypes.string,
  style: PropTypes.any,
}

export default Duration
