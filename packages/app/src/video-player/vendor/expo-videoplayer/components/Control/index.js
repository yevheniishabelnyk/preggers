/**
 *
 * Control
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import { isFunction } from 'lodash'

// import styles from './styles'

export default class Control extends React.Component {
  static propTypes = {
    callback: PropTypes.func,
    style: PropTypes.any,
    contentStyle: PropTypes.any,
    children: PropTypes.any,
  }

  hitSlop = { top: 20, left: 20, bottom: 20, right: 20 }

  render() {
    const { style, contentStyle, children, ...otherProps } = this.props

    return (
      <TouchableOpacity
        {...otherProps}
        style={style}
        hitSlop={this.hitSlop}
        onPress={this._controlPressed}
        activeOpacity={0.95}
      >
        <View style={contentStyle}>{children}</View>
      </TouchableOpacity>
    )
  }

  _controlPressed = () => {
    const { callback } = this.props

    if (isFunction(callback)) {
      callback()
    }
  }
}
