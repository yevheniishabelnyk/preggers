import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: Platform.OS === 'ios' ? moderateScale(22.5, 2.5) : null,
  },

  innerContainer: {
    paddingHorizontal: moderateScale(22.5, 2.5),
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  formField: {
    marginBottom: moderateScale(13, resizeFactor),
  },

  buttonWithArrow: {
    marginRight: moderateScale(-22.5, 2.5),
  },

  childIsBornButton: {
    marginTop: moderateScale(32, resizeFactor),
    marginBottom: moderateScale(11, resizeFactor),
  },

  calculateDueDateLink: {
    marginBottom: moderateScale(30, resizeFactor),
    marginRight: 'auto',
  },

  calculateDueDateLinkText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, resizeFactor),
    letterSpacing: 0.6,
    backgroundColor: 'transparent',
    color: 'rgb(131,146,167)',
    textDecorationLine: 'underline',
  },

  dialog: {
    width: moderateScale(330, resizeFactor),
    height: 'auto',
    overflow: 'hidden',
  },

  button: {
    marginBottom: moderateScale(37, resizeFactor),
    marginTop: 'auto',
  },

  datePicker: {
    alignSelf: 'stretch',
  },

  buttonsWrapper: {
    alignSelf: 'stretch',
    paddingLeft: Platform.OS === 'android' ? moderateScale(22.5, 2.5) : null,
  },

  textInput: {
    marginBottom: moderateScale(13, resizeFactor),
    paddingRight: moderateScale(9, 0.7),
  },
})

export default styles
