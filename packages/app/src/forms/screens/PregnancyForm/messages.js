import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitlePregnancy: 'Pregnancy & due dates',
  headerTitleAddPregnancy: 'Add pregnancy',
  babyNameInputTitle: 'Baby nickname',
  babyGenderSelectTitle: 'Baby gender',
  dueDateDatePickerTitle: 'Due date',
  calculateDueDateLinkText: 'Help me calculate',
  pregnancyLengthInputTitle: 'Pregnancy length',
  pregnancyLossButtonTitle: 'Pregnancy loss',
  childIsBornButtonTitle: 'My child is born',
  addPregnancyButtonTitle: 'Add pregnancy',
  daysText: 'days',
  girl: 'Girl',
  boy: 'Boy',
  childNameErrorTitle: 'Please enter a nickname',
  dueDateErrorTitle: 'Due date is wrong',
  noButtonTitle: 'No',
  yesButtonTitle: 'Yes',
  connectionErrorTitle:
    'Sorry, something went wrong. Do you want to try again?',
  defaultBabyName: 'My baby',
})
