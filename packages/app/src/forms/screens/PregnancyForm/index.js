/**
 *
 * PregnancyForm
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import moment from 'moment'

import { TouchableOpacity, View, Platform } from 'react-native'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import ActionButton from '@/settings/components/atoms/ActionButton'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import FormInput from '@/settings/components/molecules/FormInput'
import FormSelect from '@/settings/components/molecules/FormSelect'
import FormSelectWithClear from '@/settings/components/molecules/FormSelectWithClear'
import WhiteFormButton from '@/settings/components/molecules/WhiteFormButton'
import SettingsDatePicker from '@/settings/components/molecules/SettingsDatePicker'
import FormError from '@/forms/components/atoms/FormError'
import Prompt from '@/shared/components/Prompt'
import PopupDialog, { FadeAnimation } from 'react-native-popup-dialog'

import * as PregnanciesActions from '@/app/database/actions/pregnancies'
import { NavigationActions } from 'react-navigation'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { PREGNANCY__DEFAULT_BABY_NAME } from '@/app/constants/BabyNames'

import styles from './styles'

import { isFunction } from 'lodash'

import { firebaseConnect } from 'react-redux-firebase'

import { getPregnancy } from '@/app/redux/selectors'
import Pregnancy from '@/app/database/models/Pregnancy'

@firebaseConnect(props => {
  const flow = props.navigation.getParam('flow')

  switch (flow) {
    case 'EDIT': {
      const pregnancyId = props.navigation.getParam('id')

      if (pregnancyId) {
        return [`pregnancies/${pregnancyId}`]
      }

      break
    }
  }
})
@connect((state, props) => () => {
  const flow = props.navigation.getParam('flow', 'ADD')

  switch (flow) {
    case 'EDIT': {
      const pregnancyId = props.navigation.getParam('id')

      const pregnancy = getPregnancy(state, pregnancyId)

      return {
        pregnancy,
        isFetching: pregnancy === null,
      }
    }
  }

  return {
    isFetching: false,
  }
})
export default class PregnancyForm extends BaseScreen {
  static propTypes = {
    pregnancy: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props, context) {
    super(props)

    const { formatMessage } = context.intl

    this.flow = props.navigation.getParam('flow', 'ADD')

    this.genderTypes = [
      { label: formatMessage(messages.girl), value: 'f' },
      { label: formatMessage(messages.boy), value: 'm' },
    ]

    this.pregnancyLengthTypes = [
      {
        label: `280 ${formatMessage(messages.daysText)} (39+6)`,
        value: '39+6',
      },
      {
        label: `280 ${formatMessage(messages.daysText)} (40+0)`,
        value: '40+0',
      },
    ]

    this.state = this._getInitialState(
      this.flow,
      props.pregnancy,
      context.intl.formatMessage
    )

    this.dueDateMinDate = new Date()
    this.dueDateMaxDate = moment()
      .add(266, 'days')
      .toDate()
  }

  render() {
    const { pregnancy } = this.props
    const { isRequesting } = this.state
    const { formatMessage } = this.context.intl

    const title = pregnancy
      ? formatMessage(messages.headerTitlePregnancy)
      : formatMessage(messages.headerTitleAddPregnancy)

    const onDone = pregnancy ? this._doneButtonPressed : undefined

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={title}
              onBackButtonPress={this._goBack}
              onDoneButtonPress={onDone}
              style={styles.header}
            />
          ),

          popups: [
            <PopupDialog
              key="connectionErrorDialog"
              ref={ref => (this._connectionErrorDialog = ref)}
              dialogAnimation={new FadeAnimation({ animationDuration: 200 })}
              dialogStyle={styles.dialog}
              overlayOpacity={0.7}
            >
              <Prompt
                text={formatMessage(messages.connectionErrorTitle)}
                negativeButton={{
                  title: formatMessage(messages.noButtonTitle),
                  onPress: this._endRequest,
                }}
                positiveButton={{
                  title: formatMessage(messages.yesButtonTitle),
                  onPress: this._repeatRequest,
                }}
              />
            </PopupDialog>,
          ],
        }}
        headerHeight={SettingsHeader.height}
      >
        <View style={Platform.OS === 'android' ? styles.innerContainer : null}>
          <FormInput
            title={formatMessage(messages.babyNameInputTitle)}
            value={this.state.childName}
            onChange={this._setChildName}
            onFocus={this._onInputFocused}
            error={this.state.childNameError}
            autoCorrect={false}
            style={styles.textInput}
          />

          <FormError
            isVisible={this.state.childNameError}
            title={formatMessage(messages.childNameErrorTitle)}
          />

          <FormSelectWithClear
            title={formatMessage(messages.babyGenderSelectTitle)}
            value={this.state.childGender}
            items={this.genderTypes}
            onSelect={this._setChildGender}
            onClear={this._onClear}
            style={styles.formField}
          />

          <SettingsDatePicker
            title={formatMessage(messages.dueDateDatePickerTitle)}
            onSubmit={this._setDueDate}
            value={this.state.dueDate}
            style={styles.formField}
            minimumDateLimit={this.dueDateMinDate}
            maximumDateLimit={this.dueDateMaxDate}
            contentContainerStyle={styles.datePicker}
            error={this.state.dueDateError}
          />

          <FormError
            isVisible={this.state.dueDateError}
            title={formatMessage(messages.dueDateErrorTitle)}
          />

          <TouchableOpacity
            style={styles.calculateDueDateLink}
            onPress={this._calculateDueDateLinkPressed}
            activeOpacity={0.95}
          >
            <FormattedMessage
              {...messages.calculateDueDateLinkText}
              style={styles.calculateDueDateLinkText}
            />
          </TouchableOpacity>

          <FormSelect
            title={formatMessage(messages.pregnancyLengthInputTitle)}
            value={this.state.pregnancyLengthType}
            items={this.pregnancyLengthTypes}
            defaultValue={this.pregnancyLengthTypes[0].value}
            onSelect={this._setPregnancyLengthType}
            style={styles.formField}
          />
        </View>

        {this.flow === 'EDIT' ? (
          <View style={styles.buttonsWrapper}>
            <WhiteFormButton
              title={formatMessage(messages.childIsBornButtonTitle)}
              onPress={this._childIsBornButtonPressed}
              style={[styles.buttonWithArrow, styles.childIsBornButton]}
              arrow
            />

            <WhiteFormButton
              title={formatMessage(messages.pregnancyLossButtonTitle)}
              onPress={this._pregnancyLossButtonPressed}
              style={styles.buttonWithArrow}
              arrow
            />
          </View>
        ) : null}

        {this.flow === 'ADD' ? (
          <ActionButton
            title={formatMessage(messages.addPregnancyButtonTitle)}
            onPress={this._addPregnancyButtonPressed}
            loader={isRequesting}
            style={styles.button}
          />
        ) : null}
      </ViewWrapper>
    )
  }

  _setChildName = childName =>
    this.setState({ childName, childNameError: null })

  _setChildGender = childGender => this.setState({ childGender })

  _onClear = () => this.setState({ childGender: null })

  _setPregnancyLengthType = pregnancyLengthType =>
    this.setState({ pregnancyLengthType })

  _childIsBornButtonPressed = () =>
    this._goTo('/forms-child', {
      flow: 'IS_BORN',
      pregnancyId: this.props.pregnancy.id,
    })

  _addPregnancyButtonPressed = async () => {
    const {
      childName,
      childGender,
      dueDate,
      pregnancyLengthType,
      isDateBetween,
    } = this.state
    const { formatMessage } = this.context.intl

    // TODO set child gender to "unknows" if missing
    if (childName && dueDate && isDateBetween) {
      this.setState({ isRequesting: true })

      try {
        const pregnancy = await this._(
          PregnanciesActions.add({
            childName:
              childName === formatMessage(messages.defaultBabyName)
                ? PREGNANCY__DEFAULT_BABY_NAME
                : childName,
            childGender,
            dueDate,
            pregnancyLengthType: pregnancyLengthType
              ? pregnancyLengthType
              : '39+6',
          })
        )

        this.setState({ isRequesting: false })

        const { pregnancyWeek } = Pregnancy.getProgress(pregnancy.dueDate)

        this._(
          NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: '/~home',
                action: NavigationActions.navigate({
                  routeName: '/~start-view',
                  action: NavigationActions.navigate({
                    routeName: '/~pregnancy',
                    params: {
                      pregnancyId: pregnancy.id,
                      pregnancyCurrentWeekNumber: pregnancyWeek,
                      activeWeekNumber: pregnancyWeek,
                    },
                  }),
                }),
              }),
            ],
          })
        )
      } catch (err) {
        console.log('err', err)

        this.setState({ isRequesting: false })
      }
    } else if (!childName) {
      this.setState({ childNameError: true, isRequesting: false })
    } else if (!isDateBetween) {
      this.setState({ dueDateError: true, isRequesting: false })
    }
  }

  _setValue = (key, value) =>
    this.setState({
      [key]: value,
      childNameError: null,
    })

  _setDueDate = (value, date, isDateBetween) => {
    this.setState({ dueDate: value, isDateBetween, dueDateError: false })
  }

  _pregnancyLossButtonPressed = () =>
    this._goTo('/pregnancy-loss', {
      id: this.props.pregnancy.id,
    })

  _calculateDueDateLinkPressed = () =>
    this._goTo('/forms-pregnancy-due-date', { onDone: this._setDueDate })

  _onInputFocused = () => {
    const { childName } = this.state
    const { formatMessage } = this.context.intl

    if (childName === formatMessage(messages.defaultBabyName)) {
      this.setState({ childName: '' })
    }
  }

  _doneButtonPressed = async () => {
    const { pregnancy } = this.props
    const {
      childName,
      childGender,
      dueDate,
      pregnancyLengthType,
      isDateBetween,
    } = this.state
    const { formatMessage } = this.context.intl

    if (childName && dueDate && isDateBetween) {
      const updatedPregnancyData = {
        childName:
          childName === formatMessage(messages.defaultBabyName)
            ? PREGNANCY__DEFAULT_BABY_NAME
            : childName,
        childGender: childGender ? childGender : null,
        dueDate: dueDate ? dueDate : '',
        lengthType: pregnancyLengthType ? pregnancyLengthType : '39+6',
      }

      if (updatedPregnancyData) {
        const handleRequest = async () => {
          await this._(
            PregnanciesActions.update(pregnancy, updatedPregnancyData)
          )

          this._goBack()
        }

        try {
          await handleRequest()
        } catch (err) {
          console.log(err)

          this._handleRequestError(handleRequest)
        }
      }
    } else if (!childName) {
      this.setState({ childNameError: true })
    } else if (!isDateBetween) {
      this.setState({ dueDateError: true })
    }
  }

  _repeatRequest = async () => {
    this._connectionErrorDialog.dismiss()

    if (isFunction(this._failedRequestCall)) {
      try {
        await this._failedRequestCall()
      } catch (err) {
        console.log(err)

        this._handleRequestError(this._failedRequestCall)
      }
    }
  }

  _handleRequestError = handleRequest => {
    this._failedRequestCall = handleRequest

    this._connectionErrorDialog.show()
  }

  _endRequest = () => {
    this._failedRequestCall = null

    this._connectionErrorDialog.dismiss()
  }

  _getInitialState = (flow, pregnancy, formatMessage) => {
    switch (flow) {
      case 'EDIT':
        if (pregnancy) {
          return {
            childName:
              pregnancy.childName === PREGNANCY__DEFAULT_BABY_NAME
                ? formatMessage(messages.defaultBabyName)
                : pregnancy.childName,
            childGender: pregnancy.childGender,
            dueDate: pregnancy.dueDate,
            pregnancyLengthType: pregnancy.lengthType,
            isDateBetween: true,
          }
        }

        return {
          childName: '',
          childGender: null,
          dueDate: '',
          isDateBetween: true,
          pregnancyLengthType: '39+6',
        }

      case 'ADD':
        return {
          childName: formatMessage(messages.defaultBabyName),
          childGender: null,
          dueDate: moment()
            .add(266, 'days')
            .format('YYYY-MM-DD'),
          isRequesting: false,
          pregnancyLengthType: '39+6',
          isDateBetween: true,
        }

      default:
        return {}
    }
  }
}
