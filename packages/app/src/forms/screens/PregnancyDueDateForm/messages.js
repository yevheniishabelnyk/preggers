import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Due date',
  firstDayOfTheLastPeriodText: 'Enter the first day of the last period:',
  datePickerTitle: 'Date',
  dayOfTheConceptionText: 'Or enter the day of the conception:',
  resultTitle: 'Based on this, your due date is',
  saveButtonTitle: 'Save',
  daueDateErrorTitle: 'Date is wrong',
})
