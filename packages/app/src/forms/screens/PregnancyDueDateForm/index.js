/**
 *
 * PregnancyDueDateForm
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import ActionButton from '@/settings/components/atoms/ActionButton'
import SettingsDatePicker from '@/settings/components/molecules/SettingsDatePicker'
import FormError from '@/forms/components/atoms/FormError'

import moment from 'moment/min/moment-with-locales'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { isFunction } from 'lodash'

import styles from './styles'

export default class PregnancyDueDateForm extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      lastPeriod: '',
      conception: '',
      dueDate: '',
    }

    this.lastPeriodMinDate = moment()
      .subtract(266, 'days')
      .toDate()
    this.lastPeriodMaxDate = new Date()

    this.conceptionMinDate = moment()
      .subtract(266, 'days')
      .toDate()
    this.conceptionMaxDate = new Date()

    const { locale } = props.screenProps
    moment.locale(locale)
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <FormattedMessage
          {...messages.firstDayOfTheLastPeriodText}
          style={styles.formFieldTitle}
        />

        <SettingsDatePicker
          title={formatMessage(messages.datePickerTitle)}
          onSubmit={this._calculateDueDateByLastPeriod}
          value={this.state.lastPeriod}
          minimumDateLimit={this.lastPeriodMinDate}
          maximumDateLimit={this.lastPeriodMaxDate}
          style={styles.datePicker}
          error={this.state.lastPeriodError}
        />

        <FormError
          isVisible={this.state.lastPeriodError}
          title={formatMessage(messages.daueDateErrorTitle)}
        />

        <FormattedMessage
          {...messages.dayOfTheConceptionText}
          style={[styles.formFieldTitle, styles.conceptionTitle]}
        />

        <SettingsDatePicker
          title={formatMessage(messages.datePickerTitle)}
          onSubmit={this._calculateDueDateByConception}
          value={this.state.conception}
          minimumDateLimit={this.conceptionMinDate}
          maximumDateLimit={this.conceptionMaxDate}
          style={styles.datePicker}
          error={this.state.conceptionError}
        />

        <FormError
          isVisible={this.state.conceptionError}
          title={formatMessage(messages.daueDateErrorTitle)}
        />

        {this.state.dueDate ? (
          <View style={styles.resultBlock}>
            <FormattedMessage
              {...messages.resultTitle}
              style={styles.resultTitle}
            />

            <Text style={styles.result}>
              {moment(this.state.dueDate, 'YYYY-MM-DD').format('DD MMM YYYY')}
            </Text>
          </View>
        ) : null}

        <ActionButton
          title={formatMessage(messages.saveButtonTitle)}
          onPress={this._saveButtonPressed}
          style={styles.saveButton}
        />
      </ViewWrapper>
    )
  }

  _saveButtonPressed = async () => {
    const { dueDate, isDateBetween, lastPeriod } = this.state

    const onDone = this.props.navigation.getParam('onDone')

    if (!isDateBetween) {
      if (lastPeriod) {
        this.setState({ lastPeriodError: true })
      } else {
        this.setState({ conceptionError: true })
      }
    } else if (isFunction(onDone)) {
      onDone(dueDate, null, isDateBetween)

      this._goBack()
    }
  }

  _calculateDueDateByLastPeriod = (value, date, isDateBetween) => {
    const lastPeriod = value
    const conception = ''

    const dueDate = moment(date)
      .add(280, 'days')
      .format('YYYY-MM-DD')

    this.setState({
      dueDate,
      lastPeriod,
      conception,
      isDateBetween,
      lastPeriodError: false,
      conceptionError: false,
    })
  }

  _calculateDueDateByConception = (value, date, isDateBetween) => {
    const conception = value
    const lastPeriod = ''

    const dueDate = moment(date)
      .add(266, 'days')
      .format('YYYY-MM-DD')

    this.setState({
      dueDate,
      conception,
      lastPeriod,
      isDateBetween,
      lastPeriodError: false,
      conceptionError: false,
    })
  }
}
