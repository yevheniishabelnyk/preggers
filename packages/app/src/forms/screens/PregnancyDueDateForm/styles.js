import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: moderateScale(22.5, 2.5),
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  formFieldTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15, resizeFactor),
    letterSpacing: 0.8,
    color: 'rgb(59,72,89)',
    textAlign: 'left',
  },

  saveButton: {
    marginTop: 'auto',
    marginBottom: moderateScale(38, resizeFactor),
  },

  resultBlock: {
    borderRadius: 8,
    backgroundColor: 'white',
    paddingTop: moderateScale(32, resizeFactor),
    paddingBottom: moderateScale(41, resizeFactor),
  },

  resultTitle: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(15, resizeFactor),
    lineHeight: moderateScale(42, resizeFactor),
    color: 'rgb(43,56,87)',
    textAlign: 'center',
  },

  result: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(25, resizeFactor),
    lineHeight: moderateScale(42, resizeFactor),
    color: 'rgb(43,56,87)',
    textAlign: 'center',
  },

  datePicker: {
    marginTop: moderateScale(17, resizeFactor),
    marginBottom: moderateScale(13, resizeFactor),
  },

  conceptionTitle: {
    marginTop: moderateScale(15, resizeFactor),
  },
})

export default styles
