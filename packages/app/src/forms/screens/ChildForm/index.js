/**
 *
 * ChildForm
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { TouchableOpacity, View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import FormInput from '@/settings/components/molecules/FormInput'
import FormSelectWithClear from '@/settings/components/molecules/FormSelectWithClear'
import SettingsWeightInput from '@/settings/components/molecules/SettingsWeightInput'
import SettingsDatePicker from '@/settings/components/molecules/SettingsDatePicker'
import CameraButton from '@/forms/components/atoms/CameraButton'
import RemoveChildButton from '@/forms/components/atoms/RemoveChildButton'
import FormError from '@/forms/components/atoms/FormError'

import { PREGNANCY__DEFAULT_BABY_NAME } from '@/app/constants/BabyNames'

import convertUnits from 'convert-units'

import * as PregnanciesActions from '@/app/database/actions/pregnancies'
import * as ChildrenActions from '@/app/database/actions/children'
import * as RouterActions from '@/app/redux/router/actions'

import Prompt from '@/shared/components/Prompt'
import PopupDialog, {
  SlideAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog'

import cropImage from '@/shared/utils/cropImage'

import moment from 'moment'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { getChild, getPregnancy } from '@/app/redux/selectors'

import { isFunction, round, isEmpty } from 'lodash'

import { ExpoImage } from '@/vendor/cache-manager'

import styles from './styles'

import { firebaseConnect } from 'react-redux-firebase'

import {
  TIME_FORMAT as DATABASE_TIME_FORMAT,
  WEIGHT_UNITS as DATABASE_WEIGHT_UNITS,
  LENGTH_UNITS as DATABASE_LENGTH_UNITS,
} from '@/app/constants/databaseDataFormats'

@firebaseConnect(props => {
  const flow = props.navigation.getParam('flow', 'ADD')

  switch (flow) {
    case 'EDIT': {
      const childId = props.navigation.getParam('childId')

      if (childId) {
        return [`children/${childId}`]
      }

      break
    }

    case 'IS_BORN': {
      const pregnancyId = props.navigation.getParam('pregnancyId')

      if (pregnancyId) {
        return [`pregnancies/${pregnancyId}`]
      }

      break
    }
  }
})
@connect((state, props) => {
  const flow = props.navigation.getParam('flow', 'ADD')
  const settingsWeightUnits = state.settings.weightUnits
  const settingsLengthUnits = state.settings.lengthUnits

  switch (flow) {
    case 'EDIT': {
      const childId = props.navigation.getParam('childId')

      const child = getChild(state, childId)

      return {
        child,
        isFetching: child === null,
        settingsWeightUnits,
        settingsLengthUnits,
      }
    }

    case 'IS_BORN': {
      const pregnancyId = props.navigation.getParam('pregnancyId')

      const pregnancy = getPregnancy(state, pregnancyId)

      return {
        pregnancy,
        isFetching: pregnancy === null,
        settingsWeightUnits,
        settingsLengthUnits,
      }
    }
  }

  return {
    isFetching: false,
    settingsWeightUnits,
    settingsLengthUnits,
  }
})
export default class ChildForm extends BaseScreen {
  static propTypes = {
    child: PropTypes.object,
    pregnancy: PropTypes.object,
    isFetching: PropTypes.bool.isRequired,
    settingsWeightUnits: PropTypes.string,
    settingsLengthUnits: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props, context) {
    super(props)

    const { formatMessage } = context.intl

    this.flow = props.navigation.getParam('flow', 'ADD')

    this.genderTypes = [
      { label: formatMessage(messages.girl), value: 'f' },
      { label: formatMessage(messages.boy), value: 'm' },
    ]

    this.headerTitle =
      this.flow === 'EDIT'
        ? formatMessage(messages.editTitle)
        : formatMessage(messages.addTitle)

    const { locale } = props.screenProps

    switch (locale) {
      case 'en':
        this.birthTimeFormat = 'hh:mm A'
        break
      case 'sv':
        this.birthTimeFormat = 'HH:mm'
        break
      default:
        this.birthTimeFormat = DATABASE_TIME_FORMAT
    }

    this.birthdayMaxDate = moment().format('YYYY-MM-DD')

    this.state = this._getInitialState(
      this.flow,
      props.child,
      props.pregnancy,
      context.intl.formatMessage
    )
  }

  render() {
    const { isFetching, settingsWeightUnits, settingsLengthUnits } = this.props
    const { formatMessage } = this.context.intl
    const { workInProgress } = this.state

    const photoTitle = this.state.image
      ? messages.changePhotoButtonTitle
      : messages.addPhotoButtonTitle

    const onDone = !isFetching ? this._doneButtonPressed : null

    const lengthUnitsTitle =
      settingsLengthUnits && settingsLengthUnits !== DATABASE_LENGTH_UNITS
        ? settingsLengthUnits
        : DATABASE_LENGTH_UNITS

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        scrollable
        keyboard
        behavior="padding"
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={this.headerTitle}
              onBackButtonPress={this._goBack}
              onDoneButtonPress={onDone}
              style={styles.header}
              workInProgress={workInProgress}
            />
          ),

          popups: [
            <PopupDialog
              key="childIsBornDialog"
              ref={ref => (this._childIsBornDialog = ref)}
              dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
              dialogStyle={styles.dialog}
              overlayOpacity={0.7}
            >
              <Prompt
                text={formatMessage(messages.childIsBornDialogQuestion)}
                negativeButton={{
                  title: formatMessage(messages.noButtonTitle),
                  onPress: () => this._childIsBornDialog.dismiss(),
                }}
                positiveButton={{
                  title: formatMessage(messages.yesButtonTitle),
                  onPress: this._setChildIsBorn,
                }}
              />
            </PopupDialog>,

            <PopupDialog
              key="removeChildDialog"
              ref={ref => (this._removeChildDialog = ref)}
              dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
              dialogStyle={styles.dialog}
              overlayOpacity={0.7}
            >
              <Prompt
                text={formatMessage(messages.removeChildDialogQuestion)}
                negativeButton={{
                  title: formatMessage(messages.noButtonTitle),
                  onPress: () => this._removeChildDialog.dismiss(),
                }}
                positiveButton={{
                  title: formatMessage(messages.yesButtonTitle),
                  onPress: this._removeChild,
                }}
              />
            </PopupDialog>,

            <PopupDialog
              key="connectionErrorDialog"
              ref={ref => (this._connectionErrorDialog = ref)}
              dialogAnimation={new FadeAnimation({ animationDuration: 200 })}
              dialogStyle={styles.dialog}
              overlayOpacity={0.7}
            >
              <Prompt
                text={formatMessage(messages.connectionErrorTitle)}
                negativeButton={{
                  title: formatMessage(messages.noButtonTitle),
                  onPress: this._endRequest,
                }}
                positiveButton={{
                  title: formatMessage(messages.yesButtonTitle),
                  onPress: this._repeatRequest,
                }}
              />
            </PopupDialog>,
          ],
        }}
        headerHeight={SettingsHeader.height}
      >
        {!isFetching ? (
          <View style={styles.content}>
            <View style={styles.changePhoto}>
              {this.state.image ? (
                <TouchableOpacity
                  style={styles.photoContainer}
                  onPress={this._changePhotoButtonPressed}
                  activeOpacity={0.95}
                >
                  <ExpoImage source={this.state.image} style={styles.photo} />
                </TouchableOpacity>
              ) : (
                <CameraButton onPress={this._changePhotoButtonPressed} />
              )}

              <TouchableOpacity
                style={styles.changePhotoButtonTitleContainer}
                onPress={this._changePhotoButtonPressed}
                activeOpacity={0.95}
              >
                <FormattedMessage
                  {...photoTitle}
                  style={styles.changePhotoTitle}
                />
              </TouchableOpacity>
            </View>

            <FormInput
              title={formatMessage(messages.childNameInputTitle)}
              value={this.state.name}
              onChange={this._setChildName}
              style={styles.formField}
              error={this.state.childNameError}
              autoCorrect={false}
              returnKeyType="done"
            />

            <FormError
              isVisible={this.state.childNameError}
              title={formatMessage(messages.childNameErrorTitle)}
            />

            {settingsWeightUnits !== DATABASE_WEIGHT_UNITS ? (
              <SettingsWeightInput
                title={formatMessage(messages.birthWeightInputTitle)}
                outputFormat="lb [lb] oz [oz]"
                placeholder="lb"
                valueUnits={DATABASE_WEIGHT_UNITS}
                value={this.state.birthWeight}
                onChange={this._setBirthWeight}
                style={styles.formField}
              />
            ) : (
              <FormInput
                title={formatMessage(messages.birthWeightInputTitle)}
                value={this.state.birthWeight}
                onChange={this._setBirthWeight}
                style={styles.formField}
                units="g"
                keyboardType="numeric"
                clearButtonMode="never"
                returnKeyType="done"
              />
            )}

            <FormInput
              title={formatMessage(messages.birthLengthInputTitle)}
              value={this.state.birthLength}
              onChange={this._setBirthLength}
              style={styles.formField}
              units={lengthUnitsTitle.toLowerCase()}
              clearButtonMode="never"
              keyboardType="numeric"
              returnKeyType="done"
            />

            <FormSelectWithClear
              title={formatMessage(messages.genderInputTitle)}
              value={this.state.gender}
              items={this.genderTypes}
              placeholder="Select"
              onSelect={this._setChildGender}
              onClear={this._onClear}
              style={styles.formField}
              error={this.state.childGenderError}
            />

            <FormError
              isVisible={this.state.childGenderError}
              title={formatMessage(messages.childGenderErrorTitle)}
            />

            <SettingsDatePicker
              title={formatMessage(messages.birthdayDatePickerTitle)}
              onSubmit={this._setChildBirthday}
              value={this.state.birthday}
              style={styles.formField}
              maximumDate={this.birthdayMaxDate}
              error={this.state.maxDateError}
            />

            <FormError
              isVisible={this.state.maxDateError}
              title={formatMessage(messages.maxDateErrorTitle)}
            />

            <SettingsDatePicker
              title={formatMessage(messages.birthTimeDatePickerTitle)}
              mode="time"
              format={this.birthTimeFormat}
              onSubmit={this._setChildBirthTime}
              value={this.state.birthTime}
              style={[
                styles.formField,
                this.flow !== 'EDIT' ? styles.lastFormField : null,
              ]}
            />

            {this.flow === 'EDIT' ? (
              <RemoveChildButton
                onPress={this._removeChildButtonPressed}
                style={styles.deleteButton}
                title={formatMessage(messages.removeChildButtonTitle)}
              />
            ) : null}
          </View>
        ) : null}
      </ViewWrapper>
    )
  }

  _setChildName = name => this.setState({ name, childNameError: null })

  _setBirthLength = birthLength => {
    const metricNumber = birthLength.replace(/,/g, '.')

    this.setState({ birthLength: metricNumber })
  }

  _setBirthWeight = (birthWeight = '') => {
    const { settingsWeightUnits } = this.props
    let metricNumber = birthWeight

    if (settingsWeightUnits !== 'lb') {
      metricNumber = birthWeight.replace(/,/g, '.')
    }

    this.setState({ birthWeight: metricNumber })
  }

  _setChildGender = gender =>
    this.setState({
      gender,
      childGenderError: null,
    })

  _onClear = () => this.setState({ childGender: null })

  _setChildBirthday = birthday => this.setState({ birthday })

  _setChildBirthTime = birthTime => this.setState({ birthTime })

  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.child && nextProps.child) ||
      (!this.props.pregnancy && nextProps.pregnancy)
    ) {
      const initialState = this._getInitialState(
        this.flow,
        nextProps.child,
        nextProps.pregnancy,
        this.context.intl.formatMessage
      )

      this.setState(initialState)
    }
  }

  _doneButtonPressed = () => {
    const { name, birthday } = this.state

    if (name && birthday <= this.birthdayMaxDate) {
      switch (this.flow) {
        case 'IS_BORN':
          this._childIsBornDialog.show()
          break

        case 'EDIT':
          this._updateChild()
          break

        case 'ADD':
          this._createChild()
          break
      }
    } else if (!name) {
      this.setState({ childNameError: !name })
    } else if (birthday > this.birthdayMaxDate) {
      this.setState({ maxDateError: true })
    }
  }

  _repeatRequest = async () => {
    this._connectionErrorDialog.dismiss()

    if (isFunction(this._failedRequestCall)) {
      try {
        await this._failedRequestCall()
      } catch (err) {
        console.log(err)

        this._handleRequestError(this._failedRequestCall)
      }
    }
  }

  _handleRequestError = handleRequest => {
    this._failedRequestCall = handleRequest

    this._connectionErrorDialog.show()
  }

  _endRequest = () => {
    this._failedRequestCall = null

    this._connectionErrorDialog.dismiss()
    this._childIsBornDialog.dismiss()
  }

  _updateChild = async () => {
    const { child } = this.props
    const {
      workInProgress,
      gender,
      name,
      birthday,
      birthWeight,
      birthLength,
    } = this.state

    if (!workInProgress) {
      this.setState({ workInProgress: true })

      const updatedData = {}

      if (name && (!child.name || child.name !== name)) {
        updatedData.name = name
      }

      if (gender && (!child.gender || child.gender !== gender)) {
        updatedData.gender = gender
      } else if (gender === undefined) {
        updatedData.gender = null
      }

      if (birthday && (!child.birthday || child.birthday !== birthday)) {
        updatedData.birthday = birthday
      }

      if (this.state.birthTime) {
        let birthTime = this.state.birthTime

        if (this.birthTimeFormat !== DATABASE_TIME_FORMAT) {
          birthTime = moment(birthTime, this.birthTimeFormat).format(
            DATABASE_TIME_FORMAT
          )
        }

        if (!child.birthTime || child.birthTime !== birthTime) {
          updatedData.birthTime = birthTime
        }
      }

      if (
        (!child.birthWeight && Number(birthWeight)) ||
        (child.birthWeight && !birthWeight) ||
        (child.birthWeight &&
          birthWeight &&
          child.birthWeight !== Number(birthWeight))
      ) {
        updatedData.birthWeight = birthWeight ? Number(birthWeight) : null
      }

      if (
        (!child.birthLength && Number(birthLength)) ||
        (child.birthLength && !birthLength) ||
        (child.birthLength &&
          birthLength &&
          child.birthLength !== Number(birthLength))
      ) {
        updatedData.birthLength = this._checkForAppLengthUnits() || null
      }

      if (this.state.isImageChanged) {
        updatedData.image = {
          type: 'file',
          uri: this.state.image.uri,
        }
      }

      if (!isEmpty(updatedData)) {
        const handleRequest = async () => {
          await this._(ChildrenActions.update(child.id, updatedData))

          if (this.state.isImageChanged) {
            this._(ChildrenActions.updatePicture(child, this.state.image))
          }

          this._(
            RouterActions.resetToUrl([
              `/~home/~start-view/child?childId=${child.id}`,
            ])
          )
        }

        try {
          await handleRequest()
        } catch (err) {
          console.info('err: ', err)

          this.setState({ workInProgress: false })

          this._handleRequestError(handleRequest)
        }
      } else {
        this._(
          RouterActions.resetToUrl([
            `/~home/~start-view/child?childId=${child.id}`,
          ])
        )
      }
    }
  }

  _checkForAppLengthUnits = () => {
    const { settingsLengthUnits } = this.props
    const { birthLength } = this.state

    let newValue = null

    if (birthLength) {
      if (settingsLengthUnits !== DATABASE_LENGTH_UNITS) {
        newValue = round(
          convertUnits(Number(birthLength))
            .from(settingsLengthUnits)
            .to(DATABASE_LENGTH_UNITS),
          2
        )
      } else {
        newValue = Number(birthLength)
      }
    }

    return newValue
  }

  _createChild = async () => {
    const { workInProgress } = this.state

    if (!workInProgress) {
      this.setState({ workInProgress: true })

      const handleRequest = async () => {
        let birthTime = this.state.birthTime

        if (this.birthTimeFormat !== DATABASE_TIME_FORMAT) {
          birthTime = moment(birthTime, this.birthTimeFormat).format(
            DATABASE_TIME_FORMAT
          )
        }

        const child = await this._(
          ChildrenActions.add({
            name: this.state.name,
            birthday: this.state.birthday,
            birthTime,
            birthWeight: this.state.birthWeight
              ? Number(this.state.birthWeight)
              : null,
            birthLength: this._checkForAppLengthUnits(),
            gender: this.state.gender,
            image: this.state.image
              ? { type: 'file', uri: this.state.image.uri }
              : null,
          })
        )

        this._(
          RouterActions.resetToUrl([
            `/~home/~start-view/child?childId=${child.id}`,
          ])
        )

        if (this.state.image) {
          this._(ChildrenActions.updatePicture(child, this.state.image))
        }
      }

      try {
        await handleRequest()
      } catch (err) {
        console.info('err: ', err)

        this.setState({ workInProgress: false })

        this._handleRequestError(handleRequest)
      }
    }
  }

  _setChildIsBorn = async () => {
    const { pregnancy } = this.props
    const { workInProgress } = this.state

    if (!workInProgress) {
      if (pregnancy) {
        this.setState({ workInProgress: true })

        const handleRequest = async () => {
          let birthTime = this.state.birthTime

          if (this.birthTimeFormat !== DATABASE_TIME_FORMAT) {
            birthTime = moment(birthTime, this.birthTimeFormat).format(
              DATABASE_TIME_FORMAT
            )
          }

          const image = this.state.image
            ? { type: 'file', uri: this.state.image.uri }
            : null

          const childData = {
            name: this.state.name,
            gender: this.state.gender,
            birthday: this.state.birthday,
            birthTime,
            birthWeight: this.state.birthWeight
              ? Number(this.state.birthWeight)
              : null,
            birthLength: this._checkForAppLengthUnits(),
            image,
            birthPhoto: image,
          }

          const bornChild = await this._(
            PregnanciesActions.childIsBorn(childData, pregnancy)
          )

          this._(
            RouterActions.resetToUrl([
              `/~home/~start-view/child?childId=${bornChild.id}`,
            ])
          )

          if (this.state.image) {
            const withBirthPhoto = true

            this._(
              ChildrenActions.updatePicture(
                bornChild,
                this.state.image,
                withBirthPhoto
              )
            )
          }
        }

        try {
          await handleRequest()
        } catch (err) {
          console.info('err: ', err)

          this.setState({ workInProgress: false })

          this._handleRequestError(handleRequest)
        }
      }
    }
  }

  _removeChild = async () => {
    const handleRequest = async () => {
      const { child } = this.props

      await this._(ChildrenActions.remove(child))

      this._goBack()
    }

    try {
      await handleRequest()
    } catch (err) {
      console.info('err: ', err)

      this._handleRequestError(handleRequest)
    }
  }

  _handleImagePicked = async data => {
    if (data && data.image) {
      this.setState({
        image: { type: 'file', ...data.image },
        isImageChanged: true,
      })

      try {
        const cropResult = await cropImage(data.image, {
          type: 'square',
          width: 200,
        })

        this.setState({
          image: cropResult,
        })
      } catch (err) {
        console.log(err)
      }
    }
  }

  _changePhotoButtonPressed = () =>
    this._goTo('/take-photo', { onDone: this._handleImagePicked })

  _removeChildButtonPressed = () => this._removeChildDialog.show()

  _getInitialState = (flow, child, pregnancy, formatMessage) => {
    const { settingsLengthUnits } = this.props

    switch (flow) {
      case 'EDIT':
        if (child) {
          let birthLength

          if (child.birthLength) {
            if (settingsLengthUnits !== DATABASE_LENGTH_UNITS) {
              birthLength = round(
                convertUnits(child.birthLength)
                  .from(DATABASE_LENGTH_UNITS)
                  .to(settingsLengthUnits),
                2
              ).toString()
            } else {
              birthLength = round(child.birthLength, 2).toString()
            }
          }

          return {
            name: child.name,
            gender: child.gender,
            birthTime: child.birthTime || '',
            birthWeight: child.birthWeight
              ? Math.floor(child.birthWeight).toString()
              : null,
            birthLength,
            image: child.image || null,
            birthday: child.birthday,
            birthdayDate: moment(child.birthday, 'YYYY-MM-DD').toDate(),
            birthTimeDate: child.birthTime
              ? moment(child.birthday, 'YYYY-MM-DD')
                  .add(Number(child.birthTime.substring(0, 2)), 'hours')
                  .add(Number(child.birthTime.substring(3)), 'minutes')
                  .toDate()
              : new Date(),
          }
        }

        return {
          name: '',
          gender: null,
          birthWeight: null,
          birthLength: null,
          image: null,
          birthday: '',
          birthTime: '',
        }

      case 'ADD':
        return {
          name: formatMessage(messages.defaultBabyName),
          gender: '',
          birthWeight: null,
          birthLength: null,
          image: null,
          birthday: moment().format('YYYY-MM-DD'),
          birthTime: moment().format(this.birthTimeFormat),
        }

      case 'IS_BORN':
        return {
          name: pregnancy
            ? pregnancy.childName === PREGNANCY__DEFAULT_BABY_NAME
              ? formatMessage(messages.defaultBabyName)
              : pregnancy.childName
            : formatMessage(messages.defaultBabyName),
          gender: pregnancy ? pregnancy.childGender : null,
          birthWeight: null,
          birthLength: null,
          image: null,
          birthday: moment().format('YYYY-MM-DD'),
          birthTime: moment().format(this.birthTimeFormat),
        }

      default:
        return {}
    }
  }
}
