import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const photoWidth = moderateScale(109, resizeFactor)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    paddingHorizontal: moderateScale(22, 2.5),
    paddingBottom: moderateScale(50, resizeFactor),
    alignItems: 'stretch',
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  formField: {
    marginBottom: moderateScale(13, resizeFactor),
  },

  cameraButton: {
    marginBottom: moderateScale(3, resizeFactor),
  },

  deleteButton: {
    marginTop: moderateScale(37, resizeFactor),
    marginBottom: moderateScale(37, resizeFactor),
  },

  lastFormField: {
    marginBottom: moderateScale(37, resizeFactor),
  },

  dialog: {
    width: moderateScale(330, resizeFactor),
    paddingTop: moderateScale(30, resizeFactor),
    height: 'auto',
    overflow: 'hidden',
  },

  changePhotoWrapper: {
    marginTop: moderateScale(7, resizeFactor),
    alignItems: 'center',
  },

  changePhoto: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: moderateScale(13, resizeFactor),
  },

  changePhotoTitle: {
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(20, resizeFactor),
    textAlign: 'center',
    color: 'rgb(138,142,172)',
    fontFamily: 'Now-Medium',
    marginTop: moderateScale(20, resizeFactor),
  },

  photoContainer: {
    width: photoWidth,
    height: photoWidth,
    borderRadius: photoWidth / 2,
    alignSelf: 'center',
    overflow: 'hidden',
  },

  photo: {
    flex: 1,
    width: null,
    height: null,
  },
})

export default styles
