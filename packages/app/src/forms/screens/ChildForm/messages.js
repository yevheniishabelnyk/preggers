import { defineMessages } from 'react-intl'

export default defineMessages({
  childIsBornDialogQuestion:
    'Do you really want to change your pregnancy to child?',
  noButtonTitle: 'No',
  yesButtonTitle: 'Yes',
  removeChildDialogQuestion:
    'Do you really want to delete your child from the app?',
  childNameInputTitle: 'Childs name',
  birthWeightInputTitle: 'Birth weight',
  birthLengthInputTitle: 'Birth length',
  genderInputTitle: 'Gender',
  birthdayDatePickerTitle: 'Birthday',
  birthTimeDatePickerTitle: 'Time',
  removeChildButtonTitle: 'Remove child',
  addPhotoButtonTitle: 'Add photo',
  changePhotoButtonTitle: 'Change photo',
  girl: 'Girl',
  boy: 'Boy',
  editTitle: 'Edit child',
  addTitle: 'Add child',
  childNameErrorTitle: 'Please enter childs name',
  childGenderErrorTitle: 'Please choose child gender',
  maxDateErrorTitle: 'Date can not be in the future',
  connectionErrorTitle:
    'Sorry, something went wrong. Do you want to try again?',
  defaultBabyName: 'My baby',
})
