/*
 *
 * Forms screens
 *
 */

import PregnancyForm from './screens/PregnancyForm'
import ChildForm from './screens/ChildForm'
import PregnancyDueDateForm from './screens/PregnancyDueDateForm'

export default {
  '/forms-pregnancy': {
    screen: PregnancyForm,
  },

  '/forms-child': {
    screen: ChildForm,
  },

  '/forms-pregnancy-due-date': {
    screen: PregnancyDueDateForm,
  },
}
