import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const iconWidth = moderateScale(26, resizeFactor)

const closeButtonWidth = moderateScale(39, resizeFactor)
const gutter = moderateScale(15, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    borderRadius: 100,
    backgroundColor: 'rgb(240,240,247)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: gutter,
    overflow: 'hidden',
    position: 'relative',
  },

  icon: {
    width: iconWidth,
    height: iconWidth,
    marginRight: moderateScale(13, resizeFactor),
  },

  input: {
    paddingVertical: moderateScale(13, resizeFactor),
    flex: 1,
    alignSelf: 'center',
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },

  button: {
    height: closeButtonWidth,
    width: closeButtonWidth,
    marginRight: -gutter,
  },
})

export default styles
