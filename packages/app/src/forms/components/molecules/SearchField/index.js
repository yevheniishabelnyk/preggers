/**
 *
 * SearchField
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, TextInput } from 'react-native'

import styles from './styles'

export default class SearchField extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    returnKeyType: PropTypes.string,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { placeholder, value, onChange, style, returnKeyType } = this.props

    return (
      <View style={[styles.container, style]}>
        <Image
          source={require('assets/icons/search.png')}
          style={styles.icon}
        />

        <TextInput
          underlineColorAndroid="transparent"
          placeholder={placeholder}
          placeholderTextColor="#8392a7"
          multiline={false}
          onChangeText={onChange}
          value={value}
          style={styles.input}
          clearButtonMode="always"
          returnKeyType={returnKeyType}
        />
      </View>
    )
  }
}
