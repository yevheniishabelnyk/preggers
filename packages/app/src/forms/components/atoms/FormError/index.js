/**
 *
 * FormError
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Animated, Easing } from 'react-native'

import styles, { lineHeight, marginBottom } from './styles'

export default class FormError extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    isVisible: PropTypes.bool,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  state = { displayErrorMessage: 'none' }

  render() {
    const { title, style } = this.props

    const animatedStyle = {
      display: this.state.displayErrorMessage,
      lineHeight: this.lineHeightValue,
      marginBottom: this.lineHeightValue.interpolate({
        inputRange: [0, lineHeight],
        outputRange: [0, marginBottom],
      }),
    }

    return (
      <Animated.Text style={[styles.container, animatedStyle, style]}>
        {title}
      </Animated.Text>
    )
  }

  componentWillMount() {
    this.lineHeightValue = new Animated.Value(0.01)
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isVisible && nextProps.isVisible) {
      this.setState({ displayErrorMessage: 'flex' })
      this._show()
    }

    if (this.props.isVisible && !nextProps.isVisible) {
      this._hide()
    }
  }

  _show = () => {
    Animated.timing(this.lineHeightValue, {
      toValue: lineHeight,
      duration: 300,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start()
  }

  _hide = () => {
    Animated.timing(this.lineHeightValue, {
      toValue: 0.01,
      duration: 300,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start(this._hideTextOnAnimationComplete())
  }

  _hideTextOnAnimationComplete = () => {
    this.setState({ displayErrorMessage: 'none' })
  }
}
