import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

export const lineHeight = moderateScale(20, resizeFactor)

export const marginBottom = moderateScale(10, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    marginLeft: moderateScale(20, resizeFactor),
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(11, resizeFactor),
    color: 'rgb(250,65,105)',
  },
})

export default styles
