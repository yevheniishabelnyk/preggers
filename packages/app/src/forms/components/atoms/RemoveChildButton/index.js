/**
 *
 * RemoveChildButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableOpacity } from 'react-native'

import styles from './styles'

export default class RemoveChildButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    title: PropTypes.string,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { onPress, style, title } = this.props

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <View style={styles.content}>
          <View style={styles.iconContainer}>
            <Image
              style={styles.icon}
              source={require('assets/icons/trash.png')}
            />
          </View>

          <View style={styles.titleContainer}>
            <Text style={styles.title}>{title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}
