import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: moderateScale(60, resizeFactor),
    display: 'flex',
    alignSelf: 'stretch',
    borderRadius: moderateScale(30, resizeFactor),
    overflow: 'hidden',
  },

  content: {
    flex: 1,
    position: 'relative',
    alignSelf: 'stretch',
  },

  iconContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    paddingLeft: moderateScale(16, resizeFactor),
  },

  titleContainer: {
    ...StyleSheet.absoluteFillObject,
    zIndex: -1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: 'rgb(112,52,153)',
  },

  icon: {
    width: moderateScale(25, resizeFactor),
    height: moderateScale(31, resizeFactor),
  },
})

export default styles
