import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const buttonWidth = moderateScale(109, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },

  icon: {
    width: moderateScale(40, resizeFactor),
    height: moderateScale(33, resizeFactor),
  },
})

export default styles
