/**
 *
 * CameraButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import cameraIcon from 'assets/icons/camera.png'

import styles from './styles'

export default class CameraButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { onPress, style } = this.props

    return (
      <TouchableOpacity
        style={[styles.button, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Image style={styles.icon} source={cameraIcon} />
      </TouchableOpacity>
    )
  }
}
