/**
 *
 * Start View Navigator
 *
 */

import { TabNavigator } from 'react-navigation'

import PregnancyNavigator from '@/pregnancy/navigator'
import Child from '@/children/screens/Child'
import MissingPregnancy from '@/start-view/screens/MissingPregnancy'
import TabBar from './components/organisms/TabBar'

export default TabNavigator(
  {
    '/~pregnancy': {
      screen: PregnancyNavigator,
    },

    '/child': {
      screen: Child,
    },

    '/missing-pregnancy': {
      screen: MissingPregnancy,
    },
  },
  {
    navigationOptions: {
      gesturesEnabled: false,
    },
    animationEnabled: false,
    tabBarPosition: 'top',
    tabBarComponent: TabBar,
  }
)
