/**
 *
 * WeekBar
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, FlatList, Platform } from 'react-native'
import WeekBarArrow from '@/start-view/components/atoms/WeekBarArrow'
import WeekBarItem from '@/start-view/components/atoms/WeekBarItem'

import { BoxShadow } from '@/vendor/react-native-shadow'

import styles, {
  containerHeight,
  containerShadowLeftArrow,
  containerShadowRightArrow,
} from './styles'

import { moderateScale } from '@/app/scaling'

import { range, isFunction } from 'lodash'

export default class WeekBar extends React.Component {
  static propTypes = {
    onWeekChange: PropTypes.func,
    initialWeek: PropTypes.number,
    minWeek: PropTypes.number,
    maxWeek: PropTypes.number,
    style: PropTypes.any,
  }

  static height = containerHeight

  static defaultProps = {
    initialWeek: 3,
    minWeek: 3,
    maxWeek: 42,
  }

  constructor(props) {
    super(props)

    this.state = {
      data: range(props.minWeek, props.maxWeek + 1).map(item => ({
        index: item,
        active: item == props.initialWeek,
      })),
      activeWeek: props.initialWeek,
      scrollEnabled: false,
      dragEnabled: false,
    }

    this.initialScrollIndex = props.initialWeek - props.minWeek

    if (props.initialWeek < props.minWeek) {
      this.initialScrollIndex = props.minWeek - props.minWeek
    }

    if (props.initialWeek > props.maxWeek) {
      this.initialScrollIndex = props.maxWeek - props.minWeek
    }
  }

  render() {
    const { style } = this.props

    return (
      <View style={[styles.container, style]}>
        {Platform.OS === 'android' ? (
          <View style={styles.arrow}>
            <BoxShadow setting={containerShadowLeftArrow}>
              <WeekBarArrow onPress={this._prevWeek} />
            </BoxShadow>
          </View>
        ) : (
          <WeekBarArrow onPress={this._prevWeek} />
        )}

        <FlatList
          horizontal
          ref={ref => (this._weeks = ref)}
          style={styles.weeks}
          initialScrollIndex={this.initialScrollIndex}
          keyExtractor={this._extractListKey}
          data={this.state.data}
          showsHorizontalScrollIndicator={false}
          onPressItem={this._onPressItem}
          renderItem={this._renderItem}
          getItemLayout={this._getItemLayout}
          onScrollBeginDrag={this._onScrollBeginDrag}
          onScrollEndDrag={this._onScrollEndDrag}
          onMomentumScrollBegin={this._onMomentumScrollBegin}
          onMomentumScrollEnd={this._onScroll}
        />

        {Platform.OS === 'android' ? (
          <View style={styles.arrow}>
            <BoxShadow setting={containerShadowRightArrow}>
              <WeekBarArrow right onPress={this._nextWeek} />
            </BoxShadow>
          </View>
        ) : (
          <WeekBarArrow right onPress={this._nextWeek} />
        )}
      </View>
    )
  }

  _extractListKey(item, index) {
    return `flat-list-item-${index}`
  }

  _onScrollBeginDrag = () => this.setState({ dragEnabled: true })

  _onMomentumScrollBegin = () => this.setState({ scrollEnabled: true })

  _onScroll = event => {
    const { minWeek, maxWeek } = this.props
    const index = Math.round(
      event.nativeEvent.contentOffset.x / moderateScale(135, 1)
    )

    if (index >= 0 && index <= maxWeek - minWeek) {
      const diff = event.nativeEvent.targetContentOffset
        ? event.nativeEvent.targetContentOffset.x -
          event.nativeEvent.contentOffset.x
        : null

      if (diff === 0 && this.state.dragEnabled) {
        this._weeks.scrollToIndex({
          animated: true,
          index,
        })
      } else if (this.state.scrollEnabled) {
        this._weeks.scrollToIndex({
          animated: true,
          index,
        })

        this._setActiveWeek(index + minWeek)
      }
    }

    this.setState({ scrollEnabled: false, dragEnabled: false })
  }

  _getItemLayout = (data, index) => ({
    length: moderateScale(135, 1),
    offset: moderateScale(135, 1) * index,
    index,
  })

  _renderItem = ({ item: { index: week, active } }) => (
    <WeekBarItem
      week={week}
      onSelect={this._setActiveWeek}
      last={week === this.props.maxWeek}
      active={active}
    />
  )

  _prevWeek = () => {
    const prevWeek = this.state.activeWeek - 1

    if (prevWeek >= this.props.minWeek) {
      // if (prevWeek !== this.props.minWeek) {
      this._weeks.scrollToIndex({
        animated: true,
        index: prevWeek - 3,
      })
      // }

      this.setState(prevState => ({
        ...prevState,
        data: prevState.data.map(item => {
          if (item.index === prevWeek) {
            return { ...item, active: true }
          }
          if (item.active) {
            return { ...item, active: false }
          }
          return item
        }),
        activeWeek: prevWeek,
      }))

      if (isFunction(this.props.onWeekChange)) {
        this.props.onWeekChange(prevWeek)
      }
    }
  }

  _nextWeek = () => {
    const nextWeek = this.state.activeWeek + 1

    if (nextWeek <= this.props.maxWeek) {
      // if (nextWeek !== this.props.maxWeek) {
      this._weeks.scrollToIndex({
        animated: true,
        index: nextWeek - 3,
      })
      // }

      this.setState(prevState => ({
        ...prevState,
        data: prevState.data.map(item => {
          if (item.index === nextWeek) {
            return { ...item, active: true }
          }
          if (item.active) {
            return { ...item, active: false }
          }
          return item
        }),
        activeWeek: nextWeek,
      }))

      if (isFunction(this.props.onWeekChange)) {
        this.props.onWeekChange(nextWeek)
      }
    }
  }

  _setActiveWeek = (week, withCallBack = true) => {
    if (week >= this.props.minWeek && week <= this.props.maxWeek) {
      this.setState(prevState => ({
        ...prevState,
        data: prevState.data.map(item => {
          if (item.index === week) {
            return { ...item, active: true }
          }
          if (item.active) {
            return { ...item, active: false }
          }
          return item
        }),
        activeWeek: week,
      }))

      // if (week !== this.props.maxWeek && week !== this.props.minWeek) {
      setTimeout(() => {
        this._weeks.scrollToIndex({
          animated: true,
          index: week - 3,
        })
      }, 0)
      // }

      if (withCallBack && isFunction(this.props.onWeekChange)) {
        this.props.onWeekChange(week)
      }
    }
  }

  setWeek = week => {
    this._setActiveWeek(week, false)
  }
}
