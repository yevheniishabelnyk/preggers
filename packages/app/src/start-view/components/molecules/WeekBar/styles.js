import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

export const containerHeight = moderateScale(55, 0.7)

const contatinerWidth = moderateScale(72, resizeFactor)

export const containerShadowLeftArrow = {
  width: contatinerWidth,
  height: containerHeight,
  color: '#000000',
  radius: moderateScale(27, resizeFactor),
  opacity: Platform.OS === 'android' ? 0.035 : 0.025,
  border: moderateScale(8),
  x: Platform.OS === 'android' ? 3 : 0,
  y: 0,
}

export const containerShadowRightArrow = {
  width: contatinerWidth,
  height: containerHeight,
  color: '#000000',
  radius: moderateScale(27, resizeFactor),
  opacity: Platform.OS === 'android' ? 0.035 : 0.025,
  border: moderateScale(8),
  x: 0,
  y: 0,
}

const styles = StyleSheet.create({
  container: {
    height:
      Platform.OS === 'android'
        ? moderateScale(75, resizeFactor)
        : containerHeight,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'stretch',
    paddingHorizontal: moderateScale(11, resizeFactor),
    marginTop: moderateScale(5, resizeFactor),
    marginBottom:
      Platform.OS === 'android'
        ? moderateScale(3, resizeFactor)
        : moderateScale(15, resizeFactor),
    marginLeft:
      Platform.OS === 'android' ? moderateScale(-55, resizeFactor) : 0,
    marginRight:
      Platform.OS === 'android' ? moderateScale(-55, resizeFactor) : 0,
  },

  weeks: {
    flex: 1,
    flexDirection: 'row',
    paddingLeft: moderateScale(92, 1.1),
    marginTop: Platform.OS === 'android' ? moderateScale(-8, resizeFactor) : 0,
  },

  arrow: {
    marginTop: moderateScale(7),
  },
})

export default styles
