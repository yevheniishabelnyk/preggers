import { StyleSheet, Dimensions, Platform } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const containerHeight = moderateScale(138, resizeFactor)

const styles = StyleSheet.create({
  container: {
    maxHeight: moderateScale(420, resizeFactor),
    width,
    position: 'absolute',
    top: Platform.OS === 'android' ? moderateScale(90, resizeFactor) : '100%',
    backgroundColor: 'white',
  },

  childRow: {
    backgroundColor: 'white',
    height: moderateScale(70, resizeFactor),
    borderWidth: 1,
    borderColor: '#EEF0F3',
  },

  childRowContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: moderateScale(26, resizeFactor),
  },

  childName: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: '#484E62',
    letterSpacing: 0,
  },

  addChildButton: {
    backgroundColor: 'white',
    height: containerHeight / 2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: moderateScale(26, resizeFactor),
    shadowOpacity: 0.5,
    shadowRadius: 6,
    shadowColor: '#E6E8F1',
    shadowOffset: { height: 5, width: -2 },
  },

  addChildButtonTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: '#703499',
    letterSpacing: 0,
  },

  checkedIcon: {
    width: moderateScale(21, resizeFactor),
    height: moderateScale(16, resizeFactor),
    marginTop: moderateScale(2, resizeFactor),
    marginLeft: 'auto',
  },
})

export default styles
