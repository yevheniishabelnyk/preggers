/**
 *
 * ChildSelect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableHighlight, ScrollView } from 'react-native'

import checkedIcon from 'assets/icons/checked-emerald.png'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { isFunction } from 'lodash'

import { PREGNANCY__DEFAULT_BABY_NAME } from '@/app/constants/BabyNames'

import styles from './styles'

class ChildRow extends React.Component {
  static propTypes = {
    childName: PropTypes.string,
    onPress: PropTypes.func,
    active: PropTypes.bool,
    child: PropTypes.object,
  }

  render() {
    const { childName, active } = this.props

    return (
      <TouchableHighlight
        style={styles.childRow}
        onPress={this._childPressed}
        underlayColor="white"
      >
        <View style={styles.childRowContainer}>
          <Text style={styles.childName}>
            {childName === PREGNANCY__DEFAULT_BABY_NAME ? (
              <FormattedMessage {...messages.defaultBabyName} />
            ) : (
              childName
            )}
          </Text>

          {active ? (
            <Image source={checkedIcon} style={styles.checkedIcon} />
          ) : null}
        </View>
      </TouchableHighlight>
    )
  }

  _childPressed = () => {
    const { child, onPress } = this.props

    if (isFunction(onPress)) {
      onPress(child)
    }
  }
}

class AddChildButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { onPress } = this.props

    return (
      <TouchableHighlight
        style={styles.addChildButton}
        onPress={onPress}
        underlayColor="white"
      >
        <FormattedMessage
          {...messages.addChildButtonTitle}
          style={styles.addChildButtonTitle}
        />
      </TouchableHighlight>
    )
  }
}

export default class ChildSelect extends React.Component {
  static propTypes = {
    onSelect: PropTypes.func,
    onAddChildren: PropTypes.func,
    options: PropTypes.array,
    activeId: PropTypes.string,
    isAddChildButtonVisisble: PropTypes.bool,
  }

  static defaultProps = {
    options: [],
    isAddChildButtonVisisble: false,
  }

  render() {
    const {
      options,
      activeId,
      onAddChildren,
      isAddChildButtonVisisble,
    } = this.props

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.childrenListWrap}>
          {options.map(item => (
            <ChildRow
              key={item.id}
              childId={item.id}
              childName={item.childName}
              active={activeId === item.id}
              child={item}
              onPress={this._childSelected}
            />
          ))}
        </ScrollView>

        {isAddChildButtonVisisble ? (
          <AddChildButton onPress={onAddChildren} />
        ) : null}
      </View>
    )
  }

  _childSelected = child => {
    const { onSelect } = this.props

    if (isFunction(onSelect)) {
      onSelect(child)
    }
  }
}
