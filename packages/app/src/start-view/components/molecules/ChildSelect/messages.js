import { defineMessages } from 'react-intl'

export default defineMessages({
  addChildButtonTitle: '+ Add more children',
  defaultBabyName: 'My baby',
})
