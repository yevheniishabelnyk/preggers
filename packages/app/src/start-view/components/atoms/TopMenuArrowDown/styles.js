import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const containerWidth = moderateScale(40)

const styles = StyleSheet.create({
  container: {
    width: containerWidth,
    height: containerWidth,
    borderRadius: containerWidth,
    backgroundColor: 'rgb(249,250,252)',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },

  contentWrapper: {
    flex: 1,
  },

  icon: {
    width: moderateScale(16, 0.2),
    height: moderateScale(8, 0.2),
  },

  leftLine: {
    height: moderateScale(2),
    width: moderateScale(8),
    backgroundColor: 'rgb(59,72,89)',
    transform: [{ rotate: '45deg' }],
    marginLeft: moderateScale(-4),
  },

  rightLine: {
    height: moderateScale(2),
    width: moderateScale(8),
    backgroundColor: 'rgb(59,72,89)',
    transform: [{ rotate: '145deg' }],
    marginRight: moderateScale(-10),
  },
})

export default styles
