/**
 *
 * TopMenuArrowDown
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image } from 'react-native'

import icon from 'assets/icons/arrow-down.png'

import styles from './styles'

export default class TopMenuArrowDown extends React.Component {
  static propTypes = {
    prop: PropTypes.object,
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={icon} style={styles.icon} />
      </View>
    )
  }
}
