import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  buttonWrapper: {
    justifyContent: 'center',
  },

  weekTitle: {
    width: moderateScale(135, 1),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12, 0.8),
    color: 'rgb(23,24,28)',
    letterSpacing: 0.59,
    textAlign: 'center',
    marginTop: moderateScale(3, resizeFactor),
  },

  lastWeekTitle: {
    marginRight: moderateScale(167, 1.3),
  },

  activeWeekTitle: {
    color: '#fa4168',
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(15, 0.8),
    marginTop: 0,
  },
})

export default styles
