/**
 *
 * WeekBarItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity } from 'react-native'

import { isFunction } from 'lodash'

import messages from './messages'
import { FormattedMessage } from '@/vendor/react-intl-native'

import styles from './styles'

export default class WeekBarItem extends React.Component {
  static propTypes = {
    week: PropTypes.number,
    onSelect: PropTypes.func,
    active: PropTypes.bool,
    last: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { week, active, last } = this.props

    return (
      <TouchableOpacity
        onPress={this._itemPressed}
        activeOpacity={0.95}
        style={styles.buttonWrapper}
      >
        <FormattedMessage
          {...messages.title}
          values={{ weekNumber: week > 3 ? week : '1-3' }}
          style={[
            styles.weekTitle,
            last ? styles.lastWeekTitle : null,
            active ? styles.activeWeekTitle : null,
          ]}
        />
      </TouchableOpacity>
    )
  }

  _itemPressed = () => {
    const { week, onSelect } = this.props

    if (isFunction(onSelect)) {
      onSelect(week)
    }
  }
}
