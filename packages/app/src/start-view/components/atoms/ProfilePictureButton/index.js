/**
 *
 * ProfilePictureButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Image, TouchableHighlight, View } from 'react-native'

import defaultUser from 'assets/icons/user.png'

import styles from './styles'

const ProfilePictureButton = ({ image = defaultUser, onPress }) => (
  <TouchableHighlight
    style={styles.profilePictureButtonContainer}
    onPress={onPress}
    underlayColor="transparent"
  >
    <View style={styles.button}>
      <Image style={styles.image} source={image} resizeMode="contain" />
    </View>
  </TouchableHighlight>
)

ProfilePictureButton.propTypes = {
  image: PropTypes.any,
  onPress: PropTypes.func,
}

export default ProfilePictureButton
