import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const imageWidth = moderateScale(36)

const styles = StyleSheet.create({
  profilePictureButtonContainer: {
    height: moderateScale(63, 0.3),
    width: moderateScale(90),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 'auto',
  },
  button: {
    height: moderateScale(50),
    width: moderateScale(63),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 'auto',
  },

  image: {
    height: imageWidth,
    width: imageWidth,
    borderRadius: imageWidth / 2,
  },
})

export default styles
