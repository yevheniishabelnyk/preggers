/**
 *
 * WeekBarArrow
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableHighlight, Image } from 'react-native'

import styles from './styles'

const WeekBarArrow = ({ right, onPress }) => (
  <TouchableHighlight
    style={[styles.arrow, right ? styles.rightArrow : null]}
    onPress={onPress}
    underlayColor="rgba(255,255,255,.95)"
  >
    <Image
      style={[styles.arrowIcon, !right ? styles.leftArrowIcon : null]}
      resizeMode="contain"
      source={require('assets/icons/arrow-right.png')}
    />
  </TouchableHighlight>
)

WeekBarArrow.propTypes = {
  right: PropTypes.bool,
  onPress: PropTypes.func,
}

export default WeekBarArrow
