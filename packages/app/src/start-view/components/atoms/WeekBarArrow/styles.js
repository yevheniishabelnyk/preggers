import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.7

export const containerHeight = moderateScale(55, resizeFactor)

const styles = StyleSheet.create({
  arrow: {
    width: moderateScale(72, resizeFactor),
    height: moderateScale(55, resizeFactor),
    borderRadius: moderateScale(27.5, resizeFactor),
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.15,
    shadowRadius: 10,
    shadowColor: '#111723',
    shadowOffset: { height: 8, width: -2 },
    marginLeft: Platform.OS === 'ios' ? moderateScale(-55, resizeFactor) : 0,
  },

  arrowIcon: {
    height: moderateScale(12.5, resizeFactor),
    width: moderateScale(12.5, resizeFactor),
    marginRight: moderateScale(35, resizeFactor),
  },

  leftArrowIcon: {
    transform: [{ rotate: '180deg' }],
    marginRight: 0,
    marginLeft: moderateScale(35, resizeFactor),
  },

  rightArrow: {
    marginLeft: 'auto',
    marginRight: Platform.OS === 'ios' ? moderateScale(-55, resizeFactor) : 0,
  },
})

export default styles
