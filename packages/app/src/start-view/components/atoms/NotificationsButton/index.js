/**
 *
 * NotificationsButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableHighlight } from 'react-native'

import styles from './styles'

const NotificationsButton = ({ unreadCount, onPress }) => (
  <TouchableHighlight
    style={styles.notificationsButtonContainer}
    underlayColor="transparent"
    onPress={onPress}
  >
    <View style={styles.button}>
      <View style={styles.content}>
        <Image
          source={require('assets/icons/bell.png')}
          style={styles.icon}
          resizeMode={'contain'}
        />

        {unreadCount ? (
          <View style={styles.unreadCountContainer}>
            <Text style={styles.unreadCountText}>{unreadCount}</Text>
          </View>
        ) : null}
      </View>
    </View>
  </TouchableHighlight>
)

NotificationsButton.propTypes = {
  unreadCount: PropTypes.number,
  onPress: PropTypes.func,
}

export default NotificationsButton
