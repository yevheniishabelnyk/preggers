import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const iconWidth = moderateScale(36)
const unreadCountContainerWidth = moderateScale(18)

const styles = StyleSheet.create({
  notificationsButtonContainer: {
    height: moderateScale(63, 0.3),
    width: moderateScale(90),
    backgroundColor: 'white',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },

  button: {
    height: moderateScale(50),
    width: moderateScale(63),
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },

  content: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },

  icon: {
    width: iconWidth,
    height: iconWidth,
  },

  unreadCountContainer: {
    width: unreadCountContainerWidth,
    height: unreadCountContainerWidth,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(250,65,105)',
    borderRadius: unreadCountContainerWidth / 2,
    position: 'absolute',
    top: moderateScale(-3),
    right: moderateScale(-3),
  },

  unreadCountText: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(11),
    color: 'white',
    letterSpacing: 0,
    textAlign: 'center',
    backgroundColor: 'transparent',
    lineHeight: moderateScale(19),
  },
})

export default styles
