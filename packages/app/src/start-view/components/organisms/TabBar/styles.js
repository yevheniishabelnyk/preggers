import { StyleSheet, Dimensions } from 'react-native'
import { Constants } from 'expo'

const { statusBarHeight } = Constants
const { width, height } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

export const containerHeight = moderateScale(63, resizeFactor) + statusBarHeight
export const childSelectContainerHeight = height - containerHeight

const styles = StyleSheet.create({
  container: {
    paddingTop: statusBarHeight,
    height: containerHeight,
    width,
    position: 'relative',
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },

  selectOpened: {
    position: 'absolute',
    height,
    alignItems: 'flex-start',
  },

  childSelectContainer: {
    position: 'absolute',
    bottom: -childSelectContainerHeight,
    left: 0,
    width,
    height: childSelectContainerHeight,
    backgroundColor: 'transparent',
    zIndex: 3,
  },

  childSelectToggleContainer: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },

  childSelectToggleContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  childSelectButton: {
    width: '100%',
    height: moderateScale(63, resizeFactor),
    justifyContent: 'center',
  },

  childSelectToggleText: {
    fontSize: moderateScale(17, resizeFactor),
    fontFamily: 'Now-Medium',
    color: '#3b4858',
    letterSpacing: 0,
    lineHeight: moderateScale(22, resizeFactor),
    marginLeft: moderateScale(20, resizeFactor),
  },

  arrowDownIcon: {
    height: moderateScale(11, resizeFactor),
    marginTop: moderateScale(2, resizeFactor),
  },
})

export default styles
