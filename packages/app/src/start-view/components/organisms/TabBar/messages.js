import { defineMessages } from 'react-intl'

export default defineMessages({
  defaultBabyName: 'My baby',
  menuTitle: 'My children',
})
