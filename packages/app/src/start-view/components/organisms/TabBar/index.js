/**
 *
 * StartView TabBar
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Constants } from 'expo'

import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Platform,
} from 'react-native'
import { get } from 'lodash'
import isApplicationUpdateAvailable from '@/shared/utils/isApplicationUpdateAvailable'

import NotificationsButton from '@/start-view/components/atoms/NotificationsButton'
import ProfilePictureButton from '@/start-view/components/atoms/ProfilePictureButton'
import TopMenuArrowDown from '@/start-view/components/atoms/TopMenuArrowDown'

import ChildSelect from '@/start-view/components/molecules/ChildSelect'

import { NavigationActions } from 'react-navigation'
import * as RouterActions from '@/app/redux/router/actions'

import {
  getUserChildren,
  getUserPregnancies,
  getUserUnreadInvitationsCount,
  getLatestAppVersion,
} from '@/app/redux/selectors'

import { PREGNANCY__DEFAULT_BABY_NAME } from '@/app/constants/BabyNames'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { firebaseConnect } from 'react-redux-firebase'

import styles from './styles'

@firebaseConnect(props => {
  const listeners = ['appVersion']

  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    listeners.push(`userOwned/invitations/${profile.id}`)
    listeners.push(`derivedData/notificationUnreadCount/${profile.id}`)

    const isConnectedPartner =
      profile.partner &&
      profile.type !== 'mother' &&
      profile.type !== 'single_mother'

    if (isConnectedPartner) {
      listeners.push(`childrenByUser/${profile.partner}`)
    }
  }

  return listeners
})
@connect((state, props) => {
  const { profile } = props.screenProps

  let unreadCount = 0
  let userChildren, userPregnancies
  let childSelectOptions = []
  let isConnectedPartner

  if (!profile.isEmpty) {
    unreadCount = getUserUnreadInvitationsCount(state, profile.id)
    unreadCount += get(
      state.firebase.data,
      `derivedData.notificationUnreadCount.${profile.id}`
    )

    if (profile.pregnancy) {
      userPregnancies = getUserPregnancies(state, profile.id)

      if (userPregnancies && userPregnancies[profile.pregnancy]) {
        childSelectOptions.push({
          id: profile.pregnancy,
          childName: userPregnancies[profile.pregnancy],
          dataType: 'pregnancy',
        })
      }
    } else {
      userPregnancies = null
    }

    const isConnectedPartner = Boolean(
      profile.partner &&
        profile.type !== 'mother' &&
        profile.type !== 'single_mother'
    )

    // console.info('isConnectedPartner: ', isConnectedPartner)

    if (isConnectedPartner) {
      userChildren = getUserChildren(state, profile.partner)
    } else {
      userChildren = getUserChildren(state, profile.id)
    }

    if (userChildren) {
      Object.keys(userChildren).forEach(childId => {
        childSelectOptions.push({
          id: childId,
          childName: userChildren[childId],
          dataType: 'child',
        })
      })
    }
  }

  const latestVersion = getLatestAppVersion(state, Platform.OS)

  if (
    latestVersion &&
    isApplicationUpdateAvailable(Constants.manifest.version, latestVersion)
  ) {
    unreadCount += 1
  }

  // console.info('childSelectOptions: ', childSelectOptions)

  return {
    isFetching: userChildren === undefined || userPregnancies === undefined,
    childSelectOptions,
    unreadCount,
    isConnectedPartner,
    isCurrentRouteOpenedWithDeepLink:
      state.router.isCurrentRouteOpenedWithDeepLink,
  }
})
export default class TabBar extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
    unreadCount: PropTypes.number,
    isFetching: PropTypes.bool,
    isCurrentRouteOpenedWithDeepLink: PropTypes.bool,
    isConnectedPartner: PropTypes.bool,
    childSelectOptions: PropTypes.array.isRequired,
    screenProps: PropTypes.shape({
      profile: PropTypes.object.isRequired,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    childSelectIsVisible: false,
  }

  render() {
    const { profile } = this.props.screenProps
    const {
      childSelectOptions,
      navigation,
      isConnectedPartner,
      unreadCount,
    } = this.props
    const { childSelectIsVisible } = this.state

    const activeRoute = navigation.state.routes[navigation.state.index]

    let selectedOption = null

    let activeId = null

    if (
      activeRoute.routeName !== '/missing-pregnancy' &&
      childSelectOptions.length
    ) {
      const { params = {} } = activeRoute

      activeId = params.childId || params.pregnancyId

      selectedOption = childSelectOptions.find(item => item.id === activeId)
    }

    return (
      <View
        style={[
          styles.container,
          Platform.OS === 'android' && childSelectIsVisible
            ? styles.selectOpened
            : null,
        ]}
      >
        <ProfilePictureButton
          image={profile.image}
          onPress={this._goToSettingsMenu}
        />

        <View style={styles.childSelectToggleContainer}>
          <TouchableHighlight
            onPress={this._toggleChildSelect}
            underlayColor="transparent"
            style={styles.childSelectButton}
          >
            <View style={styles.childSelectToggleContent}>
              <TopMenuArrowDown rotate={childSelectIsVisible} />

              <Text style={styles.childSelectToggleText}>
                {selectedOption ? (
                  selectedOption.childName === PREGNANCY__DEFAULT_BABY_NAME ? (
                    <FormattedMessage {...messages.defaultBabyName} />
                  ) : (
                    selectedOption.childName
                  )
                ) : (
                  <FormattedMessage {...messages.menuTitle} />
                )}
              </Text>
            </View>
          </TouchableHighlight>
        </View>

        <NotificationsButton
          unreadCount={unreadCount}
          onPress={this._goToNotifications}
        />

        {childSelectIsVisible ? (
          Platform.OS === 'android' ? (
            <ChildSelect
              options={childSelectOptions}
              onSelect={this._onChildSelect}
              onAddChildren={this._onAddChildren}
              activeId={activeId}
              isAddChildButtonVisisble={!isConnectedPartner}
            />
          ) : (
            <TouchableOpacity
              style={styles.childSelectContainer}
              onPress={this._toggleChildSelect}
              activeOpacity={0.95}
            >
              <View>
                <ChildSelect
                  options={childSelectOptions}
                  onSelect={this._onChildSelect}
                  onAddChildren={this._onAddChildren}
                  activeId={activeId}
                  isAddChildButtonVisisble={!isConnectedPartner}
                />
              </View>
            </TouchableOpacity>
          )
        ) : null}
      </View>
    )
  }

  componentWillMount() {
    if (!this.props.isFetching) {
      const { navigation } = this.props

      const currentRoute = navigation.state.routes[navigation.state.index]

      if (!this.props.isCurrentRouteOpenedWithDeepLink) {
        if (
          currentRoute &&
          currentRoute.routeName === '/~pregnancy' &&
          (!currentRoute.params ||
            currentRoute.params.pregnancyId !==
              this.props.screenProps.profile.pregnancy)
        ) {
          console.log('TUTA')
          this.props.navigation.dispatch(
            RouterActions.resetToAuthenitcatedInitialRoute()
          )
          setTimeout(() => {
            const { childSelectOptions } = this.props
            this._validateNavigationState(navigation.state, childSelectOptions)
          }, 0)
        }
      } else {
        this.props.navigation.dispatch(RouterActions.resetDeepLinkFlag())
      }
    }

    this.didFocusSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.isFocused = true

        const { state } = payload

        const { isFetching } = this.props

        if (!isFetching) {
          setTimeout(() => {
            const { childSelectOptions } = this.props
            this._validateNavigationState(state, childSelectOptions)
          }, 0)
        } else {
          this.isWaitingFetchEnd = true
        }
      }
    )

    this.didBlurSubscription = this.props.navigation.addListener(
      'didBlur',
      () => {
        this.isFocused = false
      }
    )
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.isWaitingFetchEnd &&
      this.props.isFetching &&
      !nextProps.isFetching
    ) {
      this.isWaitingFetchEnd = false

      const { navigation, childSelectOptions } = nextProps
      this._validateNavigationState(navigation.state, childSelectOptions)
    }

    if (
      !this.props.screenProps.profile.isEmpty &&
      this.isFocused &&
      (this.props.childSelectOptions.length !==
        nextProps.childSelectOptions.length ||
        this.props.screenProps.profile.pregnancy !==
          nextProps.screenProps.profile.pregnancy)
    ) {
      const { navigation, childSelectOptions } = nextProps

      this._validateNavigationState(navigation.state, childSelectOptions)
    }
  }

  componentWillUnmount() {
    this.didFocusSubscription.remove()
    this.didBlurSubscription.remove()
  }

  _goToSettingsMenu = () => this._goTo('/settings-menu')

  _goToNotifications = () => this._goTo('/notifications')

  _validateNavigationState = (state, childSelectOptions) => {
    const activeRoute = state.routes[state.index]

    if (childSelectOptions.length) {
      if (
        activeRoute.routeName === '/missing-pregnancy' ||
        !activeRoute.params ||
        (activeRoute.params &&
          !childSelectOptions.find(
            item =>
              item.id === activeRoute.params.childId ||
              item.id === activeRoute.params.pregnancyId
          ))
      ) {
        const nextSelectedChild = childSelectOptions[0]

        if (nextSelectedChild.dataType === 'pregnancy') {
          if (activeRoute.routeName === '/~pregnancy') {
            this._setParams(activeRoute.key, {
              pregnancyId: nextSelectedChild.id,
            })
          } else {
            this.props.navigation.navigate('/~pregnancy', {
              pregnancyId: nextSelectedChild.id,
            })
          }
        } else if (activeRoute.routeName === '/~child') {
          this._setParams(activeRoute.key, {
            childId: nextSelectedChild.id,
          })
        } else {
          this.props.navigation.navigate('/child', {
            childId: nextSelectedChild.id,
          })
        }
      }
    } else if (activeRoute.routeName !== '/missing-pregnancy') {
      this.props.navigation.navigate('/missing-pregnancy')
    }
  }

  _onChildSelect = selectedChild => {
    const { navigation } = this.props

    const activeRoute = navigation.state.routes[navigation.state.index]
    const activeRouteParams = activeRoute.params

    if (
      activeRoute.routeName === '/missing-pregnancy' ||
      !activeRouteParams ||
      selectedChild.id !== activeRouteParams.pregnancyId ||
      selectedChild.id !== activeRouteParams.childId
    ) {
      switch (selectedChild.dataType) {
        case 'child': {
          if (activeRoute.routeName === '/child') {
            this._setParams(activeRoute.key, { childId: selectedChild.id })
          } else {
            this.props.navigation.navigate('/child', {
              childId: selectedChild.id,
            })
          }

          break
        }

        case 'pregnancy': {
          if (activeRoute.routeName === '/~pregnancy') {
            this._setParams(activeRoute.key, { pregnancyId: selectedChild.id })
          } else if (Platform.OS === 'android') {
            const pregnancyParams = this.props.navigation.state.routes.find(
              item => item.routeName === '/~pregnancy'
            ).params

            this.props.navigation.dispatch(
              NavigationActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: '/~home',
                    action: NavigationActions.navigate({
                      routeName: '/~start-view',
                      action: NavigationActions.navigate({
                        routeName: '/~pregnancy',
                        params: {
                          ...(pregnancyParams || {}),
                          pregnancyId: selectedChild.id,
                        },
                      }),
                    }),
                  }),
                ],
              })
            )
          } else {
            this.props.navigation.navigate('/~pregnancy', {
              pregnancyId: selectedChild.id,
            })
          }
          break
        }
      }
    }

    this._toggleChildSelect()
  }

  _setParams = (routeKey, params) => {
    const setParamsAction = NavigationActions.setParams({
      params,
      key: routeKey,
    })

    this.props.navigation.dispatch(setParamsAction)
  }

  _onAddChildren = () => {
    this._goTo('/forms-child', { flow: 'ADD' })

    this._toggleChildSelect()
  }

  _toggleChildSelect = () =>
    this.setState(prevState => ({
      childSelectIsVisible: !prevState.childSelectIsVisible,
    }))

  _goTo = (routeName, params) =>
    this.props.navigation.navigate(routeName, params)
}
