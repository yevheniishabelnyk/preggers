import { defineMessages } from 'react-intl'

export default defineMessages({
  noPregnancyText: 'You have no current pregnancy. Add one down below.',
  noPregnancyTextConnectedPartner:
    'You have no current pregnancy. A new pregnancy can be added by the mother.',
  addPregnancyButtonTitle: 'Add pregnancy',
})
