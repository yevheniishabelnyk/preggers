import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },

  contentContainerStyle: {
    paddingHorizontal: moderateScale(50),
    paddingBottom: moderateScale(29),
    alignItems: 'center',
  },

  image: {
    width: moderateScale(168),
    height: moderateScale(191),
  },

  text: {
    width: moderateScale(260),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(23),
    lineHeight: moderateScale(33),
    color: 'rgb(23, 24, 28)',
    marginTop: moderateScale(69),
    textAlign: 'center',
  },

  button: {
    width: moderateScale(170),
    height: moderateScale(48),
    marginTop: moderateScale(92),
  },

  textStyle: {
    fontSize: moderateScale(14),
    fontFamily: 'Now-Medium',
  },
})

export default styles
