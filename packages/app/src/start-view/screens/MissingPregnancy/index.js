/*
 *
 * Missing Pregnancy screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GradientButton from '@/pregnancy/components/atoms/GradientButton'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@connect((state, props) => {
  const { profile } = props.screenProps

  let allowedToAddPregnancy = false

  if (!profile.isEmpty && !profile.pregnancy) {
    allowedToAddPregnancy =
      !profile.connection ||
      (profile.type === 'mother' || profile.type === 'single_mother')
  }

  return {
    allowedToAddPregnancy,
  }
})
export default class MissingPregnancy extends BaseScreen {
  static propTypes = {
    allowedToAddPregnancy: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { allowedToAddPregnancy } = this.props
    const { formatMessage } = this.context.intl

    const textMessage = allowedToAddPregnancy
      ? messages.noPregnancyText
      : messages.noPregnancyTextConnectedPartner

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        keyboard
      >
        <Image
          source={require('assets/img/pregnant-with-phone.png')}
          style={styles.image}
        />

        <FormattedMessage {...textMessage} style={styles.text} />

        {allowedToAddPregnancy ? (
          <GradientButton
            title={formatMessage(messages.addPregnancyButtonTitle)}
            style={styles.button}
            textStyle={styles.textStyle}
            onPress={this._addPregnancyButtonPressed}
          />
        ) : null}
      </ViewWrapper>
    )
  }

  _addPregnancyButtonPressed = () =>
    this._goTo('/forms-pregnancy', { flow: 'ADD' })
}
