/**
 *
 * FormSelect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Modal,
  Picker,
  Text,
  TouchableHighlight,
  View,
  Image,
  Platform,
} from 'react-native'
import { BoxShadow } from '@/vendor/react-native-shadow'

import { isFunction } from 'lodash'

import arrowDownIcon from 'assets/icons/arrow-down-bold.png'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles, { containerShadow } from './styles'

export default class FormSelect extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    items: PropTypes.array,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onSelect: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.labelMap = props.items.reduce((out, item) => {
      out[item.value] = item.label
      return out
    }, {})

    this.initialValue = props.value

    this.state = {
      showPicker: false,
      selectedIndex: null,
      selectedValue: props.value || null,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.items !== nextProps.items) {
      this.labelMap = nextProps.items.reduce((out, item) => {
        out[item.value] = item.label
        return out
      }, {})
    }
  }

  render() {
    const { style } = this.props
    const { formatMessage } = this.context.intl

    return (
      <View>
        <BoxShadow setting={containerShadow}>
          <View style={[styles.container, style]}>
            <TouchableHighlight
              onPress={this._showModal}
              underlayColor="rgba(255,255,255,.5)"
            >
              <View style={styles.formField}>
                <Text style={styles.title}>{this.props.title}</Text>

                <Text
                  style={[
                    styles.value,
                    !this.labelMap[this.props.value]
                      ? styles.placeholderColor
                      : null,
                  ]}
                >
                  {this.labelMap[this.props.value] ||
                    formatMessage(messages.selectButtonTitle)}
                </Text>

                <Image source={arrowDownIcon} style={styles.icon} />
                {Platform.OS === 'android' && (
                  <View style={styles.androidPickerOverlay}>
                    {this._renderPicker()}
                  </View>
                )}
              </View>
            </TouchableHighlight>

            {Platform.OS === 'ios' && (
              <Modal
                animationType="fade"
                transparent
                visible={this.state.showPicker}
                onRequestClose={this._cancel}
              >
                <TouchableHighlight
                  style={styles.cancelMask}
                  onPress={this._cancel}
                  underlayColor="rgba(255,255,255,.5)"
                >
                  <View style={styles.pickerWrapper}>
                    {this._renderPicker()}
                    <TouchableHighlight
                      style={styles.selectButton}
                      onPress={this._select}
                    >
                      <FormattedMessage
                        {...messages.selectButtonTitle}
                        style={styles.selectButtonTitle}
                      />
                    </TouchableHighlight>
                    <TouchableHighlight
                      style={styles.cancelButton}
                      onPress={this._cancel}
                    >
                      <FormattedMessage
                        {...messages.cancelButtonTitle}
                        style={styles.cancelButtonTitle}
                      />
                    </TouchableHighlight>
                  </View>
                </TouchableHighlight>
              </Modal>
            )}
          </View>
        </BoxShadow>
      </View>
    )
  }

  _renderPicker = () => (
    <Picker
      style={styles.picker}
      selectedValue={this.state.selectedValue}
      onValueChange={this._valueChangeHandler}
    >
      {this.props.items.map(item => (
        <Picker.Item
          key={item.value}
          label={item.label}
          value={item.value}
          style={styles.label}
        />
      ))}
    </Picker>
  )

  _showModal = () => this.setState({ showPicker: true })

  _setVisibility = showPicker => this.setState({ showPicker })

  _select = () => {
    this.initialValue =
      this.state.selectedValue || Object.keys(this.labelMap)[0]
    this._setVisibility(false)

    if (isFunction(this.props.onSelect)) {
      this.props.onSelect(this.initialValue)
    }
  }

  _cancel = () => {
    if (this.initialValue !== this.state.selectedValue) {
      this.setState({
        selectedValue: this.initialValue,
        showPicker: false,
      })
    } else {
      this.setState({ showPicker: false })
    }
  }

  _valueChangeHandler = selectedValue => {
    this.setState({ selectedValue }, () => {
      if (Platform.OS === 'android') {
        this._select()
      }
    })
  }
}
