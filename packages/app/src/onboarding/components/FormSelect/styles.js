import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'
const { width } = Dimensions.get('window')

const containerHeight = moderateScale(60)

const containerWidth = moderateScale(330)

export const containerShadow = {
  width: containerWidth,
  height: containerHeight,
  color: '#000000',
  radius: 8,
  opacity: 0.025,
  border: 22,
  x: 0,
  y: 8,
  style: { marginVertical: 8 },
}

const styles = StyleSheet.create({
  container: {
    height: containerHeight,
    width: containerWidth,
    borderRadius: moderateScale(8),
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: 'white',
    position: 'relative',
  },

  formField: {
    paddingHorizontal: moderateScale(18, 0.3),
    justifyContent: 'center',
    height: moderateScale(60, 0.3),
  },

  icon: {
    position: 'absolute',
    top: moderateScale(28, 0.3),
    right: moderateScale(23),
    width: moderateScale(10),
    height: moderateScale(5),
  },

  value: {
    fontSize: moderateScale(14),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: '#000',
  },

  placeholderColor: {
    color: 'white',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(10),
    color: 'rgb(131, 146, 167)',
  },

  pickerWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },

  picker: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingTop: moderateScale(20),
    width,
    backgroundColor: 'white',
  },

  selectButton: {
    position: 'absolute',
    backgroundColor: '#6341fb',
    bottom: 185,
    right: moderateScale(15),
    borderRadius: moderateScale(20),
    paddingHorizontal: moderateScale(10, 1),
    paddingVertical: moderateScale(10, 0.3),
    alignItems: 'center',
    justifyContent: 'center',
  },

  selectButtonTitle: {
    backgroundColor: '#6341fb',
    color: 'white',
  },

  androidPickerOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    opacity: 0,
  },

  cancelButton: {
    backgroundColor: 'rgba(0,0,0,0)',
    position: 'absolute',
    bottom: 185,
    left: moderateScale(15),
    borderRadius: moderateScale(20),
    paddingHorizontal: moderateScale(10, 1),
    paddingVertical: moderateScale(10, 0.3),
    alignItems: 'center',
    justifyContent: 'center',
  },

  cancelButtonTitle: {
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#8A8EAC',
  },

  cancelMask: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,.5)',
  },

  expandIcon: {
    position: 'absolute',
    right: moderateScale(38),
    top: moderateScale(23),
    width: moderateScale(26),
    height: moderateScale(14),
  },
})

export default styles
