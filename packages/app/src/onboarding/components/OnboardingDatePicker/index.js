/**
 *
 * OnboardingDatePicker
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableHighlight } from 'react-native'
import { BoxShadow } from '@/vendor/react-native-shadow'

import datePickerModal from '@/shared/decorators/datePickerModal'

import calendarIcon from 'assets/icons/red-calendar-icon.png'
import clockIcon from 'assets/icons/clock.png'

import styles, { containerShadow } from './styles'

@datePickerModal
export default class OnboardingDatePicker extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    mode: PropTypes.string,
    showDatePicker: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { value, mode, showDatePicker, style } = this.props

    return (
      <BoxShadow setting={containerShadow}>
        <TouchableHighlight
          style={style}
          underlayColor="rgba(255,255,255,.5)"
          onPress={showDatePicker}
        >
          <View style={styles.content}>
            <Text style={styles.value}>{value}</Text>

            <Image
              source={mode === 'time' ? clockIcon : calendarIcon}
              style={[styles.icon, mode === 'time' ? styles.clock : null]}
            />
          </View>
        </TouchableHighlight>
      </BoxShadow>
    )
  }
}
