import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'
const resizeFactor = 0.35

const containerHeight = moderateScale(60)

const containerWidth = moderateScale(330)

export const containerShadow = {
  width: containerWidth + 2, // 2 = border width
  height: containerHeight,
  color: '#000000',
  radius: 8,
  opacity: 0.025,
  border: 22,
  x: 0,
  y: 8,
  style: { marginVertical: 8 },
}

const styles = StyleSheet.create({
  content: {
    width: containerWidth,
    height: containerHeight,
    alignItems: 'center',
    borderRadius: moderateScale(8),
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingHorizontal: moderateScale(18),
  },

  value: {
    fontSize: moderateScale(14, 0.3),
    lineHeight: moderateScale(40, 0.3),
    marginBottom: moderateScale(-4, 0.3),
    fontFamily: 'Now-Medium',
    color: 'rgb(5,5,5)',
    textAlign: 'left',
  },

  icon: {
    position: 'absolute',
    alignSelf: 'center',
    right: moderateScale(18, resizeFactor),
    height: moderateScale(23, resizeFactor),
    width: moderateScale(23, resizeFactor),
    marginLeft: moderateScale(11, resizeFactor),
  },

  clock: {
    height: moderateScale(25),
    width: moderateScale(25),
  },
})

export default styles
