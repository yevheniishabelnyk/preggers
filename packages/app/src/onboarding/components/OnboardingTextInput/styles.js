import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const containerHeight = moderateScale(60)

const containerWidth = moderateScale(330)

export const containerShadow = {
  width: containerWidth,
  height: containerHeight,
  color: '#000000',
  radius: 8,
  opacity: 0.025,
  border: 22,
  x: 0,
  y: 8,
  style: { marginVertical: 8 },
}

const styles = StyleSheet.create({
  inputWrapper: {
    width: containerWidth,
    height: containerHeight,
    borderRadius: moderateScale(8),
    justifyContent: 'center',
    backgroundColor: 'white',
    overflow: 'visible',
  },

  input: {
    width: moderateScale(330),
    height: moderateScale(45),
    color: '#000',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    paddingRight: moderateScale(40),
    paddingLeft: moderateScale(18),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(20),
    lineHeight: moderateScale(33),
    marginBottom: moderateScale(13),
    textAlign: 'center',
  },

  label: {
    fontFamily: 'Now-Medium',
    color: 'rgb(131, 146, 167)',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(18),
  },

  errorText: {
    color: 'rgb(255, 100, 101)',
  },

  errorIcon: {
    position: 'absolute',
    width: moderateScale(20),
    height: moderateScale(20),
    top: moderateScale(19),
    right: moderateScale(18),
  },
})

export default styles
