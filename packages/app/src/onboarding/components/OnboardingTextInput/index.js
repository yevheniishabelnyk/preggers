/*
 *
 * OnboardingTextInput
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TextInput, Text, Animated, Image } from 'react-native'
import { BoxShadow } from '@/vendor/react-native-shadow'

import { moderateScale } from '@/app/scaling'

import styles, { containerShadow } from './styles'

import { isFunction } from 'lodash'

const labelAnimatedTopOutputRange = [moderateScale(21), moderateScale(11)]
const labelAnimatedFontSizeOutputRange = [moderateScale(14), moderateScale(10)]

export default class OnboardingTextInput extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    returnKeyType: PropTypes.string,
    keyboardType: PropTypes.string,
    autoCapitalize: PropTypes.string,
    error: PropTypes.bool,
    autoCorrect: PropTypes.bool,
    enablesReturnKeyAutomatically: PropTypes.bool,
    onChangeText: PropTypes.func,
    onKeyPress: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    numberOfLines: PropTypes.number,
    maxLength: PropTypes.number,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  state = {
    isFocused: false,
  }

  constructor(props) {
    super(props)

    this._animatedIsFocused = new Animated.Value(props.value === '' ? 0 : 1)
  }

  render() {
    const {
      placeholder = '',
      onChangeText,
      value,
      style,
      returnKeyType,
      onSubmitEditing,
      title,
      keyboardType,
      autoCapitalize,
      enablesReturnKeyAutomatically,
      error,
      maxLength,
    } = this.props

    const labelStyle = {
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: labelAnimatedTopOutputRange,
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: labelAnimatedFontSizeOutputRange,
      }),
    }

    return (
      <View style={[styles.container, style]}>
        <BoxShadow setting={containerShadow}>
          {title ? <Text style={styles.title}>{title}</Text> : null}
          <View style={styles.inputWrapper}>
            {placeholder ? (
              <Animated.Text
                style={[
                  styles.label,
                  labelStyle,
                  error ? styles.errorText : null,
                ]}
              >
                {placeholder}
              </Animated.Text>
            ) : null}

            <TextInput
              style={styles.input}
              onChangeText={onChangeText}
              value={value}
              returnKeyType={returnKeyType}
              onSubmitEditing={onSubmitEditing}
              onFocus={this._handleFocus}
              onBlur={this._handleBlur}
              multiline={false}
              keyboardType={keyboardType}
              autoCapitalize={autoCapitalize}
              enablesReturnKeyAutomatically={enablesReturnKeyAutomatically}
              maxLength={maxLength}
              underlineColorAndroid="transparent"
            />

            {error ? (
              <Image
                source={require('assets/icons/error-icon.png')}
                style={styles.errorIcon}
              />
            ) : null}
          </View>
        </BoxShadow>
      </View>
    )
  }

  componentDidUpdate() {
    const { value } = this.props

    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || value !== '' ? 1 : 0,
      duration: 200,
    }).start()
  }

  _handleFocus = () => {
    const { onFocus } = this.props

    this.setState({ isFocused: true })

    if (isFunction(onFocus)) {
      onFocus()
    }
  }

  _handleBlur = () => {
    const { onBlur } = this.props

    this.setState({ isFocused: false })

    if (isFunction(onBlur)) {
      onBlur()
    }
  }
}
