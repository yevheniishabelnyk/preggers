import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },

  topImage: {
    width: moderateScale(92),
    height: moderateScale(106),
    marginBottom: moderateScale(15),
  },

  title: {
    fontSize: moderateScale(35),
    fontFamily: 'Now-Medium',
    marginBottom: moderateScale(15),
    color: '#000',
    textAlign: 'center',
  },

  description: {
    fontSize: moderateScale(16),
    lineHeight: moderateScale(27),
    textAlign: 'center',
    fontFamily: 'Roboto-Regular',
    color: '#000',
  },

  nameInput: {
    marginTop: moderateScale(81),
  },
})

export default styles
