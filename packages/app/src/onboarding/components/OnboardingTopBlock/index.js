/*
 *
 * EnterName screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'

import styles from './styles'

export default class EnterName extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    description: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    style: PropTypes.any,
  }

  render() {
    const { title, description } = this.props

    return (
      <View style={styles.container}>
        <Image
          style={styles.topImage}
          source={require('assets/img/onboarding-top-block-image.png')}
        />

        <Text style={styles.title}>{title}</Text>

        <Text style={[styles.description, this.props.style]}>
          {description}
        </Text>
      </View>
    )
  }
}
