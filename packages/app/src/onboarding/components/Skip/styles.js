import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  skipButton: {
    position: 'absolute',
    top: moderateScale(22),
    right: moderateScale(20),
    zIndex: 1,
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15),
    lineHeight: moderateScale(33),
    color: 'rgb(250, 65, 105)',
  },
})

export default styles
