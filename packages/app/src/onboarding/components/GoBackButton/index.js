/*
 *
 * GoBackButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import styles from './styles'

export default class GoBackButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { onPress, style } = this.props

    return (
      <TouchableOpacity
        style={[styles.buttonWrap, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Image
          source={require('assets/icons/arrow-left-thin.png')}
          style={styles.arrow}
        />
      </TouchableOpacity>
    )
  }
}
