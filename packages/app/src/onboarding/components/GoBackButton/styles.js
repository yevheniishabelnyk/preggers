import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  buttonWrap: {
    width: moderateScale(40, 0.3),
    height: moderateScale(40, 0.3),
    borderRadius: moderateScale(31, 0.3),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: moderateScale(20, 0.3),
    zIndex: 1,
    top: moderateScale(21, 0.3),
  },

  arrow: {
    width: moderateScale(22, 0.3),
    height: moderateScale(15, 0.3),
  },
})

export default styles
