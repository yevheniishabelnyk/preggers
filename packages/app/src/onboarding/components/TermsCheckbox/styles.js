import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: moderateScale(20),
  },

  button: {
    height: moderateScale(28),
    width: moderateScale(28),
    borderRadius: moderateScale(5),
    borderWidth: 1,
    borderColor: '#ffffff',
    backgroundColor: '#ffffff',
    marginRight: moderateScale(14),
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.05,
    shadowRadius: 6,
    shadowColor: '#000000',
    shadowOffset: { height: 2, width: -2 },
  },

  checked: {
    borderRadius: moderateScale(4),
    height: moderateScale(26),
    width: moderateScale(26),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },

  text: {
    color: '#000',
    fontSize: moderateScale(10),
    flex: 1,
  },

  checkMark: {
    width: moderateScale(17),
    height: moderateScale(13),
  },
})

export default styles
