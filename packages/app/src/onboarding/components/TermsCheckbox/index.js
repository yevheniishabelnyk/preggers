/*
 *
 * TermsCheckbox
 *
 */
import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View, Text, Image } from 'react-native'

import styles from './styles'

export default class TermsCheckbox extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    checked: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
    onTextPress: PropTypes.func,
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    color: PropTypes.string,
  }

  render() {
    const { text, checked, onTextPress, textStyle, color = '#000' } = this.props

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={this._onCheckboxPress}
          style={styles.button}
          activeOpacity={0.95}
        >
          {checked && (
            <View style={styles.checked}>
              <Image
                style={styles.checkMark}
                source={require('assets/icons/checked-green.png')}
              />
            </View>
          )}
        </TouchableOpacity>
        <Text style={[styles.text, { color }, textStyle]} onPress={onTextPress}>
          {text}
        </Text>
      </View>
    )
  }

  _onCheckboxPress = () => this.props.onChange(!this.props.checked)
}
