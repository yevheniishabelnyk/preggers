/*
 *
 * Link
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Text, View } from 'react-native'
import { BoxShadow } from '@/vendor/react-native-shadow'

import styles, { containerShadow } from './styles'

export default class Link extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { title, style, onPress } = this.props

    return (
      <View style={style}>
        <BoxShadow setting={containerShadow}>
          <TouchableOpacity
            style={[styles.buttonWrapper, style]}
            onPress={onPress}
            activeOpacity={0.95}
          >
            <Text style={styles.text}>{title}</Text>
          </TouchableOpacity>
        </BoxShadow>
      </View>
    )
  }
}
