import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const buttonHeight = moderateScale(55)

const buttonWidth = moderateScale(234)

const buttonBorderRadius = buttonHeight / 2

export const containerShadow = {
  width: buttonWidth,
  height: buttonHeight,
  color: '#8c2d7c',
  radius: buttonBorderRadius,
  opacity: 0.1,
  border: 14,
  x: 0,
  y: 5,
  style: { marginVertical: 5 },
}

const styles = StyleSheet.create({
  buttonWrapper: {
    width: buttonWidth,
    height: buttonHeight,
    paddingTop:
      Platform.OS === 'android' ? moderateScale(13) : moderateScale(17, 0.6),
    borderWidth: 1,
    borderColor: 'rgb(250, 65, 105)',
    backgroundColor: 'rgb(250, 65, 105)',
    borderRadius: buttonBorderRadius,
    alignItems: 'center',
    position: 'relative',
  },

  text: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16),
    color: 'white',
  },
})

export default styles
