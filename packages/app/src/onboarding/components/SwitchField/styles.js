import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const containerHeight = moderateScale(60)

const containerWidth = moderateScale(330)

export const containerShadow = {
  width: containerWidth,
  height: containerHeight,
  color: '#000000',
  radius: 8,
  opacity: 0.025,
  border: 22,
  x: 0,
  y: 8,
  style: { marginVertical: 8 },
}

const styles = StyleSheet.create({
  switch: {
    width: moderateScale(330),
    height: moderateScale(60),
    borderRadius: moderateScale(8),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: moderateScale(18),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13),
    color: 'rgb(131, 146, 167)',
  },

  input: {
    flexGrow: 1,
    paddingHorizontal: moderateScale(10),
    color: 'white',
  },
})

export default styles
