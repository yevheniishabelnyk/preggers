/*
 *
 * SwitchField
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Switch } from 'react-native'
import { BoxShadow } from '@/vendor/react-native-shadow'

import styles, { containerShadow } from './styles'

export default class SwitchField extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.bool,
    error: PropTypes.bool,
    disabled: PropTypes.bool,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    title: PropTypes.string,
  }

  render() {
    const { title = '', value = false, onChange, disabled = false } = this.props

    return (
      <View>
        <BoxShadow setting={containerShadow}>
          <View style={styles.switch}>
            <Text style={styles.title}>{title}</Text>

            <Switch
              value={value}
              onValueChange={onChange}
              disabled={disabled}
              onTintColor="rgb(64, 224, 190)"
            />
          </View>
        </BoxShadow>
      </View>
    )
  }
}
