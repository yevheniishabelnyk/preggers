import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

export const loaderWidth = moderateScale(56, resizeFactor)
const buttonHeight = moderateScale(56, resizeFactor)
const borderRadius = buttonHeight / 2

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: buttonHeight,
    borderRadius: borderRadius,
    borderWidth: moderateScale(1, resizeFactor),
    borderColor: 'rgb(250, 65, 105)',
    shadowOpacity: 0.25,
    shadowRadius: 8,
    shadowColor: 'rgb(140, 45, 124)',
    shadowOffset: { height: 5, width: 0 },
    marginTop: 'auto',
    alignSelf: 'center',
    backgroundColor: 'rgb(250, 65, 105)',
    minWidth: moderateScale(186),
  },

  button: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    height: buttonHeight,
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16, resizeFactor),
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: moderateScale(45, resizeFactor),
  },

  loader: {
    marginTop:
      Platform.OS === 'ios'
        ? moderateScale(1, resizeFactor)
        : moderateScale(-2, resizeFactor),
    marginLeft: Platform.OS === 'ios' ? moderateScale(3, resizeFactor) : null,
  },
})

export default styles
