/*
 *
 * Next Button
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, ActivityIndicator, Animated } from 'react-native'

import styles, { loaderWidth } from './styles'

export default class NextButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    style: PropTypes.any,
    loader: PropTypes.bool,
    disabled: PropTypes.bool,
  }

  render() {
    const { title, onPress, style, loader, disabled } = this.props

    const animatedWidth = { width: this._width }
    const animatedOpacity = { opacity: this._opacity }

    if (loader) {
      animatedWidth.minWidth = 'auto'
    }

    return (
      <Animated.View
        style={[
          styles.container,
          style,
          animatedWidth,

          !this.buttonWidth ? styles.stretch : null,
        ]}
        onLayout={this._onLayout}
      >
        <TouchableOpacity
          style={styles.button}
          onPress={onPress}
          disabled={disabled}
          activeOpacity={0.95}
        >
          {loader ? (
            <ActivityIndicator
              animating
              size="large"
              color="white"
              style={styles.loader}
            />
          ) : (
            <Animated.Text style={[styles.title, animatedOpacity]}>
              {title}
            </Animated.Text>
          )}
        </TouchableOpacity>
      </Animated.View>
    )
  }

  _onLayout = e => {
    if (!this.buttonWidth) {
      this.buttonWidth = e.nativeEvent.layout.width + 2

      if (!this._width) {
        this._width = new Animated.Value(this.buttonWidth)
      }
    }
  }

  componentWillMount() {
    this._opacity = new Animated.Value(1)

    const { loader } = this.props

    if (loader) {
      this._width = new Animated.Value(loaderWidth)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.loader && nextProps.loader) {
      setTimeout(() => {
        this._showLoader()
      }, 0)
    }

    if (this.props.loader && !nextProps.loader) {
      setTimeout(() => {
        this._hideLoader()
      }, 0)
    }
  }

  _showLoader = () => {
    this._opacity.setValue(0)

    Animated.timing(this._width, {
      toValue: loaderWidth,
      duration: 200,
    }).start()
  }

  _hideLoader = () => {
    Animated.timing(this._width, {
      toValue: this.buttonWidth,
      duration: 300,
    }).start()

    Animated.timing(this._opacity, {
      toValue: 1,
      duration: 700,
    }).start()
  }
}
