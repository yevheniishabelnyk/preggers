/*
 *
 * ShareTextInput
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TextInput, Animated, Image, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

import { isFunction } from 'lodash'

import styles from './styles'

export default class ShareTextInput extends React.Component {
  static propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.string,
    returnKeyType: PropTypes.string,
    keyboardType: PropTypes.string,
    autoCapitalize: PropTypes.string,
    error: PropTypes.bool,
    autoCorrect: PropTypes.bool,
    enablesReturnKeyAutomatically: PropTypes.bool,
    onChange: PropTypes.func,
    onChangeText: PropTypes.func,
    onKeyPress: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    numberOfLines: PropTypes.number,
    maxLength: PropTypes.number,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  static defaultProps = {
    autoCorrect: false,
  }

  state = {
    isFocused: false,
  }

  constructor(props) {
    super(props)

    this._animatedIsFocused = new Animated.Value(props.value === '' ? 0 : 1)
  }

  render() {
    const {
      value,
      onChangeText,
      onChange,
      style,
      placeholder = '',
      numberOfLines = 1,
      returnKeyType,
      enablesReturnKeyAutomatically,
      onKeyPress,
      keyboardType,
      autoCapitalize,
      autoCorrect,
      onSubmitEditing,
      maxLength,
      error,
    } = this.props

    const resizeFactor = 0.35

    const labelStyle = {
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [
          Platform.OS === 'android'
            ? moderateScale(18, resizeFactor)
            : moderateScale(22, resizeFactor),
          moderateScale(15, resizeFactor),
        ],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [
          moderateScale(14, resizeFactor),
          moderateScale(10, resizeFactor),
        ],
      }),
    }

    return (
      <View>
        <View style={styles.container}>
          {placeholder ? (
            <Animated.Text
              style={[
                styles.label,
                labelStyle,
                error ? styles.errorTitle : null,
              ]}
            >
              {placeholder}
            </Animated.Text>
          ) : null}

          <TextInput
            multiline={numberOfLines > 1}
            numberOfLines={numberOfLines}
            onChangeText={onChangeText}
            maxLength={maxLength}
            onChange={onChange}
            placeholderTextColor="#8392a7"
            autoCapitalize={autoCapitalize}
            value={value}
            style={[styles.textInput, style]}
            returnKeyType={returnKeyType}
            enablesReturnKeyAutomatically={enablesReturnKeyAutomatically}
            onKeyPress={onKeyPress}
            keyboardType={keyboardType}
            autoCorrect={autoCorrect}
            onSubmitEditing={onSubmitEditing}
            onFocus={this._handleFocus}
            onBlur={this._handleBlur}
            underlineColorAndroid="transparent"
          />

          {error ? (
            <Image
              source={require('assets/icons/error-icon.png')}
              style={styles.errorIcon}
            />
          ) : null}
        </View>
      </View>
    )
  }

  componentDidUpdate() {
    const { value } = this.props

    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || value !== '' ? 1 : 0,
      duration: 200,
    }).start()
  }

  _handleFocus = () => {
    const { onFocus } = this.props

    this.setState({ isFocused: true })

    if (isFunction(onFocus)) {
      onFocus()
    }
  }

  _handleBlur = () => {
    const { onBlur } = this.props

    this.setState({ isFocused: false })

    if (isFunction(onBlur)) {
      onBlur()
    }
  }
}
