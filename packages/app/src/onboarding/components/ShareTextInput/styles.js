import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignSelf: 'stretch',
    borderRadius: moderateScale(8),
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    height: moderateScale(60, resizeFactor),
    borderWidth: moderateScale(1),
    borderColor: 'transparent',
    justifyContent: 'center',
    shadowOpacity: 0.05,
    shadowRadius: 6,
    shadowColor: '#000000',
    shadowOffset: { height: 2, width: -2 },
  },

  label: {
    position: 'absolute',
    left: moderateScale(18, resizeFactor),
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },

  errorTitle: {
    color: 'rgb(255, 100, 101)',
  },

  errorIcon: {
    position: 'absolute',
    width: moderateScale(20),
    height: moderateScale(20),
    top: moderateScale(19, resizeFactor),
    right: moderateScale(25, resizeFactor),
  },

  value: {
    textAlign: 'right',
    alignSelf: 'center',
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    marginLeft: 'auto',
    fontFamily: 'Now-Medium',
    color: 'rgb(55,55,64)',
  },

  textInput: {
    height: moderateScale(60, resizeFactor),
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(55,55,64)',
    flex: 1,
    paddingLeft: moderateScale(18, resizeFactor),
    paddingTop: moderateScale(18, resizeFactor),
  },
})

export default styles
