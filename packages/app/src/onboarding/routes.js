/*
 *
 * Onboarding flow routes
 *
 */

import EnterName from './screens/EnterName'
import ChooseType from './screens/ChooseType'
import EnterPregnancyDueDate from './screens/EnterPregnancyDueDate'
import CalculateDate from './screens/CalculateDate'
import ChooseToConnect from './screens/ChooseToConnect'
import ConnectWithPartner from './screens/ConnectWithPartner'
import ConnectionRequested from './screens/ConnectionRequested'
import ModernaOnboardingInsuranceOffer from './screens/ModernaOnboardingInsuranceOffer'
import EnterInvitationCode from './screens/EnterInvitationCode'
import InvitationAccepted from './screens/InvitationAccepted'
import Competition from '@/competitions/screens/Competition'

export default {
  '/onboarding-enter-name': {
    screen: EnterName,
  },

  '/onboarding-choose-type': {
    screen: ChooseType,
  },

  '/onboarding-enter-pregnancy-due-date': {
    screen: EnterPregnancyDueDate,
  },

  '/onboarding-calculate-date': {
    screen: CalculateDate,
  },

  '/onboarding-choose-to-connect': {
    screen: ChooseToConnect,
  },

  '/onboarding-connect-with-partner': {
    screen: ConnectWithPartner,
  },

  '/onboarding-connection-requested': {
    screen: ConnectionRequested,
  },

  '/onboarding-safety-offer': {
    screen: ModernaOnboardingInsuranceOffer,
  },

  '/competition': {
    screen: Competition,
  },

  '/onboarding-enter-invitation-code': {
    screen: EnterInvitationCode,
  },

  '/onboarding-invitation-accepted': {
    screen: InvitationAccepted,
  },
}
