import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Congrats!',
  description: 'When is your estimated due date?',
  dueDateFieldTitle: 'My due date is',
  helpButtonTitle: 'Help me calculate',
  nextButtonTitle: 'Start pregnancy',
  dueDateErrorTitle: 'Due date is wrong',
})
