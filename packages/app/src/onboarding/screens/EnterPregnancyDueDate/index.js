/*
 *
 * EnterPregnancyDueDate screen
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import moment from 'moment'

import { TouchableOpacity } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import OnboardingDatePicker from '@/onboarding/components/OnboardingDatePicker'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import NextButton from '@/onboarding/components/NextButton'
import GoBackButton from '@/onboarding/components/GoBackButton'
import FormError from '@/forms/components/atoms/FormError'

import Pregnancy from '@/app/database/models/Pregnancy'

import * as TempActions from '@/app/redux/temp'
import * as OnboardingActions from '@/app/redux/onboarding/actions'

import * as UserTypes from '@/app/constants/UserTypes'
import * as AdActions from '@/app/redux/ads/actions'
import {
  AD_TYPE_ONBOARDING,
  CAMPAIGN_ROUTE_MAP,
} from '@/app/redux/ads/constants'

import {
  getPartnerConnections,
  getUserChildren,
  getPregnancies,
  getAdsByType,
} from '@/app/redux/selectors'
import { FormattedMessage } from '@/vendor/react-intl-native'

import messages from './messages'
import styles from './styles'

@connect(state => ({
  onboarding: state.temp.onboarding || {},
  deviceCountryCode: state.device.country,
  ads: state.ads || {},
  partnerConnections: getPartnerConnections(state),
  children: getUserChildren(state),
  pregnancies: getPregnancies(state),
  onboardingAds: getAdsByType(state, AD_TYPE_ONBOARDING),
  locale: state.settings.locale,
}))
export default class EnterPregnancyDueDate extends BaseScreen {
  static propTypes = {
    onboarding: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    isFetchingAds: false,
  }

  constructor(props) {
    super(props)

    this.state = {
      pregnancyDueDate: props.onboarding.pregnancyDueDate || '',
    }

    this.dueDateInitialDate = moment()
      .add(266, 'days')
      .toDate()

    this.dueDateMinDate = new Date()
    this.dueDateMaxDate = moment()
      .add(266, 'days')
      .toDate()
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <OnboardingTopBlock
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
        />

        <FormattedMessage
          {...messages.dueDateFieldTitle}
          style={styles.dueDateFieldTitle}
        />

        <OnboardingDatePicker
          onSubmit={this._onSelectDate}
          value={this.state.pregnancyDueDate}
          initialDate={this.dueDateInitialDate}
          minimumDateLimit={this.dueDateMinDate}
          maximumDateLimit={this.dueDateMaxDate}
          style={styles.datePicker}
          error={this.state.dueDateError}
        />

        <FormError
          isVisible={this.state.dueDateError}
          title={formatMessage(messages.dueDateErrorTitle)}
          style={styles.error}
        />

        <TouchableOpacity
          style={styles.helpButton}
          onPress={this._helpButtonPressed}
          activeOpacity={0.95}
        >
          <FormattedMessage
            {...messages.helpButtonTitle}
            style={styles.helpButtonTitle}
          />
        </TouchableOpacity>

        <NextButton
          style={styles.nextButton}
          title={formatMessage(messages.nextButtonTitle)}
          onPress={this._nextButtonPressed}
          loader={this.state.isFetchingAds}
        />
      </ViewWrapper>
    )
  }

  _onSelectDate = (date, value, isDateBetween) => {
    this.setState({ isDateBetween, dueDateError: false })

    this._setPregnancyDueDate(date)
  }

  _setPregnancyDueDate = date => {
    const pregnancyDueDate = moment(date).format('YYYY-MM-DD')

    if (pregnancyDueDate) {
      this._(
        TempActions.update('onboarding', {
          pregnancyDueDate: { $set: pregnancyDueDate },
        })
      )

      this.setState({ pregnancyDueDate })
    }
  }

  _fetchAds = async () => {
    const { profile } = this.props.screenProps
    const { deviceCountryCode, onboarding, partnerEmail, locale } = this.props

    const requestBody = {
      types: [AD_TYPE_ONBOARDING],
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      userType: onboarding.userType,
      pregnancies: [this.state.dueDate],
      partnerEmail,
    }

    try {
      await this._(AdActions.fetchAds(requestBody))
    } catch (e) {
      console.log(e)
    }
  }

  _helpButtonPressed = () => this._goTo('/onboarding-calculate-date')

  _nextButtonPressed = async () => {
    const { onboarding } = this.props
    const { isDateBetween } = this.state

    if (!isDateBetween) {
      this.setState({ dueDateError: true })
      return
    }

    if (onboarding.userType === UserTypes.MOTHER) {
      return this._goTo('/onboarding-choose-to-connect')
    }

    const pregnancyData = Pregnancy.getProgress(
      onboarding.pregnancyDueDate,
      '39+6'
    )
    const { passedDays } = pregnancyData

    if (
      passedDays > 28 &&
      this.props.onboarding.userType === UserTypes.PARTNER &&
      onboarding.pregnancyDueDate
    ) {
      this.setState({ isFetchingAds: true })
      await this._fetchAds()
      this.setState({ isFetchingAds: false })
      const onboardingAd = Object.values(this.props.onboardingAds || [])[0]

      if (onboardingAd && CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign]) {
        return this._goTo(CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign], {
          ad: onboardingAd,
          dueDate: onboarding.pregnancyDueDate,
          navigationCallback: async () => {
            if (!this.state.isRequesting) {
              this.setState({ isRequesting: true })

              try {
                await this._(OnboardingActions.startApp())
              } catch (err) {
                this.setState({ isRequesting: false })
              }
            }
          },
        })
      }
    }

    if (!this.state.isRequesting) {
      this.setState({ isRequesting: true })

      try {
        await this._(OnboardingActions.startApp())
      } catch (err) {
        this.setState({ isRequesting: false })
      }
    }
  }
}
