import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
    paddingHorizontal: moderateScale(22, 3),
    paddingTop: moderateScale(21),
    paddingBottom: moderateScale(25),
    alignItems: 'center',
  },

  dueDateFieldTitle: {
    fontSize: moderateScale(20),
    fontFamily: 'Now-Medium',
    color: '#000',
    textAlign: 'center',
    marginBottom: moderateScale(20),
    marginTop: moderateScale(60),
    alignSelf: 'center',
  },

  helpButton: {
    marginTop: moderateScale(25),
  },

  helpButtonTitle: {
    fontSize: moderateScale(13),
    fontFamily: 'Now-Medium',
    color: '#000',
  },

  error: {
    marginLeft: moderateScale(18, 2.6),
  },
})

export default styles
