/*
 *
 * Onboarding Due Date screen
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import moment from 'moment/min/moment-with-locales'

import * as UserTypes from '@/app/constants/UserTypes'

import { Text, View } from 'react-native'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import OnboardingDatePicker from '@/onboarding/components/OnboardingDatePicker'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import NextButton from '@/onboarding/components/NextButton'
import GoBackButton from '@/onboarding/components/GoBackButton'
import FormError from '@/forms/components/atoms/FormError'

import * as TempActions from '@/app/redux/temp'
import * as OnboardingActions from '@/app/redux/onboarding/actions'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@connect(state => ({
  onboarding: state.temp.onboarding || {},
}))
export default class CalculateDate extends BaseScreen {
  static propTypes = {
    onboarding: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      lastPeriod: '',
      conception: '',
      periodDate: new Date(),
      pregnancyDueDate: '',
    }

    this.periodMinDate = moment()
      .subtract(266, 'days')
      .toDate()
    this.periodMaxDate = new Date()

    this.conceptionMinDate = moment()
      .subtract(266, 'days')
      .toDate()
    this.conceptionMaxDate = new Date()

    const { locale } = props.screenProps
    moment.locale(locale)
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isRequesting } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <OnboardingTopBlock title={formatMessage(messages.title)} />

        <View style={styles.lastPeriodContainer}>
          <FormattedMessage
            {...messages.lastPeriodFieldTitle}
            style={styles.fieldTitle}
          />

          <OnboardingDatePicker
            onSubmit={this._calculateDueDateByLastPeriod}
            value={this.state.lastPeriod}
            minimumDateLimit={this.periodMinDate}
            maximumDateLimit={this.periodMaxDate}
            error={this.state.dueDateByLastPeriodError}
          />

          <FormError
            isVisible={this.state.dueDateByLastPeriodError}
            style={styles.error}
            title={formatMessage(messages.dueDateErrorTitle)}
          />
        </View>

        <View style={styles.conceptionContainer}>
          <FormattedMessage
            {...messages.conceptionDateFieldTitle}
            style={styles.fieldTitle}
          />

          <OnboardingDatePicker
            onSubmit={this._calculateDueDateByConception}
            value={this.state.conception}
            minimumDateLimit={this.conceptionMinDate}
            maximumDateLimit={this.conceptionMaxDate}
            error={this.state.dueDateByConceptionError}
          />

          <FormError
            isVisible={this.state.dueDateByConceptionError}
            style={styles.error}
            title={formatMessage(messages.dueDateErrorTitle)}
          />
        </View>

        {this.state.pregnancyDueDate ? (
          <View style={styles.calculatedDateContainer}>
            <FormattedMessage
              {...messages.calculatedDateTitle}
              style={styles.calculatedDateTitle}
            />

            <Text style={styles.calculatedDate}>
              {moment(this.state.pregnancyDueDate, 'YYYY-MM-DD').format(
                'DD MMM. YYYY'
              )}
            </Text>
          </View>
        ) : null}

        <NextButton
          style={styles.nextButton}
          title={formatMessage(messages.nextButtonTitle)}
          onPress={this._nextButtonPressed}
          loader={isRequesting}
        />
      </ViewWrapper>
    )
  }

  _calculateDueDateByLastPeriod = (value, date, isDateBetween) => {
    const lastPeriod = value
    const conception = ''

    const pregnancyDueDate = moment(date)
      .add(280, 'days')
      .format('YYYY-MM-DD')

    this.setState({
      pregnancyDueDate,
      lastPeriod,
      conception,
      isDateBetween,
      dueDateByLastPeriodError: false,
      dueDateByConceptionError: false,
    })
  }

  _calculateDueDateByConception = (value, date, isDateBetween) => {
    const conception = value
    const lastPeriod = ''

    const pregnancyDueDate = moment(date)
      .add(266, 'days')
      .format('YYYY-MM-DD')

    this.setState({
      pregnancyDueDate,
      conception,
      lastPeriod,
      isDateBetween,
      dueDateByLastPeriodError: false,
      dueDateByConceptionError: false,
    })
  }

  _nextButtonPressed = async () => {
    const { onboarding } = this.props
    const { pregnancyDueDate, isDateBetween, lastPeriod } = this.state

    if (!isDateBetween) {
      if (lastPeriod) {
        this.setState({ dueDateByLastPeriodError: true })
      } else {
        this.setState({ dueDateByConceptionError: true })
      }

      return
    }

    if (pregnancyDueDate) {
      this._(
        TempActions.update('onboarding', {
          pregnancyDueDate: { $set: pregnancyDueDate },
        })
      )

      if (onboarding.userType === UserTypes.MOTHER) {
        this._goTo('/onboarding-choose-to-connect')
      } else {
        this._startApp()
      }
    }
  }

  _startApp = async () => {
    if (!this.state.isRequesting) {
      this.setState({ isRequesting: true })

      try {
        await this._(OnboardingActions.startApp())
      } catch (err) {
        this.setState({ isRequesting: false })
      }
    }
  }
}
