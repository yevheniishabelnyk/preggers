import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
    paddingHorizontal: moderateScale(22, 3),
    paddingTop: moderateScale(21),
    paddingBottom: moderateScale(25),
    alignItems: 'center',
  },

  fieldTitle: {
    fontSize: moderateScale(15),
    fontFamily: 'Now-Medium',
    color: '#000',
    lineHeight: moderateScale(33),
    alignSelf: 'center',
    textAlign: 'center',
  },

  helpTextContainer: {
    marginTop: moderateScale(25),
  },

  helpText: {
    fontSize: moderateScale(13),
    fontFamily: 'Now-Medium',
    color: 'white',
  },

  lastPeriodContainer: {
    alignItems: 'center',
  },

  conceptionContainer: {
    marginTop: moderateScale(5),
    alignItems: 'center',
  },

  calculatedDateContainer: {
    marginTop: moderateScale(5),
  },

  calculatedDateTitle: {
    color: '#000',
    textAlign: 'center',
    lineHeight: moderateScale(30),
    fontSize: moderateScale(15),
    fontFamily: 'Now-Bold',
  },

  calculatedDate: {
    color: '#000',
    textAlign: 'center',
    fontSize: moderateScale(25),
    fontFamily: 'Now-Bold',
  },

  error: {
    marginLeft: moderateScale(18),
  },
})

export default styles
