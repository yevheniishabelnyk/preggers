import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'We will help you calculate',
  lastPeriodFieldTitle: 'The first day of your last period:',
  conceptionDateFieldTitle: 'Or enter the day of the conception:',
  calculatedDateTitle: 'Based on this, your due date is',
  nextButtonTitle: 'Next',
  dueDateErrorTitle: 'Date is wrong',
})
