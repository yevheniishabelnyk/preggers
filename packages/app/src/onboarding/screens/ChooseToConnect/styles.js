import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
    paddingTop: moderateScale(21),
    paddingBottom: moderateScale(29),
  },

  linkContainer: {
    marginTop: moderateScale(120),
    alignItems: 'center',
  },

  topBlock: {
    width: moderateScale(290),
  },
})

export default styles
