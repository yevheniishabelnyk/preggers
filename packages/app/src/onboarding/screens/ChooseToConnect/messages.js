import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Share your journey',
  description:
    'Share your pregnancy journey with someone you love. Connect with your partner, friend or family.',
  connectButtonTitle: 'Connect',
})
