/*
 *
 * ChooseToConnect screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import GoBackButton from '@/onboarding/components/GoBackButton'
import NextButton from '@/onboarding/components/NextButton'
import Skip from '@/onboarding/components/Skip'
import {
  AD_TYPE_ONBOARDING,
  CAMPAIGN_ROUTE_MAP,
} from '@/app/redux/ads/constants'

import { getAdsByType } from '@/app/redux/selectors'
import Pregnancy from '@/app/database/models/Pregnancy'

import * as AdActions from '@/app/redux/ads/actions'
import * as PregnanciesActions from '@/app/database/actions/pregnancies'
import * as OnboardingActions from '@/app/redux/onboarding/actions'

import messages from './messages'

import styles from './styles'

@connect(state => ({
  onboarding: state.temp.onboarding,
  onboardingAds: getAdsByType(state, AD_TYPE_ONBOARDING),
  ads: state.ads || {},
  locale: state.settings.locale,
}))
export default class ChooseToConnect extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    isFetchingAds: false,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <Skip onPress={this._skipButtonPressed} />

        <OnboardingTopBlock
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
          style={styles.topBlock}
        />

        <NextButton
          title={formatMessage(messages.connectButtonTitle)}
          onPress={this._connectButtonPressed}
        />
      </ViewWrapper>
    )
  }

  componentDidMount() {
    const { profile } = this.props.screenProps
    const { onboarding = {} } = this.props

    if (!profile.isEmpty && !profile.pregnancy && onboarding.pregnancyDueDate) {
      this._(
        PregnanciesActions.add({
          dueDate: onboarding.pregnancyDueDate,
        })
      )
    }
  }

  _skipButtonPressed = async () => {
    const { onboarding } = this.props
    const pregnancyData = Pregnancy.getProgress(
      onboarding.pregnancyDueDate,
      '39+6'
    )
    const { passedDays } = pregnancyData

    if (passedDays > 28) {
      this.setState({ isFetchingAds: true })
      await this._fetchAds()
      this.setState({ isFetchingAds: false })
      const { onboardingAds } = this.props
      const onboardingAd = Object.values(onboardingAds || [])[0]

      if (onboardingAd && CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign]) {
        return this._goTo(CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign], {
          ad: onboardingAd,
          dueDate: onboarding.pregnancyDueDate,
          navigationCallback: async () => {
            if (!this.state.isRequesting) {
              this.setState({ isRequesting: true })

              try {
                await this._(OnboardingActions.startApp())
              } catch (err) {
                this.setState({ isRequesting: false })
              }
            }
          },
        })
      }
    }

    if (!this.state.isRequesting) {
      this.setState({ isRequesting: true })

      try {
        await this._(OnboardingActions.startApp())
      } catch (err) {
        this.setState({ isRequesting: false })
      }
    }
  }

  _connectButtonPressed = () => {
    this._goTo('/onboarding-connect-with-partner')
  }

  _fetchAds = async () => {
    const { profile } = this.props.screenProps
    const { deviceCountryCode, onboarding, locale } = this.props

    const requestBody = {
      types: [AD_TYPE_ONBOARDING],
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      userType: onboarding.userType,
      pregnancies: [onboarding.pregnancyDueDate],
    }

    try {
      await this._(AdActions.fetchAds(requestBody))
    } catch (e) {
      console.log(e)
    }
  }
}
