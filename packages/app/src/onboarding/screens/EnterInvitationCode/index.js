/*
 *
 * EnterInvitationCode screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Keyboard } from 'react-native'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import GoBackButton from '@/onboarding/components/GoBackButton'
import Skip from '@/onboarding/components/Skip'
import NextButton from '@/onboarding/components/NextButton'
import ShareTextInput from '@/onboarding/components/ShareTextInput'
import FormError from '@/forms/components/atoms/FormError'

import { withFirebase } from 'react-redux-firebase'

import { ConnectApi } from '@/app/api/Preggers'

import messages from './messages'

import styles from './styles'

@withFirebase
@connect(state => ({
  onboarding: state.temp.onboarding,
}))
export default class EnterInvitationCode extends BaseScreen {
  static propTypes = {
    firebase: PropTypes.object.isRequired,
    onboarding: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    code: '',
    codeError: false,
    isRequesting: false,
  }

  render() {
    const { codeError, isRequesting, inputFocused } = this.state
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={[
          styles.contentContainerStyle,
          inputFocused ? styles.noFlex : null,
        ]}
        keyboard
        behavior="padding"
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <Skip onPress={this._skipButtonPressed} />

        {codeError ? (
          <OnboardingTopBlock
            title={formatMessage(messages.errorTitle)}
            description={codeError}
            style={styles.errorTopBlock}
          />
        ) : (
          <OnboardingTopBlock
            title={formatMessage(messages.title)}
            description={formatMessage(messages.description)}
            style={styles.topBlock}
          />
        )}

        <View style={styles.inputContainer}>
          <ShareTextInput
            placeholder={formatMessage(messages.codeFieldTitle)}
            value={this.state.code}
            keyboardType="numeric"
            returnKeyType="done"
            enablesReturnKeyAutomatically
            style={styles.input}
            onChangeText={this._onChangeText}
            error={this.state.errorEmptyField}
            onSubmitEditing={this._verifyCode}
            maxLength={10}
            isRequesting={isRequesting}
            onFocus={this._onFocus}
            onBlur={this._onBlur}
          />
        </View>

        <FormError
          isVisible={this.state.errorEmptyField}
          title={formatMessage(messages.errorEmptyFieldTitle)}
          style={styles.error}
        />

        <NextButton
          title={formatMessage(messages.connectButtonTitle)}
          onPress={this._verifyCode}
          loader={isRequesting}
          style={inputFocused ? styles.nextButtonMargin : null}
        />
      </ViewWrapper>
    )
  }

  _onFocus = () => {
    this.setState({ inputFocused: true })
  }

  _onBlur = () => {
    setTimeout(() => {
      this.setState({ inputFocused: false })
    }, 100)
  }

  _skipButtonPressed = () => this._goTo('/onboarding-enter-pregnancy-due-date')

  _verifyCode = async () => {
    const { profile } = this.props.screenProps
    const { firebase, onboarding } = this.props
    const { code, isRequesting } = this.state

    if (!this.state.code) {
      this.setState({ errorEmptyField: true })
    } else if (!isRequesting && code) {
      this.setState({ isRequesting: true })
      Keyboard.dismiss()

      try {
        if (!profile.isEmpty && profile.type !== onboarding.userType) {
          await this.props.firebase.updateProfile({ type: onboarding.userType })
        }

        const response = await ConnectApi.acceptInvitation(null, code)

        const { connectionId, pregnancyId } = response.body

        await firebase.updateProfile({
          connection: connectionId,
          pregnancy: pregnancyId,
        })

        this.setState({ isRequesting: false })

        this._goTo('/onboarding-invitation-accepted')
      } catch (err) {
        console.info('err: ', JSON.stringify(err, null, 4))

        const { formatMessage } = this.context.intl

        let errorMessage

        switch (err.code) {
          case 'connect/invitation-was-resolved':
            errorMessage = formatMessage(messages.errorInvitationHasExpired)
            break

          case 'connect/from-user-already-connected-with-another-user': {
            const payload = err.payload

            const name =
              payload && payload.fromUserName
                ? payload.fromUserName
                : formatMessage(messages.mother)

            errorMessage = formatMessage(messages.errorFromUserHasConnection, {
              name,
            })
            break
          }

          case 'connect/to-user-already-connected-with-another-user':
            errorMessage = formatMessage(messages.errorToUserHasConnection)
            break

          case 'connect/to-user-type-is-not-one-of-partner-types':
            errorMessage = formatMessage(messages.errorToUserWrongType)
            break

          case 'connect/from-user-type-is-not-one-of-mother-types':
            errorMessage = formatMessage(messages.errorFromUserInvalidType)
            break

          case 'connect/permission-denied':
            errorMessage = formatMessage(
              messages.errorRequestUserIsNotInvitationToUser
            )
            break

          case 'connect/invitation-is-missing':
            errorMessage = formatMessage(messages.errorBadRequest)
            break

          case 'network/offline':
            errorMessage = formatMessage(messages.errorNetwork)
            break

          case 'connect/wrong-code':
            errorMessage = formatMessage(messages.errorWrongCode)
            break

          default:
            errorMessage = formatMessage(messages.errorBadRequest)
        }

        this.setState({ codeError: errorMessage, isRequesting: false })
      }

      this.setState({ isRequesting: false })
    }
  }

  _onChangeText = value =>
    this.setState({ codeError: false, code: value, errorEmptyField: false })
}
