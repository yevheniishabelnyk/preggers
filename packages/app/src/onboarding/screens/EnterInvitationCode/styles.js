import { StyleSheet } from 'react-native'

import { ifIphoneX } from 'react-native-iphone-x-helper'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  container: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 252)',
  },

  contentContainerStyle: {
    flex: 1,
    padding: moderateScale(20, 2),
  },

  topBlock: {
    width: moderateScale(280),
  },

  errorTopBlock: {
    width: moderateScale(280),
  },

  inputContainer: {
    alignSelf: 'stretch',
    marginTop: moderateScale(80),
  },

  noFlex: {
    flex: 0,
  },

  nextButtonMargin: {
    marginTop: moderateScale(100),
    ...ifIphoneX({
      marginTop: moderateScale(200),
    }),
  },

  error: {
    marginLeft: moderateScale(18),
    marginTop: moderateScale(10),
  },
})

export default styles
