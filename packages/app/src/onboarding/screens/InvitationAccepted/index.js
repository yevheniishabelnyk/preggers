/*
 *
 * InvitationAccepted screen
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { isEmpty } from 'lodash'

import { Text } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import NextButton from '@/onboarding/components/NextButton'

import { withFirebase } from 'react-redux-firebase'
import * as TempActions from '@/app/redux/temp'
import { NavigationActions } from 'react-navigation'
import * as ProfileActions from '@/app/redux/profile/actions'
import {
  CAMPAIGN_ROUTE_MAP,
  AD_TYPE_ONBOARDING,
} from '@/app/redux/ads/constants'
import {
  getAdsByType,
  getPregnancy,
  getConnection,
} from '@/app/redux/selectors'
import * as AdActions from '@/app/redux/ads/actions'
import Pregnancy from '@/app/database/models/Pregnancy'

import { MailingApi } from '@/app/api/Preggers'

import messages from './messages'
import styles from './styles'

@withFirebase
@connect((state, props) => ({
  onboarding: state.temp.onboarding || {},
  deviceCountryCode: state.device.country,
  ads: state.ads || {},
  onboardingAds: getAdsByType(state, AD_TYPE_ONBOARDING),
  userPregnancy:
    getPregnancy(state, props.screenProps.profile.pregnancy) || undefined,
  userConnection: getConnection(state, props.screenProps.profile.connection),
  locale: state.settings.locale,
}))
export default class InvitationAccepted extends BaseScreen {
  static propTypes = {
    firebase: PropTypes.object,
    onboarding: PropTypes.object,
  }

  static defaultProps = {
    userPregnancy: {},
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static navigationOptions = {
    gesturesEnabled: false,
  }

  state = {
    connectedUserName: '',
    isRequesting: false,
  }

  render() {
    const { connectedUserName, isRequesting } = this.state
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249, 250, 252)"
      >
        <OnboardingTopBlock
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
          style={styles.topBlock}
        />

        <Text style={styles.info}>{connectedUserName}</Text>

        <NextButton
          title={formatMessage(messages.startAppButtonTitle)}
          onPress={this._startAppButtonPressed}
          loader={isRequesting}
        />
      </ViewWrapper>
    )
  }

  async componentWillMount() {
    const {
      screenProps: { profile },
      firebase,
    } = this.props

    try {
      const connectedUserNameSnap = await firebase.watchEvent(
        'once',
        `connections/${profile.connection}/fromName`
      )

      if (connectedUserNameSnap && connectedUserNameSnap.data) {
        const connectedUserName = connectedUserNameSnap.data

        this.setState({ connectedUserName })
      }
    } catch (err) {
      console.log(err)
    }
  }

  _fetchAds = async () => {
    const { profile } = this.props.screenProps
    const {
      deviceCountryCode,
      onboarding,
      userPregnancy,
      userConnection,
      locale,
    } = this.props
    const partnerEmail = userConnection ? userConnection.fromEmail : null

    const requestBody = {
      types: [AD_TYPE_ONBOARDING],
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      userType: onboarding.userType,
      pregnancies: [userPregnancy.dueDate],
      partnerEmail,
    }

    try {
      await this._(AdActions.fetchAds(requestBody))
    } catch (e) {
      console.log(e)
    }
  }

  _startAppButtonPressed = async () => {
    const { profile } = this.props.screenProps
    const { firebase, onboarding, userPregnancy } = this.props

    try {
      this.setState({ isRequesting: true })

      const userDiff = {}

      if (onboarding.userName && profile.name !== onboarding.userName) {
        userDiff.name = onboarding.userName
      }

      if (onboarding.userType && profile.type !== onboarding.userType) {
        userDiff.type = onboarding.userType
      }

      if (!isEmpty(userDiff)) {
        await firebase.updateProfile(userDiff)

        if (userDiff.name) {
          await this._(
            ProfileActions.updateFirebaseProfileDisplayName(userDiff.name)
          )
        }
      }

      let navigationCallback

      if (profile.pregnancy) {
        const pregnancyData = Pregnancy.getProgress(
          userPregnancy.dueDate,
          userPregnancy.lengthType
        )
        const { passedDays, pregnancyWeek } = pregnancyData

        if (passedDays > 28) {
          try {
            await this._fetchAds()
          } catch (err) {
            console.log('[ADS] Failed fetching onboarding ad', err)
          }
        }

        navigationCallback = () => {
          this._(
            NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: '/~home',
                  action: NavigationActions.navigate({
                    routeName: '/~start-view',
                    action: NavigationActions.navigate({
                      routeName: '/~pregnancy',
                      params: {
                        pregnancyId: profile.pregnancy,
                        pregnancyCurrentWeekNumber: pregnancyWeek,
                        activeWeekNumber: pregnancyWeek,
                      },
                    }),
                  }),
                }),
              ],
            })
          )

          MailingApi.sendNewUserWelcomeEmail().catch(err =>
            console.log('Error sending new user welcome email', err)
          )
        }
      } else {
        navigationCallback = () => {
          this._(
            NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: '/~home',
                }),
              ],
            })
          )

          MailingApi.sendNewUserWelcomeEmail().catch(err =>
            console.log('Error sending new user welcome email', err)
          )
        }
      }

      this.setState({ isRequesting: false })

      const { onboardingAds } = this.props
      const onboardingAd = Object.values(onboardingAds || [])[0]
      if (onboardingAd && CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign]) {
        this._goTo(CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign], {
          ad: onboardingAd,
          dueDate: onboarding.pregnancyDueDate,
          navigationCallback,
        })
      } else {
        navigationCallback()
      }

      this._(TempActions.remove('onboarding'))
    } catch (err) {
      console.log('err', err)

      this.setState({ isRequesting: false })

      this._(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: '/~home',
            }),
          ],
        })
      )
    }
  }
}
