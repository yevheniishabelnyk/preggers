import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
    paddingTop: moderateScale(21),
    paddingBottom: moderateScale(25),
    paddingHorizontal: moderateScale(44),
  },

  topBlock: {
    width: moderateScale(324),
  },

  info: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(20),
    textAlign: 'center',
    lineHeight: moderateScale(33),
  },
})

export default styles
