import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Share your pregnancy journey',
  description:
    'Connect with your partner or family. Let the people you love follow your journey',
  userTypeSelectTitle: 'Connection With',
  emailFieldTitle: 'E-mail',
  SMSFieldTitle: 'Or SMS',
  connectFormError: 'You need to fill in an e-mail to connect',
  errorEmailTitle: 'Not a valid e-mail',
  errorToUserIsMotherType:
    'You have registered as a mother. You can not invite another mother to follow your pregnancy. You can only invite a user which has register as partner, father, friend or family.',
  errorConnectingToSelf: 'You can not connect to yourself.',
  shareFormTitle: 'Share',
  shareFormBabyNamesSwitchTitle: 'Baby Names',
  shareFormContractionTimerSwitchTitle: 'Contraction timer',
  shareFormTimelineSwitchTitle: 'Timeline',
  shareFormChecklistSwitchTitle: 'Checklist',
  connectButtonTitle: 'Connect',
  shareRequiredError: 'You need to choose what to share',
  fatherUserType: 'Father',
  partnerUserType: 'Partner',
  friendUserType: 'Friend',
  familyUserType: 'Family',
})
