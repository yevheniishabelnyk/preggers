/*
 *
 * ConnectWithPartner screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Keyboard } from 'react-native'

import * as ConnectionTypes from '@/app/constants/ConnectionTypes'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import GoBackButton from '@/onboarding/components/GoBackButton'
import NextButton from '@/onboarding/components/NextButton'
import Skip from '@/onboarding/components/Skip'
import {
  CAMPAIGN_ROUTE_MAP,
  AD_TYPE_ONBOARDING,
} from '@/app/redux/ads/constants'
import FormSelect from '@/onboarding/components/FormSelect'
import SwitchField from '@/onboarding/components/SwitchField'
import FormError from '@/forms/components/atoms/FormError'
import { getAdsByType } from '@/app/redux/selectors'
import OnboardingTextInput from '@/onboarding/components/OnboardingTextInput'
import Pregnancy from '@/app/database/models/Pregnancy'

import * as AdActions from '@/app/redux/ads/actions'
import * as PregnanciesActions from '@/app/database/actions/pregnancies'
import * as OnboardingActions from '@/app/redux/onboarding/actions'

import { isValidEmail } from '@/shared/utils/validators'

import { ConnectApi } from '@/app/api/Preggers'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@connect(state => ({
  onboarding: state.temp.onboarding,
  onboardingAds: getAdsByType(state, AD_TYPE_ONBOARDING),
  ads: state.ads || {},
  locale: state.settings.locale,
}))
export default class ConnectWithPartner extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    isFetchingAds: false,
  }

  constructor(props, context) {
    super(props)

    const { formatMessage } = context.intl

    this.userStatuses = [
      {
        label: formatMessage(messages.fatherUserType),
        value: ConnectionTypes.FATHER,
      },
      {
        label: formatMessage(messages.partnerUserType),
        value: ConnectionTypes.PARTNER,
      },
      {
        label: formatMessage(messages.familyUserType),
        value: ConnectionTypes.FAMILY,
      },
      {
        label: formatMessage(messages.friendUserType),
        value: ConnectionTypes.FRIEND,
      },
    ]

    this.state = {
      relationshipType: ConnectionTypes.FATHER,
      email: '',
      shareBabyNames: true,
      shareTimeline: true,
      shareChecklist: true,
      shareContractionTimer: true,
      emailMSISDNError: false,
      emailErrorText: null,
      shareError: null,
      isButtonDisabled: false,
      isRequesting: false,
    }
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isButtonDisabled, isRequesting } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        keyboard
        scrollable
        behavior="padding"
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <Skip onPress={this._skipButtonPressed} />

        <OnboardingTopBlock
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
          style={styles.topBlock}
        />

        <View style={styles.form}>
          <View style={styles.selectField}>
            <FormSelect
              title={formatMessage(messages.userTypeSelectTitle)}
              style={styles.textInput}
              value={this.state.relationshipType}
              items={this.userStatuses}
              onSelect={this._setRelationshipType}
            />
          </View>

          <View style={styles.inputWrapper}>
            <OnboardingTextInput
              placeholder={formatMessage(messages.emailFieldTitle)}
              value={this.state.email.toLowerCase()}
              onChangeText={this._setEmail}
              keyboardType="email-address"
              returnKeyType="done"
              autoCapitalize="none"
              enablesReturnKeyAutomatically
              error={this.state.emailMSISDNError}
              isRequesting={isRequesting}
            />

            <FormError
              isVisible={this.state.emailMSISDNError}
              title={this.state.emailErrorText}
              style={styles.errorText}
            />
          </View>
        </View>

        <View style={styles.shareContainer}>
          <FormattedMessage
            {...messages.shareFormTitle}
            style={styles.shareFormTitle}
          />

          <SwitchField
            title={formatMessage(messages.shareFormBabyNamesSwitchTitle)}
            onChange={this._setShareBabyNames}
            value={this.state.shareBabyNames}
            error={this.state.shareError}
          />

          {/* <SwitchField
            title={formatMessage(
              messages.shareFormContractionTimerSwitchTitle
            ).toUpperCase()}
            onChange={this._setValue.bind(null, 'shareContractionTimer')}
            value={this.state.shareContractionTimer}
            error={this.state.shareError}
          />

          <SwitchField
            title={formatMessage(
              messages.shareFormTimelineSwitchTitle
            ).toUpperCase()}
            onChange={this._setValue.bind(null, 'shareTimeline')}
            value={this.state.shareTimeline}
            error={this.state.shareError}
          />

          <SwitchField
            title={formatMessage(
              messages.shareFormChecklistSwitchTitle
            ).toUpperCase()}
            onChange={this._setValue.bind(null, 'shareChecklist')}
            value={this.state.shareChecklist}
            error={this.state.shareError}
          /> */}

          <FormError
            isVisible={this.state.shareError}
            title={formatMessage(messages.shareRequiredError)}
            style={styles.errorText}
          />
        </View>

        <View
          style={isButtonDisabled ? styles.disabledButton : styles.enableButton}
        >
          <NextButton
            title={formatMessage(messages.connectButtonTitle)}
            onPress={this._sendRequest}
            style={styles.sendButton}
            disabled={isButtonDisabled}
            loader={isRequesting}
          />
        </View>
      </ViewWrapper>
    )
  }

  componentDidMount() {
    const { profile } = this.props.screenProps
    const { onboarding = {} } = this.props

    if (!profile.isEmpty && !profile.pregnancy && onboarding.pregnancyDueDate) {
      this._(
        PregnanciesActions.add({
          dueDate: onboarding.pregnancyDueDate,
        })
      )
    }
  }

  _skipButtonPressed = async () => {
    const { onboarding } = this.props
    const pregnancyData = Pregnancy.getProgress(
      onboarding.pregnancyDueDate,
      '39+6'
    )
    const { passedDays } = pregnancyData

    if (passedDays > 28) {
      this.setState({ isFetchingAds: true })
      await this._fetchAds()
      this.setState({ isFetchingAds: false })
      const { onboardingAds } = this.props
      const onboardingAd = Object.values(onboardingAds || [])[0]

      if (onboardingAd && CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign]) {
        return this._goTo(CAMPAIGN_ROUTE_MAP[onboardingAd.customCampaign], {
          ad: onboardingAd,
          dueDate: onboarding.pregnancyDueDate,
          navigationCallback: async () => {
            if (!this.state.isRequesting) {
              this.setState({ isRequesting: true })

              try {
                await this._(OnboardingActions.startApp())
              } catch (err) {
                this.setState({ isRequesting: false })
              }
            }
          },
        })
      }
    }

    if (!this.state.isRequesting) {
      this.setState({ isRequesting: true })

      try {
        await this._(OnboardingActions.startApp())
      } catch (err) {
        this.setState({ isRequesting: false })
      }
    }
  }

  _fetchAds = async () => {
    const { profile } = this.props.screenProps
    const { deviceCountryCode, onboarding, locale } = this.props

    const requestBody = {
      types: [AD_TYPE_ONBOARDING],
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      userType: onboarding.userType,
      pregnancies: [onboarding.pregnancyDueDate],
    }

    try {
      await this._(AdActions.fetchAds(requestBody))
    } catch (e) {
      console.log(e)
    }
  }

  _setRelationshipType = value => this._setValue('relationshipType', value)

  _setEmail = value => this._setValue('email', value)

  _setShareBabyNames = value => this._setValue('shareBabyNames', value)

  _setValue = (key, value) =>
    this.setState({
      [key]: value,
      emailMSISDNError: null,
      shareError: null,
      isButtonDisabled: false,
    })

  _sendRequest = async () => {
    const { formatMessage } = this.context.intl

    const {
      email,
      shareContractionTimer,
      shareTimeline,
      shareChecklist,
      shareBabyNames,
      relationshipType,
      isRequesting,
    } = this.state

    if (!isValidEmail(email)) {
      this.setState({
        emailMSISDNError: true,
        emailErrorText: formatMessage(messages.errorEmailTitle),
        isButtonDisabled: true,
      })

      return
    }

    if (
      !(
        shareContractionTimer ||
        shareTimeline ||
        shareChecklist ||
        shareBabyNames
      )
    ) {
      this.setState({
        shareError: true,
        isButtonDisabled: true,
      })

      return
    }

    if (!isRequesting) {
      this.setState({ isRequesting: true })

      Keyboard.dismiss()

      try {
        await ConnectApi.sendInvitation({
          email,
          relationshipType,
          shareContractionTimer,
          shareTimeline,
          shareChecklist,
          shareBabyNames,
        })

        this.setState({ isRequesting: false })

        this._goTo('/onboarding-connection-requested', { partnerEmail: email })
      } catch (err) {
        switch (err.code) {
          case 'connect/to-user-is-one-of-mother-types':
            this.setState({
              emailMSISDNError: true,
              emailErrorText: formatMessage(messages.errorToUserIsMotherType),
            })
            break

          case 'connect/cant-connect-to-yourself':
            this.setState({
              emailMSISDNError: true,
              emailErrorText: formatMessage(messages.errorConnectingToSelf),
            })
            break

          default:
            console.log(err)
            break
        }

        this.setState({ isRequesting: false })
      }
    }
  }
}
