import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'
import { ifIphoneX } from 'react-native-iphone-x-helper'

const gutter = moderateScale(22)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  contentContainerStyle: {
    paddingTop: moderateScale(10),
    paddingBottom: moderateScale(25),
    paddingHorizontal: gutter,
    alignItems: 'center',
  },

  topBlock: {
    width: moderateScale(290),
  },

  form: {
    marginTop: moderateScale(30),
    paddingHorizontal: gutter,

    alignItems: 'center',
  },

  selectField: {
    alignSelf: 'stretch',
  },

  inputWrapper: {
    alignSelf: 'stretch',
  },

  shareContainer: {
    marginBottom: moderateScale(27),
    ...ifIphoneX({
      marginBottom: moderateScale(7),
    }),
    alignItems: 'center',
  },

  shareFormTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(40),
    marginTop: moderateScale(34),
    marginBottom: moderateScale(10),
    alignSelf: 'center',
    color: '#000',
  },

  errorText: {
    fontSize: moderateScale(11),
    fontFamily: 'Now-Medium',
    color: 'rgb(255, 100, 101)',
    marginTop: moderateScale(3),
  },

  button: {
    flex: 1,
    height: moderateScale(55),
    borderRadius: moderateScale(31),
    borderWidth: 1,
    borderColor: 'white',
    overflow: 'hidden',
    alignItems: 'center',
  },

  disabledButton: {
    opacity: 0.5,
  },

  enableButton: {
    opacity: 1,
  },

  loader: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignSelf: 'center',
  },

  closeButton: {
    marginLeft: 'auto',
  },
})

export default styles
