import { defineMessages } from 'react-intl'

export default defineMessages({
  heading3: 'Sign up for the insurance',
  linkText: 'Read more',
  numberOfKidsFieldTitle: 'Children in the pregnancy',
  SSNFieldTitle: 'Civic number',
  phoneFieldTitle: 'Mobile number',
  emailFieldTitle: 'E-mail',
  cancelButtonText: 'No, thanks',
  errorSsn: 'This is not a valid Swedish personal identity number',
  errorPhone: 'This is not a valid Swedish mobile number',
  errorNumberOfChildren: 'Must be at least 1',
  errorEmail: 'Not a valid e-mail address',
  checkboxText:
    'By clicking "Insure us", I confirm that I\'ve read the order information, and that I have read and accept Moderna Försäkringars terms and conditions and privacy policy',
  checklistItem1: 'Medical disability',
  checklistItem2: 'Death benefits',
  checklistItem3: 'Crisis assistance',
  termsError: 'You need to accept the terms',
})
