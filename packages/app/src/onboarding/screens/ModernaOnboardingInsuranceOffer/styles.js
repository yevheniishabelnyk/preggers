import { StyleSheet, Dimensions } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  header: {
    position: 'relative',
    top: 0,
    left: 0,
    height: moderateScale(214),
    width,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  headerImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
    width: '100%',
    height: moderateScale(214),
    overflow: 'visible',
  },

  logo: {
    width: moderateScale(160),
    height: moderateScale(54),
    backgroundColor: 'transparent',
  },

  container: {
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  backButton: {
    ...ifIphoneX({
      top: 25 + moderateScale(20),
    }),
  },

  innerContainer: {
    flex: 1,
    marginTop: moderateScale(12, 4),
    paddingTop: moderateScale(21, 1.1),
    paddingBottom: moderateScale(29),
    paddingHorizontal: moderateScale(22, 3),
    backgroundColor: 'rgb(249, 250, 252)',
  },

  errorText: {
    marginBottom: 0,
    marginTop: moderateScale(11),
    marginLeft: moderateScale(18),
  },

  description: {
    fontSize: moderateScale(14),
    width: moderateScale(320),
  },

  link: {
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    textDecorationColor: '#000',
  },

  form: {
    flex: 1,
  },

  input: {
    marginBottom: moderateScale(25),
  },

  sendButton: {
    marginTop: moderateScale(39),
    marginBottom: moderateScale(8),
    backgroundColor: 'rgb(51, 212, 176)',
    borderColor: 'transparent',
  },

  footer: {
    width: moderateScale(352),
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: moderateScale(10),
    lineHeight: moderateScale(14),
    color: 'rgb(231, 232, 234)',
  },

  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(27),
    color: 'rgb(15, 15, 15)',
    textAlign: 'center',
    marginTop: moderateScale(26),
  },

  h1: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(25),
    lineHeight: moderateScale(31),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    marginBottom: moderateScale(10),
  },

  h2: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(12),
    lineHeight: moderateScale(22),
    color: 'rgb(29, 29, 29)',
    textAlign: 'center',
  },

  h3: {
    fontFamily: 'Now-Black',
    fontSize: moderateScale(15),
    lineHeight: moderateScale(33),
    color: '#000',
    marginTop: moderateScale(40),
    marginBottom: moderateScale(10),
  },

  checklistContainer: {
    marginTop: moderateScale(30),
  },

  checklistItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: moderateScale(15),
  },

  checklistItemIcon: {
    width: moderateScale(17),
    height: moderateScale(13),
  },

  checklistItemIconContainer: {
    width: moderateScale(37),
    height: moderateScale(37),
    borderRadius: moderateScale(18.5),
    backgroundColor: 'rgb(80, 227, 194)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  checklistItemText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    color: '#000',
    marginLeft: moderateScale(14),
  },

  cancelButtonText: {
    flex: 1,
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(15),
    color: 'rgb(11,11,11)',
    textAlign: 'center',
    height: moderateScale(33),
    marginTop: moderateScale(26),
  },

  inputWrapper: {
    marginBottom: moderateScale(11),
  },

  cybexContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  cybexLogo: {
    position: 'relative',
    height: moderateScale(145),
    width: '100%',
    marginRight: moderateScale(10),
    borderRadius: moderateScale(8),
  },
})

export default styles
