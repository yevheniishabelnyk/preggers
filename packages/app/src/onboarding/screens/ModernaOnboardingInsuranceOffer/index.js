/*
 *
 * ModernaOnboardingInsuranceOffer screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image } from 'react-native'

import { get } from 'lodash'

import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import {
  getModernaFreeTodoId,
  getActiveCompetitionUid,
} from '@/app/redux/selectors'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BaseScreen from '@/app/base/components/BaseScreen'
import TermsCheckbox from '@/onboarding/components/TermsCheckbox'
import GoBackButton from '@/onboarding/components/GoBackButton'
import NextButton from '@/onboarding/components/NextButton'
import ShareTextInput from '@/onboarding/components/ShareTextInput'
import FormError from '@/forms/components/atoms/FormError'
import ModernaErrorModal from '@/insurance/components/ModernaErrorModal'

import { ADS_FETCH_HEADERS } from '@/app/redux/ads/constants'
import * as AdActions from '@/app/redux/ads/actions'

import { CONTENT_SERVICE__API_BASE } from 'config'

import messages from './messages'

import styles from './styles'

import * as ChecklistActions from '@/app/database/actions/checklist'

@firebaseConnect(['content/checklistItems', 'competitions/activeCompetitions'])
@connect((state, props) => ({
  ...props.navigation.state.params,
  id: get(state, 'firebase.profile.id', {}),
  email: get(state, 'firebase.profile.email', {}),
  onboarding: state.temp.onboarding || {},
  modernaFreeTodoId: getModernaFreeTodoId(state),
  activeCompetitionUid: getActiveCompetitionUid(state),
}))
export default class ModernaOnboardingInsuranceOffer extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      ssn: '',
      phoneNumber: '',
      email: '',
      validPhone: true,
      validSsn: true,
      validEmail: true,
      validNumberOfChildren: true,
      agreedToTerms: false,
      canSubmit: false,
      numberOfChildren: '1',
      modernaError: false,
      errorCode: false,
      termsError: false,
      isSubmitting: false,
    }
  }

  componentDidMount() {
    const { email, ad } = this.props

    this.setState({ email })

    this._(AdActions.markInteraction(ad, 'impression'))
  }

  render() {
    const { formatMessage } = this.context.intl
    const { ad } = this.props
    const {
      agreedToTerms,
      validPhone,
      validSsn,
      validNumberOfChildren,
      email,
      validEmail,
      numberOfChildren,
      modernaError,
      isSubmitting,
      ssn,
      phoneNumber,
      termsError,
      errorCode,
    } = this.state

    return (
      <KeyboardAwareScrollView style={styles.container}>
        <View style={styles.header}>
          <GoBackButton onPress={this._goBack} style={styles.backButton} />

          <Image
            source={require('assets/img/moderna-free.png')}
            style={styles.headerImage}
          />
        </View>

        <View style={styles.innerContainer}>
          <View style={styles.cybexContainer}>
            <Text style={styles.h2}>{ad.label}</Text>

            <Text style={styles.h1}>{ad.title}</Text>

            <Image source={{ uri: ad.images[0] }} style={styles.cybexLogo} />

            <Text style={styles.text}>
              {`${ad.text} `}

              <Text style={styles.link} onPress={this._readMorePressed}>
                {`${formatMessage(messages.linkText)}.`}
              </Text>
            </Text>
          </View>

          <View style={styles.checklistContainer}>
            {[
              formatMessage(messages.checklistItem1),
              formatMessage(messages.checklistItem2),
              formatMessage(messages.checklistItem3),
            ].map(message => (
              <View key={message} style={styles.checklistItemContainer}>
                <View style={styles.checklistItemIconContainer}>
                  <Image
                    source={require('assets/icons/checked-white.png')}
                    style={styles.checklistItemIcon}
                  />
                </View>

                <Text style={styles.checklistItemText}>{message}</Text>
              </View>
            ))}
          </View>

          <Text style={styles.h3}>{formatMessage(messages.heading3)}</Text>

          <View style={styles.form}>
            <View style={styles.inputWrapper}>
              <ShareTextInput
                placeholder={formatMessage(messages.numberOfKidsFieldTitle)}
                onChangeText={this._onChangeNumberOfChildren}
                value={numberOfChildren}
                maxLength={3}
                returnKeyType="done"
                error={!validNumberOfChildren ? true : false}
              />

              <FormError
                isVisible={!validNumberOfChildren}
                title={formatMessage(messages.errorNumberOfChildren)}
                style={styles.errorText}
              />
            </View>

            <View style={styles.inputWrapper}>
              <ShareTextInput
                value={ssn}
                placeholder={formatMessage(messages.SSNFieldTitle)}
                onChangeText={this._onChangeSsn}
                maxLength={15}
                keyboardType="numeric"
                returnKeyType="done"
                error={!validSsn ? true : false}
              />

              <FormError
                isVisible={!validSsn}
                title={formatMessage(messages.errorSsn)}
                style={styles.errorText}
              />
            </View>

            <View style={styles.inputWrapper}>
              <ShareTextInput
                value={phoneNumber}
                placeholder={formatMessage(messages.phoneFieldTitle)}
                onChangeText={this._onChangeTelephoneNumber}
                maxLength={15}
                keyboardType="numeric"
                returnKeyType="done"
                error={!validPhone ? true : false}
              />

              <FormError
                isVisible={!validPhone}
                title={formatMessage(messages.errorPhone)}
                style={styles.errorText}
              />
            </View>

            <ShareTextInput
              placeholder={formatMessage(messages.emailFieldTitle)}
              onChangeText={this._onChangeEmail}
              value={email}
              returnKeyType="done"
              error={!validEmail ? true : false}
            />

            <FormError
              isVisible={!validEmail}
              title={formatMessage(messages.errorEmail)}
              style={styles.errorText}
            />

            <TermsCheckbox
              text={formatMessage(messages.checkboxText)}
              onChange={this._onCheckboxChange}
              checked={agreedToTerms}
              onTextPress={this._termsPressed}
            />

            <FormError
              isVisible={termsError}
              title={formatMessage(messages.termsError)}
              style={styles.errorText}
            />
          </View>

          <NextButton
            title={ad.buttonLabel}
            style={styles.sendButton}
            onPress={this._sendButtonPressed}
            loader={isSubmitting}
          />

          <Text style={styles.cancelButtonText} onPress={this._goNext}>
            {formatMessage(messages.cancelButtonText)}
          </Text>
        </View>

        <ModernaErrorModal
          visible={modernaError}
          onClose={this._onCloseModal}
          errorCode={errorCode}
        />
      </KeyboardAwareScrollView>
    )
  }

  _onChangeSsn = ssn => this.setState({ ssn })

  _onChangeEmail = email => this.setState({ email })

  _onChangeNumberOfChildren = numberOfChildren =>
    this.setState({ numberOfChildren: numberOfChildren.replace(/[^0-9]/g, '') })

  _onChangeTelephoneNumber = phoneNumber => this.setState({ phoneNumber })

  _onCheckboxChange = agreedToTerms =>
    this.setState({ agreedToTerms, termsError: false })

  _readMorePressed = () => this._goTo('/moderna-pregnancy-insurance-info')

  _termsPressed = () => this._goTo('/moderna-pregnancy-insurance-terms')

  _validateInput = async () => {
    const {
      phoneNumber,
      ssn,
      agreedToTerms,
      numberOfChildren,
      email,
    } = this.state
    let [
      validPhone,
      validSsn,
      validNumberOfChildren,
      validEmail,
      termsError,
    ] = [true, true, true, true, false]

    // Swedish phone number
    if (!phoneNumber.match(/^(\+46|0046|0)7[0-9]{8}$/)) {
      validPhone = false
    }

    // Swedish ssn
    if (!ssn.match(/^(19|20)?(\d{6}(-|\s)\d{4}|(?!19|20)\d{10})$/)) {
      validSsn = false
    }

    if (+numberOfChildren === 0) {
      validNumberOfChildren = false
    }

    if (!email.match(/\S+@\S+\.\S+/)) {
      validEmail = false
    }

    if (!agreedToTerms) {
      termsError = true
    }

    const canSubmit =
      validPhone &&
      validSsn &&
      agreedToTerms &&
      validNumberOfChildren &&
      !termsError &&
      validEmail

    await this.setState({
      validPhone,
      validSsn,
      canSubmit,
      validNumberOfChildren,
      validEmail,
      termsError,
    })
  }

  _sendButtonPressed = async () => {
    await this._validateInput()

    if (this.state.canSubmit) {
      this.setState({ isSubmitting: true })

      await this._submit()

      this.setState({ isSubmitting: false })

      if (!this.state.modernaError) {
        const { activeCompetitionUid } = this.props

        if (activeCompetitionUid) {
          this._goTo('/competition', {
            activeCompetitionUid: activeCompetitionUid.name,
            isNewUser: true,
          })
        } else {
          this.props.navigationCallback()
        }
      }
    }
  }

  _submit = async () => {
    const { id, dueDate, ad } = this.props
    const { ssn, phoneNumber, numberOfChildren, email } = this.state
    const [insuranceType, contentUid] = ['FREE', ad.uid]

    this._(AdActions.markInteraction(ad, 'click'))

    try {
      const response = await fetch(
        `${CONTENT_SERVICE__API_BASE}/moderna/new-insurance/`,
        {
          method: 'POST',
          headers: ADS_FETCH_HEADERS,
          body: JSON.stringify({
            contentUid,
            id,
            email,
            dueDate,
            ssn,
            phoneNumber,
            numberOfChildren,
            insuranceType,
          }),
        }
      )

      if (response.ok) {
        const json = await response.json()

        if (+json.code === 201) {
          const { modernaFreeTodoId } = this.props

          if (modernaFreeTodoId) {
            this._(ChecklistActions.completeAdminTodo(modernaFreeTodoId))
          }
        } else {
          this.setState({ modernaError: true, errorCode: json.code })
        }
      } else {
        this.setState({ modernaError: true })
      }
    } catch (err) {
      console.log(err)
      this.setState({ modernaError: true })
    }
  }

  _onCloseModal = () => this.setState({ modernaError: false })

  _goNext = () => {
    this.props.navigationCallback()
  }
}
