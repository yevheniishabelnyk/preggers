import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 252)',
    paddingHorizontal: moderateScale(50, 2),
    paddingTop: moderateScale(21),
  },

  linkContainer: {
    position: 'relative',
    marginTop: moderateScale(30),
    alignItems: 'center',
  },

  topBlock: {
    width: moderateScale(271),
  },

  link: {
    marginBottom: moderateScale(20, 1.2),
  },
})

export default styles
