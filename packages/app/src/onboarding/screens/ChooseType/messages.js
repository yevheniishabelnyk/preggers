import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Hello {name}!',
  description: 'Happy you want to use Preggers. Who are you?',
  pregnantTypeButtonTitle: 'I’m pregnant',
  partnerTypeButtonTitle: 'I’m the partner',
  familyTypeButtonTitle: 'I’m family',
  friendTypeButtonTitle: 'I’m a friend',
})
