/*
 *
 * ChooseType screen
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import Link from '@/onboarding/components/Link'
import GoBackButton from '@/onboarding/components/GoBackButton'

import * as TempActions from '@/app/redux/temp'

import { has } from 'lodash'

import * as UserTypes from '@/app/constants/UserTypes'

import messages from './messages'

import styles from './styles'

import { withFirebase } from 'react-redux-firebase'

@withFirebase
@connect(state => ({
  onboarding: state.temp.onboarding,
  firebaseAuth: state.firebase.auth,
}))
export default class ChooseType extends BaseScreen {
  static propTypes = {
    onboarding: PropTypes.object,
    firebase: PropTypes.object,
    firebaseAuth: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: has(navigation.state, 'params.gesturesEnabled'),
  })

  constructor(props) {
    super(props)

    this.gesturesEnabled = has(props.navigation.state, 'params.gesturesEnabled')
  }

  render() {
    const { profile } = this.props.screenProps
    const { onboarding, firebaseAuth } = this.props
    const { formatMessage } = this.context.intl

    let userName
    if (onboarding && onboarding.userName) {
      userName = onboarding.userName
    } else {
      userName = firebaseAuth.displayName || profile.name
    }

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249, 250, 252)"
      >
        {this.gesturesEnabled ? <GoBackButton onPress={this._goBack} /> : null}

        <OnboardingTopBlock
          title={formatMessage(messages.title, { name: userName })}
          description={formatMessage(messages.description)}
          style={styles.topBlock}
        />

        <View style={styles.linkContainer}>
          <Link
            title={formatMessage(messages.pregnantTypeButtonTitle)}
            style={styles.link}
            onPress={this._motherTypeSelected}
          />

          <Link
            title={formatMessage(messages.partnerTypeButtonTitle)}
            style={styles.link}
            onPress={this._partnerTypeSelected}
          />

          <Link
            title={formatMessage(messages.familyTypeButtonTitle)}
            style={styles.link}
            onPress={this._familyTypeSelected}
          />

          <Link
            title={formatMessage(messages.friendTypeButtonTitle)}
            style={styles.link}
            onPress={this._friendTypeSelected}
          />
        </View>
      </ViewWrapper>
    )
  }

  _motherTypeSelected = () => this._linkPressed(UserTypes.MOTHER)

  _partnerTypeSelected = () => this._linkPressed(UserTypes.PARTNER)

  _friendTypeSelected = () => this._linkPressed(UserTypes.FRIEND)

  _familyTypeSelected = () => this._linkPressed(UserTypes.FAMILY)

  _linkPressed = userType => {
    const { profile } = this.props.screenProps
    const { onboarding, firebase } = this.props

    if (!onboarding) {
      this._(TempActions.init('onboarding', { userType }))
    } else {
      this._(TempActions.update('onboarding', { userType: { $set: userType } }))
    }

    if (userType === UserTypes.MOTHER) {
      this._goTo('/onboarding-enter-pregnancy-due-date')
    } else {
      this._goTo('/onboarding-enter-invitation-code')
    }

    if (!profile.isEmpty) {
      firebase.updateProfile({ type: userType })
    }
  }
}
