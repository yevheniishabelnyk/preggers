/*
 *
 * EnterName screen
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { View, Keyboard } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import OnboardingTextInput from '@/onboarding/components/OnboardingTextInput'
import OnboardingTopBlock from '@/onboarding/components/OnboardingTopBlock'
import NextButton from '@/onboarding/components/NextButton'
import FormError from '@/forms/components/atoms/FormError'

import * as TempActions from '@/app/redux/temp'
import * as ProfileActions from '@/app/redux/profile/actions'

import { withFirebase } from 'react-redux-firebase'

import messages from './messages'

import styles from './styles'

@withFirebase
@connect(state => ({ onboarding: state.temp.onboarding }))
export default class EnterName extends BaseScreen {
  static propTypes = {
    onboarding: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static navigationOptions = {
    gesturesEnabled: false,
  }

  state = {
    userName: '',
  }

  render() {
    const { formatMessage } = this.context.intl
    const { inputFocused } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={[
          styles.contentContainerStyle,
          inputFocused ? styles.noFlex : null,
        ]}
        keyboard
        scrollable
        behavior="padding"
        deviceIndicators="rgb(249, 250, 252)"
      >
        <OnboardingTopBlock
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
          style={styles.topBlock}
        />

        <View>
          <OnboardingTextInput
            style={styles.nameInput}
            title={formatMessage(messages.nameInputTitle)}
            placeholder={formatMessage(messages.nameFieldPlaceholder)}
            onChangeText={this._setUserName}
            value={this.state.userName}
            returnKeyType="go"
            onSubmitEditing={this._nextButtonPressed}
            onFocus={this._onFocus}
            error={this.state.nameError}
            onBlur={this._onBlur}
          />

          <FormError
            isVisible={this.state.nameError}
            title={formatMessage(messages.nameErrorTitle)}
            style={styles.error}
          />
        </View>

        <NextButton
          style={[
            styles.nextButton,
            inputFocused ? styles.nextButtonMargin : null,
          ]}
          title={formatMessage(messages.nextButtonTitle)}
          onPress={this._nextButtonPressed}
        />
      </ViewWrapper>
    )
  }

  _onFocus = () => {
    this.setState({ inputFocused: true })
  }

  _onBlur = () => {
    setTimeout(() => {
      this.setState({ inputFocused: false })
    }, 100)
  }

  _setUserName = value => {
    this.setState({ userName: value, nameError: false })
  }

  _nextButtonPressed = () => {
    const { firebase, onboarding } = this.props
    const { userName } = this.state

    if (!userName) {
      this.setState({ nameError: true })
    } else {
      if (!onboarding) {
        this._(
          TempActions.init('onboarding', {
            userName,
          })
        )
      }

      Keyboard.dismiss()

      this._goTo('/onboarding-choose-type', { gesturesEnabled: true })

      firebase.updateProfile({ name: userName })

      this._(ProfileActions.updateFirebaseProfileDisplayName(userName))
    }
  }
}
