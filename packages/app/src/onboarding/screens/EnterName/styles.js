import { StyleSheet } from 'react-native'

import { ifIphoneX } from 'react-native-iphone-x-helper'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  contentContainerStyle: {
    flex: 1,
    paddingTop: moderateScale(21),
    paddingBottom: moderateScale(25),
    alignItems: 'center',
  },

  noFlex: {
    flex: 0,
  },

  nextButtonMargin: {
    marginTop: moderateScale(100),
    ...ifIphoneX({
      marginTop: moderateScale(200),
    }),
  },

  topBlock: {
    width: moderateScale(275),
  },

  nameInput: {
    marginTop: moderateScale(50),
    marginBottom: moderateScale(13, 0.3),
  },

  error: {
    marginLeft: moderateScale(18),
    marginTop: moderateScale(32),
  },
})

export default styles
