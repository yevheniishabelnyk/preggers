import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Welcome',
  description:
    'Im here to help you through your pregnancy. Start with telling me a little bit about yourself.',
  nameFieldPlaceholder: 'Enter name',
  nextButtonTitle: 'Next',
  nameInputTitle: "What's your name?",
  nameErrorTitle: 'You need to enter your name',
})
