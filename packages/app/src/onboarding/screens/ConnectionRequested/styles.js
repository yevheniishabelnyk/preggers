import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
    paddingTop: moderateScale(21),
    paddingBottom: moderateScale(25),
    paddingHorizontal: moderateScale(44, 2),
  },

  linkContainer: {
    marginTop: moderateScale(120),
    alignItems: 'center',
  },

  topBlock: {
    width: moderateScale(290),
  },

  skipButton: {
    position: 'absolute',
    top: moderateScale(22),
    right: moderateScale(20),
    zIndex: 1,
  },

  skipText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15),
    lineHeight: moderateScale(33),
    color: 'white',
  },
})

export default styles
