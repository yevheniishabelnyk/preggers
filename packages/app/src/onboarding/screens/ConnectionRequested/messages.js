import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'What a great team!',
  description:
    'You will get a notification when your invitation has been accepted.',
  nextButtonTitle: 'Done',
})
