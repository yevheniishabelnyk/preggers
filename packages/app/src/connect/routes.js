/*
 *
 * Connect partner screens
 *
 */

import AcceptInvitation from './screens/AcceptInvitation'
import RequestSent from './screens/RequestSent'
import SendRequest from './screens/SendRequest'
import ShareSettings from './screens/ShareSettings'
import VerifyConnection from './screens/VerifyConnection'
import SuccessfulConnect from './screens/SuccessfulConnect'

export default {
  '/connect-accept-invitation': {
    screen: AcceptInvitation,
  },

  '/connect-request-sent': {
    screen: RequestSent,
  },

  '/connect-send-request': {
    screen: SendRequest,
  },

  '/connect-share-settings': {
    screen: ShareSettings,
  },

  '/connect-verify-connection': {
    screen: VerifyConnection,
  },

  '/connect-successful': {
    screen: SuccessfulConnect,
  },
}
