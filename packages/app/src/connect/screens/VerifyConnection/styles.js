import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    paddingHorizontal: moderateScale(22.5, 4),
    alignItems: 'center',
  },

  closeButton: {
    position: 'absolute',
    top: moderateScale(10, resizeFactor),
    right: moderateScale(10, resizeFactor),
    zIndex: 3,
    marginLeft: 0,
  },

  image: {
    width: moderateScale(200, resizeFactor),
    height: moderateScale(225, resizeFactor),
    marginTop: moderateScale(20, resizeFactor),
    marginBottom: moderateScale(30, resizeFactor),
  },

  errorImage: {
    width: moderateScale(150, resizeFactor),
    height: moderateScale(181, resizeFactor),
    backgroundColor: 'transparent',
    marginTop: moderateScale(65, resizeFactor),
    marginBottom: moderateScale(53, resizeFactor),
  },

  text: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18, resizeFactor),
    lineHeight: moderateScale(27, resizeFactor),
    color: 'rgb(43,56,87)',
    maxWidth: moderateScale(300, resizeFactor),
    textAlign: 'center',
    alignSelf: 'center',
    paddingBottom: moderateScale(18, resizeFactor),
  },

  wideText: {
    maxWidth: width,
  },

  formField: {
    marginTop: 'auto',
  },

  connectButton: {
    marginTop: moderateScale(10, resizeFactor),
  },

  acceptButton: {
    marginTop: 'auto',
    marginBottom: moderateScale(10, resizeFactor),
  },

  declineButton: {
    marginBottom: moderateScale(10, resizeFactor),
  },
  error: {
    marginLeft: moderateScale(18, resizeFactor),
    marginTop: moderateScale(10, resizeFactor),
    marginBottom: 0,
  },
})

export default styles
