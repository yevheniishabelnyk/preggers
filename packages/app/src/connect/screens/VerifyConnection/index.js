/**
 *
 * VerifyConnection
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Image } from 'react-native'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import FormInput from '@/settings/components/molecules/FormInput'
import FormError from '@/forms/components/atoms/FormError'
import CloseButton from '@/settings/components/atoms/CloseButton'
import ViewportText from '@/shared/components/ViewportText'

import ActionButton from '@/settings/components/atoms/ActionButton'

import { ConnectApi } from '@/app/api/Preggers'

import { withFirebase } from 'react-redux-firebase'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@withFirebase
export default class VerifyConnection extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    code: '',
    isRequesting: false,
    error: null,
  }

  render() {
    const { error, isRequesting } = this.state
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        deviceIndicators="rgb(249,250,252)"
        keyboard
      >
        <CloseButton white style={styles.closeButton} onPress={this._goBack} />

        {error ? (
          <Image
            source={require('assets/img/connection-faild.png')}
            style={styles.errorImage}
          />
        ) : (
          <Image
            source={require('assets/img/connect-partner.png')}
            style={styles.image}
          />
        )}

        {error ? (
          <ViewportText text={error} textStyle={styles.text} alignCenter />
        ) : (
          <FormattedMessage
            {...messages.description}
            style={[styles.text, styles.wideText]}
          />
        )}

        <FormInput
          title={formatMessage(messages.invitationCodeInputTitle)}
          value={this.state.code}
          keyboardType="numeric"
          enablesReturnKeyAutomatically
          style={styles.formField}
          onChange={this._onChange}
          error={this.state.errorEmptyField}
          returnKeyType="done"
          onSubmitEditing={this._verifyCode}
          isRequesting={isRequesting}
        />

        <FormError
          isVisible={this.state.errorEmptyField}
          title={formatMessage(messages.errorEmptyFieldTitle)}
          style={styles.error}
        />

        <ActionButton
          title={formatMessage(messages.connectButtonTitle)}
          onPress={this._verifyCode}
          style={styles.connectButton}
          loader={isRequesting}
        />
      </ViewWrapper>
    )
  }

  _verifyCode = async () => {
    const { code, isRequesting } = this.state

    if (!this.state.code) {
      this.setState({ errorEmptyField: true })
    } else if (!isRequesting && code) {
      this.setState({ isRequesting: true })

      try {
        const response = await ConnectApi.acceptInvitation(null, code)

        const { connectionId, pregnancyId } = response.body

        await this.props.firebase.updateProfile({
          connection: connectionId,
          pregnancy: pregnancyId,
        })

        this.setState({ isRequesting: false })

        this._goTo('/connect-successful')
      } catch (err) {
        console.info('err: ', JSON.stringify(err, null, 4))

        const { formatMessage } = this.context.intl

        let errorMessage

        switch (err.code) {
          case 'connect/invitation-was-resolved':
            errorMessage = formatMessage(messages.errorInvitationHasExpired)
            break

          case 'connect/from-user-already-connected-with-another-user': {
            const payload = err.payload

            const name =
              payload && payload.fromUserName
                ? payload.fromUserName
                : formatMessage(messages.mother)

            errorMessage = formatMessage(messages.errorFromUserHasConnection, {
              name,
            })
            break
          }

          case 'connect/to-user-already-connected-with-another-user':
            errorMessage = formatMessage(messages.errorToUserHasConnection)
            break

          case 'connect/to-user-type-is-not-one-of-partner-types':
            errorMessage = formatMessage(messages.errorToUserWrongType)
            break

          case 'connect/from-user-type-is-not-one-of-mother-types':
            errorMessage = formatMessage(messages.errorFromUserInvalidType)
            break

          case 'connect/permission-denied':
            errorMessage = formatMessage(
              messages.errorRequestUserIsNotInvitationToUser
            )
            break

          case 'connect/invitation-is-missing':
            errorMessage = formatMessage(messages.errorBadRequest)
            break

          case 'network/offline':
            errorMessage = formatMessage(messages.errorNetwork)
            break

          case 'connect/wrong-code':
            errorMessage = formatMessage(messages.errorWrongCode)
            break

          default:
            errorMessage = formatMessage(messages.errorBadRequest)
        }

        this.setState({ error: errorMessage, isRequesting: false })
      }

      this.setState({ isRequesting: false })
    }
  }

  _onChange = value => {
    if (this.state.error) {
      this.setState({ error: null, code: value, errorEmptyField: false })
    } else {
      this.setState({ code: value, errorEmptyField: false })
    }
  }
}
