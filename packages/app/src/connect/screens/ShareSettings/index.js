/**
 *
 * ShareSettings
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import Section from '@/shared/components/Section'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import FormSwitch from '@/settings/components/molecules/FormSwitch'
import FormError from '@/forms/components/atoms/FormError'

import { ServerTimestamp } from '@/app/database/models/shared'

import messages from './messages'

import styles from './styles'

import { withFirebase } from 'react-redux-firebase'

@withFirebase
export default class ShareSettings extends BaseScreen {
  static propTypes = {
    navigation: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { navigation } = this.props
    const { formatMessage } = this.context.intl

    const shareTimeline = navigation.getParam('shareTimeline')
    const shareChecklist = navigation.getParam('shareChecklist')
    const shareContractionTimer = navigation.getParam('shareContractionTimer')
    const shareBabyNames = navigation.getParam('shareBabyNames')

    const error = !(
      shareTimeline ||
      shareChecklist ||
      shareContractionTimer ||
      shareBabyNames
    )

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        scrollable
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              onDoneButtonPress={this._doneButtonPressed}
              style={styles.header}
              cancel
              done
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <Section
          title={formatMessage(messages.shareSectionTitle)}
          style={styles.section}
        >
          <FormSwitch
            title={formatMessage(messages.babyNamesSwitchTitle)}
            onChange={this._setShareBabyNames}
            value={shareBabyNames}
            style={styles.formField}
          />

          {/* <FormSwitch
            title={formatMessage(messages.contractionTimerSwitchTitle)}
            onChange={this._setShareValue.bind(null, 'shareContractionTimer')}
            value={shareContractionTimer}
            style={styles.formField}
          />

          <FormSwitch
            title={formatMessage(messages.timelineSwitchTitle)}
            onChange={this._setShareValue.bind(null, 'shareTimeline')}
            value={shareTimeline}
            style={styles.formField}
          />

          <FormSwitch
            title={formatMessage(messages.checklistSwitchTitle)}
            onChange={this._setShareValue.bind(null, 'shareChecklist')}
            value={shareChecklist}
            style={styles.formField}
          /> */}

          <FormError
            isVisible={error}
            title={formatMessage(messages.shareRequiredError)}
            style={styles.errorText}
          />
        </Section>
      </ViewWrapper>
    )
  }

  _setShareBabyNames = value => this._setShareValue('shareBabyNames', value)

  _setShareValue = (key, value) => {
    const { navigation } = this.props

    navigation.setParams({ [key]: value })
  }

  _doneButtonPressed = () => {
    const { navigation } = this.props

    const shareTimeline = navigation.getParam('shareTimeline')
    const shareChecklist = navigation.getParam('shareChecklist')
    const shareContractionTimer = navigation.getParam('shareContractionTimer')
    const shareBabyNames = navigation.getParam('shareBabyNames')

    const connectionId = navigation.getParam('connectionId')
    const invitationId = navigation.getParam('invitationId')

    if (
      shareTimeline ||
      shareChecklist ||
      shareContractionTimer ||
      shareBabyNames
    ) {
      const { firebase } = this.props

      const shareUpdate = {
        shareTimeline,
        shareChecklist,
        shareContractionTimer,
        shareBabyNames,
        updatedAt: ServerTimestamp,
      }

      try {
        if (connectionId) {
          firebase.update(`connections/${connectionId}`, shareUpdate)
        } else if (invitationId) {
          firebase.update(`invitations/${invitationId}`, shareUpdate)
        }
      } catch (err) {
        console.info('err: ', err)
      }

      this._goBack()
    }
  }
}
