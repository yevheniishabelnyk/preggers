import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Share settings',
  shareSectionTitle: 'Share',
  babyNamesSwitchTitle: 'Baby names',
  contractionTimerSwitchTitle: 'Contraction timer',
  timelineSwitchTitle: 'Timeline',
  checklistSwitchTitle: 'Checklist',
  shareRequiredError: 'You need to choose what to share',
})
