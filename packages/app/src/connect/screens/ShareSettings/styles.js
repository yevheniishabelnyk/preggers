import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: 0.08 * width,
  },

  header: {
    paddingHorizontal: 0.06 * width,
    backgroundColor: 'rgb(249,250,252)',
  },

  formField: {
    marginBottom: (11 / 667) * height,
  },

  section: {
    marginTop: (5 / 667) * height,
  },
})

export default styles
