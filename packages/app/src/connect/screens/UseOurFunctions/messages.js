import { defineMessages } from 'react-intl'

export default defineMessages({
  dialogMessage: 'Want to use all our functions? Create an account, it’s free!',
  dialogDescription:
    'In order to use all the functions in our app you need to create an account. Just click the link below ',
  yesButtonTitle: 'Of course I want to!',
  noButtonTitle: 'No thanks',
})
