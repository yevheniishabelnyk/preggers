/**
 *
 * UseOurFunctions
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, TouchableOpacity } from 'react-native'

import ActionButton from '@/settings/components/atoms/ActionButton'

import { isFunction } from 'lodash'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class UseOurFunctions extends React.Component {
  static propTypes = {
    onCancel: PropTypes.func,
    onOk: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <Image
          source={require('assets/img/create-account-popup.png')}
          style={styles.image}
        />

        <FormattedMessage {...messages.dialogMessage} style={styles.title} />

        <FormattedMessage
          {...messages.dialogDescription}
          style={styles.description}
        />

        <View style={styles.buttonWrapper}>
          <ActionButton
            title={formatMessage(messages.yesButtonTitle)}
            onPress={this._yesButtonPressed}
            style={styles.yesButton}
          />
        </View>

        <TouchableOpacity
          style={styles.noButton}
          onPress={this._noButtonPressed}
          activeOpacity={0.95}
        >
          <FormattedMessage
            {...messages.noButtonTitle}
            style={styles.noButtonTitle}
          />
        </TouchableOpacity>
      </View>
    )
  }

  _yesButtonPressed = () => {
    const { onOk } = this.props

    if (isFunction(onOk)) {
      onOk()
    }
  }

  _noButtonPressed = () => {
    const { onCancel } = this.props

    if (isFunction(onCancel)) {
      onCancel()
    }
  }
}
