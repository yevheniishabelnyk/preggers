import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: (23 / 375) * width,
    alignItems: 'center',
  },

  image: {
    width: (200 / 375) * width,
    height: (217 / 375) * width,
    marginTop: (72 / 667) * height,
    marginBottom: (45 / 667) * height,
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: (26 / 667) * height,
    color: 'rgb(41,53,69)',
    maxWidth: (243 / 375) * width,
    textAlign: 'center',
    marginBottom: (12 / 667) * height,
  },

  text: {
    fontFamily: 'Now-Medium',
    fontSize: (18 / 667) * height,
    lineHeight: (27 / 667) * height,
    color: 'rgb(43,56,87)',
    maxWidth: (243 / 375) * width,
    textAlign: 'center',
  },

  buttonWrapper: {
    marginTop: 'auto',
    alignSelf: 'stretch',
    marginBottom: (39 / 667) * height,
  },
})

export default styles
