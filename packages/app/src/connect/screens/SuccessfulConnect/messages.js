import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'What a great team!',
  description: 'You are now synced with {motherName}!',
  doneButtonTitle: 'Done',
})
