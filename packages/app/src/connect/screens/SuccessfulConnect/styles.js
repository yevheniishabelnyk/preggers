import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: moderateScale(23),
    alignItems: 'center',
  },

  image: {
    width: moderateScale(200),
    height: moderateScale(217),
    marginTop: moderateScale(72),
    marginBottom: moderateScale(45),
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(26),
    color: 'rgb(41,53,69)',
    maxWidth: moderateScale(243, 1),
    textAlign: 'center',
    marginBottom: moderateScale(12),
  },

  text: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(27),
    color: 'rgb(43,56,87)',
    maxWidth: moderateScale(243, 1),
    textAlign: 'center',
  },

  buttonWrapper: {
    marginTop: 'auto',
    width: moderateScale(315),
    marginBottom: moderateScale(39),
  },
})

export default styles
