/**
 *
 * Successfull connect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Image, View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import ActionButton from '@/settings/components/atoms/ActionButton'

import { NavigationActions } from 'react-navigation'

import { findIndex } from 'lodash'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { getConnection } from '@/app/redux/selectors'

import styles from './styles'

@connect((state, props) => {
  const { profile } = props.screenProps

  const userConnection = getConnection(state, profile.connection)
  const motherName = userConnection ? userConnection.fromName : ''

  return {
    motherName,
    router: state.router,
  }
})
export default class SuccessfulConnect extends BaseScreen {
  static propTypes = {
    router: PropTypes.object.isRequired,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { motherName } = this.props
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
      >
        <Image
          source={require('assets/img/connection-success.png')}
          style={styles.image}
        />

        <FormattedMessage {...messages.title} style={styles.title} />

        <FormattedMessage
          {...messages.description}
          style={styles.text}
          values={{
            motherName,
          }}
        />

        <View style={styles.buttonWrapper}>
          <ActionButton
            title={formatMessage(messages.doneButtonTitle)}
            onPress={this._doneButtonPressed}
            style={styles.doneButton}
          />
        </View>
      </ViewWrapper>
    )
  }

  _doneButtonPressed = () => {
    const { router } = this.props

    const nextScreenIndex = findIndex(router.routes, {
      routeName: '/~home',
    })

    this._(
      NavigationActions.reset({
        index: nextScreenIndex !== -1 ? nextScreenIndex : 0,
        actions:
          nextScreenIndex !== -1
            ? router.routes
                .slice(0, nextScreenIndex + 1)
                .map(route => NavigationActions.navigate(route))
            : [NavigationActions.navigate({ routeName: '/~home' })],
      })
    )
  }
}
