import { defineMessages } from 'react-intl'

export default defineMessages({
  dialogMessage: 'Are you sure you want to disconnect?',
  noButtonTitle: 'No of course not',
  yesButtonTitle: 'Yes, I am',
})
