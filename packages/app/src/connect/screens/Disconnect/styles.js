import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: moderateScale(28, resizeFactor),
    paddingTop: moderateScale(28, resizeFactor),
    alignItems: 'center',
  },

  image: {
    width: moderateScale(210, resizeFactor),
    height: moderateScale(235, resizeFactor),
  },

  title: {
    marginTop: moderateScale(11),
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(25, resizeFactor),
    lineHeight: moderateScale(31, resizeFactor),
    color: 'rgb(43,56,87)',
    textAlign: 'center',
  },

  buttonWrapper: {
    marginTop: 'auto',
    alignSelf: 'stretch',
  },

  cancelButton: {
    shadowOpacity: 0.42,
    shadowRadius: 6,
    shadowColor: 'rgb(101,150,255)',
    shadowOffset: { height: 5, width: 2 },
  },

  okButton: {
    paddingVertical: moderateScale(20, resizeFactor),
    alignSelf: 'stretch',
  },

  okButtonTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12, resizeFactor),
    textAlign: 'center',
    color: 'rgb(154,153,159)',
    backgroundColor: 'transparent',
  },
})

export default styles
