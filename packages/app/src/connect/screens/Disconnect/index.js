/**
 *
 * Disconnect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, TouchableOpacity } from 'react-native'

import ActionButton from '@/settings/components/atoms/ActionButton'

import { isFunction } from 'lodash'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class Disconnect extends React.Component {
  static propTypes = {
    onCancel: PropTypes.func,
    onOk: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <Image
          source={require('assets/img/disconnect.png')}
          style={styles.image}
        />

        <FormattedMessage {...messages.dialogMessage} style={styles.title} />

        <View style={styles.buttonWrapper}>
          <ActionButton
            title={formatMessage(messages.noButtonTitle)}
            onPress={this._cancelButtonPressed}
            style={styles.cancelButton}
          />
        </View>

        <TouchableOpacity
          style={styles.okButton}
          onPress={this._submitButtonPressed}
          activeOpacity={0.95}
        >
          <FormattedMessage
            {...messages.yesButtonTitle}
            style={styles.okButtonTitle}
          />
        </TouchableOpacity>
      </View>
    )
  }

  _cancelButtonPressed = () => {
    const { onCancel } = this.props

    if (isFunction(onCancel)) {
      onCancel()
    }
  }

  _submitButtonPressed = () => {
    const { onOk } = this.props

    if (isFunction(onOk)) {
      onOk()
    }
  }
}
