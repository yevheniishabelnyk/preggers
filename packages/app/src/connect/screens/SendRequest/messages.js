import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Connect',
  whoSectionTitle: 'Who',
  connectedWithInputTitle: 'Connect with',
  emailInputTitle: 'E-mail',
  smsInputTitle: 'Or SMS',
  errorEmailTitle: 'Not a valid e-mail',
  errorToUserIsMotherType:
    'You have registered as a mother. You can not invite another mother to follow your pregnancy. You can only invite a user which has register as partner, father, friend or family.',
  errorConnectingToSelf: 'You can not connect to yourself.',
  shareSectionTitle: 'Share',
  babyNamesSwitchTitle: 'Baby names',
  contractionTimerSwitchTitle: 'Contraction timer',
  timelineSwitchTitle: 'Timeline',
  checklistSwitchTitle: 'Checklist',
  shareRequiredError: 'You need to choose what to share',
  sendingButtonTitle: 'Sending',
  sendButtonTitle: 'Send',
  fatherUserType: 'Father',
  partnerUserType: 'Partner',
  friendUserType: 'Friend',
  familyUserType: 'Family',
})
