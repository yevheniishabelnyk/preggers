import { StyleSheet } from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 2.5)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    paddingHorizontal: gutter,
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  formField: {
    marginBottom: moderateScale(11, resizeFactor),
  },

  secondSection: {
    marginTop: moderateScale(5, resizeFactor),
    marginBottom: moderateScale(15, resizeFactor),
  },

  connection: {
    marginTop: moderateScale(15, resizeFactor),
    marginBottom: moderateScale(39, resizeFactor),
  },

  disabledButton: {
    backgroundColor: 'rgb(201,201,201)',
  },

  sendButton: {
    marginTop: 'auto',
    ...ifIphoneX(
      {
        marginBottom: moderateScale(50, resizeFactor),
      },
      {
        marginBottom: moderateScale(30, resizeFactor),
      }
    ),
  },
})

export default styles
