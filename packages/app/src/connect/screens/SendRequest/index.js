/**
 *
 * SendRequest
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import * as ConnectionTypes from '@/app/constants/ConnectionTypes'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import Section from '@/shared/components/Section'
import FormInput from '@/settings/components/molecules/FormInput'
import FormSelect from '@/settings/components/molecules/FormSelect'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import ActionButton from '@/settings/components/atoms/ActionButton'
import FormSwitch from '@/settings/components/molecules/FormSwitch'

import { isValidEmail } from '@/shared/utils/validators'

import FormError from '@/forms/components/atoms/FormError'

import { ConnectApi } from '@/app/api/Preggers'

import messages from './messages'

import styles from './styles'

export default class SendRequest extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props, context) {
    super(props)

    const { formatMessage } = context.intl

    this.userStatuses = [
      {
        label: formatMessage(messages.fatherUserType),
        value: ConnectionTypes.FATHER,
      },
      {
        label: formatMessage(messages.partnerUserType),
        value: ConnectionTypes.PARTNER,
      },
      {
        label: formatMessage(messages.familyUserType),
        value: ConnectionTypes.FAMILY,
      },
      {
        label: formatMessage(messages.friendUserType),
        value: ConnectionTypes.FRIEND,
      },
    ]

    this.state = {
      connectionType: ConnectionTypes.FATHER,
      email: '',
      shareBabyNames: true,
      shareTimeline: true,
      shareChecklist: true,
      shareContractionTimer: true,
      emailError: null,
      emailErrorText: null,
      shareError: null,
      isRequesting: false,
    }
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isRequesting } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        deviceIndicators="rgb(249,250,252)"
        scrollable
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
              cancel
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <Section title={formatMessage(messages.whoSectionTitle)}>
          <FormSelect
            title={formatMessage(messages.connectedWithInputTitle)}
            value={this.state.connectionType}
            items={this.userStatuses}
            onSelect={this._setConnectionType}
            style={styles.formField}
          />

          <FormInput
            title={formatMessage(messages.emailInputTitle)}
            value={this.state.email.toLowerCase()}
            onChange={this._setEmail}
            keyboardType="email-address"
            returnKeyType="done"
            autoCapitalize="none"
            enablesReturnKeyAutomatically
            style={styles.formField}
            error={this.state.emailError}
            autoCorrect={false}
            isRequesting={isRequesting}
          />

          <FormError
            isVisible={this.state.emailError}
            title={this.state.emailErrorText}
          />
        </Section>

        <Section
          title={formatMessage(messages.shareSectionTitle)}
          style={styles.secondSection}
        >
          <FormSwitch
            title={formatMessage(messages.babyNamesSwitchTitle)}
            onChange={this._setShareBabyNames}
            value={this.state.shareBabyNames}
            error={this.state.shareError}
            style={styles.formField}
          />

          {/* <FormSwitch
            title={formatMessage(messages.contractionTimerSwitchTitle)}
            onChange={this._setValue.bind(null, 'shareContractionTimer')}
            value={this.state.shareContractionTimer}
            error={this.state.shareError}
            style={styles.formField}
          />

          <FormSwitch
            title={formatMessage(messages.timelineSwitchTitle)}
            onChange={this._setValue.bind(null, 'shareTimeline')}
            value={this.state.shareTimeline}
            error={this.state.shareError}
            style={styles.formField}
          />

          <FormSwitch
            title={formatMessage(messages.checklistSwitchTitle)}
            onChange={this._setValue.bind(null, 'shareChecklist')}
            value={this.state.shareChecklist}
            error={this.state.shareError}
            style={styles.formField}
          /> */}

          <FormError
            isVisible={this.state.shareError}
            title={formatMessage(messages.shareRequiredError)}
          />
        </Section>

        <ActionButton
          title={formatMessage(messages.sendButtonTitle)}
          onPress={this._sendRequest}
          style={[
            styles.sendButton,
            this.state.isButtonDisabled ? styles.disabledButton : null,
          ]}
          loader={isRequesting}
        />
      </ViewWrapper>
    )
  }

  _setValue = (key, value) =>
    this.setState({
      [key]: value,
      emailError: null,
      shareError: null,
      isButtonDisabled: false,
    })

  _setConnectionType = value => this._setValue('connectionType', value)

  _setEmail = value => this._setValue('email', value.toLowerCase())

  _setShareBabyNames = value => this._setValue('shareBabyNames', value)

  _sendRequest = async () => {
    const { formatMessage } = this.context.intl

    const {
      email,
      shareContractionTimer,
      shareTimeline,
      shareChecklist,
      shareBabyNames,
      relationshipType,
    } = this.state

    const { isRequesting } = this.state

    if (!isValidEmail(email)) {
      this.setState({
        emailError: true,
        emailErrorText: formatMessage(messages.errorEmailTitle),
        isButtonDisabled: true,
      })

      return
    }

    if (
      !(
        shareContractionTimer ||
        shareTimeline ||
        shareChecklist ||
        shareBabyNames
      )
    ) {
      this.setState({
        shareError: true,
        isButtonDisabled: true,
      })

      return
    }

    if (!isRequesting) {
      this.setState({ isRequesting: true })

      try {
        await ConnectApi.sendInvitation({
          email,
          relationshipType,
          shareContractionTimer,
          shareTimeline,
          shareChecklist,
          shareBabyNames,
        })

        this.setState({ isRequesting: false })

        this._goTo('/connect-request-sent')
      } catch (err) {
        this.setState({ isRequesting: false })

        switch (err.code) {
          case 'connect/to-user-is-one-of-mother-types':
            this.setState({
              emailError: true,
              emailErrorText: formatMessage(messages.errorToUserIsMotherType),
            })
            break

          case 'connect/cant-connect-to-yourself':
            this.setState({
              emailError: true,
              emailErrorText: formatMessage(messages.errorConnectingToSelf),
            })
            break

          default:
            console.log(err)
            break
        }
      }
    }
  }
}
