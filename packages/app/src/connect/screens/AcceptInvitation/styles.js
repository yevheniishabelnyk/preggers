import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 18,
    paddingBottom: 10,
    backgroundColor: 'rgb(249,250,252)',
  },
})

export default styles
