/**
 *
 * AcceptInvitation
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ConnectDialog from '@/connect/components/organisms/ConnectDialog'

import { firebaseConnect } from 'react-redux-firebase'

import { getInvitation } from '@/app/redux/selectors'

import styles from './styles'

@firebaseConnect(props => {
  const invitationId = props.navigation.getParam('invitationId')

  if (invitationId) {
    return [`invitations/${invitationId}`]
  }
})
@connect((state, props) => {
  const invitationId = props.navigation.getParam('invitationId')

  if (invitationId) {
    const invitation = getInvitation(state, invitationId)

    return {
      invitation,
      isFetching: invitation === undefined,
    }
  }

  return {
    isFetching: true,
  }
})
export default class AcceptInvitation extends BaseScreen {
  static propTypes = {
    invitation: PropTypes.object,
    isFetching: PropTypes.bool,
    dispatch: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { invitation } = this.props

    if (!invitation) {
      return null
    }

    return (
      <View style={styles.container}>
        <ConnectDialog
          invitation={invitation}
          dispatch={this._}
          onAccept={this._invitationAccepted}
          onClose={this._goBack}
          onDecline={this._goBack}
          isInPopup={false}
        />
      </View>
    )
  }

  _invitationAccepted = () => this._goTo('/connect-successful')
}
