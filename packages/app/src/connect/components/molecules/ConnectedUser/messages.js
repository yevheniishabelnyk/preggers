import { defineMessages } from 'react-intl'

export default defineMessages({
  pendingInvitationPrefix: 'Pending',
  activeConnectionPrefix: 'Connected with',
})
