import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const containerHeight = moderateScale(60, resizeFactor)
const pictureWidth = moderateScale(35, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    height: containerHeight,
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: moderateScale(17, resizeFactor),
  },

  photo: {
    width: pictureWidth,
    height: pictureWidth,
    borderRadius: pictureWidth / 2,
    marginRight: moderateScale(11, resizeFactor),
  },

  prefix: {
    fontSize: moderateScale(10, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },

  userIdentifier: {
    fontFamily: 'Now-Medium',
    color: 'rgb(55,55,64)',
    fontSize: moderateScale(14, resizeFactor),
    paddingTop: moderateScale(3, resizeFactor),
  },

  removeButton: {
    height: containerHeight,
    width: containerHeight,
    marginLeft: 'auto',
    marginRight: 0,
  },
})

export default styles
