/**
 *
 * ConnectedUser
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'
import SmallCloseButton from '@/settings/components/atoms/SmallCloseButton'

import defaultUser from 'assets/icons/user.png'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class ConnectedUser extends React.Component {
  static propTypes = {
    onRemove: PropTypes.func,
    status: PropTypes.string,
    name: PropTypes.string,
    email: PropTypes.string,
    photo: PropTypes.string,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { onRemove, style, status, name, email, photo } = this.props

    const userIdentifier = name || email

    let prefixMessage, userIdentifierMaxLength

    userIdentifierMaxLength = 20

    switch (status) {
      case 'active':
        prefixMessage = messages.activeConnectionPrefix
        break

      case 'pending':
        prefixMessage = messages.pendingInvitationPrefix
        break

      default:
        prefixMessage = messages.pendingInvitationPrefix
    }

    return (
      <View style={[styles.container, style]}>
        <Image
          style={styles.photo}
          source={photo ? { uri: photo } : defaultUser}
        />

        <View>
          <FormattedMessage {...prefixMessage} style={styles.prefix} />

          <Text style={styles.userIdentifier}>
            {this._addDotsForLongString(
              userIdentifier,
              userIdentifierMaxLength
            )}
          </Text>
        </View>

        {onRemove ? (
          <SmallCloseButton onPress={onRemove} style={styles.removeButton} />
        ) : null}
      </View>
    )
  }

  _addDotsForLongString = (string, limit) => {
    const dots = '...'

    if (string) {
      const stringLength = string.length

      if (stringLength > limit) {
        return string.substring(0, limit) + dots
      }

      return string
    }
  }
}
