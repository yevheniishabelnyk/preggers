/**
 *
 * DisconnectedFrom
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'
import ActionButton from '@/settings/components/atoms/ActionButton'

import { isFunction, get } from 'lodash'

import * as NotificationActions from '@/app/database/actions/notifications'

import messages from './messages'

import styles from './styles'

export default class DisconnectedFrom extends React.Component {
  static propTypes = {
    onOk: PropTypes.func,
    dispatch: PropTypes.func,
    notification: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { notification } = this.props
    const { formatMessage } = this.context.intl

    const name = get(notification.payload, 'cancellerName', '')

    return (
      <View style={styles.container}>
        <Image
          source={require('assets/img/disconnect.png')}
          style={styles.image}
        />

        <Text style={styles.title}>
          {formatMessage(messages.dialogMessage, { name })}
        </Text>

        <View style={styles.buttonWrapper}>
          <ActionButton
            title={formatMessage(messages.okButtonTitle)}
            onPress={this._okButtonPressed}
            style={styles.button}
          />
        </View>
      </View>
    )
  }

  componentWillMount() {
    const { notification } = this.props

    if (notification) {
      this._markAsDone(notification)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.notification && nextProps.notification) {
      this._markAsDone(nextProps.notification)
    }
  }

  _markAsDone = async notification => {
    const { dispatch } = this.props

    if (notification.id) {
      try {
        await dispatch(NotificationActions.markAsDone(notification.id))
      } catch (err) {
        console.log('err', err)
      }
    }
  }

  _okButtonPressed = () => {
    const { onOk } = this.props

    if (isFunction(onOk)) {
      onOk()
    }
  }
}
