import { defineMessages } from 'react-intl'

export default defineMessages({
  dialogMessage: '{name} has disconnected from you',
  okButtonTitle: 'Ok',
})
