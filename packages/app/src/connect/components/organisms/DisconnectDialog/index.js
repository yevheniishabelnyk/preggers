/**
 *
 * DisconnectDialog
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import Disconnect from '@/connect/screens/Disconnect'
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog'

import { isFunction } from 'lodash'

import styles from './styles'

export default class DisconnectDialog extends React.Component {
  static propTypes = {
    onOk: PropTypes.func,
  }

  render() {
    return (
      <PopupDialog
        ref={ref => (this._dialog = ref)}
        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
        dialogStyle={styles.dialog}
        overlayOpacity={0.7}
      >
        <Disconnect onOk={this._onOk} onCancel={this.hide} />
      </PopupDialog>
    )
  }

  _onOk = () => {
    const { onOk } = this.props

    this.hide()

    if (isFunction(onOk)) {
      onOk()
    }
  }

  show = () => this._dialog.show()

  hide = () => this._dialog.dismiss()
}
