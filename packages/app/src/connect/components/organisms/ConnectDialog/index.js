/**
 *
 * ConnectDialog
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, TouchableOpacity } from 'react-native'
import ActionButton from '@/settings/components/atoms/ActionButton'
import CloseButton from '@/settings/components/atoms/CloseButton'
import ViewportText from '@/shared/components/ViewportText'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { isFunction } from 'lodash'

import * as InvitationsActions from '@/app/database/actions/invitations'

import styles from './styles'

export default class ConnectDialog extends React.Component {
  static propTypes = {
    invitation: PropTypes.object,
    dispatch: PropTypes.func,
    onAccept: PropTypes.func,
    onDecline: PropTypes.func,
    onClose: PropTypes.func,
    onDone: PropTypes.func,
    isInPopup: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    isInPopup: true,
  }

  state = {
    error: null,
  }

  render() {
    const { invitation, onClose, isInPopup } = this.props
    const { error, isAccepted, isRequesting } = this.state

    const { formatMessage } = this.context.intl

    if (!invitation) {
      return null
    }

    let text, image

    if (isAccepted) {
      image = require('assets/img/connection-success.png')

      text = formatMessage(messages.successDescription, {
        motherName: invitation.fromName,
      })
    } else if (error) {
      image = require('assets/img/connection-faild.png')

      text = this._getErrorMessage(error)
    } else {
      image = require('assets/img/connect-partner.png')

      text = formatMessage(messages.description, {
        partnerName: invitation.fromName,
      })
    }

    return (
      <View style={styles.content}>
        {onClose ? (
          <CloseButton white style={styles.closeButton} onPress={onClose} />
        ) : null}

        <View style={styles.imageWrapper}>
          <Image
            source={image}
            style={[styles.image, !isInPopup ? styles.bigImage : null]}
            resizeMode="contain"
          />
        </View>

        {isAccepted ? (
          <FormattedMessage {...messages.successTitle} style={styles.title} />
        ) : null}

        <View style={styles.textWrapper}>
          <ViewportText text={text} textStyle={styles.text} alignCenter />
        </View>

        <View style={styles.formWrapper}>
          {isAccepted ? (
            <ActionButton
              title={formatMessage(messages.doneButtonTitle)}
              onPress={this._doneButtonPressed}
              style={styles.acceptButton}
              loader={isRequesting}
            />
          ) : (
            <ActionButton
              title={formatMessage(messages.acceptButtonTitle)}
              onPress={this._acceptButtonPressed}
              style={styles.acceptButton}
              loader={isRequesting}
            />
          )}

          {!isAccepted ? (
            <TouchableOpacity
              style={styles.declineButtonWrapper}
              onPress={this._declineButtonPressed}
              activeOpacity={0.95}
            >
              <FormattedMessage
                {...messages.declineButtonTitle}
                style={styles.declineButton}
              />
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    )
  }

  componentWillMount() {
    const { invitation } = this.props

    if (invitation && invitation.status === 'unread') {
      this._markAsRead(invitation)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      !this.props.invitation &&
      nextProps.invitation &&
      nextProps.invitation.status === 'unread'
    ) {
      this._markAsRead(nextProps.invitation)
    }
  }

  _declineButtonPressed = async () => {
    const { onDecline, invitation, dispatch } = this.props

    try {
      await dispatch(InvitationsActions.decline(invitation))

      if (isFunction(onDecline)) {
        onDecline(invitation)
      }
    } catch (err) {
      console.info('err: ', err)
    }
  }

  _acceptButtonPressed = async () => {
    const { onAccept, invitation, dispatch, isInPopup } = this.props

    try {
      this.setState({ isRequesting: true, error: null })

      await dispatch(InvitationsActions.accept(invitation))

      this.setState({ isAccepted: isInPopup, isRequesting: false })

      if (isFunction(onAccept)) {
        onAccept(invitation)
      }
    } catch (err) {
      console.info('err: ', JSON.stringify(err, null, 4))

      this.setState({ error: err, isRequesting: false })
    }
  }

  _markAsRead = async invitation => {
    const { dispatch } = this.props

    try {
      await dispatch(InvitationsActions.updateStatus(invitation, 'read'))
    } catch (err) {
      console.log('err', err)
    }
  }

  _doneButtonPressed = () => {
    const { onDone } = this.props

    if (isFunction(onDone)) {
      onDone()
    }
  }

  _getErrorMessage = error => {
    const { formatMessage } = this.context.intl

    let errorMessage

    switch (error.code) {
      case 'connect/invitation-was-resolved':
        errorMessage = formatMessage(messages.errorInvitationHasExpired)
        break

      case 'connect/from-user-already-connected-with-another-user': {
        const payload = error.payload

        const name =
          payload && payload.fromUserName
            ? payload.fromUserName
            : formatMessage(messages.mother)

        errorMessage = formatMessage(messages.errorFromUserHasConnection, {
          name,
        })
        break
      }

      case 'connect/to-user-already-connected-with-another-user':
        errorMessage = formatMessage(messages.errorToUserHasConnection)
        break

      case 'connect/to-user-type-is-not-one-of-partner-types':
        errorMessage = formatMessage(messages.errorToUserWrongType)
        break

      case 'connect/from-user-type-is-not-one-of-mother-types':
        errorMessage = formatMessage(messages.errorFromUserInvalidType)
        break

      case 'connect/permission-denied':
        errorMessage = formatMessage(
          messages.errorRequestUserIsNotInvitationToUser
        )
        break

      case 'connect/invitation-is-missing':
        errorMessage = formatMessage(messages.errorBadRequest)
        break

      case 'network/offline':
        errorMessage = formatMessage(messages.errorNetwork)
        break

      case 'connect/wrong-code':
        errorMessage = formatMessage(messages.errorWrongCode)
        break

      default:
        errorMessage = formatMessage(messages.errorBadRequest)
    }

    return errorMessage
  }
}
