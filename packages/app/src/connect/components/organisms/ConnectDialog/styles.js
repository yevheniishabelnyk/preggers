import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  content: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: moderateScale(30),
    paddingTop: moderateScale(15),
    backgroundColor: 'rgb(249,250,252)',
    paddingBottom: moderateScale(15),
    position: 'relative',
  },

  closeButton: {
    position: 'absolute',
    top: moderateScale(10),
    right: moderateScale(10),
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(26),
    color: 'rgb(41,53,69)',
    maxWidth: moderateScale(243),
    textAlign: 'center',
    marginTop: moderateScale(20),
    marginBottom: moderateScale(12),
  },

  imageWrapper: {
    width: moderateScale(180),
    height: moderateScale(202.5),
  },

  image: {
    flex: 1,
    width: null,
    height: null,
  },

  bigImage: {
    width: moderateScale(200),
    height: moderateScale(225),
    marginTop: moderateScale(20),
  },

  textWrapper: {
    alignSelf: 'stretch',
    height: moderateScale(220),
  },

  text: {
    paddingVertical: moderateScale(7),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(27),
    color: 'rgb(43,56,87)',
    textAlign: 'center',
  },

  acceptButton: {
    marginBottom: moderateScale(20),
  },

  declineButtonWrapper: {
    alignSelf: 'center',
  },

  declineButton: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    color: 'rgb(154,154,154)',
  },

  formWrapper: {
    alignSelf: 'center',
    alignItems: 'center',
    width: moderateScale(315),
    marginTop: 'auto',
  },
})

export default styles
