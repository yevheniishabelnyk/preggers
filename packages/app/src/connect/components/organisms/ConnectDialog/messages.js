import { defineMessages } from 'react-intl'

export default defineMessages({
  description:
    'Yay you have been asked to connect with {partnerName}. Your data will be synced with the mothers account',
  acceptButtonTitle: 'Accept',
  declineButtonTitle: 'Decline',
  successTitle: 'What a great team!',
  successDescription: 'You are now synced with {motherName}!',
  doneButtonTitle: 'Done',
  mother: 'Mother',
  errorNetwork: 'Ooops! Seems like you are offline. Please try again later.',
  errorWrongCode:
    'Ooops! Seems like it’s the wrong code. Check your e-mail to retrieve the right one.',
  errorFromUserInvalidType:
    'The user you are trying to connect to is not registered as a mother. You can only accept an invitation from a user which have registered as a mother. The user which invited you can change their user type under their account settings.',
  errorInvitationHasExpired:
    'The invitation you are trying to accept has expired. Please ask the mother to send a new invitation.',
  errorFromUserHasConnection:
    'It seems that this person is already connected with somebody else.',
  errorBadRequest: 'Woops! Something went wrong.',
  errorToUserWrongType:
    'You have registered as a mother. You can not accept an invitation from another mother to follow her pregnancy. You can only follow another users pregnancy when registered as partner, father, friend or family. You can change your user type under your account settings',
  errorToUserHasConnection:
    'You have already initiated a connection with another user. You can only be connected to one user.',
  errorRequestUserIsNotInvitationToUser:
    'You are trying to use an invitation code which is not registered to your e-mail. The invitation code has to be sent to the e-mail you used to sign up for this account.',
})
