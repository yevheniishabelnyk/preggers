/**
 *
 * Pregnancy Navigator
 *
 */

import React from 'react'

import { TabNavigator, NavigationActions } from 'react-navigation'

import TabBar from './components/organisms/TabBar'
import Statistics from './screens/Statistics'
import Weeks from './screens/Weeks'

const BabyWeeks = props => <Weeks {...props} tabType="baby" />
const MotherWeeks = props => <Weeks {...props} tabType="mother" />
const PartnerWeeks = props => <Weeks {...props} tabType="partner" />

const PregnancyNavigator = TabNavigator(
  {
    '/statistics': {
      screen: Statistics,
    },

    '/baby': {
      screen: BabyWeeks,
    },

    '/mother': {
      screen: MotherWeeks,
    },

    '/partner': {
      screen: PartnerWeeks,
    },
  },
  {
    initialRouteName: '/statistics',
    order: ['/statistics', '/baby', '/mother', '/partner'],
    navigationOptions: {
      gesturesEnabled: false,
      swipeEnabled: false,
    },
    animationEnabled: true,
    tabBarPosition: 'top',
    tabBarComponent: TabBar,
  }
)

const defaultGetStateForAction = PregnancyNavigator.router.getStateForAction

PregnancyNavigator.router.getStateForAction = function(action, state) {
  switch (action.type) {
    case NavigationActions.SET_PARAMS: {
      if (
        action.params &&
        (action.params.activeWeekNumber != null ||
          action.params.pregnancyId ||
          action.params.pregnancyCurrentWeekNumber != null)
      ) {
        const actionParams = {}

        if (action.params.pregnancyId) {
          actionParams.pregnancyId = action.params.pregnancyId
        }

        if (action.params.activeWeekNumber != null) {
          actionParams.activeWeekNumber = action.params.activeWeekNumber
        }

        if (action.params.pregnancyCurrentWeekNumber != null) {
          actionParams.pregnancyCurrentWeekNumber =
            action.params.pregnancyCurrentWeekNumber
        }

        return {
          ...state,
          routes: state.routes.map(route => ({
            ...route,
            params: {
              ...route.params,
              ...actionParams,
            },
          })),
          params: {
            ...state.params,
            ...actionParams,
          },
        }
      }

      return defaultGetStateForAction(action, state)
    }

    default:
      return defaultGetStateForAction(action, state)
  }
}

export default PregnancyNavigator
