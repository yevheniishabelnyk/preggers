/**
 *
 * Statistics
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ShareButton from '@/pregnancy/components/atoms/ShareButton'
import PregnancyProgressSummary from '@/pregnancy/components/organisms/PregnancyProgressSummary'
import PregnancyProgressFruit from '@/pregnancy/components/organisms/PregnancyProgressFruit'
import PregnancyProgressBaby from '@/pregnancy/components/organisms/PregnancyProgressBaby'
import PregnancyProgressWeek from '@/pregnancy/components/organisms/PregnancyProgressWeek'
import PregnancyProgressDay from '@/pregnancy/components/organisms/PregnancyProgressDay'
import PregnancyProgressPercentage from '@/pregnancy/components/organisms/PregnancyProgressPercentage'
import Loader from '@/shared/components/Loader'
import Slider from '@/shared/components/Slider'
import FifteenDaysOverDue from '@/pregnancy/components/atoms/FifteenDaysOverDue'

import { without } from 'lodash'

import { getPregnancy } from '@/app/redux/selectors'

import Pregnancy from '@/app/database/models/Pregnancy'

import messages from './messages'

import styles from './styles'

const getPregnancyProgress = Pregnancy.makeGetProgress()

@connect((state, props) => {
  const pregnancyId = props.navigation.getParam('pregnancyId')

  if (pregnancyId) {
    const pregnancy = getPregnancy(state, pregnancyId)

    let isPregnancyEnding, isPregnancyOverDue, isPregnancyFifteenDaysOverDue

    if (pregnancy) {
      const pregnancyProgress = getPregnancyProgress(
        pregnancy.dueDate,
        pregnancy.lengthType
      )

      isPregnancyEnding = pregnancyProgress.passedDays >= 260
      isPregnancyOverDue = pregnancyProgress.passedDays > 280
      isPregnancyFifteenDaysOverDue = pregnancyProgress.passedDays > 294
    }

    return {
      pregnancy,
      isPregnancyEnding,
      isPregnancyOverDue,
      isPregnancyFifteenDaysOverDue,
    }
  }

  return {}
})
export default class Statistics extends BaseScreen {
  static propTypes = {
    pregnancy: PropTypes.object,
    isPregnancyEnding: PropTypes.bool,
    isPregnancyOverDue: PropTypes.bool,
    isPregnancyFifteenDaysOverDue: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    activeSlideIndex: 0,
  }

  posters = [
    'pregnancy_progress_fruit',
    'pregnancy_progress_week',
    'pregnancy_progress_day',
    'pregnancy_progress_percentage',
    'pregnancy_progress_baby',
    'pregnancy_progress_summary',
  ]

  render() {
    const {
      pregnancy,
      isPregnancyEnding,
      isPregnancyFifteenDaysOverDue,
    } = this.props
    const { formatMessage } = this.context.intl

    if (!pregnancy) {
      return <Loader />
    }

    return (
      <View style={styles.container}>
        {isPregnancyFifteenDaysOverDue ? (
          <FifteenDaysOverDue />
        ) : (
          <Slider
            slides={this._getSlides()}
            slideContainerStyle={styles.slideContainer}
            onChange={this._slideChanged}
          />
        )}

        <View style={styles.footer}>
          {isPregnancyFifteenDaysOverDue ? (
            <ShareButton
              onPress={this._endPregnancyButtonPrressed}
              title={formatMessage(messages.endPregnancyButtonTitle)}
              style={styles.shareButton}
            />
          ) : (
            <ShareButton
              onPress={this._shareButtonPressed}
              title={formatMessage(messages.shareButtonTitle)}
              style={styles.shareButton}
              icon
            />
          )}

          {isPregnancyEnding ? (
            <ShareButton
              onPress={this._childIsBornButtonPressed}
              title={formatMessage(messages.childIsBornButtonTitle)}
              style={styles.childIsBornButton}
            />
          ) : null}
        </View>
      </View>
    )
  }

  _getSlides = () => {
    const { pregnancy, isPregnancyOverDue } = this.props
    const { activeSlideIndex } = this.state

    const slides = []

    slides.push(
      <PregnancyProgressFruit
        isVisible={activeSlideIndex === slides.length}
        animate
        pregnancy={pregnancy}
      />
    )

    slides.push(
      <PregnancyProgressWeek
        isVisible={activeSlideIndex === slides.length}
        animate
        pregnancy={pregnancy}
      />
    )

    slides.push(
      <PregnancyProgressDay
        isVisible={activeSlideIndex === slides.length}
        animate
        pregnancy={pregnancy}
      />
    )

    if (!isPregnancyOverDue) {
      slides.push(
        <PregnancyProgressPercentage
          isVisible={activeSlideIndex === slides.length}
          animate
          pregnancy={pregnancy}
        />
      )
    }

    slides.push(
      <PregnancyProgressBaby
        isVisible={activeSlideIndex === slides.length}
        animate
        pregnancy={pregnancy}
      />
    )

    slides.push(
      <PregnancyProgressSummary
        isVisible={activeSlideIndex === slides.length}
        animate
        pregnancy={pregnancy}
      />
    )

    return slides
  }

  _slideChanged = activeSlideIndex => this.setState({ activeSlideIndex })

  _childIsBornButtonPressed = () =>
    this._goTo('/forms-child', {
      flow: 'IS_BORN',
      pregnancyId: this.props.pregnancy.id,
    })

  _endPregnancyButtonPrressed = () => this._goTo('/pregnancy-loss')

  _shareButtonPressed = () => {
    const { pregnancy, isPregnancyOverDue } = this.props
    const { activeSlideIndex } = this.state

    let posters = this.posters

    if (isPregnancyOverDue) {
      posters = without(posters, 'pregnancy_progress_percentage')
    }

    this._goTo('/poster', {
      posterId: posters[activeSlideIndex],
      posterContext: {
        pregnancyId: pregnancy.id,
      },
    })
  }
}
