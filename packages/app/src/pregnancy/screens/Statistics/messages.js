import { defineMessages } from 'react-intl'

export default defineMessages({
  shareButtonTitle: 'Share',
  childIsBornButtonTitle: 'My child is born',
  endPregnancyButtonTitle: 'End pregnancy',
})
