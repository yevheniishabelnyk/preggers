import { StyleSheet, Platform } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },

  slideContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  footer: {
    marginTop: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: Platform.OS === 'android' ? moderateScale(60) : moderateScale(72),
    flexDirection: 'row',
  },

  childIsBornButton: {
    marginLeft: moderateScale(20),
  },
})

export default styles
