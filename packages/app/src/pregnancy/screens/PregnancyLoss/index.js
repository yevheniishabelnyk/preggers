/**
 *
 * PregnancyLoss
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import Prompt from '@/shared/components/Prompt'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import Card from '@/shared/components/Card'
import ActionButton from '@/settings/components/atoms/ActionButton'
import SettingsDatePicker from '@/settings/components/molecules/SettingsDatePicker'

import * as RouterActions from '@/app/redux/router/actions'
import * as PregnanciesActions from '@/app/database/actions/pregnancies'

import moment from 'moment'

import messages from './messages'

import styles from './styles'

import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog'

export default class PregnancyLoss extends BaseScreen {
  static propTypes = {
    pregnancyId: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      lossDay: moment().format('YYYY-MM-DD'),
    }

    this.lossMaxDate = new Date()
    this.lossMinDate = moment()
      .subtract(280, 'days')
      .toDate()
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),

          popups: [
            <PopupDialog
              key="deleteDialog"
              ref={ref => (this.deleteDialog = ref)}
              dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
              dialogStyle={styles.dialog}
              overlayOpacity={0.7}
            >
              <Prompt
                question={formatMessage(messages.deleteDialogQuestion)}
                text={formatMessage(messages.deleteDialogText)}
                negativeButton={{
                  title: formatMessage(messages.noButtonTitle),
                  onPress: this._hideDeleteDialog,
                }}
                positiveButton={{
                  title: formatMessage(messages.yesButtonTitle),
                  onPress: this._deletePregnancy,
                }}
              />
            </PopupDialog>,

            <PopupDialog
              key="cancelDialog"
              ref={ref => (this.cancelDialog = ref)}
              dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
              dialogStyle={styles.dialog}
              overlayOpacity={0.7}
            >
              <Prompt
                question={formatMessage(messages.cancelDialogQuestion)}
                text={formatMessage(messages.cancelDialogText)}
                negativeButton={{
                  title: formatMessage(messages.noButtonTitle),
                  onPress: this._hideCancelDialog,
                }}
                positiveButton={{
                  title: formatMessage(messages.yesButtonTitle),
                  onPress: this._cancelPregnancy,
                }}
              />
            </PopupDialog>,
          ],
        }}
        headerHeight={SettingsHeader.height}
      >
        <Card
          title={formatMessage(messages.firstCardTitle)}
          content={formatMessage(messages.firstCardContent)}
          onLinkPress={this._linkPressed}
        />

        <SettingsDatePicker
          title={formatMessage(messages.datePickerTitle)}
          onSubmit={this._setPregnancyLossDay}
          value={this.state.lossDay}
          maximumDate={this.lossMaxDate}
          minimumDate={this.lossMinDate}
          contentContainerStyle={styles.datePicker}
        />

        <ActionButton
          title={formatMessage(messages.cancelPregnancyButtonTitle)}
          onPress={this._showCancelDialog}
          style={styles.button}
        />

        <Card
          title={formatMessage(messages.secondCardTitle)}
          content={formatMessage(messages.secondCardContent)}
          onLinkPress={this._linkPressed}
        />

        <ActionButton
          title={formatMessage(messages.deletePregnancyButtonTitle)}
          onPress={this._showDeleteDialog}
          style={[styles.button, styles.bottomButton]}
        />
      </ViewWrapper>
    )
  }

  _showDeleteDialog = () => this.deleteDialog.show()

  _hideDeleteDialog = () => this.deleteDialog.dismiss()

  _showCancelDialog = () => this.cancelDialog.show()

  _hideCancelDialog = () => this.cancelDialog.dismiss()

  _setPregnancyLossDay = value => this.setState({ lossDay: value })

  _deletePregnancy = async () => {
    const pregnancyId = this.props.navigation.getParam('id')

    if (pregnancyId) {
      await this._(PregnanciesActions.remove(pregnancyId))

      this._(RouterActions.resetToUrl(['/~home/~start-view/missing-pregnancy']))
    }
  }

  _cancelPregnancy = async () => {
    const { lossDay } = this.state

    const pregnancyId = this.props.navigation.getParam('id')

    if (pregnancyId) {
      await this._(PregnanciesActions.cancel(pregnancyId, lossDay))

      this._(RouterActions.resetToUrl(['/~home/~start-view/missing-pregnancy']))
    }
  }

  _linkPressed = link => this._(RouterActions.handleLink(link))
}
