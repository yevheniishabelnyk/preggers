import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Pregnancy loss',
  deleteDialogQuestion: 'Are you sure?',
  deleteDialogText:
    'This step is not reversible, all information about this pregnancy will be deleted.',
  cancelDialogQuestion: 'Are you sure?',
  cancelDialogText:
    'The pregnancy will end and all information will be saved and summarized in the app.',
  noButtonTitle: 'No',
  yesButtonTitle: 'Yes',
  datePickerTitle: 'Date',
  cancelPregnancyButtonTitle: 'End pregnancy',
  deletePregnancyButtonTitle: 'Delete pregnancy',
  cardTitle: 'Card title',
  firstCardTitle: 'End pregnancy',
  firstCardContent:
    'If you choose to end your pregnancy, all information is saved and the pregnancy is terminated and summarized in the app.',
  secondCardTitle: 'Delete pregnancy',
  secondCardContent:
    'If you choose to delete your pregancy, all information about this pregancy will be deleted.',
})
