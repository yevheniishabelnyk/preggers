import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: moderateScale(22.5, 2.5),
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  button: {
    marginTop: moderateScale(20, resizeFactor),
    marginBottom: moderateScale(30, resizeFactor),
  },

  bottomButton: {
    marginBottom: moderateScale(37, resizeFactor),
  },

  datePicker: {
    marginTop: moderateScale(13, resizeFactor),
    alignSelf: 'stretch',
  },

  dialog: {
    width: moderateScale(330, resizeFactor),
    paddingTop: moderateScale(25, resizeFactor),
    height: 'auto',
    overflow: 'hidden',
  },
})

export default styles
