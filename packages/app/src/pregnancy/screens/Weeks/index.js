/**
 *
 * Weeks
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, ActivityIndicator } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import WeekBar from '@/start-view/components/molecules/WeekBar'
import WeekTabSlide from '@/pregnancy/components/organisms/WeekTabSlide'

import Swiper from '@/vendor/react-native-swiper'

import { MIN_PREGNANCY_WEEK, MAX_PREGNANCY_WEEK } from 'config'

import { getWeeks, getPregnancy } from '@/app/redux/selectors'

import { range } from 'lodash'

import styles from './styles'

import Pregnancy from '@/app/database/models/Pregnancy'

const getPregnancyProgress = Pregnancy.makeGetProgress()

const weekNumbers = range(MIN_PREGNANCY_WEEK, MAX_PREGNANCY_WEEK + 1)

@connect((state, props) => {
  const pregnancyId = props.navigation.getParam('pregnancyId')
  const weeks = getWeeks(state)

  if (pregnancyId) {
    const pregnancy = getPregnancy(state, pregnancyId)

    let pregnancyCurrentWeekNumber

    if (pregnancy) {
      const pregnancyProgress = getPregnancyProgress(
        pregnancy.dueDate,
        pregnancy.lengthType
      )

      pregnancyCurrentWeekNumber = pregnancyProgress.pregnancyWeek
    }

    return {
      pregnancy,
      pregnancyCurrentWeekNumber,
      weeks,
      isFetching: pregnancy === undefined,
    }
  }

  return {
    weeks,
    isFetching: true,
  }
})
export default class Weeks extends BaseScreen {
  static propTypes = {
    tabType: PropTypes.string,
    screenProps: PropTypes.shape({
      locale: PropTypes.string,
      profile: PropTypes.object,
    }),
    navigation: PropTypes.object,
    pregnancy: PropTypes.object,
    weeks: PropTypes.object,
    pregnancyCurrentWeekNumber: PropTypes.number,
  }

  state = {
    activeWeekNumber: null,
    swiperScrollEnabled: false,
  }

  constructor(props) {
    super(props)

    const { navigation, pregnancyCurrentWeekNumber } = props

    const activeWeekNumber = navigation.getParam('activeWeekNumber')

    this.weekBarInitialWeek = activeWeekNumber || pregnancyCurrentWeekNumber
  }

  render() {
    const { profile, locale } = this.props.screenProps
    const { pregnancy, tabType, weeks = {} } = this.props
    const { swiperScrollEnabled } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        components={{
          header: (
            <WeekBar
              ref={ref => (this._weekBar = ref)}
              minWeek={MIN_PREGNANCY_WEEK}
              maxWeek={MAX_PREGNANCY_WEEK}
              onWeekChange={this._onWeekChange}
              initialWeek={this.weekBarInitialWeek || MIN_PREGNANCY_WEEK}
            />
          ),
        }}
        headerHeight={WeekBar.height}
      >
        {swiperScrollEnabled ? (
          <Swiper
            ref={ref => (this._swiper = ref)}
            style={styles.swiper}
            loop={false}
            scrollEnabled={this.state.swiperScrollEnabled}
            showsPagination={false}
            index={this.swiperInitialIndex}
            onIndexChanged={this._swiperIndexChanged}
            loadMinimal
            loadMinimalSize={1}
            loadMinimalLoader={
              <View style={styles.loaderWrapper}>
                <ActivityIndicator
                  animating
                  size="large"
                  color="gray"
                  style={styles.loader}
                />
              </View>
            }
          >
            {weekNumbers.map(number => (
              <WeekTabSlide
                tabType={tabType}
                key={number}
                weekNumber={number}
                week={weeks[number]}
                goTo={this._goTo}
                profile={profile}
                locale={locale}
                onBackToCurrentWeek={this._backToCurrentPregnancyWeek}
                pregnancyCurrentWeekNumber={
                  this.props.pregnancyCurrentWeekNumber
                }
                pregnancy={pregnancy}
                disableParrentScroll={this._disableSwiperScroll}
                enableParrentScroll={this._enableSwiperScroll}
              />
            ))}
          </Swiper>
        ) : (
          <View style={styles.loaderWrapper}>
            <ActivityIndicator
              animating
              size="large"
              color="gray"
              style={styles.loader}
            />
          </View>
        )}
      </ViewWrapper>
    )
  }

  componentWillMount() {
    const { pregnancy } = this.props

    if (pregnancy) {
      this._initSwiper()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.pregnancy && nextProps.pregnancy) {
      setTimeout(() => {
        this._initSwiper()
      }, 0)
    }

    if (
      this.props.navigation.getParam('activeWeekNumber') !==
      nextProps.navigation.getParam('activeWeekNumber')
    ) {
      this._activeWeekNumberParamChanged(
        nextProps.navigation.getParam('activeWeekNumber')
      )
    }
  }

  _initSwiper = () => {
    const { pregnancyCurrentWeekNumber } = this.props

    let activeWeekNumber = this.weekBarInitialWeek || pregnancyCurrentWeekNumber

    if (activeWeekNumber < MIN_PREGNANCY_WEEK) {
      activeWeekNumber = MIN_PREGNANCY_WEEK
    } else if (activeWeekNumber > MAX_PREGNANCY_WEEK) {
      activeWeekNumber = MAX_PREGNANCY_WEEK
    }

    this.swiperInitialIndex =
      activeWeekNumber - MIN_PREGNANCY_WEEK > 0
        ? activeWeekNumber - MIN_PREGNANCY_WEEK
        : 0

    this.currentSwiperIndex = this.swiperInitialIndex

    this.setState({
      activeWeekNumber,
      pregnancyCurrentWeekNumber,
      swiperScrollEnabled: true,
    })

    if (activeWeekNumber !== this.weekBarInitialWeek) {
      setTimeout(() => {
        if (this._weekBar) {
          this._weekBar.setWeek(activeWeekNumber)
        }
      }, 0)
    }
  }

  _activeWeekNumberParamChanged = nextActiveWeekNumber => {
    // console.info(
    //   `[${this.props.tabType.toUpperCase()}] nextActiveWeekNumber: `,
    //   nextActiveWeekNumber
    // )
    // console.info(
    //   `[${this.props.tabType.toUpperCase()}] this.isSwipingWeekBar: `,
    //   this.isSwipingWeekBar
    // )
    // console.info(
    //   `[${this.props.tabType.toUpperCase()}] this.isSwipingContent: `,
    //   this.isSwipingContent
    // )
    // console.log('----------------')

    if (this.isSwipingWeekBar) {
      if (this._swiper) {
        const diff =
          nextActiveWeekNumber - (this.currentSwiperIndex + MIN_PREGNANCY_WEEK)

        this.currentSwiperIndex = this.currentSwiperIndex + diff

        this._swiper.scrollBy(diff)
      }
    } else if (this.isSwipingContent) {
      this._weekBar.setWeek(nextActiveWeekNumber)

      this.isSwipingContent = false
    } else {
      setTimeout(() => {
        if (this._swiper) {
          const diff =
            nextActiveWeekNumber -
            (this.currentSwiperIndex + MIN_PREGNANCY_WEEK)
          this.currentSwiperIndex = this.currentSwiperIndex + diff
          this._swiper.scrollBy(diff)
        }
      }, 50)
    }
  }

  _onWeekChange = async weekNumber => {
    // console.info(
    //   `[${this.props.tabType.toUpperCase()}] _onWeekChange: `,
    //   weekNumber
    // )

    if (!this.isSwipingContent) {
      this.isSwipingWeekBar = true

      // console.info(
      //   `[${this.props.tabType.toUpperCase()}] updating content swiper...`
      // )

      const currentActiveWeekNumber = this.props.navigation.getParam(
        'activeWeekNumber'
      )

      if (currentActiveWeekNumber !== weekNumber) {
        this.props.navigation.setParams({ activeWeekNumber: weekNumber })
      } else if (this._swiper) {
        const diff = weekNumber - (this.currentSwiperIndex + MIN_PREGNANCY_WEEK)
        this.currentSwiperIndex = this.currentSwiperIndex + diff
        this._swiper.scrollBy(diff)
      }
    } else {
      this.isSwipingContent = false
    }
  }

  _swiperIndexChanged = index => {
    // console.info(
    //   `[${this.props.tabType.toUpperCase()}] _swiperIndexChanged: `,
    //   index
    // )

    if (!this.isSwipingWeekBar) {
      this.isSwipingContent = true

      // console.info(
      //   `[${this.props.tabType.toUpperCase()}] updating weekbar...`,
      //   index
      // )

      this.currentSwiperIndex = index

      const currentActiveWeekNumber = this.props.navigation.getParam(
        'activeWeekNumber'
      )

      const nextActiveWeekNumber = index + MIN_PREGNANCY_WEEK

      if (currentActiveWeekNumber !== nextActiveWeekNumber) {
        this.props.navigation.setParams({
          activeWeekNumber: index + MIN_PREGNANCY_WEEK,
        })
      } else {
        this._weekBar.setWeek(index + MIN_PREGNANCY_WEEK)
        this.isSwipingContent = false
      }
    } else {
      this.isSwipingWeekBar = false
    }
  }

  _backToCurrentPregnancyWeek = () => {
    const { navigation } = this.props

    const pregnancyCurrentWeekNumber = navigation.getParam(
      'pregnancyCurrentWeekNumber'
    )

    if (pregnancyCurrentWeekNumber && pregnancyCurrentWeekNumber < 4) {
      navigation.setParams({
        activeWeekNumber: 3,
      })
    } else if (pregnancyCurrentWeekNumber && pregnancyCurrentWeekNumber > 42) {
      navigation.setParams({
        activeWeekNumber: 42,
      })
    } else {
      navigation.setParams({
        activeWeekNumber: pregnancyCurrentWeekNumber,
      })
    }
  }

  _disableSwiperScroll = () => this.setState({ swiperScrollEnabled: false })

  _enableSwiperScroll = () => this.setState({ swiperScrollEnabled: true })
}
