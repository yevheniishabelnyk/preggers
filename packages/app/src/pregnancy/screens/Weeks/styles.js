import { StyleSheet, Dimensions, Platform } from 'react-native'

const { height } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    marginTop: Platform.OS === 'android' ? moderateScale(4) : null,
  },

  swiper: {
    height: (430 / 667) * height,
    backgroundColor: 'white',
    marginTop: Platform.OS === 'android' ? moderateScale(-18) : null,
  },

  loaderWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  loader: {
    backgroundColor: 'white',
  },
})

export default styles
