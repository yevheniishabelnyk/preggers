import { StyleSheet, Platform } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const buttonWidth = moderateScale(61)
export const textFontSize = moderateScale(14)

const containerHeight = moderateScale(105)

const adnroidContatinerWidth = moderateScale(340)

export const containerShadow = {
  width: adnroidContatinerWidth,
  height: containerHeight,
  color: '#000000',
  radius: moderateScale(8),
  opacity: Platform.OS === 'android' ? 0.035 : 0.025,
  border: moderateScale(6),
  x: -4,
  y: 0,
  style: { marginVertical: 7 },
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    alignSelf: 'stretch',
    borderRadius: moderateScale(8),
    marginBottom:
      Platform.OS === 'android' ? moderateScale(10) : moderateScale(12),
    marginRight: Platform.OS === 'android' ? moderateScale(4) : null,
    shadowOpacity: 0.05,
    shadowRadius: 6,
    shadowColor: '#000000',
    shadowOffset: { height: 2, width: -2 },
  },

  image: {
    width: moderateScale(105, 0.3),
    height: moderateScale(105, 0.3),
  },

  contentWrapper: {
    flex: 1,
    height: moderateScale(105, 0.3),
    paddingTop: moderateScale(6),
    paddingBottom: moderateScale(11),
    marginLeft: moderateScale(17),
  },

  titleWrapper: {
    flex: 1,
  },

  title: {
    fontFamily: 'Now-Bold',
    lineHeight: moderateScale(19),
    color: 'rgb(35, 40, 46)',
    width: moderateScale(200),
  },

  iconWrapper: {
    marginTop: moderateScale(4),
  },

  icon: {
    width: moderateScale(20),
    height: moderateScale(20),
  },
})

export default styles
