/**
 *
 * Knowledge bank item
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import BaseScreen from '@/app/base/components/BaseScreen'

import { View, Text, Image, TouchableOpacity, Platform } from 'react-native'
import { ExpoImage } from '@/vendor/cache-manager'

import { BoxShadow } from '@/vendor/react-native-shadow'

import { isFunction } from 'lodash'

import styles, { textFontSize, containerShadow } from './styles'

export default class KnowledgeItem extends BaseScreen {
  static propTypes = {
    item: PropTypes.object,
    onPress: PropTypes.func,
  }

  state = {
    titleScale: 1,
  }

  render() {
    return (
      <View>
        {Platform.OS === 'android' ? (
          <BoxShadow setting={containerShadow}>
            {this._renderContent()}
          </BoxShadow>
        ) : (
          this._renderContent()
        )}
      </View>
    )
  }

  _renderContent = () => {
    const { item, style } = this.props

    return (
      <TouchableOpacity activeOpacity={0.95} onPress={this._itemPressed}>
        <View style={[styles.container, style]}>
          {item.image ? (
            <View style={styles.imageWrapper}>
              <ExpoImage source={item.image} style={styles.image} cacheImage />
            </View>
          ) : (
            <View style={styles.imageWrapper}>
              <Image
                style={styles.image}
                source={require('assets/img/article-default.png')}
              />
            </View>
          )}

          <View style={styles.contentWrapper}>
            {item.title ? (
              <View
                onLayout={this._onLayoutTitleWrapper}
                style={styles.titleWrapper}
              >
                <Text
                  style={[
                    styles.title,
                    {
                      fontSize: textFontSize * this.state.titleScale,
                      lineHeight: textFontSize * 1.5 * this.state.titleScale,
                    },
                  ]}
                  numberOfLines={2}
                  onLayout={this._onLayoutTitle}
                >
                  {item.title}
                </Text>
              </View>
            ) : null}

            <View style={styles.iconWrapper}>
              <Image
                source={require('assets/icons/article-icon.png')}
                style={styles.icon}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _onLayoutTitleWrapper = e => {
    this.titleWrapperHeight = e.nativeEvent.layout.height

    if (this.titleHeight) {
      this._fitText(this.titleHeight, this.titleWrapperHeight)
    }
  }

  _onLayoutTitle = e => {
    this.titleHeight = e.nativeEvent.layout.height

    if (this.titleWrapperHeight) {
      this._fitText(this.titleHeight, this.titleWrapperHeight)
    }
  }

  _fitText = (titleHeight, titleWrapperHeight) => {
    if (titleHeight > titleWrapperHeight) {
      this.setState(prevState => ({
        titleScale: prevState.titleScale * 0.94,
      }))
    }
  }

  _itemPressed = () => {
    const { item, onPress } = this.props

    if (isFunction(onPress)) {
      onPress(item.id)
    }
  }
}
