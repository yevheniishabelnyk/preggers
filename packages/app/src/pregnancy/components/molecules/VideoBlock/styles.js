import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

export const contatinerWidth = width - ((2 * 19) / 375) * width
export const buttonWidth = (61 / 375) * width

const styles = StyleSheet.create({
  container: {
    width: contatinerWidth,
    borderRadius: 8,
    marginBottom: (28 / 375) * width,
    shadowOpacity: 0.05,
    shadowRadius: 6,
    shadowColor: '#000000',
    shadowOffset: { height: 4, width: -2 },
    position: 'relative',
  },

  thumb: {
    width: contatinerWidth,
    height: null,
    flex: 1,
  },

  playButton: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    borderWidth: 5,
    borderColor: 'white',
    backgroundColor: 'rgb(64,224,190)',
    position: 'absolute',
    right: (27 / 375) * width,
    top: (205 / 375) * width,
    zIndex: 3,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.2,
    shadowRadius: 9,
    shadowColor: 'rgb(131,133,173)',
    shadowOffset: { height: 12, width: 0 },
  },

  triangle: {
    width: 0,
    height: 0,
    borderTopWidth: (11.5 / 375) * width,
    borderRightWidth: 0,
    borderBottomWidth: (11.5 / 375) * width,
    borderLeftWidth: (20 / 375) * width,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'white',
    backgroundColor: 'transparent',
    marginLeft: (2 / 375) * width,
  },

  info: {
    paddingHorizontal: (24 / 375) * width,
    paddingTop: (16 / 375) * width,
    paddingBottom: (38 / 375) * width,
    backgroundColor: 'white',
  },

  footerRow: {
    height: (24 / 375) * width,
    alignItems: 'center',
    flexDirection: 'row',
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: (18 / 375) * width,
    lineHeight: (25 / 375) * width,
    color: 'rgb(35,40,46)',
  },

  tag: {
    fontFamily: 'Now-Medium',
    fontSize: (12 / 375) * width,
    lineHeight: (25 / 375) * width,
    color: 'rgb(151,166,185)',
  },

  duration: {
    marginLeft: (8 / 375) * width,
  },
})

export default styles
