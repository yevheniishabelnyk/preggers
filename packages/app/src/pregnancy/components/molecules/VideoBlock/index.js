/**
 *
 * VideoBlock
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, Dimensions } from 'react-native'
import { WebBrowser } from 'expo'
import VideoDuration from '@/pregnancy/components/atoms/VideoDuration'
import VideoPlayer from '@/video-player'

import * as YouTubeApi from '@/app/api/YouTube'
import * as VimeoApi from '@/app/api/Vimeo'

import { parse as parseUrl } from 'url'

import styles from './styles'

export default class VideoBlock extends React.Component {
  static propTypes = {
    videoId: PropTypes.string,
    url: PropTypes.string,
    source: PropTypes.any,
  }

  state = {
    videoId: null,
    videoDataIsFetched: false,
    thumb: null,
    tag: 'Health',
    description: '',
    duration: 0,
  }

  render() {
    const {
      // thumb,
      tag,
      title,
      duration,
    } = this.state
    const videoWidth =
      Dimensions.get('window').width -
      (38 / 375) * Dimensions.get('window').width
    const videoHeight = (videoWidth * 9) / 16

    return (
      <View style={styles.container}>
        {/*<Image
          source={thumb}
          style={[
            styles.thumb,
            thumb
              ? {
                  width: contatinerWidth,
                  height: thumb.height * contatinerWidth / thumb.width,
                }
              : null,
          ]}
        />*/}

        <VideoPlayer
          isPortrait={false}
          videoWidth={videoWidth}
          videoHeight={videoHeight}
          style={{}}
          source={this.props.source}
        />

        {/*<TouchableOpacity
          onPress={this._videoPressed}
          underlayColor="rgb(255,255,255)"
          style={[
            styles.playButton,
            thumb && thumb.height
              ? {
                  top:
                    thumb.height * contatinerWidth / thumb.width -
                    buttonWidth / 2,
                }
              : null,
          ]}
        >
          <View style={styles.triangle} />
        </TouchableOpacity>*/}

        <View style={styles.info}>
          <Text style={styles.tag}>{tag.toUpperCase()}</Text>

          <Text style={styles.title}>{title}</Text>

          <View style={styles.footerRow}>
            {this.state.type === 'youtube' ? (
              <Image source={require('assets/icons/video-active.png')} />
            ) : null}

            <VideoDuration style={styles.duration} value={duration} />
          </View>
        </View>
      </View>
    )
  }

  async componentWillMount() {
    const { type } = this.props

    if (type === 'youtube') {
      let videoId

      if (this.props.videoId) {
        videoId = this.props.videoId
      } else if (this.props.url) {
        videoId = this.props.url.split('v=')[1].split('&')[0]
      }

      if (videoId) {
        const data = await YouTubeApi.getVideoData(videoId)

        // console.info('data: ', data)

        this.setState({
          videoId,
          videoDataIsFetched: true,
          duration: data.duration,
          title: data.title,
          description: data.description,
          thumb: data.thumb,
          type: 'youtube',
        })
      }
    } else if (type === 'vimeo') {
      let videoId

      if (this.props.videoId) {
        videoId = this.props.videoId
      } else if (this.props.url) {
        const pathname = parseUrl(this.props.url).pathname
        videoId = pathname.match(/\d+/)[0]
      }

      if (videoId) {
        const data = await VimeoApi.getVideoData(videoId)

        this.setState({
          videoId,
          videoDataIsFetched: true,
          duration: data.duration,
          title: data.title,
          description: data.description,
          thumb: data.thumb,
          type: 'vimeo',
        })
      }
    }
  }

  // async componentWillReceiveProps(nextProps) {
  // let videoId
  // if (this.props.videoId !== nextProps.videoId) {
  //   videoId = nextProps.videoId
  // }
  // if (this.props.url !== nextProps.url) {
  //   videoId = nextProps.url.split('v=')[1].split('&')[0]
  // }
  // if (videoId) {
  //   const data = await Youtube.getVideoData(videoId)
  //   // console.info('data: ', data)
  //   const duration = moment.duration(data.duration).format('H[h] m[m] s[s]')
  //   this.setState({
  //     videoId,
  //     videoDataIsFetched: true,
  //     duration,
  //     title: data.title,
  //     description: data.description,
  //     thumb: data.thumb,
  //   })
  // }
  // }

  _videoPressed = () => {
    const { type } = this.state

    if (type === 'youtube') {
      WebBrowser.openBrowserAsync(
        `https://www.youtube.com/watch?v=${this.state.videoId}`
      )
    } else {
      WebBrowser.openBrowserAsync(`https://vimeo.com/${this.state.videoId}`)
    }
  }
}
