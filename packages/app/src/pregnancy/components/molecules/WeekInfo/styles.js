import { StyleSheet, Platform, Dimensions } from 'react-native'

import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')

export const progressMarkWidth = moderateScale(41)
export const androidContainerWidth = moderateScale(340)
export const initialAndroidContentHeight = moderateScale(1000) // uses only on Android devices

const ulLiPrefixWidth = moderateScale(6)

const gutter = moderateScale(27)

export const imageMaxWidth = width - 2 * gutter

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: moderateScale(27),
    paddingTop: moderateScale(22),
    paddingBottom: moderateScale(20),
    borderRadius: 8,
    marginBottom: moderateScale(28),
    shadowOpacity: 0.15,
    shadowRadius: 10,
    shadowColor: '#111723',
    shadowOffset: { height: 8, width: -2 },
  },

  androidContainerWrapper: {
    alignItems: 'center',
  },

  chartWrapper: {
    paddingTop:
      Platform.OS === 'android' ? moderateScale(20) : moderateScale(10),
    marginTop: Platform.OS === 'android' ? moderateScale(-15) : null,
    marginBottom: moderateScale(-20),
    height: moderateScale(230),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(22),
    color: '#703499',
    letterSpacing: 0,
    marginBottom: moderateScale(13),
  },

  info: {
    flexDirection: 'row',
    height: moderateScale(33),
    marginTop: moderateScale(12),
    alignItems: 'center',
  },

  infoRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  infoRowLength: {
    marginRight: moderateScale(10),
  },

  babyWeight: {
    fontFamily: 'Now-Medium',
    color: '#6f7b96',
  },

  babyLength: {
    fontFamily: 'Now-Medium',
    color: '#6f7b96',
  },

  value: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13),
    color: '#131416',
    marginLeft: moderateScale(5),
  },

  week: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(23),
    color: '#131416',
    lineHeight: moderateScale(33),
    marginBottom: moderateScale(15),
  },

  weekImage: {
    width: null,
    height: null,
    flex: 1,
  },

  motherImageWrapper: {
    position: 'relative',
    width: '100%',
    height: moderateScale(220),
  },

  linkText: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    color: 'rgb(41,123,237)',
  },

  olLiStyle: {
    flexDirection: 'row',
    paddingRight: moderateScale(20),
  },

  ulLiStyle: {
    flexDirection: 'row',
    paddingRight: moderateScale(20),
  },

  olLiPrefix: {
    fontFamily: 'Roboto-Medium',
    fontSize: moderateScale(14),
    color: 'rgb(59,72,89)',
    alignSelf: 'flex-start',
  },

  ulLiPrefix: {
    marginVertical: moderateScale(9),
    marginRight: moderateScale(8),
    width: ulLiPrefixWidth,
    height: ulLiPrefixWidth,
    borderRadius: ulLiPrefixWidth / 2,
    backgroundColor: 'rgb(59,72,89)',
  },

  listWrapper: {
    marginTop: moderateScale(11),
    marginBottom: moderateScale(11),
  },

  audioPlayer: {
    marginTop: moderateScale(14),
    marginBottom: moderateScale(21),
  },

  backToCurrentWeekButton: {
    marginBottom: moderateScale(18),
  },

  babyZoomButton: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? moderateScale(40) : moderateScale(20),
    right: Platform.OS === 'android' ? moderateScale(55) : moderateScale(35),
  },

  motherZoomButton: {
    position: 'absolute',
    bottom: moderateScale(-20),
    right: moderateScale(65),
    zIndex: 10,
  },

  ingressText: {
    alignSelf: 'stretch',
    fontFamily: 'Roboto-Medium',
    fontSize: moderateScale(20),
    lineHeight: moderateScale(26),
    color: 'rgb(42, 44, 46)',
    marginBottom: moderateScale(20),
  },

  progressBarContent: {
    ...StyleSheet.absoluteFillObject,
  },

  progressBarImage: {
    width: null,
    height: null,
    flex: 1,
  },

  progressMark: {
    backgroundColor: '#42e0be',
    width: progressMarkWidth,
    height: progressMarkWidth,
    borderRadius: progressMarkWidth / 2,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowColor: 'black',
    shadowOffset: { height: 3, width: -2 },
  },

  progressMarkTitle: {
    fontFamily: 'Roboto-Bold',
    fontSize: moderateScale(8),
    textAlign: 'center',
    color: 'white',
    marginTop: moderateScale(5),
  },

  progressMarkMessage: {
    fontFamily: 'Roboto-Bold',
    fontSize: moderateScale(15),
    textAlign: 'center',
    color: 'white',
  },
})

export const tagStyles = {
  p: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(23),
    color: 'rgb(59, 70, 85)',
    marginBottom: moderateScale(15),
  },

  strong: {
    fontFamily: 'Roboto-Bold',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    color: 'rgb(76,82,91)',
  },

  em: {
    fontFamily: 'Roboto-LightItalic',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    color: 'rgb(131,146,167)',
  },

  h1: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17),
  },

  h2: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17),
  },

  h3: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    color: 'rgb(59,72,89)',
    marginVertical: moderateScale(17),
  },
}

export default styles
