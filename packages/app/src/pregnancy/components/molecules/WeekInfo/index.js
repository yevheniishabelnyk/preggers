/**
 *
 * WeekInfo
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Text, TouchableOpacity, Platform } from 'react-native'
import { ExpoImage } from '@/vendor/cache-manager'
import HTML from 'react-native-render-html'
import WhiteButton from '@/pregnancy/components/atoms/WhiteButton'
import ZoomButton from '@/pregnancy/components/atoms/ZoomButton'
import CircleProgressBar from '@/pregnancy/components/molecules/CircleProgressBar'
import AudioPlayer from '@/audio-player'

import { BoxShadow } from '@/vendor/react-native-shadow'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { round } from 'lodash'

import styles, {
  tagStyles,
  androidContainerWidth,
  initialAndroidContentHeight,
  imageMaxWidth,
  progressMarkWidth,
} from './styles'

import memoize from 'fast-memoize'

@connect(state => ({
  lengthUnits: state.settings.lengthUnits,
  weightUnits: state.settings.weightUnits,
}))
export default class WeekInfo extends React.Component {
  static propTypes = {
    babyLength: PropTypes.string,
    tabType: PropTypes.string.isRequired,
    babyWeight: PropTypes.string,
    weekNumber: PropTypes.number,
    pregnancyCurrentWeekNumber: PropTypes.number,
    onBackToCurrentWeek: PropTypes.func,
    onLinkPress: PropTypes.func,
    description: PropTypes.string,
    ingressText: PropTypes.string,
    image: PropTypes.any,
    audio: PropTypes.object,
    enableParrentScroll: PropTypes.func,
    disableParrentScroll: PropTypes.func,
    lengthUnits: PropTypes.string,
    weightUnits: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    volume: 1,
    isPlaying: false,
  }

  render() {
    const containerShadow = {
      width: androidContainerWidth,
      height: this.state.height
        ? this.state.height
        : initialAndroidContentHeight,
      color: '#000000',
      radius: 8,
      opacity: 0.025,
      border: 10,
      x: 0,
      y: 0,
      style: { marginVertical: 20 },
    }

    return (
      <View
        style={
          Platform.OS === 'android' ? styles.androidContainerWrapper : null
        }
      >
        {Platform.OS === 'android' ? (
          <BoxShadow setting={containerShadow}>
            {this._renderContent()}
          </BoxShadow>
        ) : (
          this._renderContent()
        )}
      </View>
    )
  }

  _renderContent = () => {
    const { formatMessage } = this.context.intl

    const {
      babyLength,
      babyWeight,
      weekNumber,
      description,
      ingressText,
      image,
      audio,
      enableParrentScroll,
      disableParrentScroll,
      tabType,
      onBackToCurrentWeek,
      pregnancyCurrentWeekNumber,
    } = this.props

    let weekNumberTitle

    if (pregnancyCurrentWeekNumber < 4) {
      weekNumberTitle = '1-3'
    } else if (pregnancyCurrentWeekNumber >= 42) {
      weekNumberTitle = 42
    } else {
      weekNumberTitle = pregnancyCurrentWeekNumber
    }

    const percent = weekNumber * 2.5 > 100 ? 100 : weekNumber * 2.5
    const portion = percent / 100

    return (
      <View style={[styles.container]} onLayout={this._onContentLayout}>
        {weekNumber !== pregnancyCurrentWeekNumber &&
        (weekNumber > 3 || pregnancyCurrentWeekNumber > 3) &&
        pregnancyCurrentWeekNumber <= 42 ? (
          <WhiteButton
            onPress={onBackToCurrentWeek}
            title={formatMessage(messages.backToCurrentWeekButtonTitle, {
              weekNumber: weekNumberTitle,
            })}
            style={styles.backToCurrentWeekButton}
          />
        ) : null}

        {tabType === 'baby' ? (
          <View>
            <TouchableOpacity
              onPress={this._babyWeekImagePressed}
              activeOpacity={0.95}
              style={Platform.OS === 'android' ? styles.chartWrapper : null}
            >
              <CircleProgressBar
                portion={portion}
                circlePartsNumber={3}
                progressMarkElementWidth={progressMarkWidth}
                progressMarkElement={
                  <View style={styles.progressMark}>
                    <FormattedMessage
                      style={styles.progressMarkTitle}
                      {...messages.progressMarkTitle}
                    />

                    <Text style={styles.progressMarkMessage}>{weekNumber}</Text>
                  </View>
                }
              >
                <View style={styles.progressBarContent}>
                  {image ? (
                    <ExpoImage
                      style={styles.progressBarImage}
                      source={image}
                      resizeMode="contain"
                      cacheImage
                    />
                  ) : null}
                </View>
              </CircleProgressBar>
            </TouchableOpacity>

            {image ? (
              <ZoomButton
                style={styles.babyZoomButton}
                images={[{ source: image }]}
                ref={ref => (this._babyGalleryRef = ref)}
              />
            ) : null}
          </View>
        ) : image ? (
          <View style={styles.motherImageWrapper}>
            <TouchableOpacity
              style={styles.weekImage}
              onPress={this._motherWeekImagePressed}
              activeOpacity={0.95}
            >
              <ExpoImage
                style={styles.weekImage}
                source={image}
                resizeMode="contain"
                cacheImage
              />
            </TouchableOpacity>

            {image ? (
              <ZoomButton
                style={styles.motherZoomButton}
                images={[{ source: image }]}
                ref={ref => (this._motherGalleryRef = ref)}
              />
            ) : null}
          </View>
        ) : null}

        {tabType === 'baby' ? (
          <View style={styles.info}>
            {babyLength ? (
              <View style={[styles.infoRow, styles.infoRowLength]}>
                <FormattedMessage
                  {...messages.babyLength}
                  style={styles.babyLength}
                />

                <Text style={styles.value}>
                  {this._convertLengthUnits(babyLength)}
                </Text>
              </View>
            ) : null}

            {babyWeight ? (
              <View style={styles.infoRow}>
                <FormattedMessage
                  {...messages.babyWeight}
                  style={styles.babyWeight}
                />

                <Text style={styles.value}>
                  {this._convertWeightUnits(babyWeight)}
                </Text>
              </View>
            ) : null}
          </View>
        ) : null}

        <FormattedMessage
          {...messages.weekNumber}
          style={styles.week}
          values={{ number: weekNumber > 3 ? weekNumber : '1-3' }}
        />

        {audio && audio.uri ? (
          <AudioPlayer
            url={audio.uri}
            disableParrentScroll={disableParrentScroll}
            enableParrentScroll={enableParrentScroll}
            overlayText={formatMessage(messages.audioPlayerOverlayText)}
            style={styles.audioPlayer}
          />
        ) : null}

        {ingressText ? (
          <Text style={styles.ingressText}>{ingressText}</Text>
        ) : null}

        {description ? (
          <HTML
            html={description.replace(
              new RegExp('<li[^>]*>([\\s\\S]*?)<\\/li>', 'g'),
              '<li>$1</li>'
            )}
            // eslint-disable-next-line react/jsx-no-bind
            customWrapper={RNContent => (
              <View style={styles.contentConteiner}>{RNContent}</View>
            )}
            textSelectable
            imagesMaxWidth={imageMaxWidth}
            onLinkPress={this._linkPressed}
            tagsStyles={tagStyles}
            renderers={{
              ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, (child, index) => {
                  return (
                    <View style={styles.olLiStyle}>
                      <Text style={styles.olLiPrefix}>{index + 1}.</Text>
                      {child}
                    </View>
                  )
                })

                return (
                  <View key={passProps.key} style={styles.listWrapper}>
                    {content}
                  </View>
                )
              },

              ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                const content = React.Children.map(children, child => {
                  return (
                    <View style={styles.ulLiStyle}>
                      <View style={styles.ulLiPrefix} />
                      {child}
                    </View>
                  )
                })

                return (
                  <View key={passProps.key} style={styles.listWrapper}>
                    {content}
                  </View>
                )
              },
            }}
          />
        ) : null}
      </View>
    )
  }

  _linkPressed = (evt, href) => {
    const { onLinkPress } = this.props

    onLinkPress(href)
  }

  _onContentLayout = e => {
    this.setState({
      height: e.nativeEvent.layout.height,
      width: e.nativeEvent.layout.width,
    })
  }
  _babyWeekImagePressed = () => this._babyGalleryRef.show()

  _motherWeekImagePressed = () => this._motherGalleryRef.show()

  _convertLengthUnits = value =>
    this._memoizedConvertLengthUnits(value, this.props.lengthUnits)

  _memoizedConvertLengthUnits = memoize((value, lengthUnits) => {
    let newValue

    if (lengthUnits === 'in') {
      newValue = round(value / 2.54, 1)
    } else {
      newValue = value
    }

    return lengthUnits ? `${newValue} ${lengthUnits.toLowerCase()}` : ''
  })

  _convertWeightUnits = value =>
    this._memoizedConvertWeightUnits(value, this.props.weightUnits)

  _memoizedConvertWeightUnits = memoize((value, weightUnits) => {
    let newValue

    if (weightUnits === 'g') {
      newValue = `${value} g`
    } else {
      const nearExact = value / 1000 / 0.45359237
      const lbs = Math.floor(nearExact)
      const oz = round((nearExact - lbs) * 16, 2)

      newValue = `${lbs} lb ${oz} oz`
    }

    return newValue
  })
}
