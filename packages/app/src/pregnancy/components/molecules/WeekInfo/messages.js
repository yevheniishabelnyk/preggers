import { defineMessages } from 'react-intl'

export default defineMessages({
  babyLength: 'Length',
  babyWeight: 'Weight',
  weekNumber: 'Week {number}',
  audioPlayerOverlayText: 'Play this text',
  readMoreButtonTitle: 'Read more',
  backToCurrentWeekButtonTitle: 'Back to current week {weekNumber}',
  progressMarkTitle: 'Week',
})
