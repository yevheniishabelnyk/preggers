/**
 *
 * CircleProgressBar
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import CircleSegment from '@/shared/components/CircleSegment'

import { scalableStyles } from '@/app/scaling'
import rawStyles, { defaultWidth, strokeWidth } from './styles'

@scalableStyles(rawStyles)
export default class CircleProgressBar extends React.Component {
  static propTypes = {
    children: PropTypes.any,
    portion: PropTypes.number,
    scale: PropTypes.number,
    circlePartsNumber: PropTypes.number,
    progressMarkElement: PropTypes.element,
    progressMarkElementWidth: PropTypes.number,
  }

  render() {
    const {
      children,
      scale,
      portion,
      progressMarkElement,
      progressMarkElementWidth,
      circlePartsNumber,
    } = this.props

    let p = portion
    if (p > 1) {
      p = 1
    }

    let strokes = [
      { color: 'linear-pink', gradient: true, segment: p },
      { color: '#E6E8F1', segment: 1 - p },
    ]

    if (p === 0) {
      strokes = [
        { color: '#E6E8F1', segment: 0.5 },
        { color: '#E6E8F1', segment: 0.5 },
      ]
    }

    this.styles.scale = scale
    const scaledWidth = defaultWidth * this.styles.scale
    const scaledStrokeWidth = strokeWidth * this.styles.scale

    const circleRadius = ((defaultWidth - strokeWidth) / 2) * this.styles.scale

    const progressMarkPosition = getProgressMarkPositionStyle(
      portion * 100,
      circleRadius
    )

    return (
      <View style={this.styles.container}>
        <CircleSegment
          width={scaledWidth}
          strokeWidth={scaledStrokeWidth}
          strokes={strokes}
        />

        {circlePartsNumber
          ? getAnglesByCirclePortionsNumber(circlePartsNumber).map(angle => (
              <View
                key={angle}
                style={[
                  this.styles.circlePortionLineContainer,
                  {
                    ...getCirclePortionLineContainerPositionStyle(
                      angle,
                      1,
                      this.styles.scale
                    ),
                  },
                ]}
              >
                <View
                  style={[
                    this.styles.circlePortionLine,
                    {
                      transform: [
                        {
                          rotate: angle - 90 + 'deg',
                        },
                      ],
                    },
                  ]}
                />
              </View>
            ))
          : null}

        <View
          style={[
            {
              marginTop:
                (-progressMarkElementWidth * this.styles.scale) / 2 - 5,
              marginLeft:
                (-progressMarkElementWidth * this.styles.scale) / 2 - 4,
            },
            progressMarkPosition,
          ]}
        >
          {progressMarkElement}
        </View>

        <View style={this.styles.contentWrapper}>{children}</View>
      </View>
    )
  }
}

const deg2rad = Math.PI / 180

function getProgressMarkPositionStyle(percent, circleRadius) {
  const radians = ((360 * ((percent + 75) % 100)) / 100) * deg2rad

  const positionOffSet = 8

  const top =
    circleRadius + Math.round(circleRadius * Math.sin(radians)) + positionOffSet

  const left =
    circleRadius + Math.round(circleRadius * Math.cos(radians)) + positionOffSet

  return { top, left, position: 'absolute' }
}

const defaultCenterX = (defaultWidth - strokeWidth) / 2
const defaultCenterY = defaultCenterX

function getCirclePortionLineContainerPositionStyle(angle, distance, scale) {
  const centerX = defaultCenterX * scale - 0.1
  const centerY = defaultCenterY * scale - 0.1
  const radius = defaultCenterX * scale - 1

  const left =
    centerX + radius * Math.cos(((angle - 90) * Math.PI) / 180) * distance
  const top =
    centerY + radius * Math.sin(((angle - 90) * Math.PI) / 180) * distance

  return { top, left }
}

function getAnglesByCirclePortionsNumber(value) {
  const angles = []

  for (let i = 0; i < value; i++) {
    angles.push((360 / value) * i)
  }

  return angles
}
