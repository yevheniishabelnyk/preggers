import { moderateScale } from '@/app/scaling'

export const defaultWidth = moderateScale(190)
export const strokeWidth = moderateScale(6)

export default {
  container: {
    position: 'relative',
  },

  contentWrapper: {
    position: 'absolute',
    top: 1,
    left: 1,
    width: defaultWidth - 2,
    height: defaultWidth - 2,
    zIndex: -1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: (defaultWidth - 2) / 2,
    overflow: 'hidden',
  },

  circlePortionLineContainer: {
    position: 'absolute',
    width: strokeWidth,
    height: strokeWidth,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
    borderRadius: strokeWidth / 2,
  },

  circlePortionLine: {
    width: strokeWidth + 0.1,
    marginLeft: -0.2,
    height: moderateScale(2),
    backgroundColor: 'white',
  },
}
