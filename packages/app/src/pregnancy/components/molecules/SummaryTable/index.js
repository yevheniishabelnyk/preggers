/**
 *
 * SummaryTable
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'

import { keys } from 'lodash'

import rawStyles from './styles'
import { scalableStyles } from '@/app/scaling'

import chroma from 'chroma-js'

@scalableStyles(rawStyles)
export default class SummaryTable extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    lightTextColor: PropTypes.string,
    darkTextColor: PropTypes.string,
    scale: PropTypes.number,
    style: PropTypes.any,
  }

  render() {
    const { data, style, lightTextColor, darkTextColor, scale } = this.props

    this.styles.scale = scale

    return (
      <View style={style}>
        {keys(data).map(key => (
          <View
            key={key}
            style={[
              this.styles.tableRow,
              lightTextColor
                ? {
                    borderBottomColor: chroma(lightTextColor)
                      .alpha(0.3)
                      .css(),
                  }
                : null,
            ]}
          >
            <View style={this.styles.cell}>
              <Text
                style={[
                  this.styles.text,
                  lightTextColor ? { color: lightTextColor } : null,
                ]}
              >
                {data[key].title}
              </Text>
            </View>

            <View style={[this.styles.cell, this.styles.rightCell]}>
              <Text
                style={[
                  this.styles.text,
                  this.styles.rightCellText,
                  darkTextColor ? { color: darkTextColor } : null,
                ]}
              >
                {data[key].value}
              </Text>
            </View>
          </View>
        ))}
      </View>
    )
  }
}
