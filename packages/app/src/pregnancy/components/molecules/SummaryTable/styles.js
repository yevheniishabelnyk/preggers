import { moderateScale } from '@/app/scaling'

export default {
  tableRow: {
    flexDirection: 'row',
    height: moderateScale(33),
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(151, 151, 151, 0.29)',
  },

  whiteSeparator: {
    borderBottomColor: 'white',
  },

  rightCell: {
    width: moderateScale(131),
    marginLeft: 'auto',
  },

  rightCellText: {
    fontFamily: 'Now-Bold',
    color: 'rgb(23,24,28)',
    textAlign: 'right',
  },

  text: {
    fontFamily: 'Now-Regular',
    color: '#95a2b4',
    fontSize: moderateScale(13),
    lineHeight: moderateScale(15),
    marginTop: 'auto',
  },
}
