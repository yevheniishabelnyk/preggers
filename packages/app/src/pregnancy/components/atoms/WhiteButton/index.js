/**
 *
 * WhiteButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Text } from 'react-native'

import styles from './styles'

const WhiteButton = ({ title, style, onPress }) => (
  <TouchableOpacity
    onPress={onPress}
    style={[styles.button, style]}
    activeOpacity={0.95}
  >
    <Text style={styles.title}>{title}</Text>
  </TouchableOpacity>
)

WhiteButton.propTypes = {
  title: PropTypes.string,
  style: PropTypes.any,
  onPress: PropTypes.func,
}

export default WhiteButton
