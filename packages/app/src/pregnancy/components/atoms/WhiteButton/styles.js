import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  button: {
    height: moderateScale(40, resizeFactor),
    width: moderateScale(222, resizeFactor),
    borderRadius: moderateScale(30, resizeFactor),
    borderWidth: moderateScale(2, resizeFactor),
    borderColor: 'rgb(250,65,105)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(24, resizeFactor),
    color: 'rgb(57,56,56)',
    textAlign: 'center',
  },
})

export default styles
