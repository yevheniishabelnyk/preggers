/**
 *
 * WeekButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, TouchableOpacity } from 'react-native'

import styles from './styles'

export default class WeekButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    title: PropTypes.string,
  }

  render() {
    const { onPress, style, title } = this.props

    return (
      <TouchableOpacity
        style={[styles.button, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Text style={styles.title}>{title}</Text>
      </TouchableOpacity>
    )
  }
}
