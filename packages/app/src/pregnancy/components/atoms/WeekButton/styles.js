import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: moderateScale(38),
    paddingVertical: moderateScale(11),
    backgroundColor: 'rgb(250,65,105)',
    borderRadius: moderateScale(30),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15),
    letterSpacing: 0.8,
    color: 'white',
    textAlign: 'center',
  },
})

export default styles
