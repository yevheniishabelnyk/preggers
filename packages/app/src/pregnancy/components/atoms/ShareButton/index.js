/**
 *
 * ShareButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Image, TouchableHighlight, View, Text } from 'react-native'
import { LinearGradient } from 'expo'

import whiteCamera from 'assets/icons/white-camera.png'

import styles from './styles'

const ShareButton = ({ onPress, title, icon, style }) => (
  <TouchableHighlight
    onPress={onPress}
    style={[styles.button, style]}
    underlayColor="transparent"
  >
    <View style={styles.container}>
      <View style={styles.content}>
        {icon ? (
          <Image
            source={whiteCamera}
            style={styles.icon}
            resizeMode={'contain'}
          />
        ) : null}

        <Text style={styles.title}>{title}</Text>
      </View>

      <LinearGradient
        colors={['rgb(198,53,131)', 'rgb(246,54,105)']}
        start={[0, 0.4]}
        end={[1, 0.6]}
        style={styles.backgroundGradient}
      />
    </View>
  </TouchableHighlight>
)

ShareButton.propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.string,
  icon: PropTypes.bool,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number,
  ]),
}

export default ShareButton
