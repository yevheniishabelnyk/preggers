import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    height: moderateScale(48),
    width: moderateScale(160),
    backgroundColor: 'white',
    alignItems: 'stretch',
    justifyContent: 'center',
    borderRadius: moderateScale(150),
    shadowOpacity: 0.25,
    shadowRadius: 6,
    shadowColor: 'rgb(140,45,124)',
    shadowOffset: { height: 5, width: -2 },
  },

  container: {
    flex: 1,
    position: 'relative',
    flexDirection: 'row',
    borderRadius: moderateScale(150),
    overflow: 'hidden',
  },

  backgroundGradient: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: 1,
  },

  content: {
    position: 'absolute',
    top: 1,
    bottom: 1,
    right: 1,
    left: 1,
    borderRadius: moderateScale(150),
    overflow: 'hidden',
    backgroundColor: 'transparent',
    zIndex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    color: 'white',
    letterSpacing: 0,
    textAlign: 'center',
  },

  icon: {
    width: moderateScale(25),
    height: moderateScale(21),
    marginRight: moderateScale(16),
  },
})

export default styles
