import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    zIndex: 100,
  },

  icon: {
    width: moderateScale(40, 0.3),
    height: moderateScale(40, 0.3),
  },
})

export default styles
