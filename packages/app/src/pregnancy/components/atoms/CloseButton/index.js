/**
 *
 * CloseButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import styles from './styles'

export default class CloseButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { onPress, style } = this.props

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.button, style]}
        activeOpacity={0.95}
      >
        <Image
          source={require('assets/icons/close-black.png')}
          style={styles.icon}
        />
      </TouchableOpacity>
    )
  }
}
