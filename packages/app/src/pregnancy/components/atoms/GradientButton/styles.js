import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  button: {
    width: width * 0.91,
    marginHorizontal: width * 0.045,
    marginVertical: width * 0.06,
    height: height * 0.092,
    borderRadius: 100,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: 'red',
    borderColor: 'rgba(151, 151, 151, 0.46)',
    borderWidth: 1,
    shadowOpacity: 0.15,
    shadowRadius: 8,
    shadowColor: '#111723',
    shadowOffset: { height: 10, width: -2 },
  },

  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonActive: {
    opacity: 1,
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderWidth: 0,
  },

  buttonOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 100,
  },

  buttonText: {
    color: 'white',
    fontSize: 16,
    backgroundColor: 'transparent',
    letterSpacing: 0,
    paddingHorizontal: 20,
  },

  buttonTextActive: {
    color: '#FFFFFF',
    fontFamily: 'Now-Medium',
  },
})

export default styles
