/*
 *
 * GradientButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableHighlight, View, Text } from 'react-native'
import { LinearGradient } from 'expo'

import styles from './styles'

const GradientButton = ({
  title,
  onPress,
  style,
  textStyle,
  active = true,
}) => (
  <TouchableHighlight
    onPress={onPress}
    underlayColor="rgba(255,255,255,.5)"
    style={[styles.button, active ? styles.buttonActive : null, style]}
  >
    <View style={styles.container}>
      {active ? (
        <LinearGradient
          colors={['rgb(198, 53, 131)', 'rgb(246, 54, 105)']}
          start={[0, 0.4]}
          end={[1, 0.6]}
          style={styles.buttonOverlay}
        />
      ) : null}

      <Text
        style={[
          styles.buttonText,
          active ? styles.buttonTextActive : null,
          textStyle,
        ]}
      >
        {title}
      </Text>
    </View>
  </TouchableHighlight>
)

GradientButton.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  style: PropTypes.any,
  textStyle: PropTypes.any,
  active: PropTypes.bool,
}

export default GradientButton
