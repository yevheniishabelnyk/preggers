import { moderateScale } from '@/app/scaling'

export default {
  container: {
    width: '100%',
    height: moderateScale(58),
    position: 'relative',
  },

  bar: {
    backgroundColor: 'rgb(230,232,241)',
    borderRadius: moderateScale(10),
    height: moderateScale(10),
    flexDirection: 'row',
    position: 'relative',
    alignSelf: 'stretch',
    marginTop: 'auto',
  },

  progress: {
    borderRadius: moderateScale(10),
    position: 'relative',
  },

  percentBlock: {
    width: moderateScale(47.4),
    height: moderateScale(58),
    position: 'absolute',
    marginLeft: -moderateScale(23.7),
    bottom: 0,
  },

  percentWrapper: {
    width: moderateScale(47.4),
    height: moderateScale(47.4),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 2,
    top: 0,
    left: 0,
  },

  percent: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: moderateScale(10),
    color: '#FFFFFF',
    letterSpacing: 0,
    backgroundColor: 'transparent',
    textAlign: 'center',
  },

  drop: {
    width: moderateScale(47.4),
    height: moderateScale(58),
  },
}
