/**
 *
 * GradientBar
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'
import { LinearGradient, Svg } from 'expo'

import { scalableStyles } from '@/app/scaling'
import rawStyles from './styles'

const { G, Circle, Path } = Svg

@scalableStyles(rawStyles)
export default class GradientBar extends React.Component {
  static propTypes = {
    progress: PropTypes.number,
    scale: PropTypes.number,
    style: PropTypes.any,
  }

  static defaultProps = {
    progress: 0,
  }

  render() {
    const { progress, style, scale } = this.props

    const percent = Math.round(progress * 1000) / 10

    this.styles.scale = scale

    return (
      <View style={[this.styles.container, style]}>
        <View style={this.styles.bar}>
          <LinearGradient
            colors={['#C723B5', '#F63669']}
            start={[0, 0.4]}
            end={[1, 0.6]}
            style={[this.styles.progress, { width: `${percent}%` }]}
          />
        </View>

        <View style={[this.styles.percentBlock, { left: `${percent}%` }]}>
          <Svg style={this.styles.drop} viewBox="0 0 49 60">
            <G fill="none" fillRule="evenodd">
              <G transform="translate(4 2)">
                <Circle fill="#40E0BE" cx={20.5} cy={20.5} r={20.5} />
              </G>

              <Path fill="#40E0BE" d="M24.5 60L42 33H7z" />
            </G>
          </Svg>

          <View style={this.styles.percentWrapper}>
            <Text style={this.styles.percent}>{percent}%</Text>
          </View>
        </View>
      </View>
    )
  }
}
