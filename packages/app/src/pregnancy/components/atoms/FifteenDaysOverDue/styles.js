import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: moderateScale(60),
    width: moderateScale(261),
    alignItems: 'center',
  },

  image: {
    height: moderateScale(166.4),
    width: moderateScale(260),
  },

  weekTitle: {
    fontSize: moderateScale(23),
    lineHeight: moderateScale(33),
    fontFamily: 'Now-Medium',
    color: 'rgb(23,24,28)',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingVertical: moderateScale(25),
  },

  weekTitleContainer: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: 'rgba(151,151,151, 0.29)',
    borderTopColor: 'rgba(151,151,151, 0.29)',
  },
})

export default styles
