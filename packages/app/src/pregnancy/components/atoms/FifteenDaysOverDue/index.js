/**
 *
 * FifteenDaysOverDue
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image } from 'react-native'
import ViewportText from '@/shared/components/ViewportText'

import messages from './messages'

import styles from './styles'

export default class FifteenDaysOverDue extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={require('assets/img/fifteen-days-over-due.png')}
          resizeMode="contain"
        />

        <ViewportText
          text={formatMessage(messages.title)}
          textStyle={styles.weekTitle}
          contentContainerStyle={styles.weekTitleContainer}
          alignCenter
        />
      </View>
    )
  }
}
