/**
 *
 * Tab
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, View, TouchableHighlight, Platform } from 'react-native'

import { BoxShadow } from '@/vendor/react-native-shadow'

import styles, { defaultContainerWidth, containerHeight } from './styles'

export default class Tab extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    title: PropTypes.string,
    active: PropTypes.bool,
    style: PropTypes.any,
    containerWidth: PropTypes.number,
  }

  render() {
    const { onPress, containerWidth } = this.props

    const containerShadow = {
      width: containerWidth || defaultContainerWidth,
      height: containerHeight,
      color: '#000000',
      radius: 18,
      opacity: 0.025,
      border: 4,
      x: 0,
      y: 1,
      style: { marginVertical: 8, marginHorizontal: 4 },
    }

    return (
      <TouchableHighlight
        underlayColor="transparent"
        onPress={onPress}
        style={styles.container}
      >
        {Platform.OS === 'android' ? (
          <BoxShadow setting={containerShadow}>
            {this._renderContent()}
          </BoxShadow>
        ) : (
          this._renderContent()
        )}
      </TouchableHighlight>
    )
  }

  _renderContent = () => {
    const { title, active, style } = this.props

    return (
      <View
        style={[styles.buttonWrapper, active ? styles.activeBg : null, style]}
      >
        <Text style={[styles.title, active ? styles.activeTitle : null]}>
          {title}
        </Text>
      </View>
    )
  }
}
