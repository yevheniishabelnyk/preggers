import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.7

export const containerHeight = moderateScale(37, resizeFactor)
export const defaultContainerWidth = moderateScale(80, resizeFactor)

const styles = StyleSheet.create({
  buttonWrapper: {
    height: moderateScale(37, resizeFactor),
    position: 'relative',
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: moderateScale(19, resizeFactor),
    shadowOpacity: 0.1,
    shadowRadius: 5,
    shadowColor: 'rgba(0,0,0, 0.5)',
    shadowOffset: { height: 2, width: 0 },
  },

  container: {
    borderRadius: moderateScale(19, resizeFactor),
    height: moderateScale(54, resizeFactor),
    justifyContent: 'center',
  },

  activeBg: {
    backgroundColor: 'rgb(250, 65, 105)',
  },

  title: {
    fontSize: moderateScale(11, resizeFactor),
    lineHeight: moderateScale(22, resizeFactor),
    fontFamily: 'Now-Bold',
    color: 'rgb(34, 33, 39)',
    letterSpacing: 0,
    textAlign: 'center',
    flexShrink: 1,
    width: 'auto',
    alignSelf: 'center',
  },

  activeTitle: {
    color: 'white',
  },

  titleWrapper: {
    alignSelf: 'center',
    height: moderateScale(25, resizeFactor),
    marginTop: moderateScale(-7, resizeFactor),
  },
})

export default styles
