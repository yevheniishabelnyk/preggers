/**
 *
 * VideoDuration
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'

import moment from 'moment'
import momentDurationFormatSetup from 'moment-duration-format'
momentDurationFormatSetup(moment)

import styles from './styles'

export default class VideoDuration extends React.Component {
  static propTypes = {
    value: PropTypes.number,
    style: PropTypes.any,
  }

  render() {
    const { value, style } = this.props

    return (
      <Text style={[styles.text, style]}>
        {moment.duration(value).format('H[h] m[m] s[s]')}
      </Text>
    )
  }
}
