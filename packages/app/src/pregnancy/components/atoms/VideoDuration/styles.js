import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: (12 / 375) * width,
    lineHeight: (24 / 375) * width,
    color: 'rgb(131,146,167)',
  },
})

export default styles
