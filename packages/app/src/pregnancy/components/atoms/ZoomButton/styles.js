import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  button: {
    width: (45 / 375) * width,
    height: (48 / 375) * width,
    alignItems: 'center',
    justifyContent: 'center',
  },

  image: {
    width: (30 / 375) * width,
    height: (32 / 375) * width,
    zIndex: 10,
  },
})

export default styles
