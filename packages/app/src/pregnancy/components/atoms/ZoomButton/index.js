/**
 *
 * Zoom Button
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import imageGalleryModal from '@/shared/decorators/imageGalleryModal'

import styles from './styles'

@imageGalleryModal
export default class ZoomButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { onPress, style } = this.props

    return (
      <TouchableOpacity
        style={[styles.button, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Image
          source={require('assets/icons/zoom-icon.png')}
          style={styles.image}
        />
      </TouchableOpacity>
    )
  }
}
