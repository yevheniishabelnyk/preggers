import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'
import { defaults as theme } from './colors'

export const aspectRatio = 262 / 366
export const defaultWidth = moderateScale(262)
export const defaultHeight = moderateScale(366)
export const progressMarkWidth = moderateScale(41)

export default {
  container: {
    width: defaultWidth,
    height: defaultHeight,
    overflow: 'hidden',
    alignItems: 'center',
    backgroundColor: theme.ackgroundColor,
    paddingTop: moderateScale(20),
  },

  progressBarContent: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
  },

  progressMark: {
    backgroundColor: '#42e0be',
    width: progressMarkWidth,
    height: progressMarkWidth,
    borderRadius: progressMarkWidth / 2,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowColor: 'black',
    shadowOffset: { height: 3, width: -2 },
  },

  progressMarkMessage: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: moderateScale(10),
    textAlign: 'center',
    color: 'white',
  },

  progressBarSubTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    textAlign: 'center',
    color: theme.textColor,
  },

  progressBarTitleBlock: {
    flexDirection: 'row',
    height: moderateScale(78),
    position: 'relative',
    marginBottom: '3%',
    marginTop: '-1%',
    overflow: 'visible',
  },

  progressBarTitle: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(80),
    lineHeight: moderateScale(100),
    textAlign: 'center',
    color: theme.textColor,
  },

  dayMessage: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(28),
    lineHeight: moderateScale(36),
    textAlign: 'center',
    color: theme.textColor,
    marginTop: moderateScale(17),
    marginBottom: moderateScale(15),
  },

  days: {
    fontFamily: 'Now-Bold',
  },

  overDueMessageContainer: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  overDueMessage: {
    flexWrap: 'wrap',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(23),
    lineHeight: moderateScale(33),
    textAlign: 'center',
    color: theme.textColor,
  },

  dueDateMessage: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    textAlign: 'center',
    color: theme.textColor,
    marginTop: moderateScale(17),
  },

  dueDate: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(25),
    textAlign: 'center',
    color: theme.textColor,
    marginBottom: moderateScale(9),
  },

  info: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.borderColor,
    height: moderateScale(87),
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    marginBottom: moderateScale(10),
  },
}
