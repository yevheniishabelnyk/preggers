import { defineMessages } from 'react-intl'

export default defineMessages({
  progressBarSubTitleTop: 'I HAVE',
  progressBarSubTitleTopOverDue: 'I HAVE MAX',
  progressBarSubTitleBottom: 'DAYS LEFT',
  weekMessage: 'Week',
  dueDateMessage: 'MY ESTIMATED DUE DATE IS',
  overDueMessage: '{childName} will come any day now!',
  defaultChildName: 'My baby',
})
