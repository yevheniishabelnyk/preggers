import { StyleSheet, Dimensions, Platform } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(19, 3)

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    width,
    paddingHorizontal: Platform.OS === 'ios' ? gutter : 0,
    position: 'relative',
  },

  slideContainer: {
    paddingTop: moderateScale(10),
    paddingBottom: moderateScale(50),
  },

  loaderWrapper: {
    height: moderateScale(430),
    width,
    alignItems: 'center',
    justifyContent: 'center',
  },

  loader: {
    backgroundColor: 'white',
  },

  noData: {
    width,
    height: moderateScale(430),
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },

  noDataText: {
    fontSize: moderateScale(14),
  },

  adsCointainer: {
    marginTop: moderateScale(-20),
    marginBottom: moderateScale(50),
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default styles
