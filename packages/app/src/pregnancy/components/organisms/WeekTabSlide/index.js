/**
 *
 * WeekTabSlide
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, ScrollView, ActivityIndicator } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import WeekInfo from '@/pregnancy/components/molecules/WeekInfo'
import KnowledgeItems from '@/pregnancy/components/organisms/KnowledgeItems'
import ContentServiceAdvertisement from '@/ads/components/molecules/ContentServiceAdvertisement'

import { shuffle, has, keys } from 'lodash'

import * as RouterActions from '@/app/redux/router/actions'
import * as AdActions from '@/app/redux/ads/actions'

import { firebaseConnect } from 'react-redux-firebase'
import {
  getArticlesById,
  getWeekAdsByNumberAndType,
} from '@/app/redux/selectors'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@firebaseConnect(props => {
  const { week } = props

  if (week && week.articles) {
    return keys(week.articles).map(articleId => `content/articles/${articleId}`)
  }
})
@connect((state, props) => {
  const { week, weekNumber, tabType } = props

  let articles
  let ads

  if (week && week.articles) {
    articles = getArticlesById(state, keys(week.articles))
  }

  if (weekNumber && tabType) {
    ads = getWeekAdsByNumberAndType(state, weekNumber, tabType)
  }

  return {
    articles,
    ads,
  }
})
export default class WeekTabSlide extends BaseScreen {
  static propTypes = {
    profile: PropTypes.object,
    week: PropTypes.object,
    firebase: PropTypes.object,
    pregnancy: PropTypes.object,
    locale: PropTypes.string,
    onBackToCurrentWeek: PropTypes.func,
    disableParrentScroll: PropTypes.func,
    enableParrentScroll: PropTypes.func,
    tabType: PropTypes.string.isRequired,
    weekNumber: PropTypes.number.isRequired,
    pregnancyCurrentWeekNumber: PropTypes.number,
    articles: PropTypes.array,
    goTo: PropTypes.func,
  }

  static defaultProps = {
    ads: [],
    articles: [],
  }

  constructor(props) {
    super(props)

    this.state = {
      isFetching: props.week === undefined,
      ads: [],
    }
  }

  render() {
    const {
      disableParrentScroll,
      enableParrentScroll,
      tabType,
      week,
      articles,
      locale,
    } = this.props

    const { isFetching, ads } = this.state

    if (isFetching) {
      return (
        <View style={styles.loaderWrapper}>
          <ActivityIndicator
            animating
            size="large"
            color="gray"
            style={styles.loader}
          />
        </View>
      )
    }

    if (week) {
      const currentLocaleArticles = articles.filter(
        article => article.language === locale
      )

      return (
        <ScrollView
          style={styles.slide}
          contentContainerStyle={styles.slideContainer}
        >
          <WeekInfo
            tabType={tabType}
            babyLength={week.babyLength}
            babyWeight={week.babyWeight}
            weekNumber={week.number}
            onBackToCurrentWeek={this.props.onBackToCurrentWeek}
            onLinkPress={this._linkPressed}
            pregnancyCurrentWeekNumber={this.props.pregnancyCurrentWeekNumber}
            description={this._getBabyArticle(week)}
            image={this._getTabImage(week)}
            audio={this._getTabAudio(week)}
            ingressText={this._getIngressText(week)}
            disableParrentScroll={disableParrentScroll}
            enableParrentScroll={enableParrentScroll}
          />

          <View style={styles.adsCointainer}>
            {ads &&
              ads.map(ad => (
                <ContentServiceAdvertisement
                  key={ad.uid}
                  ad={ad}
                  dispatch={this.props.dispatch}
                  containerStyle={styles.advertisingCardContainer}
                />
              ))}
          </View>

          <KnowledgeItems
            articles={currentLocaleArticles}
            onItemPress={this._articleItemPressed}
            dispatch={this.props.dispatch}
            weekNumber={week.number}
          />
        </ScrollView>
      )
    }
    return (
      <View style={styles.noData}>
        <FormattedMessage
          {...messages.noWeekDataText}
          style={styles.noDataText}
          values={{ weekNumber: this.props.weekNumber }}
        />
      </View>
    )
  }

  _articleItemPressed = articleId =>
    this.props.goTo('/knowledge-article', { articleId })

  _onAdPress = ad => {
    if (ad.link) {
      this.props.dispatch(RouterActions.handleLink(ad.link))

      this.props.dispatch(AdActions.markInteraction(ad, 'click'))
    }
  }

  componentDidMount() {
    const { ads } = this.props

    if (ads.length) {
      this.setState({ ads: shuffle(ads).slice(0, 2) })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.isFetching && nextProps.week !== undefined) {
      this.setState({ isFetching: false })
    }

    if (nextProps.ads !== undefined) {
      const prevUids = this.props.ads.map(ad => ad.uid)

      const nextUids = nextProps.ads.map(ad => ad.uid)

      const difference = nextUids.filter(uid => !prevUids.includes(uid))

      if (difference.length) {
        this.setState({ ads: shuffle(nextProps.ads).slice(0, 2) })
      }
    }
  }

  _getBabyArticle = week => {
    const { profile, locale, pregnancy, tabType } = this.props

    const articlePath = `${tabType}Article`

    if (profile && pregnancy && has(week, `${articlePath}.${locale}`)) {
      const userName = profile.name || ''
      const babyName = pregnancy.childName || ''

      const article = week[articlePath][locale]
        .replace('*|NAME|*', userName)
        .replace('*|BABY_NAME|*', babyName)

      return article
    }

    return null
  }

  _getTabImage = week => {
    const { tabType } = this.props

    switch (tabType) {
      case 'baby':
        return week.babyTabImage

      case 'mother':
        return week.motherTabImage

      case 'partner':
        return week.partnerTabImage

      default:
        return null
    }
  }

  _getTabAudio = week => {
    const { tabType } = this.props

    switch (tabType) {
      case 'baby':
        return week.babyAudio

      case 'mother':
        return week.motherAudio

      case 'partner':
        return week.partnerAudio

      default:
        return null
    }
  }

  _getIngressText = week => {
    const { tabType, locale } = this.props

    switch (tabType) {
      case 'baby':
        return week.babyIngressText ? week.babyIngressText[locale] : ''

      case 'mother':
        return week.motherIngressText ? week.motherIngressText[locale] : ''

      case 'partner':
        return week.partnerIngressText ? week.partnerIngressText[locale] : ''

      default:
        return null
    }
  }

  _linkPressed = link => this.props.dispatch(RouterActions.handleLink(link))
}
