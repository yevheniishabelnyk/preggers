/**
 *
 * Pregnancy View TabBar
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { difference } from 'lodash'

import { View } from 'react-native'
import Tab from '@/pregnancy/components/atoms/Tab'

import Pregnancy from '@/app/database/models/Pregnancy'

import { AD_TYPE_WEEK_CARD } from '@/app/redux/ads/constants'

import * as AdActions from '@/app/redux/ads/actions'

import { MIN_PREGNANCY_WEEK, MAX_PREGNANCY_WEEK } from 'config'

import { SINGLE_MOTHER, FATHER } from '@/app/constants/UserTypes'

import { firebaseConnect } from 'react-redux-firebase'
import { getPregnancy } from '@/app/redux/selectors'

import messages from './messages'

import styles, { singleMotherTabWidth } from './styles'

const getPregnancyProgress = Pregnancy.makeGetProgress()

@firebaseConnect(props => {
  const listeners = []

  const pregnancyId = props.navigation.getParam('pregnancyId')

  if (pregnancyId) {
    listeners.push(`pregnancies/${pregnancyId}`)
  }

  return listeners
})
@connect((state, props) => {
  const pregnancyId = props.navigation.getParam('pregnancyId')

  if (pregnancyId) {
    const pregnancy = getPregnancy(state, pregnancyId)

    if (pregnancy) {
      const { dueDate, lengthType } = pregnancy
      const pregnancyProgress = getPregnancyProgress(dueDate, lengthType)

      return {
        pregnancyCurrentWeekNumber: pregnancyProgress.pregnancyWeek,
        pregnancyDueDate: dueDate,
        locale: state.settings.locale,
      }
    }
  }

  return {}
})
export default class TabBar extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
    firebase: PropTypes.object,
    pregnancyCurrentWeekNumber: PropTypes.number,
    screenProps: PropTypes.shape({
      profile: PropTypes.object,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const activeTabIndex = this.props.navigation.state.index
    const userType = this.props.screenProps.profile.type

    const { formatMessage } = this.context.intl

    const isFatherUserType = userType === FATHER

    const tabTitles = [
      formatMessage(messages.statistics),
      formatMessage(messages.baby),
      formatMessage(messages.mother),
      isFatherUserType
        ? formatMessage(messages.father)
        : formatMessage(messages.partner),
    ]

    return (
      <View style={styles.container}>
        <Tab
          title={tabTitles[0]}
          active={activeTabIndex === 0}
          onPress={this._navigateStatistics}
          style={
            userType === SINGLE_MOTHER ? styles.singleMotherTab : styles.tab
          }
          containerWidth={
            userType === SINGLE_MOTHER ? singleMotherTabWidth : null
          }
        />

        <Tab
          title={tabTitles[1]}
          active={activeTabIndex === 1}
          onPress={this._navigateBaby}
          style={
            userType === SINGLE_MOTHER ? styles.singleMotherTab : styles.tab
          }
          containerWidth={
            userType === SINGLE_MOTHER ? singleMotherTabWidth : null
          }
        />

        <Tab
          title={tabTitles[2]}
          active={activeTabIndex === 2}
          onPress={this._navigateMother}
          style={
            userType === SINGLE_MOTHER ? styles.singleMotherTab : styles.tab
          }
          containerWidth={
            userType === SINGLE_MOTHER ? singleMotherTabWidth : null
          }
        />

        {userType !== SINGLE_MOTHER ? (
          <Tab
            title={tabTitles[3]}
            active={activeTabIndex === 3}
            onPress={this._navigatePartner}
            style={styles.tab}
          />
        ) : null}
      </View>
    )
  }

  componentWillMount() {
    const { navigation, pregnancyCurrentWeekNumber } = this.props
    const activeWeekNumber = navigation.getParam('activeWeekNumber')
    const weekNumber = activeWeekNumber || pregnancyCurrentWeekNumber
    if (weekNumber != null) {
      // this._fetchAds(weekNumber)

      this._watchActiveWeeks(weekNumber)
    }

    this.didFocusSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        const { pregnancyCurrentWeekNumber, navigation } = this.props
        const { state } = payload

        console.info(
          'DID_FOCUS pregnancyCurrentWeekNumber: ',
          pregnancyCurrentWeekNumber
        )

        if (pregnancyCurrentWeekNumber != null) {
          if (
            !state.params ||
            state.params.pregnancyCurrentWeekNumber == null ||
            state.params.activeWeekNumber == null ||
            pregnancyCurrentWeekNumber !==
              state.params.pregnancyCurrentWeekNumber
          ) {
            navigation.setParams({
              pregnancyCurrentWeekNumber,
              activeWeekNumber: pregnancyCurrentWeekNumber,
            })
          }
        }
      }
    )

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      () => {
        this._unWatchActiveWeeks()
      }
    )
  }

  componentWillUnmount() {
    this.didFocusSubscription.remove()
    this.willBlurSubscription.remove()
  }

  componentWillReceiveProps(nextProps) {
    const { profile } = nextProps.screenProps

    if (
      this.props.screenProps.profile.pregnancy &&
      !nextProps.screenProps.profile.pregnancy
    ) {
      this.props.navigation.setParams({
        pregnancyId: null,
        pregnancyCurrentWeekNumber: null,
        activeWeekNumber: null,
      })
    } else if (
      profile.pregnancy &&
      this.props.pregnancyCurrentWeekNumber !==
        nextProps.pregnancyCurrentWeekNumber &&
      nextProps.pregnancyCurrentWeekNumber
    ) {
      this.props.navigation.setParams({
        pregnancyCurrentWeekNumber: nextProps.pregnancyCurrentWeekNumber,
        activeWeekNumber: nextProps.pregnancyCurrentWeekNumber,
      })
    }

    if (
      profile.pregnancy &&
      this.props.navigation.getParam('activeWeekNumber') !==
        nextProps.navigation.getParam('activeWeekNumber')
    ) {
      this._watchActiveWeeks(
        nextProps.navigation.getParam('activeWeekNumber'),
        this.props.navigation.getParam('activeWeekNumber')
      )
    }
  }

  _navigateStatistics = () => this._tabPressed(0)
  _navigateBaby = () => this._tabPressed(1)
  _navigateMother = () => this._tabPressed(2)
  _navigatePartner = () => this._tabPressed(3)

  _tabPressed = async tabIndex => {
    const { navigation, pregnancyCurrentWeekNumber } = this.props

    const activeTabIndex = navigation.state.index

    if (tabIndex !== activeTabIndex) {
      const nextRoute = navigation.state.routes[tabIndex]

      navigation.navigate(nextRoute.routeName)

      const activeWeekNumber = navigation.getParam('activeWeekNumber')

      const weekNumber = activeWeekNumber || pregnancyCurrentWeekNumber

      this._fetchAds(weekNumber, tabIndex)
    }
  }

  _watchActiveWeeks = (nextActive, currentActive) => {
    console.log('_watchActiveWeeks')

    const { firebase } = this.props

    const currentActiveWeeks = []
    const nextActiveWeeks = [nextActive]

    if (currentActive != null) {
      currentActiveWeeks.push(currentActive)
      if (currentActive > MIN_PREGNANCY_WEEK) {
        currentActiveWeeks.push(currentActive - 1)
      }
      if (currentActive < MAX_PREGNANCY_WEEK) {
        currentActiveWeeks.push(currentActive + 1)
      }
    }

    if (nextActive > MIN_PREGNANCY_WEEK) {
      nextActiveWeeks.push(nextActive - 1)
    }
    if (nextActive < MAX_PREGNANCY_WEEK) {
      nextActiveWeeks.push(nextActive + 1)
    }

    // console.info('currentActiveWeeks: ', currentActiveWeeks)
    // console.info('nextActiveWeeks: ', nextActiveWeeks)

    const newWatchedWeeks = difference(nextActiveWeeks, currentActiveWeeks)

    // console.info('newWatchedWeeks: ', newWatchedWeeks)

    newWatchedWeeks.forEach(weekNumber =>
      firebase.watchEvent('value', `content/weeks/${weekNumber}`)
    )

    newWatchedWeeks.forEach(weekNumber => this._fetchAds(weekNumber))

    if (currentActive != null) {
      const unWatchedWeeks = difference(currentActiveWeeks, nextActiveWeeks)

      // console.info('unWatchedWeeks: ', unWatchedWeeks)

      unWatchedWeeks.forEach(weekNumber =>
        firebase.unWatchEvent('value', `content/weeks/${weekNumber}`)
      )
    }
  }

  _unWatchActiveWeeks = () => {
    const activeWeekNumber = this.props.navigation.getParam('activeWeekNumber')

    if (activeWeekNumber != null) {
      const { firebase } = this.props

      firebase.unWatchEvent('value', `content/weeks/${activeWeekNumber}`)

      if (activeWeekNumber > MIN_PREGNANCY_WEEK) {
        firebase.unWatchEvent('value', `content/weeks/${activeWeekNumber - 1}`)
      }

      if (activeWeekNumber < MAX_PREGNANCY_WEEK) {
        firebase.unWatchEvent('value', `content/weeks/${activeWeekNumber + 1}`)
      }
    }
  }

  _fetchAds = async (weekNumber, tabIndex = false) => {
    const { navigation } = this.props

    const { profile } = this.props.screenProps

    const activeTabIndex = tabIndex || navigation.state.index

    const tabTypeIndexMap = [null, 'baby', 'mother', 'partner']

    const weekTabType = tabTypeIndexMap[activeTabIndex]

    if (weekTabType) {
      const { deviceCountryCode, pregnancyDueDate, locale } = this.props

      const requestBody = {
        types: [AD_TYPE_WEEK_CARD],
        email: profile.email,
        countryCode: profile.location
          ? profile.location.countryCode
          : deviceCountryCode,
        locale,
        userType: profile.type,
        pregnancies: [pregnancyDueDate],
        weekNumber,
        weekTabType,
      }

      try {
        await navigation.dispatch(
          AdActions.fetchAdsAndReplaceTypeState(requestBody)
        )
      } catch (error) {
        console.log(error)
      }
    }
  }
}
