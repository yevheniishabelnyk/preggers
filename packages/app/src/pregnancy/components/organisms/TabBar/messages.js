import { defineMessages } from 'react-intl'

export default defineMessages({
  statistics: 'Statistics',
  baby: 'Baby',
  mother: 'Mother',
  partner: 'Partner',
  father: 'Father',
})
