import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(12, 5)
export const singleMotherTabWidth = moderateScale(109, 0.7)

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: gutter,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  tab: {
    width: moderateScale(80, 0.7),
  },

  singleMotherTab: {
    width: moderateScale(109, 0.7),
  },
})

export default styles
