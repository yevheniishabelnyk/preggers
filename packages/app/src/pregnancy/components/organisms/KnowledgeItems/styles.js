import { StyleSheet, Platform } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    marginBottom: moderateScale(34),
    shadowOpacity: 0.1,
    shadowRadius: 8,
    shadowColor: '#000',
    shadowOffset: { height: 6, width: -2 },
    alignItems: Platform.OS === 'android' ? 'center' : null,
  },

  title: {
    width: Platform.OS === 'android' ? moderateScale(340) : null,
    textAlign: Platform.OS === 'android' ? 'left' : null,
    alignSelf: Platform.OS === 'android' ? 'center' : null,
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(26),
    color: 'rgb(41, 53, 69)',
    marginBottom: moderateScale(16),
  },

  viewAllButton: {
    marginTop:
      Platform.OS === 'android' ? moderateScale(5, 4) : moderateScale(2, 4),
    width: moderateScale(182),
    alignSelf: 'center',
  },
})

export default styles
