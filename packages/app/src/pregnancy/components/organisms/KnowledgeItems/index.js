/**
 *
 * KnowledgeItems
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import KnowledgeItem from '@/pregnancy/components/molecules/KnowledgeItem'
import WeekButton from '@/pregnancy/components/atoms/WeekButton'

import * as RouterActions from '@/app/redux/router/actions'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class KnowledgeItems extends BaseScreen {
  static propTypes = {
    articles: PropTypes.array,
    onItemPress: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    articles: [],
  }

  render() {
    const { articles } = this.props

    if (!articles.length) {
      return null
    }

    const { onItemPress } = this.props
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <FormattedMessage {...messages.title} style={styles.title} />

        {articles.slice(0, 3).map(article => (
          <KnowledgeItem
            onPress={onItemPress}
            key={article.id}
            item={article}
          />
        ))}

        <WeekButton
          style={styles.viewAllButton}
          title={formatMessage(messages.viewAllButtonTitle)}
          onPress={this._viewAllButtonPressed}
        />
      </View>
    )
  }

  _viewAllButtonPressed = () => {
    const { weekNumber } = this.props

    this.props.dispatch(
      RouterActions.goToComplexUrl(
        `/~knowledge/my-week?weekNumber=${weekNumber}`
      )
    )
  }
}
