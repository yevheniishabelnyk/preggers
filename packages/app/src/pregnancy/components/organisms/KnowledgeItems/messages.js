import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Articles',
  viewAllButtonTitle: 'View all',
})
