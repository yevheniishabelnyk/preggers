/**
 *
 * PregnancyProgressBaby
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Text, Animated, Easing } from 'react-native'
import CircleProgressBar from '@/pregnancy/components/molecules/CircleProgressBar'
import { ExpoImage } from '@/vendor/cache-manager'

import Pregnancy from '@/app/database/models/Pregnancy'

import { pick, omit, round } from 'lodash'
import memoize from 'fast-memoize'

import { PREGNANCY__DEFAULT_BABY_NAME } from '@/app/constants/BabyNames'

import { firebaseConnect } from 'react-redux-firebase'
import applyForwardedRef from '@/shared/decorators/applyForwardedRef'
import { getWeek } from '@/app/redux/selectors'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { getColors } from './colors'

import { scalableStyles } from '@/app/scaling'
import rawStyles, {
  defaultWidth,
  defaultHeight,
  aspectRatio,
  progressMarkWidth,
} from './styles'

const ANIMATED_VALUES = ['portion', 'pregnancyWeek']

const getPregnancyProgress = Pregnancy.makeGetProgress()

@firebaseConnect(props => {
  const { pregnancy } = props

  if (pregnancy) {
    const { pregnancyWeek } = getPregnancyProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    return [`content/weeks/${pregnancyWeek}`]
  }
})
@connect((state, props) => {
  const { pregnancy } = props

  let pregnancyWeekData

  if (pregnancy) {
    const { pregnancyWeek } = getPregnancyProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    pregnancyWeekData = getWeek(state, pregnancyWeek)
  }

  return {
    pregnancyWeekData,
    lengthUnits: state.settings.lengthUnits,
    weightUnits: state.settings.weightUnits,
  }
})
@applyForwardedRef
@scalableStyles(rawStyles)
export default class PregnancyProgressBaby extends React.Component {
  static propTypes = {
    scale: PropTypes.number,
    pregnancy: PropTypes.object,
    animate: PropTypes.bool,
    isVisible: PropTypes.bool,
    lengthUnits: PropTypes.string,
    weightUnits: PropTypes.string,
    backgroundColor: PropTypes.string,
    theme: PropTypes.string,
    pregnancyWeekData: PropTypes.shape({
      babyLength: PropTypes.string,
      babyWeight: PropTypes.string,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static aspectRatio = aspectRatio
  static defaultWidth = defaultWidth
  static defaultHeight = defaultHeight

  state = {
    dueDate: '',
    passedDays: 0,
    pregnancyWeek: 1,
    portion: 0,
    isOverDue: false,
    ...getColors(this.props.theme, this.props.backgroundColor),
  }

  constructor(props) {
    super(props)

    this.isRendered = false
    this.hasRenderingError = false

    this.data = this.state

    if (props.animate) {
      this.animatedValue = new Animated.Value(0)
    }
  }

  render() {
    const {
      scale,
      pregnancy,
      pregnancyWeekData,
      weightUnits,
      animate,
    } = this.props

    if (!pregnancy) {
      return null
    }

    const {
      dueDate,
      portion,
      pregnancyWeek,
      isOverDue,
      backgroundColor,
      borderColor,
      textColor,
    } = this.state

    let childName

    if (isOverDue) {
      const { formatMessage } = this.context.intl

      childName =
        pregnancy.childName === PREGNANCY__DEFAULT_BABY_NAME
          ? formatMessage(messages.defaultChildName)
          : pregnancy.childName
    }

    const babyImage = do {
      if (pregnancyWeekData) {
        if (animate && pregnancyWeekData.babyTabImage) {
          pregnancyWeekData.babyTabImage
        } else if (pregnancyWeekData.babyTabShareImage) {
          pregnancyWeekData.babyTabShareImage
        }
      }
    }

    const babyLength =
      pregnancyWeekData && pregnancyWeekData.babyLength
        ? pregnancyWeekData.babyLength
        : ''

    const babyWeight =
      pregnancyWeekData && pregnancyWeekData.babyWeight
        ? pregnancyWeekData.babyWeight
        : ''

    const borderStyle = { borderColor }
    const textStyle = { color: textColor }

    this.styles.scale = scale

    return (
      <View style={[this.styles.container, { backgroundColor }]}>
        <CircleProgressBar
          portion={portion}
          scale={this.styles.scale}
          circlePartsNumber={3}
          progressMarkElementWidth={progressMarkWidth}
          progressMarkElement={
            <View style={this.styles.progressMark}>
              <FormattedMessage
                style={this.styles.progressMarkTitle}
                {...messages.progressMarkTitle}
              />

              <Text style={this.styles.progressMarkMessage}>
                {pregnancyWeek}
              </Text>
            </View>
          }
        >
          <View style={this.styles.progressBarContent}>
            {babyImage ? (
              <ExpoImage
                style={this.styles.progressBarImage}
                source={babyImage}
                resizeMode="contain"
                cacheImage
                onLoad={this._imageLoaded}
                onError={this._imageLoadedWithError}
              />
            ) : null}
          </View>
        </CircleProgressBar>

        <Text
          style={[
            this.styles.lengthAndWeightMessage,
            weightUnits !== 'g' ? this.styles.weightPoundsUnits : null,
            textStyle,
          ]}
        >
          {babyLength ? (
            <React.Fragment>
              <FormattedMessage {...messages.lengthMessage} />:{' '}
              <Text style={this.styles.lengthAndWeightValues}>
                {this._convertLengthUnits(babyLength)}
              </Text>{' '}
            </React.Fragment>
          ) : null}

          {babyWeight ? (
            <React.Fragment>
              <FormattedMessage {...messages.weightMessage} />:{' '}
              <Text style={this.styles.lengthAndWeightValues}>
                {this._convertWeightUnits(babyWeight)}
              </Text>
            </React.Fragment>
          ) : null}
        </Text>

        <View style={[this.styles.info, borderStyle]}>
          {isOverDue ? (
            <View style={this.styles.overDueMessageContainer}>
              <FormattedMessage
                {...messages.overDueMessage}
                style={[this.styles.overDueMessage, textStyle]}
                values={{ childName }}
              />
            </View>
          ) : (
            <React.Fragment>
              <FormattedMessage
                style={[this.styles.dueDateMessage, textStyle]}
                {...messages.dueDateMessage}
              />

              <Text style={[this.styles.dueDate, textStyle]}>{dueDate}</Text>
            </React.Fragment>
          )}
        </View>
      </View>
    )
  }

  componentWillMount() {
    if (this.animatedValue) {
      this.animatedValueListener = this.animatedValue.addListener(({ value }) =>
        this.setState({
          portion: this.data.portion * value,
          pregnancyWeek: Math.floor(this.data.pregnancyWeek * value) || 1,
        })
      )
    }

    const { pregnancy } = this.props

    if (pregnancy) {
      this._updateData(pregnancy, 400)
    }
  }

  componentWillUnmount() {
    if (this.animatedValue) {
      this.animatedValue.removeListener(this.animatedValueListener)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.pregnancy && nextProps.pregnancy) ||
      (this.props.pregnancy &&
        nextProps.pregnancy &&
        (this.props.pregnancy.dueDate !== nextProps.pregnancy.dueDate ||
          this.props.pregnancy.lengthType !== nextProps.pregnancy.lengthType))
    ) {
      this._updateData(nextProps.pregnancy, 150)
    }

    if (this.props.backgroundColor !== nextProps.backgroundColor) {
      const nextColors = getColors(nextProps.backgroundColor)

      console.info('nextColors: ', nextColors)

      this.setState(nextColors)
    }

    if (
      this.hasPendingAnimation &&
      !this.props.isVisible &&
      nextProps.isVisible
    ) {
      setTimeout(() => this._animate(), 300)
    }
  }

  _getData = pregnancy => {
    const pregnancyProgress = Pregnancy.getProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    const isOverDue = pregnancyProgress.leftDays < 0

    let portion = pregnancyProgress.passedDays / Pregnancy.LENGTH

    if (portion > 1) {
      portion = 1
    }

    return {
      dueDate: pregnancy.dueDate,
      leftDays: pregnancyProgress.leftDays,
      pregnancyWeek: pregnancyProgress.pregnancyWeek,
      passedWeeks: pregnancyProgress.passedWeeks,
      passedWeeksDays: pregnancyProgress.passedWeeksDays,
      portion,
      isOverDue,
    }
  }

  _updateData = (pregnancy, animationDelay = 0) => {
    const newData = this._getData(pregnancy)

    if (this.animatedValue) {
      const newAnimatedValue = this.data.portion / newData.portion

      this.data = newData

      this.setState(omit(this.data, ANIMATED_VALUES), () => {
        this.animatedValue.setValue(newAnimatedValue)

        if (this.props.isVisible) {
          setTimeout(() => this._animate(), animationDelay)
        } else {
          this.hasPendingAnimation = true
        }
      })
    } else {
      this.data = newData

      this.setState(this.data)
    }
  }

  _animate = () => {
    this.hasPendingAnimation = false

    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 1000,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start(() => this.setState(pick(this.data, ANIMATED_VALUES)))
  }

  _convertLengthUnits = value =>
    this._memoizedConvertLengthUnits(value, this.props.lengthUnits)

  _memoizedConvertLengthUnits = memoize((value, lengthUnits) => {
    let newValue

    if (lengthUnits === 'in') {
      newValue = round(value / 2.54, 1)
    } else {
      newValue = value
    }

    return lengthUnits ? `${newValue} ${lengthUnits.toLowerCase()}` : ''
  })

  _convertWeightUnits = value =>
    this._memoizedConvertWeightUnits(value, this.props.weightUnits)

  _memoizedConvertWeightUnits = memoize((value, weightUnits) => {
    let newValue

    if (weightUnits === 'g') {
      newValue = `${value} g`
    } else {
      const nearExact = value / 1000 / 0.45359237
      const lbs = Math.floor(nearExact)
      const oz = round((nearExact - lbs) * 16, 2)

      newValue = `${lbs} lb ${oz} oz`
    }

    return newValue
  })

  _imageLoaded = () => {
    this.isRendered = true
    this.hasRenderingError = false
  }

  _imageLoadedWithError = () => {
    this.isRendered = true
    this.hasRenderingError = true
  }

  waitFor = () => {
    if (this.isRendered) {
      if (this.hasRenderingError) {
        return Promise.reject()
      }

      return Promise.resolve()
    }

    return this._listenRendering().then(() => {
      if (this.hasRenderingError) {
        return Promise.reject()
      }
    })
  }

  _listenRendering = () =>
    new Promise(resolve => {
      const interval = setInterval(() => {
        if (this.isRendered) {
          clearInterval(interval)

          resolve()
        }
      }, 20)
    })
}
