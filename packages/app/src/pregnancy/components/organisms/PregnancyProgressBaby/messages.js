import { defineMessages } from 'react-intl'

export default defineMessages({
  dueDateMessage: 'MY ESTIMATED DUE DATE IS',
  lengthMessage: 'Length',
  weightMessage: 'Weight',
  progressMarkTitle: 'Week',
  overDueMessage: '{childName} will come any day now!',
  defaultChildName: 'My baby',
})
