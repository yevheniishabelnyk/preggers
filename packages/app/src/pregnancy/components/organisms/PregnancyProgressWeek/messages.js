import { defineMessages } from 'react-intl'

export default defineMessages({
  progressBarSubTitleTop: 'IM IN WEEK',
  maxMessage: 'Max',
  daysLeft: 'days left',
  dueDateMessage: 'MY ESTIMATED DUE DATE IS',
  overDueMessage: '{childName} will come any day now!',
  defaultChildName: 'My baby',
})
