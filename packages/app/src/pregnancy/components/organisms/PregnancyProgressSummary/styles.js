import { moderateScale } from '@/app/scaling'
import { defaults as theme } from './colors'

export const aspectRatio = 304 / 368
export const defaultWidth = moderateScale(304)
export const defaultHeight = moderateScale(368)

export default {
  container: {
    alignItems: 'center',
    width: defaultWidth,
    height: defaultHeight,
    paddingHorizontal: moderateScale(21),
    overflow: 'hidden',
    backgroundColor: theme.backgroundColor,
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(23),
    lineHeight: moderateScale(33),
    height: moderateScale(33),
    color: theme.darkTextColor,
    letterSpacing: 0,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: moderateScale(10),
  },

  subTitle: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(17),
    lineHeight: moderateScale(22),
    height: moderateScale(22),
    color: theme.lightTextColor,
    marginBottom: moderateScale(6),
  },

  days: {
    fontFamily: 'Now-Bold',
    color: theme.darkTextColor,
  },

  table: {
    marginTop: moderateScale(8),
  },
}
