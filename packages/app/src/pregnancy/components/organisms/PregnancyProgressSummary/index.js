/**
 *
 * PregnancyProgressSummary
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Animated, Easing } from 'react-native'
import GradientBar from '@/pregnancy/components/atoms/GradientBar'
import SummaryTable from '@/pregnancy/components/molecules/SummaryTable'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import Pregnancy from '@/app/database/models/Pregnancy'

import { getColors } from './colors'

import { scalableStyles } from '@/app/scaling'
import rawStyles, { defaultWidth, defaultHeight, aspectRatio } from './styles'

@scalableStyles(rawStyles)
export default class PregnancyProgressSummary extends React.Component {
  static propTypes = {
    pregnancy: PropTypes.object,
    isVisible: PropTypes.bool,
    animate: PropTypes.bool,
    scale: PropTypes.number,
    backgroundColor: PropTypes.string,
    theme: PropTypes.string,
    style: PropTypes.any,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    isVisible: true,
  }

  static aspectRatio = aspectRatio
  static defaultWidth = defaultWidth
  static defaultHeight = defaultHeight

  constructor(props) {
    super(props)

    this.animatedValue = new Animated.Value(0)

    const initialState = {
      pregnancyDueDateDate: props.pregnancy ? props.pregnancy.dueDate : '',
      passedDays: 0,
      leftDays: 280,
      percent: 0,
      pregnancyWeek: 0,
      pregnancyFullWeeks: 0,
      pregnancyFullWeeksPlusDays: '0 + 0',
      calendarMonth: 1,
      pregnancyMonth: 1,
      trimester: 1,
      ...getColors(props.theme, props.backgroundColor),
    }

    this.state = initialState
  }

  render() {
    const { scale } = this.props
    const {
      pregnancyDueDateDate,
      passedDays,
      leftDays,
      percent,
      pregnancyWeek,
      pregnancyFullWeeksPlusDays,
      trimester,
      pregnancyMonth,
      calendarMonth,
      lightTextColor,
      darkTextColor,
      backgroundColor,
    } = this.state

    const { formatMessage } = this.context.intl

    const lightTextStyle = { color: lightTextColor }
    const darkTextStyle = { color: darkTextColor }

    this.styles.scale = scale

    return (
      <View
        style={[this.styles.container, this.props.style, { backgroundColor }]}
      >
        <FormattedMessage
          style={[this.styles.title, darkTextStyle]}
          {...messages.summaryTitle}
        />

        <Text style={[this.styles.subTitle, lightTextStyle]}>
          <Text style={[this.styles.days, darkTextStyle]}>{passedDays} </Text>

          <FormattedMessage {...messages.ofText} />

          <Text style={[this.styles.days, darkTextStyle]}> 280 </Text>

          <FormattedMessage {...messages.days} />
        </Text>

        <GradientBar scale={this.styles.scale} progress={percent / 100} />

        <SummaryTable
          style={this.styles.table}
          scale={this.styles.scale}
          lightTextColor={lightTextColor}
          darkTextColor={darkTextColor}
          data={[
            {
              title: formatMessage(messages.dueDateRowTitle),
              value: pregnancyDueDateDate,
            },
            {
              title: formatMessage(messages.pregnancyWeekRowTitle),
              value: pregnancyWeek,
            },
            {
              title: formatMessage(messages.beenPregnantRowTitle),
              value:
                pregnancyFullWeeksPlusDays + ' ' + formatMessage(messages.days),
            },
            {
              title: formatMessage(messages.calendarMonthRowTitle),
              value: calendarMonth,
            },
            {
              title: formatMessage(messages.pregnancyMonthRowTitle),
              value: pregnancyMonth,
            },
            {
              title: formatMessage(messages.trimesterRowTitle),
              value: trimester,
            },
            {
              title: formatMessage(messages.daysLeftRowTitle),
              value: 280 - leftDays < 0 ? 0 : 280 - leftDays,
            },
          ]}
        />
      </View>
    )
  }

  componentDidMount() {
    if (this.props.pregnancy) {
      this.calculateInitialData(
        this.props.pregnancy.dueDate,
        this.props.pregnancy.lengthType
      )
    }

    this.animatedValueListener = this.animatedValue.addListener(({ value }) => {
      this.setState(this.calculateData(value))
    })
  }

  componentWillUnmount() {
    this.animatedValue.removeListener(this.animatedValueListener)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.backgroundColor !== nextProps.backgroundColor) {
      const nextColors = getColors(nextProps.backgroundColor)

      console.info('nextColors: ', nextColors)

      this.setState(nextColors)
    }

    if (
      (!this.props.pregnancy && nextProps.pregnancy) ||
      (this.props.pregnancy &&
        nextProps.pregnancy &&
        (this.props.pregnancy.dueDate !== nextProps.pregnancy.dueDate ||
          this.props.pregnancy.lengthType !== nextProps.pregnancy.lengthType))
    ) {
      this.calculateInitialData(
        nextProps.pregnancy.dueDate,
        nextProps.pregnancy.lengthType
      )

      this.setState(this.getInitialData())
    }

    if (this.props.isVisible !== nextProps.isVisible) {
      setTimeout(() => {
        if (nextProps.isVisible && this.state.passedDays === 0) {
          if (this.props.animate) {
            this.animate()
          } else {
            this.setState(this.getInitialData())
          }
        }
      }, 300)
    }
  }

  calculateInitialData = (pregnancyDueDate, pregnancyLengthType) => {
    const pregnancyData = Pregnancy.getProgress(
      pregnancyDueDate,
      pregnancyLengthType,
      1,
      true
    )

    this.pregnancyDueDate = pregnancyDueDate
    this.pregnancyLengthType = pregnancyLengthType

    this.passedDays = pregnancyData.passedDays

    this.leftDays = pregnancyData.passedDays

    this.percent =
      Math.round((pregnancyData.passedDays / 280) * 1000) / 10 > 100
        ? 100
        : Math.round((pregnancyData.passedDays / 280) * 1000) / 10

    this.pregnancyWeek = pregnancyData.pregnancyWeek

    this.pregnancyFullWeeks = pregnancyData.passedWeeks

    this.pregnancyFullWeeksDays = pregnancyData.passedWeeksDays

    this.pregnancyFullWeeksPlusDays = `${pregnancyData.passedWeeks} + ${
      pregnancyData.passedWeeksDays
    }`

    this.pregnancyTrimester = pregnancyData.trimester

    this.calendarMonth = pregnancyData.calendarMonth
    this.pregnancyMonth = pregnancyData.pregnancyMonth

    if (this.props.isVisible) {
      if (this.props.animate) {
        setTimeout(() => this.animate(), 0)
      } else {
        this.setState(this.getInitialData())
      }
    }
  }

  calculateData = p => {
    const pregnancyData = Pregnancy.getProgress(
      this.pregnancyDueDate,
      this.pregnancyLengthType,
      p,
      true
    )

    const passedDays = Math.floor(this.passedDays * p)
    const leftDays = passedDays

    const percent =
      Math.floor((passedDays / 280) * 100) > 100
        ? 100
        : Math.floor((passedDays / 280) * 100)

    const pregnancyFullWeeksPlusDays = `${pregnancyData.passedWeeks} + ${
      pregnancyData.passedWeeksDays
    }`

    return {
      leftDays,
      passedDays: pregnancyData.passedDays,
      percent,
      pregnancyWeek: pregnancyData.pregnancyWeek,
      pregnancyFullWeeks: pregnancyData.passedWeeks,
      pregnancyFullWeeksPlusDays,
      calendarMonth: pregnancyData.calendarMonth,
      pregnancyMonth: pregnancyData.pregnancyMonth,
      trimester: pregnancyData.trimester,
    }
  }

  getInitialData = () => ({
    leftDays: this.leftDays,
    passedDays: this.passedDays,
    percent: this.percent,
    calendarMonth: this.calendarMonth,
    pregnancyMonth: this.pregnancyMonth,
    pregnancyWeek: this.pregnancyWeek,
    pregnancyFullWeeks: this.pregnancyFullWeeks,
    pregnancyFullWeeksPlusDays: this.pregnancyFullWeeksPlusDays,
    trimester: this.pregnancyTrimester,
  })

  animate = () =>
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 1000,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start(() => this.setState(this.getInitialData()))
}
