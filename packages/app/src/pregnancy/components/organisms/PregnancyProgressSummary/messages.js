import { defineMessages } from 'react-intl'

export default defineMessages({
  summaryTitle: 'Summary',
  days: 'days',
  dueDateRowTitle: 'DUE DATE:',
  pregnancyWeekRowTitle: 'PREGNANT WEEK:',
  beenPregnantRowTitle: 'BEEN PREGNANT:',
  calendarMonthRowTitle: 'CALENDAR MONTH:',
  pregnancyMonthRowTitle: 'PREGNANCY MONTH:',
  trimesterRowTitle: 'TRIMESTER:',
  daysLeftRowTitle: 'DAYS LEFT:',
  ofText: 'of',
})
