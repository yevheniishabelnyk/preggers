/**
 *
 * colors
 *
 */

import chroma from 'chroma-js'
import predefinedColors from './backgroundColors'

const predefinedColorsList = predefinedColors.list || {}

const defaultBackgroundColor = predefinedColors.default
const defaultColorSchema = predefinedColorsList[defaultBackgroundColor] || {}

export const defaults = {
  backgroundColor: defaultBackgroundColor,
  ...defaultColorSchema,
}

export function getColors(themeInput, backgroundColor) {
  const theme = themeInput || backgroundColor || predefinedColors.default

  if (typeof predefinedColorsList[theme] === 'object') {
    if (backgroundColor) {
      return {
        ...predefinedColorsList[theme],
        backgroundColor,
      }
    }

    return predefinedColorsList[theme]
  }

  let lightTextColor, darkTextColor

  const hasHighContrast = chroma.contrast(theme, 'white') > 3.4

  if (hasHighContrast) {
    lightTextColor = '#ffffff'

    darkTextColor = '#ffffff'
  } else {
    lightTextColor = chroma(theme)
      .darken(1.5)
      .hex()

    darkTextColor = chroma(lightTextColor)
      .darken(1.5)
      .hex()
  }

  if (hasHighContrast) {
    return {
      lightTextColor: darkTextColor,
      darkTextColor: lightTextColor,
      backgroundColor,
    }
  }

  return {
    lightTextColor,
    darkTextColor,
    backgroundColor,
  }
}
