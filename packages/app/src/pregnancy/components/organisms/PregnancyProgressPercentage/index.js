/**
 *
 * PregnancyProgressPercentage
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Animated, Easing } from 'react-native'
import CircleProgressBar from '@/pregnancy/components/molecules/CircleProgressBar'

import Pregnancy from '@/app/database/models/Pregnancy'

import { pick, omit } from 'lodash'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { getColors } from './colors'

import { scalableStyles } from '@/app/scaling'
import rawStyles, {
  defaultWidth,
  defaultHeight,
  aspectRatio,
  progressMarkWidth,
} from './styles'

const ANIMATED_VALUES = ['portion', 'pregnancyWeek']

@scalableStyles(rawStyles)
export default class PregnancyProgressPercentage extends React.Component {
  static propTypes = {
    scale: PropTypes.number,
    pregnancy: PropTypes.object,
    animate: PropTypes.bool,
    isVisible: PropTypes.bool,
    backgroundColor: PropTypes.string,
    theme: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static aspectRatio = aspectRatio
  static defaultWidth = defaultWidth
  static defaultHeight = defaultHeight

  state = {
    dueDate: '',
    passedDays: 0,
    pregnancyWeek: 1,
    portion: 0,
    percent: 0,
    ...getColors(this.props.theme, this.props.backgroundColor),
  }

  constructor(props) {
    super(props)

    this.data = this.state

    if (props.animate) {
      this.animatedValue = new Animated.Value(0)
    }
  }

  render() {
    const { scale, pregnancy } = this.props

    if (!pregnancy) {
      return null
    }

    const {
      dueDate,
      passedDays,
      portion,
      percent,
      pregnancyWeek,
      borderColor,
      textColor,
      backgroundColor,
    } = this.state

    const borderStyle = { borderColor }
    const textStyle = { color: textColor }

    this.styles.scale = scale

    return (
      <View style={[this.styles.container, { backgroundColor }]}>
        <CircleProgressBar
          portion={portion}
          scale={this.styles.scale}
          circlePartsNumber={3}
          progressMarkElementWidth={progressMarkWidth}
          progressMarkElement={
            <View style={this.styles.progressMark}>
              <FormattedMessage
                style={this.styles.progressMarkTitle}
                {...messages.progressMarkTitle}
              />

              <Text style={this.styles.progressMarkMessage}>
                {pregnancyWeek}
              </Text>
            </View>
          }
        >
          <View style={this.styles.progressBarContent}>
            <FormattedMessage
              style={[this.styles.progressBarSubTitle, textStyle]}
              {...messages.progressBarSubTitleTop}
            />

            <View style={this.styles.progressBarTitleBlock}>
              <Text
                style={[
                  this.styles.progressBarTitle,
                  percent >= 100 ? this.styles.progressBarTitleLong : null,
                  textStyle,
                ]}
              >
                {percent}
              </Text>

              <Text
                style={[
                  this.styles.progressBarPercent,
                  percent >= 100 ? this.styles.progressBarPercentTop : null,
                  textStyle,
                ]}
              >
                %
              </Text>
            </View>

            <FormattedMessage
              style={[this.styles.progressBarSubTitle, textStyle]}
              {...messages.progressBarSubTitleBottom}
            />
          </View>
        </CircleProgressBar>

        <Text style={[this.styles.dayMessage, textStyle]}>
          <FormattedMessage {...messages.dayMessage} />{' '}
          <Text style={this.styles.days}>{passedDays}</Text>{' '}
          <FormattedMessage {...messages.ofMessage} />{' '}
          <Text style={this.styles.days}>{Pregnancy.LENGTH}</Text>
        </Text>

        <View style={[this.styles.info, borderStyle]}>
          <FormattedMessage
            style={[this.styles.dueDateMessage, textStyle]}
            {...messages.dueDateMessage}
          />

          <Text style={[this.styles.dueDate, textStyle]}>{dueDate}</Text>
        </View>
      </View>
    )
  }

  componentWillMount() {
    if (this.animatedValue) {
      this.animatedValueListener = this.animatedValue.addListener(
        ({ value }) => {
          this.setState({
            portion: this.data.portion * value,
            pregnancyWeek: Math.floor(this.data.pregnancyWeek * value) || 1,
          })
        }
      )
    }

    const { pregnancy } = this.props

    if (pregnancy) {
      this._updateData(pregnancy, 400)
    }
  }

  componentWillUnmount() {
    if (this.animatedValue) {
      this.animatedValue.removeListener(this.animatedValueListener)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.pregnancy && nextProps.pregnancy) ||
      (this.props.pregnancy &&
        nextProps.pregnancy &&
        (this.props.pregnancy.dueDate !== nextProps.pregnancy.dueDate ||
          this.props.pregnancy.lengthType !== nextProps.pregnancy.lengthType))
    ) {
      this._updateData(nextProps.pregnancy, 150)
    }

    if (this.props.backgroundColor !== nextProps.backgroundColor) {
      const nextColors = getColors(nextProps.backgroundColor)

      console.info('nextColors: ', nextColors)

      this.setState(nextColors)
    }

    if (
      this.hasPendingAnimation &&
      !this.props.isVisible &&
      nextProps.isVisible
    ) {
      setTimeout(() => this._animate(), 300)
    }
  }

  _getData = pregnancy => {
    const pregnancyProgress = Pregnancy.getProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    let portion = pregnancyProgress.passedDays / Pregnancy.LENGTH

    if (portion > 1) {
      portion = 1
    }

    return {
      dueDate: pregnancy.dueDate,
      passedDays: pregnancyProgress.passedDays,
      percent: Math.floor(portion * 100),
      pregnancyWeek: pregnancyProgress.pregnancyWeek,
      portion: portion,
    }
  }

  _updateData = (pregnancy, animationDelay = 0) => {
    const newData = this._getData(pregnancy)

    if (this.animatedValue) {
      const newAnimatedValue = this.data.portion / newData.portion

      this.data = newData

      this.setState(omit(this.data, ANIMATED_VALUES), () => {
        this.animatedValue.setValue(newAnimatedValue)

        if (this.props.isVisible) {
          setTimeout(() => this._animate(), animationDelay)
        } else {
          this.hasPendingAnimation = true
        }
      })
    } else {
      this.data = newData

      this.setState(this.data)
    }
  }

  _animate = () => {
    this.hasPendingAnimation = false

    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 1000,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start(() => this.setState(pick(this.data, ANIMATED_VALUES)))
  }
}
