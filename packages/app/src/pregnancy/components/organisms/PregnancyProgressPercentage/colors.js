/**
 *
 * colors
 *
 */

import chroma from 'chroma-js'
import predefinedColors from './backgroundColors'

const predefinedColorsList = predefinedColors.list || {}

const defaultBackgroundColor = predefinedColors.default
const defaultColorSchema = predefinedColorsList[defaultBackgroundColor] || {}

export const defaults = {
  backgroundColor: defaultBackgroundColor,
  ...defaultColorSchema,
}

export function getColors(themeInput, backgroundColor) {
  const theme = themeInput || backgroundColor || predefinedColors.default

  if (typeof predefinedColorsList[theme] === 'object') {
    if (backgroundColor) {
      return {
        ...predefinedColorsList[theme],
        backgroundColor,
      }
    }

    return predefinedColorsList[theme]
  }

  let borderColor, textColor

  const hasHighContrast = chroma.contrast(theme, 'white') > 3.4

  if (hasHighContrast) {
    borderColor = '#ffffff'

    textColor = '#ffffff'
  } else {
    borderColor = chroma(theme)
      .darken(1.5)
      .hex()

    textColor = chroma(borderColor)
      .darken(1.5)
      .hex()
  }

  if (hasHighContrast) {
    return {
      borderColor: textColor,
      textColor: borderColor,
      backgroundColor,
    }
  }

  return { borderColor, textColor, backgroundColor }
}
