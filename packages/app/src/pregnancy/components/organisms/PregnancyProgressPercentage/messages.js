import { defineMessages } from 'react-intl'

export default defineMessages({
  progressBarSubTitleTop: 'I AM',
  progressBarSubTitleBottom: 'DONE',
  dayMessage: 'Day',
  ofMessage: 'of',
  dueDateMessage: 'MY ESTIMATED DUE DATE IS',
  progressMarkTitle: 'Week',
})
