import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'
import { defaults as theme } from './colors'

export const aspectRatio = 262 / 366
export const defaultWidth = moderateScale(262)
export const defaultHeight = moderateScale(366)
export const progressMarkWidth = moderateScale(41)

export default {
  container: {
    width: defaultWidth,
    height: defaultHeight,
    backgroundColor: theme.backgroundColor,
    overflow: 'hidden',
    alignItems: 'center',
    paddingTop: moderateScale(20),
  },

  progressBarContent: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
  },

  progressMark: {
    backgroundColor: '#42e0be',
    width: progressMarkWidth,
    height: progressMarkWidth,
    borderRadius: progressMarkWidth / 2,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowColor: 'black',
    shadowOffset: { height: 3, width: -2 },
  },

  progressMarkTitle: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: moderateScale(8),
    textAlign: 'center',
    color: 'white',
    marginTop: moderateScale(5),
  },

  progressMarkMessage: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: moderateScale(15),
    marginTop: moderateScale(-5),
    textAlign: 'center',
    color: 'white',
  },

  progressBarSubTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    textAlign: 'center',
    color: theme.textColor,
  },

  progressBarTitleBlock: {
    flexDirection: 'row',
    height: moderateScale(78),
    position: 'relative',
    marginBottom: '3%',
    marginTop: '-1%',
    overflow: 'visible',
  },

  progressBarTitle: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(80),
    lineHeight: moderateScale(100),
    textAlign: 'center',
    color: theme.textColor,
  },

  progressBarTitleLong: {
    fontSize: moderateScale(70),
    marginTop: moderateScale(4),
  },

  progressBarPercent: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(20),
    textAlign: 'center',
    color: theme.textColor,
    position: 'absolute',
    top: '12%',
    right: -moderateScale(16),
  },

  progressBarPercentTop: {
    top: '15%',
  },

  dayMessage: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(28),
    lineHeight: moderateScale(36),
    textAlign: 'center',
    color: theme.textColor,
    marginTop: moderateScale(17),
    marginBottom: moderateScale(15),
  },

  days: {
    fontFamily: 'Now-Bold',
  },

  dueDateMessage: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    textAlign: 'center',
    color: theme.textColor,
  },

  dueDate: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(25),
    textAlign: 'center',
    color: theme.textColor,
  },

  info: {
    paddingTop: moderateScale(17),
    paddingBottom: moderateScale(9),
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.borderColor,
    height: moderateScale(87),
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    marginBottom: moderateScale(10),
  },
}
