import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'
import { defaults as theme } from './colors'

export const aspectRatio = 262 / 366
export const defaultWidth = moderateScale(262)
export const defaultHeight = moderateScale(366)
export const progressMarkWidth = moderateScale(41)

export default {
  container: {
    width: defaultWidth,
    height: defaultHeight,
    overflow: 'hidden',
    alignItems: 'center',
    backgroundColor: theme.backgroundColor,
    paddingTop: moderateScale(20),
  },

  progressBarContent: {
    ...StyleSheet.absoluteFillObject,
  },

  progressBarImage: {
    width: null,
    height: null,
    flex: 1,
  },

  progressMark: {
    backgroundColor: '#42e0be',
    width: progressMarkWidth,
    height: progressMarkWidth,
    borderRadius: progressMarkWidth / 2,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowColor: 'black',
    shadowOffset: { height: 3, width: -2 },
  },

  progressMarkMessage: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: moderateScale(10),
    textAlign: 'center',
    color: 'white',
  },

  progressBarSubTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    textAlign: 'center',
    color: theme.textColor,
  },

  progressBarTitleBlock: {
    flexDirection: 'row',
    height: moderateScale(78),
    position: 'relative',
    marginBottom: '3%',
    marginTop: '-1%',
    overflow: 'visible',
  },

  progressBarTitle: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(80),
    textAlign: 'center',
    color: theme.textColor,
  },

  weekTitleWrapper: {
    alignSelf: 'center',
  },

  weekTitleContainer: {
    paddingVertical: moderateScale(5),
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.borderColor,
    height: moderateScale(87),
    alignSelf: 'stretch',
    justifyContent: 'center',
  },

  weekTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(25),
    textAlign: 'center',
  },

  info: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    marginTop: moderateScale(16),
    marginBottom: moderateScale(3),
  },

  column: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  weekWrapper: {
    position: 'relative',
    flexDirection: 'row',
  },

  fullWeeksDays: {
    fontSize: moderateScale(12),
    color: theme.textColor,
    fontFamily: 'Now-Bold',
    letterSpacing: 0,
    backgroundColor: 'transparent',
    alignSelf: 'flex-start',
    marginTop: moderateScale(6),
  },

  columnTitle: {
    fontSize: moderateScale(10),
    lineHeight: moderateScale(12),
    marginBottom: moderateScale(2.5),
    fontFamily: 'Now-Regular',
    color: theme.textColor,
    letterSpacing: 0,
    backgroundColor: 'transparent',
  },

  columnData: {
    fontSize: moderateScale(28),
    lineHeight: moderateScale(34),
    color: theme.textColor,
    fontFamily: 'Now-Bold',
  },
}
