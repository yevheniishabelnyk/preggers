/**
 *
 * PregnancyProgressFruit
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Text, Animated, Easing } from 'react-native'
import CircleProgressBar from '@/pregnancy/components/molecules/CircleProgressBar'
import { ExpoImage } from '@/vendor/cache-manager'

import Pregnancy from '@/app/database/models/Pregnancy'

import { pick, omit } from 'lodash'
import memoize from 'fast-memoize'
import applyForwardedRef from '@/shared/decorators/applyForwardedRef'

import { firebaseConnect } from 'react-redux-firebase'
import { getWeek } from '@/app/redux/selectors'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { getColors } from './colors'

import { scalableStyles } from '@/app/scaling'
import rawStyles, {
  defaultWidth,
  defaultHeight,
  aspectRatio,
  progressMarkWidth,
} from './styles'

import { PREGNANCY__DEFAULT_BABY_NAME } from '@/app/constants/BabyNames'

const ANIMATED_VALUES = ['portion']

const getPregnancyProgress = Pregnancy.makeGetProgress()

@firebaseConnect(props => {
  const { pregnancy } = props

  if (pregnancy) {
    const { pregnancyWeek } = getPregnancyProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    return [`content/weeks/${pregnancyWeek}`]
  }
})
@connect((state, props) => {
  const { pregnancy } = props

  let pregnancyWeekData
  const profileName = state.firebase.profile.name
  const locale = state.settings.locale

  if (pregnancy) {
    const { pregnancyWeek } = getPregnancyProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    pregnancyWeekData = getWeek(state, pregnancyWeek)
  }

  return {
    pregnancyWeekData,
    profileName,
    locale,
  }
})
@applyForwardedRef
@scalableStyles(rawStyles)
export default class PregnancyProgressFruit extends React.Component {
  static propTypes = {
    scale: PropTypes.number,
    pregnancy: PropTypes.object,
    animate: PropTypes.bool,
    isVisible: PropTypes.bool,
    profileName: PropTypes.string,
    locale: PropTypes.string,
    backgroundColor: PropTypes.string,
    theme: PropTypes.string,
    pregnancyWeekData: PropTypes.shape({
      babyLength: PropTypes.string,
      babyWeight: PropTypes.string,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    isVisible: true,
  }

  static aspectRatio = aspectRatio
  static defaultWidth = defaultWidth
  static defaultHeight = defaultHeight

  state = {
    passedDays: 0,
    leftDays: Pregnancy.LENGTH,
    pregnancyWeek: 1,
    passedWeeks: 0,
    passedWeeksDays: 0,
    portion: 0,
    ...getColors(this.props.theme, this.props.backgroundColor),
  }

  constructor(props) {
    super(props)

    this.isRendered = false
    this.hasRenderingError = false

    this.data = this.state

    if (props.animate) {
      this.animatedValue = new Animated.Value(0)
    }
  }

  render() {
    const { scale, pregnancy, animate, pregnancyWeekData } = this.props

    if (!pregnancy) {
      return null
    }

    const {
      portion,
      passedDays,
      leftDays,
      pregnancyWeek,
      passedWeeks,
      passedWeeksDays,
      borderColor,
      textColor,
      backgroundColor,
    } = this.state

    const percent = Math.floor(portion * 1000) / 10

    const isOverDue = leftDays < 0

    const fruitImage = do {
      if (pregnancyWeekData) {
        if (animate && pregnancyWeekData.image) {
          pregnancyWeekData.image
        } else if (pregnancyWeekData.shareImage) {
          pregnancyWeekData.shareImage
        }
      }
    }

    const weekTitle = this._getWeekTitle()

    const borderStyle = { borderColor }
    const textStyle = { color: textColor }

    this.styles.scale = scale

    return (
      <View style={[this.styles.container, { backgroundColor }]}>
        <CircleProgressBar
          portion={portion}
          scale={this.styles.scale}
          circlePartsNumber={3}
          progressMarkElementWidth={progressMarkWidth}
          progressMarkElement={
            <View style={this.styles.progressMark}>
              <Text style={this.styles.progressMarkMessage}>{percent}%</Text>
            </View>
          }
        >
          <View style={this.styles.progressBarContent}>
            {fruitImage ? (
              <ExpoImage
                style={this.styles.progressBarImage}
                source={fruitImage}
                resizeMode="contain"
                cacheImage
                onLoad={this._imageLoaded}
                onError={this._imageLoadedWithError}
              />
            ) : null}
          </View>
        </CircleProgressBar>

        <View style={this.styles.info}>
          <View style={this.styles.column}>
            <FormattedMessage
              {...messages.weekMessage}
              style={[this.styles.columnTitle, textStyle]}
            />

            <View style={this.styles.weekWrapper}>
              <Text style={[this.styles.columnData, textStyle]}>
                {pregnancyWeek}
              </Text>

              <Text style={[this.styles.fullWeeksDays, textStyle]}>
                {passedWeeks}+{passedWeeksDays}
              </Text>
            </View>
          </View>

          <View style={this.styles.column}>
            <FormattedMessage
              {...messages.daysPassedMessage}
              style={[this.styles.columnTitle, textStyle]}
            />

            <Text style={[this.styles.columnData, textStyle]}>
              {passedDays}
            </Text>
          </View>

          <View style={this.styles.column}>
            <FormattedMessage
              {...(isOverDue
                ? messages.daysOverMessage
                : messages.daysLeftMessage)}
              style={[this.styles.columnTitle, textStyle]}
            />

            <Text style={[this.styles.columnData, textStyle]}>
              {Math.abs(leftDays)}
            </Text>
          </View>
        </View>

        <View style={[this.styles.weekTitleContainer, borderStyle]}>
          <Text numberOfLines={3} style={[this.styles.weekTitle, textStyle]}>
            {weekTitle}
          </Text>
        </View>
      </View>
    )
  }

  componentWillMount() {
    if (this.animatedValue) {
      this.animatedValueListener = this.animatedValue.addListener(({ value }) =>
        this.setState({ portion: this.data.portion * value })
      )
    }

    const { pregnancy } = this.props

    if (pregnancy) {
      this._updateData(pregnancy, 400)
    }
  }

  componentWillUnmount() {
    if (this.animatedValue) {
      this.animatedValue.removeListener(this.animatedValueListener)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.pregnancy && nextProps.pregnancy) ||
      (this.props.pregnancy &&
        nextProps.pregnancy &&
        (this.props.pregnancy.dueDate !== nextProps.pregnancy.dueDate ||
          this.props.pregnancy.lengthType !== nextProps.pregnancy.lengthType))
    ) {
      this._updateData(nextProps.pregnancy, 150)
    }

    if (this.props.backgroundColor !== nextProps.backgroundColor) {
      const nextColors = getColors(nextProps.backgroundColor)

      console.info('nextColors: ', nextColors)

      this.setState(nextColors)
    }

    if (
      this.hasPendingAnimation &&
      !this.props.isVisible &&
      nextProps.isVisible
    ) {
      setTimeout(() => this._animate(), 300)
    }
  }

  _getData = pregnancy => {
    const pregnancyProgress = Pregnancy.getProgress(
      pregnancy.dueDate,
      pregnancy.lengthType
    )

    let portion = pregnancyProgress.passedDays / Pregnancy.LENGTH

    if (portion > 1) {
      portion = 1
    }

    return {
      leftDays: pregnancyProgress.leftDays,
      passedDays: pregnancyProgress.passedDays,
      pregnancyWeek: pregnancyProgress.pregnancyWeek,
      passedWeeks: pregnancyProgress.passedWeeks,
      passedWeeksDays: pregnancyProgress.passedWeeksDays,
      portion,
    }
  }

  _updateData = (pregnancy, animationDelay = 0) => {
    const newData = this._getData(pregnancy)

    if (this.animatedValue) {
      const newAnimatedValue = this.data.portion / newData.portion

      this.data = newData

      this.setState(omit(this.data, ANIMATED_VALUES), () => {
        this.animatedValue.setValue(newAnimatedValue)

        if (this.props.isVisible) {
          setTimeout(() => this._animate(), animationDelay)
        } else {
          this.hasPendingAnimation = true
        }
      })
    } else {
      this.data = newData

      this.setState(this.data)
    }
  }

  _animate = () => {
    this.hasPendingAnimation = false

    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 1000,
      easing: Easing.bezier(0.81, 0.8, 0.65, 0.99),
    }).start(() => this.setState(pick(this.data, ANIMATED_VALUES)))
  }

  _getWeekTitle = () => {
    const { pregnancyWeekData, locale } = this.props

    if (
      !pregnancyWeekData ||
      !pregnancyWeekData.title ||
      !pregnancyWeekData.title[locale]
    ) {
      return ''
    }

    const title = pregnancyWeekData.title[locale]

    const { formatMessage } = this.context.intl
    const { profileName, pregnancy } = this.props

    const childName =
      pregnancy.childName === PREGNANCY__DEFAULT_BABY_NAME
        ? formatMessage(messages.defaultChildName)
        : pregnancy.childName

    return this._memoizedReplaceMergeTags(title, profileName, childName)
  }

  _memoizedReplaceMergeTags = memoize((text, profileName, childName) =>
    text
      .replace(/\*\|NAME\|\*/g, profileName)
      .replace(/\*\|BABY_NAME\|\*/g, childName)
  )

  _imageLoaded = () => {
    this.isImageLoaded = true
    this.isRendered = true

    this.hasRenderingError = false
  }

  _imageLoadedWithError = () => {
    this.isImageLoaded = true
    this.isRendered = true

    this.hasRenderingError = true
  }

  waitFor = () => {
    if (this.isRendered) {
      if (this.hasRenderingError) {
        return Promise.reject()
      }

      return Promise.resolve()
    }

    return this._listenRendering().then(() => {
      if (this.hasRenderingError) {
        return Promise.reject()
      }
    })
  }

  _listenRendering = () =>
    new Promise(resolve => {
      const interval = setInterval(() => {
        if (this.isRendered) {
          clearInterval(interval)

          resolve()
        }
      }, 20)
    })
}
