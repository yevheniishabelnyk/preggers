import { defineMessages } from 'react-intl'

export default defineMessages({
  weekMessage: 'WEEK',
  daysPassedMessage: 'DAYS PASSED',
  daysLeftMessage: 'DAYS LEFT',
  daysOverMessage: 'DAYS OVER',
  defaultChildName: 'My baby',
})
