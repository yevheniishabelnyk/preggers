/*
 *
 * Pregnancy screens
 *
 */

import PregnancyLoss from './screens/PregnancyLoss'

export default {
  '/pregnancy-loss': {
    screen: PregnancyLoss,
  },
}
