import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  headerContainer: {
    marginHorizontal: moderateScale(22),
    position: 'absolute',
    zIndex: 1,
    top: 0,
    backgroundColor: 'rgb(249, 250, 252)',
    paddingBottom: moderateScale(20),
  },

  listContainer: {
    position: 'relative',
    top: moderateScale(170, 0.3),
  },

  header: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, 0.6),
    color: 'rgb(59, 72, 89)',
    textAlign: 'center',
    marginTop: moderateScale(53, 0),
    paddingBottom: moderateScale(30),
  },

  formSelect: {
    minWidth: moderateScale(330, 1),
  },

  footer: {
    height: moderateScale(200),
  },
})

export default styles
