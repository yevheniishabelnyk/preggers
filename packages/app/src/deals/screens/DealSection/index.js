import React from 'react'
import PropTypes from 'prop-types'
import { View, FlatList } from 'react-native'

import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { getCategoriesForDealAds } from '@/app/redux/selectors/dealCategories'
import { FormattedMessage } from '@/vendor/react-intl-native'

import { AD_TYPE_DEALS } from '@/app/redux/ads/constants'
import { getPregnancy, getDealAdsList } from '@/app/redux/selectors'
import * as AdActions from '@/app/redux/ads/actions'
import * as RouterActions from '@/app/redux/router/actions'

import BaseScreen from '@/app/base/components/BaseScreen'
import FormSelect from '@/settings/components/molecules/FormSelect'
import ContentServiceAdvertisement from '@/ads/components/molecules/ContentServiceAdvertisement'

import messages from './messages'
import styles from './styles'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  const listeners = ['content/dealCategories']

  if (!profile.isEmpty) {
    listeners.push(`users/${profile.id}/${profile.pregnancy}`)
  }

  return listeners
})
@connect((state, props) => {
  const {
    locale,
    profile: { pregnancy },
  } = props.screenProps

  let userPregnancy

  if (pregnancy) {
    userPregnancy = getPregnancy(state, pregnancy)
  }

  return {
    isFocused: props.navigation.isFocused(),
    isConnected: state.device.isConnected,
    categories: getCategoriesForDealAds(state, locale),
    userPregnancy,
    dealAds: getDealAdsList(state),
  }
})
export default class DealSectionScreen extends BaseScreen {
  static propTypes = {
    categories: PropTypes.array,
  }

  static defaultProps = {
    dealAds: [],
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    selectedCategory: 'All',
  }

  render() {
    const { dealAds } = this.props
    const { formatMessage } = this.context.intl

    let { categories } = this.props

    const categoryAllOption = {
      label: formatMessage(messages.allCategories),
      value: 'All',
    }

    categories = [categoryAllOption, ...categories]

    const visibaleAds = this._getVisibleAds(dealAds)

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <FormattedMessage {...messages.title} style={styles.header} />

          <FormSelect
            title={formatMessage(messages.formTitle)}
            value={this.state.selectedCategory}
            items={categories}
            onSelect={this._onCategorySelect}
            style={styles.formSelect}
          />
        </View>

        <View style={styles.listContainer}>
          <FlatList
            data={visibaleAds}
            extraData={this.state.selectedCategory}
            renderItem={this._renderItem}
            keyExtractor={this._keyExtractor}
            ListFooterComponent={<View style={styles.footer} />}
          />
        </View>
      </View>
    )
  }

  componentDidMount() {
    this._fetchAds()
  }

  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.isFocused && nextProps.isFocused) ||
      this.props.isConnected !== nextProps.isConnected
    ) {
      this._fetchAds()
    }
  }

  _renderItem = adItem => {
    const ad = adItem.item

    return <ContentServiceAdvertisement dispatch={this._} ad={ad} />
  }

  _keyExtractor = item => String(item.uid)

  _onCategorySelect = id => {
    this.setState({ selectedCategory: id })
  }

  _onAdPress = ad => {
    this._(RouterActions.handleLink(ad.link))

    this._(AdActions.markInteraction(ad, 'click'))
  }

  _getVisibleAds = (allAds = []) => {
    const { selectedCategory } = this.state

    if (selectedCategory === 'All') {
      return allAds
    }

    return this._filterAds(allAds, selectedCategory)
  }

  _filterAds = (allAds, selectedCategory) => {
    const selectedCategoryList = allAds.filter(category => {
      return (
        category.dealCategoryIds &&
        category.dealCategoryIds.includes(selectedCategory)
      )
    })

    if (selectedCategoryList.length < 1) {
      return []
    }

    return selectedCategoryList
  }

  _fetchAds = async () => {
    const { profile, locale } = this.props.screenProps
    const { deviceCountryCode, userPregnancy } = this.props

    const pregnancies = []

    if (userPregnancy) {
      pregnancies.push(userPregnancy.dueDate)
    }

    const requestBody = {
      types: [AD_TYPE_DEALS],
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      userType: profile.type,
      pregnancies,
    }

    try {
      await this._(AdActions.fetchAdsAndReplaceTypeState(requestBody))
    } catch (err) {
      console.log(err)
    }
  }
}
