import { defineMessages } from 'react-intl'

export default defineMessages({
  allCategories: 'All deals',
  title: 'Deals',
  formTitle: 'Categories',
})
