/*
 *
 * Deals routes
 *
 */

import DealSection from './screens/DealSection'

export default {
  '/deal-section': {
    screen: DealSection,
  },
}
