import { defineMessages } from 'react-intl'

export default defineMessages({
  heading: 'Sign up for the insurance',
  buttonText: 'Insure us',
  readMore: 'Read more about the insurance',
  numberOfChildren: 'Children in the pregnancy',
  pin: 'Civic number',
  phoneNumber: 'Mobile number',
  email: 'E-mail',
  checkboxText:
    'By clicking "Insure us", I confirm that I\'ve read the order information, and that I have read and accept Moderna Försäkringar\'s terms and conditions and privacy policy',
  termsError: 'You need to accept the terms',
  emailError: 'You need to provide an e-mail',
  phoneNumberError: 'You need to provide a mobile number',
  pinError: 'Please provide your civic number',
  numberOfChildrenError: 'Please specify the number of children',
})
