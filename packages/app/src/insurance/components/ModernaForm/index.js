import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { isFunction } from 'lodash'

import FormInput from '@/settings/components/molecules/FormInput'
import TermsCheckbox from '@/onboarding/components/TermsCheckbox'
import ActionButton from '@/settings/components/atoms/ActionButton'
import FormError from '@/forms/components/atoms/FormError'

import messages from './messages'
import styles from './styles'

export default class ModernaForm extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static propTypes = {
    onSubmit: PropTypes.func,
    onAfterSubmit: PropTypes.func,
    interactionAction: PropTypes.func,
    navigation: PropTypes.object,
    defaultEmail: PropTypes.string,
    isLarge: PropTypes.bool,
  }

  state = {
    numberOfChildren: '1',
    numberOfChildrenError: false,
    pin: '',
    pinError: false,
    phoneNumber: '',
    phoneNumberError: false,
    email: '',
    emailError: false,
    agreedToTerms: false,
    canSubmit: false,
    isSubmitting: false,
    termsError: false,
  }

  componentDidMount() {
    const { defaultEmail } = this.props
    if (defaultEmail) {
      this.setState({ email: defaultEmail })
    }
  }

  render() {
    const { formatMessage } = this.context.intl
    const {
      agreedToTerms,
      numberOfChildren,
      pin,
      phoneNumber,
      email,
      numberOfChildrenError,
      pinError,
      phoneNumberError,
      emailError,
      isSubmitting,
      termsError,
    } = this.state

    return (
      <View style={styles.container}>
        <Text style={styles.heading}>{formatMessage(messages.heading)}</Text>
        <FormInput
          title={formatMessage(messages.numberOfChildren)}
          value={numberOfChildren}
          onChange={this._onChangeNumberOfChildren}
          keyboardType="numeric"
          titleStyle={styles.input}
          inputStyle={styles.input}
          style={styles.inputContainer}
          error={numberOfChildrenError}
          autoCorrct={false}
          returnKeyType="done"
        />

        <FormError
          isVisible={numberOfChildrenError}
          style={styles.error}
          title={formatMessage(messages.numberOfChildrenError)}
        />

        <FormInput
          title={formatMessage(messages.pin)}
          value={pin}
          onChange={this._onChangePin}
          keyboardType="numeric"
          titleStyle={styles.input}
          inputStyle={styles.input}
          style={styles.inputContainer}
          error={pinError}
          autoCorrect={false}
          returnKeyType="done"
        />

        <FormError
          isVisible={pinError}
          style={styles.error}
          title={formatMessage(messages.pinError)}
        />

        <FormInput
          title={formatMessage(messages.phoneNumber)}
          value={phoneNumber}
          onChange={this._onChangePhoneNumber}
          keyboardType="phone-pad"
          titleStyle={styles.input}
          inputStyle={styles.input}
          style={styles.inputContainer}
          error={phoneNumberError}
          autoCorrect={false}
          returnKeyType="done"
        />

        <FormError
          isVisible={phoneNumberError}
          style={styles.error}
          title={formatMessage(messages.phoneNumberError)}
        />

        <FormInput
          title={formatMessage(messages.email)}
          value={email}
          onChange={this._onChangeEmail}
          keyboardType="email-address"
          titleStyle={styles.input}
          inputStyle={styles.input}
          style={styles.inputContainer}
          error={emailError}
          autoCorrect={false}
          returnKeyType="done"
        />

        <FormError
          isVisible={emailError}
          style={styles.error}
          title={formatMessage(messages.emailError)}
        />

        <TermsCheckbox
          text={formatMessage(messages.checkboxText)}
          onChange={this._onCheckboxChange}
          checked={agreedToTerms}
          onTextPress={this._onTermsPress}
          color="rgb(88, 88, 88)"
          checkboxColor="rgb(250, 65, 105)"
          textStyle={styles.terms}
        />

        <FormError
          isVisible={termsError}
          style={styles.error}
          title={formatMessage(messages.termsError)}
        />

        <View style={styles.buttonContainer}>
          <ActionButton
            title={formatMessage(messages.buttonText)}
            onPress={this._onSubmit}
            textStyle={styles.buttonText}
            style={styles.button}
            loader={isSubmitting}
          />
        </View>
        <TouchableOpacity
          onPress={this._onReadMorePress}
          style={styles.readMoreButton}
        >
          <Text style={styles.readMoreButtonText}>
            {formatMessage(messages.readMore)}
          </Text>
          <Image
            source={require('assets/icons/arrow-left-thin.png')}
            style={styles.readMoreButtonArrowIcon}
          />
        </TouchableOpacity>
      </View>
    )
  }

  _onTermsPress = () =>
    this.props.navigation.navigate('/moderna-pregnancy-insurance-terms')

  _onReadMorePress = () =>
    this.props.navigation.navigate('/moderna-pregnancy-insurance-info', {
      isLarge: this.props.isLarge,
    })

  _updateCanSubmit = async () => {
    const {
      phoneNumber,
      pin,
      agreedToTerms,
      numberOfChildren,
      email,
    } = this.state

    let numberOfChildrenError,
      pinError,
      phoneNumberError,
      emailError,
      termsError

    // Swedish phone number
    if (!phoneNumber.match(/^(\+46|0046|0)7[0-9]{8}$/)) {
      phoneNumberError = true
    }

    // Swedish ssn
    if (!pin.match(/^(19|20)?(\d{6}(-|\s)\d{4}|(?!19|20)\d{10})$/)) {
      pinError = true
    }

    if (+numberOfChildren === 0) {
      numberOfChildrenError = true
    }

    if (!email.match(/\S+@\S+\.\S+/)) {
      emailError = true
    }

    if (!agreedToTerms) {
      termsError = true
    }

    const canSubmit =
      agreedToTerms &&
      !phoneNumberError &&
      !pinError &&
      !numberOfChildrenError &&
      !termsError &&
      !emailError

    await this._setStateAsync({
      canSubmit,
      phoneNumberError,
      pinError,
      numberOfChildrenError,
      emailError,
      termsError,
    })
  }

  _setStateAsync = data => new Promise(resolve => this.setState(data, resolve))

  _onChangeNumberOfChildren = numberOfChildren =>
    this._onChangeValue('numberOfChildren', numberOfChildren)

  _onChangePin = pin => this._onChangeValue('pin', pin)

  _onChangePhoneNumber = phoneNumber =>
    this._onChangeValue('phoneNumber', phoneNumber)

  _onChangeEmail = email => this._onChangeValue('email', email)

  _onCheckboxChange = agreedToTerms =>
    this._onChangeValue('agreedToTerms', agreedToTerms)

  _onChangeValue = (key, value) => this.setState({ [key]: value })

  _onSubmit = async () => {
    const {
      agreedToTerms,
      numberOfChildren,
      pin,
      phoneNumber,
      email,
    } = this.state

    const { interactionAction } = this.props

    if (isFunction(interactionAction)) {
      interactionAction()
    }

    await this._updateCanSubmit()

    if (this.state.canSubmit) {
      this.setState({ isSubmitting: true })

      await this.props.onSubmit({
        numberOfChildren,
        pin,
        phoneNumber,
        email,
        agreedToTerms,
      })

      this.setState({ isSubmitting: false })

      this.props.onAfterSubmit()
    }
  }
}
