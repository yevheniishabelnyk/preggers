import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: moderateScale(21),
    marginTop: moderateScale(30),
  },

  heading: {
    color: 'rgb(43, 56, 87)',
    fontSize: moderateScale(20),
    fontFamily: 'Now-Black',
    marginBottom: moderateScale(15),
  },

  terms: {
    fontFamily: 'Roboto-Regular',
    lineHeight: moderateScale(14),
    fontSize: moderateScale(12),
    color: 'rgb(34, 33, 33)',
  },

  input: {
    color: 'rgb(131, 146, 167)',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
  },

  inputContainer: {
    minHeight: moderateScale(60),
    height: moderateScale(60),
    marginTop: moderateScale(13),
  },

  error: {
    marginTop: moderateScale(13),
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  button: {
    marginTop: moderateScale(20),
  },

  readMoreButton: {
    marginTop: moderateScale(27),
    marginBottom: moderateScale(50),
    height: moderateScale(60),
    width: moderateScale(353),
    borderTopLeftRadius: moderateScale(8),
    borderBottomLeftRadius: moderateScale(8),
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },

  readMoreButtonText: {
    color: 'rgb(34, 34, 34)',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    marginLeft: (20 / 375) * width,
  },

  readMoreButtonArrowIcon: {
    width: moderateScale(20),
    height: moderateScale(10),
    marginRight: moderateScale(20),
    transform: [
      {
        rotate: '180deg',
      },
    ],
  },

  buttonText: {
    color: 'white',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16),
  },
})

export default styles
