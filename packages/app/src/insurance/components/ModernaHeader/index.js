import React from 'react'
import PropTypes from 'prop-types'
import { View, Image } from 'react-native'

import GoBackButton from '@/onboarding/components/GoBackButton'

import styles from './styles'

export default class ModernaHeader extends React.Component {
  static propTypes = {
    image: Image.propTypes.source,
    goBack: PropTypes.func,
  }

  render() {
    const { image, goBack } = this.props

    return (
      <View style={styles.header}>
        <GoBackButton onPress={goBack} style={styles.backButton} />

        <View style={styles.headerImageWrapper}>
          <Image source={image} style={styles.headerImage} />
        </View>
      </View>
    )
  }
}
