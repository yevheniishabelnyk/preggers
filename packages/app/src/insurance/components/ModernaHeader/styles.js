import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'
const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  header: {
    position: 'relative',
    top: 0,
    left: 0,
    height: moderateScale(214),
    width,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  backButton: {
    top: moderateScale(30),
  },

  headerImageWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
  },

  headerImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
    width: '100%',
    height: moderateScale(214),
    overflow: 'visible',
  },
})

export default styles
