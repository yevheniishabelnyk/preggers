import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: (21 / 375) * width,
    marginTop: (30 / 375) * width,
  },

  heading: {
    color: 'rgb(43, 56, 87)',
    fontSize: (20 / 375) * width,
    fontFamily: 'Now-Black',
    marginBottom: (15 / 375) * width,
  },

  terms: {
    fontFamily: 'Roboto-Regular',
    lineHeight: (14 / 375) * width,
    fontSize: (12 / 375) * width,
    color: 'rgb(34, 33, 33)',
  },

  input: {
    color: 'rgb(131, 146, 167)',
    fontFamily: 'Now-Medium',
    fontSize: (14 / 375) * width,
  },

  inputContainer: {
    minHeight: (60 / 375) * width,
    height: (60 / 375) * width,
    marginTop: (13 / 375) * width,
  },

  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  button: {
    marginTop: (20 / 375) * width,
  },

  readMoreButton: {
    marginTop: (27 / 375) * width,
    marginBottom: (50 / 375) * width,
    height: (60 / 375) * width,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },

  readMoreButtonText: {
    color: 'rgb(34, 34, 34)',
    fontFamily: 'Now-Medium',
    fontSize: (14 / 375) * width,
    marginLeft: (20 / 375) * width,
  },

  readMoreButtonArrowIcon: {
    width: 20,
    height: 10,
    marginRight: (20 / 375) * width,
    transform: [
      {
        rotate: '180deg',
      },
    ],
  },

  buttonText: {
    color: 'white',
    fontFamily: 'Now-Medium',
    fontSize: (16 / 375) * width,
  },
})

export default styles
