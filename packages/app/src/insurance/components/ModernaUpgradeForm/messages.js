import { defineMessages } from 'react-intl'

export default defineMessages({
  heading: 'Sign up for the insurance',
  checkboxText:
    'By clicking "Insure us", I confirm that I\'ve read the order information, and that I have read and accept Moderna Försäkringars terms and conditions and privacy policy.',
  button: 'Insure us',
  readMore: 'Read more about the insurance',
})
