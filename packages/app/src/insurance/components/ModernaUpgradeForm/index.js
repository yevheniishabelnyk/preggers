import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { isFunction } from 'lodash'

import TermsCheckbox from '@/onboarding/components/TermsCheckbox'
import ActionButton from '@/settings/components/atoms/ActionButton'

import styles from './styles'

import messages from './messages'

export default class ModernaUpgradeForm extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static propTypes = {
    onSubmit: PropTypes.func,
    interactionAction: PropTypes.func,
    onAfterSubmit: PropTypes.func,
    navigation: PropTypes.object,
  }

  state = {
    isTermsAccepted: false,
    isSubmitting: false,
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isTermsAccepted, isSubmitting } = this.state

    return (
      <View style={styles.container}>
        <Text style={styles.heading}>{formatMessage(messages.heading)}</Text>

        <TermsCheckbox
          text={formatMessage(messages.checkboxText)}
          onChange={this._onChangeTermsAcceptance}
          checked={isTermsAccepted}
          onTextPress={this._onTermsPress}
          color="rgb(88, 88, 88)"
          checkboxColor="rgb(250, 65, 105)"
          textStyle={styles.terms}
        />

        <View style={styles.buttonContainer}>
          <ActionButton
            title={formatMessage(messages.button)}
            onPress={this._onSubmit}
            textStyle={styles.buttonText}
            style={styles.button}
            loader={isSubmitting}
          />
        </View>

        <TouchableOpacity
          onPress={this._onReadMorePress}
          style={styles.readMoreButton}
        >
          <Text style={styles.readMoreButtonText}>
            {formatMessage(messages.readMore)}
          </Text>
          <Image
            source={require('assets/icons/arrow-left-thin.png')}
            style={styles.readMoreButtonArrowIcon}
          />
        </TouchableOpacity>
      </View>
    )
  }

  _onTermsPress = () =>
    this.props.navigation.navigate('/moderna-pregnancy-insurance-terms')

  _onReadMorePress = () =>
    this.props.navigation.navigate('/moderna-pregnancy-insurance-info', {
      isLarge: true,
    })

  _onChangeTermsAcceptance = value => this.setState({ isTermsAccepted: value })

  _onSubmit = async () => {
    const { interactionAction } = this.props

    if (isFunction(interactionAction)) {
      interactionAction()
    }

    if (this.state.isTermsAccepted) {
      this.setState({ isSubmitting: true })
      await this.props.onSubmit({})
      this.setState({ isSubmitting: false })
      this.props.onAfterSubmit()
    }
  }
}
