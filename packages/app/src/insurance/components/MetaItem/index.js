import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Text } from 'react-native'
import styles from './styles'

const checkMark = (
  <View style={[styles.iconContainer, styles.green]}>
    <Image
      source={require('assets/icons/checked-white.png')}
      style={styles.checkMark}
    />
  </View>
)

const cross = (
  <View style={[styles.iconContainer, styles.red]}>
    <Image
      source={require('assets/icons/white-cross.png')}
      style={styles.cross}
    />
  </View>
)

const MetaItem = ({ checked, heading, text, borderRightWidth }) => (
  <View style={[styles.container, { borderRightWidth }]}>
    {heading && <Text style={styles.heading}>{heading}</Text>}
    {checked && checkMark}
    {checked || cross}
    <Text style={styles.text}>{text}</Text>
  </View>
)

MetaItem.propTypes = {
  checked: PropTypes.bool,
  borderRightWidth: PropTypes.number,
  text: PropTypes.string,
  heading: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
}

export default MetaItem
