import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: moderateScale(13),
    borderBottomColor: 'rgba(151, 151, 151, 0.29)',
    borderBottomWidth: 1,
    borderRightColor: 'rgba(151, 151, 151, 0.29)',
    borderRightWidth: 1,
    width: width / 2 - (22 / 375) * width - 1,
    paddingHorizontal: moderateScale(15),
    paddingBottom: moderateScale(8),
  },

  iconContainer: {
    width: moderateScale(20),
    height: moderateScale(20),
    borderRadius: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'center',
  },

  green: {
    backgroundColor: 'rgb(80, 227, 194)',
  },

  red: {
    backgroundColor: 'rgb(242, 83, 117)',
  },

  checkMark: {
    width: moderateScale(10),
    height: moderateScale(7.5),
  },

  cross: {
    width: moderateScale(10),
    height: moderateScale(10),
  },

  heading: {
    color: 'rgb(35, 40, 46)',
    fontSize: moderateScale(15),
    lineHeight: moderateScale(23),
    fontFamily: 'Now-Black',
    marginVertical: moderateScale(10),
    textAlign: 'center',
    width: width / 2 - (22 / 375) * width - 1,
    paddingHorizontal: moderateScale(15),
  },

  text: {
    marginVertical: moderateScale(7),
    textAlign: 'center',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    lineHeight: moderateScale(22),
    color: 'rgb(35, 40, 46)',
  },
})

export default styles
