/**
 *
 * ModernaErrorModal
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  View,
  Text,
  Image,
  Modal,
  TouchableOpacity,
  Linking,
} from 'react-native'
import ActionButton from '@/settings/components/atoms/ActionButton'

import styles from './styles'

import messages from './messages'

const ERROR_CODE_ALREADY_INSURED = 'ME1'

const ERROR_CODE_INVALID_SSN = 'ME2'

export default class ModernaErrorModal extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func,
    errorCode: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  }

  render() {
    return (
      <Modal animationType="fade" transparent visible={this.props.visible}>
        <View style={styles.container}>
          <View style={styles.dialog}>
            <TouchableOpacity
              onPress={this.props.onClose}
              style={styles.closeModalButton}
            >
              <Image
                source={require('assets/icons/cross-gray.png')}
                style={styles.closeModalIcon}
              />
            </TouchableOpacity>

            <Image
              source={require('assets/img/connection-faild.png')}
              style={styles.image}
            />

            {this._renderBody()}

            {this._renderButton()}
          </View>
        </View>
      </Modal>
    )
  }

  _renderBody = () => {
    const { formatMessage } = this.context.intl

    const { errorCode } = this.props

    if (errorCode === ERROR_CODE_ALREADY_INSURED) {
      return (
        <Text style={styles.text}>
          {formatMessage(messages.alreadyInsured)}
        </Text>
      )
    }

    if (errorCode === ERROR_CODE_INVALID_SSN) {
      return (
        <Text style={styles.text}>{formatMessage(messages.invalidSsn)}</Text>
      )
    }

    return (
      <View>
        <Text style={styles.text}>{formatMessage(messages.text1)}</Text>

        <Text style={styles.largeText}>
          {formatMessage(messages.telephone)}
        </Text>

        <Text style={styles.text}>{formatMessage(messages.text2)}</Text>
      </View>
    )
  }

  _renderButton = () => {
    const { formatMessage } = this.context.intl

    const { errorCode } = this.props

    let title = formatMessage(messages.button)

    if (errorCode === ERROR_CODE_INVALID_SSN) {
      title = formatMessage(messages.ok)
    }

    return (
      <ActionButton
        title={title}
        onPress={this._onPress}
        style={styles.button}
        textStyle={styles.buttonText}
      />
    )
  }

  _onPress = () => {
    const { formatMessage } = this.context.intl

    const { errorCode, onClose } = this.props

    if (errorCode === ERROR_CODE_INVALID_SSN) {
      return onClose()
    }

    return Linking.openURL(`tel:${formatMessage(messages.telephoneFormatted)}`)
  }
}
