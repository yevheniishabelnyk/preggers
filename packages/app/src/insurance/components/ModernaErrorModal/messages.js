import { defineMessages } from 'react-intl'

export default defineMessages({
  text1:
    'Something has gone wrong, please contact\nModerna Försäkringars\ncustomer service on',
  telephone: '0200 - 259 259',
  telephoneFormatted: '+46200259259',
  text2: 'and they will help you!',
  button: 'Call',
  ok: 'Ok',
  alreadyInsured:
    'It seems that you already have an existing insurance at Moderna. Please contact Moderna Försäkringars customer service on 0200 - 259 259 and they will help you!',
  invalidSsn: 'Please check your civic number',
})
