/**
 *
 * ModernaTermListItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Text, Image } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class ModernaTermListItem extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onPress: PropTypes.func,
  }

  render() {
    const { item } = this.props

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this._itemPressed}
        activeOpacity={0.95}
      >
        <Image source={require('assets/icons/pdf.png')} style={styles.icon} />

        <Text style={styles.title}>{item.title}</Text>
      </TouchableOpacity>
    )
  }

  _itemPressed = () => {
    const { item, onPress } = this.props

    if (isFunction(onPress)) {
      onPress(item)
    }
  }
}
