import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    marginTop: moderateScale(21),
    flexDirection: 'row',
    marginLeft: moderateScale(27),
  },

  icon: {
    width: moderateScale(29),
    height: moderateScale(33),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(27),
    color: 'rgb(35, 40, 46)',
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    textDecorationColor: 'rgb(35, 40, 46)',
    marginLeft: moderateScale(11),
  },
})

export default styles
