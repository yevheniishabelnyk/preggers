import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Here you can read more about our terms and conditions!',
  productInformation: 'Short information about the insurance',
  orderInformation: 'Order information',
  termsAndConditions: 'Terms and conditions',
  appendixToTerms: 'Appendix to terms and conditions',
  privacyPolicy: 'Privacy policy',
})
