import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  body: {
    paddingHorizontal: moderateScale(22),
    marginTop: moderateScale(13, 4),
  },

  headerTitle: {
    color: 'rgb(43, 56, 87)',
    fontSize: moderateScale(25),
    fontFamily: 'Now-Bold',
    marginVertical: moderateScale(30),
    textAlign: 'center',
  },
})

export default styles
