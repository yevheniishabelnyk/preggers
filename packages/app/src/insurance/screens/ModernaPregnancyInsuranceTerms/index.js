/*
 *
 * ModernaPregnancyInsuranceTerms screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { WebBrowser } from 'expo'
import { View, ScrollView } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import ModernaHeader from '@/insurance/components/ModernaHeader'
import ModernaTermListItem from '@/insurance/components/ModernaTermListItem'

import messages from './messages'
import { FormattedMessage } from '@/vendor/react-intl-native'

import styles from './styles'

export default class ModernaPregnancyInsuranceTerms extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props, context) {
    super(props)

    const { formatMessage } = context.intl

    this.state = {
      terms: [
        {
          id: 'product_information',
          title: formatMessage(messages.productInformation),
          link:
            'https://firebasestorage.googleapis.com/v0/b/preggers-production.appspot.com/o/moderna%2FMF%20produktfaktablad%20Gravidf%C3%B6rs%C3%A4kring.pdf?alt=media&token=69bac553-4d01-4ce8-b32c-8132e60a707f',
        },
        {
          id: 'order_information',
          title: formatMessage(messages.orderInformation),
          link:
            'https://firebasestorage.googleapis.com/v0/b/preggers-production.appspot.com/o/moderna%2FF%C3%B6rk%C3%B6psinfo%20gravid%20MF%202018-10-08.pdf?alt=media&token=cc7deae9-7454-4071-ba57-70634c0b8855',
        },

        {
          id: 'terms_and_conditions',
          title: formatMessage(messages.termsAndConditions),
          link:
            'https://firebasestorage.googleapis.com/v0/b/preggers-production.appspot.com/o/moderna%2FVillkor%20Gravidf%C3%B6rs%C3%A4kring%20MF%202016-09-01.pdf?alt=media&token=28f04a5f-193d-4155-8103-946c82845171',
        },

        {
          id: 'appendix_to_terms',
          title: formatMessage(messages.appendixToTerms),
          link:
            'https://firebasestorage.googleapis.com/v0/b/preggers-production.appspot.com/o/moderna%2FBilaga%20till%20f%C3%B6rs%C3%A4kringsvillkor%20MF%20Gravid.pdf?alt=media&token=130f2310-d60b-4def-a9ea-9a0d92854fd1',
        },

        {
          id: 'privacy_policy',
          title: formatMessage(messages.privacyPolicy),
          link: 'https://www.modernaforsakringar.se/allmant/pul/',
        },
      ],
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <ModernaHeader
          image={require('assets/img/moderna-large.png')}
          goBack={this._goBack}
        />

        <View style={styles.body}>
          <FormattedMessage
            style={styles.headerTitle}
            {...messages.headerTitle}
          />
        </View>

        {this.state.terms.map(item => (
          <ModernaTermListItem
            key={item.id}
            item={item}
            onPress={this._openTermLink}
          />
        ))}
      </ScrollView>
    )
  }

  _openTermLink = term => {
    WebBrowser.openBrowserAsync(term.link)
  }
}
