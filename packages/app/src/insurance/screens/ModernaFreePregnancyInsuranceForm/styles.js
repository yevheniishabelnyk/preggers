import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  body: {
    paddingHorizontal: moderateScale(22),
    marginTop: moderateScale(13, 4),
  },

  h1: {
    color: 'rgb(43, 56, 87)',
    fontSize: moderateScale(25),
    fontFamily: 'Now-Bold',
    marginVertical: moderateScale(30),
    textAlign: 'center',
    marginBottom: moderateScale(45),
  },

  h2: {
    color: 'rgb(43, 56, 87)',
    fontSize: moderateScale(20),
    fontFamily: 'Now-Bold',
    marginBottom: moderateScale(5),
  },

  metaIcon: {
    width: moderateScale(17),
    height: moderateScale(13),
  },

  meta: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(27),
    color: 'rgb(35, 40, 46)',
    marginLeft: moderateScale(14),
  },

  metaItemContainer: {
    marginTop: moderateScale(12),
    flexDirection: 'row',
  },

  metaIconContainer: {
    width: moderateScale(34),
    height: moderateScale(34),
    borderRadius: moderateScale(16),
    backgroundColor: 'rgb(80, 227, 194)',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default styles
