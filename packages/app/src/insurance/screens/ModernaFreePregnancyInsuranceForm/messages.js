import { defineMessages } from 'react-intl'

export default defineMessages({
  heading1: 'Maternity Insurance Free',
  heading2: 'This is included:',
  meta1: 'Medical disability',
  meta2: 'Crisis assistance',
  meta3: 'Death benefits',
  termsError: 'You need to accept the terms',
})
