/*
 *
 * ModernaFreePregnancyInsuranceForm screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { firebaseConnect } from 'react-redux-firebase'
import { getActiveCompetitionUid, getPregnancy } from '@/app/redux/selectors'

import { View, Text, Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ModernaHeader from '@/insurance/components/ModernaHeader'
import ModernaForm from '@/insurance/components/ModernaForm'
import ModernaErrorModal from '@/insurance/components/ModernaErrorModal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { ADS_FETCH_HEADERS } from '@/app/redux/ads/constants'
import { CONTENT_SERVICE__API_BASE } from 'config'

import * as AdActions from '@/app/redux/ads/actions'
import * as ChecklistActions from '@/app/database/actions/checklist'

import messages from './messages'

import styles from './styles'

@firebaseConnect(['competitions/activeCompetitions'])
@connect((state, props) => {
  return {
    userPregnancy: getPregnancy(state, props.screenProps.profile.pregnancy),
    activeCompetitionUid: getActiveCompetitionUid(state) || null,
  }
})
export default class ModernaFreePregnancyInsuranceForm extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    modernaError: false,
    errorCode: false,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <KeyboardAwareScrollView style={styles.container}>
        <ModernaHeader
          image={require('assets/img/moderna-free.png')}
          goBack={this._goBack}
        />

        <View style={styles.body}>
          <Text style={styles.h1}>{formatMessage(messages.heading1)}</Text>

          <Text style={styles.h2}>{formatMessage(messages.heading2)}</Text>

          {[
            formatMessage(messages.meta1),
            formatMessage(messages.meta2),
            formatMessage(messages.meta3),
          ].map((text, i) => (
            <View key={i} style={styles.metaItemContainer}>
              <View style={styles.metaIconContainer}>
                <Image
                  source={require('assets/icons/checked-white.png')}
                  style={styles.metaIcon}
                />
              </View>

              <Text style={styles.meta}>{text}</Text>
            </View>
          ))}
        </View>

        <ModernaForm
          navigation={this.props.navigation}
          onSubmit={this._onSubmit}
          onAfterSubmit={this._onAfterSubmit}
          defaultEmail={this.props.screenProps.profile.email}
          isLarge={false}
          interactionAction={this._markClickInteraction}
        />

        <ModernaErrorModal
          errorCode={this.state.errorCode}
          visible={this.state.modernaError}
          onClose={this._onCloseModal}
        />
      </KeyboardAwareScrollView>
    )
  }

  componentDidMount() {
    const { navigation } = this.props

    const { email } = this.props.screenProps.profile

    let connectedAdUid

    const connectedAd = navigation.getParam('connectedAd')

    const adSource = navigation.getParam('adSource')

    if (!connectedAd || connectedAd === 'THIS_IS_NOT_AN_ID') {
      connectedAdUid = navigation.getParam('connectedAdUid')

      this._(
        AdActions.markInteractionByUidAndEmail(
          connectedAdUid,
          email,
          'impression',
          adSource
        )
      )
    } else if (connectedAd) {
      this._(AdActions.markInteraction(connectedAd, 'impression', adSource))
    }
  }

  _markClickInteraction = () => {
    const { profile } = this.props.screenProps
    const { navigation } = this.props

    const adSource = navigation.getParam('adSource')
    const connectedAd = navigation.getParam('connectedAd')

    const actionType = 'click'

    if (!connectedAd || connectedAd === 'THIS_IS_NOT_AN_ID') {
      const connectedAdUid = navigation.getParam('connectedAdUid')

      this._(
        AdActions.markInteractionByUidAndEmail(
          connectedAdUid,
          profile.email,
          actionType,
          adSource
        )
      )
    } else {
      this._(AdActions.markInteraction(connectedAd, actionType, adSource))
    }
  }

  _onSubmit = async formData => {
    const { userPregnancy, navigation } = this.props
    const { id } = this.props.screenProps.profile

    const connectedAd = navigation.getParam('connectedAd')
    const adSource = navigation.getParam('adSource')

    const { pin, phoneNumber, numberOfChildren, email } = formData

    let connectedAdUid

    if (!connectedAd || connectedAd === 'THIS_IS_NOT_AN_ID') {
      connectedAdUid = navigation.getParam('connectedAdUid')
    } else if (connectedAd) {
      connectedAdUid = connectedAd.uid
    }

    try {
      const response = await fetch(
        `${CONTENT_SERVICE__API_BASE}/moderna/new-insurance/`,
        {
          method: 'POST',
          headers: ADS_FETCH_HEADERS,
          body: JSON.stringify({
            contentUid: connectedAdUid,
            id,
            email,
            dueDate: userPregnancy.dueDate,
            ssn: pin,
            phoneNumber,
            numberOfChildren,
            insuranceType: 'FREE',
            adSource,
          }),
        }
      )

      if (response.ok) {
        const json = await response.json()

        if (+json.code === 201) {
          if (connectedAd && connectedAd.type) {
            this._(AdActions.remove(connectedAd))
          }
        } else {
          this.setState({ modernaError: true, errorCode: json.code })
        }
      } else {
        this.setState({ modernaError: true })
      }
    } catch (err) {
      console.log(err)
      this.setState({ modernaError: true })
    }
  }

  _onCloseModal = () => this.setState({ modernaError: false })

  _onAfterSubmit = async () => {
    if (!this.state.modernaError) {
      const { navigation, activeCompetitionUid } = this.props
      const connectedTodoIds = navigation.getParam('connectedTodoIds', [])

      try {
        await Promise.all(
          connectedTodoIds.map(id =>
            this._(ChecklistActions.completeAdminTodo(id))
          )
        )
      } catch (err) {
        console.log(err)
      }

      if (activeCompetitionUid) {
        this._goTo('/competition-screen', {
          activeCompetitionUid: activeCompetitionUid.name,
        })
      } else {
        this._goBack()
      }
    }
  }
}
