import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249, 250, 252)',
  },

  body: {
    paddingHorizontal: moderateScale(22),
    marginTop: moderateScale(13, 4),
  },

  h1: {
    color: 'rgb(43, 56, 87)',
    fontSize: moderateScale(25),
    fontFamily: 'Now-Bold',
    marginVertical: moderateScale(30),
    textAlign: 'center',
  },

  h2: {
    color: 'rgb(43, 56, 87)',
    fontSize: moderateScale(20),
    fontFamily: 'Now-Black',
    marginVertical: moderateScale(40),
    textAlign: 'center',
  },

  p: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(24),
    color: 'rgb(59, 70, 85)',
  },

  link: {
    textDecorationLine: 'underline',
  },

  meta: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    lineHeight: moderateScale(22),
    color: 'rgb(35, 40, 46)',
  },

  metaItemsContainer: {
    borderTopColor: 'rgba(151, 151, 151, 0.29)',
    borderTopWidth: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
})

export default styles
