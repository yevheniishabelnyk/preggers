/*
 *
 * ModernaPregnancyInsuranceInfo screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { range } from 'lodash'
import { WebBrowser } from 'expo'

import { View, Text, ScrollView } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import MetaItem from '@/insurance/components/MetaItem'
import ModernaHeader from '@/insurance/components/ModernaHeader'

import styles from './styles'

import messages from './messages'

export default class ModernaPregnancyInsuranceInfo extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl
    const { navigation } = this.props

    const isLarge = navigation.getParam('isLarge', false)

    const headingRegular = formatMessage(messages.headingRegular)

    const headingLarge = formatMessage(messages.headingLarge)

    const bodyRegular = formatMessage(messages.bodyRegular)

    const bodyLarge = formatMessage(messages.bodyLarge) + ' '

    const heading = isLarge ? headingLarge : headingRegular

    const body = isLarge ? bodyLarge : bodyRegular

    return (
      <ScrollView style={styles.container}>
        {isLarge ? (
          <ModernaHeader
            image={require('assets/img/moderna-large.png')}
            goBack={this._goBack}
          />
        ) : (
          <ModernaHeader
            image={require('assets/img/moderna-free.png')}
            goBack={this._goBack}
          />
        )}

        <View style={styles.body}>
          <Text style={styles.h1}>{heading}</Text>

          <Text style={styles.p}>
            {body}
            {isLarge && (
              <Text style={styles.link} onPress={this._onReadMorePress}>
                {formatMessage(messages.here)}
              </Text>
            )}
          </Text>

          <Text style={styles.h2}>{formatMessage(messages.heading2)}:</Text>

          <View style={styles.metaItemsContainer}>
            {range(1, 13).map(i => (
              <MetaItem
                text={formatMessage(messages[`meta${Math.floor((i + 1) / 2)}`])}
                checked={i < 3 || !!((i + 1) % 2)}
                borderRightWidth={i % 2}
                heading={[headingRegular, headingLarge][i - 1]}
                key={i}
              />
            ))}
          </View>
        </View>
      </ScrollView>
    )
  }

  _onReadMorePress = () => {
    const { formatMessage } = this.context.intl

    WebBrowser.openBrowserAsync(formatMessage(messages.readMoreUrl))
  }
}
