import { defineMessages } from 'react-intl'

export default defineMessages({
  headingRegular: 'Maternity Insurance Free',
  headingLarge: 'Maternity Insurance Large',
  bodyRegular:
    'A maternity insurance plan provides security when you are pregnant. No other type of insurance pays the compensation offered by a maternity insurance plan. The insurance covers accidents, for example, and provides support for you and your partner should anything serious happen.\n\nIt covers the costs of any crisis assistance and death benefit payments, and is available in two options: a free plan, and a paid plan for additional cover.\n\nYou can sign up for a maternity insurance plan at any time during your pregnancy, but coverage will not start until week 22.',
  bodyLarge:
    'We offer two types of maternity insurance plans, Free and Large. We always recommend signing up for the paid plan, because it provides more extensive and better coverage than the free plan. The contents of each insurance plan are listed below.\n\nWhen you sign up for Maternity Insurance Large, you will also be eligible to sign up for a Child Insurance Medium Plan without requiring a health declaration. You can feel secure in the knowledge that a child insurance plan is guaranteed. You will also be able to deduct premiums of SEK 1,395 as a discount on your child insurance during the first 12 months. The offer to sign up for the child insurance plan will be sent to you at the end of your pregnancy (week 37). To be eligible for the discount, your invoice must be paid within 14 days of the birth.\n\nWhy you should sign up for child insurance early – read more',
  meta1:
    'Permanent disability (medical disability) due to accident, death benefit and crisis assistance',
  meta2: 'Hospitalisation for mother and child',
  meta3:
    'Permanent disability (medical disability) for the child due to illness',
  meta4: 'Complications during pregnancy and delivery',
  meta5: 'Compensation if the child is born with certain birth defects',
  meta6: 'Crisis assistance for postpartum depression, etc.',
  here: 'here!',
  heading2: 'This is included',
  readMoreUrl:
    'https://www.konsumenternas.se/forsakring/olika-forsakringar/om-barnforsakringar',
})
