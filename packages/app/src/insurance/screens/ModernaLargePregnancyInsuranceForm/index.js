/*
 *
 * ModernaLargePregnancyInsuranceForm screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

import { View, Text, Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ModernaHeader from '@/insurance/components/ModernaHeader'
import ModernaForm from '@/insurance/components/ModernaForm'
import ModernaUpgradeForm from '@/insurance/components/ModernaUpgradeForm'
import ModernaErrorModal from '@/insurance/components/ModernaErrorModal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { CONTENT_SERVICE__API_BASE } from 'config'
import { ADS_FETCH_HEADERS } from '@/app/redux/ads/constants'

import { getPregnancy, getActiveCompetitionUid } from '@/app/redux/selectors'

import * as AdActions from '@/app/redux/ads/actions'
import * as ChecklistActions from '@/app/database/actions/checklist'

import messages from './messages'
import styles from './styles'

@firebaseConnect(['competitions/activeCompetitions'])
@connect((state, props) => {
  const { profile } = props.screenProps
  let userPregnancy

  if (!profile.isEmpty) {
    userPregnancy = getPregnancy(state, profile.pregnancy)
  }

  return {
    activeCompetitionUid: getActiveCompetitionUid(state) || null,
    userPregnancy,
  }
})
export default class ModernaLargePregnancyInsuranceForm extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    modernaError: false,
    errorCode: false,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <KeyboardAwareScrollView style={styles.container}>
        <ModernaHeader
          image={require('assets/img/moderna-large.png')}
          goBack={this._goBack}
        />

        <View style={styles.body}>
          <Text style={styles.h1}>{formatMessage(messages.heading1)}</Text>

          <Text style={styles.h2}>{formatMessage(messages.heading2)}</Text>

          {[
            formatMessage(messages.meta1),
            formatMessage(messages.meta2),
            formatMessage(messages.meta3),
          ].map((text, i) => (
            <View key={i} style={styles.metaItemContainer}>
              <View style={styles.metaIconContainer}>
                <Image
                  source={require('assets/icons/checked-white.png')}
                  style={styles.metaIcon}
                />
              </View>

              <Text style={styles.meta}>{text}</Text>
            </View>
          ))}
        </View>

        {this._shouldSeeRegularForm() && (
          <ModernaForm
            navigation={this.props.navigation}
            onSubmit={this._onSubmit}
            onAfterSubmit={this._onAfterSubmit}
            defaultEmail={this.props.screenProps.profile.email}
            isLarge
            interactionAction={this._markClickInteraction}
          />
        )}

        {this._shouldSeeUpgradeForm() && (
          <ModernaUpgradeForm
            navigation={this.props.navigation}
            onSubmit={this._onSubmit}
            onAfterSubmit={this._onAfterSubmit}
            interactionAction={this._markClickInteraction}
          />
        )}

        <ModernaErrorModal
          visible={this.state.modernaError}
          onClose={this._onCloseModal}
          errorCode={this.state.errorCode}
        />
      </KeyboardAwareScrollView>
    )
  }

  componentDidMount() {
    const { navigation } = this.props
    const connectedAd = navigation.getParam('connectedAd')

    this._(AdActions.markInteraction(connectedAd, 'impression'))
  }

  _markClickInteraction = () => {
    const { navigation } = this.props
    const connectedAd = navigation.getParam('connectedAd')
    const adSource = navigation.getParam('adSource')

    if (connectedAd) {
      this._(AdActions.markInteraction(connectedAd, 'click', adSource))
    }
  }

  _onSubmit = async formData => {
    const { userPregnancy, navigation } = this.props

    const { id } = this.props.screenProps.profile

    const { pin, phoneNumber, numberOfChildren, email } = formData

    const connectedAd = navigation.getParam('connectedAd')

    const adSource = navigation.getParam('adSource')

    if (!connectedAd) {
      return null
    }

    let url = `${CONTENT_SERVICE__API_BASE}/moderna/new-insurance/`

    let requestBody = {
      contentUid: connectedAd.uid,
      id,
      email,
      dueDate: userPregnancy.dueDate,
      ssn: pin,
      phoneNumber,
      numberOfChildren,
      insuranceType: 'LARGE',
      adSource,
    }

    if (this._isUpgrading()) {
      url = `${CONTENT_SERVICE__API_BASE}/moderna/upgrade-insurance/`

      requestBody = {
        contentUid: connectedAd.uid,
        id,
      }
    }

    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: ADS_FETCH_HEADERS,
        body: JSON.stringify(requestBody),
      })

      if (response.ok) {
        const json = await response.json()

        if (+json.code === 201) {
          this._(AdActions.remove(connectedAd))
        } else {
          this.setState({ modernaError: true, errorCode: json.code })
        }
      } else {
        this.setState({ modernaError: true })
      }
    } catch (err) {
      console.log(err)

      this.setState({ modernaError: true })
    }
  }

  _onAfterSubmit = async () => {
    if (!this.state.modernaError) {
      const { navigation, activeCompetitionUid } = this.props

      const connectedTodoIds = navigation.getParam('connectedTodoIds', [])

      try {
        await Promise.all(
          connectedTodoIds.map(id =>
            this._(ChecklistActions.completeAdminTodo(id))
          )
        )
      } catch (err) {
        console.log(err)
      }

      if (activeCompetitionUid) {
        this._goTo('/competition-screen', {
          activeCompetitionUid: activeCompetitionUid.name,
        })
      } else {
        this._goBack()
      }
    }
  }

  _onCloseModal = () => this.setState({ modernaError: false })

  _isUpgrading = () => {
    const { navigation } = this.props
    const connectedAd = navigation.getParam('connectedAd')
    return connectedAd.modernaInsuranceCampaignType === 'UPGRADE'
  }

  _shouldSeeUpgradeForm = () => this._isUpgrading()

  _shouldSeeRegularForm = () => !this._isUpgrading()
}
