import { defineMessages } from 'react-intl'

export default defineMessages({
  heading1: 'Maternity Insurance Large',
  heading2: 'Three advantages of Moderna’s Maternity Insurance Large Plan:',
  meta1:
    'The insurance can be continued into a Child Insurance Medium Plan without requiring a health declaration.',
  meta2:
    'The only insurance company that pays compensation to women who suffer permanent disability (medical disability) due to childbirth.',
  meta3:
    'Compensation if the child is born with certain birth defects, such as severe heart disease, Down Syndrome or Cerebral Palsy.',
})
