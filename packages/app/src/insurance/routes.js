/*
 *
 * Insurance screen routes
 *
 */

import ModernaPregnancyInsuranceInfo from './screens/ModernaPregnancyInsuranceInfo'
import ModernaPregnancyInsuranceTerms from './screens/ModernaPregnancyInsuranceTerms'
import ModernaFreePregnancyInsuranceForm from './screens/ModernaFreePregnancyInsuranceForm'
import ModernaLargePregnancyInsuranceForm from './screens/ModernaLargePregnancyInsuranceForm'
import Competition from '@/competitions/screens/Competition'

export default {
  '/moderna-pregnancy-insurance-info': {
    screen: ModernaPregnancyInsuranceInfo,
  },

  '/moderna-pregnancy-insurance-terms': {
    screen: ModernaPregnancyInsuranceTerms,
  },

  '/moderna-free-pregnancy-insurance-form': {
    screen: ModernaFreePregnancyInsuranceForm,
  },

  '/competition-screen': {
    screen: Competition,
  },

  '/moderna-large-pregnancy-insurance-form': {
    screen: ModernaLargePregnancyInsuranceForm,
  },
}
