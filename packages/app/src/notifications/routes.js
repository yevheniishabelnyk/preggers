/*
 *
 * Notifications routes
 *
 */

import Notifications from './screens/Notifications'
import NotificationItem from './screens/NotificationItem'

export default {
  '/notifications': {
    screen: Notifications,
  },

  '/notification-item': {
    screen: NotificationItem,
  },
}
