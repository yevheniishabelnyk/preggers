/**
 *
 * NotificationItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import DisconnectedFrom from '@/connect/components/organisms/DisconnectedFrom'
import BaseScreen from '@/app/base/components/BaseScreen'

import messages from './messages'

import { firebaseConnect } from 'react-redux-firebase'
import { getNotificationItem } from '@/app/redux/selectors'

import styles from './styles'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    const notificationId = props.navigation.getParam('notificationId')

    if (notificationId) {
      return [`userOwned/notifications/${profile.id}/${notificationId}`]
    }
  }
})
@connect((state, props) => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    const notificationId = props.navigation.getParam('notificationId')

    const notification = getNotificationItem(state, profile.id, notificationId)

    return {
      notification,
      isFetching: notification === undefined,
    }
  }

  return {
    isFetching: true,
  }
})
export default class NotificationItem extends BaseScreen {
  static propTypes = {
    notification: PropTypes.object,
    isFetching: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { notification } = this.props
    const { formatMessage } = this.context.intl

    const not = this._renderNotification(notification)

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        scrollable
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        {not}
      </ViewWrapper>
    )
  }

  _renderNotification = notification => {
    if (!notification) {
      return null
    }

    switch (notification.notificationType) {
      case 'connection-cancelled':
        return (
          <DisconnectedFrom
            dispatch={this._}
            notification={notification}
            onOk={this._goBack}
          />
        )

      default:
        return null
    }
  }
}
