/**
 *
 * Notifications
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { View, Platform } from 'react-native'
import { connect } from 'react-redux'

import { Constants } from 'expo'
import { firebaseConnect } from 'react-redux-firebase'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import NotificationUpdateAvailable from '@/notifications/components/molecules/NotificationUpdateAvailable'

import isApplicationUpdateAvailable from '@/shared/utils/isApplicationUpdateAvailable'

import * as RouterActions from '@/app/redux/router/actions'
import {
  getUserInvitations,
  getNotifications,
  getUpdateAvailableNotification,
  getLatestAppVersion,
} from '@/app/redux/selectors'
import Notification from '@/notifications/components/molecules/Notification'

import messages from './messages'

import styles from './styles'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    return [
      {
        path: 'invitations',
        queryParams: ['orderByChild=toId', `equalTo=${profile.id}`],
      },
      `userOwned/notifications/${profile.id}`,
      'appVersion',
    ]
  }
})
@connect((state, props) => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    const invitations = getUserInvitations(state, profile.id)

    const notifications = [
      ...Object.values(getNotifications(state, profile.id) || {}),
      ...(invitations || []),
    ]
      .filter(n => !!n)
      .sort((a, b) => b.createdAt - a.createdAt)

    let latestVersion = getLatestAppVersion(state, Platform.OS)

    let updateAvailable = false
    let updateAvailableNotification

    if (
      isApplicationUpdateAvailable(Constants.manifest.version, latestVersion)
    ) {
      updateAvailableNotification = getUpdateAvailableNotification(state)

      updateAvailable = true
    }

    return {
      latestVersion,
      updateAvailable,
      updateAvailableNotification,
      notifications,
      isFetching: invitations === undefined,
    }
  }

  return {
    isFetching: true,
  }
})
export default class Notifications extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    notifications: [],
  }

  render() {
    const {
      notifications,
      updateAvailable,
      updateAvailableNotification,
      screenProps: { locale },
    } = this.props

    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        scrollable
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <View style={styles.feed}>
          {updateAvailable && (
            <NotificationUpdateAvailable
              notification={updateAvailableNotification}
              locale={locale}
              onPress={this._onAvailableUpdatePress}
            />
          )}

          {notifications.map(this._renderNotificationFeedItem)}
        </View>
      </ViewWrapper>
    )
  }

  _renderNotificationFeedItem = item => (
    <Notification key={item.id} notification={item} dispatch={this._} />
  )

  _onAvailableUpdatePress = link => this._(RouterActions.handleLink(link))
}
