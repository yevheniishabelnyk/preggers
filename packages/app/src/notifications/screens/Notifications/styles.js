import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(22.5, 3)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: gutter,
  },
  contentContainer: {
    alignItems: 'stretch',
  },

  header: {
    paddingHorizontal: moderateScale(22.5),
    backgroundColor: 'rgb(249,250,252)',
  },
})

export default styles
