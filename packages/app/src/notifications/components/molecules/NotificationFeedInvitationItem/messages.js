import { defineMessages } from 'react-intl'

export default defineMessages({
  wantsToConnect: 'wants to connect',
})
