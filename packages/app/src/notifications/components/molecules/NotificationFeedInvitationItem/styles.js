import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  name: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    letterSpacing: 0.7,
    color: 'rgb(55,55,64)',
  },
})

export default styles
