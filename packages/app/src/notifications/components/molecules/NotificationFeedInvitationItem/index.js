/**
 *
 * NotificationFeedInvitationItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'
import NotificationFeedItem from '@/notifications/components/atoms/NotificationFeedItem'
import { NavigationActions } from 'react-navigation'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class NotificationFeedInvitationItem extends React.Component {
  static propTypes = {
    notification: PropTypes.object,
    dispatch: PropTypes.func,
  }

  render() {
    const { notification } = this.props

    return (
      <NotificationFeedItem
        text={this._getText()}
        isRead={['read', 'accepted'].includes(notification.status)}
        actionRequired={notification.status !== 'accepted'}
        onPress={this._openInvitation}
      />
    )
  }

  _getText = () => {
    const { notification } = this.props

    return (
      <Text>
        <Text style={styles.name}>{notification.fromName} </Text>

        <FormattedMessage {...messages.wantsToConnect} style={styles.text} />
      </Text>
    )
  }

  _openInvitation = () => {
    const { notification, dispatch } = this.props

    if (notification.status === 'unread' || notification.status === 'read') {
      dispatch(
        NavigationActions.navigate({
          routeName: '/connect-accept-invitation',
          params: {
            invitationId: notification.id,
          },
        })
      )
    }
  }
}
