/**
 *
 * PregnancyUpdatedNotificationModal
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Modal, TouchableWithoutFeedback } from 'react-native'

import styles from './styles'

export default class PregnancyUpdatedNotificationModal extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static propTypes = {
    text: PropTypes.node,
    visible: PropTypes.bool,
    onClose: PropTypes.func,
  }

  render() {
    const { visible, onClose, text } = this.props

    return (
      <Modal
        animationType="fade"
        transparent
        visible={visible}
        onRequestClose={onClose}
      >
        <TouchableWithoutFeedback onPress={onClose}>
          <View style={styles.container}>
            <View style={styles.dialog}>
              <Text style={styles.text}>{text}</Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}
