import { StyleSheet, Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,.65)',
    alignItems: 'center',
    justifyContent: 'center',
    width,
    height,
  },

  dialog: {
    width: moderateScale(331),
    maxHeight: moderateScale(547),
    overflow: 'hidden',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: moderateScale(8),
    padding: moderateScale(40),
  },

  text: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    letterSpacing: 0.7,
    color: 'rgb(131,146,167)',
    textAlign: 'center',
    width: moderateScale(263),
  },
})

export default styles
