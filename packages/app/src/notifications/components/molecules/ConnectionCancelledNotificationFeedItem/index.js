/**
 *
 * ConnectionCancelledNotificationFeedItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'

import NotificationFeedItem from '@/notifications/components/atoms/NotificationFeedItem'

import * as NotificationActions from '@/app/database/actions/notifications'

import messages from './messages'

import styles from './styles'

export default class ConnectionCancelledNotificationFeedItem extends React.Component {
  static propTypes = {
    notification: PropTypes.object,
    dispatch: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { notification } = this.props

    return (
      <NotificationFeedItem
        text={this._getText()}
        isRead={['read', 'done'].includes(notification.status)}
        onPress={this._markAsRead}
      />
    )
  }

  _getText = () => {
    const { formatMessage } = this.context.intl

    const { notification } = this.props

    return (
      <Text>
        <Text style={styles.name}>{notification.payload.cancellerName} </Text>

        {formatMessage(messages.connectionCancelled)}
      </Text>
    )
  }

  _markAsRead = () => {
    const { dispatch, notification } = this.props

    if (notification.status === 'unread') {
      dispatch(NotificationActions.markAsRead(notification.id))
    }
  }
}
