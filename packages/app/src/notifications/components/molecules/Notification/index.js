/**
 *
 * NotificationFeedItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import ConnectionCancelledNotificationFeedItem from '@/notifications/components/molecules/ConnectionCancelledNotificationFeedItem'
import PregnancyUpdatedNotificationFeedItem from '@/notifications/components/molecules/PregnancyUpdatedNotificationFeedItem'
import PregnancyEndedNotificationFeedItem from '@/notifications/components/molecules/PregnancyEndedNotificationFeedItem'
import ChildIsBornNotificationFeedItem from '@/notifications/components/molecules/ChildIsBornNotificationFeedItem'
import NotificationFeedInvitationItem from '@/notifications/components/molecules/NotificationFeedInvitationItem'

const REQUIRED_INVITATION_KEYS = [
  'id',
  'status',
  'fromId',
  'fromEmail',
  'toId',
  'toEmail',
]

export default class NotificationFeedItem extends React.Component {
  static propTypes = {
    notification: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { notification } = this.props

    if (!notification) {
      return null
    }

    if (this._isActuallyInvitation(notification)) {
      return <NotificationFeedInvitationItem {...this.props} />
    }

    switch (notification.notificationType) {
      case 'connection-cancelled':
        return <ConnectionCancelledNotificationFeedItem {...this.props} />

      case 'child-updated':
        return <PregnancyUpdatedNotificationFeedItem {...this.props} />

      case 'child-is-born':
        return <ChildIsBornNotificationFeedItem {...this.props} />

      case 'pregnancy-ended':
      case 'pregnancy-deleted':
        return (
          <PregnancyEndedNotificationFeedItem
            {...this.props}
            type={notification.type}
          />
        )

      default:
        return null
    }
  }

  _isActuallyInvitation = item =>
    Object.keys(item).filter(key => REQUIRED_INVITATION_KEYS.includes(key))
      .length === REQUIRED_INVITATION_KEYS.length
}
