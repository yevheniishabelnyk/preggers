import { defineMessages } from 'react-intl'

export default defineMessages({
  endedPregnancy: 'has ended the pregnancy',
  deletedPregnancy: 'has deleted the pregnancy',
})
