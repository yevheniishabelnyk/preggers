/**
 *
 * ChildIsBornNotificationFeedItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import NotificationFeedItem from '@/notifications/components/atoms/NotificationFeedItem'

import * as NotificationActions from '@/app/database/actions/notifications'

import messages from './messages'

export default class ChildIsBornNotificationFeedItem extends React.Component {
  static propTypes = {
    notification: PropTypes.object,
    dispatch: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    const { notification } = this.props

    return (
      <NotificationFeedItem
        text={formatMessage(messages.childIsBorn)}
        isRead={['read', 'done'].includes(notification.status)}
        onPress={this._markAsRead}
      />
    )
  }

  _markAsRead = () => {
    const { dispatch, notification } = this.props

    if (notification.status === 'unread') {
      dispatch(NotificationActions.markAsRead(notification.id))
    }
  }
}
