import { defineMessages } from 'react-intl'

export default defineMessages({
  childIsBorn: 'Finally! The baby is born!',
})
