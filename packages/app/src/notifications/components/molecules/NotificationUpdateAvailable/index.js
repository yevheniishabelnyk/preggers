import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, Image, TouchableOpacity, Platform } from 'react-native'
import { isFunction } from 'lodash'
import styles from './styles'

export default class NotificationUpdateAvailable extends React.Component {
  static propTypes = {
    notification: PropTypes.object,
    onPress: PropTypes.func,
    locale: PropTypes.string,
  }

  render() {
    const { notification, locale } = this.props

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this._onNotificationPress}
        activeOpacity={0.95}
      >
        <View style={styles.redLine} />

        <Image style={styles.image} source={require('assets/icons/logo.png')} />

        <Text style={styles.text}>{notification.text[locale]}</Text>
      </TouchableOpacity>
    )
  }

  _onNotificationPress = () => {
    const { notification, onPress } = this.props

    if (isFunction(onPress)) {
      onPress(notification.link[Platform.OS])
    }
  }
}
