import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: moderateScale(60),
    width: moderateScale(330),
    borderRadius: moderateScale(8),
    backgroundColor: 'rgb(255, 255, 255)',
    marginBottom: moderateScale(8),
  },

  text: {
    position: 'absolute',
    left: moderateScale(65),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    color: 'rgb(55, 55, 64)',
  },

  image: {
    position: 'absolute',
    left: moderateScale(21),
    height: moderateScale(35),
    width: moderateScale(35),
  },

  redLine: {
    position: 'absolute',
    left: 0,
    height: moderateScale(60),
    width: moderateScale(4),
    backgroundColor: 'rgb(250,65,105)',
  },
})

export default styles
