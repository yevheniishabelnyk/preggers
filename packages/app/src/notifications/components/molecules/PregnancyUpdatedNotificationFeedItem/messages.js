import { defineMessages } from 'react-intl'

export default defineMessages({
  hasChanged__childName: 'has changed the nickname of the baby to {childName}',
  hasChanged__childGender:
    'has changed the gender of the baby to {childGender}',
  hasChanged__dueDate: 'has changed the due date to {dueDate}',
  hasChanged__childName_dueDate:
    'has changed the nickname to {childName} and the due date to {dueDate}',
  hasChanged__childGender_childName:
    'has changed the nickname to {childName} and the gender to {childGender}',
  hasChanged__childGender_dueDate:
    'has changed the due date to {dueDate} and the gender to {childGender}',
  hasChanged__childGender_childName_dueDate:
    'has changed the nickname to {childName}, the gender to {childGender}, and the due date to {dueDate}',
  gender__m: 'a boy',
  gender__f: 'a girl',
  gender__unknown: 'unknown',
})
