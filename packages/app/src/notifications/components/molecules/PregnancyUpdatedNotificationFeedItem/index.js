/**
 *
 * PregnancyUpdatedNotificationFeedItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, View } from 'react-native'
import NotificationFeedItem from '@/notifications/components/atoms/NotificationFeedItem'
import PregnancyUpdatedNotificationModal from '@/notifications/components/molecules/PregnancyUpdatedNotificationModal'

import * as NotificationActions from '@/app/database/actions/notifications'

import messages from './messages'

import styles from './styles'

export default class PregnancyUpdatedNotificationFeedItem extends React.Component {
  static propTypes = {
    notification: PropTypes.object,
    dispatch: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    modalVisible: false,
  }

  render() {
    const { notification } = this.props

    return (
      <View>
        <NotificationFeedItem
          text={this._getText()}
          isRead={['read', 'done'].includes(notification.status)}
          onPress={this._onPress}
        />

        <PregnancyUpdatedNotificationModal
          visible={this.state.modalVisible}
          onClose={this._hideModal}
          text={this._getText()}
        />
      </View>
    )
  }

  _getText = () => {
    const { formatMessage } = this.context.intl
    const { notification } = this.props
    const { payload } = notification

    if (payload) {
      const { updatedValues, changedByUser } = payload

      // Format for keys in messages.js:
      // hasChanged__key1_key2_key3
      let translationKey = 'hasChanged__'

      translationKey += Object.keys(updatedValues)
        .filter(key => ['childGender', 'childName', 'dueDate'].includes(key))
        .sort((a, b) => a.localeCompare(b))
        .join('_')

      const { childName, dueDate } = updatedValues

      let childGender = updatedValues.childGender

      if (!childGender) {
        childGender = 'unknown'
      }

      childGender = formatMessage(messages[`gender__${childGender}`])

      return (
        <Text>
          <Text style={styles.name}>{changedByUser} </Text>

          {formatMessage(messages[translationKey], {
            dueDate,
            childGender,
            childName,
          })}
        </Text>
      )
    }
  }

  _onPress = () => {
    this._markAsRead()

    this._showModal()
  }

  _showModal = () => this.setState({ modalVisible: true })

  _hideModal = () => this.setState({ modalVisible: false })

  _markAsRead = () => {
    const { dispatch, notification } = this.props

    if (notification.status === 'unread') {
      dispatch(NotificationActions.markAsRead(notification.id))
    }
  }
}
