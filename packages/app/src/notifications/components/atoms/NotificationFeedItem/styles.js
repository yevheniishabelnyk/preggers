import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const pictureWidth = moderateScale(35)
const containerHeight = moderateScale(60)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    height: containerHeight,
    borderRadius: moderateScale(8),
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    overflow: 'hidden',
    marginBottom: moderateScale(9),
  },

  line: {
    height: containerHeight,
    width: moderateScale(4),
    backgroundColor: '#ffffff',
  },

  unread: {
    backgroundColor: '#fa4169',
  },

  actionRequired: {
    backgroundColor: '#1791d0',
  },

  transparent: {
    backgroundColor: 'transparent',
  },

  photo: {
    width: pictureWidth,
    height: pictureWidth,
    borderRadius: pictureWidth / 2,
    marginRight: moderateScale(11),
    marginLeft: moderateScale(17),
  },

  name: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    letterSpacing: 0.7,
    color: 'rgb(55,55,64)',
  },

  text: {
    flex: 1,
    paddingRight: moderateScale(11),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    letterSpacing: 0.7,
    color: 'rgb(131,146,167)',
  },
})

export default styles
