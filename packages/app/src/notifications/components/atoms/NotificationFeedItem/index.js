/**
 *
 * NotificationFeedItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableOpacity } from 'react-native'
import defaultUser from 'assets/icons/user.png'

import styles from './styles'

export default class NotificationFeedItem extends React.Component {
  static propTypes = {
    image: PropTypes.string,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    isRead: PropTypes.bool,
    actionRequired: PropTypes.bool,
    onPress: PropTypes.func,
  }

  static defaultProps = {
    actionRequired: false,
  }

  render() {
    const { onPress, image, text, isRead, actionRequired } = this.props

    const lineStyle = [styles.line]

    if (!isRead) {
      lineStyle.push(styles.unread)
    }

    if (isRead && actionRequired) {
      lineStyle.push(styles.actionRequired)
    }

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <View style={[styles.transparent, lineStyle]} />

        <Image
          source={image ? { uri: image } : defaultUser}
          style={styles.photo}
        />

        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.text}>
          {text}
        </Text>
      </TouchableOpacity>
    )
  }
}
