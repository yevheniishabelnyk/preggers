/*
 *
 * Tools routes
 *
 */

import ToolsMenu from './screens/ToolsMenu'

export default {
  '/tools-menu': {
    screen: ToolsMenu,
  },
}
