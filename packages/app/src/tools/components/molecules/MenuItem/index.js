/**
 *
 * MenuItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity, Image, Text } from 'react-native'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class MenuItem extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    title: PropTypes.string,
    icon: PropTypes.number,
    disabled: PropTypes.bool,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    imageStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { title, onPress, style, icon, disabled, imageStyle } = this.props

    return (
      <View style={[styles.container, styles.shadow, style]}>
        <TouchableOpacity onPress={onPress} activeOpacity={disabled ? 1 : 0.8}>
          {disabled ? (
            <View style={[styles.disabledContainer, styles.disabled]}>
              <Text
                style={[styles.title, disabled ? styles.titleDisabled : null]}
              >
                {title}
              </Text>

              <FormattedMessage
                {...messages.comingSoon}
                style={styles.disabledText}
              />
            </View>
          ) : (
            <Text style={styles.title}>{title}</Text>
          )}
          <Image
            style={[
              styles.icon,
              disabled ? styles.iconDisabled : null,
              imageStyle,
            ]}
            source={icon}
          />
        </TouchableOpacity>
      </View>
    )
  }
}
