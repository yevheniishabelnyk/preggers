import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(255, 255, 255)',
    width: '48%',
    height: moderateScale(169, 0.8),
    borderRadius: 5,
    alignItems: 'center',
    marginBottom: moderateScale(20, resizeFactor),
  },

  title: {
    marginTop: moderateScale(11, resizeFactor),
    fontSize: moderateScale(16, resizeFactor),
    lineHeight: moderateScale(22, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(41, 53, 69)',
    textAlign: 'center',
    paddingHorizontal: moderateScale(5, resizeFactor),
  },

  titleDisabled: {
    fontSize: moderateScale(15, resizeFactor),
  },

  disabled: {
    opacity: 0.5,
  },

  disabledText: {
    fontSize: moderateScale(14, resizeFactor),
    fontFamily: 'Now-Regular',
    color: 'rgb(41, 53, 69)',
    textAlign: 'center',
  },

  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.13)',
    shadowRadius: 29,
    shadowOpacity: 1,
  },

  icon: {
    alignSelf: 'center',
    marginTop: moderateScale(10, 2.5),
    width: moderateScale(121, 0.8),
    height: moderateScale(121, 0.8),
  },

  iconDisabled: {
    width: moderateScale(94, 0.8),
    height: moderateScale(94, 0.8),
  },

  disabledContainer: {
    height: moderateScale(55, resizeFactor),
    width: moderateScale(145, resizeFactor),
  },
})

export default styles
