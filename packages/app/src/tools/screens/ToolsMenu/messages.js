import { defineMessages } from 'react-intl'

export default defineMessages({
  menuTitle: 'Tools',
  publicMenuTitle: 'Test our tools',
  foodMenuItemTitle: 'Food',
  timelineMenuItemTitle: 'Timeline',
  babyNamesMenuItemTitle: 'Baby names',
  kickCounterMenuItemTitle: 'Kick counter',
  photoBoothMenuItemTitle: 'Photo booth',
  contractionTimerMenuItemTitle: 'Contraction timer',
  exercisesMenuItemTitle: 'Exercises',
  knowledgeBankMenuItemTitle: 'Knowledge',
  createAccountButtonTitle: 'Create an account now!',
  createAccountText:
    'Want to use all our functions? Create an account, it’s free!',
  createAccountDescription:
    'In order to use all the functions in our app you need to create an account.',
})
