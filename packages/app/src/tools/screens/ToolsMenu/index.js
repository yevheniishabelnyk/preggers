/**
 *
 * Tools Menu
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, ScrollView } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import MenuItem from '@/tools/components/molecules/MenuItem'
import GoBackButton from '@/onboarding/components/GoBackButton'
import ActionButton from '@/settings/components/atoms/ActionButton'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class ToolsMenu extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.isPublicScreen =
      props.navigation.getParam('IS_PUBLIC') ||
      props.navigation.getParam('isOpenedWithDeepLink')
  }

  render() {
    const { formatMessage } = this.context.intl

    const screenTitle = this.isPublicScreen
      ? messages.publicMenuTitle
      : messages.menuTitle

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <FormattedMessage
            {...screenTitle}
            style={this.isPublicScreen ? styles.publicHeader : styles.header}
          />
          {this.isPublicScreen ? (
            <GoBackButton onPress={this._goBack} style={styles.goBackButton} />
          ) : null}
        </View>

        <ScrollView style={styles.listContainer}>
          <View style={styles.layout}>
            <MenuItem
              title={formatMessage(messages.babyNamesMenuItemTitle)}
              icon={require('assets/img/tools-baby-names.png')}
              onPress={this._babyNamesMenuItemPressed}
            />

            <MenuItem
              title={formatMessage(messages.foodMenuItemTitle)}
              icon={require('assets/img/tools-food.png')}
              onPress={this._foodMenuItemPressed}
            />

            {this.isPublicScreen ? (
              <MenuItem
                title={formatMessage(messages.knowledgeBankMenuItemTitle)}
                icon={require('assets/img/knowledgebank-helpfull-information.png')}
                onPress={this._knowledgeBankMenuItemPressed}
                imageStyle={styles.knowledgeBankItemIcon}
              />
            ) : null}

            {!this.isPublicScreen ? (
              <React.Fragment>
                <MenuItem
                  title={formatMessage(messages.photoBoothMenuItemTitle)}
                  icon={require('assets/img/photo-booth-disabled.png')}
                  disabled
                  // onPress={this._photoBoothMenuItemPressed}
                />

                <MenuItem
                  title={formatMessage(messages.contractionTimerMenuItemTitle)}
                  icon={require('assets/img/tools-contaction-timer-disabled.png')}
                  disabled
                />

                <MenuItem
                  title={formatMessage(messages.exercisesMenuItemTitle)}
                  icon={require('assets/img/exercises.png')}
                  disabled
                />

                <MenuItem
                  title={formatMessage(messages.kickCounterMenuItemTitle)}
                  icon={require('assets/img/tools-kick-counter-disabled.png')}
                  disabled
                />

                <MenuItem
                  title={formatMessage(messages.timelineMenuItemTitle)}
                  icon={require('assets/img/timeline.png')}
                  disabled
                />
              </React.Fragment>
            ) : null}

            {this.isPublicScreen ? (
              <View style={styles.createAccountContainer}>
                <React.Fragment>
                  <FormattedMessage
                    {...messages.createAccountText}
                    style={styles.createAccountText}
                  />

                  <FormattedMessage
                    {...messages.createAccountDescription}
                    style={styles.createAccountDescription}
                  />

                  <ActionButton
                    title={formatMessage(messages.createAccountButtonTitle)}
                    onPress={this._createAccountButtonPressed}
                    style={styles.createAccountButton}
                  />
                </React.Fragment>
              </View>
            ) : null}
          </View>
        </ScrollView>
      </View>
    )
  }

  _createAccountButtonPressed = () => this._goBack()

  _foodMenuItemPressed = () => this._goTo('/food-search')

  _babyNamesMenuItemPressed = () => this._goTo('/baby-names-menu')

  _knowledgeBankMenuItemPressed = () => this._goTo('/knowledge-search')

  _photoBoothMenuItemPressed = () => this._goTo('/poster-background')
}
