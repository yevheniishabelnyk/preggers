import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'
import isIPhoneX from '@/shared/utils/isIPhoneX'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  headerContainer: {
    position: 'absolute',
    alignItems: 'center',
    zIndex: 1,
    top: 0,
    width: '100%',
    paddingBottom: moderateScale(20),
  },

  listContainer: {
    position: 'absolute',
    top: isIPhoneX() ? moderateScale(95, 0.3) : moderateScale(75, 0.3),
    marginTop: moderateScale(25, 0.3),
    paddingTop: moderateScale(55, 0.3),
    paddingHorizontal: moderateScale(22.5, 0.3),
    flex: 1,
    height: '100%',
  },

  header: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, 0.6),
    color: 'rgb(59, 72, 89)',
    marginTop: moderateScale(53, 0),
    paddingBottom: moderateScale(50),
  },

  publicHeader: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, 0.6),
    color: 'rgb(59, 72, 89)',
    textAlign: 'center',
    marginTop: isIPhoneX() ? moderateScale(75, 0) : moderateScale(50, 0),
    paddingBottom: moderateScale(30),
  },

  knowledgeBankItemIcon: {
    width: moderateScale(100, 0.8),
    height: moderateScale(110, 0.8),
  },

  goBackButton: {
    backgroundColor: 'white',
    zIndex: 6,
    top: isIPhoneX() ? moderateScale(64, 0.3) : moderateScale(40, 0.3),
    left: moderateScale(22, 0.3),
    position: 'absolute',
  },

  layout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingBottom: moderateScale(200),
  },

  createAccountContainer: {
    marginTop: moderateScale(25),
  },

  createAccountText: {
    alignSelf: 'center',
    color: 'rgb(43,56,87)',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(27),
    letterSpacing: 0,
    fontFamily: 'Now-Bold',
    textAlign: 'center',
    marginTop: moderateScale(6),
    marginBottom: moderateScale(10),
  },

  createAccountDescription: {
    alignSelf: 'center',
    color: 'rgb(43,56,87)',
    fontSize: moderateScale(12),
    lineHeight: moderateScale(22),
    letterSpacing: 0,
    fontFamily: 'Now-Regular',
    textAlign: 'center',
    marginBottom: moderateScale(19),
    width: moderateScale(325, 0.8),
  },

  createAccountButton: {
    width: 300,
    alignSelf: 'center',
  },
})

export default styles
