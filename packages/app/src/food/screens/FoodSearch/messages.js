import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Food',
  searchFieldTitle: 'Search',
  goodFoodIndicatorTitle: 'Okay to eat',
  warningFoodIndicatorTitle: 'Be careful, read more',
  avoidFoordIndicatorTitle: 'Avoid',
  recommendationsDropDownTitle: 'General recommendations',
})
