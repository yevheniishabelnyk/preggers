/**
 *
 * FoodSearch
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import SearchField from '@/forms/components/molecules/SearchField'
import DropDown from '@/shared/components/DropDown'
import Indicator from '@/food/components/atoms/Indicator'

import * as firebase from 'firebase'

import * as RouterActions from '@/app/redux/router/actions'
import * as ProfileActions from '@/app/redux/profile/actions'

import { keys, values } from 'lodash'

import Fuse from 'fuse.js'

import messages from './messages'

import styles from './styles'

export default class FoodSearch extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.fuseOptions = {
      threshold: 0.3,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ['name', 'category'],
      id: 'id',
    }

    this.state = {
      searchQuery: '',
      results: {},
      categories: {},
    }
  }

  render() {
    const { formatMessage } = this.context.intl

    const content = this.state.searchQuery
      ? this.state.results
      : this.state.categories
    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._customGoBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <SearchField
          placeholder={formatMessage(messages.searchFieldTitle)}
          value={this.state.searchQuery}
          style={styles.search}
          onChange={this._search}
          returnKeyType="done"
        />

        {!this.state.searchQuery ? (
          <View>
            <Indicator
              ok
              title={formatMessage(messages.goodFoodIndicatorTitle)}
              style={styles.firstIndicator}
            />

            <Indicator
              warning
              title={formatMessage(messages.warningFoodIndicatorTitle)}
              style={styles.indicator}
            />

            <Indicator
              danger
              title={formatMessage(messages.avoidFoordIndicatorTitle)}
              style={styles.indicator}
            />

            <DropDown
              style={styles.dropDownGeneralRecomendations}
              title={formatMessage(messages.recommendationsDropDownTitle)}
              content={this.state.generalRecommendations}
              onLinkPress={this._linkPressed}
            />
          </View>
        ) : null}

        {keys(content).map(category => {
          return (
            <View key={category}>
              <Text style={styles.categoryTitle}>{category}</Text>

              {content[category].map(food => (
                <DropDown
                  key={food.id}
                  style={styles.marginTop}
                  title={food.name}
                  indicator={food.usefulness}
                  content={food.description}
                  onLinkPress={this._linkPressed}
                />
              ))}
            </View>
          )
        })}
      </ViewWrapper>
    )
  }

  async componentWillMount() {
    const { locale } = this.props.screenProps

    try {
      const [
        foodsSnap,
        foodCategoriesSnap,
        foodsGeneralRecommendationsSnap,
      ] = await Promise.all([
        firebase
          .database()
          .ref('/content/foods')
          .once('value'),

        firebase
          .database()
          .ref('/content/foodCategories')
          .once('value'),

        firebase
          .database()
          .ref('/content/foodsGeneralRecommendations')
          .once('value'),
      ])

      const foodCategoriesVal = foodCategoriesSnap.val()
      const foodsVal = foodsSnap.val()
      const foodsGeneralRecommendations = foodsGeneralRecommendationsSnap.val()

      const { categories, foods } = this._convertData(
        foodsVal,
        foodCategoriesVal,
        locale
      )

      this.setState({
        categories,
        foods,
        generalRecommendations:
          foodsGeneralRecommendations &&
          foodsGeneralRecommendations.text[locale]
            ? foodsGeneralRecommendations.text[locale]
            : '',
      })

      this.fuse = new Fuse(foods, this.fuseOptions)
    } catch (err) {
      console.info('err: ', err)
    }
  }

  _customGoBack = () => {
    this._(ProfileActions.maybeAskUserToRateApp())

    this._goBack()
  }

  _convertData(foods, foodCategories, locale) {
    const foodsArray = values(foods).map(item =>
      Object.assign({}, item, {
        name: item.name[locale] || '',
        description: item.description ? item.description[locale] : '',
        category: foodCategories[item.category]
          ? foodCategories[item.category].name[locale]
          : '',
      })
    )

    const categories = Object.keys(foodCategories).reduce((out, categoryId) => {
      const foodsByCategory = foodsArray.filter(
        item => item.category === foodCategories[categoryId].name[locale]
      )

      if (foodsByCategory.length) {
        out[foodCategories[categoryId].name[locale]] = foodsByCategory
      }

      return out
    }, {})

    return {
      foods: foodsArray,
      categories,
    }
  }

  _search = query => {
    this.setState({ searchQuery: query })

    if (this.fuse) {
      const resultsData = this.fuse
        .search(query)
        .map(id => this.state.foods.find(item => item.id === id))

      const results = resultsData
        .map(item => item.category)
        .reduce((out, category) => {
          out[category] = resultsData.filter(item => item.category === category)

          return out
        }, {})

      this.setState({ results })
    }
  }

  _linkPressed = link => this._(RouterActions.handleLink(link))
}
