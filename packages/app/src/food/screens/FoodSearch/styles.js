import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.5

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainerStyle: {
    paddingBottom: moderateScale(130, resizeFactor),
    alignItems: 'stretch',
    paddingHorizontal: moderateScale(20, 4),
  },

  header: {
    paddingHorizontal: moderateScale(22.5),
    backgroundColor: 'rgb(249,250,252)',
  },

  categoryTitle: {
    fontFamily: 'Now-Bold',
    color: 'rgb(41,53,69)',
    fontSize: moderateScale(26, 0.2),
    textAlign: 'left',
    marginTop: moderateScale(25, 0.7),
  },

  marginTop: {
    marginTop: moderateScale(13, resizeFactor),
  },

  indicator: {
    marginTop: moderateScale(8, resizeFactor),
  },

  firstIndicator: {
    marginTop: moderateScale(23, resizeFactor),
  },

  dropDownGeneralRecomendations: {
    marginTop: moderateScale(20, resizeFactor),
  },
})

export default styles
