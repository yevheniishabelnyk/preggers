import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    height: moderateScale(40, resizeFactor),
    backgroundColor: 'rgb(64,224,190)',
    borderRadius: 8,
    alignSelf: 'stretch',
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: moderateScale(11, resizeFactor),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: 'white',
    lineHeight: moderateScale(20, resizeFactor),
    backgroundColor: 'transparent',
  },

  iconWrapper: {
    width: moderateScale(15, resizeFactor),
    height: moderateScale(15, resizeFactor),
    marginRight: moderateScale(10, resizeFactor),
  },

  icon: {
    width: null,
    height: null,
    flex: 1,
  },

  bgGreen: {
    backgroundColor: 'rgb(64,224,190)',
  },

  bgOrange: {
    backgroundColor: 'rgb(255,201,60)',
  },

  bgRed: {
    backgroundColor: 'rgb(242,83,117)',
  },
})

export default styles
