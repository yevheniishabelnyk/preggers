/**
 *
 * Indicator
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'

import okIcon from 'assets/icons/checked-white.png'
import warningIcon from 'assets/icons/warning.png'
import dangerIcon from 'assets/icons/white-cross.png'

import styles from './styles'

export default class Indicator extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    ok: PropTypes.bool,
    warning: PropTypes.bool,
    danger: PropTypes.bool,
    style: PropTypes.any,
  }

  render() {
    const { title, warning, danger, style } = this.props

    let bgColor = styles.bgGreen
    let icon = okIcon

    if (warning) {
      icon = warningIcon
      bgColor = styles.bgOrange
    } else if (danger) {
      icon = dangerIcon
      bgColor = styles.bgRed
    }

    return (
      <View style={[styles.container, bgColor, style]}>
        <View style={styles.iconWrapper}>
          <Image style={styles.icon} source={icon} resizeMode="contain" />
        </View>

        <Text style={styles.title}>{title}</Text>
      </View>
    )
  }
}
