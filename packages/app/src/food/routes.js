/*
 *
 * Food screens
 *
 */

import FoodSearch from './screens/FoodSearch'

export default {
  '/food-search': {
    screen: FoodSearch,
  },
}
