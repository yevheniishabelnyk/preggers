import { StyleSheet, Dimensions } from 'react-native'

import { moderateScale } from '@/app/scaling'

export const { width, height } = Dimensions.get('window')

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 2.5)

const gutterTextContainer = moderateScale(22, 2.5)

export const imageMaxWidth = width - 4 * gutterTextContainer

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  header: {
    paddingHorizontal: gutter,
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    paddingBottom: gutter * 3,
    alignItems: 'stretch',
    position: 'relative',
  },

  loaderContainer: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 2,
    backgroundColor: 'rgb(249,250,252)',
    overflow: 'hidden',
  },

  loader: {
    position: 'absolute',
    top: 0,
    left: 0,
    width,
    height,
  },

  textContainer: {
    paddingHorizontal: gutterTextContainer,
  },

  textWrapper: {
    marginTop: moderateScale(25, resizeFactor),
    paddingTop: moderateScale(20, resizeFactor),
    paddingBottom: moderateScale(25, resizeFactor),
    paddingHorizontal: moderateScale(20, 2.5),
    backgroundColor: 'white',
    borderRadius: 8,
    alignItems: 'center',
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(26, resizeFactor),
    color: 'rgb(41, 53, 69)',
    marginBottom: moderateScale(8, resizeFactor),
  },

  ingress: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18, resizeFactor),
    lineHeight: moderateScale(27, resizeFactor),
    color: 'rgb(41, 53, 69)',
    marginBottom: moderateScale(8, resizeFactor),
    width: moderateScale(220, resizeFactor),
    textAlign: 'center',
  },

  downloadButton: {
    marginTop: moderateScale(28, resizeFactor),
  },

  mainPoster: {
    width: width,
    height: moderateScale(249, 0.6),
  },
})

export const tagStyles = {
  p: {
    color: 'rgb(131, 146, 167)',
    lineHeight: moderateScale(24, resizeFactor),
    textAlign: 'center',
    fontSize: moderateScale(14, resizeFactor),
  },

  img: {
    marginVertical: moderateScale(10, resizeFactor),
  },
}

export default styles
