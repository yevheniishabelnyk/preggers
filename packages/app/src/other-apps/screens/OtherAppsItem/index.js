/**
 *
 * OtherAppsItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Text } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import ActionButton from '@/settings/components/atoms/ActionButton'
import Loader from '@/shared/components/Loader'

import * as RouterActions from '@/app/redux/router/actions'

import { getOtherApp } from '@/app/redux/selectors'
import { ExpoImage } from '@/vendor/cache-manager'

import AppLink from 'react-native-app-link'

import HTML from 'react-native-render-html'

import { firebaseConnect } from 'react-redux-firebase'

import styles, { tagStyles, imageMaxWidth } from './styles'

@firebaseConnect(props => {
  const appId = props.navigation.getParam('appId')

  if (appId) {
    return [`content/otherApps/${appId}`]
  }
})
@connect((state, props) => {
  const appId = props.navigation.getParam('appId')

  let app

  if (appId) {
    app = getOtherApp(state, appId)
  }

  return {
    app,
    isFetching: app === undefined,
  }
})
export default class OtherAppsItem extends BaseScreen {
  static propTypes = {
    app: PropTypes.object,
    screenProps: PropTypes.shape({
      locale: PropTypes.string,
    }),
  }

  render() {
    const { app, isFetching } = this.props
    const { locale } = this.props.screenProps

    if (isFetching) {
      return <Loader background="#f9fafc" />
    }

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        deviceIndicators="rgb(249,250,252)"
      >
        <GoBackButton onPress={this._goBack} />

        {app.image ? (
          <ExpoImage source={app.image} style={styles.mainPoster} cacheImage />
        ) : null}

        <View style={styles.textContainer}>
          <View style={styles.textWrapper}>
            {app.title ? (
              <Text style={styles.title}>{app.title[locale]}</Text>
            ) : null}

            {app.ingress ? (
              <Text style={styles.ingress}>{app.ingress[locale]}</Text>
            ) : null}

            {app.text ? (
              <HTML
                html={app.text[locale].replace(
                  new RegExp('<li[^>]*>([\\s\\S]*?)<\\/li>', 'g'),
                  '<li><p>$1</p></li>'
                )}
                // eslint-disable-next-line react/jsx-no-bind
                customWrapper={RNContent => (
                  <View style={styles.contentConteiner}>{RNContent}</View>
                )}
                onLinkPress={this._linkPressed}
                tagsStyles={tagStyles}
                imagesMaxWidth={imageMaxWidth}
                renderers={{
                  ol: (
                    htmlAttribs,
                    children,
                    convertedCSSStyles,
                    passProps
                  ) => {
                    const content = React.Children.map(
                      children,
                      (child, index) => {
                        return (
                          <View style={styles.olLiStyle}>
                            <Text style={styles.olLiPrefix}>{index + 1}.</Text>
                            {child}
                          </View>
                        )
                      }
                    )

                    return <View key={passProps.key}>{content}</View>
                  },

                  ul: (
                    htmlAttribs,
                    children,
                    convertedCSSStyles,
                    passProps
                  ) => {
                    const content = React.Children.map(children, child => {
                      return (
                        <View style={styles.ulLiStyle}>
                          <View style={styles.ulLiPrefix} />
                          {child}
                        </View>
                      )
                    })

                    return <View key={passProps.key}>{content}</View>
                  },
                }}
              />
            ) : null}

            {app.appStoreId || app.buttonUrl || app.playStoreId ? (
              <ActionButton
                title={app.buttonText[locale]}
                onPress={this._buttonPressed}
                style={styles.downloadButton}
                buttonColor={app.buttonColor}
              />
            ) : null}
          </View>
        </View>
      </ViewWrapper>
    )
  }

  _linkPressed = (evt, href) => this._(RouterActions.handleLink(href))

  _buttonPressed = () => {
    const { app } = this.props

    if (app.appStoreId) {
      AppLink.openInStore(app.appStoreId, app.playStoreId)
        .then(() => {
          // do stuff
        })
        .catch(err => {
          console.log(err)
        })
    } else if (app.buttonUrl) {
      this._(RouterActions.handleLink(app.buttonUrl))
    }
  }
}
