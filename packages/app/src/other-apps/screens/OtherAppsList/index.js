/*
 *
 * OtherAppsList
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View } from 'react-native'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import GridItem from '@/other-apps/components/atoms/GridItem'
import ListItem from '@/other-apps/components/atoms/ListItem'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { firebaseConnect } from 'react-redux-firebase'

import { getOtherApps } from '@/app/redux/selectors'

import { compact } from 'lodash'

import styles from './styles'

@firebaseConnect(['content/otherApps'])
@connect(state => {
  const apps = getOtherApps(state) || {}

  const gridApps = compact(
    Object.keys(apps).map(key => {
      if (apps[key].location === 'grid') {
        return apps[key]
      }
    })
  )

  const listApps = compact(
    Object.keys(apps).map(key => {
      if (apps[key].location === 'list') {
        return apps[key]
      }
    })
  )

  return {
    gridApps,
    listApps,
  }
})
export default class OtherAppsList extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
    gridApps: PropTypes.object,
    listApps: PropTypes.object,
  }

  render() {
    const { gridApps, listApps } = this.props

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboard
        scrollable
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <FormattedMessage
          {...messages.headerTitle}
          style={styles.headerTitle}
        />

        <View style={styles.gridWrapper}>
          <FormattedMessage {...messages.gridTitle} style={styles.title} />

          <View style={styles.gridItemsWrapper}>
            {gridApps.map(app => (
              <GridItem
                key={app.id}
                onPress={this._appPressed}
                appId={app.id}
                logo={app.logo}
              />
            ))}
          </View>
        </View>

        <View style={styles.listWrapper}>
          <FormattedMessage {...messages.listTitle} style={styles.title} />

          <View style={styles.listItemsWrapper}>
            {listApps.map(app => (
              <ListItem
                key={app.id}
                style={styles.listItem}
                onPress={this._appPressed}
                appId={app.id}
                logo={app.logo}
              />
            ))}
          </View>
        </View>
      </ViewWrapper>
    )
  }

  _appPressed = appId => this._goTo('/other-apps-item', { appId })
}
