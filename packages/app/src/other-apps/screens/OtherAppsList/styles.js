import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 2.5)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 252)',
    paddingBottom: moderateScale(24, resizeFactor),
  },

  contentContainer: {
    paddingHorizontal: gutter,
  },

  headerTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, resizeFactor),
    color: 'rgb(59, 72, 89)',
    marginTop: moderateScale(33, resizeFactor),
    textAlign: 'center',
  },

  title: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(26, resizeFactor),
    color: 'rgb(41, 53, 69)',
    marginTop: moderateScale(30, resizeFactor),
    marginBottom: moderateScale(10, resizeFactor),
  },

  gridItemsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },

  listItem: {
    marginBottom: moderateScale(13, resizeFactor),
  },
})

export default styles
