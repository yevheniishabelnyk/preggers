import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Other useful apps & services',
  gridTitle: 'Our apps & services',
  listTitle: 'Other useful apps',
})
