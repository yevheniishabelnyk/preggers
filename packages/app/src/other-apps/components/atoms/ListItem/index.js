/**
 *
 * ListItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity, Image } from 'react-native'
import { ExpoImage } from '@/vendor/cache-manager'

import { isFunction } from 'lodash'

import styles, { imageMaxWidth } from './styles'

export default class ListItem extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    appId: PropTypes.string,
    logo: PropTypes.object,
  }

  state = {
    imageWidth: 0,
    imageHeight: 0,
  }

  render() {
    const { style, logo } = this.props

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={this._itemPressed}
        activeOpacity={0.95}
      >
        <View style={styles.content}>
          {logo ? (
            <ExpoImage
              source={logo}
              style={[
                styles.logo,
                {
                  width: this.state.imageWidth,
                  height: this.state.imageHeight,
                },
              ]}
              cacheImage
            />
          ) : null}

          <View style={styles.iconWrapper}>
            <Image
              source={require('assets/icons/arrow-left-thin.png')}
              style={[styles.icon, styles.rotate]}
              resizeMode="contain"
            />
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  componentDidMount() {
    if (this.props.logo) {
      this._appLoaded(this.props.logo)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.logo && nextProps.logo) {
      this._appLoaded(nextProps.logo)
    }

    if (
      this.props.logo &&
      nextProps.logo &&
      this.props.logo !== nextProps.logo
    ) {
      this._calculateImageSize(nextProps.logo, imageMaxWidth).then(
        ({ width, height }) =>
          this.setState({
            imageWidth: width,
            imageHeight: height,
          })
      )
    }
  }

  _appLoaded = async logo => {
    if (logo) {
      const { width, height } = await this._calculateImageSize(
        logo,
        imageMaxWidth
      )

      this.setState({
        imageWidth: width,
        imageHeight: height,
      })
    }
  }

  _calculateImageSize = (image, imageMaxWidth) =>
    new Promise(resolve => {
      if (image) {
        Image.getSize(
          image.uri,
          (srcWidth, srcHeight) => {
            const ratio = imageMaxWidth / srcWidth

            resolve({
              width: srcWidth * ratio,
              height: srcHeight * ratio,
            })
          },
          error => {
            console.log('error:', error)

            resolve({ width: 0, height: 0 })
          }
        )
      } else {
        resolve({ width: 0, height: 0 })
      }
    })

  _itemPressed = () => {
    const { onPress, appId } = this.props

    if (isFunction(onPress)) {
      onPress(appId)
    }
  }
}
