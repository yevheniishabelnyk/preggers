import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

export const imageMaxWidth = moderateScale(90, resizeFactor)

const styles = StyleSheet.create({
  container: {
    width,
    height: moderateScale(60, resizeFactor),
    backgroundColor: 'white',
    paddingRight: moderateScale(60, 1),
    borderRadius: 8,
  },

  content: {
    height: moderateScale(60, resizeFactor),
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: moderateScale(15, resizeFactor),
  },

  iconWrapper: {
    width: moderateScale(23, resizeFactor),
    height: moderateScale(23, resizeFactor),
    backgroundColor: 'transparent',
    position: 'absolute',
    right: 0,
  },

  icon: {
    backgroundColor: 'transparent',
    flex: 1,
    width: null,
    height: null,
  },

  rotate: {
    transform: [{ rotate: '180deg' }],
  },
})

export default styles
