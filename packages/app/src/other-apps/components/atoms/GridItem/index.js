/*
 *
 * GridItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'
import { ExpoImage } from '@/vendor/cache-manager'

import { isFunction } from 'lodash'

import styles, { imageMaxWidth } from './styles'

export default class GridItem extends React.Component {
  static propTypes = {
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    onPress: PropTypes.func,
    appId: PropTypes.string,
    logo: PropTypes.object,
  }

  state = {
    imageWidth: 0,
    imageHeight: 0,
  }

  render() {
    const { style, logo } = this.props

    return (
      <TouchableOpacity
        style={[styles.button, style]}
        onPress={this._itemPressed}
        activeOpacity={0.95}
      >
        {logo ? (
          <ExpoImage
            source={logo}
            style={[
              styles.logo,
              {
                width: this.state.imageWidth,
                height: this.state.imageHeight,
              },
            ]}
            cacheImage
          />
        ) : null}
      </TouchableOpacity>
    )
  }

  componentDidMount() {
    if (this.props.logo) {
      this._appLoaded(this.props.logo)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.logo && nextProps.logo) {
      this._appLoaded(nextProps.logo)
    }

    if (
      this.props.logo &&
      nextProps.logo &&
      this.props.logo !== nextProps.logo
    ) {
      this._calculateImageSize(nextProps.logo, imageMaxWidth).then(
        ({ width, height }) =>
          this.setState({
            imageWidth: width,
            imageHeight: height,
          })
      )
    }
  }

  _appLoaded = async logo => {
    if (logo) {
      const { width, height } = await this._calculateImageSize(
        logo,
        imageMaxWidth
      )

      this.setState({
        imageWidth: width,
        imageHeight: height,
      })
    }
  }

  _calculateImageSize = (image, imageMaxWidth) =>
    new Promise(resolve => {
      if (image) {
        Image.getSize(
          image.uri,
          (srcWidth, srcHeight) => {
            const ratio = imageMaxWidth / srcWidth

            resolve({
              width: srcWidth * ratio,
              height: srcHeight * ratio,
            })
          },
          error => {
            console.log('error:', error)

            resolve({ width: 0, height: 0 })
          }
        )
      } else {
        resolve({ width: 0, height: 0 })
      }
    })

  _itemPressed = () => {
    const { onPress, appId } = this.props

    if (isFunction(onPress)) {
      onPress(appId)
    }
  }
}
