import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

export const imageMaxWidth = moderateScale(70, resizeFactor)

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'white',
    width: moderateScale(150, resizeFactor),
    height: moderateScale(132, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: moderateScale(10, resizeFactor),
    borderRadius: 8,
  },
})

export default styles
