/*
 *
 * Other apps routes
 *
 */

import OtherAppsList from './screens/OtherAppsList'
import OtherAppsItem from './screens/OtherAppsItem'

export default {
  '/other-apps-list': {
    screen: OtherAppsList,
  },

  '/other-apps-item': {
    screen: OtherAppsItem,
  },
}
