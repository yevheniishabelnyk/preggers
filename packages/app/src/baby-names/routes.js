/*
 *
 * BabyNames routes
 *
 */

import AddBabyname from './screens/AddBabyname'
import CountryBabynames from './screens/CountryBabynames'
import BabynamesMenu from './screens/BabynamesMenu'
import FavoritesNavigator from './navigators/FavoritesNavigator'

export default {
  '/baby-names-add': {
    screen: AddBabyname,
  },

  '/baby-names-country': {
    screen: CountryBabynames,
  },

  '/baby-names-menu': {
    screen: BabynamesMenu,
  },

  '/~baby-names-favorites': {
    screen: FavoritesNavigator,
  },
}
