import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249,250,252)',
    alignItems: 'center',
    paddingTop: 18, // device indicators
  },

  withTabs: {
    paddingBottom: moderateScale(25), // tab shadow radius
  },

  header: {
    paddingHorizontal: moderateScale(22.5, 0.3),
    backgroundColor: 'rgb(249,250,252)',
  },

  tabs: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    paddingHorizontal: moderateScale(22.5, 3),
  },

  tab: {
    width: moderateScale(157, 0.7),
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(18),
    height: moderateScale(37),
    shadowRadius: 12,
    shadowColor: 'black',
    shadowOpacity: 0.05,
    shadowOffset: { height: 8, width: 0 },
  },

  activeTab: {
    backgroundColor: 'rgb(250,65,105)',
  },

  tabTitle: {
    color: 'black',
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(13),
    textAlign: 'center',
  },

  activeTabTitle: {
    color: 'white',
  },
})

export default styles
