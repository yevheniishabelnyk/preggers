/**
 *
 * FavoritesTabBar
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, TouchableOpacity } from 'react-native'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

import { getConnection } from '@/app/redux/selectors'

import { firebaseConnect } from 'react-redux-firebase'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  if (!profile.isEmpty && profile.connection) {
    return [`connections/${profile.connection}/shareBabyNames`]
  }
})
@connect((state, props) => {
  const { profile } = props.screenProps

  if (!profile.isEmpty && profile.connection) {
    const userConnection = getConnection(state, profile.connection)

    if (userConnection && userConnection.shareBabyNames) {
      return {
        shareBabyNames: true,
      }
    }
  }

  return {
    shareBabyNames: false,
  }
})
export default class FavoritesTabBar extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
    shareBabyNames: PropTypes.bool,
    screenProps: PropTypes.shape({
      profile: PropTypes.object,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { shareBabyNames } = this.props
    const { formatMessage } = this.context.intl

    const activeTabIndex = this.props.navigation.state.index

    return (
      <View style={[styles.container, shareBabyNames ? styles.withTabs : null]}>
        <SettingsHeader
          title={formatMessage(messages.headerTitle)}
          onBackButtonPress={this._goBack}
          style={styles.header}
        />

        {shareBabyNames ? (
          <View style={styles.tabs}>
            <TouchableOpacity
              style={[
                styles.tab,
                activeTabIndex === 0 ? styles.activeTab : null,
              ]}
              onPress={this._myFavoriteTabPressed}
              activeOpacity={0.95}
            >
              <FormattedMessage
                {...messages.myFavorites}
                style={[
                  styles.tabTitle,
                  activeTabIndex === 0 ? styles.activeTabTitle : null,
                ]}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                styles.tab,
                activeTabIndex === 1 ? styles.activeTab : null,
              ]}
              onPress={this._ourFavoriteTabPressed}
              activeOpacity={0.95}
            >
              <FormattedMessage
                {...messages.ourFavorites}
                style={[
                  styles.tabTitle,
                  activeTabIndex === 1 ? styles.activeTabTitle : null,
                ]}
              />
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.shareBabyNames &&
      !nextProps.shareBabyNames &&
      this.props.navigation.state.index === 1
    ) {
      this.props.navigation.navigate('/my')
    }
  }

  _myFavoriteTabPressed = () => {
    const { navigation } = this.props

    const activeTabIndex = navigation.state.index

    if (activeTabIndex !== 0) {
      navigation.navigate('/my')
    }
  }

  _ourFavoriteTabPressed = () => {
    const { navigation } = this.props

    const activeTabIndex = navigation.state.index

    if (activeTabIndex !== 1) {
      navigation.navigate('/our')
    }
  }

  _goBack = () => this.props.navigation.goBack()
}
