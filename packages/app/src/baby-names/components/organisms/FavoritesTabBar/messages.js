import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Favorites',
  myFavorites: 'My favorites',
  ourFavorites: 'Our favorites',
})
