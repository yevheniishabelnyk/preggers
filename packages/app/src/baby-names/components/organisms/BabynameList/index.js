/**
 *
 * BabynameList
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import BabynameListItem from '@/baby-names/components/molecules/BabynameListItem'

import SwedenFlag from 'assets/icons/sweden-flag.png'
import DenmarkFlag from 'assets/icons/denmark-flag.png'
import UKFlag from 'assets/icons/uk-flag.png'
import USAFlag from 'assets/icons/usa-flag.png'
import NorwayFlag from 'assets/icons/norway-flag.png'
import FinlandFlag from 'assets/icons/finland-flag.png'
import defaultUser from 'assets/icons/user.png'

import { has } from 'lodash'

import styles from './styles'

export default class BabynameList extends React.Component {
  static propTypes = {
    babynames: PropTypes.array,
    onRemove: PropTypes.func,
    onAddStar: PropTypes.func,
    onRemoveStar: PropTypes.func,
    profile: PropTypes.object.isRequired,
    isCommonList: PropTypes.bool,
    withCountryIcon: PropTypes.bool,
    partnerName: PropTypes.string,
    partnerImage: PropTypes.object,
    style: PropTypes.any,
  }

  render() {
    const { isCommonList } = this.props

    if (isCommonList) {
      return this._renderCommonBabynameList()
    }

    return this._renderSimpleBabynameList()
  }

  _renderSimpleBabynameList = () => {
    const {
      babynames = [],
      style,
      profile,
      onAddStar,
      onRemoveStar,
      withCountryIcon,
    } = this.props

    return (
      <View style={[styles.list, style]}>
        {babynames.map((item, index) => {
          const isMyFavorite = has(item, `stars.${profile.id}`)

          const isFavoriteButtonDisabled = profile.isEmpty

          return (
            <BabynameListItem
              key={item.id}
              index={index + 1}
              item={item}
              onAddStar={onAddStar}
              onRemoveStar={onRemoveStar}
              style={styles.listItem}
              isMyFavorite={isMyFavorite}
              isFavoriteButtonDisabled={isFavoriteButtonDisabled}
              icon={
                withCountryIcon ? this._getIcon(item.countryCode) : undefined
              }
            />
          )
        })}
      </View>
    )
  }

  _renderCommonBabynameList = () => {
    const {
      babynames = [],
      style,
      profile,
      partnerName,
      onAddStar,
      onRemoveStar,
      withCountryIcon,
    } = this.props

    return (
      <View style={[styles.list, style]}>
        {babynames.map((item, index) => {
          const isMyFavorite = has(item, `stars.${profile.id}`)
          const isPartnerFavorite = has(item, `stars.${profile.partner}`)
          const createdByPatner = item.createdBy === profile.partner

          return (
            <BabynameListItem
              isCommonList
              key={item.id}
              index={index + 1}
              item={item}
              onAddStar={onAddStar}
              onRemoveStar={onRemoveStar}
              style={styles.listItem}
              isMyFavorite={isMyFavorite}
              isPartnerFavorite={isPartnerFavorite}
              partnerName={partnerName}
              createdByPatner={createdByPatner}
              icon={
                withCountryIcon
                  ? this._getIcon(item.countryCode, createdByPatner)
                  : undefined
              }
            />
          )
        })}
      </View>
    )
  }

  _getIcon = (countryCode, createdByPatner) => {
    const { profile, partnerImage } = this.props

    switch (countryCode) {
      case 'UK':
        return UKFlag

      case 'SE':
        return SwedenFlag

      case 'DK':
        return DenmarkFlag

      case 'US':
        return USAFlag

      case 'NO':
        return NorwayFlag

      case 'FI':
        return FinlandFlag

      default:
        if (createdByPatner) {
          return partnerImage || defaultUser
        }

        return profile.image || defaultUser
    }
  }
}
