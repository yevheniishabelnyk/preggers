import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  list: {
    alignSelf: 'stretch',
    alignItems: 'stretch',
  },

  listItem: {
    marginBottom: moderateScale(11, resizeFactor),
  },
})

export default styles
