/**
 *
 * StarButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import styles from './styles'

import grayStarIcon from 'assets/icons/gray-star.png'
import yellowStarIcon from 'assets/icons/yellow-star.png'

export default class StarButton extends React.Component {
  static propTypes = {
    isMyFavorite: PropTypes.bool,
    onPress: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { onPress, style, isMyFavorite } = this.props

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.button, style]}
        activeOpacity={0.95}
      >
        <Image
          source={isMyFavorite ? yellowStarIcon : grayStarIcon}
          style={styles.icon}
        />
      </TouchableOpacity>
    )
  }
}
