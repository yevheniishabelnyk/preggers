import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const iconWidth = moderateScale(21, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: iconWidth * 2,
    height: iconWidth * 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: iconWidth * -0.5,
  },

  icon: {
    width: iconWidth,
    height: iconWidth,
  },
})

export default styles
