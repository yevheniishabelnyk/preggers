import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    height: moderateScale(38, resizeFactor),
    alignSelf: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: moderateScale(8, resizeFactor),
    paddingHorizontal: moderateScale(10, resizeFactor),
    backgroundColor: 'white',
    borderRadius: 8,
  },

  name: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12, resizeFactor),
    letterSpacing: 0.6,
    color: 'rgb(131,146,167)',
  },

  removeButton: {},
})

export default styles
