/**
 *
 * AddedBabyName
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'
import SmallCloseButton from '@/settings/components/atoms/SmallCloseButton'

import { isFunction } from 'lodash'

import styles from './styles'

export default class AddedBabyName extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    style: PropTypes.any,
    onRemove: PropTypes.func,
    record: PropTypes.object,
  }

  render() {
    const { name, style } = this.props
    return (
      <View style={[styles.container, style]}>
        <Text style={styles.name}>{name}</Text>

        <SmallCloseButton
          onPress={this._removeButtonPressed}
          style={styles.removeButton}
        />
      </View>
    )
  }

  _removeButtonPressed = () => {
    const { onRemove, record, name } = this.props

    if (isFunction(onRemove)) {
      onRemove({ record, name })
    }
  }
}
