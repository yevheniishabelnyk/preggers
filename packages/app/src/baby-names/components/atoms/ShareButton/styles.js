import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  title: {
    fontFamily: 'Now-Medium',
    color: 'rgb(59,124,255)',
    fontSize: moderateScale(14, resizeFactor),
  },

  button: {
    justifyContent: 'flex-end',
  },
})

export default styles
