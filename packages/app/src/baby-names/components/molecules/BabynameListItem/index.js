/**
 *
 * BabynameListItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'
import StarButton from '@/baby-names/components/atoms/StarButton'

import grayStarIcon from 'assets/icons/gray-star.png'
import yellowStarIcon from 'assets/icons/yellow-star.png'

import { isFunction } from 'lodash'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class BabynameListItem extends React.Component {
  static propTypes = {
    isCommonList: PropTypes.bool,
    item: PropTypes.object.isRequired,
    index: PropTypes.number,
    onAddStar: PropTypes.func,
    onRemoveStar: PropTypes.func,
    isFavoriteButtonDisabled: PropTypes.bool,
    isMyFavorite: PropTypes.bool,
    isPartnerFavorite: PropTypes.bool,
    partnerName: PropTypes.string,
    createdByPatner: PropTypes.bool,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    icon: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      isMyFavorite: props.isMyFavorite,
    }
  }

  render() {
    const {
      style,
      index,
      icon,
      isCommonList,
      isPartnerFavorite,
      partnerName,
      createdByPatner,
      item,
    } = this.props

    return (
      <View
        style={[
          styles.container,
          isPartnerFavorite && this.state.isMyFavorite
            ? styles.yellowBorder
            : null,
          style,
        ]}
      >
        {icon ? (
          <View style={styles.iconWrapper}>
            <Image source={icon} style={styles.icon} resizeMode="contain" />
          </View>
        ) : null}

        <Text style={styles.text}>{`${index}. ${item.name}`}</Text>

        {isCommonList && createdByPatner ? (
          <FormattedMessage
            {...messages.addedBy}
            values={{ name: partnerName }}
            style={styles.addedByText}
          />
        ) : null}

        {isCommonList ? (
          <Image
            source={isPartnerFavorite ? yellowStarIcon : grayStarIcon}
            style={styles.connectedFavoriteStar}
          />
        ) : null}

        <StarButton
          onPress={this._starButtonPressed}
          isMyFavorite={this.state.isMyFavorite}
          style={styles.button}
        />
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.isMyFavorite !== nextProps.isMyFavorite &&
      nextProps.isMyFavorite !== this.state.isMyFavorite
    ) {
      this.setState({ isMyFavorite: nextProps.isMyFavorite })
    }
  }

  _starButtonPressed = () => {
    const {
      item,
      onAddStar,
      onRemoveStar,
      isFavoriteButtonDisabled,
    } = this.props
    const { isMyFavorite } = this.state

    if (!isFavoriteButtonDisabled) {
      this.setState(prevState => ({ isMyFavorite: !prevState.isMyFavorite }))
    }

    if (isMyFavorite && isFunction(onRemoveStar)) {
      onRemoveStar(item)
    } else if (isFunction(onAddStar)) {
      onAddStar(item)
    }
  }
}
