import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const containerHeight = moderateScale(60, resizeFactor)
const gutter = moderateScale(19, resizeFactor)

const connectedFavoriteStarWidth = moderateScale(12, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    height: containerHeight,
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: moderateScale(17, resizeFactor),
    borderWidth: 1,
    borderColor: 'transparent',
  },

  text: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
    marginRight: 'auto',
  },

  button: {
    height: containerHeight,
    width: containerHeight,
    marginRight: -gutter,
  },

  iconWrapper: {
    width: moderateScale(12, resizeFactor),
    height: moderateScale(12, resizeFactor),
    borderRadius: moderateScale(6, resizeFactor),
    overflow: 'hidden',
    marginRight: moderateScale(8, resizeFactor),
    marginLeft: moderateScale(-6, resizeFactor),
  },

  icon: {
    width: null,
    height: null,
    flex: 1,
  },

  yellowBorder: {
    borderColor: 'rgb(244,209,50)',
  },

  connectedFavoriteStar: {
    width: connectedFavoriteStarWidth,
    height: connectedFavoriteStarWidth,
    marginRight: moderateScale(-13, resizeFactor),
  },

  addedByText: {
    fontSize: moderateScale(8, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
    textAlign: 'left',
    marginRight: moderateScale(35, resizeFactor),
    marginLeft: 'auto',
  },
})

export default styles
