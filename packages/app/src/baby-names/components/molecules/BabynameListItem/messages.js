import { defineMessages } from 'react-intl'

export default defineMessages({
  addedBy: 'Added by {name}',
})
