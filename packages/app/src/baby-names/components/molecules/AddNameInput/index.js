/**
 *
 * AddNameInput
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TextInput, Animated } from 'react-native'
import PlusButton from '@/settings/components/atoms/PlusButton'

import { moderateScale } from '@/app/scaling'

import { isFunction } from 'lodash'

import styles from './styles'

const labelAnimatedTopOutputRange = [moderateScale(21), moderateScale(11)]
const labelAnimatedFontSizeOutputRange = [moderateScale(14), moderateScale(10)]

export default class AddNameInput extends React.Component {
  static propTypes = {
    onAdd: PropTypes.func,
    onChange: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    title: PropTypes.string,
    value: PropTypes.string,
    error: PropTypes.bool,
    placeholder: PropTypes.string,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  state = {
    value: '',
    isFocused: false,
  }

  constructor(props) {
    super(props)

    this._animatedIsFocused = new Animated.Value(props.value === '' ? 0 : 1)
  }

  render() {
    const { style, title } = this.props

    const labelStyle = {
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: labelAnimatedTopOutputRange,
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: labelAnimatedFontSizeOutputRange,
      }),
    }

    return (
      <View style={[styles.container, style]}>
        <View
          style={[
            styles.inputWrapper,
            this.props.style,
            this.props.error ? styles.inputError : null,
          ]}
        >
          {title ? (
            <Animated.Text
              style={[
                styles.label,
                labelStyle,
                this.props.error ? styles.labelError : null,
              ]}
            >
              {title}
            </Animated.Text>
          ) : null}

          <TextInput
            underlineColorAndroid="transparent"
            style={styles.input}
            multiline={false}
            autoCorrect={false}
            onChangeText={this._onChange}
            value={this.state.value}
            returnKeyType="done"
            onSubmitEditing={this._plusButtonPressed}
            enablesReturnKeyAutomatically
            onFocus={this._onFocus}
            onBlur={this._onBlur}
          />
          <View style={styles.plusButtonWrapper}>
            <PlusButton onPress={this._plusButtonPressed} />
          </View>
        </View>
      </View>
    )
  }

  componentWillMount() {
    const { value } = this.props
    let newValue

    if (value === '' || value === null || value === undefined) {
      newValue = ''
    }

    this._animatedIsFocused = new Animated.Value(newValue === '' ? 0 : 1)
  }

  componentDidUpdate() {
    const { value } = this.props

    let newValue

    if (value === '' || value === null || value === undefined) {
      newValue = ''
    }

    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || newValue !== '' ? 1 : 0,
      duration: 200,
    }).start()
  }

  _onFocus = () => {
    this.setState({ isFocused: true })

    if (isFunction(this.props.onFocus)) {
      this.props.onFocus()
    }
  }

  _onBlur = () => {
    this.setState({
      isFocused: false,
    })

    if (isFunction(this.props.onBlur)) {
      this.props.onBlur()
    }
  }

  clear = () => this.setState({ value: '' })

  _onChange = value => {
    const { onChange } = this.props

    if (isFunction(onChange)) {
      onChange(value)
    }

    this._setValue(value)
  }

  _setValue = value => this.setState({ value })

  _plusButtonPressed = () => {
    const { onAdd } = this.props
    const { value } = this.state
    const { onSubmitEditing } = this.props

    if (value && isFunction(onAdd)) {
      onAdd(value)
    } else if (isFunction(onSubmitEditing)) {
      onSubmitEditing(true)
    }
  }
}
