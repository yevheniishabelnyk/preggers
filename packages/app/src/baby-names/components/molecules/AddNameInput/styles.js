import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const containerHeight = moderateScale(60)

const containerWidth = moderateScale(330)

const styles = StyleSheet.create({
  inputWrapper: {
    width: containerWidth,
    height: containerHeight,
    borderRadius: moderateScale(8),
    justifyContent: 'center',
    backgroundColor: 'white',
    overflow: 'visible',
    marginBottom: 8,
  },

  input: {
    width: moderateScale(330, resizeFactor),
    height: moderateScale(45),
    color: '#000',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    paddingLeft: moderateScale(18, resizeFactor),
    paddingRight: moderateScale(65, resizeFactor),
  },

  label: {
    fontFamily: 'Now-Medium',
    color: 'rgb(131, 146, 167)',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(18),
  },

  labelError: {
    color: 'rgb(250,65,105)',
  },

  errorText: {
    color: 'rgb(255, 100, 101)',
  },

  plusButtonWrapper: {
    position: 'absolute',
    right: 18,
  },
})

export default styles
