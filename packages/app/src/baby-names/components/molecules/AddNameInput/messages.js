import { defineMessages } from 'react-intl'

export default defineMessages({
  addNameErrorTitle: 'Please enter a name',
})
