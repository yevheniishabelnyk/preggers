/**
 *
 * EmptyFavorites
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Text } from 'react-native'

import styles from './styles'

const EmptyFavorites = ({ title }) => (
  <View style={styles.container}>
    <Image
      source={require('assets/img/empty-favorites.png')}
      style={styles.image}
    />

    <Text style={styles.title}>{title}</Text>
  </View>
)

EmptyFavorites.propTypes = {
  title: PropTypes.string,
}

export default EmptyFavorites
