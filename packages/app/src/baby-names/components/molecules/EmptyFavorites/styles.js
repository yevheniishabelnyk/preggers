import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  image: {
    width: moderateScale(190, 0.7),
    height: moderateScale(234, 0.7),
    marginBottom: moderateScale(44),
    marginTop: moderateScale(12),
  },

  title: {
    color: 'rgb(43,56,87)',
    fontSize: moderateScale(18),
    fontFamily: 'Now-Regular',
    lineHeight: moderateScale(27),
    width: moderateScale(243, 0.95),
    textAlign: 'center',
    marginBottom: moderateScale(30),
  },
})

export default styles
