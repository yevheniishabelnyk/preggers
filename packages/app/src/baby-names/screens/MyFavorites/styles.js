import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    alignItems: 'stretch',
    paddingHorizontal: moderateScale(22.5, 3),
    paddingBottom: moderateScale(22.5, 0.3),
  },

  listHeader: {
    flexDirection: 'row',
    marginBottom: moderateScale(19, 0.3),
    alignItems: 'center',
  },

  list: {
    marginBottom: moderateScale(23, 0.3),
  },

  listTitle: {
    fontFamily: 'Now-Bold',
    color: 'rgb(41,53,69)',
    fontSize: moderateScale(26, 0.3),
    marginRight: 'auto',
  },
})

export default styles
