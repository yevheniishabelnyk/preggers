import { defineMessages } from 'react-intl'

export default defineMessages({
  emptyTitle:
    'No favorites added yet, press the star after each name to add them as favorites.',
  girlsNames: 'Girls names',
  boysNames: 'Boys names',
  shareBoysEmailTitle: "This is {name}'s favorite boys names",
  shareGirlsEmailTitle: "This is {name}'s favorite girls names",
  hiText: 'Hi',
  boysFavoriteNameText: 'This is my favourite boys names:',
  girlsFavoriteNameText: 'This is my favourite girls names:',
  whatYouThink: 'Let me know what you think! :)',
  downloadText:
    'This favourite name list were created by the Preggers app. Download it here: https://preggersapp.com/',
})
