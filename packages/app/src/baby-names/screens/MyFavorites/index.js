/**
 *
 * MyFavorites
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { ScrollView, View, Share } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import Loader from '@/shared/components/Loader'
import EmptyFavorites from '@/baby-names/components/molecules/EmptyFavorites'
import BabynameList from '@/baby-names/components/organisms/BabynameList'
import ShareButton from '@/baby-names/components/atoms/ShareButton'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

import {
  getUserFavoriteBabynames,
  getUserOwnedBabynames,
} from '@/app/redux/selectors'

import { isEmpty } from 'lodash'

import * as BabynamesActions from '@/app/database/actions/babynames'

@connect((state, props) => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    const favoriteBabynames = getUserFavoriteBabynames(state, profile.id)

    const userOwnedBabynames = getUserOwnedBabynames(state, profile.id)

    return {
      babynames: Object.assign({}, favoriteBabynames, userOwnedBabynames),
      isFetching:
        favoriteBabynames === undefined || userOwnedBabynames === undefined,
    }
  }

  return {
    isFetching: true,
  }
})
export default class MyFavorites extends BaseScreen {
  static propTypes = {
    babynames: PropTypes.object,
    isFetching: PropTypes.bool,
    screenProps: PropTypes.shape({
      profile: PropTypes.object,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {}

  render() {
    const { isFetching } = this.props

    if (isFetching) {
      return <Loader background="#f9fafc" />
    }

    const { babynames } = this.props
    const { formatMessage } = this.context.intl

    if (isEmpty(babynames)) {
      return (
        <ScrollView style={styles.container}>
          <EmptyFavorites title={formatMessage(messages.emptyTitle)} />
        </ScrollView>
      )
    }

    const { profile } = this.props.screenProps

    const boysBabynames = Object.values(babynames).filter(
      item => item.gender === 'm'
    )

    const girlsBabynames = Object.values(babynames).filter(
      item => item.gender === 'f'
    )

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        {girlsBabynames.length ? (
          <React.Fragment>
            <View style={styles.listHeader}>
              <FormattedMessage
                {...messages.girlsNames}
                style={styles.listTitle}
              />

              <ShareButton onPress={this._shareGirls} />
            </View>

            <BabynameList
              style={styles.list}
              babynames={girlsBabynames}
              profile={profile}
              onRemoveStar={this._removeFromFavorites}
              withCountryIcon
            />
          </React.Fragment>
        ) : null}

        {boysBabynames.length ? (
          <React.Fragment>
            <View style={styles.listHeader}>
              <FormattedMessage
                {...messages.boysNames}
                style={styles.listTitle}
              />

              <ShareButton onPress={this._shareBoys} />
            </View>

            <BabynameList
              style={styles.list}
              babynames={boysBabynames}
              profile={profile}
              onRemoveStar={this._removeFromFavorites}
              withCountryIcon
            />
          </React.Fragment>
        ) : null}
      </ScrollView>
    )
  }

  _removeFromFavorites = async babyname => {
    try {
      if (babyname.createdBy) {
        await this._(BabynamesActions.removeFromOwned(babyname))
      } else {
        await this._(BabynamesActions.removeFromFavorites(babyname))
      }
    } catch (err) {
      console.info('err: ', err)
    }
  }

  _shareBoys = () => this._share('m')

  _shareGirls = () => this._share('f')

  _share = gender => {
    const { profile } = this.props.screenProps
    const { babynames } = this.props
    const { formatMessage } = this.context.intl

    const boysBabynames = Object.values(babynames).filter(
      item => item.gender === gender
    )

    const title =
      gender === 'm'
        ? formatMessage(messages.shareBoysEmailTitle, { name: profile.name })
        : formatMessage(messages.shareGirlsEmailTitle, { name: profile.name })

    const favoriteNameText =
      gender === 'm'
        ? formatMessage(messages.boysFavoriteNameText)
        : formatMessage(messages.girlsFavoriteNameText)

    const message = `
${formatMessage(messages.hiText)},

${favoriteNameText}
${boysBabynames.reduce(
      (out, name, index) => out.concat(`\n${index + 1}. ${name.name}`),
      ''
    )}

${formatMessage(messages.whatYouThink)}

${formatMessage(messages.downloadText)}
`
    const content = {
      title,
      message,
    }

    Share.share(content).then(res => console.info('res: ', res))
  }
}
