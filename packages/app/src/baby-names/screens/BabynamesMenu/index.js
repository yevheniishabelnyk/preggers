/**
 *
 * BabynamesMenu
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import WhiteFormButton from '@/settings/components/molecules/WhiteFormButton'
import ActionPopup from '@/shared/components/ActionPopup'

import { firebaseConnect } from 'react-redux-firebase'

import * as RouterActions from '@/app/redux/router/actions'

import messages from './messages'

import styles from './styles'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  const listeners = ['content/countryBabyNames']

  if (!profile.isEmpty) {
    listeners.push(
      `babynameLikesByUser/${profile.id}`,
      `userOwned/babyNames/${profile.id}`
    )
  }

  return listeners
})
export default class BabynamesMenu extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              onPlusButtonPress={this._plusButtonPressed}
              style={styles.header}
            />
          ),

          popups: [
            <ActionPopup
              key="useOurFunctionsDialog"
              ref={ref => (this._useOurFunctionsDialog = ref)}
              image={require('assets/img/create-account-popup.png')}
              largeTitle={formatMessage(messages.actionPopupTitle)}
              text={formatMessage(messages.actionPopupText)}
              actionButton={{
                title: formatMessage(messages.actionPopupActionButton),
                onPress: this._goToLoginPage,
              }}
              okButton={{
                title: formatMessage(messages.actionPopupOkButton),
                onPress: this._dismissActionDialog,
              }}
            />,
          ],
        }}
        headerHeight={SettingsHeader.height}
      >
        <WhiteFormButton
          title={formatMessage(messages.favoritesButtonTitle)}
          onPress={this._favoritesButtonPressed}
          style={styles.button}
          arrow
          icon={require('assets/icons/yellow-star.png')}
        />

        <WhiteFormButton
          title={formatMessage(messages.SwedishNamesButtonTitle)}
          onPress={this._swedishNamesButtonPressed}
          style={styles.button}
          arrow
          icon={require('assets/icons/sweden-flag.png')}
        />

        <WhiteFormButton
          title={formatMessage(messages.NorweiganNamesButtonTitle)}
          onPress={this._norweiganNamesButtonPressed}
          style={styles.button}
          arrow
          icon={require('assets/icons/norway-flag.png')}
        />

        <WhiteFormButton
          title={formatMessage(messages.DanishNamesButtonTitle)}
          onPress={this._danishNamesButtonPressed}
          style={styles.button}
          arrow
          icon={require('assets/icons/denmark-flag.png')}
        />

        <WhiteFormButton
          title={formatMessage(messages.FinlandNamesButtonTitle)}
          onPress={this._finlandNamesButtonPressed}
          style={styles.button}
          arrow
          icon={require('assets/icons/finland-flag.png')}
        />

        <WhiteFormButton
          title={formatMessage(messages.BritishNamesButtonTitle)}
          onPress={this._britishNamesButtonPressed}
          style={styles.button}
          arrow
          icon={require('assets/icons/uk-flag.png')}
        />

        <WhiteFormButton
          title={formatMessage(messages.AmericanNamesButtonTitle)}
          onPress={this._americanNamesButtonPressed}
          style={styles.button}
          arrow
          icon={require('assets/icons/usa-flag.png')}
        />
      </ViewWrapper>
    )
  }

  _plusButtonPressed = () => {
    const { profile } = this.props.screenProps

    if (profile.isEmpty) {
      this._useOurFunctionsDialog._show()
    } else {
      this._goTo('/baby-names-add')
    }
  }

  _favoritesButtonPressed = () => {
    const { profile } = this.props.screenProps

    if (profile.isEmpty) {
      this._useOurFunctionsDialog._show()
    } else {
      this._goTo('/~baby-names-favorites')
    }
  }

  _swedishNamesButtonPressed = () =>
    this._goTo('/baby-names-country', { countryCode: 'SE' })

  _norweiganNamesButtonPressed = () =>
    this._goTo('/baby-names-country', { countryCode: 'NO' })

  _danishNamesButtonPressed = () =>
    this._goTo('/baby-names-country', { countryCode: 'DK' })

  _finlandNamesButtonPressed = () =>
    this._goTo('/baby-names-country', { countryCode: 'FI' })

  _britishNamesButtonPressed = () =>
    this._goTo('/baby-names-country', { countryCode: 'UK' })

  _americanNamesButtonPressed = () =>
    this._goTo('/baby-names-country', { countryCode: 'US' })

  _goToLoginPage = () => this._(RouterActions.resetToUrl(['/auth-login']))

  _dismissActionDialog = () => {
    this._useOurFunctionsDialog._hide()
  }
}
