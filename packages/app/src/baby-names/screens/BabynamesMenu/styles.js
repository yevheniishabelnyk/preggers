import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.5

const gutter = moderateScale(22.5, resizeFactor)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingLeft: gutter,
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  button: {
    marginRight: Platform.OS === 'android' ? -gutter : 0,
    marginBottom: moderateScale(16, resizeFactor),
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
})

export default styles
