import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Baby names',
  favoritesButtonTitle: 'My favorites',
  SwedishNamesButtonTitle: 'Swedish names',
  BritishNamesButtonTitle: 'British names',
  AmericanNamesButtonTitle: 'American names',
  NorweiganNamesButtonTitle: 'Norweigan names',
  DanishNamesButtonTitle: 'Danish names',
  FinlandNamesButtonTitle: 'Finnish names',
  actionPopupTitle:
    'Want to use all our functions? Create an account, it’s free!',
  actionPopupText:
    'In order to use all the functions in our app you need to create an account. Just click the link below',
  actionPopupActionButton: 'Of course I want to!',
  actionPopupOkButton: 'No thanks',
})
