import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(22.5, 0.3)

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    alignItems: 'stretch',
    paddingHorizontal: moderateScale(22.5, 3),
    paddingBottom: gutter * 2,
  },

  listHeader: {
    flexDirection: 'row',
    marginBottom: moderateScale(19, 0.3),
  },

  list: {
    marginBottom: moderateScale(23, 0.3),
  },

  listTitle: {
    fontFamily: 'Now-Bold',
    color: 'rgb(41,53,69)',
    fontSize: moderateScale(26, 0.3),
    marginRight: 'auto',
  },

  notEmptyTitle: {
    fontFamily: 'Now-Regular',
    color: 'rgb(43,56,87)',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(27),
    textAlign: 'center',
    width: moderateScale(243),
    alignSelf: 'center',
    marginBottom: moderateScale(32),
  },

  notEmptyImage: {
    width: moderateScale(200),
    height: moderateScale(153),
    alignSelf: 'center',
    marginBottom: moderateScale(28),
    marginTop: moderateScale(20),
  },
})

export default styles
