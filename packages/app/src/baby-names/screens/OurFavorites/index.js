/**
 *
 * OurFavorites
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { ScrollView, View, Share, Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import Loader from '@/shared/components/Loader'
import EmptyFavorites from '@/baby-names/components/molecules/EmptyFavorites'
import ShareButton from '@/baby-names/components/atoms/ShareButton'
import BabynameList from '@/baby-names/components/organisms/BabynameList'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

import { isEmpty } from 'lodash'

import {
  getUserFavoriteBabynames,
  getUserOwnedBabynames,
  getUser,
} from '@/app/redux/selectors'

import * as BabynamesActions from '@/app/database/actions/babynames'

import { firebaseConnect } from 'react-redux-firebase'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  if (!profile.isEmpty && profile.partner) {
    return [
      `babynameLikesByUser/${profile.partner}`,
      `userOwned/babyNames/${profile.partner}`,
      `users/${profile.partner}`,
    ]
  }
})
@connect((state, props) => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    const userFavoriteBabynames = getUserFavoriteBabynames(state, profile.id)
    const userOwnedBabynames = getUserOwnedBabynames(state, profile.id)

    const partnerFavoriteBabynames = getUserFavoriteBabynames(
      state,
      profile.partner
    )
    const partnerOwnedBabynames = getUserOwnedBabynames(state, profile.partner)

    const partner = getUser(state, profile.partner)

    return {
      partnerName: partner && partner.name,
      partnerImage: partner && partner.image,
      babynames: Object.assign(
        {},
        userFavoriteBabynames,
        partnerFavoriteBabynames,
        userOwnedBabynames,
        partnerOwnedBabynames
      ),
      isFetching:
        userFavoriteBabynames === undefined ||
        partnerFavoriteBabynames === undefined ||
        userOwnedBabynames === undefined ||
        partnerOwnedBabynames === undefined,
    }
  }

  return {
    isFetching: true,
  }
})
export default class OurFavorites extends BaseScreen {
  static propTypes = {
    partnerName: PropTypes.string,
    partnerImage: PropTypes.object,
    babynames: PropTypes.object,
    isFetching: PropTypes.bool,
    screenProps: PropTypes.shape({
      profile: PropTypes.object,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { isFetching } = this.props

    if (isFetching) {
      return <Loader background="#f9fafc" />
    }

    const { babynames } = this.props
    const { formatMessage } = this.context.intl

    if (isEmpty(babynames)) {
      return (
        <ScrollView style={styles.container}>
          <EmptyFavorites title={formatMessage(messages.emptyTitle)} />
        </ScrollView>
      )
    }

    const { profile } = this.props.screenProps
    const { partnerName, partnerImage } = this.props

    const boysBabynames = Object.values(babynames).filter(
      item => item.gender === 'm'
    )

    const girlsBabynames = Object.values(babynames).filter(
      item => item.gender === 'f'
    )

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Image
          source={require('assets/img/common-favorite-babynames.png')}
          style={styles.notEmptyImage}
        />

        <FormattedMessage
          {...messages.notEmptyTitle}
          style={styles.notEmptyTitle}
        />

        {girlsBabynames.length ? (
          <React.Fragment>
            <View style={styles.listHeader}>
              <FormattedMessage
                {...messages.girlsNames}
                style={styles.listTitle}
              />

              <ShareButton onPress={this._shareGirls} />
            </View>

            <BabynameList
              isCommonList
              style={styles.list}
              babynames={girlsBabynames}
              profile={profile}
              onAddStar={this._addToFavorites}
              onRemoveStar={this._removeFromFavorites}
              withCountryIcon
              partnerName={partnerName}
              partnerImage={partnerImage}
            />
          </React.Fragment>
        ) : null}

        {boysBabynames.length ? (
          <React.Fragment>
            <View style={styles.listHeader}>
              <FormattedMessage
                {...messages.boysNames}
                style={styles.listTitle}
              />

              <ShareButton onPress={this._shareBoys} />
            </View>

            <BabynameList
              isCommonList
              style={styles.list}
              babynames={boysBabynames}
              profile={profile}
              onAddStar={this._addToFavorites}
              onRemoveStar={this._removeFromFavorites}
              withCountryIcon
              partnerName={partnerName}
              partnerImage={partnerImage}
            />
          </React.Fragment>
        ) : null}
      </ScrollView>
    )
  }

  _addToFavorites = async babyname => {
    try {
      await this._(BabynamesActions.addToFavorites(babyname))
    } catch (err) {
      console.info('err: ', err)
    }
  }

  _removeFromFavorites = async babyname => {
    try {
      if (babyname.createdBy) {
        await this._(BabynamesActions.removeFromOwned(babyname))
      } else {
        await this._(BabynamesActions.removeFromFavorites(babyname))
      }
    } catch (err) {
      console.info('err: ', err)
    }
  }

  _shareBoys = () => this._share('m')

  _shareGirls = () => this._share('f')

  _share = gender => {
    const { profile } = this.props.screenProps
    const { babynames } = this.props
    const { formatMessage } = this.context.intl

    const boysBabynames = Object.values(babynames).filter(
      item => item.gender === gender
    )

    const title =
      gender === 'm'
        ? formatMessage(messages.shareBoysEmailTitle, { name: profile.name })
        : formatMessage(messages.shareGirlsEmailTitle, { name: profile.name })

    const favoriteNameText =
      gender === 'm'
        ? formatMessage(messages.boysFavoriteNameText)
        : formatMessage(messages.girlsFavoriteNameText)

    const message = `
${formatMessage(messages.hiText)},

${favoriteNameText}
${boysBabynames.reduce(
      (out, name, index) => out.concat(`\n${index + 1}. ${name.name}`),
      ''
    )}

${formatMessage(messages.whatYouThink)}

${formatMessage(messages.downloadText)}
`
    const content = {
      title,
      message,
    }

    Share.share(content).then(res => console.info('res: ', res))
  }
}
