/**
 *
 * CountryBabynames
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Text, ScrollView } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import BabynameList from '@/baby-names/components/organisms/BabynameList'
import FormTitledSwitch from '@/settings/components/molecules/FormTitledSwitch'
import Loader from '@/shared/components/Loader'

import ActionPopup from '@/shared/components/ActionPopup'

import * as BabynamesActions from '@/app/database/actions/babynames'
import * as RouterActions from '@/app/redux/router/actions'
import * as ProfileActions from '@/app/redux/profile/actions'

import messages from './messages'

import { moderateScale } from '@/app/scaling'

import styles from './styles'

import { getCountryBabynames } from '@/app/redux/selectors'

import { firebaseConnect } from 'react-redux-firebase'

import { isEmpty } from 'lodash'

import SwedenFlag from 'assets/icons/sweden-flag.png'
import DenmarkFlag from 'assets/icons/denmark-flag.png'
import UKFlag from 'assets/icons/uk-flag.png'
import USAFlag from 'assets/icons/usa-flag.png'
import NorwayFlag from 'assets/icons/norway-flag.png'
import FinlandFlag from 'assets/icons/finland-flag.png'

@firebaseConnect(['content/countryBabyNames'])
@connect((state, props) => {
  const countryCode = props.navigation.getParam('countryCode', 'US')
  const babynames = getCountryBabynames(state, countryCode)

  return {
    babynames,
  }
})
export default class CountryBabynames extends BaseScreen {
  static propTypes = {
    babynames: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    const gender = props.navigation.getParam('gender')

    if (!gender) {
      props.navigation.setParams({ gender: 'm' })
    }
  }

  render() {
    const { navigation } = this.props
    const { formatMessage } = this.context.intl

    const countryCode = navigation.getParam('countryCode', 'US')

    const flagIcon = this._getFlag(countryCode)

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={this._getTitle(countryCode)}
              icon={flagIcon}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),

          popups: [
            <ActionPopup
              key="useOurFunctionsDialog"
              ref={ref => (this._useOurFunctionsDialog = ref)}
              image={require('assets/img/create-account-popup.png')}
              largeTitle={formatMessage(messages.actionPopupTitle)}
              text={formatMessage(messages.actionPopupText)}
              actionButton={{
                title: formatMessage(messages.actionPopupActionButton),
                onPress: this._goToLoginPage,
              }}
              okButton={{
                title: formatMessage(messages.actionPopupOkButton),
                onPress: this._dismissActionDialog,
              }}
            />,
          ],
        }}
        headerHeight={SettingsHeader.height - moderateScale(60)}
      >
        {this._renderContent()}
      </ViewWrapper>
    )
  }

  _renderContent = () => {
    const { babynames, navigation } = this.props

    if (!babynames || isEmpty(babynames)) {
      return <Loader background="#f9fafc" />
    }

    const { profile } = this.props.screenProps
    const { formatMessage } = this.context.intl
    const gender = navigation.getParam('gender', 'm')

    const filteredBabynames = Object.values(babynames).filter(
      item => item.gender === gender
    )

    return (
      <React.Fragment>
        <View style={styles.listHeader}>
          <Text style={styles.listTitle}>
            {formatMessage(messages.topNamesTitle)}
          </Text>

          <FormTitledSwitch
            onTitle={formatMessage(messages.boys)}
            offTitle={formatMessage(messages.girls)}
            value={gender === 'm'}
            onChange={this._genderChanged}
          />
        </View>

        <ScrollView style={styles.listWrapper}>
          <BabynameList
            style={styles.list}
            babynames={filteredBabynames}
            profile={profile}
            onAddStar={this._addToFavorites}
            onRemoveStar={this._removeFromFavorites}
          />
        </ScrollView>
      </React.Fragment>
    )
  }

  _getFlag(countryCode) {
    switch (countryCode) {
      case 'UK':
        return UKFlag

      case 'SE':
        return SwedenFlag

      case 'DK':
        return DenmarkFlag

      case 'US':
        return USAFlag

      case 'NO':
        return NorwayFlag

      case 'FI':
        return FinlandFlag
    }
  }

  _genderChanged = isMale =>
    this.props.navigation.setParams({ gender: isMale ? 'm' : 'f' })

  _getTitle = countryCode => {
    const { formatMessage } = this.context.intl

    switch (countryCode) {
      case 'UK':
        return formatMessage(messages.britishNamesHeaderTitle)

      case 'SE':
        return formatMessage(messages.swedishNamesHeaderTitle)

      case 'US':
        return formatMessage(messages.americanNamesHeaderTitle)

      case 'DK':
        return formatMessage(messages.danishNamesHeaderTitle)

      case 'NO':
        return formatMessage(messages.norwaiganNamesHeaderTitle)

      case 'FI':
        return formatMessage(messages.finishNamesHeaderTitle)

      default:
        return formatMessage(messages.babyNamesHeaderTitle)
    }
  }

  _addToFavorites = async babyname => {
    const { profile } = this.props.screenProps

    if (!profile.isEmpty) {
      try {
        await this._(BabynamesActions.addToFavorites(babyname))

        this._(ProfileActions.maybeAskUserToRateApp())
      } catch (err) {
        console.info('err: ', err)
      }
    } else {
      this._useOurFunctionsDialog._show()
    }
  }

  _removeFromFavorites = async babyname => {
    const { profile } = this.props.screenProps

    if (!profile.isEmpty) {
      try {
        await this._(BabynamesActions.removeFromFavorites(babyname))
      } catch (err) {
        console.info('err: ', err)
      }
    }
  }

  _goToLoginPage = () => this._(RouterActions.resetToUrl(['/auth-login']))

  _dismissActionDialog = () => {
    this._useOurFunctionsDialog._hide()
  }
}
