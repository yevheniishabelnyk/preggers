import { defineMessages } from 'react-intl'

export default defineMessages({
  topNamesTitle: 'Top 50',
  boys: 'Boys',
  girls: 'Girls',
  britishNamesHeaderTitle: 'British names',
  swedishNamesHeaderTitle: 'Swedish names',
  americanNamesHeaderTitle: 'American names',
  norwaiganNamesHeaderTitle: 'Norweigan names',
  danishNamesHeaderTitle: 'Danish names',
  finishNamesHeaderTitle: 'Finnish names',
  babyNamesHeaderTitle: 'Baby names',
  actionPopupTitle:
    'Want to use all our functions? Create an account, it’s free!',
  actionPopupText:
    'In order to use all the functions in our app you need to create an account. Just click the link below',
  actionPopupActionButton: 'Of course I want to!',
  actionPopupOkButton: 'No thanks',
})
