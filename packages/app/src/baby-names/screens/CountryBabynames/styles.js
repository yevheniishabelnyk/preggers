import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 3)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  list: {
    marginBottom: gutter * 2,
  },

  listWrapper: {
    paddingHorizontal: gutter,
  },

  listHeader: {
    flexDirection: 'row',
    marginBottom: moderateScale(24, resizeFactor),
    alignSelf: 'stretch',
    alignItems: 'center',
    paddingHorizontal: gutter,
  },

  listTitle: {
    fontFamily: 'Now-Bold',
    color: 'rgb(41,53,69)',
    fontSize: moderateScale(26, resizeFactor),
    marginRight: 'auto',
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },
})

export default styles
