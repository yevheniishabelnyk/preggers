import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Baby names',
  boy: 'Boy',
  girl: 'Girl',
  nameInputPlaceholder: 'Type here',
  nameAlredayAdded: 'That name has already been added.',
  somethingWentWrong: 'Sorry, something went wrong. Please try again later.',
  addNameErrorTitle: 'Please enter a name',
})
