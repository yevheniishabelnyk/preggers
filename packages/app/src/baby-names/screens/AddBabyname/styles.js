import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(22.5, 3)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: gutter,
    alignItems: 'stretch',
  },

  header: {
    paddingHorizontal: moderateScale(22.5, 0.3),
    backgroundColor: 'rgb(249,250,252)',
  },

  blockRow: {
    flexDirection: 'row',
    marginBottom: moderateScale(23, 0.3),
  },

  genderButton: {
    flex: 1,
  },

  rightGenderButton: {
    marginLeft: moderateScale(8, 0.3),
  },

  leftGenderButton: {
    marginRight: moderateScale(8, 0.3),
  },

  addedBabyName: {
    marginBottom: moderateScale(7, 0.3),
    marginRight: moderateScale(7, 0.3),
  },

  names: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  error: {
    position: 'relative',
    marginLeft: moderateScale(18),
    marginTop: moderateScale(12),
  },
})

export default styles
