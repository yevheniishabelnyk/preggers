/**
 *
 * AddBabyname
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View } from 'react-native'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import AddNameInput from '@/baby-names/components/molecules/AddNameInput'
import RedButton from '@/settings/components/atoms/RedButton'
import AddedBabyName from '@/baby-names/components/atoms/AddedBabyName'
import Loader from '@/shared/components/Loader'
import FormError from '@/forms/components/atoms/FormError'

import messages from './messages'

import styles from './styles'

import { without } from 'lodash'

import { getUserOwnedBabynames, getConnection } from '@/app/redux/selectors'

import * as BabynamesActions from '@/app/database/actions/babynames'
import * as ProfileActions from '@/app/redux/profile/actions'

import { moderateScale } from '@/app/scaling'

import { firebaseConnect } from 'react-redux-firebase'

@firebaseConnect(props => {
  const { profile } = props.screenProps

  if (!profile.isEmpty && profile.partner && profile.connection) {
    return [
      `userOwned/babyNames/${profile.partner}`,
      `connections/${profile.connection}/shareBabyNames`,
    ]
  }
})
@connect((state, props) => {
  const { profile } = props.screenProps

  const babynames = getUserOwnedBabynames(state, profile.id)
  let partnerBabynames = null
  let shareBabyNames = false

  if (!profile.isEmpty && profile.connection && profile.partner) {
    const userConnection = getConnection(state, profile.connection)

    partnerBabynames = getUserOwnedBabynames(state, profile.partner)

    shareBabyNames = Boolean(userConnection && userConnection.shareBabyNames)
  }

  return {
    babynames,
    partnerBabynames,
    shareBabyNames,
    isFetching: babynames === undefined,
  }
})
export default class AddBabyname extends BaseScreen {
  static propTypes = {
    babynames: PropTypes.object,
    partnerBabynames: PropTypes.object,
    shareBabyNames: PropTypes.bool,
    isFetching: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    gender: 'm',
    error: false,
    errorMessage: '',
    pendingNames: [],
  }

  render() {
    const { isFetching, babynames } = this.props
    const { pendingNames } = this.state
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height - moderateScale(60)}
      >
        <View style={[styles.blockRow, styles.firstRow]}>
          <RedButton
            title={formatMessage(messages.boy)}
            active={this.state.gender === 'm'}
            onPress={this._setGenderMale}
            style={[styles.genderButton, styles.leftGenderButton]}
          />

          <RedButton
            title={formatMessage(messages.girl)}
            active={this.state.gender === 'f'}
            onPress={this._setGenderFemale}
            style={[styles.genderButton, styles.rightGenderButton]}
          />
        </View>
        <View>
          <AddNameInput
            ref={ref => (this._inputRef = ref)}
            title={formatMessage(messages.nameInputPlaceholder)}
            onAdd={this._add}
            onSubmitEditing={this._checkIfEmpty}
            onChange={this._onChange}
            error={this.state.error}
          />

          <FormError
            isVisible={this.state.error}
            title={this.state.errorMessage}
            style={styles.error}
          />
        </View>

        {isFetching ? (
          <Loader background="#f9fafc" />
        ) : babynames ? (
          <View style={styles.names}>
            {Object.values(babynames)
              .filter(item => pendingNames.indexOf(item.name) === -1)
              .map(item => (
                <AddedBabyName
                  key={item.id}
                  name={item.name}
                  record={item}
                  style={styles.addedBabyName}
                  onRemove={this._removeName}
                />
              ))}
            {pendingNames.map(name => (
              <AddedBabyName
                key={name}
                name={name}
                style={styles.addedBabyName}
                onRemove={this._removePendingName}
              />
            ))}
          </View>
        ) : null}
      </ViewWrapper>
    )
  }

  _setGenderMale = () => this.setState({ gender: 'm' })

  _setGenderFemale = () => this.setState({ gender: 'f' })

  _checkIfEmpty = () => {
    const { formatMessage } = this.context.intl

    this.setState({
      error: true,
      errorMessage: formatMessage(messages.addNameErrorTitle),
    })
  }

  _add = async name => {
    if (name) {
      const { babynames, partnerBabynames, shareBabyNames } = this.props
      const { pendingNames, gender } = this.state
      const { formatMessage } = this.context.intl

      const exists =
        (babynames &&
          Object.values(babynames).find(item => item.name === name)) ||
        pendingNames.indexOf(name) !== -1

      if (exists) {
        this.setState({
          error: true,
          errorMessage: formatMessage(messages.nameAlredayAdded),
        })
      } else {
        this.setState(prevState => ({
          pendingNames: prevState.pendingNames.concat(name),
        }))

        this._inputRef.clear()

        try {
          let partnerDuplicate

          if (shareBabyNames && partnerBabynames) {
            partnerDuplicate = Object.values(partnerBabynames).find(
              item => item.name === name && item.gender === gender
            )
          }

          if (partnerDuplicate) {
            await this._(BabynamesActions.addToFavorites(partnerDuplicate))
          } else {
            await this._(BabynamesActions.addToOwned(name, gender))
          }

          this._(ProfileActions.maybeAskUserToRateApp())
        } catch (err) {
          console.log('err', err)

          this.setState({
            error: true,
            errorMessage: formatMessage(messages.somethingWentWrong),
          })
        }

        this._removePendingName({ name })
      }
    }
  }

  _removeName = async ({ record }) => {
    const { formatMessage } = this.context.intl
    try {
      await this._(BabynamesActions.removeFromOwned(record))

      if (record.name === this._inputRef.state.value) {
        this.setState({ error: false })
      }
    } catch (err) {
      console.log('err', err)

      this.setState({
        error: true,
        errorMessage: formatMessage(messages.somethingWentWrong),
      })
    }
  }

  _removePendingName = ({ name }) =>
    this.setState(prevState => ({
      pendingNames: without(prevState.pendingNames, name),
    }))

  _onChange = () => {
    if (this.state.error) {
      this.setState({ error: false })
    }
  }
}
