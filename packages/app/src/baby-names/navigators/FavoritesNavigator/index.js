/**
 *
 * Babynames Favorites Navigator
 *
 */

import { TabNavigator } from 'react-navigation'

import MyFavorites from '@/baby-names/screens/MyFavorites'
import OurFavorites from '@/baby-names/screens/OurFavorites'
import FavoritesTabBar from '@/baby-names/components/organisms/FavoritesTabBar'

export default TabNavigator(
  {
    '/my': {
      screen: MyFavorites,
    },

    '/our': {
      screen: OurFavorites,
    },
  },
  {
    initialRouteName: '/my',
    order: ['/my', '/our'],
    navigationOptions: {
      gesturesEnabled: false,
    },
    animationEnabled: true,
    tabBarPosition: 'top',
    tabBarComponent: FavoritesTabBar,
  }
)
