/*
 *
 * Tips routes
 *
 */

import DailyTip from './screens/DailyTip'

export default {
  '/daily-tip': {
    screen: DailyTip,
  },
}
