/**
 *
 * Tip
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image, Text } from 'react-native'
import CloseButton from '@/pregnancy/components/atoms/CloseButton'
import NextButton from '@/onboarding/components/NextButton'
import { ExpoImage } from '@/vendor/cache-manager'

import personBlueBG from 'assets/img/person-blue-bg.png'
import personRedBG from 'assets/img/person-red-bg.png'

import * as RouterActions from '@/app/redux/router/actions'

import messages from './messages'

import HTML from 'react-native-render-html'

import styles, { contentParagraphStyle, imageMaxWidth } from './styles'

export default class Tip extends React.Component {
  static propTypes = {
    onClose: PropTypes.func,
    dispatch: PropTypes.func,
    link: PropTypes.string,
    image: PropTypes.object,
    title: PropTypes.string,
    message: PropTypes.string,
    description: PropTypes.string,
    background: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    background: 'blue',
  }

  render() {
    const {
      link,
      onClose,
      image,
      message,
      description,
      title,
      background,
    } = this.props

    const { formatMessage } = this.context.intl

    return (
      <View
        style={[
          styles.container,
          background === 'red' ? styles.redBackground : null,
        ]}
      >
        <CloseButton style={styles.closeButton} onPress={onClose} />

        <Image
          style={styles.person}
          source={background === 'red' ? personRedBG : personBlueBG}
        />

        <View style={styles.content}>
          <Text style={styles.title}>{title}</Text>

          {image ? (
            <ExpoImage
              source={{
                uri: image.uri,
                preview: image.preview,
              }}
              cacheImage
              style={styles.image}
            />
          ) : null}

          {message ? <Text style={styles.message}>{message}</Text> : null}

          {description ? (
            <HTML
              html={description}
              containerStyle={styles.description}
              baseFontStyle={contentParagraphStyle}
              imageMaxWidth={imageMaxWidth}
              tagsStyles={{
                p: styles.contentParagraph,
                strong: styles.boldText,
                em: styles.emText,
                h1: styles.headingText,
                h2: styles.headingText,
                h3: styles.headingText,
                span: styles.spanText,
              }}
              renderers={{
                ol: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                  const content = React.Children.map(
                    children,
                    (child, index) => {
                      return (
                        <View style={styles.olLiStyle}>
                          <Text style={styles.olLiPrefix}>{index + 1}.</Text>
                          {child}
                        </View>
                      )
                    }
                  )

                  return <View key={passProps.key}>{content}</View>
                },

                ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                  const content = React.Children.map(children, child => {
                    return (
                      <View style={styles.ulLiStyle}>
                        <View style={styles.ulLiPrefix} />
                        {child}
                      </View>
                    )
                  })

                  return (
                    <View key={passProps.key} style={styles.ulTagWrapper}>
                      {content}
                    </View>
                  )
                },
              }}
            />
          ) : null}
        </View>

        {link ? (
          <NextButton
            title={formatMessage(messages.readMoreButtonTitle)}
            onPress={this._readMoreButtonPressed}
          />
        ) : null}
      </View>
    )
  }

  _readMoreButtonPressed = () => {
    const { link, dispatch } = this.props

    dispatch(RouterActions.handleLink(link))
  }
}
