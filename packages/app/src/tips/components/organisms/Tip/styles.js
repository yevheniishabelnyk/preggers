import { StyleSheet, Dimensions } from 'react-native'
import { Constants } from 'expo'

const { width } = Dimensions.get('window')

const gutter = (40 / 375) * width
const ulLiPrefixWidth = (6 / 375) * width

export const imageMaxWidth = width - 2 * gutter

export const contentParagraphStyle = {
  fontSize: (16 / 375) * width,
  lineHeight: (27 / 375) * width,
  fontFamily: 'Roboto-Regular',
  color: 'white',
  textAlign: 'center',
  backgroundColor: 'transparent',
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgb(59,124,255)',
    paddingTop: Constants.statusBarHeight + (21 / 375) * width,
    paddingBottom: (52 / 375) * width,
    paddingHorizontal: gutter,
    alignItems: 'center',
  },

  person: {
    width: (92 / 375) * width,
    height: (92 / 375) * width,
  },

  closeButton: {
    position: 'absolute',
    top: Constants.statusBarHeight + (20 / 375) * width,
    right: (20 / 375) * width,
  },

  redBackground: {
    backgroundColor: 'rgb(250,65,105)',
  },

  content: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
  },

  description: {
    marginTop: (10 / 375) * width,
    justifyContent: 'center',
  },

  title: {
    fontSize: (12 / 375) * width,
    lineHeight: (22 / 375) * width,
    fontFamily: 'Now-Medium',
    color: 'white',
  },

  image: {
    width: (206 / 375) * width,
    height: (154 / 375) * width,
    borderWidth: 5,
    borderColor: 'white',
    marginTop: (15 / 375) * width,
    alignSelf: 'center',
  },

  message: {
    fontSize: (23 / 375) * width,
    lineHeight: (33 / 375) * width,
    fontFamily: 'Now-Medium',
    color: 'white',
    textAlign: 'center',
    marginTop: (12 / 375) * width,
  },

  boldText: {
    color: 'white',
  },

  emText: {
    color: 'white',
  },

  spanText: {
    color: 'white',
  },

  headingText: {
    color: 'white',
    fontFamily: 'Now-Medium',
  },

  olLiStyle: {
    flexDirection: 'row',
    marginTop: (7 / 375) * width,
  },

  ulLiStyle: {
    flexDirection: 'row',
    marginTop: (7 / 375) * width,
  },

  olLiPrefix: {
    fontFamily: 'Roboto-Medium',
    fontSize: (14 / 375) * width,
    color: 'white',
    width: (17 / 375) * width,
    alignSelf: 'flex-start',
  },

  contentParagraph: {
    textAlign: 'center',
  },

  ulLiPrefix: {
    marginRight: (8 / 375) * width,
    width: ulLiPrefixWidth,
    height: ulLiPrefixWidth,
    borderRadius: ulLiPrefixWidth / 2,
    backgroundColor: 'white',
    alignSelf: 'center',
  },

  ulTagWrapper: {
    marginTop: (2 / 375) * width,
  },

  linkText: {
    fontFamily: 'Roboto-Light',
    fontSize: (14 / 375) * width,
    lineHeight: (24 / 375) * width,
    color: 'rgb(41,123,237)',
  },
})

export default styles
