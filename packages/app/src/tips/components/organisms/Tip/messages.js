import { defineMessages } from 'react-intl'

export default defineMessages({
  readMoreButtonTitle: 'Read More',
})
