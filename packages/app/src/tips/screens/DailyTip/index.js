/*
 *
 * Daily Tip screen
 *
 */

import React from 'react'

import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import BaseScreen from '@/app/base/components/BaseScreen'
import Loader from '@/shared/components/Loader'
import Tip from '@/tips/components/organisms/Tip'

import User from '@/app/database/models/User'

import messages from './messages'

import { firebaseConnect } from 'react-redux-firebase'
import { getDailyTip } from '@/app/redux/selectors'

@firebaseConnect(props => {
  const day = props.navigation.getParam('day')

  if (day) {
    return [`content/dailyTips/${day}`]
  }
})
@connect((state, props) => {
  const day = props.navigation.getParam('day')

  if (day) {
    const tip = getDailyTip(state, day)

    return {
      tip,
      isFetching: tip === undefined,
      locale: state.settings.locale,
    }
  }

  return {
    isFetching: true,
  }
})
export default class DailyTipScreen extends BaseScreen {
  static propTypes = {
    tip: PropTypes.object,
    locale: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { isFetching, tip, locale } = this.props

    if (isFetching) {
      return <Loader background="#3b7cff" />
    }

    const { formatMessage } = this.context.intl

    if (tip) {
      const { profile } = this.props.screenProps

      let message, description, image, link

      if (!tip.useMotherTipForAll && User.hasParnterType(profile)) {
        message = tip.partnerMessage ? tip.partnerMessage[locale] : ''
        description = tip.partnerDescription
          ? tip.partnerDescription[locale]
          : ''
        image = tip.partnerImage
        link = tip.partnerLink
      } else {
        message = tip.motherMessage ? tip.motherMessage[locale] : ''
        description = tip.motherDescription ? tip.motherDescription[locale] : ''
        image = tip.motherImage
        link = tip.motherLink
      }

      return (
        <Tip
          title={formatMessage(messages.title)}
          image={image}
          message={message}
          description={description}
          link={link}
          onClose={this._closeButtonPressed}
          dispatch={this.props.dispatch}
        />
      )
    }

    return null
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isFetching !== nextProps.isFetching && !nextProps.tip) {
      this._closeButtonPressed()
    }
  }

  _closeButtonPressed = () => this._goBack()
}
