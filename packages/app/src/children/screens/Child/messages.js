import { defineMessages } from 'react-intl'

export default defineMessages({
  years: 'Years',
  months: 'Months',
  weeks: 'Weeks',
  days: 'Days',
  minutes: 'Minutes',
  hours: 'Hours',
  shareButtonTitle: 'Share',
})
