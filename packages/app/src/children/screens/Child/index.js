/**
 *
 * Child
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Text, Image } from 'react-native'
import Loader from '@/shared/components/Loader'

import BaseScreen from '@/app/base/components/BaseScreen'
import AgeDataItem from '@/children/components/atoms/AgeDataItem'
import DefaultChildPicture from '@/children/components/atoms/DefaultChildPicture'
import ShareButton from '@/pregnancy/components/atoms/ShareButton'

import moment from 'moment'
import formatNumber from 'format-number-with-string'

import { firebaseConnect } from 'react-redux-firebase'

import { getChild } from '@/app/redux/selectors'

import messages from './messages'

import styles from './styles'

const momentDurationFormatSetup = require('moment-duration-format')

momentDurationFormatSetup(moment)

@firebaseConnect(props => {
  const listeners = []

  const childId = props.navigation.getParam('childId')

  if (childId) {
    listeners.push(`children/${childId}`)
  }

  return listeners
})
@connect((state, props) => {
  const childId = props.navigation.getParam('childId')

  if (childId) {
    const child = getChild(state, childId)

    return {
      child,
      isListening: Boolean(
        state.firebase.listeners.byId[`value:/children/${childId}`]
      ),
      isFetching: child === undefined,
      clockMinutes: state.clock.minutes,
    }
  }

  return {
    isFetching: true,
  }
})
export default class Child extends BaseScreen {
  static propTypes = {
    child: PropTypes.object,
    navigation: PropTypes.object,
    firebase: PropTypes.object,
    isFetching: PropTypes.bool,
    isListening: PropTypes.bool,
    clockMinutes: PropTypes.number,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    years: 0,
    months: 0,
    weeks: 0,
    days: 0,
    hours: 0,
    minutes: 0,
  }

  constructor(props) {
    super(props)

    if (props.child) {
      this.state = this._calculateChildAgeData(
        props.child.birthday,
        props.child.birthTime
      )
    }
  }

  render() {
    const { child } = this.props
    const { formatMessage } = this.context.intl

    if (!child) {
      return <Loader />
    }

    return (
      <View style={styles.container}>
        {child.image ? (
          <Image source={child.image} style={styles.photo} />
        ) : (
          <DefaultChildPicture />
        )}

        <View style={styles.nameWrapper}>
          <Text style={styles.name}>{child.name} - </Text>
          <Text style={styles.daysCount}>
            {this.state.allDays} {formatMessage(messages.days).toLowerCase()}
          </Text>
        </View>

        <View style={styles.ageData}>
          <AgeDataItem
            title={formatMessage(messages.years)}
            value={this.state.years}
          />

          <AgeDataItem
            title={formatMessage(messages.months)}
            value={this.state.months}
          />

          <AgeDataItem
            title={formatMessage(messages.weeks)}
            value={this.state.weeks}
          />

          <AgeDataItem
            title={formatMessage(messages.days)}
            value={formatNumber(this.state.days, '### ### ###')}
          />
        </View>

        <View style={[styles.ageData, styles.bottomRow]}>
          <AgeDataItem
            title={formatMessage(messages.hours)}
            value={formatNumber(this.state.hours, '### ### ###')}
          />

          <AgeDataItem
            title={formatMessage(messages.minutes)}
            value={formatNumber(this.state.minutes, '### ### ###')}
          />
        </View>

        <ShareButton
          title={formatMessage(messages.shareButtonTitle)}
          style={styles.shareButton}
          icon
        />
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (
      (!this.props.child && nextProps.child) ||
      (this.props.child &&
        nextProps.child &&
        this.props.child.birthday !== nextProps.child.birthday) ||
      (nextProps.child && this.props.clockMinutes !== nextProps.clockMinutes)
    ) {
      const state = this._calculateChildAgeData(
        nextProps.child.birthday,
        nextProps.child.birthTime
      )

      this.setState(state)
    }

    if (this.props.isListening && !nextProps.isListening) {
      const childId = this.props.navigation.getParam('childId')

      this.props.firebase.watchEvent('value', `children/${childId}`)
    }
  }

  _calculateChildAgeData = (birthday, birthTime) => {
    const nowDate = moment()
    const birthdayDate = moment(birthday + ' ' + birthTime)
    const diff = moment.duration(nowDate.diff(birthdayDate))
    const allDays = Math.floor(diff.asDays())

    const durationInMinutes = Math.floor(diff.asMinutes())

    const timeDurationString = moment
      .duration(durationInMinutes, 'minutes')
      .format('Y ,M ,W ,D ,h, m')

    const timeDurationArray = timeDurationString.split(',')
    const timeDurationArrayLength = timeDurationArray.length

    const minutes = timeDurationArray[timeDurationArrayLength - 1] || 0
    const hours = timeDurationArray[timeDurationArrayLength - 2] || 0
    const days = timeDurationArray[timeDurationArrayLength - 3] || 0
    const weeks = timeDurationArray[timeDurationArrayLength - 4] || 0
    const months = timeDurationArray[timeDurationArrayLength - 5] || 0
    const years = timeDurationArray[timeDurationArrayLength - 6] || 0

    return {
      years,
      months,
      weeks,
      days,
      hours,
      minutes,
      allDays,
    }
  }
}
