import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const photoWidth = moderateScale(182)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: moderateScale(15),
    paddingBottom: moderateScale(29),
    zIndex: 0,
  },

  photo: {
    width: photoWidth,
    height: photoWidth,
    borderRadius: photoWidth / 2,
  },

  nameWrapper: {
    marginTop: moderateScale(40),
    marginBottom: moderateScale(24),
    flexDirection: 'row',
    alignItems: 'center',
  },

  name: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(17),
    color: 'rgb(59, 72, 89)',
  },

  daysCount: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    color: 'rgb(131, 146, 167)',
    marginTop: moderateScale(2),
  },

  ageData: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'center',
  },

  minutesContainer: {
    alignItems: 'center',
    marginTop: moderateScale(20),
  },

  minutesTitle: {
    fontSize: moderateScale(13),
    color: '#703499',
    letterSpacing: 0,
  },

  minutes: {
    color: '#703499',
    letterSpacing: 0,
    marginTop: moderateScale(13),
    fontSize: moderateScale(30),
  },

  bottomRow: {
    marginTop: moderateScale(14),
  },

  shareButton: {
    marginTop: 'auto',
    display: 'none',
  },
})

export default styles
