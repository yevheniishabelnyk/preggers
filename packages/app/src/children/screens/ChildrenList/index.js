/**
 *
 * ChildrenList
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import ActionButton from '@/settings/components/atoms/ActionButton'
import NoChildrenPoster from '@/children/components/atoms/NoChildrenPoster'
import { default as ChildrenListComponent } from '@/children/components/organisms/ChildrenList'

import { keys, has } from 'lodash'

import messages from './messages'

import styles from './styles'
import { firebaseConnect } from 'react-redux-firebase'

@firebaseConnect((props, store) => {
  const listeners = []
  const { profile } = props.screenProps
  const childrenByUser = store.getState().firebase.data.childrenByUser

  if (childrenByUser && childrenByUser[profile.id]) {
    const childIds = keys(childrenByUser[profile.id])

    childIds.forEach(childId => {
      listeners.push(`children/${childId}`)
    })
  }

  return listeners
})
@connect((state, props) => {
  const profileId = props.screenProps.profile.id

  const data = state.firebase.data

  let childrenIds
  let userChildren = []

  if (data.childrenByUser && data.childrenByUser[profileId]) {
    childrenIds = keys(data.childrenByUser[profileId])
  }

  if (childrenIds) {
    childrenIds.forEach(childId => {
      if (has(data, `children.${childId}`) && data.children[childId] !== null) {
        userChildren.push(data.children[childId])
      }
    })
  }

  return {
    userChildren,
  }
})
export default class ChildrenList extends BaseScreen {
  static propTypes = {
    userChildren: PropTypes.array,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { locale } = this.props.screenProps
    const { userChildren } = this.props
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        scrollable
        contentContainerStyle={styles.contentContainerStyle}
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        {userChildren.length === 0 ? (
          <NoChildrenPoster />
        ) : (
          <ChildrenListComponent
            items={userChildren}
            locale={locale}
            onItemPress={this._childPressed}
            style={styles.childrenList}
          />
        )}

        <ActionButton
          title={formatMessage(messages.addChildButtonTitle)}
          onPress={this._addChildButtonPressed}
          style={styles.button}
        />
      </ViewWrapper>
    )
  }

  _addChildButtonPressed = () => this._goTo('/forms-child')

  _childPressed = child =>
    this._goTo('/forms-child', { flow: 'EDIT', childId: child.id })
}
