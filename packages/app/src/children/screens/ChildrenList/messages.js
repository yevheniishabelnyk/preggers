import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'My children',
  addChildButtonTitle: 'Add child',
})
