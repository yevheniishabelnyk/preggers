/*
 *
 * Children screens
 *
 */

import ChildrenList from './screens/ChildrenList'

export default {
  '/children-list': {
    screen: ChildrenList,
  },
}
