/**
 *
 * AgeDataItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'

import styles from './styles'

const AgeDataItem = ({ title, value }) => (
  <View style={styles.ageDataItem}>
    <Text style={styles.itemTitle}>{title.toUpperCase()}</Text>
    <Text style={styles.itemValue}>{value}</Text>
  </View>
)

AgeDataItem.propTypes = {
  title: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}

AgeDataItem.defaultProps = {
  title: '',
}

export default AgeDataItem
