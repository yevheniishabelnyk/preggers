import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  ageDataItem: {
    alignItems: 'center',
    paddingHorizontal: moderateScale(15),
  },

  itemTitle: {
    fontSize: moderateScale(12),
    lineHeight: moderateScale(22),
    color: 'rgb(49, 38, 56)',
    fontFamily: 'Now-Medium',
  },

  itemValue: {
    fontSize: moderateScale(20),
    lineHeight: moderateScale(22),
    color: 'rgb(49, 38, 56)',
    fontFamily: 'Now-Bold',
    marginTop: moderateScale(6),
  },
})

export default styles
