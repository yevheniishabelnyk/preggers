/**
 *
 *
 *
 */

import React from 'react'

import { View, Image } from 'react-native'

import styles from './styles'

const DefaultChildPicture = () => (
  <View style={styles.defaultPictureWrapper}>
    <Image
      source={require('assets/img/baby-gray.png')}
      style={styles.defaultPicture}
    />
  </View>
)

export default DefaultChildPicture
