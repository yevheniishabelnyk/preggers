import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const photoWidth = moderateScale(182)

const styles = StyleSheet.create({
  defaultPictureWrapper: {
    width: photoWidth,
    height: photoWidth,
    borderRadius: photoWidth / 2,
    backgroundColor: 'rgb(249, 250, 252)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  defaultPicture: {
    width: moderateScale(71),
    height: moderateScale(106),
  },
})

export default styles
