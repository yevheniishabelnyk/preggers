import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.5

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  image: {
    width: moderateScale(271, resizeFactor),
    height: moderateScale(227, resizeFactor),
    marginTop: moderateScale(85, resizeFactor),
  },

  text: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15, resizeFactor),
    letterSpacing: 0.8,
    textAlign: 'left',
    color: 'rgb(59,72,89)',
    marginTop: moderateScale(55, resizeFactor),
  },
})

export default styles
