/**
 *
 * NoChildrenPoster
 *
 */

import React from 'react'

import { View, Image } from 'react-native'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class NoChildrenPoster extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('assets/img/empty-children.png')}
          style={styles.image}
          resizeMode="contain"
        />

        <FormattedMessage {...messages.description} style={styles.text} />
      </View>
    )
  }
}
