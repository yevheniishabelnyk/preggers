import { defineMessages } from 'react-intl'

export default defineMessages({
  description: 'No children are added',
})
