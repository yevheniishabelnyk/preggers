import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const imageWidth = moderateScale(48, resizeFactor)

const styles = StyleSheet.create({
  container: {
    height: moderateScale(88, resizeFactor),
    flexDirection: 'row',
    alignItems: 'stretch',
    backgroundColor: 'white',
    borderRadius: 8,
  },

  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
  },

  leftContainer: {
    padding: moderateScale(20, resizeFactor),
  },

  rightContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexGrow: 1,
  },

  image: {
    width: imageWidth,
    height: imageWidth,
    borderRadius: imageWidth / 2,
  },

  imagePlaceholder: {
    width: imageWidth,
    height: imageWidth,
    borderRadius: imageWidth / 2,
    backgroundColor: 'rgb(59,72,89)',
  },

  name: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16, resizeFactor),
    letterSpacing: 1,
    textAlign: 'left',
    color: 'rgb(59,72,89)',
  },

  age: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.9,
    textAlign: 'left',
    color: 'rgb(131,146,167)',
  },
})

export default styles
