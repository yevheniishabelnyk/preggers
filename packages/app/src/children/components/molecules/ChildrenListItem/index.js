/**
 *
 * ChildrenListItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View, Text, Image } from 'react-native'

import humanizeDuration from 'humanize-duration'

import moment from 'moment'

import { isFunction } from 'lodash'

import styles from './styles'

export default class ChildrenListItem extends React.Component {
  static propTypes = {
    child: PropTypes.object,
    onPress: PropTypes.func,
    locale: PropTypes.string,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { child, style, locale } = this.props

    let age = 0

    if (child.birthday) {
      const now = moment()

      let birthTime

      if (child.birthTime) {
        birthTime = child.birthTime
          ? moment(child.birthday, 'YYYY-MM-DD')
              .add(Number(child.birthTime.substring(0, 2)), 'hours')
              .add(Number(child.birthTime.substring(3)), 'minutes')
          : ''
      } else {
        birthTime = moment(child.birthday, 'YYYY-MM-DD')
      }

      age = moment.duration(now.diff(birthTime)).asMilliseconds()
    }

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={this._itemPressed}
        activeOpacity={0.95}
      >
        <View style={styles.content}>
          <View style={styles.leftContainer}>
            {child.image ? (
              <Image source={child.image} style={styles.image} />
            ) : (
              <View style={styles.imagePlaceholder} />
            )}
          </View>

          <View style={styles.rightContainer}>
            <Text style={styles.name}>{child.name}</Text>

            <Text style={styles.age}>
              {humanizeDuration(age, {
                language: locale,
                units: ['y', 'mo', 'w', 'd', 'h', 'm'],
                delimiter: ' ',
                round: true,
                largest: 2,
              })}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _itemPressed = () => {
    const { onPress, child } = this.props

    if (isFunction(onPress)) {
      onPress(child)
    }
  }
}
