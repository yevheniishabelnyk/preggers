import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  listItem: {
    marginTop: moderateScale(13, resizeFactor),
  },
})

export default styles
