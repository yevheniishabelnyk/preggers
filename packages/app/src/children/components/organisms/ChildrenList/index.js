/**
 *
 * ChildrenList
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'

import ChildrenListItem from '@/children/components/molecules/ChildrenListItem'

import styles from './styles'

export default class ChildrenList extends React.Component {
  static propTypes = {
    locale: PropTypes.string,
    items: PropTypes.array,
    onItemPress: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { items, locale, onItemPress, style } = this.props

    return (
      <View style={[styles.container, style]}>
        {items.map(item => (
          <ChildrenListItem
            key={item.id}
            child={item}
            locale={locale}
            style={styles.listItem}
            onPress={onItemPress}
          />
        ))}
      </View>
    )
  }
}
