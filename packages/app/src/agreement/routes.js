/*
 *
 * agreement routes
 *
 */

import AboutUs from './screens/AboutUs'
import TermsPrivacyPolicy from './screens/TermsPrivacyPolicy'
import FAQAnswer from './screens/FAQAnswer'
import FAQSearch from './screens/FAQSearch'

export default {
  '/agreement-about-us': {
    screen: AboutUs,
  },

  '/agreement-terms-privacy-policy': {
    screen: TermsPrivacyPolicy,
  },

  '/agreement-faq-answer': {
    screen: FAQAnswer,
  },

  '/agreement-faq-search': {
    screen: FAQSearch,
  },
}
