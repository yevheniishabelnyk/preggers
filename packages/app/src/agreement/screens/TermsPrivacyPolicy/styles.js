import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: moderateScale(22.5, 2.5),
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  marginTop: {
    marginTop: moderateScale(19, resizeFactor),
  },

  generalTerms: {
    marginBottom: moderateScale(20, resizeFactor),
  },
})

export default styles
