/**
 *
 * General Terms & Privacy Policy
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import HTMLRender from '@/agreement/components/HTMLRender'

import { get } from 'lodash'

import * as RouterActions from '@/app/redux/router/actions'

import { firebaseConnect } from 'react-redux-firebase'

import messages from './messages'

import styles from './styles'

@firebaseConnect(['content/generalTerms', 'content/privacyPolicy'])
@connect(state => ({
  generalTermsArticle: get(
    state.firebase.data,
    `content.generalTerms.description.${state.settings.locale}`
  ),

  privacyPolicyArticle: get(
    state.firebase.data,
    `content.privacyPolicy.description.${state.settings.locale}`
  ),
}))
export default class TermsPrivacyPolicy extends BaseScreen {
  static propTypes = {
    generalTermsArticle: PropTypes.string,
    privacyPolicyArticle: PropTypes.string,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl
    const { generalTermsArticle, privacyPolicyArticle } = this.props

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        {generalTermsArticle ? (
          <HTMLRender
            style={styles.generalTerms}
            content={generalTermsArticle}
            onLinkPress={this._linkPressed}
          />
        ) : null}

        {privacyPolicyArticle ? (
          <HTMLRender
            style={styles.privacyPolicy}
            content={privacyPolicyArticle}
            onLinkPress={this._linkPressed}
          />
        ) : null}
      </ViewWrapper>
    )
  }

  _linkPressed = link => this._(RouterActions.handleLink(link))
}
