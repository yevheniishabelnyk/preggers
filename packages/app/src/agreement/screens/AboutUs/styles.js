import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.6

const gutter = moderateScale(21, resizeFactor)

export const imageMaxWidth = width

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  header: {
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    paddingBottom: gutter * 3,
    alignItems: 'stretch',
    position: 'relative',
  },

  loaderContainer: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 2,
    backgroundColor: 'rgb(249,250,252)',
    overflow: 'hidden',
  },

  loader: {
    position: 'absolute',
    top: 0,
    left: 0,
    width,
    height,
  },

  textWrapper: {
    paddingHorizontal: moderateScale(22.5, 2.5),
  },

  image: {
    width: width,
    height: moderateScale(249, resizeFactor),
    marginBottom: moderateScale(22, resizeFactor),
  },
})

export default styles
