import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Preggers App',
  aboutUsTabTitle: 'About Us',
  policyPolicyTabTitle: 'Privacy policy',
})
