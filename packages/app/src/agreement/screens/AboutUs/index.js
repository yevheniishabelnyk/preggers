/**
 *
 * AboutUs
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import DropDown from '@/shared/components/DropDown'
import GoBackButton from '@/onboarding/components/GoBackButton'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import { ExpoImage } from '@/vendor/cache-manager'

import { firebaseConnect } from 'react-redux-firebase'

import * as RouterActions from '@/app/redux/router/actions'

import { has, get } from 'lodash'

import messages from './messages'

import styles from './styles'

@firebaseConnect(['content/aboutUs'])
@connect(state => ({
  aboutUs: get(state.firebase.data, 'content.aboutUs'),
}))
export default class AboutUs extends BaseScreen {
  static propTypes = {
    aboutUs: PropTypes.object,
    screenProps: PropTypes.shape({
      locale: PropTypes.string,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    imageWidth: 0,
    imageHeight: 0,
  }

  render() {
    const { locale } = this.props.screenProps
    const { aboutUs } = this.props
    const { formatMessage } = this.context.intl

    const image = aboutUs && aboutUs.image ? aboutUs.image : null

    const article = has(aboutUs, `description.${locale}`)
      ? aboutUs.description[locale]
      : ''

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        deviceIndicators="rgb(249,250,252)"
        components={
          !image
            ? {
                header: (
                  <SettingsHeader
                    title={formatMessage(messages.headerTitle)}
                    onBackButtonPress={this._goBack}
                    style={styles.header}
                  />
                ),
              }
            : undefined
        }
        headerHeight={!image ? SettingsHeader.height : undefined}
      >
        {image ? <GoBackButton onPress={this._goBack} /> : null}

        {image ? (
          <ExpoImage source={image} style={[styles.image]} cacheImage />
        ) : null}

        <View style={styles.textWrapper}>
          <DropDown
            title={formatMessage(messages.aboutUsTabTitle)}
            content={article}
            expanded
            onLinkPress={this._linkPressed}
          />
        </View>
      </ViewWrapper>
    )
  }

  _linkPressed = link => this._(RouterActions.handleLink(link))
}
