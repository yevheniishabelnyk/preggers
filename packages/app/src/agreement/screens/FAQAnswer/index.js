/**
 *
 * FAQAnswer
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import Card from '@/shared/components/Card'

import messages from './messages'

import * as RouterActions from '@/app/redux/router/actions'

import styles from './styles'

export default class FAQAnswer extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const {
      navigation: {
        state: { params: { faq = {} } = {} },
      },
    } = this.props
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <Card
          style={styles.marginTop}
          title={faq.question}
          content={faq.answer}
          html
          onLinkPress={this._linkPressed}
        />
      </ViewWrapper>
    )
  }

  _linkPressed = link => this._(RouterActions.handleLink(link))
}
