/**
 *
 * FAQSearch
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import SearchField from '@/forms/components/molecules/SearchField'
import FAQSearchItem from '@/agreement/components/FAQSearchItem'

import * as firebase from 'firebase'

import Fuse from 'fuse.js'

import styles from './styles'

import messages from './messages'

import { values, transform } from 'lodash'

export default class FAQSearch extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  FuseOption = {
    // shouldSort: true,
    // threshold: 0.6,
    // location: 0,
    // distance: 100,
    // maxPatternLength: 32,
    // minMatchCharLength: 1,
    keys: ['question'],
    id: 'id',
  }

  state = {
    faqs: [],
    searchQuery: '',
    results: [],
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <SearchField
          placeholder={formatMessage(messages.searchInputPlaceholder)}
          style={styles.search}
          value={this.state.searchQuery}
          onChange={this._search}
        />

        {this.state.searchQuery
          ? this.state.results.map(id => this.faqsById[id]).map((item, i) => {
              if (item) {
                return (
                  <FAQSearchItem
                    key={i}
                    style={styles.marginBottom}
                    arrowStyle={styles.arrowStyle}
                    item={item}
                    onSelect={this._showAnswer}
                  />
                )
              }
              return null
            })
          : this.state.faqs.map((item, i) => (
              <FAQSearchItem
                key={i}
                style={styles.question}
                arrowStyle={styles.arrowStyle}
                item={item}
                onSelect={this._showAnswer}
              />
            ))}
      </ViewWrapper>
    )
  }

  async componentWillMount() {
    const { locale } = this.props.screenProps

    const faqsSnap = await firebase
      .database()
      .ref('/content/faqs')
      .once('value')

    const faqs = faqsSnap.val()

    this.faqsById = transform(
      faqs,
      (result, value, key) => {
        result[key] = {
          id: value.id,
          question: value.question[locale],
          answer: value.answer[locale],
        }
      },
      {}
    )

    const faqsArray = values(this.faqsById)

    this.fuse = new Fuse(faqsArray, this.FuseOption)

    this.setState({
      faqs: faqsArray,
    })
  }

  _showAnswer = faq => this._goTo('/agreement-faq-answer', { faq })

  _search = query => {
    this.setState({ searchQuery: query, results: this.fuse.search(query) })
  }
}
