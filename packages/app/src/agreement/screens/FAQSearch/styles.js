import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: moderateScale(22.5, 2.5),
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  search: {
    marginBottom: moderateScale(13, resizeFactor),
  },

  question: {
    marginBottom: moderateScale(13, resizeFactor),
  },

  marginBottom: {
    marginBottom: moderateScale(13, resizeFactor),
  },

  arrowStyle: {
    marginRight:
      Platform.OS === 'android' ? moderateScale(5, resizeFactor) : null,
  },
})

export default styles
