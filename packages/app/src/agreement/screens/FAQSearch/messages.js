import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'FAQ',
  searchInputPlaceholder: 'Search',
})
