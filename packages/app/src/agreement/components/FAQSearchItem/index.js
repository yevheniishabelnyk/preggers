/**
 *
 * FAQSearchItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import WhiteFormButton from '@/settings/components/molecules/WhiteFormButton'

import { isFunction } from 'lodash'

export default class FAQSearchItem extends React.Component {
  static propTypes = {
    onSelect: PropTypes.func,
    item: PropTypes.object,
    style: PropTypes.any,
    arrowStyle: PropTypes.any,
  }

  render() {
    const { item, style, arrowStyle } = this.props

    return (
      <WhiteFormButton
        style={style}
        arrowStyle={arrowStyle}
        title={item.question}
        onPress={this._itemPressed}
        arrow
      />
    )
  }

  _itemPressed = () => {
    const { onSelect, item } = this.props

    if (isFunction(onSelect)) {
      onSelect(item)
    }
  }
}
