/*
 *
 * Logo
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Image } from 'react-native'

import styles from './styles'

export default class Logo extends React.Component {
  static propTypes = {
    style: PropTypes.any,
  }

  render() {
    return (
      <Image
        style={[
          styles.logo,
          this.props.style,
          {
            borderRadius: this.props.style.width
              ? this.props.style.width / 2
              : styles.logo.borderRadius,
          },
        ]}
        source={require('assets/icons/logo.png')}
        preferredPixelRatio={2}
      />
    )
  }
}
