import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  logo: {
    width: 121,
    height: 121,
    borderRadius: 60.5,
    overflow: 'hidden',
  },
})

export default styles
