/*
 *
 * FacebookButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View, Image } from 'react-native'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class FacebookButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { style, onPress } = this.props

    return (
      <TouchableOpacity
        style={[styles.button, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <View style={styles.buttonContent}>
          <Image
            style={styles.fbIcon}
            source={require('assets/icons/facebook-white.png')}
          />

          <FormattedMessage {...messages.title} style={styles.title} />
        </View>
      </TouchableOpacity>
    )
  }
}
