import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.5

const buttonWidth = moderateScale(322, resizeFactor)
const buttonHeight = moderateScale(55, resizeFactor)
const buttonBorderRadius = moderateScale(31, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth,
    height: buttonHeight,
    borderRadius: buttonBorderRadius,
    shadowOpacity: 0.1,
    shadowRadius: 8,
    shadowColor: '#111723',
    shadowOffset: { height: 8, width: -2 },
  },

  buttonContent: {
    flexDirection: 'row',
    alignItems: 'center',
    width: buttonWidth,
    height: buttonHeight,
    borderRadius: buttonBorderRadius,
    paddingHorizontal: moderateScale(26, resizeFactor),
    justifyContent: 'center',
    position: 'relative',
    backgroundColor: 'rgb(41, 84, 239)',
  },

  title: {
    fontFamily: 'Now-Medium',
    color: 'white',
    fontSize: moderateScale(16, 0.4),
    backgroundColor: 'transparent',
    letterSpacing: 0,
    marginRight: 'auto',
  },

  fbIcon: {
    marginRight: 'auto',
    width: moderateScale(13, resizeFactor),
    height: moderateScale(23, resizeFactor),
  },
})

export default styles
