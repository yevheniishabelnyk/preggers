import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

export const containerHeight = (30 / 375) * width

const styles = StyleSheet.create({
  container: {
    height: containerHeight,
    width,
    backgroundColor: 'white',
  },

  backIcon: {
    width: (10 / 375) * width,
    height: (16.8 / 375) * width,
  },

  backButton: {
    width: (40 / 375) * width,
    height: containerHeight,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default styles
