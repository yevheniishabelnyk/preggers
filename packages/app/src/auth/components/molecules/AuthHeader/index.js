/**
 *
 * AuthHeader
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableHighlight, Image } from 'react-native'

import styles, { containerHeight } from './styles'

export default class AuthHeader extends React.Component {
  static propTypes = {
    onBackButtonPress: PropTypes.func,
  }

  static height = containerHeight

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.backButton}
          underlayColor="rgba(255,255,255,.5)"
          onPress={this.props.onBackButtonPress}
        >
          <Image
            style={styles.backIcon}
            source={require('assets/icons/back.png')}
          />
        </TouchableHighlight>
      </View>
    )
  }
}
