/**
 *
 * TextField
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TextInput, Animated, Image, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

import { isFunction } from 'lodash'

import styles from './styles'

const labelAnimatedTopOutputRange = [moderateScale(21), moderateScale(11)]
const labelAnimatedFontSizeOutputRange = [moderateScale(14), moderateScale(10)]

export default class TextField extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    secureTextEntry: PropTypes.bool,
    required: PropTypes.bool,
    error: PropTypes.bool,
    autoCapitalize: PropTypes.string,
    placeholder: PropTypes.string,
    defaultValue: PropTypes.string,
    keyboardType: PropTypes.string,
    style: PropTypes.any,
    onChangeText: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onKeyPress: PropTypes.func,
    autoCorrect: PropTypes.bool,
    clearTextOnFocus: PropTypes.bool,
    selectTextOnFocus: PropTypes.bool,
    enablesReturnKeyAutomatically: PropTypes.bool,
    returnKeyType: PropTypes.string,
    title: PropTypes.string,
    value: PropTypes.string,
  }

  state = {
    isFocused: false,
  }

  constructor(props) {
    super(props)

    this._animatedIsFocused = new Animated.Value(props.value === '' ? 0 : 1)
  }

  render() {
    const { title, value, style } = this.props

    const labelStyle = {
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: labelAnimatedTopOutputRange,
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: labelAnimatedFontSizeOutputRange,
      }),
      marginTop: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [
          0,
          Platform.OS === 'android' ? moderateScale(-2) : moderateScale(0),
        ],
      }),
    }

    return (
      <View style={[styles.container, style]}>
        <View
          style={[
            styles.inputWrapper,
            this.props.style,
            this.props.error ? styles.inputError : null,
          ]}
        >
          {title ? (
            <Animated.Text
              style={[
                styles.label,
                labelStyle,
                this.props.error ? styles.labelError : null,
              ]}
            >
              {title}
            </Animated.Text>
          ) : null}

          <TextInput
            underlineColorAndroid="transparent"
            style={styles.input}
            keyboardType={this.props.keyboardType}
            autoCapitalize={this.props.autoCapitalize}
            secureTextEntry={this.props.secureTextEntry}
            value={value}
            placeholderTextColor="rgb(131, 146, 167)"
            onChangeText={this.props.onChangeText}
            autoCorrect={this.props.autoCorrect}
            onFocus={this._onFocus}
            onBlur={this._onBlur}
            clearTextOnFocus={this.props.clearTextOnFocus}
            selectTextOnFocus={this.props.selectTextOnFocus}
            returnKeyType={this.props.returnKeyType}
            onSubmitEditing={this.props.onSubmitEditing}
            enablesReturnKeyAutomatically={
              this.props.enablesReturnKeyAutomatically
            }
            onKeyPress={this.props.onKeyPress}
          />

          {this.props.error ? (
            <Image
              source={require('assets/icons/error-icon.png')}
              style={[styles.errorIcon]}
            />
          ) : null}
        </View>
      </View>
    )
  }

  componentWillMount() {
    const { value } = this.props
    let newValue

    if (value === '' || value === null || value === undefined) {
      newValue = ''
    }

    this._animatedIsFocused = new Animated.Value(newValue === '' ? 0 : 1)
  }

  componentDidUpdate() {
    const { value } = this.props

    let newValue

    if (value === '' || value === null || value === undefined) {
      newValue = ''
    }

    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || newValue !== '' ? 1 : 0,
      duration: 200,
    }).start()
  }

  _onFocus = () => {
    this.setState({ isFocused: true })

    if (isFunction(this.props.onFocus)) {
      this.props.onFocus()
    }
  }

  _onBlur = () => {
    this.setState({ isFocused: false })

    if (isFunction(this.props.onBlur)) {
      this.props.onBlur()
    }
  }
}
