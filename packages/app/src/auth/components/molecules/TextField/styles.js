import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const containerHeight = moderateScale(60)

const containerWidth = moderateScale(330)

const styles = StyleSheet.create({
  style: { marginVertical: 8 },

  inputWrapper: {
    width: containerWidth,
    height: containerHeight,
    borderRadius: moderateScale(8),
    justifyContent: 'center',
    backgroundColor: 'white',
    overflow: 'visible',
  },

  input: {
    width: moderateScale(330),
    height: moderateScale(45),
    color: '#000',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    paddingRight: moderateScale(40),
    paddingLeft: moderateScale(18),
    marginBottom: -8,
  },

  label: {
    fontFamily: 'Now-Medium',
    color: 'rgb(131, 146, 167)',
    marginTop: moderateScale(5),
    marginLeft: moderateScale(18),
  },

  labelError: {
    color: 'rgb(250,65,105)',
  },

  errorText: {
    color: 'rgb(255, 100, 101)',
  },

  errorIcon: {
    position: 'absolute',
    width: moderateScale(20),
    height: moderateScale(20),
    top: moderateScale(19),
    right: moderateScale(18),
  },
})

export default styles
