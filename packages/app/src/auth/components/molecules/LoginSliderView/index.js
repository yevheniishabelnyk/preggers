/**
 *
 * Login slider view
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import { Video } from 'expo'

import ViewportText from '@/shared/components/ViewportText'

import styles from './styles'

export default class LoginSliderView extends React.Component {
  static propTypes = {
    video: PropTypes.any.isRequired,
    videoStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object])
      .isRequired,
    text: PropTypes.string.isRequired,
  }

  render() {
    const { video, videoStyle, text } = this.props

    return (
      <View style={styles.container}>
        <Video
          style={[styles.video, videoStyle]}
          source={video}
          resizeMode="contain"
          shouldPlay
          isLooping
        />

        <ViewportText text={text} textStyle={styles.title} alignCenter />
      </View>
    )
  }
}
