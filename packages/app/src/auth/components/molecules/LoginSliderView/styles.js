import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

import {
  indicatorSize,
  indicatorMargin,
} from '@/shared/components/Slider/styles'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    position: 'relative',
    paddingBottom: indicatorSize + indicatorMargin,
  },

  title: {
    fontFamily: 'Now-Regular',
    paddingHorizontal: moderateScale(36, 2.5),
    textAlign: 'center',
    fontSize: moderateScale(23),
    lineHeight: moderateScale(33),
    color: 'rgb(23, 24, 28)',
    paddingBottom: moderateScale(14),
    zIndex: 1,
  },

  video: {
    height: moderateScale(215, 0.7),
    width: moderateScale(215, 0.7),
    marginTop: moderateScale(55),
    zIndex: 1,
  },
})

export default styles
