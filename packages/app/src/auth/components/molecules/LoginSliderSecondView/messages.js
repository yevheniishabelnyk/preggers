import { defineMessages } from 'react-intl'

export default defineMessages({
  title:
    'Follow your weekly progress and get push-notifications when something important happens.',
})
