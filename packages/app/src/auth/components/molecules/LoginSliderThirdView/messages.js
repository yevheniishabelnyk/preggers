import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Connect with someone you love and share your journey together.',
})
