import { defineMessages } from 'react-intl'

export default defineMessages({
  title:
    'And of course a lot of other smart features like a checklist, food recommendations, name list and so on.',
})
