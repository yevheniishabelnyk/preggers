/**
 *
 * Login slider second view
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import messages from './messages'

import LoginSliderView from '@/auth/components/molecules/LoginSliderView'

import styles from './styles'

export default class LoginSliderThirdView extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <LoginSliderView
        video={require('assets/videos/slide-4.mp4')}
        videoStyle={styles.video}
        text={formatMessage(messages.title)}
      />
    )
  }
}
