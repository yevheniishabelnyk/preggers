import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  video: {
    height: moderateScale(215, 0.7),
    width: moderateScale(215, 0.7),
    marginTop: moderateScale(55),
  },
})

export default styles
