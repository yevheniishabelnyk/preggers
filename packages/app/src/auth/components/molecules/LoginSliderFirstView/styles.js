import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    position: 'relative',
  },

  bgImage: {
    width,
    height,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 0,
  },

  logo: {
    marginTop: moderateScale(115),
    width: moderateScale(85),
    height: moderateScale(85),
  },

  brand: {
    height: moderateScale(70, 0.8),
    width: moderateScale(137, 0.8),
    marginTop: moderateScale(14, 1.5),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(23),
    color: 'white',
    letterSpacing: 0,
    lineHeight: moderateScale(33),
    backgroundColor: 'transparent',
    paddingHorizontal: moderateScale(36, 2.5),
    textAlign: 'center',
  },
})

export default styles
