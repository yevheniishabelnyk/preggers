import { defineMessages } from 'react-intl'

export default defineMessages({
  title:
    'The complete app for your pregnancy and the first time with your child!',
})
