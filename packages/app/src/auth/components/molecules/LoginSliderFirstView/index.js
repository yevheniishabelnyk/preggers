/**
 *
 * Login slider first view
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image } from 'react-native'

import Logo from '@/auth/components/atoms/Logo'

import messages from './messages'

import ViewportText from '@/shared/components/ViewportText'

import styles from './styles'
import { getImageByDeviceType } from '@/app/scaling'

const bgImage = getImageByDeviceType({
  phone: require('assets/img/pregnant-woman.png'),
  tablet: require('assets/img/pregnant-woman-and-man.png'),
})

export default class LoginSliderFirstView extends React.Component {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <Image style={styles.bgImage} source={bgImage} />

        <Logo style={styles.logo} />

        <Image
          style={styles.brand}
          source={require('assets/icons/brand-white.png')}
          resizeMode="contain"
        />

        <ViewportText
          text={formatMessage(messages.title)}
          textStyle={styles.title}
          alignCenter
        />
      </View>
    )
  }
}
