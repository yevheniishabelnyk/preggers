import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  video: {
    height: moderateScale(275, 0.7),
    width: moderateScale(275, 0.7),
    marginTop: moderateScale(-10),
  },
})

export default styles
