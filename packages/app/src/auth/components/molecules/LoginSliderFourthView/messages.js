import { defineMessages } from 'react-intl'

export default defineMessages({
  title:
    'In the knowledgebank you find a lot of helpful information from articles, movies and podcasts.',
})
