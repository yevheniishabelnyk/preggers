/*
 *
 * Skreen flow routes
 *
 */

import Login from './screens/Login'
import LoginWithEmail from './screens/LoginWithEmail'
import ResetPassword from './screens/ResetPassword'
import ResetPasswordFeedback from './screens/ResetPasswordFeedback'
import FacebookLogin from './screens/FacebookLogin'
import EnterEmail from './screens/EnterEmail'

export default {
  '/auth-login': {
    screen: Login,
    navigationOptions: {
      gesturesEnabled: false,
    },
  },

  '/auth-enter-email': {
    screen: EnterEmail,
  },

  '/auth-login-with-email': {
    screen: LoginWithEmail,
  },

  '/auth-reset-password': {
    screen: ResetPassword,
  },

  '/auth-reset-password-feedback': {
    screen: ResetPasswordFeedback,
  },

  '/auth-facebook-login': {
    screen: FacebookLogin,
  },
}
