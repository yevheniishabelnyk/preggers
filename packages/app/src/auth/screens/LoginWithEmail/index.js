/*
 *
 * LoginWithEmail screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Keyboard, View, TouchableHighlight, Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import TextField from '@/auth/components/molecules/TextField'
import ActionButton from '@/settings/components/atoms/ActionButton'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import ViewportText from '@/shared/components/ViewportText'
import FormError from '@/forms/components/atoms/FormError'

import { authenticate } from '@/app/auth'

import { isValidEmail } from '@/shared/utils/validators'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class LoginWithEmail extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    const { params: { email } = {} } = props.navigation.state

    this.state = {
      email: email || '',
      password: '',
      errorText: '',
      emailError: false,
      passwordError: false,
      isRequesting: false,
      passwordMinLength: 8,
    }
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isRequesting } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboard
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <FormattedMessage
          {...messages.headerTitle}
          style={styles.headerTitle}
        />

        <Image
          style={styles.image}
          source={require('assets/img/email.png')}
          resizeMode="contain"
        />

        <FormattedMessage {...messages.title} style={styles.title} />

        <View style={styles.descriptionWrapper}>
          <ViewportText
            text={formatMessage(messages.description)}
            textStyle={styles.description}
            alignCenter
          />
        </View>

        <View style={styles.form}>
          <TextField
            title={formatMessage(messages.emailFieldPlaceholder)}
            keyboardType="email-address"
            autoCapitalize="none"
            onChangeText={this._setEmail}
            value={this.state.email}
            error={this.state.emailError}
            autoCorrect={false}
            onBlur={this._emailFieldLostFocus}
            returnKeyType="go"
            enablesReturnKeyAutomatically
            onKeyPress={this._handleKeyDown}
            onSubmitEditing={this._loginButtonPressed}
          />

          <View style={styles.bottomFieldWrapper}>
            <TextField
              title={formatMessage(messages.passwordFieldPlaceholder)}
              autoCapitalize="none"
              secureTextEntry
              onChangeText={this._setPassword}
              value={this.state.password}
              error={this.state.passwordError}
              autoCorrect={false}
              onBlur={this._passwordFieldLostFocus}
              selectTextOnFocus
              returnKeyType="go"
              enablesReturnKeyAutomatically
              onKeyPress={this._handleKeyDown}
              onSubmitEditing={this._loginButtonPressed}
            />

            <FormError
              isVisible={this.state.passwordError || this.state.emailError}
              title={this.state.errorText}
              style={styles.error}
            />
          </View>
        </View>

        <View style={styles.buttonContainer}>
          <ActionButton
            title={formatMessage(messages.loginButtonTitle)}
            onPress={this._loginButtonPressed}
            style={styles.loginButton}
            loader={isRequesting}
          />
        </View>

        <View style={styles.resetPasswordButtonContainer}>
          <TouchableHighlight
            onPress={this._resetPasswordButtonPressed}
            underlayColor="rgba(255,255,255,.5)"
          >
            <FormattedMessage
              {...messages.resetPasswordButtonTitle}
              style={styles.resetPasswordButtonTitle}
            />
          </TouchableHighlight>
        </View>
      </ViewWrapper>
    )
  }

  _setValue = (key, value) => {
    const emailError = key === 'email' ? false : this.state.emailError
    const passwordError = key === 'password' ? false : this.state.passwordError

    this.setState({
      [key]: value,
      emailError,
      passwordError,
      errorText: null,
    })
  }

  _setEmail = value => {
    this._setValue('email', value)
  }

  _setPassword = value => {
    this._setValue('password', value)
  }

  _loginButtonPressed = async () => {
    const {
      email,
      password,
      emailError,
      passwordError,
      errorText,
      passwordMinLength,
    } = this.state
    const { params: { linkCredential } = {} } = this.props.navigation.state

    if (
      email &&
      password &&
      !emailError &&
      !passwordError &&
      password.length >= passwordMinLength
    ) {
      this.setState({ isRequesting: true })

      Keyboard.dismiss()

      try {
        await this._(
          authenticate({
            providerId: 'password',
            email,
            password,
            linkCredential,
          })
        )
      } catch (err) {
        console.info('err: ', err)

        this.setState({ isRequesting: false })

        if (err.code === 'auth/account-exists-with-different-credential') {
          this._goTo('/auth-facebook-login', { linkCredential: err.credential })
        } else if (err.code === 'auth/wrong-password') {
          const { formatMessage } = this.context.intl

          this.setState({
            errorText: formatMessage(messages.errorWrongPassword),
            passwordError: true,
          })
        }
      }
    } else if (!errorText) {
      this._passwordFieldLostFocus()
      this._emailFieldLostFocus()

      this.setState({ isRequesting: false })
    }
  }

  _resetPasswordButtonPressed = () => {
    const { email } = this.state

    this._goTo('/auth-reset-password', {
      email: isValidEmail(email) ? email : null,
    })
  }

  _passwordFieldLostFocus = () => {
    const { password, passwordMinLength } = this.state
    const { formatMessage } = this.context.intl

    if (!password) {
      this.setState({
        errorText: formatMessage(messages.errorRequiredField),
        passwordError: !password,
      })
    } else if (password.length < passwordMinLength) {
      this.setState({
        errorText: formatMessage(messages.errorPasswordMinLength),
        passwordError: true,
      })
    }
  }

  _emailFieldLostFocus = () => {
    const { email } = this.state
    const { formatMessage } = this.context.intl

    if (!email) {
      this.setState({
        errorText: formatMessage(messages.errorRequiredField),
        emailError: !email,
      })
    } else if (!isValidEmail(email)) {
      this.setState({
        errorText: formatMessage(messages.errorInvalidEmail),
        emailError: true,
      })
    }
  }

  _handleKeyDown = e => {
    if (e.nativeEvent.key === 'Enter') {
      Keyboard.dismiss()
      this._emailFieldLostFocus()
      this._passwordFieldLostFocus()
      setTimeout(() => this._loginButtonPressed(), 0)
    }
  }
}
