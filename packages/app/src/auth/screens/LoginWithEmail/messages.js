import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Log in with e-mail',
  title: 'Log in',
  description:
    "Fill in your e-mail and your password. If you don't already have an account, an account will be created.",
  emailFieldPlaceholder: 'E-mail',
  passwordFieldPlaceholder: 'Password (min 8 ch)',
  loginButtonTitle: 'Log in',
  resetPasswordButtonTitle: 'Forgot password?',
  errorRequiredField: 'Field is required',
  errorPasswordMinLength: 'Password must be minimum 8 characters',
  errorWrongPassword: 'Wrong password',
  errorInvalidEmail: 'E-mail is not valid',
})
