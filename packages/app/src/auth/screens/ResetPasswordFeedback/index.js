/*
 *
 * ResetPasswordFeedback screen
 *
 */

import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Text, Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GradientButton from '@/shared/components/GradientButton'

import { findIndex } from 'lodash'
import { NavigationActions } from 'react-navigation'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@connect(({ router }) => ({
  router,
}))
export default class ResetPasswordFeedback extends BaseScreen {
  static propTypes = {
    router: PropTypes.object,
  }

  static navigationOptions = {
    gesturesEnabled: false,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { params: { email } = {} } = this.props.navigation.state
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContaierStyle={styles.contentContaier}
        keyboard
        deviceIndicators="white"
      >
        <Image
          style={styles.image}
          source={require('assets/img/green-checkbox.png')}
          resizeMode="contain"
        />

        <FormattedMessage {...messages.title} style={styles.title} />

        <Text style={styles.email}>{email ? ' ' + email : ''}</Text>

        <FormattedMessage
          {...messages.description}
          style={styles.description}
        />

        <GradientButton
          title={formatMessage(messages.backToLoginButtonTitle)}
          onPress={this._backToLogin}
          style={styles.backToLoginButton}
        />
      </ViewWrapper>
    )
  }

  _backToLogin = () => {
    const { router } = this.props
    const { params: { email } = {} } = this.props.navigation.state

    const nextScreenName = '/auth-login-with-email'

    const nextScreenIndex = findIndex(router.routes, {
      routeName: nextScreenName,
    })

    this._(
      NavigationActions.reset({
        index: nextScreenIndex !== -1 ? nextScreenIndex : 0,
        actions:
          nextScreenIndex !== -1
            ? router.routes.slice(0, nextScreenIndex + 1).map(route => {
                if (route.routeName === nextScreenName) {
                  return NavigationActions.navigate(
                    Object.assign(route, { params: { email } })
                  )
                }
                return NavigationActions.navigate(route)
              })
            : [
                NavigationActions.navigate({
                  routeName: nextScreenName,
                  params: { email },
                }),
              ],
      })
    )
  }
}
