import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  contentContaier: {
    alignItems: 'center',
    paddingBottom: moderateScale(55),
  },

  image: {
    width: moderateScale(120),
    height: moderateScale(120),
    marginTop: moderateScale(103),
  },

  title: {
    fontFamily: 'Helvetica',
    fontSize: moderateScale(20),
    color: '#2B3857',
    letterSpacing: 0,
    lineHeight: moderateScale(32),
    marginTop: moderateScale(26),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },

  email: {
    fontFamily: 'Helvetica',
    fontSize: moderateScale(20),
    letterSpacing: 0,
    lineHeight: moderateScale(32),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: '#40E0BE',
  },

  description: {
    fontSize: moderateScale(13),
    color: '#2B3857',
    textAlign: 'center',
    letterSpacing: 0,
    lineHeight: moderateScale(24),
    marginTop: moderateScale(15),
    marginBottom: moderateScale(30),
  },

  backToLoginButton: {
    marginTop: 'auto',
    marginBottom: moderateScale(55),
  },
})

export default styles
