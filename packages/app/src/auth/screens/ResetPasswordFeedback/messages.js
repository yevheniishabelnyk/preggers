import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Thank you, an e-mail has been sent to',
  description:
    'You need to click the link in your e-mail to reset your password',
  backToLoginButtonTitle: 'Back to login',
})
