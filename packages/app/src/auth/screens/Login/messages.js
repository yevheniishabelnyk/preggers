import { defineMessages } from 'react-intl'

export default defineMessages({
  signinWithEmail: 'Log in with e-mail',
  testToolsLinkTitle: 'Test our tools',
  privacyPolicy:
    'By signing in, you agree to our Terms of service and Privacy Policy',
})
