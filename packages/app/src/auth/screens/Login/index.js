/*
 *
 * Login screen
 *
 */

import React from 'react'

import { View, TouchableHighlight } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'

import FacebookButton from '@/auth/components/atoms/FacebookButton'
import Slider from '@/shared/components/Slider'
import LoginSliderFirstView from '@/auth/components/molecules/LoginSliderFirstView'
import LoginSliderSecondView from '@/auth/components/molecules/LoginSliderSecondView'
import LoginSliderThirdView from '@/auth/components/molecules/LoginSliderThirdView'
import LoginSliderFourthView from '@/auth/components/molecules/LoginSliderFourthView'
import LoginSliderFifthView from '@/auth/components/molecules/LoginSliderFifthView'

import { authenticate } from '@/app/auth'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { footerHeight } from '@/auth/screens/Login/styles'
import styles from './styles'

import { firebaseConnect } from 'react-redux-firebase'

@firebaseConnect(['content/generalTerms', 'content/privacyPolicy'])
export default class Login extends BaseScreen {
  state = {
    whiteBackground: false,
  }

  render() {
    return (
      <View style={styles.container}>
        <Slider
          scrollContainerStyle={{ paddingBottom: footerHeight }}
          slides={[
            <LoginSliderFirstView key="first_slide" />,
            <LoginSliderSecondView key="second_slide" />,
            <LoginSliderThirdView key="third_slide" />,
            <LoginSliderFourthView key="forth_slide" />,
            <LoginSliderFifthView key="fifth_slide" />,
          ]}
          onChange={this._slideChanged}
          indicators
        />

        <View style={styles.footer}>
          <FacebookButton
            onPress={this._loginWithFacebook}
            style={styles.fbButton}
          />

          <TouchableHighlight
            onPress={this._loginWithEmail}
            underlayColor="transparent"
          >
            <FormattedMessage
              {...messages.signinWithEmail}
              style={[
                styles.loginWithEmail,
                this.state.whiteBackground ? styles.loginWithEmailBlack : null,
              ]}
            />
          </TouchableHighlight>

          <TouchableHighlight
            onPress={this._openTerms}
            underlayColor="transparent"
          >
            <FormattedMessage
              {...messages.privacyPolicy}
              style={[
                styles.privacyPolicy,
                this.state.whiteBackground
                  ? styles.privacyPolicyGrayColor
                  : null,
              ]}
            />
          </TouchableHighlight>

          <TouchableHighlight
            onPress={this._testTools}
            underlayColor="transparent"
          >
            <FormattedMessage
              {...messages.testToolsLinkTitle}
              style={[
                styles.testToolsLink,
                this.state.whiteBackground
                  ? styles.privacyPolicyGrayColor
                  : null,
              ]}
            />
          </TouchableHighlight>
        </View>
      </View>
    )
  }

  _loginWithFacebook = async () => {
    try {
      await this._(
        authenticate({
          providerId: 'facebook.com',
        })
      )
    } catch (err) {
      console.info('err: ', err)

      if (err.code === 'auth/account-exists-with-different-credential') {
        this._goTo('/auth-login-with-email', {
          email: err.email,
          linkCredential: err.credential,
        })
      }
    }
  }

  _loginWithEmail = () => this._goTo('/auth-login-with-email')

  _testTools = () => this._goTo('/tools-menu', { IS_PUBLIC: true })

  _slideChanged = index => this.setState({ whiteBackground: index !== 0 })

  _openTerms = () => this._goTo('/agreement-terms-privacy-policy')
}
