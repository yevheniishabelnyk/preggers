import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const footerHeight = moderateScale(190)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: 'white',
  },

  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    backgroundColor: 'white',
  },

  fbButton: {
    marginBottom: moderateScale(22),
  },

  loginWithEmail: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16),
    color: 'white',
    letterSpacing: 0,
    backgroundColor: 'transparent',
    marginBottom: moderateScale(12),
  },

  loginWithEmailBlack: {
    color: '#554949',
  },

  privacyPolicy: {
    width: moderateScale(185),
    height: moderateScale(28),
    backgroundColor: 'transparent',
    fontSize: moderateScale(10),
    color: 'rgb(195, 205, 229)',
    letterSpacing: 0,
    lineHeight: moderateScale(14),
    textAlign: 'center',
    fontFamily: 'Now-Medium',
  },

  testToolsLink: {
    backgroundColor: 'transparent',
    fontSize: moderateScale(14),
    color: 'rgb(195, 205, 229)',
    letterSpacing: 0,
    textAlign: 'center',
    marginTop: moderateScale(8),
    marginBottom: moderateScale(20),
    fontFamily: 'Now-Medium',
  },

  privacyPolicyGrayColor: {
    color: 'rgb(122, 131, 155)',
  },

  footer: {
    position: 'absolute',
    height: footerHeight,
    bottom: 0,
    left: 0,
    right: 0,
    flexShrink: 0,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
})

export default styles
