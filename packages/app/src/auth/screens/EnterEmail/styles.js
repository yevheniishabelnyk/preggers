import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const gutter = 22

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 252)',
  },

  contentContainer: {
    paddingHorizontal: moderateScale(gutter),
    alignItems: 'center',
    paddingBottom: moderateScale(45),
  },

  buttonContainer: {
    width: moderateScale(375 - gutter * 2, 0.4),
    marginTop: 'auto',
    alignItems: 'center',
  },

  form: {
    justifyContent: 'center',
    marginBottom: moderateScale(35),
  },

  headerTitle: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(13),
    color: 'rgb(59, 72, 89)',
    marginTop: moderateScale(33),
    textAlign: 'center',
  },

  image: {
    width: moderateScale(136, 0.5),
    height: moderateScale(152, 0.5),
    marginTop: moderateScale(20, 1.5),
  },

  title: {
    width: moderateScale(262, 1.2),
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(23, 0.3),
    lineHeight: moderateScale(33, 0.3),
    color: 'rgb(23, 24, 28)',
    letterSpacing: 0,
    textAlign: 'center',
    marginTop: moderateScale(18),
  },

  descriptionWrapper: {
    marginVertical: moderateScale(7),
    flex: 1,
    alignSelf: 'stretch',
    paddingHorizontal: 10,
    maxHeight: moderateScale(200),
  },

  description: {
    alignSelf: 'stretch',
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(18, 0.25),
    lineHeight: moderateScale(27, 0.3),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    letterSpacing: 0,
  },

  error: {
    position: 'absolute',
    marginTop: moderateScale(68),
    marginLeft: moderateScale(20),
  },

  errorText: {
    color: 'rgb(255, 100, 101)',
  },

  errorIcon: {
    position: 'absolute',
    width: moderateScale(20),
    height: moderateScale(20),
    top: moderateScale(19),
    right: moderateScale(18),
  },
})

export default styles
