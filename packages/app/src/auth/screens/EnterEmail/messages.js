import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Create account',
  title: 'We need your e-mail',
  description:
    'Seems like your facebook account is not connected to an e-mail. We need your e-mail for the app to work.',
  emailFieldPlaceholder: 'E-mail',
  nextButtonTitle: 'Next',
  errorRequiredEmail: 'E-mail is required',
  errorNotValidEmail: 'E-mail is not valid',
})
