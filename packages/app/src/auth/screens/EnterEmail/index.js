/*
 *
 * EnterEmail screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Keyboard, View, Image } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import TextField from '@/auth/components/molecules/TextField'
import ActionButton from '@/settings/components/atoms/ActionButton'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import ViewportText from '@/shared/components/ViewportText'
import FormError from '@/forms/components/atoms/FormError'

import * as RouterActions from '@/app/redux/router/actions'

import { isValidEmail } from '@/shared/utils/validators'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import { isFunction } from 'lodash'

import { withFirebase } from 'react-redux-firebase'

import styles from './styles'

@withFirebase
@connect(state => ({ auth: state.firebase.auth }))
export default class EnterEmail extends BaseScreen {
  static propTypes = {
    auth: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static navigationOptions = {
    gesturesEnabled: false,
  }

  state = {
    email: '',
    error: false,
    errorMessage: '',
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboard
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBackButtonPressed} />

        <FormattedMessage
          {...messages.headerTitle}
          style={styles.headerTitle}
        />

        <Image
          style={styles.image}
          source={require('assets/img/email.png')}
          resizeMode="contain"
        />

        <FormattedMessage {...messages.title} style={styles.title} />

        <View style={styles.descriptionWrapper}>
          <ViewportText
            text={formatMessage(messages.description)}
            textStyle={styles.description}
            alignCenter
          />
        </View>

        <View style={styles.form}>
          <View>
            <TextField
              title={formatMessage(messages.emailFieldPlaceholder)}
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={this._setEmail}
              defaultValue={this.state.email}
              required
              error={Boolean(this.state.error)}
              autoCorrect={false}
              onBlur={this._emailFieldLostFocus}
              returnKeyType="go"
              enablesReturnKeyAutomatically
              onKeyPress={this._handleKeyDown}
              onSubmitEditing={this._nextButtonPressed}
            />

            <FormError
              isVisible={this.state.error}
              title={this.state.errorMessage}
              style={styles.error}
            />
          </View>
        </View>

        <View style={styles.buttonContainer}>
          <ActionButton
            title={formatMessage(messages.nextButtonTitle)}
            onPress={this._nextButtonPressed}
            style={styles.loginButton}
          />
        </View>
      </ViewWrapper>
    )
  }

  _setEmail = value =>
    this.setState({
      email: value,
      error: false,
    })

  _goBackButtonPressed = () => {
    const onDecline = this.props.navigation.getParam('onDecline')

    if (isFunction(onDecline)) {
      this._goBack()

      onDecline()
    } else {
      const { auth, firebase } = this.props

      if (!auth.isEmpty) {
        firebase.logout()
      } else {
        this._(RouterActions.resetToNonAuthenitcatedInitialRoute())
      }
    }
  }

  _nextButtonPressed = async () => {
    const { navigation } = this.props
    const { email } = this.state

    const onDone = navigation.getParam('onDone')

    if (email && isValidEmail(email)) {
      if (isFunction(onDone)) {
        onDone(email)
      } else {
        const { auth, firebase } = this.props

        if (!auth.isEmpty) {
          firebase.logout()
        } else {
          this._(RouterActions.resetToNonAuthenitcatedInitialRoute())
        }
      }
    } else {
      this._validateForm()
    }
  }

  _validateForm = () => {
    const { email } = this.state
    const { formatMessage } = this.context.intl

    if (!email) {
      this.setState({
        error: true,
        errorMessage: formatMessage(messages.errorRequiredEmail),
      })
    } else if (!isValidEmail(email)) {
      this.setState({
        error: true,
        errorMessage: formatMessage(messages.errorNotValidEmail),
      })
    }
  }

  _handleKeyDown = e => {
    if (e.nativeEvent.key === 'Enter') {
      Keyboard.dismiss()

      this._validateForm()

      setTimeout(() => this._nextButtonPressed(), 0)
    }
  }
}
