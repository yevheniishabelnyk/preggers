import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(23, 0.3),
    lineHeight: moderateScale(33, 0.3),
    color: 'rgb(23, 24, 28)',
    letterSpacing: 0,
    textAlign: 'center',
    marginTop: moderateScale(97),
  },

  fbButton: {
    alignSelf: 'center',
    marginBottom: moderateScale(55),
    marginTop: moderateScale(35),
  },

  description: {
    alignSelf: 'center',
    width: moderateScale(307, 0.7),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18, 0.25),
    lineHeight: moderateScale(27, 0.3),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    marginTop: moderateScale(24),
  },
})

export default styles
