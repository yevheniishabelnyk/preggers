import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Facebook login',
  description:
    'To add e-mail and password authentication please first login with you Facebook account',
})
