/*
 *
 * FacebookLogin screen
 *
 */

import React from 'react'

import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import FacebookButton from '@/auth/components/atoms/FacebookButton'
import AuthHeader from '@/auth/components/molecules/AuthHeader'
import GoBackButton from '@/onboarding/components/GoBackButton'

import { authenticate } from '@/app/auth'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class FacebookLogin extends BaseScreen {
  render() {
    return (
      <ViewWrapper
        mainStyle={styles.container}
        keyboard
        deviceIndicators="white"
        headerHeight={AuthHeader.height}
      >
        <GoBackButton onPress={this._goBack} />

        <FormattedMessage {...messages.title} style={styles.title} />

        <FormattedMessage
          {...messages.description}
          style={styles.description}
        />

        <FacebookButton
          onPress={this._facebookButtonPressed}
          style={styles.fbButton}
        />
      </ViewWrapper>
    )
  }

  _facebookButtonPressed = async () => {
    const { linkCredential } = this.props.navigation.state.params

    try {
      await this._(
        authenticate({
          providerId: 'facebook.com',
          linkCredential,
        })
      )
    } catch (err) {
      console.log(err)
    }
  }
}
