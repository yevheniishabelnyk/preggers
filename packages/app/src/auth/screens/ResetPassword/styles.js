import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const gutter = 22

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 252)',
    paddingBottom: moderateScale(24),
  },

  contentContainer: {
    alignItems: 'center',
    paddingHorizontal: moderateScale(gutter),
  },

  headerTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13),
    color: 'rgb(59, 72, 89)',
    marginTop: moderateScale(33),
    textAlign: 'center',
  },

  title: {
    width: moderateScale(262, 1.2),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(23, 0.3),
    lineHeight: moderateScale(33, 0.3),
    color: 'rgb(23, 24, 28)',
    letterSpacing: 0,
    textAlign: 'center',
    marginTop: moderateScale(97),
  },

  description: {
    width: moderateScale(307, 1.2),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18, 0.25),
    lineHeight: moderateScale(27, 0.3),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    marginTop: moderateScale(14),
    marginBottom: moderateScale(58),
  },

  loginButton: {
    marginTop: 'auto',
  },

  loginButtonText: {
    fontSize: moderateScale(16),
    color: '#FFFFFF',
    letterSpacing: 0,
  },

  error: {
    position: 'absolute',
    marginTop: moderateScale(68),
    marginLeft: moderateScale(20),
  },

  buttonContainer: {
    width: moderateScale(375 - gutter * 2, 0.4),
    marginTop: moderateScale(35),
    alignItems: 'center',
  },
})

export default styles
