import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Reset password',
  title: 'Reset your password',
  description: 'Type the e-mail you used when creating the account',
  emailFieldPlaceholder: 'E-mail',
  resetPasswordButtonTitle: 'Reset',
  resetPasswordErrorEmailNotFound:
    'Sorry, there is no account matching this e-mail',
  errorRequiredField: 'Field is required',
  errorNotValidEmail: 'Not valid e-mail',
})
