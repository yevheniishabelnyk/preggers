/*
 *
 * Reset password screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Keyboard, View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import TextField from '@/auth/components/molecules/TextField'
import ActionButton from '@/settings/components/atoms/ActionButton'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import FormError from '@/forms/components/atoms/FormError'

import * as firebase from 'firebase'

import { isValidEmail } from '@/shared/utils/validators'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class ResetPassword extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    const { params: { email } = {} } = props.navigation.state

    this.state = {
      email: email || '',
      error: false,
      errorMessage: '',
      isRequesting: false,
    }
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isRequesting } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboard
        deviceIndicators="rgb(249, 250, 252)"
      >
        <GoBackButton onPress={this._goBack} />

        <FormattedMessage
          {...messages.headerTitle}
          style={styles.headerTitle}
        />

        <FormattedMessage {...messages.title} style={styles.title} />

        <FormattedMessage
          {...messages.description}
          style={styles.description}
        />

        <View>
          <TextField
            title={formatMessage(messages.emailFieldPlaceholder)}
            keyboardType="email-address"
            autoCapitalize="none"
            onChangeText={this._setEmail}
            value={this.state.email}
            required
            error={this.state.emailError}
            autoCorrect={false}
            onBlur={this._emailFieldLostFocus}
            returnKeyType="go"
            enablesReturnKeyAutomatically
            onKeyPress={this._handleKeyDown}
            onSubmitEditing={this._resetPasswordButtonPressed}
          />

          <FormError
            isVisible={this.state.emailError || this.state.error}
            title={this.state.errorMessage}
            style={styles.error}
          />
        </View>

        <View style={styles.buttonContainer}>
          <ActionButton
            title={formatMessage(messages.resetPasswordButtonTitle)}
            onPress={this._resetPasswordButtonPressed}
            style={styles.loginButton}
            textStyle={styles.loginButtonText}
            loader={isRequesting}
          />
        </View>
      </ViewWrapper>
    )
  }

  _setEmail = value =>
    this.setState({ email: value, emailError: false, error: false })

  _resetPasswordButtonPressed = async () => {
    const { email, error } = this.state
    const { formatMessage } = this.context.intl

    if (email && isValidEmail(email)) {
      this.setState({ isRequesting: true })

      if (error) {
        this.setState({
          error: true,
          errorMessage: '',
          isRequesting: false,
          emailError: true,
        })
      }

      const authProviders = await firebase.auth().fetchProvidersForEmail(email)

      if (!authProviders.length) {
        this.setState({
          error: true,
          errorMessage: formatMessage(messages.resetPasswordErrorEmailNotFound),
          isRequesting: false,
          emailError: true,
        })
      } else {
        firebase
          .auth()
          .sendPasswordResetEmail(email)
          .then(() => {
            this._goTo('/auth-reset-password-feedback', { email })
          })
          .catch(err => {
            console.log(err)

            this.setState({ isRequesting: false })
          })

        this.setState({ isRequesting: false })
      }
    } else if (!email) {
      this.setState({
        error: true,
        errorMessage: formatMessage(messages.errorRequiredField),
        isRequesting: false,
        emailError: true,
      })
    } else if (!isValidEmail(email)) {
      this.setState({
        error: true,
        errorMessage: formatMessage(messages.errorNotValidEmail),
        isRequesting: false,
        emailError: true,
      })
    }
  }

  _emailFieldLostFocus = () => {
    const { email } = this.state
    const { formatMessage } = this.context.intl

    if (!email) {
      this.setState({
        error: true,
        errorMessage: formatMessage(messages.errorRequiredField),
      })
    } else if (!isValidEmail(email)) {
      this.setState({
        error: true,
        errorMessage: formatMessage(messages.errorNotValidEmail),
      })
    }
  }

  _handleKeyDown = e => {
    if (e.nativeEvent.key === 'Enter') {
      Keyboard.dismiss()
      this._emailFieldLostFocus()
      setTimeout(() => this._resetPasswordButtonPressed(), 0)
    }
  }
}
