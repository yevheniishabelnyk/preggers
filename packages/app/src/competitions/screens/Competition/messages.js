import { defineMessages } from 'react-intl'

export default defineMessages({
  cancelButtonText: 'Skip',
  answerError: 'You need to answer the question',
  motivationError: 'You need to motivate why you want to win',
  termsError: 'You need to accept the terms',
})
