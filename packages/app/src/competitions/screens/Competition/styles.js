import { StyleSheet, Dimensions } from 'react-native'

import { moderateScale } from '@/app/scaling'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  header: {
    height: 'auto',
    width,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(249, 250, 252)',
  },

  headerImage: {
    position: 'relative',
    backgroundColor: 'transparent',
    width: '100%',
    height: moderateScale(395),
  },

  cancelButtonText: {
    flex: 1,
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(15),
    color: 'rgb(11,11,11)',
    textAlign: 'center',
    height: moderateScale(33),
    marginTop: moderateScale(26),
  },

  innerContainer: {
    flex: 1,
    paddingTop: moderateScale(21),
    paddingHorizontal: moderateScale(22, 3),
    backgroundColor: 'rgb(249, 250, 252)',
  },

  h1: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(25),
    lineHeight: moderateScale(31),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    marginBottom: moderateScale(10),
  },

  h2: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(12),
    lineHeight: moderateScale(22),
    color: 'rgb(29, 29, 29)',
    textAlign: 'center',
    marginBottom: moderateScale(10),
  },

  h3: {
    fontFamily: 'Roboto-Regular',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(27),
    textAlign: 'center',
    color: 'rgb(15, 15, 15)',
    marginBottom: moderateScale(10),
    paddingHorizontal: moderateScale(40, 3),
  },

  h4: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15),
    lineHeight: moderateScale(33),
    color: 'rgb(15, 15,15)',
    textAlign: 'center',
    marginBottom: moderateScale(10),
  },

  h5: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15),
    lineHeight: moderateScale(33),
    color: 'rgb(15, 15,15)',
    textAlign: 'left',
    paddingRight: moderateScale(40),
    marginTop: moderateScale(25),
    marginBottom: moderateScale(15),
  },

  quesionsContainer: {
    marginHorizontal: moderateScale(17),
    paddingBottom: moderateScale(42),
  },

  sendButton: {
    width: moderateScale(235),
    height: moderateScale(55),
    marginTop: moderateScale(39),
    marginBottom: moderateScale(8),
    backgroundColor: 'rgb(51, 212, 176)',
    borderColor: 'transparent',
  },

  formInputContainer: {
    alignSelf: 'center',
    marginBottom: moderateScale(12),
    height: moderateScale(192),
  },

  formInput: {
    height: moderateScale(192),
    width: moderateScale(330),
    paddingRight: moderateScale(15),
    paddingBottom: moderateScale(15),
    textAlignVertical: 'top',
  },

  termsText: {
    fontFamily: 'Roboto-Regular',
    fontSize: moderateScale(12),
    lineHeight: moderateScale(14),
    color: 'rgb(23,23,23)',
    paddingRight: moderateScale(40),
    paddingBottom: moderateScale(10),
  },
})

export default styles
