import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image } from 'react-native'

import { WebBrowser } from 'expo'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { getCompetition } from '@/app/redux/selectors'
import { ServerTimestamp } from '@/app/database/models/shared'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import BaseScreen from '@/app/base/components/BaseScreen'
import TermsCheckbox from '@/onboarding/components/TermsCheckbox'
import NextButton from '@/onboarding/components/NextButton'
import MultichooseList from '@/competitions/components/molecules/MultichooseList'
import FormInput from '@/settings/components/molecules/FormInput'
import FormError from '@/forms/components/atoms/FormError'

import * as CompetitionActions from '@/app/database/actions/competition'
import * as RouterActions from '@/app/redux/router/actions'
import * as OnboardingActions from '@/app/redux/onboarding/actions'

import messages from './messages'
import styles from './styles'

@firebaseConnect(props => {
  const listeners = []
  const competitionUid = props.navigation.getParam('activeCompetitionUid')

  if (competitionUid) {
    listeners.push(`competitions/competitionList/${competitionUid}`)
  }

  return listeners
})
@connect((state, props) => {
  const competitionUid = props.navigation.getParam('activeCompetitionUid')
  const isNewUser = props.navigation.getParam('isNewUser')

  return {
    activeCompetition: getCompetition(state, competitionUid),
    isNewUser,
  }
})
export default class Competition extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      selectedAnswer: null,
      motivation: null,
      hasAgreedToTerms: false,
      motivationError: false,
      answerError: false,
      termsError: false,
      selectedAnswerIndex: null,
    }
  }

  render() {
    const { formatMessage } = this.context.intl

    const { activeCompetition } = this.props

    const { locale } = this.props.screenProps

    const {
      hasAgreedToTerms,
      motivationError,
      answerError,
      termsError,
      isRequesting,
    } = this.state

    if (!activeCompetition) {
      return null
    }

    return (
      <KeyboardAwareScrollView
        style={styles.container}
        ref={ref => (this.scrollViewRef = ref)}
      >
        <View style={styles.header}>
          {activeCompetition.bannerImageUrl[locale] ? (
            <Image
              source={{ uri: activeCompetition.bannerImageUrl[locale] }}
              style={styles.headerImage}
            />
          ) : null}
        </View>

        <View style={styles.innerContainer}>
          <Text style={styles.h2}>{activeCompetition.label[locale]}</Text>
          <Text style={styles.h1}>{activeCompetition.header[locale]}</Text>
          <Text style={styles.h3}>{activeCompetition.description[locale]}</Text>
        </View>

        <View style={styles.quesionsContainer}>
          <Text style={styles.h4}>{activeCompetition.questionOne[locale]}</Text>

          <MultichooseList
            answersList={[
              activeCompetition.answerOne,
              activeCompetition.answerTwo,
              activeCompetition.answerThree,
            ]}
            selectedAnswerIndex={this.state.selectedAnswerIndex}
            locale={locale}
            onAnswerSelect={this._selectedAnswer}
            answerNotSelected={answerError}
          />

          <FormError
            isVisible={answerError}
            title={formatMessage(messages.answerError)}
          />

          <Text style={styles.h5}>{activeCompetition.questionTwo[locale]}</Text>

          <FormInput
            style={styles.formInputContainer}
            inputStyle={styles.formInput}
            onChange={this._onTextChange}
            numberOfLines={10}
            maxLength={500}
            onFocus={this._scrollToFocuedInput}
            onBlur={this._scrollToBottom}
            onLayout={this._onLayoutToDoInput}
          />

          <FormError
            isVisible={motivationError}
            title={formatMessage(messages.motivationError)}
          />

          <TermsCheckbox
            text={activeCompetition.termsText[locale]}
            onChange={this._onCheckBoxChange}
            checked={hasAgreedToTerms}
            textStyle={styles.termsText}
            onTextPress={this._openTermsInBrowser}
          />

          <FormError
            isVisible={termsError}
            title={formatMessage(messages.termsError)}
          />

          <NextButton
            onPress={this._onSubmit}
            title={activeCompetition.buttonCopy[locale]}
            style={styles.sendButton}
            loader={isRequesting}
          />

          <Text style={styles.cancelButtonText} onPress={this._goNext}>
            {formatMessage(messages.cancelButtonText)}
          </Text>
        </View>
      </KeyboardAwareScrollView>
    )
  }

  _openTermsInBrowser = () => {
    const { activeCompetition } = this.props

    const { locale } = this.props.screenProps

    WebBrowser.openBrowserAsync(activeCompetition.termsLink[locale])
  }

  _goNext = async () => {
    if (this.props.isNewUser) {
      if (!this.state.isRequesting) {
        this.setState({ isRequesting: true })

        try {
          await this._(OnboardingActions.startApp())
        } catch (err) {
          this.setState({ isRequesting: false })
        }
      }
    } else {
      this._(RouterActions.resetToUrl(['/~home/checklist']))
    }
  }

  _onLayoutToDoInput = event => {
    this.setState({ todoInputY: event.nativeEvent.layout.y })
  }

  _scrollToFocuedInput = () => {
    const scrollPosition = this.state.todoInputY + 350
    this.scrollViewRef.scrollToPosition(0, scrollPosition)
  }

  _scrollToBottom = () => {
    this.scrollViewRef.scrollToPosition(0, 620)
  }

  _selectedAnswer = (index, answer) => {
    this.setState({
      selectedAnswer: answer,
      selectedAnswerIndex: index,
      answerError: false,
    })
  }

  _onSubmit = async () => {
    const { selectedAnswer, motivation, hasAgreedToTerms } = this.state

    if (!selectedAnswer || !motivation || !hasAgreedToTerms) {
      if (!selectedAnswer) {
        this.setState({ answerError: true })
      }

      if (!motivation) {
        this.setState({ motivationError: true })
      }

      if (!hasAgreedToTerms) {
        this.setState({ termsError: true })
      }
    } else {
      this._addSubmissonToDatabase(selectedAnswer, motivation)

      if (this.props.isNewUser) {
        if (!this.state.isRequesting) {
          this.setState({ isRequesting: true })

          try {
            await this._(OnboardingActions.startApp())
          } catch (err) {
            this.setState({ isRequesting: false })
          }
        }
      } else {
        this._(RouterActions.resetToUrl(['/~home/checklist']))
      }
    }
  }

  _onTextChange = value => {
    this.setState({ motivation: value, motivationError: false })
  }

  _onCheckBoxChange = checkBoxStatus => {
    this.setState({ hasAgreedToTerms: checkBoxStatus, termsError: false })
  }

  _addSubmissonToDatabase = (selectedAnswer, motivation) => {
    const { profile } = this.props.screenProps

    const { activeCompetition, firebase } = this.props

    const key = firebase.push().key

    const competitionSubmission = {
      userUid: profile.id,
      competitionName: activeCompetition.name,
      competitionUid: activeCompetition.id,
      userPregnancy: profile.pregnancy,
      answerQuestionOne: selectedAnswer.uid,
      answerQuestionTwo: motivation,
      createdAt: ServerTimestamp,
      id: key,
    }

    this._(CompetitionActions.submit(competitionSubmission))
  }
}
