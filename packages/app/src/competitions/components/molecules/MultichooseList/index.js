import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'

import AnswerItem from '@/competitions/components/atoms/AnswerItem'

export default class MultichooseList extends React.Component {
  static propTypes = {
    answersList: PropTypes.array,
    title: PropTypes.string,
    onAnswerSelect: PropTypes.func,
    answerNotSelected: PropTypes.bool,
    selectedAnswerIndex: PropTypes.number,
    locale: PropTypes.string,
  }

  render() {
    const {
      selectedAnswerIndex,
      answersList,
      onAnswerSelect,
      answerNotSelected,
      locale,
    } = this.props

    return (
      <View>
        {answersList.map((answer, index) => {
          return (
            <AnswerItem
              key={index}
              item={answer}
              locale={locale}
              index={index}
              isSelected={
                selectedAnswerIndex !== null && selectedAnswerIndex === index
              }
              answerNotSelected={answerNotSelected}
              onPress={onAnswerSelect}
            />
          )
        })}
      </View>
    )
  }
}
