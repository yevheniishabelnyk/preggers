import React from 'react'
import PropTypes from 'prop-types'
import isFunction from 'lodash'
import { View, Text, Image, TouchableOpacity } from 'react-native'

import checkedIcon from 'assets/icons/checked-green.png'
import styles from './styles'

export default class AnswerItem extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onPress: PropTypes.func,
    answerNotSelected: PropTypes.bool,
    locale: PropTypes.string,
    isSelected: PropTypes.bool,
    index: PropTypes.number,
  }

  render() {
    const { text } = this.props.item
    const { answerNotSelected, locale, isSelected } = this.props

    const circleBorderColor = answerNotSelected ? styles.noAnswerBoarder : null

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this._answerButtonPressed}
        activeOpacity={0.95}
      >
        <View style={[styles.circle, circleBorderColor]}>
          {isSelected && (
            <Image style={styles.isSelectedImage} source={checkedIcon} />
          )}
        </View>

        <Text style={styles.text}>{text[locale]}</Text>
      </TouchableOpacity>
    )
  }

  _answerButtonPressed = () => {
    const { onPress, index, item } = this.props

    if (isFunction(onPress)) {
      onPress(index, item)
    }
  }
}
