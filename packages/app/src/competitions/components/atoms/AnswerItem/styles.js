import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: moderateScale(5),
  },

  circle: {
    height: moderateScale(35),
    width: moderateScale(35),
    borderRadius: moderateScale(17.5),
    borderWidth: moderateScale(4),
    borderColor: 'rgb(80, 227, 194)',
    marginRight: moderateScale(10),
  },

  isSelectedImage: {
    position: 'absolute',
    zIndex: 1,
    top: moderateScale(7),
    alignSelf: 'center',
  },

  noAnswerBoarder: {
    borderColor: 'rgb(255, 100, 101)',
  },

  text: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(38),
    color: 'rgb(0,0,0)',
  },
})

export default styles
