import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Suggestion Box',
  title:
    'Congratulations on your pregnancy! You are the one who matter to us. We want to give you the best possible experience so if you have any suggestions on new functions or things we can improve. Shoot :)',
  messagePlaceholderTitle: 'My tip',
  messageErrorTitle: 'Please enter your tip',
  sendButtonTitle: 'Send',
  emailTitle: 'A tip from a Preggers',
  emailSubject: 'Suggestion box',
})
