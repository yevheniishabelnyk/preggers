import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 2.5)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 252)',
    paddingBottom: moderateScale(24, resizeFactor),
  },

  contentContainer: {
    alignItems: 'center',
    paddingHorizontal: gutter,
  },

  image: {
    width: moderateScale(170, resizeFactor),
    marginTop: moderateScale(35, resizeFactor),
    alignSelf: 'center',
  },

  headerTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, resizeFactor),
    color: 'rgb(59, 72, 89)',
    marginTop: moderateScale(33, resizeFactor),
    textAlign: 'center',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18, resizeFactor),
    lineHeight: moderateScale(27, resizeFactor),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    width: moderateScale(294, resizeFactor),
    marginTop: moderateScale(25, resizeFactor),
  },

  sendButton: {
    marginTop: moderateScale(10, resizeFactor),
    marginBottom: moderateScale(29, resizeFactor),
  },

  textarea: {
    height: moderateScale(125, resizeFactor),
    marginTop: moderateScale(4, resizeFactor),
    marginBottom: moderateScale(13, resizeFactor),
    paddingRight: moderateScale(18, resizeFactor),
    alignSelf: 'stretch',
    backgroundColor: 'white',
    borderRadius: moderateScale(8, resizeFactor),
    shadowOpacity: 0.05,
    shadowRadius: 6,
    shadowColor: 'rgb(0, 0 ,0)',
    shadowOffset: { height: 2, width: -2 },
  },

  textareaInputStyle: {
    height: moderateScale(112),
    paddingHorizontal: moderateScale(18),
    paddingTop: moderateScale(25),
    marginLeft: 0,
    alignSelf: 'center',
    textAlignVertical: 'top',
  },
})

export default styles
