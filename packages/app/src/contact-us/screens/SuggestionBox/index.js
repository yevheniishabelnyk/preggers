import { Constants } from 'expo'
import React from 'react'
import { Image, View, Keyboard } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import BaseScreen from '@/app/base/components/BaseScreen'
import ActionButton from '@/settings/components/atoms/ActionButton'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import FormInput from '@/settings/components/molecules/FormInput'
import FormError from '@/forms/components/atoms/FormError'

import OfferPopup from '@/contact-us/components/organisms/OfferPopup'

import publicIP from 'react-native-public-ip'

import { ServerTimestamp } from '@/app/database/models/shared'

import { withFirebase } from 'react-redux-firebase'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@withFirebase
@connect(state => ({
  email: state.firebase.auth.email,
}))
export default class SuggestionBox extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    isRequesting: false,
    message: '',
    messageError: false,
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isRequesting } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboard
        scrollable
        behavior="padding"
        deviceIndicators="rgb(249, 250, 252)"
        scrollViewProps={{ ref: ref => (this.scrollViewRef = ref) }}
        components={{
          popups: [
            <OfferPopup
              key="offerPopup"
              ref={ref => (this._offerPopup = ref)}
              onClose={this._returnToMenu}
            />,
          ],
        }}
      >
        <GoBackButton onPress={this._goBack} />

        <FormattedMessage
          {...messages.headerTitle}
          style={styles.headerTitle}
        />

        <View style={styles.container}>
          <Image
            source={require('assets/img/suggestion-box.png')}
            style={styles.image}
          />

          <FormattedMessage {...messages.title} style={styles.title} />
        </View>

        <FormInput
          title={formatMessage(messages.messagePlaceholderTitle)}
          value={this.state.message}
          onChange={this._setMessage}
          style={styles.textarea}
          inputStyle={styles.textareaInputStyle}
          multiline
          numberOfLines={4}
          error={this.state.messageError}
          onFocus={this._scrollToInput}
          onBlur={this._scrollToTop}
          onLayout={this._onLayoutToDoInput}
        />

        <FormError
          isVisible={this.state.messageError}
          title={formatMessage(messages.messageErrorTitle)}
        />

        <ActionButton
          title={formatMessage(messages.sendButtonTitle)}
          onPress={this._sendButtonPressed}
          style={styles.sendButton}
          textStyle={styles.sendButtonText}
          loader={isRequesting}
        />
      </ViewWrapper>
    )
  }

  _onLayoutToDoInput = event =>
    this.setState({ todoInputY: event.nativeEvent.layout.y })

  _scrollToInput = () =>
    this.scrollViewRef.scrollTo({ y: this.state.todoInputY - 200 })

  _scrollToTop = () => this.scrollViewRef.scrollTo({ y: 0 })

  _returnToMenu = () => this._goBack()

  _sendButtonPressed = async () => {
    const { message } = this.state

    if (message) {
      const { formatMessage } = this.context.intl
      const {
        firebase,
        email,
        screenProps: { profile },
      } = this.props

      this.setState({ isRequesting: true })

      const data = {
        subject: formatMessage(messages.emailSubject),
        title: formatMessage(messages.emailTitle),
        email,
        message,
        userId: profile.id,
        userName: profile.name,
        createdAt: ServerTimestamp,
        updatedAt: ServerTimestamp,
        appVersion: Constants.manifest.version,
      }

      try {
        const ref = await firebase.push('/contactRequests')

        data.id = ref.key

        data.userIp = await publicIP()

        await ref.set(data)
      } catch (err) {
        this.setState({ isRequesting: false })

        console.log('Requesting IP error: ', err)
      }

      this.setState({ isRequesting: false })

      this._showOfferPopup(Keyboard.dismiss())
    } else {
      this.setState({ messageError: true })
    }
  }

  _setMessage = message =>
    this.setState({
      message,
      messageError: false,
    })

  _showOfferPopup = () => this._offerPopup._show()
}
