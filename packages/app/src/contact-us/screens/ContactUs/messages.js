import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Contact form',
  title: 'Do you have any questions? Feel free to contact us.',
  subjectSelectTitle: 'Category',
  emailInputTitle: 'E-mail',
  inputTitle: 'Subject',
  messagePlaceholderTitle: 'My message',
  subjectErrorTitle: 'Please choose category',
  emailErrorTitle: 'Not valid e-mail',
  titleError: 'Please enter a subject',
  messageErrorTitle: 'Please enter message',
  sendButtonTitle: 'Send',
  question: 'Question',
  suggestion: 'Suggestion',
  problem: 'Problem',
  other: 'Other',
})
