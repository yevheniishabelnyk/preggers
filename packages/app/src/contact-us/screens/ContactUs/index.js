/*
 *
 * ContactUs screen
 *
 */

import Expo from 'expo'
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import BaseScreen from '@/app/base/components/BaseScreen'
import ActionButton from '@/settings/components/atoms/ActionButton'
import ViewWrapper from '@/shared/components/ViewWrapper'
import GoBackButton from '@/onboarding/components/GoBackButton'
import FormInput from '@/settings/components/molecules/FormInput'
import FormSelect from '@/settings/components/molecules/FormSelect'
import FormError from '@/forms/components/atoms/FormError'

import publicIP from 'react-native-public-ip'

import { ServerTimestamp } from '@/app/database/models/shared'

import { withFirebase } from 'react-redux-firebase'

import { isValidEmail } from '@/shared/utils/validators'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@withFirebase
@connect(state => ({
  email: state.firebase.auth.email,
}))
export default class ContactUs extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props, context) {
    super(props)

    const { formatMessage } = context.intl

    this.subjects = [
      {
        label: formatMessage(messages.question),
        value: formatMessage(messages.question),
      },
      {
        label: formatMessage(messages.suggestion),
        value: formatMessage(messages.suggestion),
      },
      {
        label: formatMessage(messages.problem),
        value: formatMessage(messages.problem),
      },
      {
        label: formatMessage(messages.other),
        value: formatMessage(messages.other),
      },
    ]

    this.state = {
      email: props.email || '',
      isRequesting: false,
    }
  }

  render() {
    const { formatMessage } = this.context.intl
    const { isRequesting } = this.state

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboard
        scrollable
        behavior="padding"
        deviceIndicators="rgb(249, 250, 252)"
        scrollViewProps={{ ref: ref => (this.scrollViewRef = ref) }}
      >
        <GoBackButton onPress={this._goBack} />

        <FormattedMessage
          {...messages.headerTitle}
          style={styles.headerTitle}
        />

        <FormattedMessage {...messages.title} style={styles.title} />

        <FormSelect
          title={formatMessage(messages.subjectSelectTitle)}
          value={this.state.subject}
          items={this.subjects}
          onSelect={this._setSubject}
          style={styles.formField}
          error={this.state.subjectError}
        />

        <FormError
          isVisible={this.state.subjectError}
          title={formatMessage(messages.subjectErrorTitle)}
        />

        <FormInput
          title={formatMessage(messages.emailInputTitle)}
          value={this.state.email}
          onChange={this._setEmail}
          style={styles.formField}
          error={this.state.emailError}
          isRequesting={isRequesting}
        />

        <FormError
          isVisible={this.state.emailError}
          title={formatMessage(messages.emailErrorTitle)}
        />

        <FormInput
          title={formatMessage(messages.inputTitle)}
          value={this.state.title}
          onChange={this._setTitle}
          error={this.state.titleError}
          style={styles.formField}
        />

        <FormError
          isVisible={this.state.titleError}
          title={formatMessage(messages.titleError)}
        />

        <FormInput
          title={formatMessage(messages.messagePlaceholderTitle)}
          value={this.state.message}
          onChange={this._setMessage}
          style={styles.textarea}
          inputStyle={styles.textareaInputStyle}
          multiline
          numberOfLines={4}
          error={this.state.messageError}
          onFocus={this._scrollToTodos}
          onBlur={this._scrollToTop}
          onLayout={this._onLayoutToDoInput}
        />

        <FormError
          isVisible={this.state.messageError}
          title={formatMessage(messages.messageErrorTitle)}
        />

        <ActionButton
          title={formatMessage(messages.sendButtonTitle)}
          onPress={this._sendButtonPressed}
          style={styles.sendButton}
          textStyle={styles.sendButtonText}
          loader={isRequesting}
        />
      </ViewWrapper>
    )
  }

  _onLayoutToDoInput = event =>
    this.setState({ todoInputY: event.nativeEvent.layout.y })

  _scrollToTodos = () =>
    this.scrollViewRef.scrollTo({ y: this.state.todoInputY - 200 })

  _scrollToTop = () => this.scrollViewRef.scrollTo({ y: 0 })

  _sendButtonPressed = async () => {
    const { firebase } = this.props
    const { profile } = this.props.screenProps
    const { subject, email, title, message } = this.state

    if (subject && isValidEmail(email) && title && message) {
      this.setState({ isRequesting: true })

      try {
        const ref = firebase
          .database()
          .ref('/contactRequests')
          .push()

        const data = {
          subject,
          email,
          title,
          message,
          id: ref.key,
          userId: profile.id,
          userName: profile.name,
          createdAt: ServerTimestamp,
          updatedAt: ServerTimestamp,
          appVersion: Expo.Constants.manifest.version,
        }

        try {
          data.userIp = await publicIP()
        } catch (err) {
          console.log('Requesting IP error: ', err)
        }

        ref.set(data)

        this.setState({ isRequesting: false })

        this._goBack()
      } catch (err) {
        console.log(err)

        this.setState({ isRequesting: false })
      }
    } else {
      if (!subject) {
        this.setState({ subjectError: true })
      }

      if (!isValidEmail(email)) {
        this.setState({ emailError: true })
      }

      if (!title) {
        this.setState({ titleError: true })
      }

      if (!message) {
        this.setState({ messageError: true })
      }
    }
  }

  _setSubject = subject =>
    this.setState({
      subject,
      subjectError: false,
    })

  _setEmail = email =>
    this.setState({
      email,
      emailError: false,
    })

  _setTitle = title =>
    this.setState({
      title,
      titleError: false,
    })

  _setMessage = message =>
    this.setState({
      message,
      messageError: false,
    })
}
