import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 2.5)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 252)',
    paddingBottom: moderateScale(24, resizeFactor),
  },

  contentContainer: {
    alignItems: 'center',
    paddingHorizontal: gutter,
  },

  headerTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, resizeFactor),
    color: 'rgb(59, 72, 89)',
    marginTop: moderateScale(33, resizeFactor),
    textAlign: 'center',
  },

  title: {
    width: moderateScale(294, resizeFactor),
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18, resizeFactor),
    lineHeight: moderateScale(29, resizeFactor),
    color: 'rgb(43, 56, 87)',
    textAlign: 'center',
    marginTop: moderateScale(48, resizeFactor),
    marginBottom: moderateScale(25, resizeFactor),
  },

  formField: {
    marginBottom: moderateScale(12, resizeFactor),
  },

  sendButton: {
    marginTop: moderateScale(10, resizeFactor),
  },

  sendButtonText: {
    fontSize: moderateScale(16, resizeFactor),
    color: '#FFFFFF',
    letterSpacing: 0,
  },

  textarea: {
    height: moderateScale(125, resizeFactor),
    marginTop: moderateScale(14, resizeFactor),
    marginBottom: moderateScale(13, resizeFactor),
    paddingRight: moderateScale(18, resizeFactor),
    alignSelf: 'stretch',
    backgroundColor: 'white',
    borderRadius: moderateScale(8, resizeFactor),
  },

  textareaInputStyle: {
    height: moderateScale(112),
    paddingHorizontal: moderateScale(18),
    paddingTop: moderateScale(25),
    marginLeft: 0,
    alignSelf: 'center',
    textAlignVertical: 'top',
  },

  error: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(12, resizeFactor),
    color: 'rgb(250,65,105)',
    textAlign: 'left',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    display: 'flex',
    marginRight: 'auto',
    marginLeft: moderateScale(22, resizeFactor),
  },
})

export default styles
