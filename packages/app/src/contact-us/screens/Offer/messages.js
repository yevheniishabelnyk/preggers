import { defineMessages } from 'react-intl'

export default defineMessages({
  dialogMessage: 'Wow, what a great tip!',
  buttonTitle: 'Ok',
})
