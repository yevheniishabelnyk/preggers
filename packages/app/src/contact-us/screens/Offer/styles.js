import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: moderateScale(28, resizeFactor),
    paddingTop: moderateScale(28, resizeFactor),
    alignItems: 'center',
  },

  image: {
    width: moderateScale(165, resizeFactor),
    height: moderateScale(179, resizeFactor),
  },

  title: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(12, resizeFactor),
    lineHeight: moderateScale(22, resizeFactor),
    letterSpacing: 0,
    color: 'rgb(29,29,29)',
    marginTop: moderateScale(24, resizeFactor),
  },

  message: {
    marginTop: moderateScale(40),
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(25, resizeFactor),
    lineHeight: moderateScale(31, resizeFactor),
    color: 'rgb(43,56,87)',
    textAlign: 'center',
  },

  buttonWrapper: {
    marginTop: 'auto',
    marginBottom: moderateScale(25, resizeFactor),
    alignSelf: 'stretch',
  },

  button: {
    shadowOpacity: 0.42,
    shadowRadius: 6,
    shadowColor: 'rgb(101,150,255)',
    shadowOffset: { height: 5, width: 2 },
  },
})

export default styles
