/**
 *
 * Offer
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Image } from 'react-native'

import ActionButton from '@/settings/components/atoms/ActionButton'

import { isFunction } from 'lodash'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class Offer extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.container}>
        <Image
          source={require('assets/img/thank-you.png')}
          style={styles.image}
        />

        <FormattedMessage {...messages.dialogMessage} style={styles.message} />

        <View style={styles.buttonWrapper}>
          <ActionButton
            title={formatMessage(messages.buttonTitle)}
            onPress={this._buttonPressed}
            style={styles.button}
          />
        </View>
      </View>
    )
  }

  _buttonPressed = () => {
    const { onPress } = this.props

    if (isFunction(onPress)) {
      onPress()
    }
  }
}
