/*
 *
 * contact us routes
 *
 */

import ContactUs from './screens/ContactUs'
import SuggestionBox from './screens/SuggestionBox'

export default {
  '/contact-us': {
    screen: ContactUs,
  },
  '/suggestion-box': {
    screen: SuggestionBox,
  },
}
