import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.4

const styles = StyleSheet.create({
  dialog: {
    flex: 1,
    maxWidth: moderateScale(331, resizeFactor),
    maxHeight: moderateScale(547, resizeFactor),
    overflow: 'hidden',
  },
})

export default styles
