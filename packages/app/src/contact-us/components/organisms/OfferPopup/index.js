/**
 *
 * OfferPopup
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import Offer from '@/contact-us/screens/Offer'
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog'
import { isFunction } from 'lodash'

import styles from './styles'

export default class OfferPopup extends React.Component {
  static propTypes = {
    onClose: PropTypes.func,
  }

  render() {
    return (
      <PopupDialog
        ref={ref => (this._dialog = ref)}
        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
        dialogStyle={styles.dialog}
        overlayOpacity={0.7}
        onDismissed={this._hide}
      >
        <Offer onPress={this._hide} />
      </PopupDialog>
    )
  }

  _show = () => this._dialog.show()

  _hide = () => {
    const { onClose } = this.props

    this._dialog.dismiss()

    if (isFunction(onClose)) {
      onClose()
    }
  }
}
