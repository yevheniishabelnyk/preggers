/**
 *
 * ContentServiceAdvertisement
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { isFunction } from 'lodash'

import AdvertisingCard from '@/ads/components/molecules/AdvertisingCard'
import ImageAdvertisement from '@/ads/components/molecules/ImageAdvertisement'

import { AD_TYPE_IMAGE_AD } from '@/app/redux/ads/constants'
import * as AdActions from '@/app/redux/ads/actions'
import * as RouterActions from '@/app/redux/router/actions'

export default class ContentServiceAdvertisement extends React.Component {
  static propTypes = {
    ad: PropTypes.object.isRequired,
    dispatch: PropTypes.func,
    customOnPress: PropTypes.func,
    containerStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    localImage: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.object,
    ]),
  }

  render() {
    const { ad, containerStyle, localImage } = this.props

    if (!ad) {
      return null
    }

    const imageUrl = !localImage && ad.images[0].length ? ad.images[0] : null

    switch (ad.type) {
      case AD_TYPE_IMAGE_AD:
        return (
          <ImageAdvertisement
            label={ad.label}
            image={imageUrl}
            onLoad={this._onLoad}
            onPress={this._onPress}
          />
        )

      default:
        return (
          <AdvertisingCard
            title={ad.title}
            image={imageUrl}
            label={ad.label}
            text={ad.text}
            buttonLabel={ad.buttonLabel}
            containerStyle={containerStyle}
            localImage={localImage}
            onLoad={this._onLoad}
            onPress={this._onPress}
          />
        )
    }
  }

  _onLoad = () => {
    const { ad, dispatch } = this.props

    if (isFunction(dispatch)) {
      dispatch(AdActions.markInteraction(ad, 'impression'))
    }
  }

  _onPress = () => {
    const { customOnPress } = this.props

    if (isFunction(customOnPress)) {
      customOnPress()
    } else {
      this._defaultOnPress()
    }
  }

  _defaultOnPress = () => {
    const { ad, dispatch } = this.props

    if (ad.link && isFunction(dispatch)) {
      dispatch(RouterActions.handleLink(ad.link))

      dispatch(AdActions.markInteraction(ad, 'click'))
    }
  }
}
