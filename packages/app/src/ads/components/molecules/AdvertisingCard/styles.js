import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: moderateScale(20),
    maxWidth: moderateScale(330, 1),
    borderRadius: moderateScale(8),
    backgroundColor: 'rgb(255, 255, 255)',
    shadowColor: 'rgba(118, 137, 177, 0.14)',
    shadowOffset: { width: 0, height: 7 },
    shadowOpacity: moderateScale(15),
  },

  image: {
    height: moderateScale(190, 1),
    width: moderateScale(330, 1),
    marginBottom: moderateScale(8),
  },

  label: {
    fontFamily: 'Now-Regular',
    fontSize: moderateScale(12),
    textAlign: 'center',
    color: 'rgb(29, 29, 29)',
    marginBottom: moderateScale(8),
    marginTop: moderateScale(8),
    marginHorizontal: moderateScale(10),
  },

  title: {
    fontFamily: 'Now-Black',
    fontSize: moderateScale(20),
    lineHeight: moderateScale(27),
    textAlign: 'center',
    color: 'rgb(35, 40, 46)',
    marginBottom: moderateScale(8),
    marginTop: moderateScale(8),
    marginHorizontal: moderateScale(10),
  },

  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    textAlign: 'center',
    color: 'rgb(19, 20, 22)',
    marginBottom: moderateScale(8),
    marginTop: moderateScale(8),
    marginHorizontal: moderateScale(20),
  },

  buttonStyle: {
    marginTop: moderateScale(12),
    marginBottom: moderateScale(22),
  },
})

export default styles
