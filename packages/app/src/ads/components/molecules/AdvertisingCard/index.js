import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image, TouchableWithoutFeedback } from 'react-native'

import { isFunction } from 'lodash'
import { moderateScale } from '@/app/scaling'

import Button from '@/shared/components/Button'
import styles from './styles'

const imageWidth = moderateScale(330)

export default class AdvertisingCard extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    onLoad: PropTypes.func,
    topLabel: PropTypes.bool,
    label: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    buttonLabel: PropTypes.string,
    image: PropTypes.string,
    containerStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    localImage: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.object,
    ]),
  }

  state = {
    imageHeight: 0,
  }

  render() {
    const { imageHeight } = this.state

    const {
      label,
      title,
      text,
      buttonLabel,
      image,
      containerStyle,
      localImage,
      topLabel,
    } = this.props

    const dynamicHeight = !localImage ? { height: imageHeight } : null

    return (
      <TouchableWithoutFeedback onPress={this._onPress}>
        <View style={[styles.container, containerStyle]}>
          {topLabel && label ? <Text style={styles.label}>{label}</Text> : null}

          {image && (
            <TouchableWithoutFeedback onPress={this._onPress}>
              <Image
                source={{ uri: image }}
                style={[styles.image, dynamicHeight]}
              />
            </TouchableWithoutFeedback>
          )}

          {localImage && (
            <TouchableWithoutFeedback onPress={this._onPress}>
              <Image source={localImage} style={styles.image} />
            </TouchableWithoutFeedback>
          )}

          {!topLabel && label ? (
            <Text style={styles.label}>{label}</Text>
          ) : null}

          {title && <Text style={styles.title}>{title}</Text>}

          {text && <Text style={styles.text}>{text}</Text>}

          {buttonLabel && (
            <Button
              title={buttonLabel}
              style={styles.buttonStyle}
              onPress={this._onPress}
            />
          )}
        </View>
      </TouchableWithoutFeedback>
    )
  }

  componentDidMount() {
    const { localImage, image, onLoad } = this.props

    if (localImage === undefined && image) {
      Image.getSize(
        image,
        (w, h) => {
          const heightRatio = h / w
          const imageHeight = imageWidth * heightRatio
          this.setState({ imageHeight })
        },
        err => console.log(`Error loading image dimensions, ${err}`)
      )
    }

    if (isFunction(onLoad)) {
      onLoad()
    }
  }

  _onPress = () => {
    const { onPress } = this.props

    if (isFunction(onPress)) {
      onPress()
    }
  }
}
