/**
 *
 * ImageAdvertisement
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image, TouchableWithoutFeedback } from 'react-native'

import { isFunction } from 'lodash'

import { moderateScale } from '@/app/scaling'

import styles from './styles'

const imageWidth = moderateScale(340)

export default class ImageAdvertisement extends React.Component {
  static propTypes = {
    image: PropTypes.string,
    label: PropTypes.string,
    onLoad: PropTypes.func,
    onPress: PropTypes.func,
  }

  state = {
    imageHeight: 0,
  }

  render() {
    const { imageHeight } = this.state

    const { label, image } = this.props

    if (!image) {
      return null
    }

    return (
      <View style={styles.container}>
        <Text style={styles.title}>{label}</Text>

        {imageHeight ? (
          <TouchableWithoutFeedback onPress={this._onPress}>
            <Image
              source={{ uri: image }}
              style={[styles.image, { height: imageHeight, width: imageWidth }]}
            />
          </TouchableWithoutFeedback>
        ) : null}
      </View>
    )
  }

  componentDidMount() {
    const { image, onLoad } = this.props

    if (isFunction(onLoad)) {
      onLoad()
    }

    if (image) {
      Image.getSize(
        image,
        (w, h) => {
          const heightRatio = h / w
          const imageHeight = imageWidth * heightRatio
          this.setState({ imageHeight })
        },
        err => console.log(`Error loading image dimensions, ${err}`)
      )
    }
  }

  _onPress = () => {
    const { onPress } = this.props

    if (isFunction(onPress)) {
      onPress()
    }
  }
}
