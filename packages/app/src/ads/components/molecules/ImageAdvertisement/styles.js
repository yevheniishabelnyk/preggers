import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    alignSelf: Platform.OS === 'android' ? 'center' : null,
    marginBottom: moderateScale(25),
  },

  title: {
    fontFamily: 'Roboto-Regular',
    fontSize: moderateScale(12),
    color: 'rgb(120, 123, 141)',
    lineHeight: moderateScale(24),
    marginBottom: moderateScale(5),
  },
})

export default styles
