/**
 *
 * ToDo
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableOpacity } from 'react-native'

import { isFunction } from 'lodash'

import caretIcon from 'assets/icons/checked-white.png'

import styles from './styles'

export default class ToDo extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    hasBorder: PropTypes.bool,
    onPress: PropTypes.func,
  }

  render() {
    const { item, hasBorder } = this.props

    return (
      <TouchableOpacity
        style={[styles.todo, hasBorder ? styles.todoBorder : null]}
        onPress={this._itemPressed}
        activeOpacity={0.95}
      >
        {item.completed ? (
          <View
            style={[
              styles.checkedIconContainer,
              styles.checkedIconContainerActive,
            ]}
          >
            <Image source={caretIcon} style={styles.caretIcon} />
          </View>
        ) : (
          <View style={styles.checkedIconContainer} />
        )}

        <Text style={styles.text}>{item.text}</Text>
      </TouchableOpacity>
    )
  }

  _itemPressed = () => {
    const { item, onPress } = this.props

    if (isFunction(onPress)) {
      onPress(item)
    }
  }
}
