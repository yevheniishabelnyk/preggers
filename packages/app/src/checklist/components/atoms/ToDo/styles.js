import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const checkedIconContainerWidth = moderateScale(34, 0.3)

const styles = StyleSheet.create({
  todo: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: moderateScale(6, 1),
    paddingHorizontal: moderateScale(6),
    width: moderateScale(275, 1.2),
  },

  todoBorder: {
    borderTopColor: 'rgba(151, 151, 151, 0.21)',
    borderTopWidth: moderateScale(1),
  },

  text: {
    marginLeft: moderateScale(14),
    fontSize: moderateScale(14),
    letterSpacing: moderateScale(0.7),
    color: 'rgb(52, 57, 65)',
    fontFamily: 'Now-Medium',
  },

  checkedIconContainer: {
    width: checkedIconContainerWidth,
    height: checkedIconContainerWidth,
    borderRadius: checkedIconContainerWidth / 2,
    borderWidth: moderateScale(1),
    borderColor: 'rgb(205,205,205)',
    overflow: 'hidden',
  },

  checkedIconContainerActive: {
    backgroundColor: 'rgb(80,227,194)',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0,
  },

  caretIcon: {
    width: moderateScale(17, 0.3),
    height: moderateScale(13, 0.3),
  },
})

export default styles
