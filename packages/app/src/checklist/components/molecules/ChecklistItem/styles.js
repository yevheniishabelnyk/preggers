import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    marginTop: moderateScale(10),
    paddingHorizontal: moderateScale(13),
    paddingVertical: moderateScale(13),
  },

  title: {
    fontSize: moderateScale(14),
    color: 'rgb(52, 57, 65)',
    fontFamily: 'Now-Medium',
  },

  arrow: {
    marginRight: 0,
  },
})

export default styles
