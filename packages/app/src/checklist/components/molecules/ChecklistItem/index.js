/**
 *
 * Offer
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import styles from './styles'

import WhiteFormButton from '@/settings/components/molecules/WhiteFormButton'

export default class ChecklistItem extends React.Component {
  static propTypes = {
    checklistItem: PropTypes.object,
    onPress: PropTypes.func,
  }

  render() {
    const { checklistItem } = this.props
    const isChecked = !!Math.min(
      ...Object.values(checklistItem.todos).map(t => t.completed)
    )

    return (
      <WhiteFormButton
        key={checklistItem.id}
        title={checklistItem.headline}
        checked={isChecked}
        onPress={this._onPress}
        style={styles.container}
        titleStyle={styles.title}
        arrowStyle={styles.arrow}
        arrow
      />
    )
  }

  _onPress = () => {
    const { checklistItem, onPress } = this.props
    onPress(checklistItem.id)
  }
}
