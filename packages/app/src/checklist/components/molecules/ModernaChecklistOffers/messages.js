import { defineMessages } from 'react-intl'

export default defineMessages({
  freeOffer: 'Maternity Insurance Free',
  largeOffer: 'Maternity Insurance Large',
  buttonLabel: 'Sign up',
})
