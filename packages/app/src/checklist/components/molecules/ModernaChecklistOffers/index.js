/**
 *
 * Offer
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import ContentServiceAdvertisement from '@/ads/components/molecules/ContentServiceAdvertisement'
import { connect } from 'react-redux'

import { getAdsByCampaign } from '@/app/redux/selectors'
import { MODERNA_PREGNANCY_INSURANCE_CAMPAIGN } from '@/app/redux/ads/constants'
import messages from './messages'
import * as AdActions from '@/app/redux/ads/actions'

const FREE_OFFER_INDEX = 0
const LARGE_OFFER_INDEX = 1

@connect(state => ({
  locale: state.settings.locale,
  modernaCampaignAds:
    getAdsByCampaign(state, MODERNA_PREGNANCY_INSURANCE_CAMPAIGN) || [],
}))
export default class ModernaChecklistOffers extends React.Component {
  static propTypes = {
    completedOffersFlag: PropTypes.number,
    todos: PropTypes.array,
    navigation: PropTypes.object,
    modernaCampaignAds: PropTypes.array,
    deviceCountryCode: PropTypes.string,
    userPregnancy: PropTypes.object,
    locale: PropTypes.string,
    profile: PropTypes.object,
    dispatch: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { formatMessage } = this.context.intl

    const freeOfferAd = {
      title: formatMessage(messages.freeOffer),
      buttonLabel: formatMessage(messages.buttonLabel),
    }

    const largeOfferAd = {
      title: formatMessage(messages.largeOffer),
      buttonLabel: formatMessage(messages.buttonLabel),
    }

    return (
      <View>
        {this._shouldSeeFreeOffer() && (
          <ContentServiceAdvertisement
            ad={freeOfferAd}
            customOnPress={this._onFreeOfferPress}
            dispatch={this.props.dispatch}
            localImage={require('assets/img/moderna-free-ad-icon.png')}
          />
        )}
        {this._shouldSeeLargeOffer() && (
          <ContentServiceAdvertisement
            ad={largeOfferAd}
            customOnPress={this._onLargeOfferPress}
            dispatch={this.props.dispatch}
            localImage={require('assets/img/moderna-large-ad-icon.png')}
          />
        )}
        {this._shouldSeeUpgradeOffer() && (
          <ContentServiceAdvertisement
            ad={largeOfferAd}
            customOnPress={this._onUpgradeOfferPress}
            dispatch={this.props.dispatch}
            localImage={require('assets/img/moderna-large-ad-icon.png')}
          />
        )}
      </View>
    )
  }

  componentDidMount() {
    this._fetchAds()
  }

  _fetchAds = async () => {
    const {
      navigation,
      deviceCountryCode,
      userPregnancy,
      locale,
      profile,
    } = this.props

    const requestBody = {
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      userType: profile.type,
      pregnancies: [userPregnancy.dueDate],
      campaign: MODERNA_PREGNANCY_INSURANCE_CAMPAIGN,
    }

    try {
      await navigation.dispatch(AdActions.fetchAds(requestBody))
    } catch (error) {
      console.log(error)
    }
  }

  _hasCompletedTodo = index => {
    const { todos } = this.props
    const completedOffers = todos.map((todo, i) => (todo.completed ? i : null))
    return completedOffers.includes(index)
  }

  _shouldSeeFreeOffer = () => {
    return (
      this._shouldSeeOffer('FREE') && !this._hasCompletedTodo(FREE_OFFER_INDEX)
    )
  }

  _shouldSeeLargeOffer = () => {
    return (
      this._shouldSeeOffer('LARGE') &&
      !this._hasCompletedTodo(LARGE_OFFER_INDEX)
    )
  }

  _shouldSeeUpgradeOffer = () => {
    return (
      this._shouldSeeOffer('UPGRADE') &&
      !this._hasCompletedTodo(LARGE_OFFER_INDEX)
    )
  }

  _shouldSeeOffer = type => {
    const { modernaCampaignAds } = this.props
    const availableTypes = Object.values(modernaCampaignAds || {}).map(
      ad => ad.modernaInsuranceCampaignType
    )

    if (type === 'FREE') {
      return (
        availableTypes.includes('FREE') &&
        availableTypes.includes('LARGE') &&
        availableTypes.includes('UPGRADE')
      )
    }

    if (type === 'LARGE') {
      return availableTypes.includes('LARGE') && availableTypes.includes('FREE')
    }

    if (type === 'UPGRADE') {
      return (
        availableTypes.includes('UPGRADE') && !availableTypes.includes('FREE')
      )
    }

    return false
  }

  _onFreeOfferPress = () => this._onOfferPress('FREE')

  _onLargeOfferPress = () => this._onOfferPress('LARGE')

  _onUpgradeOfferPress = () => this._onOfferPress('UPGRADE')

  _onOfferPress = type => {
    const { navigate } = this.props.navigation

    const { todos, modernaCampaignAds } = this.props

    const todoIds = todos.map(todo => todo.id)

    const byType = modernaCampaignAds.reduce(
      (a, v) => ({ ...a, [v.modernaInsuranceCampaignType]: v }),
      {}
    )
    const connectedAd = byType[type]

    let route, withParams

    if (type === 'FREE') {
      route = '/moderna-free-pregnancy-insurance-form'
      withParams = {
        connectedTodoIds: [todoIds[FREE_OFFER_INDEX]],
        connectedAd,
      }
    }

    if (type === 'LARGE') {
      route = '/moderna-large-pregnancy-insurance-form'
      withParams = {
        connectedTodoIds: [
          todoIds[FREE_OFFER_INDEX],
          todoIds[LARGE_OFFER_INDEX],
        ],
        connectedAd,
      }
    }

    if (type === 'UPGRADE') {
      route = '/moderna-large-pregnancy-insurance-form'
      withParams = {
        connectedTodoIds: [
          todoIds[FREE_OFFER_INDEX],
          todoIds[LARGE_OFFER_INDEX],
        ],
        connectedAd,
        isUpgrading: true,
      }
    }

    navigate(route, withParams)
  }
}
