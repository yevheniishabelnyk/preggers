import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    borderRadius: moderateScale(8),
    backgroundColor: 'white',
    paddingVertical: moderateScale(8),
    paddingHorizontal: 12,
    marginRight: moderateScale(8),
    marginBottom: moderateScale(8),
    flexDirection: 'row',
    alignItems: 'center',
  },

  text: {
    color: 'rgb(131, 146, 167)',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(12),
    letterSpacing: moderateScale(0.6),
    marginRight: moderateScale(20),
  },

  removeButton: {
    width: moderateScale(22),
    height: moderateScale(22),
    borderRadius: moderateScale(11),
    backgroundColor: 'rgb(230, 234, 241)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  removeIcon: {
    width: moderateScale(10),
    height: moderateScale(10),
  },
})

export default styles
