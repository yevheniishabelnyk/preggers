/**
 *
 * Tag
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Image, Text, TouchableOpacity, View } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class Tag extends React.Component {
  static propTypes = {
    onRemove: PropTypes.func,
    todo: PropTypes.string,
  }

  render() {
    const { todo } = this.props

    return (
      <View style={styles.container}>
        <Text style={styles.text}>{todo}</Text>

        <TouchableOpacity
          style={styles.removeButton}
          onPress={this._removeButtonPressed}
          activeOpacity={0.95}
        >
          <Image
            style={styles.removeIcon}
            source={require('assets/icons/cross-white-thin.png')}
          />
        </TouchableOpacity>
      </View>
    )
  }

  _removeButtonPressed = () => {
    const { onRemove, todo } = this.props

    if (isFunction(onRemove)) {
      onRemove(todo)
    }
  }
}
