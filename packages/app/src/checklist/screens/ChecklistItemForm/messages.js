import { defineMessages } from 'react-intl'

export default defineMessages({
  addChecklistHeaderTitle: 'New checklist',
  editChecklistHeaderTitle: 'Edit checklist',
  headlineInputPlaceholder: 'Headline',
  descriptionInputPlaceholder: 'Description',
  toDoInputPlaceholder: 'Add to-do',
  saveButtonTitle: 'Save',
  errorHeadlineTitle: 'Please give your checklist a title',
  errorAddToDoTitle: 'Please add a to-do',
})
