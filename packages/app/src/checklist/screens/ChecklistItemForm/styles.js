import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(20, 1.5)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: gutter,
    paddingBottom: moderateScale(37),
  },

  header: {
    paddingHorizontal: moderateScale(22.5),
    backgroundColor: 'rgb(249,250,252)',
  },

  saveButton: {
    marginTop: 'auto',
  },

  formField: {
    marginBottom: moderateScale(12),
    paddingLeft: moderateScale(18),
  },

  addToDoForm: {
    paddingLeft: moderateScale(18),
    paddingRight: moderateScale(18),
  },

  formFieldInput: {
    paddingLeft: 0,
    marginLeft: 0,
  },

  firstInput: {
    marginTop: moderateScale(16),
  },

  descriptionField: {
    height: moderateScale(144),
  },

  descriptionFieldInput: {
    height: moderateScale(112),
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 20,
    alignSelf: 'center',
    textAlignVertical: 'top',
  },

  inputContainer: {
    alignSelf: 'stretch',
    borderRadius: moderateScale(8),
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: moderateScale(19),
    minHeight: moderateScale(34),
    borderWidth: 1,
    borderColor: 'transparent',
  },

  todoContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingBottom: moderateScale(12),
    marginTop: 0,
  },

  error: {
    position: 'relative',
    marginLeft: moderateScale(18),
  },
})

export default styles
