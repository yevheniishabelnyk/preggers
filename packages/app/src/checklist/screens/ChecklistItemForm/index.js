/**
 *
 * ChecklistItemForm
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

import { View } from 'react-native'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import Tag from '@/checklist/components/molecules/Tag'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import ActionButton from '@/settings/components/atoms/ActionButton'
import FormInput from '@/settings/components/molecules/FormInput'
import FormError from '@/forms/components/atoms/FormError'
import PlusButton from '@/settings/components/atoms/PlusButton'
import * as ChecklistActions from '@/app/database/actions/checklist'
import * as ProfileActions from '@/app/redux/profile/actions'

import { omit } from 'lodash'

import { getUserChecklistItem } from '@/app/redux/selectors'

import messages from './messages'

import styles from './styles'

@firebaseConnect(props => {
  const flow = props.navigation.getParam('flow', 'ADD')
  const { profile } = props.screenProps

  switch (flow) {
    case 'EDIT': {
      const checklistItemId = props.navigation.getParam('checklistItemId')

      if (checklistItemId) {
        return [`userOwned/checklistItems/${profile.id}/${checklistItemId}`]
      }

      break
    }
  }
})
@connect((state, props) => {
  const flow = props.navigation.getParam('flow', 'ADD')
  const { profile } = props.screenProps

  switch (flow) {
    case 'EDIT': {
      const checklistItemId = props.navigation.getParam('checklistItemId')

      const checklistItem = getUserChecklistItem(
        state,
        profile.id,
        checklistItemId
      )

      return {
        checklistItem,
        isFetching: checklistItem === undefined,
      }
    }
  }

  return {
    isFetching: flow !== 'ADD',
  }
})
export default class ChecklistItemForm extends BaseScreen {
  static propTypes = {
    checklistItem: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    const flow = props.navigation.getParam('flow')

    if (flow === 'EDIT' && props.checklistItem) {
      this.state = {
        headline: props.checklistItem.headline,
        description: props.checklistItem.description,
        todos: props.checklistItem.todos || {},
      }
    } else {
      this.state = {
        todos: {},
        description: '',
      }
    }
  }

  render() {
    const { navigation } = this.props
    const { formatMessage } = this.context.intl
    const { headline, description, todos, todoText } = this.state

    const flow = navigation.getParam('flow')

    let title

    if (flow === 'EDIT') {
      title = formatMessage(messages.editChecklistHeaderTitle)
    } else {
      title = formatMessage(messages.addChecklistHeaderTitle)
    }

    return (
      <ViewWrapper
        scrollable
        keyboard
        behavior="padding"
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={title}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
        scrollViewProps={{ ref: ref => (this.scrollViewRef = ref) }}
      >
        <View>
          <FormInput
            title={formatMessage(messages.headlineInputPlaceholder)}
            value={headline}
            onChange={this._onChangeHeadline}
            style={[styles.formField, styles.firstInput]}
            inputStyle={styles.formFieldInput}
            numberOfLines={1}
            error={this.state.headlineError}
          />

          <FormError
            isVisible={this.state.headlineError}
            title={formatMessage(messages.errorHeadlineTitle)}
            style={styles.error}
          />
        </View>

        <View>
          <FormInput
            title={formatMessage(messages.descriptionInputPlaceholder)}
            value={description}
            onChange={this._onChangeDescription}
            style={[styles.formField, styles.descriptionField]}
            inputStyle={styles.descriptionFieldInput}
            numberOfLines={4}
            onFocus={this._scrollToTodos}
            onLayout={this._onLayoutToDoInput}
          />
        </View>

        <View>
          <FormInput
            title={formatMessage(messages.toDoInputPlaceholder)}
            value={todoText}
            style={[styles.formField, styles.addToDoForm]}
            inputStyle={styles.formFieldInput}
            onChange={this._onChangeTodoText}
            error={this.state.addToDoError}
            rightIcon={<PlusButton onPress={this._onAddToDo} />}
            returnKeyType="done"
            enablesReturnKeyAutomatically
            onSubmitEditing={this._onAddToDo}
            blurOnSubmit={false}
          />
          <FormError
            isVisible={this.state.addToDoError}
            title={formatMessage(messages.errorAddToDoTitle)}
            style={[styles.error, styles.addToDoError]}
          />
        </View>

        <View style={[styles.todoContainer, styles.formField]}>
          {Object.keys(todos).map(todo => (
            <Tag todo={todo} onRemove={this._onRemoveTodo} key={todo} />
          ))}
        </View>

        <ActionButton
          title={formatMessage(messages.saveButtonTitle)}
          onPress={this._saveButtonPressed}
          style={styles.saveButton}
        />
      </ViewWrapper>
    )
  }

  _onLayoutToDoInput = event => {
    this.setState({ todoInputY: event.nativeEvent.layout.y })
  }

  _scrollToTodos = () => {
    this.scrollViewRef.scrollTo({ y: this.state.todoInputY })
  }

  _scrollToTop = () => this.scrollViewRef.scrollTo({ y: 0 })

  componentWillReceiveProps(nextProps) {
    if (!this.props.checklistItem && nextProps.checklistItem) {
      const flow = nextProps.navigation.getParam('flow')

      if (flow === 'EDIT') {
        this.setState({
          headline: nextProps.checklistItem.headline,
          description: nextProps.checklistItem.description,
          todos: nextProps.checklistItem.todos,
        })
      }
    }
  }

  _onChangeTodoText = text => {
    this.setState({ addTodoError: false, todoText: text })
  }

  _onChangeHeadline = text => {
    this.setState({ headline: text, headlineError: false })
  }

  _onChangeDescription = text => {
    this.setState({ description: text })
  }

  _onAddToDo = () => {
    const { todoText } = this.state

    if (todoText && todoText.length) {
      this.setState(prevState => ({
        todoText: '',
        todos: Object.assign(prevState.todos, { [todoText]: false }),
      }))
    } else {
      this.setState({ addToDoError: true })
    }
  }

  _onRemoveTodo = todo =>
    this.setState(prevState => ({ todos: omit(prevState.todos, todo) }))

  _saveButtonPressed = async () => {
    const { navigation, checklistItem } = this.props
    const { headline, description, todos } = this.state

    const flow = navigation.getParam('flow')

    if (!headline) {
      this.setState({ headlineError: true })
    } else {
      try {
        const item = {
          headline,
          description,
          todos,
        }

        switch (flow) {
          case 'ADD':
            await this._(ChecklistActions.addToOwned(item))
            break

          case 'EDIT': {
            await this._(ChecklistActions.updateOwned(checklistItem, item))

            break
          }
        }

        this._(ProfileActions.maybeAskUserToRateApp())

        this._goBack()
      } catch (err) {
        console.log('err', err)
      }
    }
  }
}
