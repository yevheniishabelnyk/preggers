import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Checklist',
  editText: 'Edit',
})
