import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(22)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: gutter,
  },

  contentContainerStyle: {
    alignItems: 'stretch',
    paddingBottom: moderateScale(150),
  },

  header: {
    paddingHorizontal: moderateScale(22.5),
    backgroundColor: 'rgb(249,250,252)',
  },

  checklistContainer: {
    borderRadius: moderateScale(8),
    backgroundColor: 'white',
    paddingTop: moderateScale(24),
    paddingHorizontal: moderateScale(20),
    paddingBottom: moderateScale(30),
    marginBottom: moderateScale(38),
    shadowOpacity: moderateScale(0.14),
    shadowRadius: moderateScale(8),
    shadowColor: 'rgb(118, 137, 177)',
    shadowOffset: { height: moderateScale(7), width: 0 },
  },

  description: {
    fontFamily: 'Roboto-Light',
    fontSize: moderateScale(14),
    lineHeight: moderateScale(24),
    marginBottom: moderateScale(30),
    textAlign: 'center',
    color: 'rgb(131,146,167)',
  },

  headline: {
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(20),
    textAlign: 'center',
    color: 'rgb(52,57,65)',
    marginBottom: moderateScale(25),
  },

  editText: {
    fontSize: moderateScale(12),
    color: 'rgb(131, 146, 167)',
    fontFamily: 'Now-Medium',
    letterSpacing: moderateScale(0.6),
    textAlign: 'center',
    marginTop: moderateScale(30),
    marginBottom: moderateScale(-12),
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    textDecorationColor: 'rgb(131, 146, 167)',
  },

  adsCointainer: {
    marginTop: moderateScale(-20),
  },
})

export default styles
