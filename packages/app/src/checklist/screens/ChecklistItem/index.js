/**
 *
 * ChecklistItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'

import { View, Text } from 'react-native'
import ContentServiceAdvertisement from '@/ads/components/molecules/ContentServiceAdvertisement'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import ToDo from '@/checklist/components/atoms/ToDo'

import {
  getChecklistItemById,
  getChecklistAdsById,
  getPregnancy,
} from '@/app/redux/selectors'

import * as ChecklistActions from '@/app/database/actions/checklist'
import * as AdActions from '@/app/redux/ads/actions'
import * as ProfileActions from '@/app/redux/profile/actions'

import ModernaChecklistOffers from '@/checklist/components/molecules/ModernaChecklistOffers'
import {
  MODERNA_PREGNANCY_INSURANCE_CAMPAIGN,
  AD_TYPE_CHECKLIST_OFFER,
} from '@/app/redux/ads/constants'

import messages from './messages'
import styles from './styles'

@firebaseConnect(props => {
  const listeners = ['content/checklistItems']

  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    listeners.push(
      `userOwned/checklistItems/${profile.id}`,
      `userOwned/completedAdminTodosByUser/${profile.id}`
    )
  }

  return listeners
})
@connect((state, props) => {
  const id = props.navigation.getParam('checklistItemId')

  return {
    checklistItem: getChecklistItemById(state, id) || {},
    userPregnancy:
      getPregnancy(state, props.screenProps.profile.pregnancy) || undefined,
    ads: getChecklistAdsById(state, id) || [],
    locale: state.settings.locale,
  }
})
export default class ChecklistItem extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    userPregnancy: {},
  }

  render() {
    const { profile } = this.props.screenProps
    const { formatMessage } = this.context.intl
    const { checklistItem } = this.props
    const { offerCollectionKey } = checklistItem
    const { ads, deviceCountryCode, userPregnancy, locale } = this.props

    const isCreatedByMe = checklistItem.createdBy === profile.id
    const todos = Object.values(checklistItem.todos || {})

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <View style={styles.checklistContainer}>
          <Text style={styles.headline}>{checklistItem.headline}</Text>

          <Text style={styles.description}>{checklistItem.description}</Text>

          {todos.map((todo, index) => (
            <ToDo
              key={todo.id}
              item={todo}
              onPress={this._toggleToDo}
              hasBorder={index !== 0}
            />
          ))}

          {isCreatedByMe && (
            <Text
              onPress={this._onEdit}
              style={styles.editText}
              textDecorationLine="underline"
            >
              {formatMessage(messages.editText)}
            </Text>
          )}
        </View>

        {offerCollectionKey === MODERNA_PREGNANCY_INSURANCE_CAMPAIGN && (
          <ModernaChecklistOffers
            navigation={this.props.navigation}
            todos={todos}
            deviceCountryCode={deviceCountryCode}
            userPregnancy={userPregnancy}
            locale={locale}
            profile={profile}
            dispatch={this._}
          />
        )}

        <View style={styles.adsCointainer}>
          {!offerCollectionKey &&
            ads.map(ad => (
              <ContentServiceAdvertisement
                key={ad.uid}
                ad={ad}
                dispatch={this._}
              />
            ))}
        </View>
      </ViewWrapper>
    )
  }

  componentDidMount() {
    const { checklistItem } = this.props
    const { offerCollectionKey } = checklistItem

    if (!offerCollectionKey) {
      this._fetchAds()
    }
  }

  _offerLoaded = offer => this._(AdActions.markInteraction(offer, 'impression'))

  _toggleToDo = todo => {
    const { checklistItem } = this.props

    this._(ChecklistActions.toggleCompletedTodo(todo, checklistItem))

    this._(ProfileActions.maybeAskUserToRateApp())
  }

  _onEdit = () => {
    const { checklistItem } = this.props

    this._goTo('/checklist-item-form', {
      flow: 'EDIT',
      checklistItemId: checklistItem.id,
    })
  }

  _fetchAds = async () => {
    const { profile } = this.props.screenProps
    const {
      deviceCountryCode,
      userPregnancy,
      checklistItem,
      locale,
    } = this.props

    const requestBody = {
      types: [AD_TYPE_CHECKLIST_OFFER],
      email: profile.email,
      countryCode: profile.location
        ? profile.location.countryCode
        : deviceCountryCode,
      locale,
      userType: profile.type,
      pregnancies: [userPregnancy.dueDate],
      checklistId: checklistItem.id,
    }

    try {
      await this._(AdActions.fetchAds(requestBody))
    } catch (error) {
      console.log(error)
    }
  }
}
