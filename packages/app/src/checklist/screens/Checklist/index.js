/**
 *
 * Checklist
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { Text, View, Image } from 'react-native'

import Loader from '@/shared/components/Loader'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import ChecklistItem from '@/checklist/components/molecules/ChecklistItem'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import CircleSegment from '@/shared/components/CircleSegment'
import { FormattedMessage } from '@/vendor/react-intl-native'
import { getVisibleChecklistItems } from '@/app/redux/selectors'
import { moderateScale } from '@/app/scaling'

import styles from './styles'
import messages from './messages'

const portionOfTodosCompleted = checklistItems => {
  const todoSum = (a, cv) => a + Object.keys(cv.todos).length

  const completedSum = (a, cv) =>
    a + Object.values(cv.todos).filter(t => t.completed).length

  const numberOfTodos = checklistItems.reduce(todoSum, 0)

  const numberOfCompletedTodos = checklistItems.reduce(completedSum, 0)

  return numberOfTodos > 0 ? numberOfCompletedTodos / numberOfTodos : 0
}

const sortedChecklistItems = checklistItems => {
  const userCreated = checklistItems.filter(c => !c.isAdminCreated)

  const adminCreated = checklistItems.filter(c => c.isAdminCreated)

  const userSort = (a, b) => new Date(a.createdAt) - new Date(b.createdAt)

  const adminSort = (a, b) => b.sortPriority - a.sortPriority

  const completedFilter = c =>
    !!Math.min(...Object.values(c.todos).map(t => t.completed))

  const notCompletedFilter = c =>
    !Math.min(...Object.values(c.todos).map(t => t.completed))

  const completedUserChecklists = userCreated
    .filter(completedFilter)
    .sort(userSort)

  const completedAdminChecklists = adminCreated
    .filter(completedFilter)
    .sort(adminSort)

  const notCompletedUserChecklists = userCreated
    .filter(notCompletedFilter)
    .sort(userSort)

  const notCompletedAdminChecklists = adminCreated
    .filter(notCompletedFilter)
    .sort(adminSort)

  return [
    ...notCompletedAdminChecklists,
    ...notCompletedUserChecklists,
    ...completedAdminChecklists,
    ...completedUserChecklists,
  ]
}

@firebaseConnect(props => {
  const listeners = ['content/checklistItems']

  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    listeners.push(
      `userOwned/checklistItems/${props.screenProps.profile.id}`,
      `userOwned/completedAdminTodosByUser/${props.screenProps.profile.id}`
    )
  }

  return listeners
})
@connect((state, props) => {
  const { profile } = props.screenProps

  let checklistItems = getVisibleChecklistItems(state)

  const isFetching = checklistItems === undefined

  checklistItems = Object.values(checklistItems || {})

  return {
    auth: state.firebase.auth,
    isFetching,
    checklistItems: sortedChecklistItems(checklistItems),
    portionOfTodosCompleted: portionOfTodosCompleted(checklistItems),
    isListening: Boolean(
      state.firebase.listeners.byId[
        `value:/userOwned/checklistItems/${profile.id}`
      ]
    ),
  }
})
export default class Checklist extends BaseScreen {
  static propTypes = {
    dispatch: PropTypes.func,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { isFetching } = this.props
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        scrollable={!isFetching}
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onPlusButtonPress={this._addNewItemButtonPressed}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        {this._renderContent()}
      </ViewWrapper>
    )
  }

  _renderContent = () => {
    const { isFetching, portionOfTodosCompleted, checklistItems } = this.props

    if (isFetching) {
      return <Loader background="#f9fafc" />
    }

    return (
      <React.Fragment>
        {portionOfTodosCompleted < 1 &&
          this._renderChart(portionOfTodosCompleted)}

        {portionOfTodosCompleted === 1 && this._renderCompleteImage()}

        {checklistItems.map(item => (
          <ChecklistItem
            checklistItem={item}
            key={item.id}
            onPress={this._onChecklistItemPressed}
          />
        ))}
      </React.Fragment>
    )
  }

  _renderChart = portion => {
    // progress-circle won't display if value is 0
    const DEFAULT_VALUE_PORTION = 0.001
    const percent = portion === 0 ? DEFAULT_VALUE_PORTION : portion

    return (
      <View style={styles.chartContainer}>
        <View style={styles.chart}>
          <CircleSegment
            width={moderateScale(111, 1)}
            strokeWidth={4}
            strokes={[
              { color: 'linear-teal', gradient: true, segment: portion },
              {
                color: '#E6E8F1',
                segment: 1 - percent,
              },
            ]}
          />
        </View>
        <Text style={styles.percentage}>{Math.floor(portion * 100)}%</Text>
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isListening && !nextProps.isListening) {
      const { profile } = this.props.screenProps

      this.props.firebase.watchEvent(
        'value',
        `userOwned/checklistItems/${profile.id}`
      )
    }
  }

  _renderCompleteImage = () => (
    <View style={styles.completedContainer}>
      <Image
        source={require('assets/img/thumbs-up.png')}
        style={styles.completedImageIcon}
      />

      <FormattedMessage
        style={styles.completedText}
        {...messages.completedText}
      />
    </View>
  )

  _addNewItemButtonPressed = () =>
    this._goTo('/checklist-item-form', { flow: 'ADD' })

  _onChecklistItemPressed = checklistItemId =>
    this._goTo('/checklist-item', { checklistItemId })
}
