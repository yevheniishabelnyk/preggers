import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'Checklists',
  completedText:
    'Awesome, checklist completed! You can add more tasks manually.',
})
