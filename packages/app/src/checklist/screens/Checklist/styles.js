import { StyleSheet, Platform } from 'react-native'
import { moderateScale } from '@/app/scaling'
import isIPhoneX from '@/shared/utils/isIPhoneX'

const gutter = moderateScale(21, 1.5)

const marginTop =
  Platform.OS === 'ios'
    ? moderateScale(isIPhoneX() ? -23 : 0)
    : moderateScale(-1)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: gutter,
  },

  contentContainer: {
    paddingBottom: moderateScale(150),
  },

  header: {
    marginTop,
    paddingHorizontal: moderateScale(22.5),
    backgroundColor: 'rgb(249,250,252)',
  },

  button: {
    marginRight: -gutter,
    marginBottom: moderateScale(16),
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },

  percentage: {
    color: 'rgb(30, 31, 36)',
    fontFamily: 'Poppins-Bold',
    fontSize: moderateScale(30),
    position: 'absolute',
  },

  chartContainer: {
    width: moderateScale(111, 1),
    height: moderateScale(111, 1),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },

  chart: {
    position: 'absolute',
    left: 0,
    top: 0,
  },

  completedContainer: {
    alignItems: 'center',
    marginVertical: moderateScale(20),
  },

  completedImageIcon: {
    width: moderateScale(150),
    height: moderateScale(167.3),
  },

  completedText: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(18),
    lineHeight: moderateScale(29),
    color: 'rgb(52, 57, 65)',
    marginTop: moderateScale(29),
    width: moderateScale(282),
    textAlign: 'center',
  },
})

export default styles
