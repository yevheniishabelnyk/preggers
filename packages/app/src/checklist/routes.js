/*
 *
 * Checklist routes
 *
 */

import ChecklistItem from './screens/ChecklistItem'
import ChecklistItemForm from './screens/ChecklistItemForm'

export default {
  '/checklist-item-form': {
    screen: ChecklistItemForm,
  },

  '/checklist-item': {
    screen: ChecklistItem,
  },
}
