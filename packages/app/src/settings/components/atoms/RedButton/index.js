/**
 *
 * RedButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Text } from 'react-native'

import styles from './styles'

export default class RedButton extends React.Component {
  static propTypes = {
    active: PropTypes.bool,
    title: PropTypes.string,
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { title, active, onPress, style } = this.props

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.button, active ? styles.activeButton : null, style]}
        activeOpacity={0.95}
      >
        <Text style={[styles.title, active ? styles.activeTitle : null]}>
          {title}
        </Text>
      </TouchableOpacity>
    )
  }
}
