import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    alignSelf: 'stretch',
    backgroundColor: 'white',
    paddingTop: moderateScale(10, 0.3),
    paddingBottom: moderateScale(10, 0.3),
  },

  activeButton: {
    backgroundColor: 'rgb(250,65,105)',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(15),
    letterSpacing: 0.8,
    color: 'rgb(131,146,167)',
  },

  activeTitle: {
    color: 'white',
  },
})

export default styles
