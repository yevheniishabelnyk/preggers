/**
 *
 * BackButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View, Image } from 'react-native'

import arrowIcon from 'assets/icons/arrow-left-thin.png'

import styles from './styles'

export default class BackButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    white: PropTypes.bool,
  }

  render() {
    const { onPress, style, white } = this.props

    const whiteBackground = white ? styles.whiteBackground : null

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.button, style]}
        activeOpacity={0.95}
      >
        <View style={[styles.content, whiteBackground]}>
          <Image source={arrowIcon} style={styles.icon} resizeMode="contain" />
        </View>
      </TouchableOpacity>
    )
  }
}
