import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const buttonWidth = moderateScale(40, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth * 1.5,
    height: buttonWidth * 1.5,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: buttonWidth * -0.25,
  },

  whiteBackground: {
    backgroundColor: 'white',
  },

  content: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    backgroundColor: 'transparent',
    alignItems: 'stretch',
    justifyContent: 'center',
  },

  icon: {
    backgroundColor: 'transparent',
    flex: 1,
    width: null,
    height: null,
    margin: moderateScale(10, resizeFactor),
  },
})

export default styles
