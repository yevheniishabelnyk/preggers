import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'row',
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(59,124,255)',
    textAlign: 'right',
    backgroundColor: 'transparent',
  },

  grayColor: {
    color: 'darkgray',
  },
})

export default styles
