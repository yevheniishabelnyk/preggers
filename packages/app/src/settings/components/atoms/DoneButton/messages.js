import { defineMessages } from 'react-intl'

export default defineMessages({
  title: 'Save',
  inProgressTitle: 'Processing...',
})
