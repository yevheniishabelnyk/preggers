/**
 *
 * DoneButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity } from 'react-native'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class DoneButton extends React.Component {
  static propTypes = {
    style: PropTypes.any,
    onPress: PropTypes.func,
    workInProgress: PropTypes.bool,
  }

  render() {
    const { style, onPress, workInProgress } = this.props

    const message = workInProgress ? messages.inProgressTitle : messages.title

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <FormattedMessage
          {...message}
          style={[styles.title, workInProgress ? styles.grayColor : null]}
        />
      </TouchableOpacity>
    )
  }
}
