import { StyleSheet, Platform } from 'react-native'
import { moderateScale } from '@/app/scaling'

export const loaderWidth = moderateScale(56)
const buttonHeight = moderateScale(56, 0.4)
const borderRadius = buttonHeight / 2

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: buttonHeight,
    borderRadius: borderRadius,
    shadowOpacity: 0.2,
    shadowRadius: 8,
    shadowColor: '#111723',
    shadowOffset: { height: 5, width: 0 },
    overflow: 'hidden',
  },

  stretch: {
    alignSelf: 'stretch',
  },

  button: {
    backgroundColor: 'rgb(250, 65, 105)',
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    height: buttonHeight,
  },

  title: {
    color: 'white',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(16, 0.4),
    textAlign: 'center',
  },

  loader: {
    marginTop: Platform.OS === 'ios' ? moderateScale(3) : moderateScale(1),
    marginLeft: Platform.OS === 'ios' ? moderateScale(2.5) : null,
  },

  transparentBackground: {
    backgroundColor: 'transparent',
  },

  grayBackground: {
    backgroundColor: 'rgb(201,201,201)',
  },

  grayTitle: {
    fontFamily: 'Now-Medium',
    color: 'rgb(154,154,154)',
    fontSize: moderateScale(14, 0.4),
  },
})

export default styles
