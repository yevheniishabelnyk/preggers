/**
 *
 * ActionButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  TouchableOpacity,
  Animated,
  ActivityIndicator,
  StyleSheet,
} from 'react-native'

import styles, { loaderWidth } from './styles'

export default class ActionButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    transparent: PropTypes.bool,
    gray: PropTypes.bool,
    style: PropTypes.any,
    titleStyle: PropTypes.any,
    loader: PropTypes.bool,
    buttonColor: PropTypes.string,
  }

  constructor(props) {
    super(props)

    if (props.style) {
      const flattenStyle = StyleSheet.flatten(props.style)

      if (flattenStyle.width) {
        this.buttonWidth = flattenStyle.width

        this._width = new Animated.Value(this.buttonWidth)
      }
    }
  }

  render() {
    const {
      title,
      onPress,
      style,
      transparent,
      gray,
      loader,
      titleStyle,
      buttonColor,
    } = this.props

    const animatedWidth = { width: this._width }
    const animatedOpacity = { opacity: this._opacity }

    return (
      <Animated.View
        style={[
          styles.container,
          style,
          animatedWidth,
          !this.buttonWidth ? styles.stretch : null,
        ]}
        onLayout={this._onLayout}
      >
        <TouchableOpacity
          style={[
            styles.button,
            gray ? styles.grayBackground : null,
            transparent ? styles.transparentBackground : null,
            buttonColor ? { backgroundColor: buttonColor } : null,
          ]}
          onPress={onPress}
          activeOpacity={0.95}
        >
          {loader ? (
            <ActivityIndicator
              animating
              size="large"
              color="white"
              style={styles.loader}
            />
          ) : (
            <Animated.Text
              style={[
                styles.title,
                titleStyle,
                transparent ? styles.grayTitle : null,
                animatedOpacity,
              ]}
            >
              {title}
            </Animated.Text>
          )}
        </TouchableOpacity>
      </Animated.View>
    )
  }

  _onLayout = e => {
    if (!this.buttonWidth) {
      this.buttonWidth = e.nativeEvent.layout.width

      if (!this._width) {
        this._width = new Animated.Value(this.buttonWidth)
      }
    }
  }

  componentWillMount() {
    this._opacity = new Animated.Value(1)

    const { loader } = this.props

    if (loader) {
      this._width = new Animated.Value(loaderWidth)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.loader && nextProps.loader) {
      this._showLoader()
    }

    if (this.props.loader && !nextProps.loader) {
      this._hideLoader()
    }
  }

  _showLoader = () => {
    this._opacity.setValue(0)

    Animated.timing(this._width, {
      toValue: loaderWidth,
      duration: 200,
    }).start()
  }

  _hideLoader = () => {
    Animated.timing(this._width, {
      toValue: this.buttonWidth,
      duration: 300,
    }).start()

    Animated.timing(this._opacity, {
      toValue: 1,
      duration: 700,
    }).start()
  }
}
