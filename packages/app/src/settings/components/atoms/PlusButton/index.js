/**
 *
 * PlusButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View } from 'react-native'

import styles from './styles'

export default class PlusButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
  }

  render() {
    const { onPress } = this.props

    return (
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <View style={styles.lineWrapper}>
          <View style={styles.horizontalLine} />
        </View>

        <View style={[styles.lineWrapper, styles.onTop]}>
          <View style={styles.verticalLine} />
        </View>
      </TouchableOpacity>
    )
  }
}
