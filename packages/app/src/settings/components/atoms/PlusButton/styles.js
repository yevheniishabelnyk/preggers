import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const buttonWidth = moderateScale(40, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    backgroundColor: 'rgb(59,124,255)',
    position: 'relative',
    overflow: 'hidden',
    zIndex: 2,
  },

  lineWrapper: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },

  onTop: {
    zIndex: 2,
  },

  horizontalLine: {
    width: moderateScale(15, resizeFactor),
    height: moderateScale(2, resizeFactor),
    backgroundColor: 'white',
  },

  verticalLine: {
    height: moderateScale(15, resizeFactor),
    width: moderateScale(2, resizeFactor),
    backgroundColor: 'white',
    marginTop: moderateScale(1, resizeFactor),
  },
})

export default styles
