import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  button: {
    height: (37 / 375) * width,
    width: (156 / 375) * width,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: (37 / 2 / 375) * width,
    shadowOpacity: 1,
    shadowRadius: (10 / 375) * width,
    shadowColor: 'rgba(0,0,0,0.08)',
    shadowOffset: { height: 8, width: 1 },
  },

  buttonTitle: {
    fontFamily: 'Now-Bold',
    fontSize: (13 / 375) * width,
    textAlign: 'center',
    flex: 1,
    color: 'rgb(34, 33, 39)',
  },

  buttonActive: {
    backgroundColor: '#fa4169',
    shadowOpacity: 0,
  },

  buttonTitleActive: {
    color: 'white',
  },
})

export default styles
