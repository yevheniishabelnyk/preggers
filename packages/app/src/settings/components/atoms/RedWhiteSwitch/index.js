/**
 *
 * RedWhiteSwitch
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View, Text } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class RedWhiteSwitch extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.bool,
    onTitle: PropTypes.string,
    offTitle: PropTypes.string,
    style: PropTypes.any,
  }

  state = {
    value: true,
  }

  render() {
    const { onTitle, offTitle, style } = this.props

    return (
      <View style={[styles.container, style]}>
        <TouchableOpacity
          style={[styles.button, this.state.value ? styles.buttonActive : null]}
          onPress={this._setOn}
          activeOpacity={0.95}
        >
          <Text
            style={[
              styles.buttonTitle,
              this.state.value ? styles.buttonTitleActive : null,
            ]}
          >
            {onTitle}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[
            styles.button,
            !this.state.value ? styles.buttonActive : null,
          ]}
          onPress={this._setOff}
          activeOpacity={0.95}
        >
          <Text
            style={[
              styles.buttonTitle,
              !this.state.value ? styles.buttonTitleActive : null,
            ]}
          >
            {offTitle}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({ value: nextProps.value })
    }
  }

  _setOn = () => this._onChange(true)

  _setOff = () => this._onChange(false)

  _onChange = value => {
    if (value !== this.state.value) {
      const { onChange } = this.props

      if (isFunction(onChange)) {
        onChange(value)
      }

      this.setState({ value })
    }
  }
}
