/**
 *
 * SmallCloseButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View } from 'react-native'

import styles from './styles'

export default class SmallCloseButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    white: PropTypes.bool,
  }

  render() {
    const { onPress, style, white } = this.props

    const whiteBackground = white ? styles.whiteBackground : null

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.button, style]}
        activeOpacity={0.95}
      >
        <View style={[styles.content, whiteBackground]}>
          <View style={styles.line} />
          <View style={[styles.line, styles.rotate]} />
        </View>
      </TouchableOpacity>
    )
  }
}
