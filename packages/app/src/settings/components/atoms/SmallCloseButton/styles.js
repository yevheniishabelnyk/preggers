import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const buttonWidth = moderateScale(22, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth * 2,
    height: buttonWidth * 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: buttonWidth * -0.5,
  },

  whiteBackground: {
    backgroundColor: 'white',
  },

  content: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 1.6,
    backgroundColor: 'rgb(230, 234, 241)',

    alignItems: 'center',
    justifyContent: 'center',
  },

  line: {
    width: buttonWidth / 1.6,
    height: moderateScale(2, resizeFactor),
    backgroundColor: 'rgb(252, 252, 253)',
    transform: [{ rotate: '45deg' }],
  },

  rotate: {
    transform: [{ rotate: '135deg' }],
    marginTop: moderateScale(-2, resizeFactor),
  },
})

export default styles
