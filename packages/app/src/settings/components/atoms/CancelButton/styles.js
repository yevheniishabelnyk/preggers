import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'row',
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(255,59,149)',
    textAlign: 'left',
    backgroundColor: 'transparent',
  },
})

export default styles
