/**
 *
 * CancelButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity } from 'react-native'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class CancelButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { style, onPress } = this.props

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <FormattedMessage {...messages.title} style={styles.title} />
      </TouchableOpacity>
    )
  }
}
