import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const buttonWidth = moderateScale(40, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    backgroundColor: 'white',
    position: 'relative',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },

  searchIcon: {
    width: moderateScale(19, resizeFactor),
    height: moderateScale(19, resizeFactor),
  },
})

export default styles
