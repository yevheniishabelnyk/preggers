/**
 *
 * SearchButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Image } from 'react-native'

import styles from './styles'

export default class SearchButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
  }

  render() {
    const { onPress } = this.props

    return (
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <Image
          source={require('assets/icons/search-black.png')}
          style={styles.searchIcon}
        />
      </TouchableOpacity>
    )
  }
}
