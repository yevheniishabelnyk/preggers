/**
 *
 * FormSwitch
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Switch } from 'react-native'

import styles from './styles'

export default class FormSwitch extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.bool,
    error: PropTypes.bool,
    disabled: PropTypes.bool,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    title: PropTypes.string,
  }

  render() {
    const {
      title,
      style,
      value = false,
      onChange,
      disabled = false,
      error,
    } = this.props

    return (
      <View style={[styles.container, error ? styles.redBorder : null, style]}>
        <Text style={styles.title}>{title}</Text>

        <Switch
          onTintColor="#40E0BE"
          style={styles.switch}
          value={value}
          onValueChange={onChange}
          disabled={disabled}
        />
      </View>
    )
  }
}
