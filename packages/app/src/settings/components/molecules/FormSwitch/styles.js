import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: moderateScale(14, resizeFactor),
    paddingHorizontal: moderateScale(15, resizeFactor),
    minHeight: moderateScale(60, resizeFactor),
    borderWidth: 1,
    borderColor: 'transparent',
  },

  redBorder: {
    borderColor: 'rgb(250,65,105)',
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    color: 'rgb(131,146,167)',
  },

  switch: {
    marginLeft: 'auto',
  },
})

export default styles
