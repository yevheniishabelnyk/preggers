import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignSelf: 'stretch',
    borderRadius: moderateScale(8),
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    height: moderateScale(60, resizeFactor),
    borderWidth: moderateScale(1),
    borderColor: 'transparent',
    paddingRight: moderateScale(9, 0.7),
  },

  units: {
    position: 'absolute',
    alignSelf: 'center',
    right: moderateScale(9, 0.7),
    fontSize: moderateScale(14, resizeFactor),
    color: 'rgb(131, 146, 167)',
    fontFamily: 'Now-Medium',
  },

  inputWrapper: {
    flexDirection: 'row',
    flex: 1,
  },

  redBorder: {
    borderColor: 'rgb(250,65,105)',
  },

  titleWrapper: {
    position: 'absolute',
    zIndex: 1,
    backgroundColor: 'white',
    paddingLeft: moderateScale(19, resizeFactor),
    paddingRight: moderateScale(20, resizeFactor),
  },

  title: {
    position: 'absolute',
    left: moderateScale(18, resizeFactor),
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },

  multilineTitle: {
    marginTop: moderateScale(-15, resizeFactor),
    paddingTop: moderateScale(10, resizeFactor),
    borderRadius: moderateScale(8),
    overflow: 'hidden',
    backgroundColor: 'transparent',
    width: '100%',
    zIndex: 1,
  },

  errorTitle: {
    color: 'rgb(255, 100, 101)',
  },

  errorIcon: {
    position: 'absolute',
    width: moderateScale(20),
    height: moderateScale(20),
    top: moderateScale(19, resizeFactor),
    right: moderateScale(5, resizeFactor),
  },

  textareaErrorIcon: {
    top: moderateScale(14),
    right: moderateScale(-3.7),
  },

  value: {
    textAlign: 'right',
    alignSelf: 'center',
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    marginLeft: 'auto',
    fontFamily: 'Now-Medium',
    color: 'rgb(55,55,64)',
  },

  input: {
    height: moderateScale(60, resizeFactor),
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(55,55,64)',
    flex: 1,
    paddingLeft: moderateScale(18, resizeFactor),
    paddingTop: moderateScale(18, resizeFactor),
  },

  alignLeft: {
    textAlign: 'left',
    width: 'auto',
    flex: 1,
  },

  inputFocused: {
    shadowOpacity: 0.1,
    shadowRadius: 22,
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: { height: 8, width: -2 },
  },
})

export default styles
