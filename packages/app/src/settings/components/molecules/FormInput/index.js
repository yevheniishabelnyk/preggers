/**
 *
 * FormInput
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, TextInput, Animated, Image, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

import { isFunction } from 'lodash'

import styles from './styles'

export default class FormInput extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    error: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onLayout: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    numberOfLines: PropTypes.number,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    inputStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    titleStyle: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    returnKeyType: PropTypes.string,
    keyboardType: PropTypes.string,
    enablesReturnKeyAutomatically: PropTypes.bool,
    autoCorrect: PropTypes.bool,
    blurOnSubmit: PropTypes.bool,
    isRequesting: PropTypes.bool,
    onKeyPress: PropTypes.func,
    autoCapitalize: PropTypes.string,
    clearButtonMode: PropTypes.string,
    units: PropTypes.any,
    rightIcon: PropTypes.element,
    maxLength: PropTypes.number,
  }

  static defaultProps = {
    clearButtonMode: 'always',
  }

  state = {
    inputFocused: false,
    isFocused: false,
    isMultiline: false,
  }

  render() {
    const {
      title,
      value,
      onChange,
      style,
      placeholder = '',
      numberOfLines = 1,
      inputStyle,
      returnKeyType,
      enablesReturnKeyAutomatically,
      onKeyPress,
      keyboardType,
      autoCapitalize,
      error,
      autoCorrect,
      onSubmitEditing,
      units,
      clearButtonMode,
      titleStyle,
      rightIcon,
      blurOnSubmit,
      onLayout,
      maxLength,
    } = this.props

    const { isMultiline } = this.state
    const resizeFactor = 0.35

    const labelStyle = {
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [
          Platform.OS === 'android'
            ? moderateScale(18, resizeFactor)
            : moderateScale(22, resizeFactor),
          moderateScale(15, resizeFactor),
        ],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [
          moderateScale(14, resizeFactor),
          moderateScale(10, resizeFactor),
        ],
      }),
    }

    return (
      <View
        style={[
          styles.container,
          style,
          this.state.inputFocused ? styles.inputFocused : null,
        ]}
        onLayout={onLayout}
      >
        {title && !placeholder ? (
          <Animated.Text
            style={[
              styles.title,
              titleStyle,
              labelStyle,
              isMultiline ? styles.multilineTitle : null,
              error ? styles.errorTitle : null,
            ]}
          >
            {title}
          </Animated.Text>
        ) : null}

        {onChange ? (
          <View style={styles.inputWrapper}>
            <TextInput
              underlineColorAndroid="transparent"
              ref={ref => {
                this.textInput = ref
              }}
              maxLength={maxLength}
              multiline={numberOfLines > 1}
              numberOfLines={numberOfLines}
              onChangeText={onChange}
              placeholder={placeholder}
              placeholderTextColor="#8392a7"
              clearButtonMode={clearButtonMode}
              autoCapitalize={autoCapitalize}
              value={value}
              style={[
                styles.input,
                !title ? styles.alignLeft : null,
                inputStyle,
              ]}
              returnKeyType={returnKeyType}
              enablesReturnKeyAutomatically={enablesReturnKeyAutomatically}
              onKeyPress={onKeyPress}
              keyboardType={keyboardType}
              onFocus={this._onFocus}
              onBlur={this._onBlur}
              autoCorrect={autoCorrect}
              onSubmitEditing={onSubmitEditing}
              blurOnSubmit={blurOnSubmit}
            />

            {units ? (
              <Text style={styles.units}>{units.toUpperCase()}</Text>
            ) : null}

            {error ? (
              <Image
                source={require('assets/icons/error-icon.png')}
                style={[
                  styles.errorIcon,
                  numberOfLines > 1 ? styles.textareaErrorIcon : null,
                ]}
              />
            ) : null}
          </View>
        ) : (
          <Text style={styles.value}>{value}</Text>
        )}
        {rightIcon}
      </View>
    )
  }

  componentWillMount() {
    const { value } = this.props
    let newValue

    if (value === '' || value === null || value === undefined) {
      newValue = ''
    }

    this._animatedIsFocused = new Animated.Value(newValue === '' ? 0 : 1)
  }

  componentDidUpdate() {
    const { value } = this.props

    let newValue

    if (value === '' || value === null || value === undefined) {
      newValue = ''
    }

    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || newValue !== '' ? 1 : 0,
      duration: 200,
    }).start()
  }

  _onFocus = () => {
    const { numberOfLines } = this.props

    let isMultiline = numberOfLines > 1 ? true : false

    this.setState({ inputFocused: true, isFocused: true, isMultiline })

    if (isFunction(this.props.onFocus)) {
      this.props.onFocus()
    }
  }

  _onBlur = () => {
    const { value, numberOfLines } = this.props

    let isMultiline = numberOfLines > 1 ? true : false

    this.setState({
      inputFocused: false,
      isFocused: false,
      isMultiline: value && isMultiline ? true : false,
    })

    if (isFunction(this.props.onBlur)) {
      this.props.onBlur()
    }
  }
}
