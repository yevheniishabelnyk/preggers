import { Dimensions, StyleSheet } from 'react-native'

function vw(percentageWidth) {
  return Dimensions.get('window').width * (percentageWidth / 100)
}

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
  },

  formField: {
    flexDirection: 'row',
    paddingHorizontal: moderateScale(19, resizeFactor),
    alignItems: 'center',
    height: moderateScale(60, resizeFactor),
    borderRadius: 8,
    backgroundColor: 'white',
    overflow: 'hidden',
  },

  formFieldInColumn: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'center',
  },

  icon: {
    position: 'absolute',
    alignSelf: 'center',
    right: moderateScale(18, resizeFactor),
    width: moderateScale(12, resizeFactor),
    height: moderateScale(6, resizeFactor),
    marginLeft: moderateScale(11, resizeFactor),
  },

  value: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    marginLeft: 'auto',
    fontFamily: 'Now-Medium',
    color: '#000',
    width: moderateScale(200, resizeFactor),
    textAlign: 'right',
  },

  valueLeftAlign: {
    marginLeft: 0,
    textAlign: 'left',
  },

  placeholderColor: {
    color: 'rgb(250, 65, 105)',
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
    marginTop: moderateScale(4, resizeFactor),
  },

  titleSmall: {
    fontSize: moderateScale(10, resizeFactor),
    marginTop: moderateScale(3, resizeFactor),
    marginBottom: moderateScale(1, resizeFactor),
  },

  errorTitle: {
    color: 'rgb(255, 100, 101)',
  },

  pickerWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },

  picker: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingTop: moderateScale(20, 1.5),
    width: vw(100),
    backgroundColor: 'white',
  },

  androidPickerOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    opacity: 0,
  },

  selectButton: {
    flex: 1,
    position: 'absolute',
    backgroundColor: '#6341fb',
    bottom: moderateScale(185, 0.1),
    right: moderateScale(15, resizeFactor),
    borderRadius: 20,
    padding: moderateScale(10, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
  },

  selectButtonTitle: {
    color: 'white',
  },

  cancelButton: {
    backgroundColor: 'rgba(0,0,0,0)',
    flex: 1,
    position: 'absolute',
    bottom: moderateScale(185, 0.1),
    left: moderateScale(15, resizeFactor),
    borderRadius: 20,
    padding: moderateScale(10, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
  },

  cancelButtonTitle: {
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#8A8EAC',
  },

  cancelMask: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,.5)',
  },

  expandIcon: {
    position: 'absolute',
    right: moderateScale(38, resizeFactor),
    top: moderateScale(23, resizeFactor),
    width: moderateScale(26, resizeFactor),
    height: moderateScale(14, resizeFactor),
  },
})

export default styles
