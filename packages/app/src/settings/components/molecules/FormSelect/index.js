/**
 *
 * FormSelect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  Modal,
  Picker,
  Text,
  TouchableHighlight,
  View,
  Image,
  Platform,
} from 'react-native'

import { isFunction } from 'lodash'

import arrowDownIcon from 'assets/icons/arrow-down-bold.png'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default class FormSelect extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    items: PropTypes.array,
    selectableItems: PropTypes.array,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onSelect: PropTypes.func,
    error: PropTypes.bool,
    defaultValue: PropTypes.string,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.labelMap = props.items.reduce((out, item) => {
      out[item.value] = item.label
      return out
    }, {})

    this.initialValue = props.value

    this.state = {
      showPicker: false,
      selectedIndex: null,
      selectedValue: props.value || null,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.items !== nextProps.items) {
      this.labelMap = nextProps.items.reduce((out, item) => {
        out[item.value] = item.label
        return out
      }, {})
    }
  }

  render() {
    const { style, error, defaultValue, value } = this.props

    return (
      <View style={[styles.container, style]}>
        <TouchableHighlight
          onPress={this._showPicker}
          underlayColor="rgba(255,255,255,.5)"
        >
          <View
            style={[styles.formField, value ? styles.formFieldInColumn : null]}
          >
            <Text
              style={[
                styles.title,
                value ? styles.titleSmall : null,
                error ? styles.errorTitle : null,
              ]}
            >
              {this.props.title}
            </Text>

            <Text
              style={[
                styles.value,
                !this.labelMap[value] ? styles.placeholderColor : null,
                value ? styles.valueLeftAlign : null,
              ]}
              numberOfLines={1}
            >
              {this.labelMap[value] || this.labelMap[defaultValue]}
            </Text>

            <Image source={arrowDownIcon} style={styles.icon} />
            {Platform.OS === 'android' && (
              <View style={styles.androidPickerOverlay}>
                {this._renderPicker()}
              </View>
            )}
          </View>
        </TouchableHighlight>

        {Platform.OS === 'ios' && (
          <Modal
            onRequestClose={this._hidePicker}
            animationType="fade"
            transparent
            visible={this.state.showPicker}
          >
            <TouchableHighlight
              style={styles.cancelMask}
              onPress={this._cancel}
              underlayColor="rgba(255,255,255,.5)"
            >
              <View style={styles.pickerWrapper}>
                {this._renderPicker()}
                <TouchableHighlight
                  style={styles.selectButton}
                  onPress={this._select}
                  underlayColor="rgba(99,65,251,.8)"
                >
                  <FormattedMessage
                    {...messages.selectButtonTitle}
                    style={styles.selectButtonTitle}
                  />
                </TouchableHighlight>
                <TouchableHighlight
                  style={styles.cancelButton}
                  onPress={this._cancel}
                >
                  <FormattedMessage
                    {...messages.cancelButtonTitle}
                    style={styles.cancelButtonTitle}
                  />
                </TouchableHighlight>
              </View>
            </TouchableHighlight>
          </Modal>
        )}
      </View>
    )
  }

  _renderPicker = () => {
    const { formatMessage } = this.context.intl
    const { selectableItems, items } = this.props

    const choices = selectableItems || items

    return (
      <Picker
        style={styles.picker}
        selectedValue={this.state.selectedValue}
        onValueChange={this._valueChangeHandler}
      >
        {Platform.OS === 'android' && (
          <Picker.Item
            key={null}
            label={formatMessage(messages.defaultPickerItemLabel)}
            value={null}
          />
        )}
        {choices.map(item => (
          <Picker.Item
            key={item.value}
            label={item.label}
            value={item.value}
            style={styles.label}
          />
        ))}
      </Picker>
    )
  }

  _showPicker = () => this.setState({ showPicker: true })

  _hidePicker = () => this.setState({ showPicker: false })

  _select = () => {
    this.initialValue =
      this.state.selectedValue || Object.keys(this.labelMap)[0]

    this._hidePicker()

    if (isFunction(this.props.onSelect)) {
      this.props.onSelect(this.initialValue)
    }
  }

  _cancel = () => {
    if (this.initialValue !== this.state.selectedValue) {
      this.setState({
        selectedValue: this.initialValue,
        showPicker: false,
      })
    } else {
      this.setState({ showPicker: false })
    }
  }

  _valueChangeHandler = selectedValue => {
    this.setState({ selectedValue }, () => {
      if (Platform.OS === 'android' && this.state.selectedValue !== null) {
        this._select()
      }
    })
  }
}
