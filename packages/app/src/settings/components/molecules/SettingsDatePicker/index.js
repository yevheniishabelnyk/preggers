/**
 *
 * SettingsDatePicker
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableHighlight } from 'react-native'

import datePickerModal from '@/shared/decorators/datePickerModal'

import calendarIcon from 'assets/icons/red-calendar-icon.png'
import clockIcon from 'assets/icons/clock-red-icon.png'

import styles from './styles'

@datePickerModal
export default class SettingsDatePicker extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    value: PropTypes.string,
    mode: PropTypes.string,
    showDatePicker: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { title, value, mode, showDatePicker, style } = this.props

    return (
      <TouchableHighlight
        style={[styles.button, style]}
        underlayColor="rgba(255,255,255,.5)"
        onPress={showDatePicker}
      >
        <View style={[styles.content, value ? styles.contentInColumn : null]}>
          <Text style={[styles.title, value ? styles.smallTitle : null]}>
            {title}
          </Text>

          <Text style={styles.value}>{value}</Text>

          <Image
            source={mode === 'time' ? clockIcon : calendarIcon}
            style={[styles.icon, mode === 'time' ? styles.clock : null]}
          />
        </View>
      </TouchableHighlight>
    )
  }
}
