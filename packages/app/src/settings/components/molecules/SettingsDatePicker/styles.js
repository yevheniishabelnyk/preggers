import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: moderateScale(19, resizeFactor),
    height: moderateScale(60, resizeFactor),
    borderRadius: moderateScale(8, resizeFactor),
    backgroundColor: 'white',
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
  },

  contentInColumn: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    justifyContent: 'center',
  },

  value: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: '#000',
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
    marginTop: moderateScale(10, resizeFactor),
  },

  smallTitle: {
    fontSize: moderateScale(10, resizeFactor),
    marginTop: moderateScale(3, resizeFactor),
    marginBottom: moderateScale(1, resizeFactor),
  },

  icon: {
    position: 'absolute',
    alignSelf: 'center',
    right: moderateScale(18, resizeFactor),
    height: moderateScale(22, resizeFactor),
    width: moderateScale(22, resizeFactor),
    marginLeft: moderateScale(11, resizeFactor),
  },

  clock: {
    height: moderateScale(25, resizeFactor),
    width: moderateScale(25, resizeFactor),
  },
})

export default styles
