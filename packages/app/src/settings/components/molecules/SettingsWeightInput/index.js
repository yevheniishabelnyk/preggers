/**
 *
 * Settings Weight Input
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, TouchableHighlight, View } from 'react-native'

import lbPickerModal from '@/shared/decorators/lbPickerModal'

import styles from './styles'

@lbPickerModal
export default class SettingsWeightInput extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    value: PropTypes.string,
    showLbPicker: PropTypes.func,
    style: PropTypes.any,
  }

  render() {
    const { title, value, showLbPicker, style } = this.props

    return (
      <TouchableHighlight
        style={[styles.button, style]}
        onPress={showLbPicker}
        underlayColor="rgba(255,255,255,.5)"
      >
        <View
          style={[
            styles.contentInRow,
            value !== 'lb' ? styles.contentInColumn : null,
          ]}
        >
          <Text
            style={[styles.title, value !== 'lb' ? styles.titleSmall : null]}
          >
            {title}
          </Text>

          <Text
            style={[
              styles.value,
              value !== 'lb' ? styles.valueLeftAlign : styles.valueRightAlign,
            ]}
          >
            {value === 'lb' ? value.toUpperCase() : value}
          </Text>
        </View>
      </TouchableHighlight>
    )
  }
}
