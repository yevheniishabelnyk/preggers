import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.35

const styles = StyleSheet.create({
  contentInRow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: moderateScale(18, resizeFactor),
    paddingRight: moderateScale(18, resizeFactor),
    height: moderateScale(60, resizeFactor),
    borderRadius: 8,
    backgroundColor: 'white',
    overflow: 'hidden',
  },

  contentInColumn: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },

  titleSmall: {
    fontSize: moderateScale(10, resizeFactor),
  },

  value: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(55, 55, 64)',
    marginLeft: 'auto',
  },

  valueLeftAlign: {
    marginLeft: 0,
  },

  valueRightAlign: {
    color: 'rgb(131, 146, 167)',
  },
})

export default styles
