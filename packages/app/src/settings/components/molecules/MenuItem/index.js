/**
 *
 * MenuItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity, Text, Image } from 'react-native'

import arrowIcon from 'assets/icons/arrow-left-thin.png'

import styles from './styles'

export default class MenuItem extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    title: PropTypes.string,
    arrow: PropTypes.bool,
    last: PropTypes.bool,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { title, onPress, style, arrow, last } = this.props

    const borderBottomStyle = last ? styles.borderBottom : null

    return (
      <View style={[styles.containerWrapper, style, borderBottomStyle]}>
        <TouchableOpacity
          style={[styles.container]}
          onPress={onPress}
          activeOpacity={0.95}
        >
          <View style={styles.content}>
            <Text style={styles.title}>{title}</Text>

            {arrow ? (
              <View style={styles.iconWrapper}>
                <Image
                  source={arrowIcon}
                  style={[styles.icon, styles.rotate]}
                  resizeMode="contain"
                />
              </View>
            ) : null}
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
