import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  containerWrapper: {
    borderTopWidth: 1,
    borderTopColor: 'rgb(235,235,235)',
  },

  container: {
    width,
    height: moderateScale(60, resizeFactor),
    paddingHorizontal: moderateScale(30, resizeFactor),
  },

  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(235,235,235)',
  },

  content: {
    height: moderateScale(60, resizeFactor),
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    fontFamily: 'Now-Medium',
    letterSpacing: 0.7,
    color: 'rgb(131,146,167)',
  },

  iconWrapper: {
    width: moderateScale(23, resizeFactor),
    height: moderateScale(23, resizeFactor),
    backgroundColor: 'transparent',
  },

  icon: {
    backgroundColor: 'transparent',
    flex: 1,
    width: null,
    height: null,
  },

  rotate: {
    transform: [{ rotate: '180deg' }],
  },
})

export default styles
