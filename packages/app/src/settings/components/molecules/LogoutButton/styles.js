import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.25

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    alignSelf: 'stretch',
    height: moderateScale(60, resizeFactor),
    flexDirection: 'row',
    borderRadius: moderateScale(27, resizeFactor),
    backgroundColor: 'white',
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(112,52,153)',
    textAlign: 'center',
    flex: 1,
  },
})

export default styles
