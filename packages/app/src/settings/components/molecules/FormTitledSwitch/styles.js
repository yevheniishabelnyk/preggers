import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const styles = StyleSheet.create({
  container: {
    height: moderateScale(40, resizeFactor),
    borderRadius: moderateScale(30, resizeFactor),
    backgroundColor: 'rgb(59,124,255)',
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: moderateScale(17, resizeFactor),
    paddingRight: moderateScale(11, resizeFactor),
  },

  containerOff: {
    backgroundColor: '#40debd',
    paddingLeft: moderateScale(11, resizeFactor),
    paddingRight: moderateScale(17, resizeFactor),
  },

  title: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    color: 'white',
  },

  titleOn: {
    marginRight: moderateScale(10, resizeFactor),
  },

  titleOff: {
    marginLeft: moderateScale(10, resizeFactor),
  },

  circle: {
    width: moderateScale(20, resizeFactor),
    height: moderateScale(20, resizeFactor),
    borderRadius: moderateScale(10, resizeFactor),
    backgroundColor: 'white',
  },
})

export default styles
