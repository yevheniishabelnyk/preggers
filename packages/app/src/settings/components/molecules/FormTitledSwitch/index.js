/**
 *
 * FormTitledSwitch
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View, Text } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class FormTitledSwitch extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    onTitle: PropTypes.string,
    offTitle: PropTypes.string,
    value: PropTypes.bool,
    style: PropTypes.any,
  }

  static defaultProps = {
    onTitle: 'On',
    offTitle: 'Off',
  }

  render() {
    const { value, style, onTitle, offTitle } = this.props

    return (
      <TouchableOpacity
        onPress={this._switchPressed}
        style={[styles.switch, style]}
        activeOpacity={0.95}
      >
        {value ? (
          <View style={styles.container}>
            <Text style={[styles.title, styles.titleOn]}>{onTitle}</Text>
            <View style={styles.circle} />
          </View>
        ) : (
          <View style={[styles.container, styles.containerOff]}>
            <View style={styles.circle} />
            <Text style={[styles.title, styles.titleOff]}>{offTitle}</Text>
          </View>
        )}
      </TouchableOpacity>
    )
  }

  _switchPressed = () => {
    const { onChange, value } = this.props

    if (isFunction(onChange)) {
      onChange(!value)
    }
  }
}
