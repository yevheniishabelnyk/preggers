import { defineMessages } from 'react-intl'

export default defineMessages({
  selectButtonTitle: 'Select',
  cancelButtonTitle: 'Cancel',
  defaultPickerItemLabel: 'Please select',
})
