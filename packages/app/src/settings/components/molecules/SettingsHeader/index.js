/**
 *
 * SettingsHeader
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'

import DoneButton from '@/settings/components/atoms/DoneButton'
import PlusButton from '@/settings/components/atoms/PlusButton'
import CancelButton from '@/settings/components/atoms/CancelButton'
import CloseButton from '@/settings/components/atoms/CloseButton'
import BackButton from '@/settings/components/atoms/BackButton'
import SearchButton from '@/settings/components/atoms/SearchButton'

import styles, { containerHeight } from './styles'

export default class SettingsHeader extends React.Component {
  static propTypes = {
    onBackButtonPress: PropTypes.func,
    onDoneButtonPress: PropTypes.func,
    onPlusButtonPress: PropTypes.func,
    onSearchButtonPress: PropTypes.func,
    title: PropTypes.string,
    icon: PropTypes.any,
    style: PropTypes.any,
    titleStyle: PropTypes.any,
    close: PropTypes.bool,
    withInidcators: PropTypes.bool,
    cancel: PropTypes.bool,
    workInProgress: PropTypes.bool,
  }

  static height = containerHeight

  render() {
    const {
      onBackButtonPress,
      onDoneButtonPress,
      onPlusButtonPress,
      onSearchButtonPress,
      title = '',
      style,
      titleStyle,
      close,
      cancel,
      workInProgress,
      icon,
      withInidcators,
    } = this.props

    return (
      <View
        style={[
          styles.container,
          style,
          withInidcators ? styles.paddingTop : null,
        ]}
      >
        {cancel ? (
          <CancelButton
            style={styles.cancelButton}
            onPress={onBackButtonPress}
          />
        ) : close ? (
          <CloseButton
            white
            style={styles.backButton}
            onPress={onBackButtonPress}
          />
        ) : onBackButtonPress ? (
          <BackButton
            white
            style={styles.backButton}
            onPress={onBackButtonPress}
          />
        ) : (
          <View style={styles.leftButtonPlaceholder} />
        )}

        <View
          style={[
            styles.titleContainterAbsolute,
            withInidcators ? styles.paddingTop : null,
          ]}
        >
          {icon ? <Image source={icon} style={styles.icon} /> : null}

          <Text style={[styles.title, titleStyle]}>{title}</Text>
        </View>

        {onDoneButtonPress ? (
          <DoneButton
            style={styles.doneButton}
            onPress={onDoneButtonPress}
            workInProgress={workInProgress}
          />
        ) : null}

        {onPlusButtonPress ? (
          <PlusButton style={styles.plusButton} onPress={onPlusButtonPress} />
        ) : null}

        {onSearchButtonPress ? (
          <SearchButton
            style={styles.plusButton}
            onPress={onSearchButtonPress}
          />
        ) : null}
      </View>
    )
  }
}
