import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.4

export const containerHeight = moderateScale(80, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: containerHeight,
    alignSelf: 'stretch',
    flexDirection: 'row',
    position: 'relative',
  },

  leftButtonPlaceholder: {
    marginRight: 'auto',
  },

  backButton: {
    marginRight: 'auto',
  },

  cancelButton: {
    marginRight: 'auto',
  },

  doneButton: {
    marginLeft: 'auto',
  },

  titleContainterAbsolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  paddingTop: {
    paddingTop: moderateScale(18),
  },

  titleContainter: {
    alignSelf: 'center',
  },

  title: {
    textAlign: 'center',
    backgroundColor: 'transparent',
    color: 'rgb(59,72,89)',
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(13, 0.6),
  },

  icon: {
    width: moderateScale(15, resizeFactor),
    height: moderateScale(15, resizeFactor),
    marginRight: moderateScale(5, resizeFactor),
  },
})

export default styles
