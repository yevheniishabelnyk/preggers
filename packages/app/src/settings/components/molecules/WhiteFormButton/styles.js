import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const checkedIconContainerWidth = moderateScale(34, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    borderRadius: 8,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: moderateScale(19, resizeFactor),
    paddingVertical: moderateScale(17, resizeFactor),
  },

  title: {
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    lineHeight: moderateScale(24, resizeFactor),
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
    backgroundColor: 'transparent',
    marginRight: moderateScale(45, resizeFactor),
  },

  arrowWrapper: {
    width: moderateScale(23, resizeFactor),
    height: moderateScale(23, resizeFactor),
    backgroundColor: 'transparent',
    alignItems: 'stretch',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight:
      Platform.OS === 'android'
        ? moderateScale(25, resizeFactor)
        : moderateScale(5, resizeFactor),
  },

  arrow: {
    backgroundColor: 'transparent',
    flex: 1,
    width: null,
    height: null,
  },

  rotate: {
    transform: [{ rotate: '180deg' }],
  },

  iconWrapper: {
    width: moderateScale(23, resizeFactor),
    height: moderateScale(23, resizeFactor),
    marginRight: moderateScale(20, resizeFactor),
  },

  icon: {
    width: null,
    height: null,
    flex: 1,
  },

  checkedIconContainer: {
    width: checkedIconContainerWidth,
    height: checkedIconContainerWidth,
    borderRadius: checkedIconContainerWidth / 2,
    borderWidth: 1,
    borderColor: 'rgb(205,205,205)',
    marginRight: moderateScale(14, resizeFactor),
    overflow: 'hidden',
  },

  checkedIconContainerActive: {
    backgroundColor: 'rgb(80,227,194)',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0,
  },

  caretIcon: {
    width: checkedIconContainerWidth / 2,
    height: checkedIconContainerWidth / 2,
  },
})

export default styles
