/**
 *
 * WhiteFormButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View, Text, Image } from 'react-native'

import arrowIcon from 'assets/icons/arrow-left-thin.png'
import caretIcon from 'assets/icons/checked-white.png'

import styles from './styles'

export default class WhiteFormButton extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    arrow: PropTypes.bool,
    checked: PropTypes.bool,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
    arrowStyle: PropTypes.any,
    icon: PropTypes.any,
    iconStyle: PropTypes.any,
    titleStyle: PropTypes.any,
  }

  render() {
    const {
      title,
      onPress,
      style,
      arrowStyle,
      arrow,
      icon,
      iconStyle,
      titleStyle,
      checked = null,
    } = this.props

    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={onPress}
        activeOpacity={0.95}
      >
        {icon ? (
          <View style={[styles.iconWrapper, iconStyle]}>
            <Image source={icon} style={styles.icon} resizeMode="contain" />
          </View>
        ) : null}

        {checked !== null ? (
          checked === true ? (
            <View
              style={[
                styles.checkedIconContainer,
                styles.checkedIconContainerActive,
              ]}
            >
              <Image
                source={caretIcon}
                style={styles.caretIcon}
                resizeMode="contain"
              />
            </View>
          ) : checked === false ? (
            <View style={styles.checkedIconContainer} />
          ) : null
        ) : null}

        <Text style={[styles.title, titleStyle]}>{title}</Text>

        {arrow ? (
          <View style={[styles.arrowWrapper, arrowStyle]}>
            <Image
              source={arrowIcon}
              style={[styles.arrow, styles.rotate]}
              resizeMode="contain"
            />
          </View>
        ) : null}
      </TouchableOpacity>
    )
  }
}
