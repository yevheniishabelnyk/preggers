import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 2.5)

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: gutter,
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  block: {
    alignSelf: 'stretch',
    marginBottom: moderateScale(20, resizeFactor),
  },

  blockRow: {
    flexDirection: 'row',
  },

  blockTitle: {
    alignSelf: 'stretch',
    fontFamily: 'Now-Bold',
    fontSize: moderateScale(26, resizeFactor),
    color: 'rgb(41,53,69)',
    marginBottom: moderateScale(14, resizeFactor),
  },

  formField: {
    marginBottom: moderateScale(12, resizeFactor),
  },

  unitsButton: {
    flex: 1,
  },

  leftUnitsButton: {
    marginRight: moderateScale(8, resizeFactor),
  },

  rightUnitsButton: {
    marginLeft: moderateScale(8, resizeFactor),
  },

  firstRow: {
    marginBottom: moderateScale(20, resizeFactor),
  },

  lastBlock: {
    marginBottom: moderateScale(100, resizeFactor),
  },
})

export default styles
