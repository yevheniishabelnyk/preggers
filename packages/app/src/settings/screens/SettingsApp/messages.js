import { defineMessages } from 'react-intl'

export default defineMessages({
  headerTitle: 'App settings',
  notificationsBlockTitle: 'Notifications',
  notifyAboutOffersSwitchTitle: 'Notices Offers/Deals',
  notifyWithinFirstTrimesterSwitchTitle: 'Notices week 0-13',
  notifyWithinThirdTrimesterSwitchTitle: 'Notices week 14-40',
  developmentChildSwitchTitle: 'Development child',
  languageSelectTitle: 'Language',
  languageBlockTitle: 'Language settings',
  unitsBlockTitle: 'Units',
  mailingBlockTitle: 'E-mail',
  mailingToggleTitle: 'Weekly e-mails',
})
