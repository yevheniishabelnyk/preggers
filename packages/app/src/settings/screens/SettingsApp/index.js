/**
 *
 * SettingsApp
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View } from 'react-native'
import BaseScreen from '@/app/base/components/BaseScreen'
import ViewWrapper from '@/shared/components/ViewWrapper'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import FormSwitch from '@/settings/components/molecules/FormSwitch'
import FormSelect from '@/settings/components/molecules/FormSelect'
import RedButton from '@/settings/components/atoms/RedButton'

import * as SettingsActions from '@/app/redux/settings/actions'

import url from 'url'
import config from 'config'
import languages from 'languages'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export const engLocaleToggleOptions = config.APP_LOCALES.map(locale => ({
  label: languages.getLanguageInfo(locale).name,
  value: locale,
}))

@connect(state => ({
  settings: state.settings,
}))
export default class SettingsApp extends BaseScreen {
  static propTypes = {
    dispatch: PropTypes.func,
    app: PropTypes.object,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this.state = {
      localeToggleOptions: engLocaleToggleOptions,
    }
  }

  render() {
    const { settings } = this.props
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._backButtonPressed}
              style={styles.header}
            />
          ),
        }}
        headerHeight={SettingsHeader.height}
      >
        <View style={styles.block}>
          <FormattedMessage
            {...messages.mailingBlockTitle}
            style={styles.blockTitle}
          />

          <FormSwitch
            title={formatMessage(messages.mailingToggleTitle)}
            onChange={this._toggleMailing}
            value={settings.isWeeklyEmailsEnabled}
            style={styles.formField}
          />
        </View>

        {/* <View style={styles.block}>
          <FormattedMessage
            {...messages.notificationsBlockTitle}
            style={styles.blockTitle}
          />

          <FormSwitch
            title={formatMessage(messages.notifyAboutOffersSwitchTitle)}
            onChange={this._switchNotifyOffers}
            value={settings.notifyOffers}
            style={styles.formField}
          />

          <FormSwitch
            title={formatMessage(
              messages.notifyWithinFirstTrimesterSwitchTitle
            )}
            onChange={this._switchNotifyFirstTrimester}
            value={settings.notifyFirstTrimester}
            style={styles.formField}
          />

          <FormSwitch
            title={formatMessage(
              messages.notifyWithinThirdTrimesterSwitchTitle
            )}
            onChange={this._switchNotifySecondThirdTrimester}
            value={settings.notifySecondThirdTrimester}
            style={styles.formField}
          />

          <FormSwitch
            title={formatMessage(messages.developmentChildSwitchTitle)}
            onChange={this._switchChildDevelopment}
            value={settings.childDevelopmentIsActive}
            style={styles.formField}
          />
        </View> */}

        <View style={styles.block}>
          <FormattedMessage
            {...messages.unitsBlockTitle}
            style={styles.blockTitle}
          />

          <View style={[styles.blockRow, styles.firstRow]}>
            <RedButton
              title="CM"
              active={settings.lengthUnits === 'cm'}
              onPress={this._useCentimeters}
              style={[styles.unitsButton, styles.leftUnitsButton]}
            />

            <RedButton
              title="IN"
              active={settings.lengthUnits === 'in'}
              onPress={this._useInches}
              style={[styles.unitsButton, styles.rightUnitsButton]}
            />
          </View>

          <View style={styles.blockRow}>
            <RedButton
              title="KG"
              active={settings.weightUnits === 'g'}
              onPress={this._useGrams}
              style={[styles.unitsButton, styles.leftUnitsButton]}
            />

            <RedButton
              title="LB"
              active={settings.weightUnits === 'lb'}
              onPress={this._usePounds}
              style={[styles.unitsButton, styles.rightUnitsButton]}
            />
          </View>
        </View>

        <View style={[styles.block, styles.lastBlock]}>
          <FormattedMessage
            {...messages.languageBlockTitle}
            style={styles.blockTitle}
          />

          <FormSelect
            title={formatMessage(messages.languageSelectTitle)}
            value={settings.locale}
            items={this.state.localeToggleOptions}
            onSelect={this._changeLocale}
            style={styles.localeSelect}
          />
        </View>
      </ViewWrapper>
    )
  }

  componentDidMount() {
    const { settings } = this.props

    if (settings.locale !== 'en') {
      let engLanugagesNames = engLocaleToggleOptions.map(item => [item.label])

      const apiUrl = 'https://translation.googleapis.com/language/translate/v2'

      const requestUrl =
        apiUrl +
        url.format({
          query: {
            key: config.GOOGLE__API_KEY,
            target: settings.locale,
            q: engLanugagesNames.join(),
          },
        })

      return fetch(requestUrl)
        .then(response => response.json())
        .then(result => {
          if (result.data) {
            const translatedText = result.data.translations[0].translatedText

            const translatedLanguagesNames = translatedText.split(',')

            const options = config.APP_LOCALES.map((locale, index) => ({
              label: translatedLanguagesNames[index].trim(),
              value: locale,
            }))

            this.setState({ localeToggleOptions: options })
          }
        })
    }
  }

  _useCentimeters = () =>
    this._(SettingsActions.updateLocal({ lengthUnits: 'cm' }))

  _useInches = () => this._(SettingsActions.updateLocal({ lengthUnits: 'in' }))

  _useGrams = () => this._(SettingsActions.updateLocal({ weightUnits: 'g' }))

  _usePounds = () => this._(SettingsActions.updateLocal({ weightUnits: 'lb' }))

  _switchNotifyOffers = () => {
    const { settings } = this.props

    this._(
      SettingsActions.updateLocal({
        notifyOffers: !settings.notifyOffers,
      })
    )
  }

  _switchNotifyFirstTrimester = () => {
    const { settings } = this.props

    this._(
      SettingsActions.updateLocal({
        notifyFirstTrimester: !settings.notifyFirstTrimester,
      })
    )
  }

  _switchNotifySecondThirdTrimester = () => {
    const { settings } = this.props

    this._(
      SettingsActions.updateLocal({
        notifySecondThirdTrimester: !settings.notifySecondThirdTrimester,
      })
    )
  }

  _switchChildDevelopment = () => {
    const { settings } = this.props

    this._(
      SettingsActions.updateLocal({
        childDevelopmentIsActive: !settings.childDevelopmentIsActive,
      })
    )
  }

  _toggleMailing = () => {
    const { settings } = this.props

    this._(
      SettingsActions.updateLocal({
        isWeeklyEmailsEnabled: !settings.isWeeklyEmailsEnabled,
      })
    )
  }

  _changeLocale = locale => {
    this._(SettingsActions.updateLocal({ locale }))
  }

  _backButtonPressed = () => {
    this._goBack()

    this._(SettingsActions.syncLocalRemote())
  }
}
