/**
 *
 * SettingsAccount
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Text, TouchableOpacity, Image } from 'react-native'

import defaultUser from 'assets/icons/user.png'

import * as UserTypes from '@/app/constants/UserTypes'

import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import FormInput from '@/settings/components/molecules/FormInput'
import FormSelect from '@/settings/components/molecules/FormSelect'
import LogoutButton from '@/settings/components/molecules/LogoutButton'
import SettingsHeader from '@/settings/components/molecules/SettingsHeader'
import ActionButton from '@/settings/components/atoms/ActionButton'
import ConnectedUser from '@/connect/components/molecules/ConnectedUser'
import ActionPopup from '@/shared/components/ActionPopup'
import FormError from '@/forms/components/atoms/FormError'
import Prompt from '@/shared/components/Prompt'
import PopupDialog, { FadeAnimation } from 'react-native-popup-dialog'

import moment from 'moment'

import countries from 'i18n-iso-countries'

countries.registerLocale(require('i18n-iso-countries/langs/en.json'))
countries.registerLocale(require('i18n-iso-countries/langs/sv.json'))

import { keys, isFunction } from 'lodash'

import { ConnectApi } from '@/app/api/Preggers'

import { firebaseConnect } from 'react-redux-firebase'
import { getConnection, getInvitation } from '@/app/redux/selectors'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import * as ProfileActions from '@/app/redux/profile/actions'

import cropImage from '@/shared/utils/cropImage'

import styles from './styles'

import { ExpoImage } from '@/vendor/cache-manager'

@firebaseConnect(props => {
  const listeners = []

  const { profile } = props.screenProps

  if (profile.connection) {
    listeners.push(`connections/${profile.connection}`)
  } else if (profile.invitation) {
    listeners.push(`invitations/${profile.invitation}`)
  }

  return listeners
})
@connect((state, props) => {
  const { profile } = props.screenProps

  let connection, invitation

  if (!profile.isEmpty) {
    if (profile.connection) {
      connection = getConnection(state, profile.connection)
    } else {
      connection = null
    }

    if (profile.invitation) {
      invitation = getInvitation(state, profile.invitation)
    } else {
      invitation = null
    }
  }

  return {
    connection,
    invitation,
    isFetchingConnection: connection === undefined,
    isFetchingInvitation: invitation === undefined,
    locale: state.settings.locale,
    signInProvider: state.session && state.session.signInProvider,
  }
})
export default class SettingsAccount extends BaseScreen {
  static propTypes = {
    locale: PropTypes.string,
    connection: PropTypes.object,
    signInProvider: PropTypes.string,
    activePregnancyId: PropTypes.string,
    isFetchingConnection: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props, context) {
    super(props)

    const countryNames = countries.getNames(props.locale)

    this.selectCountryItems = keys(countryNames).map(code => ({
      label: countryNames[code],
      value: code,
    }))

    this.state = this._getInitialState(
      props.screenProps.profile,
      context.intl.formatMessage
    )
  }

  render() {
    const { profile } = this.props.screenProps
    const { isFetchingConnection, isFetchingInvitation } = this.props
    const { formatMessage } = this.context.intl

    return (
      <ViewWrapper
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        scrollable
        keyboard
        behavior="padding"
        deviceIndicators="rgb(249,250,252)"
        components={{
          header: (
            <SettingsHeader
              title={formatMessage(messages.headerTitle)}
              onBackButtonPress={this._goBack}
              onDoneButtonPress={this._doneButtonPressed}
              style={styles.header}
            />
          ),

          popups: [
            <ActionPopup
              key="disconnectDialog"
              ref={ref => (this._disconnectDialog = ref)}
              image={require('assets/img/disconnect.png')}
              largeTitle={formatMessage(messages.disconnectTitle)}
              actionButton={{
                title: formatMessage(messages.actionButtonTitle),
                onPress: this._dismissDisconnectDialog,
              }}
              okButton={{
                title: formatMessage(messages.okButtonTitle),
                onPress: this._cancelConnection,
              }}
            />,

            <PopupDialog
              key="connectionErrorDialog"
              ref={ref => (this._connectionErrorDialog = ref)}
              dialogAnimation={new FadeAnimation({ animationDuration: 200 })}
              dialogStyle={styles.dialog}
              overlayOpacity={0.7}
            >
              <Prompt
                text={formatMessage(messages.connectionErrorTitle)}
                negativeButton={{
                  title: formatMessage(messages.noButtonTitle),
                  onPress: this._endRequest,
                }}
                positiveButton={{
                  title: formatMessage(messages.yesButtonTitle),
                  onPress: this._repeatRequest,
                }}
              />
            </PopupDialog>,
          ],
        }}
        headerHeight={SettingsHeader.height}
      >
        <TouchableOpacity
          style={styles.changePhotoWrapper}
          onPress={this._changePhotoButtonPressed}
          activeOpacity={0.95}
        >
          <View style={styles.changePhoto}>
            {this.state.image ? (
              <ExpoImage style={styles.photo} source={this.state.image} />
            ) : (
              <Image style={styles.photo} source={defaultUser} />
            )}

            <FormattedMessage
              {...messages.changePhotoButtonTitle}
              style={styles.changePhotoTitle}
            />
          </View>
        </TouchableOpacity>

        {!isFetchingConnection && !isFetchingInvitation ? (
          this._maybeRenderConnection()
        ) : (
          <View style={styles.connectionPlaceholder} />
        )}

        <FormInput
          title={formatMessage(messages.nameInputTitle)}
          value={this.state.name}
          onChange={this._setUserName}
          style={styles.textInput}
          error={this.state.error}
        />

        <FormError
          isVisible={this.state.error}
          title={formatMessage(messages.errorTitle)}
        />

        <FormSelect
          title={formatMessage(messages.typeSelectTitle)}
          value={this.state.type}
          items={this.state.userTypes}
          selectableItems={this.state.userTypes.filter(
            item =>
              item.value !== UserTypes.GRANDPARENT &&
              item.value !== UserTypes.UNCLE &&
              item.value !== UserTypes.ANT
          )}
          onSelect={this._setUserType}
          style={styles.textInput}
        />

        <FormSelect
          title={formatMessage(messages.locationSelectTitle)}
          value={this.state.countryCode}
          items={this.selectCountryItems}
          onSelect={this._setCountryCode}
          style={styles.textInput}
        />

        <LogoutButton
          onPress={this._logoutButtonPressed}
          style={styles.logoutButton}
        />

        <FormattedMessage
          {...messages.accountDetailText}
          style={styles.infoText}
          values={{
            updateAt: moment(profile.updatedAt).format('YYYY-MM-DD  HH:mm'),
          }}
        />

        <Text style={[styles.infoText, styles.lastText]}>
          {this.signInProvider}: {profile.email}
        </Text>
      </ViewWrapper>
    )
  }

  _setUserName = name => this.setState({ name, error: null })

  _setUserType = type => this.setState({ type })

  _setCountryCode = countryCode => this.setState({ countryCode })

  _maybeRenderConnection = () => {
    const { connection, invitation } = this.props
    const { profile } = this.props.screenProps
    const { formatMessage } = this.context.intl

    if (connection || invitation) {
      const data = connection || invitation
      const fromMe = data.fromId === profile.id

      return (
        <View
          style={[
            styles.connection,
            !fromMe ? styles.marginBottom : styles.marginBottomFromMe,
          ]}
        >
          <ConnectedUser
            status={data.status}
            name={fromMe ? data.toName : data.fromName}
            email={fromMe ? data.toEmail : data.fromEmail}
            photo={fromMe ? data.toPhoto : data.fromPhoto}
            onRemove={this._showDisconnectDialog}
          />

          {fromMe ? (
            <TouchableOpacity
              style={styles.shareSettingsLink}
              onPress={this._changeShareSettings}
              activeOpacity={0.95}
            >
              <FormattedMessage
                {...messages.shareSettingsLinkTitle}
                style={styles.shareSettingsLinkTitle}
              />
            </TouchableOpacity>
          ) : null}
        </View>
      )
    }

    if (
      profile.type === UserTypes.MOTHER ||
      profile.type === UserTypes.SINGLE_MOTHER
    ) {
      return (
        <View style={styles.connection}>
          <ActionButton
            title={formatMessage(messages.connectWithPartnerButtonTitle)}
            onPress={this._connectButtonPressed}
            style={styles.connectButton}
          />
        </View>
      )
    }
    return (
      <View style={styles.connection}>
        <ActionButton
          title={formatMessage(messages.connectWithInvitationCodeButtonTitle)}
          onPress={this._enterInvitationCode}
          style={styles.connectButton}
        />
      </View>
    )
  }

  componentWillMount() {
    const { signInProvider } = this.props

    this.signInProvider = this._getSignInProviderName(signInProvider)
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.screenProps.profile.isEmpty &&
      !nextProps.screenProps.profile.isEmpty
    ) {
      setTimeout(() => {
        const initalState = this._getInitialState(
          nextProps.screenProps.profile,
          this.context.intl.formatMessage
        )

        this.setState(initalState)
      }, 0)
    }

    if (
      (!this.props.screenProps.profile.isEmpty &&
        this.props.screenProps.profile.connection !==
          nextProps.screenProps.profile.connection) ||
      this.props.screenProps.profile.invitation !==
        nextProps.screenProps.profile.invitation
    ) {
      this.setState({
        userTypes: this._getUserTypes(
          nextProps.screenProps.profile,
          this.context.intl.formatMessage
        ),
      })
    }

    if (
      this.props.screenProps.profile.image !==
      nextProps.screenProps.profile.image
    ) {
      this.setState({ image: nextProps.screenProps.profile.image })
    }
  }

  _getSignInProviderName = providerId => {
    const { formatMessage } = this.context.intl

    switch (providerId) {
      case 'facebook.com':
        return 'Facebook'
      case 'password':
        return formatMessage(messages.emailAndPasswordAuthProviderLabel)
      default:
        return formatMessage(messages.emailAndPasswordAuthProviderLabel)
    }
  }

  _enterInvitationCode = () => this._goTo('/connect-verify-connection')

  _changeShareSettings = () => {
    const { connection, invitation } = this.props

    if (connection) {
      this._goTo('/connect-share-settings', {
        shareContractionTimer: connection.shareContractionTimer || false,
        shareTimeline: connection.shareTimeline || false,
        shareChecklist: connection.shareChecklist || false,
        shareBabyNames: connection.shareBabyNames || false,
        connectionId: connection.id,
      })
    } else if (invitation) {
      this._goTo('/connect-share-settings', {
        shareContractionTimer: invitation.shareContractionTimer || false,
        shareTimeline: invitation.shareTimeline || false,
        shareChecklist: invitation.shareChecklist || false,
        shareBabyNames: invitation.shareBabyNames || false,
        invitationId: invitation.id,
      })
    }
  }

  _dismissDisconnectDialog = () => {
    this._disconnectDialog._hide()
  }

  _connectButtonPressed = () => this._goTo('/connect-send-request')

  _cancelConnection = async () => {
    const { connection, invitation } = this.props

    try {
      if (connection) {
        await ConnectApi.cancelConnection(connection.id)
      } else if (invitation) {
        await ConnectApi.cancelInvitation(invitation.id)
      }
    } catch (err) {
      console.info('err: ', err)
    }
    this._disconnectDialog._hide()
  }

  _showDisconnectDialog = () => this._disconnectDialog._show()

  _doneButtonPressed = async () => {
    const {
      screenProps: { profile },
      firebase,
    } = this.props
    const { image, isImageChanged } = this.state

    let userData

    if (this.state.name) {
      if (this.state.name !== profile.name) {
        userData = Object.assign({}, userData, { name: this.state.name })
      }

      if (this.state.type !== profile.type) {
        userData = Object.assign({}, userData, {
          type: this.state.type,
        })
      }

      if (
        (profile.location &&
          this.state.countryCode !== profile.location.countryCode) ||
        (!profile.location && this.state.countryCode)
      ) {
        userData = Object.assign({}, userData, {
          location: { countryCode: this.state.countryCode },
        })
      }

      if (isImageChanged) {
        userData = Object.assign({}, userData, {
          image: { type: 'file', uri: image.uri },
        })
      }

      if (userData) {
        const handleRequest = async () => {
          if (isImageChanged) {
            this._(ProfileActions.updateProfilePicture(image)).catch(err => {
              console.info('err: ', err)

              try {
                this._(ProfileActions.updateProfilePicture(image))
              } catch (err) {
                console.log('err', err)
              }
            })
          }

          await firebase.updateProfile(userData)

          if (userData.name) {
            this._(
              ProfileActions.updateFirebaseProfileDisplayName(userData.name)
            )
          }

          this._goBack()
        }

        try {
          await handleRequest()
        } catch (err) {
          console.info('err: ', err)

          this._handleRequestError(handleRequest)
        }
      } else {
        this._goBack()
      }
    } else {
      this.setState({ error: true })
    }
  }

  _repeatRequest = async () => {
    this._connectionErrorDialog.dismiss()

    if (isFunction(this._failedRequestCall)) {
      try {
        await this._failedRequestCall()
      } catch (err) {
        console.log(err)

        this._handleRequestError(this._failedRequestCall)
      }
    }
  }

  _handleRequestError = handleRequest => {
    this._failedRequestCall = handleRequest

    this._connectionErrorDialog.show()
  }

  _endRequest = () => {
    this._failedRequestCall = null

    this._connectionErrorDialog.dismiss()
  }

  _logoutButtonPressed = () => this.props.firebase.logout()

  _changePhotoButtonPressed = () =>
    this._goTo('/take-photo', { onDone: this._handleImagePicked })

  _handleImagePicked = async data => {
    if (data && data.image) {
      this.setState({
        image: { type: 'file', ...data.image },
        isImageChanged: true,
      })

      try {
        console.info('data.image: ', data.image)

        const cropResult = await cropImage(data.image, {
          type: 'square',
          width: 200,
        })

        console.info('cropResult: ', cropResult)

        this.setState({
          image: cropResult,
        })
      } catch (err) {
        console.log(err)
      }
    }
  }

  _getInitialState = (profile, formatMessage) => {
    return {
      image: profile.image,
      name: profile.name || '',
      type: profile.type || '',
      countryCode: (profile.location && profile.location.countryCode) || '',
      userTypes: this._getUserTypes(profile, formatMessage),
    }
  }

  _getUserTypes = (profile, formatMessage) => {
    if (profile.connection || profile.invitation) {
      if (
        profile.type === UserTypes.MOTHER ||
        profile.type === UserTypes.SINGLE_MOTHER
      ) {
        return [
          {
            label: formatMessage(messages.userTypeMother),
            value: UserTypes.MOTHER,
          },
          {
            label: formatMessage(messages.userTypeSingleMother),
            value: UserTypes.SINGLE_MOTHER,
          },
        ]
      }

      return [
        {
          label: formatMessage(messages.userTypeFather),
          value: UserTypes.FATHER,
        },
        {
          label: formatMessage(messages.userTypePartner),
          value: UserTypes.PARTNER,
        },
        {
          label: formatMessage(messages.userTypeGrandparent),
          value: UserTypes.GRANDPARENT,
        },
        {
          label: formatMessage(messages.userTypeUncle),
          value: UserTypes.UNCLE,
        },
        { label: formatMessage(messages.userTypeAnt), value: UserTypes.ANT },
        {
          label: formatMessage(messages.userTypeFamily),
          value: UserTypes.FAMILY,
        },
        {
          label: formatMessage(messages.userTypeFriend),
          value: UserTypes.FRIEND,
        },
      ]
    }

    return [
      {
        label: formatMessage(messages.userTypeMother),
        value: UserTypes.MOTHER,
      },
      {
        label: formatMessage(messages.userTypeSingleMother),
        value: UserTypes.SINGLE_MOTHER,
      },
      {
        label: formatMessage(messages.userTypeFather),
        value: UserTypes.FATHER,
      },
      {
        label: formatMessage(messages.userTypePartner),
        value: UserTypes.PARTNER,
      },
      {
        label: formatMessage(messages.userTypeGrandparent),
        value: UserTypes.GRANDPARENT,
      },
      { label: formatMessage(messages.userTypeUncle), value: UserTypes.UNCLE },
      { label: formatMessage(messages.userTypeAnt), value: UserTypes.ANT },
      {
        label: formatMessage(messages.userTypeFamily),
        value: UserTypes.FAMILY,
      },
      {
        label: formatMessage(messages.userTypeFriend),
        value: UserTypes.FRIEND,
      },
    ]
  }
}
