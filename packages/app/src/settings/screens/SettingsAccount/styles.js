import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const headerWidth = moderateScale(120, resizeFactor)
const headerHeight = moderateScale(167, resizeFactor)
const photoWidth = moderateScale(109, resizeFactor)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
  },

  contentContainer: {
    paddingHorizontal: moderateScale(22.5, 2.5),
    paddingBottom: moderateScale(50, resizeFactor),
    alignItems: 'center',
  },

  header: {
    paddingHorizontal: moderateScale(22.5, resizeFactor),
    backgroundColor: 'rgb(249,250,252)',
  },

  changePhotoWrapper: {
    width: headerWidth,
    height: headerHeight,
    marginTop: moderateScale(7, resizeFactor),
    alignItems: 'center',
  },

  changePhoto: {
    width: headerWidth,
    height: headerHeight,
    alignItems: 'center',
    justifyContent: 'center',
  },

  changePhotoTitle: {
    width,
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(20, resizeFactor),
    fontFamily: 'Now-Medium',
    textAlign: 'center',
    color: 'rgb(138,142,172)',
    marginBottom: moderateScale(20, resizeFactor),
    marginTop: 'auto',
  },

  photo: {
    width: photoWidth,
    height: photoWidth,
    borderRadius: photoWidth / 2,
    marginBottom: moderateScale(20, resizeFactor),
    overflow: 'hidden',
  },

  textInput: {
    marginBottom: moderateScale(11, resizeFactor),
  },

  logoutButton: {
    marginTop: moderateScale(10, resizeFactor),
    marginBottom: moderateScale(18, resizeFactor),
  },

  infoText: {
    backgroundColor: 'transparent',
    fontSize: moderateScale(10, resizeFactor),
    lineHeight: moderateScale(20, resizeFactor),
    fontFamily: 'Now-Medium',
    textAlign: 'left',
    color: 'rgb(112,52,153)',
    alignSelf: 'stretch',
  },

  enterInvitationCodeLinkTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    lineHeight: moderateScale(20, resizeFactor),
    color: 'rgb(138,142,172)',
    textAlign: 'center',
    backgroundColor: 'transparent',
    marginVertical: moderateScale(18, resizeFactor),
  },

  shareSettingsLink: {
    alignSelf: 'stretch',
    marginTop: moderateScale(8, resizeFactor),
    marginBottom: moderateScale(28, resizeFactor),
  },

  shareSettingsLinkTitle: {
    fontFamily: 'Now-Medium',
    fontSize: moderateScale(14, resizeFactor),
    color: 'rgb(59,124,255)',
    textDecorationLine: 'underline',
    textAlign: 'right',
    backgroundColor: 'transparent',
  },

  lastText: {
    marginBottom: moderateScale(28, resizeFactor),
  },

  connection: {
    alignSelf: 'stretch',
    marginBottom: moderateScale(25, resizeFactor),
  },

  marginBottom: {
    marginBottom: moderateScale(25, resizeFactor),
  },

  marginBottomFromMe: {
    marginBottom: 0,
  },

  dialog: {
    width: moderateScale(330, resizeFactor),
    height: 'auto',
    overflow: 'hidden',
  },

  connectionPlaceholder: {
    height: moderateScale(114, resizeFactor),
  },
})

export default styles
