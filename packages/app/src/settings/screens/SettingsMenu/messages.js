import { defineMessages } from 'react-intl'

export default defineMessages({
  accountSettings: 'My account',
  pregnancySettings: 'My Pregnancy & due dates',
  addPregnancySettings: 'Add pregnancy',
  childrenSettings: 'My children',
  appSettings: 'App settings',
  aboutUs: 'About us',
  terms: 'Terms of service & Privacy policy',
  FAQ: 'FAQ',
  contactUs: 'Contact us',
  suggestionBox: 'Suggestion Box',
  otherApps: 'Other useful apps & services',
})
