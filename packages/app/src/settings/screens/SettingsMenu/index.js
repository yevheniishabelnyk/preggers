/**
 *
 * SettingsMenu
 *
 */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { View, Image, Text } from 'react-native'
import ViewWrapper from '@/shared/components/ViewWrapper'
import BaseScreen from '@/app/base/components/BaseScreen'
import CloseButton from '@/settings/components/atoms/CloseButton'
import MenuItem from '@/settings/components/molecules/MenuItem'

import defaultUser from 'assets/icons/user.png'

import messages from './messages'

import styles from './styles'

@connect((state, props) => {
  const { profile } = props.screenProps

  if (!profile.isEmpty) {
    const isConnectedPartner =
      profile.partner &&
      profile.type !== 'mother' &&
      profile.type !== 'single_mother'

    return {
      isConnectedPartner,
    }
  }

  return {
    isConnectedPartner: true,
  }
})
export default class SettingsMenu extends BaseScreen {
  static propTypes = {
    isConnectedPartner: PropTypes.bool,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { profile } = this.props.screenProps
    const { isConnectedPartner } = this.props

    const { formatMessage } = this.context.intl

    const source = profile.image || defaultUser

    return (
      <ViewWrapper
        scrollable
        mainStyle={styles.container}
        contentContainerStyle={styles.contentContainer}
        deviceIndicators="white"
      >
        <CloseButton onPress={this._goBack} style={styles.closeButton} />

        <View style={styles.header}>
          <Image source={source} style={styles.photo} />

          <Text style={styles.name}>{profile.name}</Text>
        </View>

        <MenuItem
          title={formatMessage(messages.accountSettings)}
          onPress={this._accountSettingsPressed}
          arrow
        />

        {!isConnectedPartner ? (
          <MenuItem
            title={
              profile.pregnancy
                ? formatMessage(messages.pregnancySettings)
                : formatMessage(messages.addPregnancySettings)
            }
            onPress={this._goToPregnancySettings}
            arrow
          />
        ) : null}

        {!isConnectedPartner ? (
          <MenuItem
            title={formatMessage(messages.childrenSettings)}
            onPress={this._childrenPressed}
            arrow
          />
        ) : null}

        <MenuItem
          title={formatMessage(messages.appSettings)}
          onPress={this._appSettingsPressed}
          arrow
        />

        <MenuItem
          title={formatMessage(messages.aboutUs)}
          onPress={this._aboutUsPressed}
          arrow
        />

        <MenuItem
          title={formatMessage(messages.terms)}
          onPress={this._termsPressed}
          arrow
        />

        <MenuItem
          title={formatMessage(messages.FAQ)}
          onPress={this._faqPressed}
          arrow
        />

        <MenuItem
          title={formatMessage(messages.contactUs)}
          onPress={this._contactUsPressed}
          arrow
        />

        <MenuItem
          title={formatMessage(messages.suggestionBox)}
          onPress={this._suggestionBoxPressed}
          arrow
        />

        <MenuItem
          title={formatMessage(messages.otherApps)}
          onPress={this._otherAppsPressed}
          arrow
          last
        />
      </ViewWrapper>
    )
  }

  _goToPregnancySettings = () => {
    const { profile } = this.props.screenProps

    if (profile.pregnancy) {
      this._goTo('/forms-pregnancy', {
        flow: 'EDIT',
        id: profile.pregnancy,
      })
    } else {
      this._goTo('/forms-pregnancy', {
        flow: 'ADD',
      })
    }
  }

  _accountSettingsPressed = () => this._goTo('/settings-account')

  _childrenPressed = () => this._goTo('/children-list')

  _appSettingsPressed = () => this._goTo('/settings-app')

  _aboutUsPressed = () => this._goTo('/agreement-about-us')

  _termsPressed = () => this._goTo('/agreement-terms-privacy-policy')

  _faqPressed = () => this._goTo('/agreement-faq-search')

  _contactUsPressed = () => this._goTo('/contact-us')

  _suggestionBoxPressed = () => this._goTo('/suggestion-box')

  _otherAppsPressed = () => this._goTo('/other-apps-list')
}
