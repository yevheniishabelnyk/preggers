import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.5

const photoWidth = moderateScale(109, resizeFactor)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255,255,255)',
    position: 'relative',
  },

  contentContainer: {
    paddingBottom: moderateScale(90, resizeFactor),
  },

  closeButton: {
    position: 'absolute',
    top: moderateScale(10, resizeFactor),
    right: moderateScale(10, resizeFactor),
    zIndex: 3,
    marginLeft: 0,
  },

  photo: {
    width: photoWidth,
    height: photoWidth,
    borderRadius: photoWidth / 2,
    overflow: 'hidden',
    marginBottom: moderateScale(13, resizeFactor),
  },

  header: {
    width,
    height: moderateScale(220, resizeFactor),
    alignItems: 'center',
    justifyContent: 'center',
  },

  name: {
    width,
    fontSize: moderateScale(22, resizeFactor),
    fontFamily: 'Now-Medium',
    textAlign: 'center',
    color: 'rgb(23,24,28)',
  },
})

export default styles
