/*
 *
 * Skreen flow routes
 *
 */

import SettingsMenu from './screens/SettingsMenu'
import SettingsAccount from './screens/SettingsAccount'
import SettingsApp from './screens/SettingsApp'

export default {
  '/settings-menu': {
    screen: SettingsMenu,
  },

  '/settings-account': {
    screen: SettingsAccount,
  },

  '/settings-app': {
    screen: SettingsApp,
  },
}
