import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    position: 'relative',
  },

  formInput: {
    width: width * 0.9,
    height: 0.08 * height,
    borderColor: 'rgba(151, 151, 151, 0.56)',
    borderWidth: 1,
    borderRadius: 100,
    marginBottom: 0.04 * height,
  },

  formInputContainer: {
    width: width * 0.9,
    height: 0.08 * height,
    borderRadius: 0.15 * height,
    display: 'flex',
    justifyContent: 'center',
    position: 'relative',
  },

  icon: {
    height: 0.033 * height,
    width: 0.033 * height,
    marginLeft: (11 / 375) * width,
  },

  clock: {
    height: 0.038 * height,
    width: 0.038 * height,
  },

  cancelMask: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,.5)',
  },

  picker: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width,
    backgroundColor: 'white',
    paddingTop: 50,
  },

  selectButton: {
    flex: 1,
    position: 'absolute',
    backgroundColor: '#6341fb',
    bottom: 210,
    right: 15,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  selectButtonTitle: {
    backgroundColor: 'transparent',
    color: 'white',
  },

  cancelButton: {
    backgroundColor: 'rgba(255,255,255, 0.8)',
    flex: 1,
    position: 'absolute',
    bottom: 210,
    left: 15,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'rgb(131,146,167)',
  },

  cancelButtonTitle: {
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#8A8EAC',
  },

  container: {},

  formField: {
    flexDirection: 'row',
    paddingHorizontal: 0.05 * width,
    alignItems: 'center',
    height: 0.09 * height,
    borderRadius: 8,
    backgroundColor: 'white',
    overflow: 'hidden',
  },

  value: {
    fontSize: 0.02 * height,
    letterSpacing: 0.7,
    marginLeft: 'auto',
    fontFamily: 'Now-Medium',
    color: 'rgb(235,65,185)',
  },

  placeholderColor: {
    color: 'rgb(112,52,153)',
  },

  title: {
    fontSize: 0.02 * height,
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },

  tags: {
    position: 'absolute',
    top: height - 200,
    right: 0,
    left: 0,
    bottom: 0,
    width,
    height: 200,
    paddingHorizontal: 24,
    paddingVertical: 42,
    backgroundColor: 'rgba(255, 255, 255, 0.95)',
  },
})

export default styles
