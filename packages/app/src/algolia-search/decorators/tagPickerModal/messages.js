import { defineMessages } from 'react-intl'

export default defineMessages({
  doneButtonTitle: 'Done',
  cancelButtonTitle: 'Cancel',
})
