/**
 *
 * tag picker decorator
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Modal, TouchableHighlight } from 'react-native'
import RefinementList from '@/algolia-search/components/organisms/RefinementList'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

export default function tagPickerModal(WrappedComponent) {
  return class TagPicker extends React.Component {
    static propTypes = {
      style: PropTypes.any,
      contentContainerStyle: PropTypes.any,
      attribute: PropTypes.string,
      isSearchInited: PropTypes.bool,
      searchConfig: PropTypes.shape({
        appId: PropTypes.string,
        apiKey: PropTypes.string,
        indexName: PropTypes.string,
        onSearchStateChange: PropTypes.func,
        searchState: PropTypes.object,
      }),
    }

    static contextTypes = {
      intl: PropTypes.object.isRequired,
    }

    static defaultProps = {
      searchConfig: {},
    }

    state = {
      isPickerVisible: false,
    }

    render() {
      const {
        style,
        contentContainerStyle,
        searchConfig,
        attribute,
        isSearchInited,
      } = this.props
      const { isPickerVisible } = this.state

      return (
        <View style={[styles.container, contentContainerStyle]}>
          <WrappedComponent
            style={style}
            showPicker={this._show}
            attribute={attribute}
            searchConfig={!isSearchInited ? searchConfig : null}
            isSearchInited={isSearchInited}
          />

          <Modal animationType="fade" transparent visible={isPickerVisible}>
            <TouchableHighlight
              style={styles.cancelMask}
              onPress={this._cancel}
              underlayColor="rgba(255,255,255,.5)"
            >
              <View style={styles.modalContent}>
                <RefinementList
                  attribute={attribute}
                  searchConfig={searchConfig}
                  isSearchInited={false}
                  style={styles.tags}
                />

                <TouchableHighlight
                  style={styles.selectButton}
                  onPress={this._submit}
                  underlayColor="rgba(99, 65, 251, 0.5)"
                >
                  <FormattedMessage
                    {...messages.doneButtonTitle}
                    style={styles.selectButtonTitle}
                  />
                </TouchableHighlight>

                <TouchableHighlight
                  style={styles.cancelButton}
                  onPress={this._cancel}
                  underlayColor="rgba(255,255,255,.5)"
                >
                  <FormattedMessage
                    {...messages.cancelButtonTitle}
                    style={styles.cancelButtonTitle}
                  />
                </TouchableHighlight>
              </View>
            </TouchableHighlight>
          </Modal>
        </View>
      )
    }

    _hide = () => this.setState({ isPickerVisible: false })

    _show = () => this.setState({ isPickerVisible: true })

    _cancel = () => {
      const { previousDate } = this.state

      if (previousDate) {
        this.setState({ date: previousDate })
      }

      this._hide()
    }

    _submit = () => {
      // const { onSubmit, format } = this.props
      // const { date } = this.state

      // if (isFunction(onSubmit)) {
      //   this.value = moment(date).format(format)

      //   onSubmit(this.value, date)
      // }

      this._hide()
    }
  }
}
