/**
 *
 * Knowledge Items Search container
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import KnowledgeSearchHits from '@/algolia-search/components/organisms/KnowledgeSearchHits'
// import Tags from '@/algolia-search/components/organisms/Tags'
import SearchBox from '@/algolia-search/components/molecules/SearchBox'
import CloseButton from '@/algolia-search/components/atoms/CloseButton'

import { InstantSearch } from 'react-instantsearch/native'
import VirtualRefinementList from '@/algolia-search/utils/VirtualRefinementList'

import config from 'config'

import styles from './styles'

const envSuffix =
  config.ENVIRONMENT === 'production' ? '' : `__${config.ENVIRONMENT}`

export default class PreggersKnowledgeItemsIndex extends React.Component {
  static propTypes = {
    searchPlaceholder: PropTypes.string,
    locale: PropTypes.string.isRequired,
    refinementList: PropTypes.object,
    onClose: PropTypes.func,
    onItemPress: PropTypes.func,
    searchDefaultValue: PropTypes.string,
    showSearchField: PropTypes.bool,
  }

  static defaultProps = {
    placeholder: 'Search',
    locale: 'en',
  }

  constructor(props) {
    super(props)

    let searchState = {}

    if (props.refinementList) {
      searchState.refinementList = props.refinementList
    }

    this.state = {
      searchState,
      indexId: `preggers-knowledge-items--${props.locale}${envSuffix}`,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.locale !== nextProps.locale) {
      this.setState({
        indexId: `preggers-knowledge-items--${nextProps.locale}${envSuffix}`,
      })
    }
  }

  render() {
    const {
      onClose,
      onItemPress,
      searchPlaceholder,
      searchDefaultValue,
      refinementList,
      showSearchField,
    } = this.props

    return (
      <View style={styles.container}>
        <InstantSearch
          appId={config.ALGOLIA__APP_ID}
          apiKey={config.ALGOLIA__API_KEY}
          indexName={this.state.indexId}
          onSearchStateChange={this._onSearchStateChange}
          searchState={this.state.searchState}
        >
          {showSearchField ? (
            <View style={styles.header}>
              <SearchBox
                style={styles.searchBox}
                placeholder={searchPlaceholder}
                defaultRefinement={searchDefaultValue}
              />

              {onClose ? (
                <CloseButton style={styles.closeButton} onPress={onClose} />
              ) : null}
            </View>
          ) : null}

          {/* <Tags
            searchConfig={{
              appId: config.ALGOLIA__APP_ID,
              apiKey: config.ALGOLIA__API_KEY,
              indexName: this.state.indexId,
              onSearchStateChange: this._onSearchStateChange,
              searchState: this.state.searchState,
            }}
            isSearchInited
            attribute="_tags"
          /> */}

          {refinementList && refinementList.type ? (
            <VirtualRefinementList attribute="type" />
          ) : null}

          {refinementList && refinementList._tags ? (
            <VirtualRefinementList attribute="_tags" />
          ) : null}

          <KnowledgeSearchHits onItemPress={onItemPress} />
        </InstantSearch>
      </View>
    )
  }

  _onSearchStateChange = searchState => {
    console.info('searchState: ', searchState)

    this.setState({ searchState })
  }
}
