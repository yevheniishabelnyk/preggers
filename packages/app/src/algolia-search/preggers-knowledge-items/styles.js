import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const gutter = moderateScale(22.5, 3.1)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom:
      Platform.OS === 'android' ? moderateScale(-15, resizeFactor) : null,
  },

  header: {
    height: moderateScale(65, resizeFactor),
    marginTop: moderateScale(-8, 1.1),
    paddingBottom: moderateScale(5, 1),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: gutter,
    backgroundColor: 'rgb(249,250,252)',
  },

  searchBox: {
    width: '100%',
    height: moderateScale(43, resizeFactor),
    alignSelf: 'flex-end',
  },

  closeButton: {
    width: moderateScale(40, resizeFactor),
    height: moderateScale(40, resizeFactor),
    marginLeft: moderateScale(25, resizeFactor),
  },
})

export default styles
