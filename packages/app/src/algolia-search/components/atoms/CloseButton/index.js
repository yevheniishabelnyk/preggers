/**
 *
 * CloseButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, View } from 'react-native'

import styles from './styles'

export default class CloseButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { onPress, style } = this.props

    return (
      <TouchableOpacity onPress={onPress} style={[styles.button, style]}>
        <View style={styles.content}>
          <View style={styles.line} />
          <View style={[styles.line, styles.rotate]} />
        </View>
      </TouchableOpacity>
    )
  }
}
