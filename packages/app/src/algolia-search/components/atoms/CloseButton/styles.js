import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

const buttonWidth = moderateScale(40, resizeFactor)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth * 1.5,
    height: buttonWidth * 1.5,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: buttonWidth * -0.25,
  },

  content: {
    backgroundColor: 'white',
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  line: {
    width: buttonWidth / 2,
    height: moderateScale(1, resizeFactor),
    backgroundColor: 'rgba(29,29,27,0.9)',
    transform: [{ rotate: '45deg' }],
  },

  rotate: {
    transform: [{ rotate: '135deg' }],
    marginTop: moderateScale(-1, resizeFactor),
  },
})

export default styles
