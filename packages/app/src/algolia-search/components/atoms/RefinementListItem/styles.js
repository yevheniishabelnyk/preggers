import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  tag: {
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: (13.5 / 375) * width,
    paddingHorizontal: (13 / 375) * width,
    paddingVertical: (6 / 375) * width,
    color: 'rgb(35,40,46)',
    textAlign: 'left',
    fontSize: (11 / 375) * width,
    fontFamily: 'Now-Bold',
    marginRight: (13 / 375) * width,
  },

  last: {
    marginRight: 0,
  },

  redBorder: {
    borderColor: 'rgb(250,65,105)',
  },
})

export default styles
