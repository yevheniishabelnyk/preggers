/**
 *
 * RefinementListItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, TouchableOpacity } from 'react-native'

import { isFunction } from 'lodash'

import styles from './styles'

export default class RefinementListItem extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    last: PropTypes.bool,
    active: PropTypes.bool,
    value: PropTypes.string,
    onSelect: PropTypes.func,
  }

  render() {
    const { name, last, active } = this.props

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={this._itemPressed}>
        <Text
          style={[
            styles.tag,
            active ? styles.redBorder : null,
            last ? styles.last : null,
          ]}
        >
          {name}
        </Text>
      </TouchableOpacity>
    )
  }

  _itemPressed = () => {
    const { onSelect, value } = this.props

    if (isFunction(onSelect)) {
      onSelect(value)
    }
  }
}
