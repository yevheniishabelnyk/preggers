/**
 *
 * KnowledgeSearchHitItem
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image, TouchableOpacity } from 'react-native'
import { ExpoImage } from '@/vendor/cache-manager'

import { isFunction } from 'lodash'

import styles, { textFontSize } from './styles'

import { connectHighlight } from 'react-instantsearch/connectors'

const Highlight = connectHighlight(({ highlight, attribute, hit }) => {
  const parsedHit = highlight({
    attribute,
    hit,
    highlightProperty: '_highlightResult',
  })
  const highlightedHit = parsedHit.map((part, idx) => {
    if (part.isHighlighted) {
      return (
        <Text key={idx} style={styles.highlightedText}>
          {part.value}
        </Text>
      )
    }

    return part.value
  })
  return <Text>{highlightedHit}</Text>
})
export default class KnowledgeSearchHitItem extends React.Component {
  static propTypes = {
    item: PropTypes.object,
    onPress: PropTypes.func,
    style: PropTypes.any,
  }

  state = {
    titleScale: 1,
  }

  render() {
    const { item, style } = this.props

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={this._itemPressed}>
        <View style={[styles.container, style]}>
          {item.image ? (
            <View style={styles.imageWrapper}>
              <ExpoImage source={item.image} style={styles.image} findInCache />
            </View>
          ) : (
            <View style={styles.imageWrapper}>
              <Image
                style={styles.image}
                source={require('assets/img/article-default.png')}
              />
            </View>
          )}

          <View style={styles.contentWrapper}>
            {item.title ? (
              <View
                onLayout={this._onLayoutTitleWrapper}
                style={styles.titleWrapper}
              >
                <Text
                  style={[
                    styles.title,
                    {
                      fontSize: textFontSize * this.state.titleScale,
                      lineHeight: textFontSize * 1.3 * this.state.titleScale,
                    },
                  ]}
                  onLayout={this._onLayoutTitle}
                >
                  <Highlight attribute="title" hit={item} />
                </Text>

                {/*<Text>
                  <Highlight attribute="type" hit={item} />
                </Text>*/}
              </View>
            ) : null}

            <View style={styles.iconWrapper}>
              <Image
                source={require('assets/icons/article-icon.png')}
                style={styles.icon}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _onLayoutTitleWrapper = e => {
    this.titleWrapperHeight = e.nativeEvent.layout.height

    if (this.titleHeight) {
      this._fitText(this.titleHeight, this.titleWrapperHeight)
    }
  }

  _onLayoutTitle = e => {
    this.titleHeight = e.nativeEvent.layout.height

    if (this.titleWrapperHeight) {
      this._fitText(this.titleHeight, this.titleWrapperHeight)
    }
  }

  _fitText = (titleHeight, titleWrapperHeight) => {
    if (titleHeight > titleWrapperHeight) {
      this.setState(prevState => ({
        titleScale: prevState.titleScale * 0.94,
      }))
    }
  }

  _itemPressed = () => {
    const { onPress, item } = this.props

    if (isFunction(onPress)) {
      onPress(item)
    }
  }
}
