import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.3

export const contatinerWidth = moderateScale(337, resizeFactor)
export const buttonWidth = moderateScale(61, resizeFactor)
export const textFontSize = moderateScale(14, resizeFactor)

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '100%',
    borderRadius: moderateScale(8, resizeFactor),
    marginBottom: moderateScale(12, resizeFactor),
    shadowOpacity: 0.05,
    shadowRadius: 6,
    shadowColor: '#000000',
    shadowOffset: { height: 2, width: -2 },
  },

  image: {
    width: moderateScale(105, 0.3),
    height: moderateScale(105, 0.3),
  },

  contentWrapper: {
    flex: 1,
    height: moderateScale(105, 0.3),
    paddingTop: moderateScale(6, resizeFactor),
    paddingBottom: moderateScale(11, resizeFactor),
    marginLeft: moderateScale(17, resizeFactor),
  },

  titleWrapper: {
    flex: 1,
    paddingTop: moderateScale(6, 0.5),
  },

  title: {
    fontFamily: 'Now-Bold',
    lineHeight: moderateScale(19, 0.7),
    color: 'rgb(35, 40, 46)',
    width: moderateScale(200, 0.7),
  },

  iconWrapper: {
    marginTop: moderateScale(4, resizeFactor),
  },

  icon: {
    width: moderateScale(20, 0.6),
    height: moderateScale(20, 0.6),
  },

  highlightedText: {
    backgroundColor: '#ffff99',
  },
})

export default styles
