import { StyleSheet } from 'react-native'

import { moderateScale } from '@/app/scaling'

const resizeFactor = 0.4

const iconWidth = moderateScale(26, 0.3)

const gutter = moderateScale(15, resizeFactor)

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    borderRadius: moderateScale(100, resizeFactor),
    backgroundColor: 'rgb(240,240,247)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: gutter,
    overflow: 'hidden',
    position: 'relative',
  },

  icon: {
    width: iconWidth,
    height: iconWidth,
    marginRight: moderateScale(13, resizeFactor),
  },

  input: {
    paddingVertical: moderateScale(13, resizeFactor),
    flex: 1,
    alignSelf: 'center',
    fontSize: moderateScale(14, resizeFactor),
    letterSpacing: 0.7,
    fontFamily: 'Now-Medium',
    color: 'rgb(131,146,167)',
  },
})

export default styles
