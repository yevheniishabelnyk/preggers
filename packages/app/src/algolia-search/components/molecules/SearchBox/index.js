/**
 *
 * SearchBox
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { connectSearchBox } from 'react-instantsearch/connectors'

import { View, Image, TextInput } from 'react-native'

import styles from './styles'

@connectSearchBox
export default class SearchBox extends React.Component {
  static propTypes = {
    refine: PropTypes.func,
    currentRefinement: PropTypes.string,
    placeholder: PropTypes.string,
    style: PropTypes.any,
  }

  render() {
    const { currentRefinement, placeholder, style } = this.props

    return (
      <View style={[styles.container, style]}>
        <Image
          source={require('assets/icons/search.png')}
          style={styles.icon}
        />

        <TextInput
          underlineColorAndroid="transparent"
          style={styles.input}
          value={currentRefinement}
          onChangeText={this._textChanged}
          placeholderTextColor="#8392a7"
          placeholder={placeholder}
          clearButtonMode="always"
          spellCheck={false}
          autoCorrect={false}
          autoCapitalize="none"
          returnKeyType="go"
        />
      </View>
    )
  }

  _textChanged = text => this.props.refine(text)
}
