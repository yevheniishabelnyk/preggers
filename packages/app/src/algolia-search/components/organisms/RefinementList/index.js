/**
 *
 * RefinementList
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import RefinementListItem from '@/algolia-search/components/atoms/RefinementListItem'

import styles from './styles'

import { connectRefinementList } from 'react-instantsearch/connectors'
import { InstantSearch } from 'react-instantsearch/native'

const Refinements = connectRefinementList(({ refine, items, style }) => (
  <View style={[styles.container, style]}>
    {items.map((item, index) => (
      <RefinementListItem
        key={index}
        active={item.isRefined}
        name={item.label}
        last={items.length === index}
        value={item.value}
        onSelect={refine}
      />
    ))}
  </View>
))

const RefinementList = ({
  attribute,
  transformItems,
  isSearchInited,
  searchConfig,
  style,
}) => {
  if (!isSearchInited) {
    return (
      <InstantSearch {...searchConfig}>
        <Refinements
          attribute={attribute}
          transformItems={transformItems}
          style={style}
        />
      </InstantSearch>
    )
  }

  return <Refinements attribute={attribute} transformItems={transformItems} />
}

RefinementList.propTypes = {
  attribute: PropTypes.string,
  transformItems: PropTypes.func,
  isSearchInited: PropTypes.bool,
  style: PropTypes.any,
  searchConfig: PropTypes.shape({
    appId: PropTypes.string,
    apiKey: PropTypes.string,
    indexName: PropTypes.string,
    onSearchStateChange: PropTypes.func,
    searchState: PropTypes.object,
  }),
}

export default RefinementList
