import { StyleSheet, Platform } from 'react-native'

import { moderateScale } from '@/app/scaling'

const gutter = moderateScale(22.5, 3.25)

const styles = StyleSheet.create({
  container:
    Platform.OS === 'ios'
      ? {
          overflow: 'visible',
          zIndex: -1,
        }
      : null,

  contentContainer: {
    paddingHorizontal: gutter,
    paddingTop: moderateScale(22.5, 0.3),
  },
})

export default styles
