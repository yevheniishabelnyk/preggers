/**
 *
 * KnowledgeSearchHits
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { FlatList } from 'react-native'
import KnowledgeSearchHitItem from '@/algolia-search/components/molecules/KnowledgeSearchHitItem'

import { connectInfiniteHits } from 'react-instantsearch/connectors'

import { isFunction } from 'lodash'

import styles from './styles'

@connectInfiniteHits
export default class KnowledgeSearchHits extends React.Component {
  static propTypes = {
    hits: PropTypes.array,
    hasMore: PropTypes.bool,
    refine: PropTypes.func,
    onItemPress: PropTypes.func,
  }

  render() {
    const { hits } = this.props

    return (
      <FlatList
        data={hits}
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        onEndReached={this._onEndReached}
        keyExtractor={this._extractListKey}
        renderItem={this._renderItem}
      />
    )
  }

  _renderItem = ({ item }) => (
    <KnowledgeSearchHitItem item={item} onPress={this._itemPressed} />
  )

  _itemPressed = item => {
    const { onItemPress } = this.props

    if (isFunction(onItemPress)) {
      onItemPress(item)
    }
  }

  _onEndReached = () => {
    const { hasMore, refine } = this.props

    if (hasMore) {
      refine()
    }
  }

  _extractListKey(item) {
    return `flat-list-item-${item.objectID}`
  }
}
