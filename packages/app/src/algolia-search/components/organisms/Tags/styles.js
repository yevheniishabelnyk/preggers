import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const gutter = (22 / 375) * width

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: gutter,
  },

  addButton: {
    borderWidth: 1,
    borderColor: '#7E34A9',
    borderRadius: (13.5 / 375) * width,
    marginRight: (13 / 375) * width,
    overflow: 'hidden',
  },

  addButtonTitle: {
    color: '#7E34A9',
    textAlign: 'left',
    fontSize: (11 / 375) * width,
    fontFamily: 'Now-Bold',
    paddingHorizontal: (13 / 375) * width,
    paddingVertical: (6 / 375) * width,
  },
})

export default styles
