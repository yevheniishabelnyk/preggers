/**
 *
 * Tags modal
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import tagPickerModal from '@/algolia-search/decorators/tagPickerModal'
import RefinementList from '@/algolia-search/components/organisms/RefinementList'

import { FormattedMessage } from '@/vendor/react-intl-native'
import messages from './messages'

import styles from './styles'

@tagPickerModal
export default class Tags extends React.Component {
  static propTypes = {
    showPicker: PropTypes.func,
    isSearchInited: PropTypes.bool,
    attribute: PropTypes.string,
    searchConfig: PropTypes.shape({
      appId: PropTypes.string,
      apiKey: PropTypes.string,
      indexName: PropTypes.string,
      onSearchStateChange: PropTypes.func,
      searchState: PropTypes.object,
    }),
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    isSearchInited: true,
  }

  render() {
    const { searchConfig, showPicker, attribute, isSearchInited } = this.props

    return (
      <View style={styles.container}>
        <RefinementList
          attribute={attribute}
          transformItems={this._filterIsRefined}
          isSearchInited={isSearchInited}
          searchConfig={searchConfig}
        />

        <TouchableOpacity onPress={showPicker} style={styles.addButton}>
          <FormattedMessage
            {...messages.addButtonTitle}
            style={styles.addButtonTitle}
          />
        </TouchableOpacity>
      </View>
    )
  }

  _filterIsRefined(items) {
    return items.filter(item => item.isRefined)
  }
}
