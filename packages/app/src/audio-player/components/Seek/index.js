/**
 *
 * Seek
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import Slider from 'react-native-slider'

import { isFunction } from 'lodash'

import styles from './styles'

export default class Seek extends React.Component {
  static propTypes = {
    value: PropTypes.number,
    onChange: PropTypes.func,
    onComplete: PropTypes.func,
    disableParrentScroll: PropTypes.func,
    enableParrentScroll: PropTypes.func,
  }

  render() {
    const { disableParrentScroll, value } = this.props

    return (
      <Slider
        style={styles.slider}
        thumbStyle={styles.thumb}
        trackStyle={styles.track}
        thumbTouchSize={{
          width: 31,
          height: 31,
        }}
        onSlidingStart={disableParrentScroll}
        onSlidingComplete={this._completed}
        onValueChange={this._valueChanged}
        value={value}
        maximumTrackTintColor="#27b99a"
        thumbTintColor="#ffffff"
        minimumTrackTintColor="#f2fbf9"
      />
    )
  }

  _valueChanged = value => {
    const { onChange } = this.props

    if (isFunction(onChange)) {
      onChange(value)
    }
  }

  _completed = value => {
    const { onComplete, enableParrentScroll } = this.props

    if (isFunction(onComplete)) {
      onComplete(value)
    }

    if (isFunction(enableParrentScroll)) {
      enableParrentScroll()
    }
  }
}
