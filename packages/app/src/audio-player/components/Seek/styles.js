import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  slider: {
    flex: 1,
    marginHorizontal: 7,
  },

  track: {
    height: 6,
    borderRadius: 6,
  },

  thumb: {
    width: 16,
    height: 16,
  },
})

export default styles
