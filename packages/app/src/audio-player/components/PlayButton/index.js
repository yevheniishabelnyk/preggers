/**
 *
 * PlayButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import styles from './styles'

const PlayIcon = () => (
  <View style={styles.icon}>
    <View style={styles.triangle} />
  </View>
)

const PauseIcon = () => (
  <View style={styles.icon}>
    <View style={[styles.whiteLine, styles.marginRight]} />

    <View style={styles.whiteLine} />
  </View>
)

const PlayButton = ({ onPress, isPlaying }) => (
  <TouchableOpacity
    onPress={onPress}
    style={styles.button}
    activeOpacity={0.95}
  >
    {isPlaying ? <PauseIcon /> : <PlayIcon />}
  </TouchableOpacity>
)

PlayButton.propTypes = {
  onPress: PropTypes.func,
  isPlaying: PropTypes.bool,
}

PlayButton.defaultProps = {
  isPlaying: false,
}

export default PlayButton
