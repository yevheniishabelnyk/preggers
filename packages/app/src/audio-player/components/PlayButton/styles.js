import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  button: {
    height: 31,
    width: 50,
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon: {
    height: 31,
    width: 31,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  whiteLine: {
    height: 15,
    width: 4,
    borderRadius: 2.5,
    backgroundColor: 'white',
  },

  marginRight: {
    marginRight: 4,
  },

  triangle: {
    width: 0,
    height: 0,
    borderTopWidth: 8,
    borderRightWidth: 0,
    borderBottomWidth: 8,
    borderLeftWidth: 13,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'white',
    backgroundColor: 'transparent',
  },
})

export default styles
