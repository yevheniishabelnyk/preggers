import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Roboto-Regular',
    fontSize: 11,
    color: 'white',
  },
})

export default styles
