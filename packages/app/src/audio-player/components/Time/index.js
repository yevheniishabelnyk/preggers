/**
 *
 * Time
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'

import styles from './styles'

const Time = ({ value }) => <Text style={styles.text}>{value}</Text>

Time.propTypes = {
  value: PropTypes.string,
}

Time.defaultProps = {
  value: '',
}

export default Time
