import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    borderRadius: 100,
    overflow: 'hidden',
    height: 31,
    alignSelf: 'stretch',
    maxWidth: 300,
    alignItems: 'center',
  },

  player: {
    flexDirection: 'row',
    backgroundColor: 'rgb(64,224,190)',
    height: 31,
    alignSelf: 'stretch',
    maxWidth: 300,
    alignItems: 'center',
    paddingLeft: 15,
  },

  overlay: {
    backgroundColor: 'rgb(64,224,190)',
    borderRadius: 100,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    height: 31,
    zIndex: 3,
    flexDirection: 'row',
    paddingRight: 27,
    paddingLeft: 15,
  },

  overlayText: {
    fontFamily: 'Now-Medium',
    fontSize: (14 / 375) * width,
    color: 'white',
    textAlign: 'center',
  },

  overlayTriangle: {
    width: 0,
    height: 0,
    borderTopWidth: (7 / 375) * width,
    borderRightWidth: 0,
    borderBottomWidth: (7 / 375) * width,
    borderLeftWidth: (12 / 375) * width,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'white',
    backgroundColor: 'transparent',
    marginRight: (8 / 375) * width,
  },
})

export default styles
