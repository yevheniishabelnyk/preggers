/**
 *
 * AudioPlayer
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity, Text, Animated } from 'react-native'
import { Audio } from 'expo'

import Time from '../../components/Time'
import Seek from '../../components/Seek'
import PlayButton from '../../components/PlayButton'

import styles from './styles'

export default class AudioPlayer extends React.Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    overlayText: PropTypes.string,
    disableParrentScroll: PropTypes.func,
    enableParrentScroll: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  constructor(props) {
    super(props)

    this.index = 0
    this.isSeeking = false
    this.playbackInstance = null

    this.state = {
      overlayTextIsVisible: Boolean(props.overlayText),
      playbackInstancePosition: null,
      playbackInstanceDuration: null,
      shouldPlay: false,
      isPlaying: false,
      isBuffering: false,
      isLoading: true,
      volume: 1.0,
      rate: 1.0,
      duration: '00:00',
    }
  }

  render() {
    const {
      disableParrentScroll,
      enableParrentScroll,
      style,
      overlayText,
    } = this.props

    const { isPlaying, overlayTextIsVisible } = this.state

    return (
      <View style={[styles.container, style]}>
        {overlayText && overlayTextIsVisible ? (
          <Animated.View
            style={[styles.overlayContainer, { width: this.overlayWidth }]}
          >
            <TouchableOpacity
              style={styles.overlay}
              onPress={this._overlayPressed}
              onLayout={this._onOverlayLayout}
              activeOpacity={0.95}
            >
              <View style={styles.overlayTriangle} />

              <Text style={styles.overlayText}>{overlayText}</Text>
            </TouchableOpacity>
          </Animated.View>
        ) : null}

        <View style={styles.player} onLayout={this._onPlayerLayout}>
          <Time value={this._getCurrentTime()} />

          <Seek
            value={this._getSeekSliderPosition()}
            onChange={this._onSeekChange}
            onComplete={this._onSeekComplete}
            disableParrentScroll={disableParrentScroll}
            enableParrentScroll={enableParrentScroll}
          />

          <Time value={this.state.duration} />

          <PlayButton onPress={this._togglePlay} isPlaying={isPlaying} />
        </View>
      </View>
    )
  }

  componentDidMount() {
    Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
    })

    this._loadNewPlaybackInstance(false)
  }

  _loadNewPlaybackInstance = async shouldPlay => {
    if (this.playbackInstance !== null) {
      await this.playbackInstance.unloadAsync()
      this.playbackInstance.setOnPlaybackStatusUpdate(null)
      this.playbackInstance = null
    }

    const source = { uri: this.props.url }

    const initialStatus = {
      shouldPlay,
      rate: this.state.rate,
      volume: this.state.volume,
    }

    const { sound, status } = await Audio.Sound.create(
      source,
      initialStatus,
      this._onPlaybackStatusUpdate
    )

    console.info('status: ', status)

    this.playbackInstance = sound

    this.setState({
      isLoading: false,
    })
  }

  _onPlaybackStatusUpdate = status => {
    if (status.isLoaded) {
      const duration = this._getMMSSFromMillis(status.durationMillis)

      this.setState({
        playbackInstancePosition: status.positionMillis,
        playbackInstanceDuration: status.durationMillis,
        shouldPlay: status.shouldPlay,
        isPlaying: status.isPlaying,
        isBuffering: status.isBuffering,
        rate: status.rate,
        volume: status.volume,
        duration,
      })
    } else if (status.error) {
      console.log(`FATAL PLAYER ERROR: ${status.error}`)
    }
  }

  _togglePlay = () => {
    if (this.playbackInstance !== null) {
      if (this.state.isPlaying) {
        this.setState({ isPlaying: false })
        this.playbackInstance.pauseAsync()
      } else {
        this.setState({ isPlaying: true })
        this.playbackInstance.playAsync()
      }
    }
  }

  _onSeekChange = async value => {
    if (this.playbackInstance !== null && !this.isSeeking) {
      this.isSeeking = true
    }

    this.setState({ seek: value })
  }

  _onSeekComplete = async value => {
    if (this.playbackInstance !== null) {
      const seekPosition = value * this.state.playbackInstanceDuration

      if (this.state.isPlaying) {
        await this.playbackInstance.playFromPositionAsync(seekPosition)
      } else {
        await this.playbackInstance.setPositionAsync(seekPosition)
      }

      this.isSeeking = false
    }
  }

  _getSeekSliderPosition() {
    if (this.isSeeking) {
      return this.state.seek
    }

    const { playbackInstancePosition, playbackInstanceDuration } = this.state

    if (
      this.playbackInstance !== null &&
      playbackInstancePosition !== null &&
      playbackInstanceDuration !== null
    ) {
      return playbackInstancePosition / playbackInstanceDuration
    }

    return 0
  }

  _getCurrentTime() {
    const { playbackInstancePosition, playbackInstanceDuration } = this.state

    if (
      this.playbackInstance !== null &&
      playbackInstancePosition !== null &&
      playbackInstanceDuration !== null
    ) {
      return this._getMMSSFromMillis(playbackInstancePosition)
    }

    return ''
  }

  _getMMSSFromMillis(millis) {
    const totalSeconds = millis / 1000
    const seconds = Math.floor(totalSeconds % 60)
    const minutes = Math.floor(totalSeconds / 60)

    return padWithZero(minutes) + ':' + padWithZero(seconds)

    function padWithZero(number) {
      const string = number.toString()

      if (number < 10) {
        return '0' + string
      }

      return string
    }
  }

  _overlayPressed = () => {
    if (this.playbackInstance !== null) {
      this.setState({ isPlaying: true })
      this.playbackInstance.playAsync()
    }

    setTimeout(() => {
      Animated.timing(this.overlayWidth, {
        toValue: this.state.playerWidth,
        duration: 300,
      }).start(() => {
        setTimeout(() => {
          this.setState({ overlayTextIsVisible: false })
        }, 30)
      })
    }, 300)
  }

  _onPlayerLayout = e => {
    const playerWidth = e.nativeEvent.layout.width

    // console.info('playerWidth: ', playerWidth)

    this.setState({ playerWidth })
  }

  _onOverlayLayout = e => {
    const overlayWidth = e.nativeEvent.layout.width

    // console.info('overlayWidth: ', overlayWidth)

    this.overlayWidth = new Animated.Value(overlayWidth)
  }
}
