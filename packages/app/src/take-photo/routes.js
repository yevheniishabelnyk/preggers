/*
 *
 * Take photo routes
 *
 */

import TakePhoto from './screens/TakePhoto'

export default {
  '/take-photo': {
    screen: TakePhoto,
  },
}
