/**
 *
 * TakePhoto screen
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, StatusBar, Image, Platform, CameraRoll } from 'react-native'
import {
  Camera,
  Permissions,
  ImagePicker,
  MediaLibrary,
  takeSnapshotAsync,
} from 'expo'
import BaseScreen from '@/app/base/components/BaseScreen'
import Controls from '@/take-photo/components/organisms/Controls'
import PhotoPreview from '@/take-photo/components/organisms/PhotoPreview'
import Header from '@/take-photo/components/organisms/Header'
import SlideUpDown from '@/shared/components/SlideUpDown'
import PhotoFilters from '@/take-photo/components/organisms/PhotoFilters'
import Fade from '@/shared/components/Fade'

import messages from './messages'

import styles, { screenWidth } from './styles'

import * as ControlsConstants from '@/take-photo/components/organisms/Controls/constants'

import { isFunction } from 'lodash'

export default class TakePhoto extends BaseScreen {
  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  state = {
    hasCameraPermission: null,
    hasCameraRollPermission: null,
    flashMode: Camera.Constants.FlashMode.off,
    zoom: 0,
    autoFocus: Camera.Constants.AutoFocus.on,
    depth: 0,
    type: Camera.Constants.Type.back,
    whiteBalance: Camera.Constants.WhiteBalance.auto,
    ratio: '16:9',
    // ratios: [],
    photo: null,
    isProcessingFilter: false,
  }

  render() {
    const {
      isPhotoSaved,
      hasCameraPermission,
      photo,
      originalPhoto,
    } = this.state

    if (hasCameraPermission === null) {
      return <View style={styles.blackBackground} />
    }

    return (
      <View style={styles.container}>
        <StatusBar hidden />

        <Header
          isCancelButtonVisible={Boolean(photo)}
          onGoBack={this._goBack}
          onCancel={this._cancelButtonPressed}
        />

        <Camera
          style={styles.camera}
          ref={ref => (this._camera = ref)}
          type={this.state.type}
          flashMode={this.state.flash}
          autoFocus={this.state.autoFocus}
          zoom={this.state.zoom}
          whiteBalance={this.state.whiteBalance}
          focusDepth={this.state.depth}
          // ratio={this.state.ratio}
          // onCameraReady={(d) => console.info(d)}
        >
          <View style={styles.content} ref={ref => (this._content = ref)}>
            {photo ? (
              <PhotoPreview
                photo={photo}
                originalPhoto={originalPhoto}
                isProcessingFilter={this.state.isProcessingFilter}
                onResizedPhoto={this._resizedPhotoLoaded}
              />
            ) : null}
          </View>
        </Camera>

        {this.savedPhotoUri && Platform.OS === 'android' && isPhotoSaved ? (
          <Image
            style={[styles.savedPhoto, styles.androidSavedPhoto]}
            source={{
              uri: this.savedPhotoUri,
            }}
          />
        ) : null}

        <View style={styles.footer}>
          {this.savedPhotoUri ? (
            <Fade visible={isPhotoSaved}>
              <Image
                style={styles.savedPhoto}
                source={{
                  uri: this.savedPhotoUri,
                }}
              />
            </Fade>
          ) : null}

          <View style={styles.controlsWrapper} collapsable={false}>
            <Controls
              mode={
                photo
                  ? ControlsConstants.PHOTO_EDITOR_MODE
                  : ControlsConstants.CAMERA_MODE
              }
              flashMode={this.state.flashMode}
              onTakePhoto={this._takePhoto}
              onSavePhoto={this._savePhotoButtonPressed}
              onChangeFlashMode={this._toggleCameraFlash}
              onSwitchCamera={this._switchCamera}
              onOpenMediaLibrary={this._libraryButtonPressed}
              onAddFilter={this._toggleFilters}
              onDone={this._doneButtonPressed}
              doneButtonTitle={this._doneButtonTitle}
            />

            {originalPhoto ? (
              <SlideUpDown
                isVisible={this.state.isFiltersVisible}
                onSlideUp={this._filtersSlidedUp}
                onSlideDown={this._filtersSlidedDown}
                height={PhotoFilters.height}
              >
                <PhotoFilters
                  image={originalPhoto}
                  flipImage={this.state.type === Camera.Constants.Type.front}
                  resizedImageUri={this.state.resizedPhotoUri}
                  isVisible={this.state.isSlidingFiltersUpFinished}
                  onSelect={this._filterSelected}
                  onImage={this._filteredImageLoaded}
                />
              </SlideUpDown>
            ) : null}
          </View>
        </View>
      </View>
    )
  }

  _filtersSlidedUp = () => {
    console.log('_filtersSlidedUp')

    this.setState({ isSlidingFiltersUpFinished: true })
  }

  _filtersSlidedDown = () => {
    console.log('_filtersSlidedDown')

    this.setState({ isSlidingFiltersUpFinished: false })
  }

  _toggleFilters = () => {
    console.log('_toggleFilters')

    this.setState(prevState => ({
      isFiltersVisible: !prevState.isFiltersVisible,
    }))
  }

  _resizedPhotoLoaded = result => {
    console.log('_resizedPhotoLoaded', result)
    this.setState({
      resizedPhotoUri: result,
    })
  }

  _filteredImageLoaded = uri =>
    this.setState(prevState => ({
      photo: Object.assign({}, prevState.photo, { uri }),
      isProcessingFilter: false,
    }))

  _filterSelected = ({ uri, isOriginal }) => {
    if (isOriginal) {
      this.setState(prevState => ({
        photo: prevState.originalPhoto,
        photoWidth: prevState.originalPhotoWidth,
        photoHeight: prevState.originalPhotoHeight,
      }))
    } else if (uri !== null) {
      this.setState(prevState => ({
        photo: Object.assign({}, prevState.photo, { uri }),
        photoWidth: prevState.photo.width,
        photoHeight: prevState.photo.height,
      }))
    } else {
      this.setState({ isProcessingFilter: true })
    }
  }

  async componentDidMount() {
    try {
      const { status } = await Permissions.askAsync(Permissions.CAMERA)

      this.setState({ hasCameraPermission: status === 'granted' })

      if (status !== 'granted') {
        const { formatMessage } = this.context.intl

        alert(formatMessage(messages.denideOnCamera))
      }
    } catch (err) {
      console.log('err', err)
    }

    if (Platform.OS === 'android') {
      try {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)

        this.setState({ hasCameraPermission: status === 'granted' })

        if (status !== 'granted') {
          const { formatMessage } = this.context.intl

          alert(formatMessage(messages.denideOnCamera))
        }
      } catch (err) {
        console.log('err', err)
      }
    }

    try {
      const { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL)

      console.info('status: ', status)

      this.setState({
        hasCameraRollPermission:
          status === 'granted' || (status === 'undetermined' ? null : false),
      })
    } catch (err) {
      console.log('err', err)
    }

    // console.info('ratios: ', await this._getRatios())
  }

  _checkLibraryPermission = async () => {
    const { hasCameraRollPermission, hasCameraPermission } = this.state

    if (!hasCameraRollPermission && Platform.OS === 'ios') {
      if (hasCameraRollPermission === false) {
        const { formatMessage } = this.context.intl

        alert(formatMessage(messages.denideOnCameraRoll))

        return Promise.reject('Denied!')
      }

      if (hasCameraRollPermission === null) {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)

        this.setState({ hasCameraRollPermission: status === 'granted' })

        if (status !== 'granted') {
          return Promise.reject('Denied!')
        }
      }
    }

    if (!hasCameraPermission && Platform.OS === 'android') {
      if (hasCameraPermission === false) {
        const { formatMessage } = this.context.intl

        alert(formatMessage(messages.denideOnCameraRoll))

        return Promise.reject('Denied!')
      }

      if (hasCameraPermission === null) {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)

        this.setState({ hasCameraPermission: status === 'granted' })

        if (status !== 'granted') {
          return Promise.reject('Denied!')
        }
      }
    }

    return Promise.resolve()
  }

  _libraryButtonPressed = async () => {
    try {
      await this._checkLibraryPermission()
    } catch (err) {
      return
    }

    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: false,
      })

      if (result.cancelled) {
        return
      }

      const { height, width } = this._calculatePhotoSize(
        result.height,
        result.width
      )

      const photo = {
        uri: result.uri,
        height,
        width,
      }

      this.setState({
        originalPhoto: photo,
        photo,
        originalPhotoWidth: result.width,
        originalPhotoHeight: result.height,
        photoWidth: result.width,
        photoHeight: result.height,
      })
    } catch (err) {
      console.log('err', err)
    }
  }

  _savePhotoButtonPressed = async () => {
    try {
      await this._checkLibraryPermission()
    } catch (err) {
      return
    }

    const { photo } = this.state

    if (photo && photo.uri !== this.savedPhotoUri) {
      try {
        await this._saveFile(photo.uri)
      } catch (err) {
        console.log('Taking shanpshot is faild!', err)
      }
    }
  }

  _saveFile = async uri => {
    try {
      if (Platform.OS === 'android') {
        CameraRoll.saveToCameraRoll(uri, 'photo')
      } else {
        const asset = await MediaLibrary.createAssetAsync(uri)

        console.info('asset: ', asset)
      }

      this.savedPhotoUri = uri

      this.setState({ isPhotoSaved: true })

      setTimeout(() => {
        this.setState({ isPhotoSaved: false })
      }, 1500)
    } catch (err) {
      console.info('err: ', err)
    }
  }

  _takePhoto = async () => {
    if (this._camera && !this.state.photo) {
      try {
        const result = await this._camera.takePictureAsync()

        const { height, width } = this._calculatePhotoSize(
          result.height,
          result.width
        )

        const photo = {
          uri: result.uri,
          height,
          width,
        }

        console.log('photo', photo)

        this.setState({
          originalPhoto: photo,
          photo,
          originalPhotoWidth: result.width,
          originalPhotoHeight: result.height,
          photoWidth: result.width,
          photoHeight: result.height,
        })
      } catch (err) {
        console.log('err', err)
      }
    }
  }

  _calculatePhotoSize = (photoHeight, photoWidth) => ({
    width: screenWidth,
    height: (screenWidth * photoHeight) / photoWidth,
  })

  _cancelButtonPressed = () => {
    if (this.state.photo) {
      this.setState({
        originalPhoto: null,
        photo: null,
        originalPhotoWidth: null,
        originalPhotoHeight: null,
        photoWidth: null,
        photoHeight: null,
        isFiltersVisible: false,
        isSlidingFiltersUpFinished: false,
      })
    }
  }

  _doneButtonPressed = () => {
    const { photo } = this.state

    if (photo && !this.state.isProcessingFilter) {
      const redirectTo = this.props.navigation.getParam('redirectTo')

      if (redirectTo) {
        requestAnimationFrame(async () => {
          try {
            const localUri = await takeSnapshotAsync(this._content, {
              format: 'png',
              result: 'file',
              quality: 1.0,
            })

            this._goTo(redirectTo, { photo: { uri: localUri } })
          } catch (err) {
            console.log('Taking shanpshot is faild!', err)
          }
        })
      } else {
        this.setState({ isSlidingFiltersUpFinished: false })

        const onDone = this.props.navigation.getParam('onDone')

        this._goBack()

        if (isFunction(onDone)) {
          onDone({
            image: {
              uri: photo.uri,
              width: this.state.photoWidth,
              height: this.state.photoHeight,
            },
            fromFrontCamera: this.state.type === Camera.Constants.Type.front,
            fromCamera: false,
          })
        }
      }
    }
  }

  _toggleCameraFlash = () => {
    if (!this.state.photo) {
      this.setState(prevState => ({
        flashMode:
          prevState.flashMode === Camera.Constants.FlashMode.off
            ? Camera.Constants.FlashMode.on
            : Camera.Constants.FlashMode.off,
      }))
    }
  }

  _switchCamera = () => {
    if (!this.state.photo) {
      this.setState({
        type:
          this.state.type === Camera.Constants.Type.back
            ? Camera.Constants.Type.front
            : Camera.Constants.Type.back,
      })
    }
  }

  /*_getRatios = async () => {
  const ratios = await this.camera.getSupportedRatiosAsync()
  return ratios
  }

  _setRatio(ratio) {
  this.setState({
    ratio,
  })
  }*/
}
