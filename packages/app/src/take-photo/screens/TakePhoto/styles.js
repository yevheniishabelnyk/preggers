import { StyleSheet, Dimensions } from 'react-native'

export const { width: screenWidth, height: screenHeight } = Dimensions.get(
  'window'
)

import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'white',
  },

  footer: {
    zIndex: 3,
    left: 0,
    bottom: 0,
    position: 'absolute',
    marginBottom: 10,
  },

  controlsWrapper: {
    overflow: 'hidden',
    zIndex: 10,
    position: 'relative',
  },

  content: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'transparent',
    zIndex: 2,
  },

  blackBackground: {
    backgroundColor: '#000',
  },

  camera: {
    flex: 1,
    position: 'relative',
  },

  savedPhoto: {
    height: moderateScale(56),
    width: moderateScale(56),
    borderRadius: moderateScale(5),
    position: 'absolute',
    bottom: moderateScale(-10),
    left: moderateScale(70),
  },

  androidSavedPhoto: {
    bottom: moderateScale(80),
  },
})

export default styles
