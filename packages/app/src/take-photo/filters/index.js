/**
 *
 * Instagram filters entry point
 *
 */

export { default as Amaro } from './instagram/Amaro'
export { default as Brannan } from './instagram/Brannan'
export { default as Earlybird } from './instagram/Earlybird'
export { default as F1977 } from './instagram/F1977'
export { default as Hefe } from './instagram/Hefe'
export { default as Hudson } from './instagram/Hudson'
export { default as Inkwell } from './instagram/Inkwell'
export { default as Lokofi } from './instagram/Lokofi'
export { default as LordKelvin } from './instagram/LordKelvin'
export { default as Nashville } from './instagram/Nashville'
export { default as Normal } from './instagram/Normal'
export { default as Rise } from './instagram/Rise'
export { default as Sierra } from './instagram/Sierra'
export { default as Sutro } from './instagram/Sutro'
export { default as Toaster } from './instagram/Toaster'
export { default as Valencia } from './instagram/Valencia'
export { default as Walden } from './instagram/Walden'
export { default as XproII } from './instagram/XproII'
// export { default as Saturate } from './Saturate'
