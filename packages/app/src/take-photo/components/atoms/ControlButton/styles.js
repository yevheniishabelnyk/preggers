import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    padding: moderateScale(7),
  },

  content: {
    alignItems: 'center',
  },

  iconContainer: {
    width: moderateScale(45),
    height: moderateScale(45),
  },

  icon: {
    flex: 1,
    width: null,
    height: null,
  },

  title: {
    marginTop: -1,
    fontSize: moderateScale(10),
    fontFamily: 'Now-Medium',
    color: 'white',
    textAlign: 'center',
  },
})

export default styles
