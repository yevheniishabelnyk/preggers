/**
 *
 * ControlButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, TouchableOpacity, Image } from 'react-native'

import styles from './styles'

const ControlButton = ({ icon, title, style, onPress }) => (
  <TouchableOpacity
    style={[styles.button, style]}
    onPress={onPress}
    activeOpacity={0.95}
  >
    <View style={styles.content}>
      <View style={styles.iconContainer}>
        <Image style={styles.icon} source={icon} resizeMode="contain" />
      </View>

      <Text style={styles.title}>{title}</Text>
    </View>
  </TouchableOpacity>
)

ControlButton.propTypes = {
  onPress: PropTypes.func,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number,
  ]),
  icon: PropTypes.any,
  title: PropTypes.string,
}

export default ControlButton
