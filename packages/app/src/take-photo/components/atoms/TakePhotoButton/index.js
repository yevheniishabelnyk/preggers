/**
 *
 * TakePhotoButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import styles from './styles'

export default class TakePhotoButton extends React.Component {
  static propTypes = {
    onPress: PropTypes.func,
  }

  render() {
    const { onPress } = this.props

    return (
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
        activeOpacity={0.95}
      >
        <View style={styles.whiteCircle} />
      </TouchableOpacity>
    )
  }
}
