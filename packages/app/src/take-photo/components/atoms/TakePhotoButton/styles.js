import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const takePhotoBtnContainerWidth = width * 0.272
const takePhotoBtnWidth = width * 0.272 * 0.75

const styles = StyleSheet.create({
  button: {
    width: takePhotoBtnContainerWidth,
    height: takePhotoBtnContainerWidth,
    borderRadius: takePhotoBtnContainerWidth / 2,
    backgroundColor: 'rgba(255, 255, 255, 0.58)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  whiteCircle: {
    width: takePhotoBtnWidth,
    height: takePhotoBtnWidth,
    borderRadius: takePhotoBtnWidth / 2,
    backgroundColor: '#ffffff',
  },
})

export default styles
