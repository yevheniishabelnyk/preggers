/**
 *
 * SwitchCameraButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableHighlight, Image } from 'react-native'

import styles from './styles'

const CameraButton = ({ onPress, style, iconStyle, icon }) => (
  <TouchableHighlight
    onPress={onPress}
    style={[styles.button, style]}
    underlayColor="rgba(255, 255, 255, 0.2)"
  >
    <View style={[styles.iconContainer, iconStyle]}>
      <Image style={styles.icon} source={icon} resizeMode="contain" />
    </View>
  </TouchableHighlight>
)

CameraButton.propTypes = {
  onPress: PropTypes.func,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number,
  ]),
  iconStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number,
  ]),
  icon: PropTypes.any,
}

export default CameraButton
