import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const buttonWidth = moderateScale(50)
const iconWidth = moderateScale(30)

const styles = StyleSheet.create({
  button: {
    width: buttonWidth,
    height: buttonWidth,
    borderRadius: buttonWidth / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  iconContainer: {
    width: iconWidth,
    height: iconWidth,
    borderRadius: iconWidth / 2,
  },

  icon: {
    flex: 1,
    height: null,
    width: null,
  },
})

export default styles
