import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  link: {
    shadowOpacity: 0.5,
    shadowRadius: 6,
    shadowColor: '#E6E8F1',
    shadowOffset: { height: -5, width: -2 },
    backgroundColor: 'transparent',
  },

  linkText: {
    fontSize: width * 0.0373,
    color: '#FFFFFF',
    backgroundColor: 'transparent',
  },
})

export default styles
