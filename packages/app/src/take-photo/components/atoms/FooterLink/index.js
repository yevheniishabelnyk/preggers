/**
 *
 * FooterLink
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, Text } from 'react-native'

import styles from './styles'

export default class FooterLink extends React.Component {
  static propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array,
      PropTypes.number,
    ]),
  }

  render() {
    const { text, onPress, style } = this.props

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.link, style]}
        activeOpacity={0.95}
      >
        <Text style={styles.linkText}>{text}</Text>
      </TouchableOpacity>
    )
  }
}
