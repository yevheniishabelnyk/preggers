/**
 *
 * WhiteButton
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, TouchableOpacity } from 'react-native'

import styles from './styles'

const WhiteButton = ({ title, style, onPress }) => (
  <TouchableOpacity
    style={[styles.button, style]}
    onPress={onPress}
    activeOpacity={0.95}
  >
    <Text style={styles.title}>{title}</Text>
  </TouchableOpacity>
)

WhiteButton.propTypes = {
  onPress: PropTypes.func,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number,
  ]),
  title: PropTypes.string,
}

export default WhiteButton
