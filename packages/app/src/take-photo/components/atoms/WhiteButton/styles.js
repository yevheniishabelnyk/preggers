import { StyleSheet } from 'react-native'
import { moderateScale } from '@/app/scaling'

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'white',
    padding: moderateScale(7),
    borderRadius: moderateScale(100),
    paddingVertical: moderateScale(14),
    paddingHorizontal: moderateScale(27),
    alignItems: 'center',
  },

  title: {
    fontSize: moderateScale(14),
    fontFamily: 'Now-Bold',
    color: 'rgb(144,47,121)',
    textAlign: 'center',
  },
})

export default styles
