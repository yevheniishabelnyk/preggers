import { StyleSheet, Dimensions } from 'react-native'

export const { width: screenWidth, height: screenHeight } = Dimensions.get(
  'window'
)

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'black',
  },

  originalPhoto: {
    position: 'absolute',
    left: 0,
    zIndex: 1,
  },

  photoContainer: {
    position: 'absolute',
    left: 0,
    zIndex: 3,
  },

  photo: {
    width: screenWidth,
  },

  activityIndicator: {
    position: 'absolute',
    left: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.15)',
    zIndex: 9,
  },
})

export default styles
