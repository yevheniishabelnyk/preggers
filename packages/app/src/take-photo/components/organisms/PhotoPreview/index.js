/**
 *
 * PhotoPreview
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import {
  View,
  Animated,
  ActivityIndicator,
  Image,
  PanResponder,
} from 'react-native'

import { isFunction } from 'lodash'
import { takeSnapshotAsync } from 'expo'

import styles, { screenHeight } from './styles'

export default class PhotoPreview extends React.Component {
  static propTypes = {
    photo: PropTypes.shape({
      uri: PropTypes.string,
      width: PropTypes.number,
      height: PropTypes.number,
    }),
    originalPhoto: PropTypes.shape({
      uri: PropTypes.string,
      width: PropTypes.number,
      height: PropTypes.number,
    }),
    isProcessingFilter: PropTypes.bool,
    onResizedPhoto: PropTypes.func,
  }

  render() {
    const { photo, originalPhoto } = this.props

    if (!originalPhoto) {
      return null
    }

    const { isProcessingFilter } = this.props

    console.log('photo', photo)

    return (
      <View style={styles.container}>
        {isProcessingFilter ? (
          <ActivityIndicator
            animating
            size="large"
            color="white"
            style={[
              styles.activityIndicator,
              {
                height: originalPhoto.height,
                width: originalPhoto.width,
                top: (screenHeight - originalPhoto.height) / 2,
              },
            ]}
          />
        ) : null}

        <Image
          style={[
            styles.originalPhoto,
            {
              height: originalPhoto.height,
              width: originalPhoto.width,
              top: (screenHeight - originalPhoto.height) / 2,
            },
          ]}
          source={originalPhoto}
          onLoad={this._originalPhotoLoaded}
          ref={ref => (this._originalPhoto = ref)}
        />

        <Animated.View
          {...this.photoTouchResponder.panHandlers}
          style={[
            styles.photoContainer,
            {
              height: originalPhoto.height,
              width: originalPhoto.width,
              top: (screenHeight - originalPhoto.height) / 2,
            },
            { opacity: this.opacityValue },
          ]}
        >
          <Image
            source={photo}
            style={[
              styles.photo,
              {
                height: originalPhoto.height,
                width: originalPhoto.width,
              },
            ]}
          />
        </Animated.View>
      </View>
    )
  }

  _originalPhotoLoaded = () => {
    const { onResizedPhoto } = this.props

    if (isFunction(onResizedPhoto)) {
      requestAnimationFrame(async () => {
        try {
          let result = await takeSnapshotAsync(this._originalPhoto, {
            format: 'png',
            result: 'file',
            quality: 1.0,
          })

          console.info('_originalPhotoLoaded result: ', result)

          onResizedPhoto(result)
        } catch (err) {
          console.log('Taking shanpshot of resized image error!', err)
        }
      })
    }
  }

  componentWillMount() {
    this.opacityValue = new Animated.Value(1)

    this.photoTouchResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderStart: () => {
        this.opacityValue.setValue(0)
      },
      onPanResponderRelease: () => {
        this.opacityValue.setValue(1)
      },
    })
  }
}
