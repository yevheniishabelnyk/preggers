import { defineMessages } from 'react-intl'

export default defineMessages({
  cancelButtonTitle: 'Cancel',
})
