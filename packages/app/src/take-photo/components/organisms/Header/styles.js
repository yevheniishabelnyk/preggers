import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'
import { ifIphoneX } from 'react-native-iphone-x-helper'

export const { width: screenWidth, height: screenHeight } = Dimensions.get(
  'window'
)

const styles = StyleSheet.create({
  header: {
    left: 0,
    top: 0,
    width: screenWidth,
    height: moderateScale(70),
    position: 'absolute',
    zIndex: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },

  backButton: {
    ...ifIphoneX({
      top: 25 + moderateScale(20),
    }),
  },

  cancelButton: {
    position: 'absolute',
    left: moderateScale(15),
    ...ifIphoneX(
      {
        top: 25 + moderateScale(26, 0.3),
      },
      {
        top: moderateScale(26, 0.3),
      }
    ),
  },
})

export default styles
