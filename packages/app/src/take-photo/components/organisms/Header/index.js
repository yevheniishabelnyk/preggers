/**
 *
 * Header
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import FooterLink from '@/take-photo/components/atoms/FooterLink'
import GoBackButton from '@/onboarding/components/GoBackButton'

import messages from './messages'

import styles from './styles'

export default class Header extends React.Component {
  static propTypes = {
    onGoBack: PropTypes.func,
    onCancel: PropTypes.func,
    isCancelButtonVisible: PropTypes.bool,
  }

  static defaultProps = {
    isCancelButtonVisible: false,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  render() {
    const { isCancelButtonVisible, onGoBack, onCancel } = this.props
    const { formatMessage } = this.context.intl

    return (
      <View style={styles.header}>
        {isCancelButtonVisible ? (
          <FooterLink
            text={formatMessage(messages.cancelButtonTitle).toUpperCase()}
            onPress={onCancel}
            style={styles.cancelButton}
          />
        ) : (
          <GoBackButton onPress={onGoBack} style={styles.backButton} />
        )}
      </View>
    )
  }
}
