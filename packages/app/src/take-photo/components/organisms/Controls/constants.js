/**
 *
 * Controls constants
 *
 */

export const CAMERA_MODE = 'CAMERA_MODE'
export const PHOTO_EDITOR_MODE = 'PHOTO_EDITOR_MODE'
