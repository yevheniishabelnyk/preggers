import { StyleSheet, Dimensions } from 'react-native'
import { moderateScale } from '@/app/scaling'

const { width: screenWidth } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    width: screenWidth,
  },

  firstColumn: {
    justifyContent: 'flex-end',
  },

  secondColumn: {
    justifyContent: 'center',
  },

  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonsColumn: {
    flex: 1,
    flexDirection: 'row',
  },

  cameraFlashIcon: {
    height: moderateScale(25),
  },

  libraryButton: {
    paddingLeft: moderateScale(7),
  },
})

export default styles
