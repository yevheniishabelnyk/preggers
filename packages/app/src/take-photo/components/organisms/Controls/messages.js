import { defineMessages } from 'react-intl'

export default defineMessages({
  libraryButtonTitle: 'Library',
  saveButtonTitle: 'Save',
  effectsButtonTitle: 'Effects',
  doneButtonTitle: 'Done',
})
