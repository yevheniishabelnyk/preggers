/**
 *
 * Controls
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'
import { Camera } from 'expo'
import TakePhotoButton from '@/take-photo/components/atoms/TakePhotoButton'
import ControlButton from '@/take-photo/components/atoms/ControlButton'
import WhiteButton from '@/take-photo/components/atoms/WhiteButton'
import CameraButton from '@/take-photo/components/atoms/CameraButton'
import Row from 'react-native-row'
// import { View as Column } from 'react-native-row'

import flashIcon from 'assets/icons/camera-flash.png'
import flashActiveIcon from 'assets/icons/camera-flash-active.png'

import messages from './messages'

import styles from './styles'

import * as ControlsConstants from './constants'

export default class Controls extends React.Component {
  static propTypes = {
    onTakePhoto: PropTypes.func,
    onSavePhoto: PropTypes.func,
    onChangeFlashMode: PropTypes.func,
    onSwitchCamera: PropTypes.func,
    onOpenMediaLibrary: PropTypes.func,
    onAddFilter: PropTypes.func,
    onDone: PropTypes.func,
    flashMode: PropTypes.number,
    doneButtonTitle: PropTypes.string,
    mode: PropTypes.string,
    style: PropTypes.any,
  }

  static contextTypes = {
    intl: PropTypes.object.isRequired,
  }

  static defaultProps = {
    mode: ControlsConstants.CAMERA_MODE,
  }

  constructor(props, context) {
    super(props)

    this._defaultDoneButtonTitle = context.intl.formatMessage(
      messages.doneButtonTitle
    )
  }

  render() {
    const {
      style,
      mode,
      flashMode,
      onTakePhoto,
      onSavePhoto,
      onChangeFlashMode,
      onSwitchCamera,
      onOpenMediaLibrary,
      onAddFilter,
      onDone,
      doneButtonTitle = this._defaultDoneButtonTitle,
    } = this.props
    const { formatMessage } = this.context.intl

    return (
      <View style={[styles.container, style]}>
        {mode === ControlsConstants.CAMERA_MODE ? (
          <View style={styles.buttons}>
            <View style={[styles.buttonsColumn, styles.firstColumn]}>
              <CameraButton
                iconStyle={styles.cameraFlashIcon}
                onPress={onChangeFlashMode}
                icon={
                  flashMode === Camera.Constants.FlashMode.off
                    ? flashIcon
                    : flashActiveIcon
                }
              />
            </View>

            <View style={[styles.buttonsColumn, styles.secondColumn]}>
              <TakePhotoButton onPress={onTakePhoto} />
            </View>

            <View style={styles.buttonsColumn}>
              <CameraButton
                style={styles.switchCameraButton}
                onPress={onSwitchCamera}
                icon={require('assets/icons/camera-switch.png')}
              />
            </View>
          </View>
        ) : null}

        <Row padding={[0, 9]} margin={[15, 0, 0, 0]}>
          <Row flex dial={4}>
            <ControlButton
              style={styles.libraryButton}
              title={formatMessage(messages.libraryButtonTitle)}
              icon={require('assets/icons/photo-library.png')}
              onPress={onOpenMediaLibrary}
            />

            {mode === ControlsConstants.PHOTO_EDITOR_MODE ? (
              <ControlButton
                title={formatMessage(messages.saveButtonTitle)}
                icon={require('assets/icons/save-file.png')}
                onPress={onSavePhoto}
              />
            ) : null}
          </Row>

          <Row flex dial={4} spaceBetween>
            {mode === ControlsConstants.PHOTO_EDITOR_MODE ? (
              <ControlButton
                title={formatMessage(messages.effectsButtonTitle)}
                icon={require('assets/icons/magic-wand.png')}
                onPress={onAddFilter}
              />
            ) : null}

            {mode === ControlsConstants.PHOTO_EDITOR_MODE &&
            onDone &&
            doneButtonTitle ? (
              <WhiteButton title={doneButtonTitle} onPress={onDone} />
            ) : null}
          </Row>
        </Row>
      </View>
    )
  }
}
