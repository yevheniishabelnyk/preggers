/**
 *
 * PhotoFilterSelect
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { View, ScrollView, Image } from 'react-native'

import { takeSnapshotAsync } from 'expo'

import FilterPreview from '@/take-photo/components/molecules/FilterPreview'
import NormalFilterPreview from '@/take-photo/components/molecules/NormalFilterPreview'

import { keys, isFunction } from 'lodash'
import * as Filters from '@/take-photo/filters'
import { Surface } from 'gl-react-expo'

import styles, { containerHeight } from './styles'

export default class PhotoFilterSelect extends React.Component {
  static propTypes = {
    image: PropTypes.any,
    isVisible: PropTypes.bool,
    flipImage: PropTypes.bool,
    onSelect: PropTypes.func,
    onImage: PropTypes.func,
    style: PropTypes.any,
    resizedImageUri: PropTypes.string,
  }

  static height = containerHeight

  constructor(props) {
    super(props)

    this.filterNames = keys(Filters)

    this.state = {
      resizedImage: null,
      originalImageUri: null,
      requestedFilterName: null,
      loadingFilterIndex: false,
      activeFilter: 'Normal',
    }

    this.images = {}
  }

  render() {
    const { image } = this.props

    if (!image) {
      return null
    }

    const { style } = this.props

    const { requestedFilterName, requestedFilterPreload } = this.state
    const RequestedFilter = Filters[requestedFilterName]

    return (
      <View style={[styles.container, style]}>
        {requestedFilterName !== null ? (
          <Surface
            ref={ref => (this._requestedFilterSurface = ref)}
            preload={requestedFilterPreload}
            style={[
              styles.requestedFilterSurface,
              { width: image.width, height: image.height },
              this.props.flipImage ? styles.flipImage : null,
            ]}
            onLoad={this._requestedFilterSurfaceLoaded}
          >
            <RequestedFilter>{requestedFilterPreload[0]}</RequestedFilter>
          </Surface>
        ) : null}

        {image && !this.state.resizedImage ? (
          <View
            style={[
              styles.reqizedImageContainer,
              this.props.flipImage ? styles.flipImage : null,
            ]}
          >
            <Image
              ref={ref => (this._resizeImageContainer = ref)}
              style={[
                this.props.flipImage ? styles.flipImage : null,
                styles.resizedImage,
              ]}
              source={image}
              resizeMode="cover"
              onLoad={this._resizableImageLoaded}
            />
          </View>
        ) : (
          <ScrollView style={styles.filtersContainer} horizontal>
            <NormalFilterPreview
              name="Normal"
              active={this.state.activeFilter === 'Normal'}
              flipImage={this.props.flipImage}
              onPress={this._filterSelected}
              image={this.state.resizedImage}
            />

            {this.filterNames.map((name, index) => (
              <FilterPreview
                key={name}
                name={name}
                index={index}
                active={this.state.activeFilter === name}
                flipImage={this.props.flipImage}
                startLoading={this.state.loadingFilterIndex === index}
                onPress={this._filterSelected}
                onLoad={this._filterLoaded}
                image={this.state.resizedImage}
              />
            ))}
          </ScrollView>
        )}
      </View>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isVisible && nextProps.isVisible) {
      const { loadingFilterIndex } = this.state

      if (loadingFilterIndex === false) {
        this.setState({ loadingFilterIndex: 0 })
      } else if (loadingFilterIndex !== this.filterNames.length - 1) {
        this.setState({ loadingFilterIndex: loadingFilterIndex + 1 })
      }
    }

    if (this.props.image && this.props.image !== nextProps.image) {
      this.setState({ resizedImage: null })
    }
  }

  _resizableImageLoaded = () => {
    console.log('_resizableImageLoaded')

    requestAnimationFrame(async () => {
      if (this._resizeImageContainer) {
        try {
          let result = await takeSnapshotAsync(this._resizeImageContainer, {
            format: 'png',
            result: 'file',
            quality: 1.0,
          })
          this.setState({
            resizedImage: { uri: result },
            loadingFilterIndex: 0,
          })
        } catch (err) {
          console.log('Taking shanpshot of resized image error!', err)
        }
      }
    })
  }

  _filterSelected = name => {
    console.log('_filterSelected ', name)

    if (this.state.activeFilter !== name) {
      if (isFunction(this.props.onSelect)) {
        if (this.images[name] || name === 'Normal') {
          this.props.onSelect({
            uri: this.images[name],
            isOriginal: name === 'Normal',
          })

          this.setState({ activeFilter: name })
        } else {
          this.props.onSelect({ uri: null })

          if (name !== 'Normal' && isFunction(this.props.onImage)) {
            this.setState({
              requestedFilterName: name,
              requestedFilterPreload: [
                { uri: this.props.resizedImageUri || false },
              ].concat(Filters[name].preload),
            })
          }
        }
      }
    }
  }

  _filterLoaded = index => {
    if (this.props.isVisible) {
      if (index !== this.filterNames.length - 1) {
        this.setState({ loadingFilterIndex: index + 1 })
      } else {
        this.setState({ loadingFilterIndex: null })
      }
    }
  }

  _requestedFilterSurfaceLoaded = () => {
    console.info('_requestedFilterSurfaceLoaded')

    setTimeout(() => {
      requestAnimationFrame(async () => {
        if (this._requestedFilterSurface) {
          try {
            let result = await takeSnapshotAsync(this._requestedFilterSurface, {
              format: 'png',
              result: 'file',
              quality: 1.0,
            })

            const { requestedFilterName } = this.state

            this.images[requestedFilterName] = result

            if (isFunction(this.props.onImage)) {
              this.props.onImage(result)
            }

            this.setState({
              activeFilter: requestedFilterName,
              requestedFilterName: null,
              requestedFilterPreload: null,
            })
          } catch (err) {
            console.log('Taking shanpshot of resized image error!', err)
          }
        }
      })
    }, 500)
  }
}
