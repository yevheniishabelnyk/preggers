import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

export const containerHeight = 90 + 5

const styles = StyleSheet.create({
  container: {
    height: containerHeight,
    backgroundColor: 'transparent',
    position: 'relative',
    flex: 1,
  },

  filtersContainer: {
    paddingLeft: 20,
    height: 120,
  },

  resizableImage: {
    width: 70,
    height: 70,
  },

  requestedFilterSurface: {
    position: 'absolute',
    top: -height,
    left: -width,
  },

  flipImage: {
    transform: [{ scaleX: -1 }],
  },

  reqizedImageContainer: {
    width: 70,
    height: 70,
    position: 'absolute',
    left: -200,
    top: 0,
  },

  resizedImage: {
    width: 70,
    height: 70,
  },
})

export default styles
