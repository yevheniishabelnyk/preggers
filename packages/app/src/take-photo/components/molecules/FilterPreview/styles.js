import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    width: 70,
    height: 90,
    marginRight: 15,
    alignItems: 'center',
    borderRadius: 8,
    position: 'relative',
    overflow: 'hidden',
  },

  blueText: {
    color: 'skyblue',
  },

  surface: {
    width: 70,
    height: 70,
    borderRadius: 8,
    // overflow: 'hidden',
    // position: 'absolute',
    // left: 0,
    // top: 0,
  },

  preview: {
    width: 70,
    height: 70,
    borderRadius: 8,
    overflow: 'hidden',
    position: 'absolute',
    left: 0,
    top: 0,
  },

  activityIndicator: {
    width: 70,
    height: 70,
    borderRadius: 8,
    position: 'absolute',
    left: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    backgroundColor: 'rgba(216, 216, 216, 0.3)',
  },

  name: {
    fontFamily: 'Now-Medium',
    fontSize: 13,
    color: 'white',
    position: 'absolute',
    textAlign: 'center',
    bottom: 0,

    left: 0,
    right: 0,
  },

  flipImage: {
    transform: [{ scaleX: -1 }],
  },
})

export default styles
