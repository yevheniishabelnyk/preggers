/**
 *
 * FiterPreview
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Surface } from 'gl-react-expo'
import { takeSnapshotAsync } from 'expo'

import { keys, isFunction } from 'lodash'
import * as Filters from '@/take-photo/filters'

import styles from './styles'

export default class FiterPreview extends React.Component {
  static propTypes = {
    image: PropTypes.object,
    name: PropTypes.string,
    startLoading: PropTypes.bool,
    onPress: PropTypes.func,
    onLoad: PropTypes.func,
    index: PropTypes.number,
    flipImage: PropTypes.bool,
    active: PropTypes.bool,
  }

  state = {
    previewImage: null,
  }

  constructor(props) {
    super(props)

    this.filterNames = keys(Filters)
    this.preload = [props.image].concat(Filters[props.name].preload)

    // console.info('this.preload: ', this.preload)
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return (
  //     (!this.props.startLoading && nextProps.startLoading) ||
  //     this.props.active !== nextProps.active ||
  //     (!this.state.previewLoaded && nextState.previewLoaded) ||
  //     (!this.state.previewImage && nextState.previewImage)
  //   )
  // }

  _surfaceLoaded = () => {
    console.log('_surfaceLoaded')

    setTimeout(() => {
      requestAnimationFrame(async () => {
        if (this._surface) {
          try {
            let result = await takeSnapshotAsync(this._surface, {
              format: 'png',
              result: 'file',
              quality: 1.0,
            })

            this.setState({
              previewImage: { uri: result },
            })

            if (isFunction(this.props.onLoad)) {
              this.props.onLoad(this.props.index, this.props.name)
            }
          } catch (e) {
            alert(e)
          }
        }
      })
    }, 200)
  }

  _onPress = () => {
    if (this.state.previewLoaded && isFunction(this.props.onPress)) {
      this.props.onPress(this.props.name)
    }
  }

  _previewImageLoaded = () => {
    console.log('preview loaded')

    setTimeout(() => {
      this.setState({ previewLoaded: true })
    }, 0)
  }

  render() {
    const { image, name, startLoading } = this.props
    const { previewImage, previewLoaded } = this.state

    // console.info('previewLoaded: ', previewLoaded)
    // console.log('startLoading', startLoading)
    // console.info('image: ', image)
    // console.info('name: ', name)

    const Filter = Filters[name]

    // console.log('render filter ', name)

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this._onPress}
        activeOpacity={0.95}
      >
        {!previewLoaded ? (
          <ActivityIndicator
            animating
            color="white"
            style={styles.activityIndicator}
          />
        ) : null}

        {!previewLoaded && startLoading ? (
          <Surface
            ref={ref => (this._surface = ref)}
            preload={this.preload}
            style={[
              styles.surface,
              // this.props.flipImage ? styles.flipImage : null,
            ]}
            onLoad={this._surfaceLoaded}
          >
            <Filter>{image}</Filter>
          </Surface>
        ) : null}

        <Image
          style={[
            styles.preview,
            this.props.flipImage ? styles.flipImage : null,
          ]}
          source={previewImage}
          onLoad={this._previewImageLoaded}
        />

        <Text style={[styles.name, this.props.active ? styles.blueText : null]}>
          {name}
        </Text>
      </TouchableOpacity>
    )
  }
}
