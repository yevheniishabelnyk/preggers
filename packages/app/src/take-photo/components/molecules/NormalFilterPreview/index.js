/**
 *
 * FiterPreview
 *
 */

import React from 'react'
import PropTypes from 'prop-types'

import { Text, Image, TouchableOpacity } from 'react-native'
import { isFunction } from 'lodash'

import styles from './styles'

export default class FiterPreview extends React.Component {
  static propTypes = {
    image: PropTypes.object,
    name: PropTypes.string,
    onPress: PropTypes.func,
    flipImage: PropTypes.bool,
    active: PropTypes.bool,
  }

  constructor(props) {
    super(props)
  }

  _onPress = () => {
    if (isFunction(this.props.onPress)) {
      this.props.onPress(this.props.name)
    }
  }

  render() {
    const { image, name } = this.props

    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this._onPress}
        activeOpacity={0.95}
      >
        <Image
          style={[
            styles.preview,
            this.props.flipImage ? styles.flipImage : null,
          ]}
          source={image}
        />

        <Text style={[styles.name, this.props.active ? styles.blueText : null]}>
          {name}
        </Text>
      </TouchableOpacity>
    )
  }
}
