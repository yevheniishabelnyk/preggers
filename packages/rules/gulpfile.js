'use strict'

const gulp = require('gulp')
const concat = require('gulp-concat')
const bolt = require('gulp-firebase-bolt-compiler')

const paths = {
  rules: [
    'src/functions/**/*.bolt',
    'src/types/**/*.bolt',
    'src/paths/**/*.bolt',
  ],
}

gulp.task('bolt', function() {
  gulp
    .src(paths.rules)
    .pipe(concat('rules.bolt'))
    .pipe(bolt())
    .pipe(gulp.dest('./build/'))
})
