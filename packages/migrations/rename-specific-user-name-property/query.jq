import module namespace db = "http://preggersapp.com/modules/firebase" at "firebase.jq";

declare variable $projectId as string external;
declare variable $token as string external;

let $users := db:get("/users", $projectId, $token)

for $user-id in keys($users)
where $users.$user-id.email eq "demo@demo.com"
return
  copy $u := $users.$user-id
  modify (
    rename json $u("name") as "firstName"
  )
  return db:put("/users/" || $user-id, $u, $projectId, $token)

