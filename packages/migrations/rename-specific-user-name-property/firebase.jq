module namespace db = "http://preggersapp.com/modules/firebase";

import module namespace http = "http://zorba.io/modules/http-client";

declare namespace an = "http://zorba.io/annotations";

declare %an:nondeterministic function db:get($document-uri as string, $project-id as string, $token as string) {
  http:get(
    "https://" || $project-id || ".firebaseio.com" || $document-uri || ".json?access_token=" || $token
    ).body.content ! parse-json($$)
};

declare %an:sequential function db:delete($document-uri as string, $project-id as string, $token as string) {
  http:delete("https://" || $project-id || ".firebaseio.com" || $document-uri || ".json?access_token=" || $token)
};

declare %an:sequential function db:put($document-uri as string, $document as object, $project-id as string, $token as string) {
  http:put(
    "https://" || $project-id || ".firebaseio.com" || $document-uri || ".json?access_token=" || $token,
    serialize($document),
    "application/json"
  )
};
