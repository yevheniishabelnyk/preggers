import module namespace db = "http://preggersapp.com/modules/firebase" at "firebase.jq";

declare variable $projectId as string external;
declare variable $token as string external;


variable $users := db:get("/users", $projectId, $token);

variable $u :=
  for $user-id in keys($users)
  where $users.$user-id.email eq "demo@demo.com"
  return $users.$user-id;

(
  replace value of json $u.type with "mother",
  replace value of json $u.name with "Demo 2"
);

let $o := $u

return db:put("/users/" || $o.id, $o, $projectId, $token)
