# 1. Preggers Migrations

Migrations history of Preggers database.

## 1.1. Getting started

Install Docker on your machine https://www.docker.com/get-docker. Then run Docker and install Zorba json processing container

```shell
docker pull fcavalieri/zorba
```

## 1.2. Developing

Each migration is a directory with `firebase.jq`, `meta.json`, `query.jq` and `rollback.jq` files (`rollback.jq` is optional):

- `firebase.jq` has utility to communicate with Firebase Database, don't change it ever.
- `meta.json` is your migration description
- `query.jq` is your migration code written in JSONiq language
- `rollback.jq` has rollback code is your migration can be rolled back

### 1.3. JSONiq sandbox

You can test simple JSONiq code here http://pilot.zorba.io/

### 1.4. JSONiq docs

These are usefull links on JSONiq:

- http://www.jsoniq.org/docs/JSONiqExtensionToXQuery/html-single/index.html
- https://github.com/ghislainfourny/jsoniq-tutorial
- http://www.jsoniq.org/docs/JSONiq-usecases/html-single/index.html
- https://medium.com/@wcandillon/firebase-schema-evolution-ea830a62c14e

### 1.5. Examples

You can find two examples "replace-value" and "rename-specific-user-name-property". For more examples please check this link https://github.com/28msec/zorba/tree/master/test/rbkt/Queries/zorba/jsoniq

### 1.6. Migrating database

When your migration is ready, start `Docker`, then start `Preggers CLI`, navigation `migrations` and select item to migrate and press "migrate".
