# Preggers Google Cloud App Engine Cron with Google Cloud Functions for Firebase

Google App Engine provides a Cron service. Using this service for scheduling and
Google Cloud Pub/Sub for distributed messaging, you can build an application to
reliably schedule tasks which can trigger Google Cloud Functions.

This project contains:

- An App Engine application, that uses App Engine Cron Service
  to relay cron messages to Cloud Pub/Sub topics.

## Configuration

By default this engine triggers hourly, daily, and weekly. If you want to
customize this schedule for your app then you can modify the [cron.yaml](/appengine/cron.yaml).

For details on configuring this, please see the [cron.yaml Reference](https://cloud.google.com/appengine/docs/standard/python/config/cronref)
in the App Engine documentation.

## Deploying

The overview for configuring and running this sample is as follows:

### 1. Prerequisites

- If you don’t already have one, create a
  [Google Account](https://accounts.google.com/SignUp).

- Create a Developers Console project.
  1.  Install (or check that you have previously installed)
      - [`git`](https://git-scm.com/downloads)
      - [Python 2.7](https://www.python.org/download/releases/2.7/)
      - [Python `pip`](https://pip.pypa.io/en/latest/installing.html)
      - [Google Cloud SDK](http://cloud.google.com/sdk/)
  2.  [Enable the Pub/Sub API](https://console.cloud.google.com/flows/enableapi?apiid=pubsub&redirect=https://console.cloud.google.com)
  3.  [Enable Project Billing](https://support.google.com/cloud/answer/6293499#enable-billing)

### 2. Deploy to App Engine

1.  Configure the `gcloud` command-line tool to use the project your Firebase project.

```
$ gcloud config set project <your-project-id>
```

2.  Install the Python dependencies

```
$ pip install -t lib -r requirements.txt
```

3.  Create an App Engine App if you project doesn't have it yet

```
$ gcloud app create
```

4.  Deploy the application to App Engine.

```
$ gcloud app deploy app.yaml cron.yaml
```

5.  Open [Google Cloud Logging](https://console.cloud.google.com/logs/viewer) and in the right dropdown select "GAE Application". If you don't see this option, it may mean that App Engine is still in the process of deploying.

6.  Look for a log entry calling `/_ah/start`. If this entry isn't an error, then you're done deploying the App Engine app.

### 3. Verify your Cron Jobs

We can verify that our function is wired up correctly by opening the [Task Queue](https://console.cloud.google.com/appengine/taskqueues) tab in AppEngine and
clicking on **Cron Jobs**. Each of these jobs has a **Run Now** button next to it.

If you get this error "ImportError: No module named oauth2client.contrib.appengine" when you try to run job in Google Cloud Console, please do this and redeploy appengine.

```
$ pip install -t lib --upgrade oauth2client
```
