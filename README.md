# Preggers

## Upgrade your Node.js to >=8

## Clone this repo

    git clone https://bitbucket.org/strollerapp/preggers

## Navigate to repo root directory

    cd preggers/

## Install Yarn globally

    npm i -g yarn

## Install Lerna globally

    npm i -g lerna

## Install Expo CLI globally

    npm i -g exp

## Login to Expo CLI with Stroller account login and password

    exp login

## Install Firebase CLI globally

    npm i -g firebase-tools

## Login to Firebase CLI

    firebase login

## Install root dependencies

    yarn

## Install dependencies in all sub-modules

    lerna bootstrap
